<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models;

class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $datalist = array(array("role_name" => 'admin', "role_description" => 'ผู้ดูแลระบบ'),
            array("role_name" => 'manager', "role_description" => 'ผู้บริหาร'),
            array("role_name" => 'engineer', "role_description" => 'วิศวกร'),
            array("role_name" => 'foreman', "role_description" => 'ผู้ควบคุมงานก่อสร้าง'),
            array("role_name" => 'purchasing officer', "role_description" => 'เจ้าหน้าที่จัดซื้อ'),
            array("role_name" => 'store officer', "role_description" => 'เจ้าหน้าที่คลัง'),
            array("role_name" => 'employees', "role_description" => 'พนักงานทั่วไป'),
            array("role_name" => 'customer', "role_description" => 'ลูกค้า'));
        foreach ($datalist as $role) {
            Models\UserRole::create($role);
        }

        $user = array('staff_id' => '1M0001',
            'staff_username' => 'admin',
            'staff_userpwd' => md5('1234'),
            'staff_firstname' => 'Administrator',
            'staff_lastname' => 'admin',
            'staff_nickname' => 'min',
            'staff_phonenumber' => '00000000000',
            'staff_email' => 'email@mail.com',
            'staff_displayname' => 'admin',
            'staff_usertype' => 1);
        Models\Staffs::create($user);

        $user = array('staff_id' => '1M0002',
            'staff_username' => 'user1',
            'staff_userpwd' => md5('1234'),
            'staff_firstname' => 'สมศรี',
            'staff_lastname' => 'งานดี',
            'staff_nickname' => 'ศรี',
            'staff_phonenumber' => '00000000001',
            'staff_email' => 'email1@mail.com',
            'staff_displayname' => 'พนักงาน1',
            'staff_usertype' => 7);
        Models\Staffs::create($user);

        $user = array('staff_id' => '1M0003',
            'staff_username' => 'man1',
            'staff_userpwd' => md5('1234'),
            'staff_firstname' => 'สมศรี',
            'staff_lastname' => 'งานดี',
            'staff_nickname' => 'ศรี',
            'staff_phonenumber' => '00000000002',
            'staff_email' => 'email2@mail.com',
            'staff_displayname' => 'manager1',
            'staff_usertype' => 2);
        Models\Staffs::create($user);

        $user = array('staff_id' => '1M0004',
            'staff_username' => 'purchase1',
            'staff_userpwd' => md5('1234'),
            'staff_firstname' => 'สมศรี',
            'staff_lastname' => 'งานดี',
            'staff_nickname' => 'ศรี',
            'staff_phonenumber' => '00000000003',
            'staff_email' => 'email3@mail.com',
            'staff_displayname' => 'purchase1',
            'staff_usertype' => 4);
        Models\Staffs::create($user);


        $dataStore = array(array('storename_name' => 'ชำรุด'),array('storename_name' => 'OneM'));
        foreach ($dataStore as $store) {
            Models\Storename::create($store);
        }

        $dataUnit = array(array('unit_name' => 'กล่อง'), array('unit_name' => 'ชิ้น'), array('unit_name' => 'แผ่น'));
        foreach ($dataUnit as $unit) {
            Models\ProductUnit::create($unit);
        }

        $dataProduct=array(
            array('product_id'=>'001','product_name'=>'แผ่นพื้น1','product_price'=>'990','product_unit'=>3),
            array('product_id'=>'002','product_name'=>'แผ่นพื้น2','product_price'=>'990','product_unit'=>3),
            array('product_id'=>'003','product_name'=>'แผ่นพื้น3','product_price'=>'990','product_unit'=>3)
        );
        foreach($dataProduct as $product){
            Models\Product::create($product);
        }

        Models\IDgenerator::create(array('current_id' => 10));

        $coordinate = array(
            array('lat' => 15.245021, 'lng' => 104.847037, 'vehicle' => 1),
            array('lat' => 15.245857, 'lng' => 104.846001, 'vehicle' => 1),
            array('lat' => 15.246985, 'lng' => 104.844721, 'vehicle' => 1),
            array('lat' => 15.252331, 'lng' => 104.838092, 'vehicle' => 1),
            array('lat' => 15.254169, 'lng' => 104.835837, 'vehicle' => 1),
            array('lat' => 15.257364, 'lng' => 104.831944, 'vehicle' => 1),
        );
        foreach ($coordinate as $latlng) {
            Models\Travellog::create($latlng);
        }

        $coordinate = array(
            array('latitude' => 15.245021, 'longitude' => 104.847037, 'vehicle' => 1),
            array('latitude' => 15.245857, 'longitude' => 104.846001, 'vehicle' => 1),
            array('latitude' => 15.246985, 'longitude' => 104.844721, 'vehicle' => 1),
            array('latitude' => 15.252331, 'longitude' => 104.838092, 'vehicle' => 1),
            array('latitude' => 15.252331, 'longitude' => 104.838092, 'vehicle' => 1),
            array('latitude' => 15.254169, 'longitude' => 104.835837, 'vehicle' => 1),
            array('latitude' => 15.257364, 'longitude' => 104.831944, 'vehicle' => 1),
            array('latitude' => 15.243885, 'longitude' => 104.847138, 'vehicle' => 2),
            array('latitude' => 15.245251, 'longitude' => 104.848211, 'vehicle' => 2),
            array('latitude' => 15.246033, 'longitude' => 104.850641, 'vehicle' => 2),
            array('latitude' => 15.248062, 'longitude' => 104.850126, 'vehicle' => 2),
            array('latitude' => 15.251003, 'longitude' => 104.849161, 'vehicle' => 2),
            array('latitude' => 15.253524, 'longitude' => 104.848308, 'vehicle' => 2),
        );
        foreach ($coordinate as $latlng) {
            Models\GPSTrackinglog::create($latlng);
        }

        $vehicles=array(
            array('brands'=>'toyota','series'=>'vigo','license_plate'=>'1กพ2558','serial'=>'en0000001','represent_color'=>'ff0000'),
            array('brands'=>'nisson','series'=>'navara','license_plate'=>'1มค2558','serial'=>'en0000002','represent_color'=>'ffff00'),
        );
        foreach($vehicles as $v){
            Models\Vehicle::create($v);
        }

        $dataCompany = array(array('company_name' => 'BigC'), array('company_name' => 'อุบลวัสดุ'), array('company_name' => 'ยงสงวน'));
        foreach ($dataCompany as $Company) {
            Models\Company::create($Company);
        }

        $dataActivityUnit = array(array('ref_projects_activity_units_name' => 'เสาโทรเลข'),array('ref_projects_activity_units_name' => 'กม.'), array('ref_projects_activity_units_name' => 'แผ่น'), array('ref_projects_activity_units_name' => 'เมตร'), array('ref_projects_activity_units_name' => 'หลุม'));
        foreach ($dataActivityUnit as $activityUnit) {
            Models\ProjectsActivityUnits::create($activityUnit);
        }

        $dataRefProjectActivityLogWeather = array(array('ref_project_activity_log_weather_name' => 'อื่น'),array('ref_project_activity_log_weather_name' => 'ฝนตก'),array('ref_project_activity_log_weather_name' => 'แจ่มใส'));
        foreach ($dataRefProjectActivityLogWeather as $RefProjectActivityLogWeather) {
            Models\ProjectActivityLogWeather::create($RefProjectActivityLogWeather);
        }

        $dataRefProjectActivityLogProblem = array(array('ref_projects_activity_log_problem_name' => 'อื่น'),array('ref_projects_activity_log_problem_name' => 'น้ำขัง'));
        foreach ($dataRefProjectActivityLogProblem as $RefProjectActivityLogProblem) {
            Models\ProjectsActivityLogProblem::create($RefProjectActivityLogProblem);
        }
    }

}
