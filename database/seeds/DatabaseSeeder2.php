<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models;

class DatabaseSeeder2 extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        //$this->call('DatabaseSeeder2');


        $dataActivityUnit = array(array('ref_projects_activity_units_name' => 'เสาโทรเลข'),array('ref_projects_activity_units_name' => 'กม.'), array('ref_projects_activity_units_name' => 'แผ่น'), array('ref_projects_activity_units_name' => 'เมตร'), array('ref_projects_activity_units_name' => 'หลุม'));
        foreach ($dataActivityUnit as $activityUnit) {
            Models\ProjectsActivityUnits::create($activityUnit);
        }

        $dataRefProjectActivityLogWeather = array(array('ref_project_activity_log_weather_name' => 'อื่น'),array('ref_project_activity_log_weather_name' => 'ฝนตก'),array('ref_project_activity_log_weather_name' => 'แจ่มใส'));
        foreach ($dataRefProjectActivityLogWeather as $RefProjectActivityLogWeather) {
            Models\ProjectActivityLogWeather::create($RefProjectActivityLogWeather);
        }

        $dataRefProjectActivityLogProblem = array(array('ref_projects_activity_log_problem_name' => 'อื่น'),array('ref_projects_activity_log_problem_name' => 'น้ำขัง'));
        foreach ($dataRefProjectActivityLogProblem as $RefProjectActivityLogProblem) {
           Models\ProjectsActivityLogProblem::create($RefProjectActivityLogProblem);
        }
    }

}
