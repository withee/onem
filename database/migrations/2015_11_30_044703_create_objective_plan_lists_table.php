<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateObjectivePlanListsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('objective_plan_lists')) {
            Schema::create('objective_plan_lists', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('objective_plan_id');
                $table->string('list')->nullable();
                $table->float('amount', 10)->default(0.00);
                $table->float('budget', 10)->default(0.00);
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('objective_plan_lists');
    }

}
