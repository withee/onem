<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRepairAccountsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('repair_accounts')) {
            Schema::create('repair_accounts', function (Blueprint $table) {
                $table->increments('repair_accounts_id');
                $table->string('product_id')->nullable();
                $table->integer('storename_id')->nullable();
                $table->string('repair_accounts_name');
                $table->string('repair_accounts_symptom');
                $table->float('repair_accounts_price', 10)->default(0.00);
                $table->float('repair_accounts_balance', 10)->default(0.00);
                $table->enum('repair_accounts_type', array('repair', 'return', 'waste'))->default('repair');
                $table->string('payee', 64);
                $table->string('remark')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('repair_accounts');
    }

}
