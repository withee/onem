<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInventoryAccountsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('inventory_accounts')) {
            Schema::create('inventory_accounts', function (Blueprint $table) {
                $table->increments('id');
                $table->string('income_list')->nullable();
                $table->float('income', 10)->default(0.00);
                $table->string('expenses_list')->nullable();
                $table->string('expenses', 10)->default('0');
                $table->float('balance', 10)->default(0.00);
                $table->string('payee', 64);
                $table->string('remark')->nullable();
                $table->timestamps();
                $table->softDeletes();
                $table->integer('project_id')->nullable();
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('inventory_accounts');
    }

}
