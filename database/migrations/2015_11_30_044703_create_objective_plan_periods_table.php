<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateObjectivePlanPeriodsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('objective_plan_periods')) {
            Schema::create('objective_plan_periods', function (Blueprint $table) {
                $table->increments('id');
                $table->string('month', 32)->nullable();
                $table->float('plan', 10)->default(0.00);
                $table->float('completd', 10)->default(0.00);
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('objective_plan_periods');
    }

}
