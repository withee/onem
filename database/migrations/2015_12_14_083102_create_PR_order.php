<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePROrder extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if (!Schema::hasTable('pr_order')) {
            Schema::create('pr_order', function (Blueprint $table) {
                $table->increments('PR_order_id');
                $table->string('PR_order_name')->nullable();
                $table->string('PR_order_staff')->nullable();
                $table->string('PR_order_status')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('pr_order');
	}

}
