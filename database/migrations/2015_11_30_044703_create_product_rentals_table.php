<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductRentalsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('product_rentals')) {
            Schema::create('product_rentals', function (Blueprint $table) {
                $table->increments('product_rentals_id');
                $table->string('product_id')->nullable();
                $table->string('product_name');
                $table->float('product_rentals_count', 10)->default(0.00);
                $table->float('product_rentals_balance', 10)->default(0.00);
                $table->enum('product_rentals_type', array('borrow', 'return', 'dilapidated'))->default('borrow');
                $table->string('payee', 64);
                $table->string('remark')->nullable();
                $table->timestamps();
                $table->softDeletes();
                $table->integer('storename_id')->nullable();
                $table->integer('project_id')->nullable();
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product_rentals');
    }

}
