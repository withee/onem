<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsPrOrder extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('pr_order', function ($table) {
            $table->string('pr_order_tax_number')->nullable();
            $table->string('pr_order_number')->nullable();
            $table->date('pr_order_date')->nullable();
            $table->double('pr_order_tax')->default(0)->nullable();
            $table->double('pr_order_discount')->default(0)->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('pr_order', function ($table) {
            $table->dropColumn('pr_order_tax_number');
            $table->dropColumn('pr_order_number');
            $table->dropColumn('pr_order_date');
            $table->dropColumn('pr_order_tax');
            $table->dropColumn('pr_order_discount');
        });
	}

}
