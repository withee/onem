<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class AlterCountTypeToPrOrderDetailTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pr_order_detail', function(Blueprint $table)
		{
			$table->float('PR_order_detail_count')->change();
		});
	}
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pr_order_detail', function(Blueprint $table)
		{
			$table->integer('PR_order_detail_count')->change();
		});
	}

}
