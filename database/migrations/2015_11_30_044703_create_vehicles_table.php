<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVehiclesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('vehicles')) {
            Schema::create('vehicles', function (Blueprint $table) {
                $table->increments('id');
                $table->string('brands', 32);
                $table->string('series', 32);
                $table->string('color', 32)->nullable();
                $table->string('license_plate', 64)->default('no license plate');
                $table->string('serial', 64)->default('no engine number');
                $table->string('represent_color', 6)->default('ffffff');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vehicles');
    }

}
