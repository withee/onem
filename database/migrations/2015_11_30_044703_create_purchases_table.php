<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePurchasesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('purchases')) {
            Schema::create('purchases', function (Blueprint $table) {
                $table->increments('purchases_id');
                $table->integer('company_id')->unsigned()->index('purchases_company_id_foreign');
                $table->string('id_member_record', 6)->index('purchases_id_member_record_foreign');
                $table->string('id_member_toget', 6)->index('purchases_id_member_toget_foreign');
                $table->dateTime('purchases_date');
                $table->dateTime('member_toget_date');
                $table->string('purchases_number', 100);
                $table->float('purchases_vat', 10, 0);
                $table->float('purchases_discount', 10, 0);
                $table->float('purchases_sum', 10, 0);
                $table->float('purchases_total', 10, 0);
                $table->text('purchases_detail', 65535);
                $table->text('purchases_note', 65535);
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('purchases');
    }

}
