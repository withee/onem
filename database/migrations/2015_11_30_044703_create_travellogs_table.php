<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTravellogsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('travellogs')) {
            Schema::create('travellogs', function (Blueprint $table) {
                $table->increments('id');
                $table->float('lat', 10, 6);
                $table->float('lng', 10, 6);
                $table->integer('vehicle')->unsigned();
                $table->timestamps();
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('travellogs');
    }

}
