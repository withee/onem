<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStoresTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('stores')) {
            Schema::create('stores', function (Blueprint $table) {
                $table->increments('store_id');
                $table->string('product_id', 64)->index('stores_product_id_foreign');
                $table->integer('storename_id')->unsigned()->index('stores_storename_id_foreign');
                $table->float('use_product', 10, 0);
                $table->float('total_product', 10, 0);
                $table->float('balance_product', 10, 0);
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stores');
    }

}
