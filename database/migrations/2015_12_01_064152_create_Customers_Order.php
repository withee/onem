<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersOrder extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('customers_order')) {
            Schema::create('customers_order', function (Blueprint $table) {
                $table->increments('customers_order_id');
                $table->integer('customers_id')->nullable();
                $table->string('customers_order_customers')->nullable();
                $table->string('customers_order_name')->nullable();
                $table->string('customers_order_staff')->nullable();
                $table->string('customers_order_status')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customers_order');
    }

}
