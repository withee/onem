<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsCustomersOrderPlanMaterial extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('customers_order_plan_material', 'customers_order_plan_material_bring')) {
            Schema::table('customers_order_plan_material', function ($table) {
                $table->float('customers_order_plan_material_bring')->default(0)->nullable();
                $table->float('customers_order_plan_material_balance')->default(0)->nullable();
                $table->string('customers_order_plan_material_staff_bring')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers_order_plan_material', function ($table) {
            $table->dropColumn('customers_order_plan_material_bring');
            $table->dropColumn('customers_order_plan_material_balance');
            $table->dropColumn('customers_order_plan_material_staff_bring');
        });
    }

}
