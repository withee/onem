<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersOrderMaterial extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('customers_order_material')) {
            Schema::create('customers_order_material', function (Blueprint $table) {
                $table->increments('customers_order_material_id');
                $table->integer('customers_order_product_id');
                $table->integer('storename_id');
                $table->string('customers_order_material_name')->nullable();
                $table->string('customers_order_material_count')->nullable();
                $table->string('customers_order_material_staff')->nullable();
                $table->string('customers_order_material_status')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customers_order_material');
    }

}
