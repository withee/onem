<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('products')) {
            Schema::create('products', function (Blueprint $table) {
                $table->string('product_id', 64)->primary();
                $table->string('product_name', 32)->unique();
                $table->integer('product_price');
                $table->integer('product_unit')->unsigned()->index('products_product_unit_foreign');
                $table->enum('product_type', array('material', 'manufacture', 'equipment'))->default('material');
                $table->timestamps();
                $table->softDeletes();
                $table->integer('minimum')->nullable();
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }

}
