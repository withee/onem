<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsOrderTransportProduct extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('customers_order_transport_product', function ($table) {
            $table->integer('vehicles_id')->nullable();
            $table->string('vehicles_name')->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('customers_order_transport_product', function ($table) {
            $table->dropColumn('vehicles_id');
            $table->dropColumn('vehicles_name');
        });
	}

}
