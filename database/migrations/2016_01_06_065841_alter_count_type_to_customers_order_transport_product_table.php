<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCountTypeToCustomersOrderTransportProductTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers_order_transport_product', function (Blueprint $table) {
            $table->float('customers_order_product_count')->change();
            $table->float('customers_order_transport_product_complete')->change();
            $table->float('customers_order_transport_product_waste')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers_order_transport_product', function (Blueprint $table) {
            $table->integer('customers_order_product_count')->change();
            $table->integer('customers_order_transport_product_complete')->change();
            $table->integer('customers_order_transport_product_waste')->change();
        });
    }

}
