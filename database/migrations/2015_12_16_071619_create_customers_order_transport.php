<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersOrderTransport extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('customers_order_transport')) {
            Schema::create('customers_order_transport', function (Blueprint $table) {
                $table->increments('customers_order_transport_id');
                $table->integer('customers_order_id')->nullable();
                $table->string('customers_order_transport_code')->nullable();
                $table->string('customers_order_transport_staff')->nullable();
                $table->string('customers_order_transport_status')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customers_order_transport');
    }

}
