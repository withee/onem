<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBringsDetailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(!Schema::hasTable('brings_detail')) {
			Schema::create('brings_detail', function (Blueprint $table) {
				$table->increments('bring_detail_id');
				$table->integer('bring_id')->unsigned();
				$table->string('product_id', 64);
				$table->integer('storename_id')->unsigned();
				$table->enum('bring_type', array('bring', 'deteriorate'))->default('bring');
				$table->float('bring_detail_count', 10, 0);
				$table->float('bring_detail_price', 10, 0)->nullable();
				$table->text('bring_detail_location', 65535)->nullable();
				$table->timestamps();
				$table->softDeletes();
			});
		}
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('brings_detail');
	}

}
