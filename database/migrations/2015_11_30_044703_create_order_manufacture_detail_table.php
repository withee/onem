<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderManufactureDetailTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('order_manufacture_detail')) {
            Schema::create('order_manufacture_detail', function (Blueprint $table) {
                $table->increments('order_manufacture_detail_id');
                $table->integer('project_id')->nullable();
                $table->string('product_id')->nullable();
                $table->string('staff_id', 6)->nullable();
                $table->integer('storename_id')->nullable();
                $table->float('count', 10, 0)->nullable();
                $table->enum('status', array('add', 'bring', 'approve'))->default('add');
                $table->timestamps();
                $table->softDeletes();
                $table->integer('order_manufacture_id')->nullable();
                $table->float('bring', 10, 0)->nullable();
                $table->float('balance', 10, 0)->nullable();
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('order_manufacture_detail');
    }

}
