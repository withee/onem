<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsPriceAndTotalCustomersOrderProduct extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('customers_order_product', function ($table) {
            $table->double('customers_order_product_price')->default(0)->nullable();
            $table->double('customers_order_product_total')->default(0)->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('customers_order_product', function ($table) {
            $table->dropColumn('customers_order_product_price');
            $table->dropColumn('customers_order_product_total');
        });
	}

}
