<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePrListsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('pr_lists')) {
            Schema::create('pr_lists', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('pr_id');
                $table->string('product_id', 64);
                $table->integer('quantity')->default(0);
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pr_lists');
    }

}
