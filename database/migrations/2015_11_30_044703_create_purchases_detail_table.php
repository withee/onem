<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePurchasesDetailTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('purchases_detail')) {
            Schema::create('purchases_detail', function (Blueprint $table) {
                $table->increments('purchasesdetail_id');
                $table->integer('purchases_id')->unsigned()->index('purchases_detail_purchases_id_foreign');
                $table->string('product_id', 64)->index('purchases_detail_product_id_foreign');
                $table->integer('storename_id')->unsigned()->index('purchases_detail_storename_id_foreign');
                $table->string('id_member_record', 6)->nullable();
                $table->dateTime('purchasesdetail_dateproduction')->nullable();
                $table->dateTime('purchasesdetail_dateexpiration')->nullable();
                $table->dateTime('purchasesdetail_dateproductin');
                $table->float('purchasesdetail_productcount', 10, 0);
                $table->float('purchasesdetail_productprice', 10, 0);
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('purchases_detail');
    }

}
