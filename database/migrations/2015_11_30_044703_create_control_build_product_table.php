<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateControlBuildProductTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('control_build_product')) {
            Schema::create('control_build_product', function (Blueprint $table) {
                $table->increments('control_build_product_id');
                $table->integer('control_build_id')->unsigned();
                $table->string('product_id', 64);
                $table->integer('control_build_product_count');
                $table->enum('control_build_product_type', array('define', 'manufacture', 'deteriorate'))->default('define');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('control_build_product');
    }

}
