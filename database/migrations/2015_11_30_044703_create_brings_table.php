<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBringsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(!Schema::hasTable('brings')) {
			Schema::create('brings', function (Blueprint $table) {
				$table->increments('bring_id');
				$table->integer('group_bring_id')->unsigned()->nullable();
				$table->integer('project_sub_id')->unsigned()->nullable();
				$table->string('bring_number', 50);
				$table->enum('bring_type_manufacture', array('Y', 'N'))->default('N');
				$table->enum('bring_type_project', array('Y', 'N'))->default('N');
				$table->enum('bring_type_other', array('Y', 'N'))->default('N');
				$table->string('id_member_bring', 6)->nullable();
				$table->string('id_member_approve_bring', 6)->nullable();
				$table->string('id_member_approve_pay', 6)->nullable();
				$table->string('id_member_store', 6)->nullable();
				$table->string('id_member_account', 6)->nullable();
				$table->string('id_member_driver', 6)->nullable();
				$table->string('id_member_record', 6);
				$table->dateTime('member_bring_date')->nullable();
				$table->dateTime('member_approve_bring_date')->nullable();
				$table->dateTime('member_approve_pay_date')->nullable();
				$table->dateTime('member_store_date')->nullable();
				$table->dateTime('member_account_date')->nullable();
				$table->dateTime('member_driver_date')->nullable();
				$table->integer('bring_vehicles_id')->unsigned()->nullable();
				$table->dateTime('bring_date');
				$table->string('bring_depertment', 100)->nullable();
				$table->text('bring_detail', 65535)->nullable();
				$table->text('bring_note', 65535)->nullable();
				$table->timestamps();
				$table->softDeletes();
			});
		}
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('brings');
	}

}
