<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompanysTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('companys')) {
            Schema::create('companys', function (Blueprint $table) {
                $table->increments('company_id');
                $table->string('company_name');
                $table->timestamps();
                $table->softDeletes();
                $table->string('location')->nullable();
                $table->string('phone', 20)->nullable();
                $table->string('contractor')->nullable();
                $table->string('latitude')->default('15.2418413');
                $table->string('longitude')->default('104.8507708');
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('companys');
    }

}
