<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRefProjectsActivityUnitsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('ref_projects_activity_units')) {
            Schema::create('ref_projects_activity_units', function (Blueprint $table) {
                $table->increments('ref_projects_activity_units_id');
                $table->string('ref_projects_activity_units_name');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ref_projects_activity_units');
    }

}
