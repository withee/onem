<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompanyMaintenanceTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('company_maintenance')) {
            Schema::create('company_maintenance', function (Blueprint $table) {
                $table->increments('company_maintenance_id');
                $table->string('company_maintenance_name')->nullable();
                $table->string('company_maintenance_tel')->nullable();
                $table->text('company_maintenance_address', 65535)->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('company_maintenance');
    }

}
