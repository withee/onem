<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmployeesDailyTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('employees_daily')) {
            Schema::create('employees_daily', function (Blueprint $table) {
                $table->increments('employees_daily_id');
                $table->integer('project_id')->nullable();
                $table->string('employees_daily_record', 64)->nullable();
                $table->integer('employees_daily_man')->nullable();
                $table->integer('employees_daily_price_man')->nullable();
                $table->integer('employees_daily_lady')->nullable();
                $table->integer('employees_daily_price_lady')->nullable();
                $table->dateTime('employees_daily_work')->nullable();
                $table->text('employees_daily_detail', 65535)->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }

    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employees_daily');
    }

}
