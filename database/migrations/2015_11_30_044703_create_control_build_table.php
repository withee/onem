<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateControlBuildTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('control_build')) {
            Schema::create('control_build', function (Blueprint $table) {
                $table->increments('control_build_id');
                $table->dateTime('control_build_create_date')->nullable();
                $table->dateTime('control_build_complete_date')->nullable();
                $table->string('lot_No', 64)->nullable();
                $table->string('control_build_type', 64);
                $table->string('control_build_name');
                $table->text('control_build_detail', 65535)->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('control_build');
    }

}
