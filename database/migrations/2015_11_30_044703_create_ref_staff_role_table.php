<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRefStaffRoleTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('ref_staff_role')) {
            Schema::create('ref_staff_role', function (Blueprint $table) {
                $table->increments('role_id');
                $table->string('role_name', 128);
                $table->text('role_description', 65535);
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ref_staff_role');
    }

}
