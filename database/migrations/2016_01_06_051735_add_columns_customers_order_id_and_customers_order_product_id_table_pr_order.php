<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsCustomersOrderIdAndCustomersOrderProductIdTablePrOrder extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('pr_order', function ($table) {
            $table->integer('customers_order_id')->default(0)->nullable();
            $table->integer('customers_order_product_id')->default(0)->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('pr_order', function ($table) {
            $table->dropColumn('customers_order_id');
            $table->dropColumn('customers_order_product_id');
        });
	}

}
