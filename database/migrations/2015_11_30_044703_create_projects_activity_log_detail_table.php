<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProjectsActivityLogDetailTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('projects_activity_log_detail')) {
            Schema::create('projects_activity_log_detail', function (Blueprint $table) {
                $table->increments('projects_activity_log_detail_id');
                $table->integer('projects_activity_log_id')->unsigned()->nullable();
                $table->integer('projects_activity_id')->unsigned()->nullable();
                $table->string('id_staff_record', 6)->nullable();
                $table->string('id_staff_works', 6)->nullable();
                $table->string('id_staff_check', 6)->nullable();
                $table->integer('projects_activity_log_detail_value')->unsigned()->nullable();
                $table->enum('projects_activity_log_detail_type', array('Work', 'Bring', 'Finish', 'Success', 'Dilapidated'))->default('Work');
                $table->text('projects_activity_log_detail_note', 65535)->nullable();
                $table->timestamps();
                $table->softDeletes();
                $table->string('start_at', 32)->default('000');
                $table->string('end_at', 32)->default('000');
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('projects_activity_log_detail');
    }

}
