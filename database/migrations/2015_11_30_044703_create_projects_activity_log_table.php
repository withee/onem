<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProjectsActivityLogTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('projects_activity_log')) {
            Schema::create('projects_activity_log', function (Blueprint $table) {
                $table->increments('projects_activity_log_id');
                $table->integer('project_id')->unsigned()->nullable();
                $table->integer('project_sub_id')->unsigned()->nullable();
                $table->string('id_staff_record', 6)->nullable();
                $table->string('id_staff_assign', 6)->nullable();
                $table->string('id_staff_check', 6)->nullable();
                $table->integer('projects_activity_log_worker')->nullable();
                $table->string('projects_activity_log_km')->nullable();
                $table->string('projects_activity_log_village')->nullable();
                $table->string('projects_activity_log_tambon')->nullable();
                $table->string('projects_activity_log_amphoe')->nullable();
                $table->string('projects_activity_log_province')->nullable();
                $table->text('projects_activity_log_dailyEvents', 65535)->nullable();
                $table->text('projects_activity_log_weather', 65535)->nullable();
                $table->text('projects_activity_log_problem', 65535)->nullable();
                $table->text('projects_activity_log_recommend', 65535)->nullable();
                $table->dateTime('projects_activity_start_date')->nullable();
                $table->dateTime('projects_activity_end_date')->nullable();
                $table->timestamps();
                $table->softDeletes();
                $table->string('projects_activity_log_start_telegraph_poles')->nullable();
                $table->string('projects_activity_log_start_telegraph_poles_total')->nullable();
                $table->string('projects_activity_log_end_telegraph_poles')->nullable();
                $table->string('projects_activity_log_end_telegraph_poles_total')->nullable();
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('projects_activity_log');
    }

}
