<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePROrderDetail extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('pr_order_detail')) {
            Schema::create('pr_order_detail', function (Blueprint $table) {
                $table->increments('PR_order_detail_id');
                $table->integer('PR_order_id');
                $table->string('PR_order_detail_name')->nullable();
                $table->integer('PR_order_detail_count')->default(0);
                $table->string('PR_order_detail_staff')->nullable();
                $table->string('PR_order_detail_status')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pr_order_detail');
    }

}
