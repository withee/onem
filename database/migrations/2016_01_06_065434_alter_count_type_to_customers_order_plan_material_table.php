<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCountTypeToCustomersOrderPlanMaterialTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers_order_plan_material', function (Blueprint $table) {
            $table->float('customers_order_plan_material_bring')->change();
            $table->float('customers_order_plan_material_balance')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers_order_plan_material', function (Blueprint $table) {
            $table->integer('customers_order_plan_material_bring')->change();
            $table->integer('customers_order_plan_material_balance')->change();
        });
    }

}
