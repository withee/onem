<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CustomersOrderPlan extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('customers_order_plan')) {
            Schema::create('customers_order_plan', function (Blueprint $table) {
                $table->increments('customers_order_plan_id');
                $table->integer('customers_order_id');
                $table->integer('customers_order_product_id');
                $table->date('customers_order_plan_date')->nullable();
                $table->integer('customers_order_plan_count')->nullable();
                $table->string('customers_order_plan_staff')->nullable();
                $table->string('customers_order_plan_status')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customers_order_plan');
    }

}
