<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProjectsSubTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('projects_sub')) {
            Schema::create('projects_sub', function (Blueprint $table) {
                $table->increments('project_sub_id');
                $table->integer('project_id')->unsigned()->nullable();
                $table->string('id_staff_create', 6)->nullable();
                $table->string('project_sub_name');
                $table->timestamps();
                $table->softDeletes();
                $table->string('project_position');
                $table->string('start_at', 32)->default('0.00+000');
                $table->string('end_at', 32)->default('0.00+000');
                $table->float('amount_POR', 10, 0)->nullable()->default(0);
                $table->float('amount_survey', 10, 0)->nullable()->default(0);
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('projects_sub');
    }

}
