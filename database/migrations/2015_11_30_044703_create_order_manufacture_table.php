<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderManufactureTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('order_manufacture')) {
            Schema::create('order_manufacture', function (Blueprint $table) {
                $table->increments('order_manufacture_id');
                $table->integer('project_id')->nullable();
                $table->string('product_id')->nullable();
                $table->string('staff_id_asset', 6)->nullable();
                $table->string('staff_id_add', 6)->nullable();
                $table->integer('order_manufacture_count')->nullable();
                $table->integer('order_manufacture_finished')->nullable();
                $table->integer('order_manufacture_balance')->nullable();
                $table->enum('order_manufacture_type', array('add', 'bring', 'approve', 'finish'))->default('add');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('order_manufacture');
    }

}
