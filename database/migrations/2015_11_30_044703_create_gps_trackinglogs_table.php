<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGpsTrackinglogsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('gps_trackinglogs')) {
            Schema::create('gps_trackinglogs', function (Blueprint $table) {
                $table->increments('id');
                $table->float('latitude', 10, 6);
                $table->float('longitude', 10, 6);
                $table->string('serial')->default('SN-000001');
                $table->integer('vehicle')->unsigned();
                $table->timestamps();
                $table->integer('parking')->default(0);
                $table->integer('distance_from_previous')->default(-1);
                $table->float('speed', 10)->default(0.00);
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gps_trackinglogs');
    }

}
