<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStaffsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('staffs')) {
            Schema::create('staffs', function (Blueprint $table) {
                $table->string('staff_id', 6)->primary();
                $table->string('staff_username', 32)->unique();
                $table->string('staff_userpwd', 64);
                $table->string('staff_firstname', 64);
                $table->string('staff_lastname', 64);
                $table->string('staff_nickname', 64);
                $table->string('staff_phonenumber', 12)->unique();
                $table->string('staff_email')->unique();
                $table->integer('staff_usertype')->unsigned()->default(0);
                $table->string('staff_displayname', 64);
                $table->text('staff_avatar', 65535)->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('staffs');
    }

}
