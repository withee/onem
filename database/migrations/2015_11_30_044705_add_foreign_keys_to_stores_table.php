<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToStoresTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('stores', 'product_id')) {
            Schema::table('stores', function (Blueprint $table) {
                $table->foreign('product_id')->references('product_id')->on('products')->onUpdate('RESTRICT')->onDelete('RESTRICT');
                $table->foreign('storename_id')->references('storename_id')->on('ref_store_name')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stores', function (Blueprint $table) {
            $table->dropForeign('stores_product_id_foreign');
            $table->dropForeign('stores_storename_id_foreign');
        });
    }

}
