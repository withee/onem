<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVehiclesMaintenanceTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('vehicles_maintenance')) {
            Schema::create('vehicles_maintenance', function (Blueprint $table) {
                $table->increments('vehicles_maintenance_id');
                $table->integer('company_maintenance_id')->nullable();
                $table->string('vehicles_maintenance_bill_id')->nullable();
                $table->integer('vehicles_id')->nullable();
                $table->string('staff_id', 6)->nullable();
                $table->text('vehicles_maintenance_symptom', 65535)->nullable();
                $table->text('vehicles_maintenance_remark', 65535)->nullable();
                $table->enum('vehicles_maintenance_status', array('repair', 'pay'))->default('repair');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vehicles_maintenance');
    }

}
