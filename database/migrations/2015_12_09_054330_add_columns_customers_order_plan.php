<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsCustomersOrderPlan extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('customers_order_plan', 'customers_order_plan_complete')) {
            Schema::table('customers_order_plan', function ($table) {
                $table->integer('customers_order_plan_complete')->default(0)->nullable();
                $table->integer('customers_order_plan_waste')->default(0)->nullable();
                $table->string('customers_order_plan_staff_check')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers_order_plan', function ($table) {
            $table->dropColumn('customers_order_plan_complete');
            $table->dropColumn('customers_order_plan_waste');
            $table->dropColumn('customers_order_plan_staff_check');
        });
    }

}
