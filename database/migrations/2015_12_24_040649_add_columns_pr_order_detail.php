<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsPrOrderDetail extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('pr_order_detail', function ($table) {
            $table->double('PR_order_detail_accepted')->nullable();
            $table->double('pr_order_detail_price')->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('pr_order_detail', function ($table) {
            $table->dropColumn('PR_order_detail_accepted');
            $table->dropColumn('pr_order_detail_price');
        });
	}

}
