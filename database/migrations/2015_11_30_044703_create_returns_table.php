<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReturnsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('returns')) {
            Schema::create('returns', function (Blueprint $table) {
                $table->increments('return_id');
                $table->string('return_number', 50);
                $table->string('bring_number', 50)->nullable();
                $table->enum('return_type_manufacture', array('Y', 'N'))->default('N');
                $table->enum('return_type_project', array('Y', 'N'))->default('N');
                $table->enum('return_type_other', array('Y', 'N'))->default('N');
                $table->string('id_member_return', 6)->nullable();
                $table->string('id_member_approve_return', 6)->nullable();
                $table->string('id_member_approve_pay', 6)->nullable();
                $table->string('id_member_store', 6)->nullable();
                $table->string('id_member_account', 6)->nullable();
                $table->string('id_member_driver', 6)->nullable();
                $table->string('id_member_record', 6);
                $table->dateTime('member_return_date')->nullable();
                $table->dateTime('member_approve_return_date')->nullable();
                $table->dateTime('member_approve_pay_date')->nullable();
                $table->dateTime('member_store_date')->nullable();
                $table->dateTime('member_account_date')->nullable();
                $table->dateTime('member_driver_date')->nullable();
                $table->dateTime('return_date');
                $table->string('return_depertment', 100)->nullable();
                $table->text('return_detail', 65535)->nullable();
                $table->text('return_note', 65535)->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('returns');
    }

}
