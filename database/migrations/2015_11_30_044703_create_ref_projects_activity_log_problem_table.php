<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRefProjectsActivityLogProblemTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('ref_projects_activity_log_problem')) {
            Schema::create('ref_projects_activity_log_problem', function (Blueprint $table) {
                $table->increments('ref_projects_activity_log_problem_id');
                $table->string('ref_projects_activity_log_problem_name');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ref_projects_activity_log_problem');
    }

}
