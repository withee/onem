<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsCustomersOrder extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if (!Schema::hasColumn('customers_order', 'customers_order_tax_number')) {
            Schema::table('customers_order', function ($table) {
                $table->string('customers_order_tax_number')->nullable();
                $table->string('customers_order_number')->nullable();
                $table->date('customers_order_date')->nullable();
                $table->double('customers_order_tax')->default(0)->nullable();
                $table->double('customers_order_discount')->default(0)->nullable();
            });
        }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('customers_order', function ($table) {
            $table->dropColumn('customers_order_tax_number');
            $table->dropColumn('customers_order_number');
            $table->dropColumn('customers_order_date');
            $table->dropColumn('customers_order_tax');
            $table->dropColumn('customers_order_discount');
        });
	}

}
