<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateControlBuildLogTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('control_build_log')) {
            Schema::create('control_build_log', function (Blueprint $table) {
                $table->increments('control_build_log_id');
                $table->string('staff_id', 6);
                $table->integer('control_build_id')->unsigned();
                $table->integer('bring_id')->unsigned()->nullable();
                $table->integer('control_build_product_id')->unsigned()->nullable();
                $table->enum('control_build_log_type', array('bring', 'deteriorate', 'manufacture'))->default('bring');
                $table->dateTime('control_build_log_date')->nullable();
                $table->text('control_build_log_detail', 65535)->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('control_build_log');
    }

}
