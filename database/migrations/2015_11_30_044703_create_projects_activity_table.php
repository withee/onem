<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProjectsActivityTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('projects_activity')) {
            Schema::create('projects_activity', function (Blueprint $table) {
                $table->increments('projects_activity_id');
                $table->integer('project_id')->unsigned()->nullable();
                $table->string('id_staff_record', 6)->nullable();
                $table->string('projects_activity_name');
                $table->integer('projects_activity_value')->unsigned()->nullable();
                $table->integer('projects_activity_unit')->unsigned()->nullable();
                $table->timestamps();
                $table->softDeletes();
                $table->float('projects_activity_convert_mm', 10, 0)->unsigned()->nullable();
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('projects_activity');
    }

}
