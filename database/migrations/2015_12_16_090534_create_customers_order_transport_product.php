<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersOrderTransportProduct extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('customers_order_transport_product')) {
            Schema::create('customers_order_transport_product', function (Blueprint $table) {
                $table->increments('customers_order_transport_product_id');
                $table->integer('customers_order_id')->nullable();
                $table->integer('customers_order_transport_id')->nullable();
                $table->integer('customers_order_product_id')->nullable();
                $table->integer('customers_order_product_count')->default(0)->nullable();
                $table->integer('customers_order_transport_product_complete')->default(0)->nullable();
                $table->integer('customers_order_transport_product_waste')->default(0)->nullable();
                $table->string('customers_order_transport_product_staff')->nullable();
                $table->string('customers_order_transport_product_status')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customers_order_transport_product');
    }

}
