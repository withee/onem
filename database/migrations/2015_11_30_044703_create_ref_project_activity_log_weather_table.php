<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRefProjectActivityLogWeatherTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('ref_project_activity_log_weather')) {
            Schema::create('ref_project_activity_log_weather', function (Blueprint $table) {
                $table->increments('ref_project_activity_log_weather_id');
                $table->string('ref_project_activity_log_weather_name');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ref_project_activity_log_weather');
    }

}
