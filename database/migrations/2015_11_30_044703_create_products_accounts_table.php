<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsAccountsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('products_accounts')) {
            Schema::create('products_accounts', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('project_id')->nullable();
                $table->string('income_product_id');
                $table->float('income', 10)->default(0.00);
                $table->string('expenses_product_id');
                $table->string('expenses', 10)->default('0');
                $table->float('balance', 10)->default(0.00);
                $table->string('payee', 64);
                $table->string('remark')->nullable();
                $table->timestamps();
                $table->softDeletes();
                $table->string('product_id')->nullable();
                $table->integer('storename_id')->nullable();
                $table->float('price', 10, 0)->nullable()->default(0);
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products_accounts');
    }

}
