<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsCustomersOrderProduct extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('customers_order_product', 'customers_order_product_type')) {
            Schema::table('customers_order_product', function ($table) {
                $table->string('customers_order_product_type')->default('Normal')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers_order_product', function ($table) {
            $table->dropColumn('customers_order_product_type');
        });
    }

}
