<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsDepositCustomersOrder extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('customers_order', function ($table) {
            $table->double('customers_order_deposit')->default(0)->nullable();
            $table->date('customers_order_deposit_date_set')->nullable();
            $table->date('customers_order_deposit_date_limit')->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('customers_order', function ($table) {
            $table->dropColumn('customers_order_deposit');
            $table->dropColumn('customers_order_deposit_date_set');
            $table->dropColumn('customers_order_deposit_date_limit');
        });
	}

}
