<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVehiclesMaintenanceDetailTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('vehicles_maintenance_detail')) {
            Schema::create('vehicles_maintenance_detail', function (Blueprint $table) {
                $table->increments('vehicles_maintenance_detail_id');
                $table->integer('vehicles_maintenance_id')->nullable();
                $table->integer('vehicles_maintenance_detail_count')->nullable();
                $table->text('vehicles_maintenance_detail_name', 65535)->nullable();
                $table->float('vehicles_maintenance_detail_price', 10, 0)->nullable();
                $table->float('vehicles_maintenance_detail_total', 10, 0)->nullable();
                $table->enum('vehicles_maintenance_detail_status', array('add', 'pay'))->default('add');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vehicles_maintenance_detail');
    }

}
