<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPurchasesDetailTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('purchases_detail', 'product_id')) {
            Schema::table('purchases_detail', function (Blueprint $table) {
                $table->foreign('product_id')->references('product_id')->on('products')->onUpdate('RESTRICT')->onDelete('RESTRICT');
                $table->foreign('purchases_id')->references('purchases_id')->on('purchases')->onUpdate('RESTRICT')->onDelete('RESTRICT');
                $table->foreign('storename_id')->references('storename_id')->on('ref_store_name')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchases_detail', function (Blueprint $table) {
            $table->dropForeign('purchases_detail_product_id_foreign');
            $table->dropForeign('purchases_detail_purchases_id_foreign');
            $table->dropForeign('purchases_detail_storename_id_foreign');
        });
    }

}
