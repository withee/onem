<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPurchasesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('purchases', 'company_id')) {
            Schema::table('purchases', function (Blueprint $table) {
                $table->foreign('company_id')->references('company_id')->on('companys')->onUpdate('RESTRICT')->onDelete('RESTRICT');
                $table->foreign('id_member_record')->references('staff_id')->on('staffs')->onUpdate('RESTRICT')->onDelete('RESTRICT');
                $table->foreign('id_member_toget')->references('staff_id')->on('staffs')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchases', function (Blueprint $table) {
            $table->dropForeign('purchases_company_id_foreign');
            $table->dropForeign('purchases_id_member_record_foreign');
            $table->dropForeign('purchases_id_member_toget_foreign');
        });
    }

}
