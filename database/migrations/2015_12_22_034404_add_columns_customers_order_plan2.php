<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsCustomersOrderPlan2 extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('customers_order_plan', 'customers_order_plan_type')) {
            Schema::table('customers_order_plan', function ($table) {
                $table->string('customers_order_plan_type')->default('Normal')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers_order_plan', function ($table) {
            $table->dropColumn('customers_order_plan_type');
        });
    }

}
