<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersOrderProduct extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('customers_order_product')) {
            Schema::create('customers_order_product', function (Blueprint $table) {
                $table->increments('customers_order_product_id');
                $table->integer('customers_order_id');
                $table->string('customers_order_product_name')->nullable();
                $table->string('customers_order_product_count')->nullable();
                $table->string('customers_order_product_staff')->nullable();
                $table->string('customers_order_product_status')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customers_order_product');
    }

}
