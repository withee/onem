<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToProductsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('products', 'product_unit')) {
            Schema::table('products', function (Blueprint $table) {
                $table->foreign('product_unit')->references('unit_id')->on('ref_product_units')->onUpdate('CASCADE')->onDelete('CASCADE');
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public
    function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropForeign('products_product_unit_foreign');
        });
    }

}
