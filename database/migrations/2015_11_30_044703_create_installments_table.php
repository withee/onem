<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInstallmentsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('installments')) {
            Schema::create('installments', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('project_id')->unsigned();
                $table->integer('km_start')->unsigned()->default(0);
                $table->integer('km_stop')->unsigned()->default(0);
                $table->integer('meter_start')->unsigned()->default(0);
                $table->integer('meter_stop')->unsigned()->default(0);
                $table->float('distance', 10)->default(0.00);
                $table->integer('period')->unsigned()->default(1);
                $table->string('recorder');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('installments');
    }

}
