<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateObjectivePlansTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('objective_plans')) {
            Schema::create('objective_plans', function (Blueprint $table) {
                $table->increments('id');
                $table->string('project_name')->nullable();
                $table->string('contract')->nullable();
                $table->string('contractor')->nullable();
                $table->float('budget', 10)->default(0.00);
                $table->float('fine', 10)->default(0.00);
                $table->integer('contract_days')->default(0);
                $table->date('contract_start')->nullable();
                $table->date('contract_end')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('objective_plans');
    }

}
