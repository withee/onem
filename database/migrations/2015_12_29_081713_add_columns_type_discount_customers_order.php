<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsTypeDiscountCustomersOrder extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('customers_order', function ($table) {
            $table->string('customers_order_type_discount')->default('Normal')->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('customers_order', function ($table) {
            $table->dropColumn('customers_order_type_discount');
        });
	}

}
