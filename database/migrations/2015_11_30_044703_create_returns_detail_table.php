<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReturnsDetailTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('returns_detail')) {
            Schema::create('returns_detail', function (Blueprint $table) {
                $table->increments('return_detail_id');
                $table->integer('return_id')->unsigned();
                $table->string('product_id', 64);
                $table->integer('storename_id')->unsigned();
                $table->float('return_detail_count', 10, 0);
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('returns_detail');
    }

}
