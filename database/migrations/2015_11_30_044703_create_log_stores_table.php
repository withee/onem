<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLogStoresTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('log_stores')) {
            Schema::create('log_stores', function (Blueprint $table) {
                $table->increments('log_stores_id');
                $table->string('id_member', 6)->nullable();
                $table->integer('storename_id')->unsigned();
                $table->string('product_id', 64);
                $table->integer('log_stores_count')->nullable();
                $table->enum('log_stores_type', array('minus', 'plus', 'new'))->default('plus');
                $table->text('log_return_detail', 65535)->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('log_stores');
    }

}
