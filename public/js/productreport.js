/**
 * Created by mrsyrop on 16/6/2558.
 */
$(document).ready(function () {
    $("#productId").change(function () {
        var pid = $(this).val();
        //alert(pid);
        $("#store_list").empty();
        $.ajax({
            url: "/products/fromallstore",
            type: "post",
            dataType: 'json',
            data: {pid: pid}
        }).success(function (res) {
            console.log(res);
            $.each(res.data, function (key, val) {
                var el = "<tr>";
                el+="<td>"+(key+1)+"</td>";
                el+="<td>"+val.storename_name+"</td>";
                el+="<td>"+(val.use_product+val.balance_product)+"</td>";
                el+="<td>"+val.use_product+"</td>";
                el+="<td>"+val.balance_product+"</td>";
                el+="</tr>";
                $("#store_list").append(el);
            });
        });
    });
});