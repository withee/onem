/**
 * Created by mrsyrop on 10/5/2558.
 */
$(document).ready(function () {
    var staff_id = "1M0000";
    $("#addManage").click(function () {
        $("#addstaff").modal('show');
        $("#submit_edit").hide();
        $("#add-form").reset();

    });
    $("#submit_add").click(function () {
        var username = $('#add_username').val();
        var userpwd = $('#add_userpwd').val();
        var userrepwd = $('#add_userrepwd').val();
        var name = $('#add_name').val();
        var surname = $('#add_surname').val();
        var nickname = $('#add_nickname').val();
        var email = $('#add_email').val();
        var phone = $('#add_phone').val();
        var type = $('#changed-type').val();
        var allpass = true;

        if (allpass) {
            var data = $('form#add-form').serializeArray();
            $.ajax({
                url: "/staffs/add",
                type: "post",
                dataType: 'json',
                data: data
            }).success(function (res) {
                //var res = JSON.parse(response);
                alert(res.desc);
                console.log(res);
                if(!res.success){
                    $('#error-report').empty();
                    $.each(res.data,function(key,value){
                       if(key!='__proto__') {
                           //alert(value[0]);
                           $('#error-report').append("<label>"+value[0]+"</label>");
                       }
                    });
                    $('#wrong').removeClass('hide');
                }else{
                    $('#wrong').addClass('hide');
                }
            });
        }else{

        }
    });
    $("#submit_edit").click(function () {
        var username = $('#add_username').val();
        var userpwd = $('#add_userpwd').val();
        var name = $('#add_name').val();
        var surname = $('#add_surname').val();
        var nickname = $('#add_nickname').val();
        var email = $('#add_email').val();
        var phone = $('#add_phone').val();
        var type = $('#changed-type').val();

        var data = $('form#add-form').serializeArray();
        //console.log(data);
        //console.log(name);
        $.ajax({
            url: "/staffs/edit",
            type: "get",
            dataType: 'json',
            data: data
        }).success(function (res) {
            //var res = JSON.parse(response);
            alert(res.desc);
            console.log(res);
        });
    });
    $(".manage_Edit").click(function () {
        var uid = $(this).attr('uid');
        $("#addstaff").modal('show');
        staff_id = uid;
        $.ajax({
            url: "/staffs/displaybyid",
            type: "get",
            dataType: 'json',
            data: {uid: uid}
        }).success(function (res) {
            //var res = JSON.parse(response);
            if (!res.success) {
                alert(res.desc);
            } else {
                console.log(res);
                $("#staff_id").val(uid);
                $("#add_username").val(res.data.staff_username);
                $("#add_userpwd").val(res.data.staff_userpwd);
                $("#add_userrepwd").val(res.data.staff_userpwd);
                $("#add_name").val(res.data.staff_firstname);
                $("#add_surname").val(res.data.staff_lastname);
                $("#add_nickname").val(res.data.staff_nickname);
                $("#add_email").val(res.data.staff_email);
                $("#add_phone").val(res.data.staff_phonenumber);
                $(".type-list").prop('selected', false);
                $(".type-list[value=" + res.data.staff_usertype + "]").prop('selected', true);
                $("#submit_add").hide();
            }
        });
    });
    $(".close").click(function () {
        var parent = $(this).attr('parent');
        //alert(parent);
        $("#" + parent).modal('hide');
        $("#submit_edit").show();
        $("#submit_add").show();
        $('#wrong').addClass('hide');
        location.reload();

    });
    $("#submit_cancel").click(function () {
        var parent = $(this).attr('parent');
        //alert(parent);
        $("#" + parent).modal('hide');
        $("#submit_edit").show();
        $("#submit_add").show();
        $('#wrong').addClass('hide');
        location.reload();
    });
    $(".manage_Delete").click(function () {
        var uid = $(this).attr('uid');
        //alert(uid);
        var accept = confirm('ยืนยันการลบข้อมูล');
        if (accept) {
            $.ajax({
                url: "/staffs/delete",
                type: "get",
                dataType: 'json',
                data: {uid: uid}
            }).success(function (res) {
                //var res = JSON.parse(response);
                //if(!res.success) {
                alert(res.desc);
                // }else {
                console.log(res);
                location.reload();
                //}
            });
        }
    });
});
