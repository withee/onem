/**
 * Created by mrsyrop on 23/5/2558.
 */
$(document).ready(function () {
    //$('#findroute').click(function () {
    //    var data = $('#trackingform').serializeArray();
    //    console.log(data);
    //    $.get( "/maps", data);
    //});
    $(function () {
        $("#current-date").datepicker({
            dateFormat: "yy-mm-dd"
        });
    });

    $.ajax({
        url: "/speedchart",
        type: "get",
        dataType: 'json',
        data: {
            'current-date': $("#current-date").val(),
            'vehicle': $('#vehicle-list').val()
        }
    }).
        success(function (result) {
            console.log(result);
            var labels = [], data = [];
            $('#timelist').empty();
            for (var i = 0; i < result.length; i++) {
                labels.push(result[i][0]);
                data.push(result[i][1]);
                console.log(result[i][0] + ":" + result[i][1]);
                var element = "<input type='button' class='at_hour' value=" + result[i][0] + ">";
                $('#timelist').append(element);
            }


            var speedData = {
                labels: labels,
                datasets: [
                    {
                        label: "ความเร็วเฉเลี่ยทุก 10 นาที",
                        fillColor: "rgba(240, 127, 110, 0.3)",
                        strokeColor: "#f56954",
                        pointColor: "#A62121",
                        pointStrokeColor: "#741F1F",
                        data: data
                    }
                ]
            };
            var speed = document.getElementById('chart_div').getContext('2d');
            new Chart(speed).Line(speedData, {
                bezierCurve: false
            });
        });

    $(document).on('click', "input.at_hour", function () {
        var cdate = $("#current-date").val();
        var chour = $(this).val();
        var vehicle = $("#vehicle-list").val();
        $.ajax({
            url: "/speedcharthour",
            type: "get",
            dataType: 'json',
            data: {
                'current-date': cdate,
                'vehicle': vehicle,
                'current-hour': chour
            }
        }).
            success(function (result) {
                $('#chart').empty();
                $('#chart').append("<canvas id=\"chart_div\" style=\"width: 100%;height: 500px\"></canvas>");
                console.log(result);
                var labels = [], data = [];
                for (var i = 0; i < result.length; i++) {
                    labels.push(result[i][0]);
                    data.push(result[i][1]);
                    console.log(result[i][0] + ":" + result[i][1]);
                }


                var speedData = {
                    labels: labels,
                    datasets: [
                        {
                            label: "ความเร็วเฉเลี่ยทุก 10 นาที",
                            fillColor: "rgba(240, 127, 110, 0.3)",
                            strokeColor: "#f56954",
                            pointColor: "#A62121",
                            pointStrokeColor: "#741F1F",
                            data: data
                        }
                    ]
                };
                var speed = document.getElementById('chart_div').getContext('2d');
                new Chart(speed).Line(speedData, {
                    bezierCurve: false
                });
            });
    });

})
;