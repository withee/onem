/**
 * Created by mrsyrop on 14/6/2558.
 */
$(document).ready(function () {
    var Products = [];
    var current_id = 0;
    $(".pr_view").click(function () {
        var id = $(this).attr('pr_id');
        $("#prview_modal").modal('show');
        $.ajax({
            url: "/purchases/prproduct",
            type: "post",
            dataType: 'json',
            data: {id: id}
        }).success(function (res) {
            //var res = JSON.parse(response);
            //alert(res.desc);
            //console.log(res);
            $("#pr_view_table").empty();
            if (res.success) {
                $.each(res.data, function (key, product) {

                    if (key != '__proto__') {
                        var el = "<tr>";
                        el += "<td>" + (key + 1) + "</td>";
                        el += "<td>" + product.product_name + "</td>";
                        el += "<td>" + product.quantity + " " + product.unit_name + "</td>";
                        el += "<td></td>";
                        el += "</tr>";
                        $("#pr_view_table").append(el);
                    }
                })
            } else {

            }
        });
    });
    $(".pr_edit").click(function () {
        var id = $(this).attr('pr_id');
        $("#prview_modal").modal('show');
        $.ajax({
            url: "/purchases/prproduct",
            type: "post",
            dataType: 'json',
            data: {id: id}
        }).success(function (res) {
            //var res = JSON.parse(response);
            //alert(res.desc);
            //console.log(res);
            $("#pr_view_table").empty();
            if (res.success) {
                $.each(res.data, function (key, product) {

                    if (key != '__proto__') {
                        var el = "<tr>";
                        el += "<td>" + (key + 1) + "</td>";
                        el += "<td>" + product.product_name + "</td>";
                        el += "<td>" + product.quantity + " " + product.unit_name + "</td>";
                        el += "<td><input type='button' class='btn btn-danger pr_product_rm' value='ลบ' prid="+id+" pid=" + product.id + "></td>";
                        el += "</tr>";
                        $("#pr_view_table").append(el);
                    }
                })
            } else {

            }
        });
    });
    $(document).on('click', "input.pr_product_rm", function () {
        var pid = $(this).attr('pid');
        var prid = $(this).attr('prid');
        var agree = confirm('ยืนยันการลบสินค้าจากคำสั่งซื้อ');
        if (agree) {
            $.ajax({
                url: "/purchases/delprlist",
                type: "post",
                dataType: 'json',
                data: {id: pid}
            }).success(function (res) {
                //var res = JSON.parse(response);
                //alert(res.desc);
                //console.log(res);
                alert(res.desc);
                $.ajax({
                    url: "/purchases/prproduct",
                    type: "post",
                    dataType: 'json',
                    data: {id: prid}
                }).success(function (res) {
                    //var res = JSON.parse(response);
                    //alert(res.desc);
                    //console.log(res);
                    $("#pr_view_table").empty();
                    if (res.success) {
                        $.each(res.data, function (key, product) {

                            if (key != '__proto__') {
                                var el = "<tr>";
                                el += "<td>" + (key + 1) + "</td>";
                                el += "<td>" + product.product_name + "</td>";
                                el += "<td>" + product.quantity + " " + product.unit_name + "</td>";
                                el += "<td><input type='button' class='btn btn-danger pr_product_rm' value='ลบ' pid=" + product.id + "></td>";
                                el += "</tr>";
                                $("#pr_view_table").append(el);
                            }
                        })
                    } else {

                    }
                });
            });
        }

    });

    $(".pr_approve").click(function () {
        var id = $(this).attr('pr_id');
        var agree = confirm('ยืนยันการรับทราบคำสั่งซื้อ');
        if (agree) {
            $.ajax({
                url: "/purchases/acceptpr",
                type: "post",
                dataType: 'json',
                data: {id: id}
            }).success(function (res) {
                //var res = JSON.parse(response);
                //alert(res.desc);
                //console.log(res);
                alert(res.desc);
                location.reload();
            });
        }
    });

    $(".pr_rm").click(function () {
        var id = $(this).attr('pr_id');
        var agree = confirm('ยืนยันการลบคำสั่งซื้อ');
        if (agree) {
            $.ajax({
                url: "/purchases/delpr",
                type: "post",
                dataType: 'json',
                data: {id: id}
            }).success(function (res) {
                //var res = JSON.parse(response);
                //alert(res.desc);
                //console.log(res);
                alert(res.desc);
                location.reload();
            });
        }
    });

    $(".pr_add_product").click(function () {
        var id = $(this).attr('pr_id');
        current_id = id;
        $("#pr_add_modal").modal('show');
        if (Products.length == 0) {
            $.ajax({
                url: "/purchases/getproduct",
                type: "post",
                dataType: 'json',
                data: {id: id}
            }).success(function (res) {
                if (res.success) {
                    Products = res.data;
                    createProductlist();
                }
            });
        } else {
            createProductlist();
        }
    });

    $("#add_new_pr").click(function () {
        var pid = $("#pr_product_list").val();
        var quantity = $("#add_quantity").val();
        $.ajax({
            url: "/purchases/producttopr",
            type: "post",
            dataType: 'json',
            data: {id: current_id, product: pid, quantity: quantity}
        }).success(function (res) {
            alert(res.desc);
        });

    });

    $("#add_pr_btn").click(function () {
        $("#pr_purpose").val('');
        $("#add_pr_modal").modal('show');
    });

    $("#add_pr").click(function () {
        var purpose = $("#pr_purpose").val();
        $.ajax({
            url: "/purchases/addpr",
            type: "post",
            dataType: 'json',
            data: {purpose: purpose}
        }).success(function (res) {
            alert(res.desc);
            $("#add_pr_modal").modal('show');
            location.reload();
        });

    });


    function createProductlist() {
        $("#pr_product_list").empty();
        $.each(Products, function (key, val) {
            var el = " <option value=" + val.product_id + ">" + val.product_name +" ("+val.unit_name+") "+ "</option>";
            $("#pr_product_list").append(el);
        });
    }
});