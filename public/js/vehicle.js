/**
 * Created by mrsyrop on 26/5/2558.
 */
$(document).ready(function () {
    $('#addVehicle').click(function () {
        $('#vehicle-modal').modal('show');
        $("#submit_vehicle_edit").hide();
    });
    $("#submit_vehicle_cancel").click(function () {
        var parent = $(this).attr('parent');
        //alert(parent);
        $("#" + parent).modal('hide');
        $("#submit_vehicle_edit").show();
        $("#submit_vehicle_add").show();
        $('#wrong').addClass('hide');
        location.reload();
    });

    $("#submit_vehicle_add").click(function () {
        var data = $('#add-form').serializeArray();
        $.ajax({
            url: "/assets/addvehicle",
            type: "post",
            dataType: 'json',
            data: data
        }).success(function (res) {
            alert(res.desc);
            console.log(res);
            if (!res.success) {
                $('#error-report').empty();
                $.each(res.data, function (key, value) {
                    if (key != '__proto__') {
                        //alert(value[0]);
                        $('#error-report').append("<label>" + value[0] + "</label>");
                    }
                });
                $('#wrong').removeClass('hide');
            } else {
                $('#wrong').addClass('hide');
            }
        });
    });

    $('.edit_vehicle').click(function () {
        var uid = $(this).attr('uid');
        $("#submit_vehicle_add").hide();
        $("#submit_vehicle_edit").show();
        $.ajax({
            url: "/assets/getvehicle",
            type: "get",
            dataType: 'json',
            data: {id: uid}
        }).success(function (res) {
            console.log(res);
            $('#vehicle-modal').modal('show');
            $('#vehicle_id').val(res.data.id);
            $('#brands').val(res.data.brands);
            $('#series').val(res.data.series);
            $('#color').val(res.data.color);
            $('#license_plate').val(res.data.license_plate);
            $('#engine_number').val(res.data.serial);
            $('#represent_color').val(res.data.represent_color);
        });
    });

    $("#submit_vehicle_edit").click(function () {
        var data = $('#add-form').serializeArray();
        $.ajax({
            url: "/assets/editvehicle",
            type: "post",
            dataType: 'json',
            data: data
        }).success(function (res) {
            alert(res.desc);
            console.log(res);
            if (!res.success) {
                $('#error-report').empty();
                $.each(res.data, function (key, value) {
                    if (key != '__proto__') {
                        //alert(value[0]);
                        $('#error-report').append("<label>" + value[0] + "</label>");
                    }
                });
                $('#wrong').removeClass('hide');
            } else {
                $('#wrong').addClass('hide');
            }
        });
    });

    $('.remove_vehicle').click(function () {
        var uid = $(this).attr('uid');
        var agree = confirm('ยืนยันการลบข้อมูล');
        if (agree) {
            $.ajax({
                url: "/assets/deletevehicle",
                type: "get",
                dataType: 'json',
                data: {id: uid}
            }).success(function (res) {
               alert(res.desc);
                location.reload();
            });
        }

    });


        $('#represent_color').colpick({
            layout:'hex',
            submit:0,
            colorScheme:'dark',
            onChange:function(hsb,hex,rgb,el,bySetColor) {
                $(el).css('border-color','#'+hex);

                // Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
                if(!bySetColor) $(el).val(hex);
            }
        }).keyup(function(){
            $(this).colpickSetColor(this.value);
        });


});
