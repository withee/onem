$(document).ready(function () {
    $("#check-login").click(function () {
        var data = $('form').serializeArray();
        console.log(data);
        $.ajax({
            url: "/staffs/login",
            type: "post",
            dataType: 'json',
            data: data
        }).success(function (res) {
            $('#error-report').removeClass('hide');
            $('#error-report').empty();
            if (!res.succes && res.desc == "validate failed.") {
                $.each(res.data, function (key, value) {
                    $('#error-report').append("<label>" + value[0] + "</label><br>");
                });
            } else if (!res.success) {
                $('#error-report').append("<label>" + res.desc + "</label>");
            }else{
                window.location="/products/manage";
            }
            console.log(res);
        });
    });
});
