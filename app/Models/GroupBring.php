<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 8/5/2558
 * Time: 17:04 น.
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class GroupBring extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'group_bring';
    protected $primaryKey = 'group_bring_id';

    public function deleteControlGroupBuild($id){
        //1. Variable declaration.
        $result = array();
        $result['status']='';
        $result['desc']='';
        //2. find ById.
//        $controlBuild = self::find($id);
        $controlGroupBring = GroupBring::find($id);
        if(false==empty($controlGroupBring)) {
            $controlGroupBring->delete();
        }else{
            $result['status']='error';
            $result['desc']='ไม่สามารถ ปิดกลุ่มสินค้านี้ ได้';
        }

        //3. Return $result to controller.
        return $result;
    }

    public function getMaxRowGroupBring($search){
        //get max row.
        if(''==$search){
            $count = DB::select(DB::raw("select count(*) from group_bring WHERE deleted_at is NULL"));
        }else {
            $count = DB::select(DB::raw("select count(*) from group_bring WHERE deleted_at is NULL and group_bring_name LIKE '%" . $search . "%'"));
        }
        //Return
        return intval($count[0]->{'count(*)'});
    }

    public function findAllGroupBringAndPaginate($countRow,$page,$search){
        //Variable declaration.
        $bring=new Bring();
        $results=array();
        $offset=intval($page-1)*$countRow;
        $results['page']=$page;
        $results['search']=$search;
        try {
            //get max row
            $results['maxProduct']=ceil(intval(self::getMaxRowGroupBring($search))/$countRow);
            //select all Product
            if(''==$search) {
                $results['dataProduct'] = DB::select(DB::raw("
                SELECT group_bring_id,group_bring_name,deleted_at,updated_at
                FROM group_bring
                WHERE deleted_at is NULL
                LIMIT :limit OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
            }else{
                $results['dataProduct'] = DB::select(DB::raw("
                SELECT group_bring_id,group_bring_name,deleted_at,updated_at
                FROM group_bring
                WHERE deleted_at is NULL AND group_bring_name LIKE '%" . $search . "%'
                LIMIT :limit OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
            }


            foreach($results['dataProduct'] as $key=>$value){
//                var_dump($value->updated_at);
                $value->deleted_at=$bring->findByGroupBring($value->group_bring_id);
            }

        } catch (Exception $e) {
            echo $e->getLine() . ':' . $e->getMessage();
        }
        //return
        return $results;
    }

}