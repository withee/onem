<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 8/5/2558
 * Time: 17:04 น.
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Database\Eloquent\SoftDeletes;
use League\Flysystem\Exception;
use Carbon\Carbon;

class Staffs extends Model
{
    protected $table = 'staffs';
    protected $primaryKey = 'staff_id';
    protected $dates = ['deleted_at'];
    use SoftDeletes;

    public static function initID()
    {
        //1. Variable declaration
        $prefix = array('1M', '1M0', '1M00', '1M000');
        $generator = new IDgenerator();

        try {
            //2. Get id from generator
            $record = $generator->nextID();
            $i = 0;
            if ($record < 10) {
                $i = 3;
            } else if ($record < 100) {
                $i = 2;
            } else if ($record < 1000) {
                $i = 1;
            }
        } catch (Exception $e) {
            echo $e->getLine() . ':' . $e->getMessage();
        }
        //3. Return current id
        return $prefix[$i] . ($record + 1);
    }

    public function getActivityByProjectId($project_id,$month=null,$year=null){

        if(true==empty($month)){
            $datetime =Carbon::now();
            $month=$datetime->format('m');
        }

        if(true==empty($year)){
            $datetime =Carbon::now();
            $year=$datetime->format('Y');
        }

        $maxDay=$datetime->format('t');
        $datetime->setdate($year,$month,1);
        $start_date=$datetime->format("Y-m-d 00:00:00");
        $datetime->setdate($year,$month,$maxDay);
        $end_date=$datetime->format("Y-m-d 23:59:59");

        $ProjectSub=new ProjectSub();
        $results=array();
        $dataList=DB::select(DB::raw("
            select staffs.staff_id,projects_activity_log_detail.start_at,projects_activity_log_detail.end_at,projects_sub.project_sub_name,projects_sub.project_position,DAY(projects_activity_log.projects_activity_start_date) As ListDay,projects_activity.projects_activity_name,projects_activity_log_detail.projects_activity_log_detail_value
            from staffs
            INNER JOIN projects_activity_log_detail on (projects_activity_log_detail.id_staff_works=staffs.staff_id)
            INNER JOIN projects_activity_log on (projects_activity_log.projects_activity_log_id=projects_activity_log_detail.projects_activity_log_id)
            INNER JOIN projects_activity on (projects_activity.projects_activity_id=projects_activity_log_detail.projects_activity_id)
            INNER JOIN projects_sub on (projects_sub.project_sub_id=projects_activity_log.project_sub_id)
            where staffs.staff_usertype=4 and projects_sub.project_id=:project_id and projects_activity_log.projects_activity_start_date>='".$start_date."' and projects_activity_log.projects_activity_start_date<='".$end_date."'"),array('project_id' => $project_id));

        foreach($dataList as $key=>$value){
            if(false==empty($results[$value->ListDay][$value->staff_id])){
                $results[$value->ListDay][$value->staff_id]['value']+=$value->projects_activity_log_detail_value;
                $results[$value->ListDay][$value->staff_id]['text'].='<br>'.$ProjectSub->SetKmPlus($value->project_sub_name,$value->start_at,$value->end_at).'('.str_replace('ight','',str_replace('eft','',$value->project_position)).') จำนวน '.$value->projects_activity_log_detail_value.' เมตร';
            }else{
                $results[$value->ListDay][$value->staff_id]['value']=$value->projects_activity_log_detail_value;
                $results[$value->ListDay][$value->staff_id]['text']='สถานที่ <br>';
                $results[$value->ListDay][$value->staff_id]['text'].=$ProjectSub->SetKmPlus($value->project_sub_name,$value->start_at,$value->end_at).'('.str_replace('ight','',str_replace('eft','',$value->project_position)).') จำนวน '.$value->projects_activity_log_detail_value.' เมตร';
            }
        }
        return $results;
    }

    public function verifiedByUser($username, $userpwd)
    {

        //1. Variable declaration.
        $result = array('success' => true, 'desc' => 'Operation Success!!', 'data' => array());

        //2. Login process.
        try {

            $staff = Staffs::where('staff_username', '=', $username)->first();
            if (false == empty($staff)) {
                //3. Compare password
                if ($staff->staff_userpwd == md5($userpwd)) {
                    $result['data'] = $staff;
                    //4. Create session
                    Session::put('user', $staff);
                } else {
                    $result['success'] = false;
                    $result['desc'] = 'Password mismatch.';
                }
            } else {
                $result['success'] = false;
                $result['desc'] = 'User not found.';
            }
        } catch (Exception $e) {
            $result['success'] = false;
            $result['desc'] = $e->getLine() . ":" . $e->getMessage();
        } finally {

        }

        //3. Return $result to controller.
        return $result;
    }

    public static function getNameLastName($id){
        $dataStaff=Staffs::find($id);
        if(!empty($dataStaff)) {
            return $dataStaff->staff_firstname . ' ' . $dataStaff->staff_lastname;
        }else{
            return '-';
        }
    }

    public function deleteStaff($id)
    {
        //1. Variable declaration
        $success = 0;
        try {
            //2. Find staff and delete
            $staff = Staffs::where('staff_id', '=', $id)->firstOrFail();
            $success = $staff->delete();
        } catch (Exception $e) {
            echo $e->getLine() . ':' . $e->getMessage();
        } finally {

        }
        //3. Return
        return $success;
    }
}