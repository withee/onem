<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 8/5/2558
 * Time: 17:04 น.
 */
namespace App\Models;
use Session;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectSub extends Model{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'projects_sub';
    protected $primaryKey = 'project_sub_id';

    public function SetKmPlus($string,$m1,$m2){
        $data=explode("-",$string);
        $data1=explode("+",$data[0]);
        return $data1[0]."+".$m1." - ".$data1[0].'+'.$m2;
    }

    public function findBySubName($project_sub_name){
        $projectActiveLog=new ProjectsActivityLog();
        $list=DB::select(DB::raw("select projects_sub.project_sub_id,projects_sub.project_position,projects_sub.project_sub_name,projects_sub.project_sub_id,projects_sub.created_at
                from projects_sub
                where projects_sub.project_sub_name='".$project_sub_name."'
                order by project_sub_id asc"));
        foreach($list as $key=>$value){
            $value->created_at=$projectActiveLog->getActivityBySubProject($value->project_sub_id);
        }
        return $list;
    }

    public function findAllProjectSubByKm($id){
        return $results['dataProduct'] = DB::select(DB::raw("select project_sub_id,project_sub_name,amount_POR as amount_PORL,sum(amount_POR)-amount_POR as amount_PORR,sum(amount_POR) as SumPor,amount_survey as amount_surveyL,sum(amount_survey)-amount_survey as amount_surveyR,sum(amount_survey) as SumSurvey,updated_at
                from (SELECT * FROM projects_sub order by updated_at) projects_sub
                where project_id=:id
                group by project_sub_name
                #order by project_sub_name asc,updated_at DESC"), array('id' => $id));
//        return $results['dataProduct'] = DB::select(DB::raw("select * from projects_sub where projects_sub.project_id=:id order By projects_sub.project_sub_name ASC,projects_sub.project_position ASC"), array('id' => $id));
    }

    public function getMaxRowProjectSub($id,$search){
        //get max row.
        if(''==$search){
            $count = DB::select(DB::raw("select count(*) from projects_sub WHERE project_id=".$id." and deleted_at is NULL"));
        }else {
            $count = DB::select(DB::raw("select count(*) from projects_sub WHERE project_id=".$id." and deleted_at is NULL and project_sub_name LIKE '%" . $search . "%'"));
        }
        //Return
        return intval($count[0]->{'count(*)'});
    }

    public function findProjectSubByProjectIdAndProjectSubName($id,$projectSubName)
    {
        $results=array();
        $bringDetail=new BringDetail();
        $projectsActivity=new ProjectsActivity();
        $results=DB::select(DB::raw("
                SELECT projects_sub.project_position,projects_sub.project_id,projects_sub.project_sub_id,staffs.staff_firstname,staffs.staff_lastname,projects_sub.project_sub_name,projects_sub.deleted_at,projects_sub.updated_at
                FROM projects_sub
                LEFT JOIN staffs on (staffs.staff_id=projects_sub.id_staff_create)
                WHERE projects_sub.project_id=:id and projects_sub.project_sub_name=:SubName "),array('id' => $id,'SubName'=>$projectSubName));

        foreach($results as $key=>$value){
            $value->updated_at=$projectsActivity->findByProjectIdAndSubProjectId($value->project_sub_id);
            $value->deleted_at=$bringDetail->findProductList($value->project_sub_id);
        }
        return $results;
    }

    public function findAllProjectSubAndPaginate($id,$countRow,$page,$search){
        //Variable declaration.
        $bringDetail=new BringDetail();
        $projectsActivity=new ProjectsActivity();
        $results=array();
        $offset=intval($page-1)*$countRow;
        $results['page']=$page;
        $results['search']=$search;
        try {
            //get max row
            $results['maxProduct']=ceil(intval(self::getMaxRowProjectSub($id,$search))/$countRow);
            //select all Product
            if(''==$search) {
                $results['dataProduct'] = DB::select(DB::raw("
                SELECT projects_sub.project_id,projects_sub.project_sub_id,staffs.staff_firstname,staffs.staff_lastname,projects_sub.project_sub_name,projects_sub.deleted_at,projects_sub.updated_at
                FROM projects_sub
                LEFT JOIN staffs on (staffs.staff_id=projects_sub.id_staff_create)
                WHERE projects_sub.project_id=".$id." and projects_sub.deleted_at is NULL
                LIMIT :limit OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
            }else{
                $results['dataProduct'] = DB::select(DB::raw("
                SELECT projects_sub.project_sub_id,staffs.staff_firstname,staffs.staff_lastname,projects_sub.project_sub_name,projects_sub.deleted_at,projects_sub.updated_at
                FROM projects_sub
                LEFT JOIN staffs on (staffs.staff_id=projects_sub.id_staff_create)
                WHERE projects_sub.project_id=".$id." and projects_sub.deleted_at is NULL AND projects_sub.project_sub_name LIKE '%" . $search . "%'
                LIMIT :limit OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
            }

            foreach($results['dataProduct'] as $key=>$value){
                $value->updated_at=$projectsActivity->findByProjectIdAndSubProjectId($value->project_sub_id);
                $value->deleted_at=$bringDetail->findProductList($value->project_sub_id);
            }
        } catch (Exception $e) {
            echo $e->getLine() . ':' . $e->getMessage();
        }

        return $results;
    }

    public function autoCreated($project_id,$km,$max){

        $staff = Session::get('user');
        $spl = new ProjectSub();
        $spl->project_id=$project_id;
        $spl->project_sub_name=$km;
        $spl->project_position='left';
        $spl->start_at=0;
        $spl->end_at=1000-$max;
        $spl->id_staff_create=$staff->staff_id;
        $spl->save();
        $spr = new ProjectSub();
        $spr->project_id=$project_id;
        $spr->project_sub_name=$km;
        $spr->project_position='right';
        $spr->start_at=0;
        $spr->end_at=1000-$max;
        $spr->id_staff_create=$staff->staff_id;
        $spr->save();
    }
}