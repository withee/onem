<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeesDaily extends Model {

    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'employees_daily';
    protected $primaryKey = 'employees_daily_id';

    public function checkNumberOfDays($Year,$Month){
        $date =Carbon::now();
        $date->setDate($Year,$Month,1);
        return intval(date('t', strtotime($date->format('Y-m'))));
    }

    public function GetMonthSet($selectMonthSearch,$selectYearSearch){
        $datetimenow =Carbon::now();
        $datetimenow->setDate($selectYearSearch,$selectMonthSearch,1);
        $CountDayMonthNow=self::checkNumberOfDays(intval($datetimenow->format('Y')),intval($datetimenow->format('m')))-1;
        $dataReturn=array();
        $datetime =Carbon::now();
        $datetime->setDate(intval($datetimenow->format('Y')),intval($datetimenow->format('m')),1);
        $dataReturn['start']=$datetime->format('Y-m-d 00:00:00');
        $datetime->modify('+'.$CountDayMonthNow.' day');
        $dataReturn['end']=$datetime->format('Y-m-d 23:59:59');
        return $dataReturn;
    }

    public function GetCalendar($Month,$Year){
        $dataReturn=array();
        $day=0;
        $dataDate=self::GetMonthSet($Month,$Year);
        $string='select * from employees_daily where deleted_at IS NULL and employees_daily.employees_daily_work>="'.$dataDate['start'].'" and employees_daily.employees_daily_work<="'.$dataDate['end'].'" order by employees_daily.employees_daily_work asc';
        $results = DB::select(DB::raw($string));
        $list=array();
        foreach($results as $key=>$value){
            $datetimenow =new Carbon($value->employees_daily_work);
            if($key==0){
                $day=$datetimenow->format('j');
            }
            if($day!=$datetimenow->format('j')){
                if(!empty($list)) {
                    $dataReturn[$day] = $list;
                }
                $list=array();
                $value->employees_daily_work=$datetimenow->format('H:i:s');
                array_push($list,$value);
                $day=$datetimenow->format('j');
            }else{
                $value->employees_daily_work=$datetimenow->format('H:i:s');
                array_push($list,$value);
            }
            if(count($results)==$key+1){
                if(!empty($list)) {
                    $dataReturn[$day] = $list;
                }
            }
        }
        return $dataReturn;
    }
}
