<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class PrOrderDetail extends Model {
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'pr_order_detail';
    protected $primaryKey = 'PR_order_detail_id';
}
