<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 8/5/2558
 * Time: 16:25 น.
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    protected $table = 'ref_staff_role';

    public function getWithKey()
    {
        $type = UserRole::all();
        foreach ($type as $l) {
            $usertype[$l->role_id] = $l;
        }
        return $usertype;
    }
}