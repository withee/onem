<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 8/5/2558
 * Time: 17:04 น.
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class BringDetail extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'brings_detail';
    protected $primaryKey = 'bring_detail_id';

    public function findByBringIdReturnToArray($id){
        $dataReturn=array();
        $results = array();
        $results = DB::select(DB::raw("
              select brings_detail.product_id,products.product_name,sum(brings_detail.bring_detail_count)as sum
              from brings_detail
              LEFT JOIN products on (products.product_id=brings_detail.product_id)
              where brings_detail.bring_id=:id group by brings_detail.product_id"),
            array('id' => $id));

        foreach($results as $key=>$value){
              $dataReturn[$value->product_id]['sum']=$value->sum;
        }
        return $dataReturn;
    }

    public function findById($id)
    {
        $results = array();
        $results = DB::select(DB::raw("
            SELECT brings_detail.bring_type,brings_detail.product_id,brings_detail.bring_detail_id,products.product_name,ref_store_name.storename_name,brings_detail.bring_detail_count,brings_detail.bring_detail_price,brings_detail.bring_detail_location
            FROM brings_detail
            INNER JOIN products on (products.product_id=brings_detail.product_id)
            INNER JOIN ref_store_name on (ref_store_name.storename_id=brings_detail.storename_id)
            WHERE bring_id=:id"),
            array('id' => $id));


        return $results;
    }

    public function findProductList($id)
    {
        $results = array();
        $results = DB::select(DB::raw("
            select products.product_name,ref_product_units.unit_name,sum(brings_detail.bring_detail_count) as count
            from brings
            INNER JOIN brings_detail on (brings_detail.bring_id=brings.bring_id)
            INNER JOIN products on (products.product_id=brings_detail.product_id)
            INNER JOIN ref_product_units on (ref_product_units.unit_id=products.product_unit)
            where brings.project_sub_id=:id group by products.product_name"),
            array('id' => $id));
        return $results;
    }

    public function findProductByBringId($id)
    {
        $results = array();
        $results = DB::select(DB::raw("
            SELECT products.product_id,products.product_name
            FROM brings_detail
            INNER JOIN products on (products.product_id=brings_detail.product_id)
            WHERE bring_id=:id"),
            array('id' => $id));
        return $results;
    }
}