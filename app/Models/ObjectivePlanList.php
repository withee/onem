<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ObjectivePlanList extends Model {

    protected $table = 'objective_plan_lists';
    use SoftDeletes;

}
