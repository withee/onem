<?php namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class BEConverter extends Model
{

    public static function ADtoBE($datestr = null)
    {
        $bemonth = array('มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม');
        $result = array('day' => '', 'month' => '', 'year' => '');
        $now = Carbon::now();
        if (!empty($datestr)) {
            $now = Carbon::createFromFormat('Y-m-d', $datestr);
        }
        $result['day'] = $now->day;
        $result['month'] = $bemonth[$now->month - 1];
        $result['year'] = $now->year + 543;
        $result['date']=$result['day'].'-'.$result['month'].'-'.$result['year'];
        return $result['day'].' '.$result['month'].' '.$result['year'];
    }

}
