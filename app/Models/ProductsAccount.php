<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductsAccount extends Model {

    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'products_accounts';
    protected $primaryKey = 'id';

    public function formatDate($date, $format) {
        $date = date_create($date);
        $date2 = date_format($date, $format);
        return $date2;
    }

    public function checkNumberOfDays($Year,$Month){
        $date =Carbon::now();
        $date->setDate($Year,$Month,1);
        return intval(date('t', strtotime($date->format('Y-m'))));
    }

    public function GetMonthSet($selectYearSearch,$selectMonthSearch){
        $datetimenow =Carbon::now();
//        var_dump($selectMonthSearch);
        if(!empty($selectMonthSearch)) {
            $datetimenow->setDate($selectYearSearch, $selectMonthSearch, 1);
            $CountDayMonthNow = self::checkNumberOfDays(intval($datetimenow->format('Y')), intval($datetimenow->format('m'))) - 1;
        }else{
            $datetimenow->setDate($selectYearSearch, 1, 1);
        }
        $dataReturn=array();
        $datetime =Carbon::now();
        $datetime->setDate(intval($datetimenow->format('Y')),intval($datetimenow->format('m')),1);
        $dataReturn['start']=$datetime->format('Y-m-d 00:00:00');
            if(!empty($selectMonthSearch)) {
                $datetime->modify('+' . $CountDayMonthNow . ' day');
            }else{
                $datetime->modify('+1 years');
            }
        $dataReturn['end']=$datetime->format('Y-m-d 23:59:59');
        return $dataReturn;
    }

    public function getByMonthAndYear($Year,$Month=null,$Product=null,$project_id=null){
        $results=array();
        $dataDate=self::GetMonthSet($Year,$Month);
        $string='select * from products_accounts where products_accounts.created_at>="'.$dataDate['start'].'" and products_accounts.created_at<="'.$dataDate['end'].'"';
        if(!empty($Product)) {
            $string .= ' and product_id=' . $Product;
        }
        if(!empty($project_id)) {
            $string .= ' and project_id=' . $project_id;
        }
        $string.=' order by products_accounts.updated_at DESC';
//        echo $string;
//        ecit;

        $results = DB::select(DB::raw($string));
        return $results;
    }

    public function getByProjectId($id){
        $string='select * from products_accounts where deleted_at IS NULL and project_id="'.$id.'" order by products_accounts.created_at DESC';
        $results = DB::select(DB::raw($string));
        return $results;
    }

    public static function checkCostsProductId($id){
//        return $id;
        $count = DB::select(DB::raw("select (sum(price)/sum(income)) as costs
                from products_accounts
                where products_accounts.product_id='".$id."' and deleted_at IS NULL and income >0 and price >0"));
        //Return
        return $count[0]->{'costs'};;
    }
}
