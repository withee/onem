<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ObjectivePlanPeriod extends Model {

    protected $table = 'objective_plan_periods';
    use SoftDeletes;

}
