<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 8/5/2558
 * Time: 17:04 น.
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectsActivity extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'projects_activity';
    protected $primaryKey = 'projects_activity_id';

    public function findByProjectIdAndProjectSubName($id,$projectSubName)
    {
        $results=array();
        $results = DB::select(DB::raw("
            SELECT projects_activity.projects_activity_id,staffs.staff_firstname,staffs.staff_lastname,projects_activity.projects_activity_name,projects_activity.projects_activity_value,ref_projects_activity_units.ref_projects_activity_units_name
            FROM projects_activity
            INNER JOIN staffs on (staffs.staff_id=projects_activity.id_staff_record)
            INNER JOIN ref_projects_activity_units on (ref_projects_activity_units.ref_projects_activity_units_id=projects_activity.projects_activity_unit)
            WHERE project_id=:id and project_sub_name=:SubName"),
            array('id' => $id,'SubName'=>$projectSubName));
        return $results;
    }

    public function findByProjectId($id)
    {
        $results=array();
        $results = DB::select(DB::raw("
            SELECT projects_activity.projects_activity_id,staffs.staff_firstname,staffs.staff_lastname,projects_activity.projects_activity_name,projects_activity.projects_activity_value,ref_projects_activity_units.ref_projects_activity_units_name
            FROM projects_activity
            INNER JOIN staffs on (staffs.staff_id=projects_activity.id_staff_record)
            INNER JOIN ref_projects_activity_units on (ref_projects_activity_units.ref_projects_activity_units_id=projects_activity.projects_activity_unit)
            WHERE project_id=:id"),
            array('id' => $id));
        return $results;
    }


    public function findByProjectIdAndSubProjectId($id)
    {
        $results=array();
        $results = DB::select(DB::raw("
            select ref_projects_activity_units.ref_projects_activity_units_name,projects_activity.projects_activity_name,SUM(projects_activity_log_detail.projects_activity_log_detail_value) as sum
            from projects_activity_log
            INNER JOIN projects_activity_log_detail on (projects_activity_log_detail.projects_activity_log_id=projects_activity_log.projects_activity_log_id)
            INNER JOIN projects_activity on (projects_activity.projects_activity_id=projects_activity_log_detail.projects_activity_id)
            INNER JOIN ref_projects_activity_units on (ref_projects_activity_units.ref_projects_activity_units_id=projects_activity.projects_activity_unit)
            where projects_activity_log.project_sub_id=:id group by projects_activity_log_detail.projects_activity_id"),
            array('id' => $id));
        return $results;
    }

}