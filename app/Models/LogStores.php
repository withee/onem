<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 8/5/2558
 * Time: 17:04 น.
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Session;

class LogStores extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'log_stores';
    protected $primaryKey = 'log_stores_id';

    public function getAllData(){
        $results=array();
        $results = DB::select(DB::raw("
            SELECT log_stores.created_at,staffs.staff_firstname,staffs.staff_lastname,log_stores.log_stores_count,log_stores.log_stores_type,log_stores.log_return_detail,products.product_name,ref_store_name.storename_name
            FROM log_stores
            INNER JOIN staffs on (staffs.staff_id=log_stores.id_member)
            INNER JOIN products on (products.product_id=log_stores.product_id)
            INNER JOIN ref_store_name on (ref_store_name.storename_id=log_stores.storename_id)"));
        return $results;
    }

}