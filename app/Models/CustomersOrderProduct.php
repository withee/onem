<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomersOrderProduct extends Model {
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'customers_order_product';
    protected $primaryKey = 'customers_order_product_id';

    public static function getCountProductPlan($name){
        $count = DB::select(DB::raw("select sum(customers_order_transport_product.customers_order_product_count) as total ,sum(customers_order_transport_product.customers_order_transport_product_complete) as complete
from customers_order_product
inner join customers_order_transport_product on customers_order_product.customers_order_product_id=customers_order_transport_product.customers_order_product_id
where customers_order_product.customers_order_product_name='".$name."'"));
        if((!empty($count[0]->total) or $count[0]->total>=0) and (!empty($count[0]->complete or $count[0]->complete>=0))) {
            return intval($count[0]->total) - intval($count[0]->complete);
        }else{
            return 0;
        }
    }

    public static function getNameProduct($id){
        $dataCustomersOrderProduct=CustomersOrderProduct::find($id);
        if(!empty($dataCustomersOrderProduct)){
            return $dataCustomersOrderProduct->customers_order_product_name;
        }
    }

    public function getMaxRowCustomersOrderProduct($search){
        //1. Variable declaration.
        $count=array();
        //2. get max row.
        if(''==$search){
            $count = DB::select(DB::raw("select count(*) from customers_order_product where customers_order_product.deleted_at is NULL"));
        }else {
            $count = DB::select(DB::raw("select count(*) from customers_order_product WHERE customers_order_product.deleted_at is NULL and customers_order_product.customers_order_product_name LIKE '%" . $search . "%'"));
        }
        //3. Return
        return intval($count[0]->{'count(*)'});
    }

    public function findAllCustomersAndPaginate($countRow,$page,$orderId,$search){
        $results=array();
        $offset=intval($page-1)*$countRow;
        $results['page']=$page;
        $results['search']=$search;
        try{
            //get max row
            $results['max']=ceil(intval(self::getMaxRowCustomersOrderProduct($search))/$countRow);
            //select all Product
            if(''==$search){
                $results['data'] = DB::select(DB::raw("
                SELECT *
                FROM customers_order_product
                WHERE customers_order_product.deleted_at is NULL and customers_order_product.customers_order_id= :orderId
                ORDER by customers_order_product.created_at DESC
                LIMIT :limit
                OFFSET :offset"),
                    array('orderId'=>$orderId,'limit' => $countRow, 'offset' => $offset));
            }else {
                $results['data'] = DB::select(DB::raw("
                SELECT *
                FROM customers_order_product
                WHERE customers_order_product.deleted_at is NULL AND customers_order_product.customers_order_id= :orderId AND customers_order_product.customers_order_product_name LIKE '%" . $search . "%'
                ORDER by customers_order_product.created_at DESC
                LIMIT :limit
                OFFSET :offset"),
                    array('orderId'=>$orderId,'limit' => $countRow, 'offset' => $offset));
            }

        } catch (Exception $e) {
            echo $e->getLine() . ':' . $e->getMessage();
        }
        //return
        return $results;
    }
}
