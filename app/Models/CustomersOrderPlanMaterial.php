<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomersOrderPlanMaterial extends Model {
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'customers_order_plan_material';
    protected $primaryKey = 'customers_order_plan_material_id';

    public static function getByOrderPlanId($OrderPlanId){
        $dataReturn=array();
        $dataReturn = self::where('customers_order_plan_id','=',$OrderPlanId)->get();
        return $dataReturn;
    }
}
