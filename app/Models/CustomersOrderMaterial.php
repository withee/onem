<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomersOrderMaterial extends Model {
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'customers_order_material';
    protected $primaryKey = 'customers_order_material_id';

    public static function getNameMaterialName($id){
        $data=self::find($id);
        if(!empty($data)) {
            return $data->customers_order_material_name;
        }else{
            return 'ไม่มีข้อมูลสินค้า';
        }
    }

    public static function getNameStoreId($id){
        $data=self::find($id);
        if(!empty($data)) {
            return $data->storename_id;
        }else{
            return 'ไม่มีข้อมูลสินค้า';
        }
    }

    public static function getOrderMaterialByOrderId($id){
        $dataReturn=array();
        $dataReturn = DB::select(DB::raw("select ref_store_name.storename_name,customers_order_material_name,customers_order_material_id,sum(customers_order_material_count)
from customers_order_material
INNER JOIN ref_store_name ON ref_store_name.storename_id=customers_order_material.storename_id
where customers_order_material.customers_order_product_id=".$id." AND customers_order_material.deleted_at is NULL
group by customers_order_material.customers_order_material_name,customers_order_material.storename_id"));
        return $dataReturn;
    }

    public static function getListMaterial($id){
        return self::where('customers_order_product_id','=',$id)->get();
    }

    public function getMaxRowCustomersOrderMaterial($search){
        //1. Variable declaration.
        $count=array();
        //2. get max row.
        if(''==$search){
            $count = DB::select(DB::raw("select count(*) from customers_order_material where customers_order_material.deleted_at is NULL"));
        }else {
            $count = DB::select(DB::raw("select count(*) from customers_order_material WHERE customers_order_material.deleted_at is NULL and customers_order_material.customers_order_material_name LIKE '%" . $search . "%'"));
        }
        //3. Return
        return intval($count[0]->{'count(*)'});
    }

    public function findAllCustomersAndPaginate($countRow,$page,$orderId,$search){
        $results=array();
        $offset=intval($page-1)*$countRow;
        $results['page']=$page;
        $results['search']=$search;
        try{
            //get max row
            $results['max']=ceil(intval(self::getMaxRowCustomersOrderMaterial($search))/$countRow);
            //select all Product
            if(''==$search){
                $results['data'] = DB::select(DB::raw("
                SELECT *
                FROM customers_order_material
                WHERE customers_order_material.deleted_at is NULL and customers_order_material.customers_order_product_id= :orderId
                ORDER by customers_order_material.created_at DESC
                LIMIT :limit
                OFFSET :offset"),
                    array('orderId'=>$orderId,'limit' => $countRow, 'offset' => $offset));
            }else {
                $results['data'] = DB::select(DB::raw("
                SELECT *
                FROM customers_order_material
                WHERE customers_order_material.deleted_at is NULL AND customers_order_material.customers_order_product_id= :orderId AND customers_order_material.customers_order_material_name LIKE '%" . $search . "%'
                ORDER by customers_order_material.created_at DESC
                LIMIT :limit
                OFFSET :offset"),
                    array('orderId'=>$orderId,'limit' => $countRow, 'offset' => $offset));
            }

        } catch (Exception $e) {
            echo $e->getLine() . ':' . $e->getMessage();
        }
        //return
        return $results;
    }
}
