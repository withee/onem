<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class Files extends Model {
    use SoftDeletes;
    protected $table = 'file';

    public function getImg($name)
    {
        $result = array('success' => true, 'desc' => 'Operation Success!!', 'data' => array());
        //$name = Request::input('filename');

        try {
            $file = Files::where('name', '=', "$name")->orderby('id', 'DESC')->first();

            if (empty($file)) {
                $img = new Files();
                $img->path = "/images/default.jpg";
                $img->name = 'default';
                $result['data'] = $img->path;
            }else{
                $result['data'] = $file->path;
            }
        } catch (\Exception $e) {
            //echo $e->getLine() . ":" . $e->getMessage();
            $result['success']=false;
            $result['desc']=$e->getLine() . ":" . $e->getMessage();
        }
        return $result;
    }

}
