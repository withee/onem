<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 8/5/2558
 * Time: 17:04 น.
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReturnDetail extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'returns_detail';
    protected $primaryKey = 'return_detail_id';

    public function findById($id)
    {
        $results=array();
        $results = DB::select(DB::raw("
            SELECT returns_detail.return_detail_id,products.product_name,ref_store_name.storename_name,returns_detail.return_detail_count,returns_detail.product_id
            FROM returns_detail
            INNER JOIN products on (products.product_id=returns_detail.product_id)
            INNER JOIN ref_store_name on (ref_store_name.storename_id=returns_detail.storename_id)
            WHERE return_id=:id and returns_detail.deleted_at IS NULL "),
            array('id' => $id));
        return $results;
    }
}