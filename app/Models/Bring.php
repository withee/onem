<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 8/5/2558
 * Time: 17:04 น.
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bring extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'brings';
    protected $primaryKey = 'bring_id';

    public function findListProductBring($id){
        $listProductAllProject=array();
        $listProductAllProject=DB::select(DB::raw("
            select brings_detail.product_id,products.product_name
            from brings
            INNER JOIN brings_detail on (brings_detail.bring_id=brings.bring_id)
            LEFT JOIN products on (products.product_id=brings_detail.product_id)
            where brings.project_sub_id=:id group by brings_detail.product_id "),
            array('id' => $id));
        return $listProductAllProject;
    }

    public function findBringProductByDate($id){
        $bringDetail=new BringDetail();
        $returnData=array();
        $results=array();
        $results = DB::select(DB::raw("
            select brings.bring_date,brings.bring_id
            from brings
            where brings.project_sub_id=:id "),
            array('id' => $id));
        foreach($results as $key=>$value){
            $returnData[$value->bring_date]=$bringDetail->findByBringIdReturnToArray($value->bring_id);
        }
        return $returnData;
    }

    public function findByProjectSubId($id){
        $bringDetail=new BringDetail();
        $results=array();
        $results = DB::select(DB::raw("
            SELECT brings.deleted_at,brings.bring_id,brings.bring_number,brings.member_bring_date,staffs.staff_firstname,staffs.staff_lastname
            FROM brings
            LEFT JOIN staffs on (staffs.staff_id=brings.id_member_bring)
            WHERE project_sub_id=:id"),
            array('id' => $id));

        foreach($results as $key=>$value){
            $value->deleted_at=$bringDetail->findById($value->bring_id);
        }

        return $results;
    }

    public function findByGroupBring($id){

        $results=array();
        $results = DB::select(DB::raw("
            SELECT brings.created_at,brings.bring_type_project,brings.bring_type_other,brings.bring_type_manufacture,brings.bring_date,brings.bring_number,brings.group_bring_id,staffs.staff_firstname,staffs.staff_lastname
            FROM brings
            LEFT JOIN staffs on (staffs.staff_id=brings.id_member_bring)
            WHERE group_bring_id=:id"),
            array('id' => $id));
        return $results;
    }

    public function getMaxRowBring($search){
        //get max row.
        if(''==$search){
            $count = DB::select(DB::raw("select count(*) from brings WHERE deleted_at is NULL"));
        }else {
            $count = DB::select(DB::raw("select count(*) from brings WHERE deleted_at is NULL and bring_number LIKE '%" . $search . "%'"));
        }
        //Return
        return intval($count[0]->{'count(*)'});
    }

    public function findAllBringAndPaginate($countRow,$page,$search){
        //Variable declaration.
        $results=array();
        $offset=intval($page-1)*$countRow;
        $results['page']=$page;
        $results['search']=$search;
        try {
            //get max row
            $results['maxProduct']=ceil(intval(self::getMaxRowBring($search))/$countRow);
            //select all Product
            if(''==$search) {
                $results['dataProduct'] = DB::select(DB::raw("
                SELECT brings.project_sub_id,brings.bring_id,brings.bring_type_project,brings.bring_type_other,brings.bring_type_manufacture,brings.bring_date,brings.bring_number,brings.group_bring_id,staffs.staff_firstname,staffs.staff_lastname,control_build_log.control_build_log_id
                FROM brings
                LEFT JOIN control_build_log on (control_build_log.bring_id=brings.bring_id)
                LEFT JOIN staffs on (staffs.staff_id=brings.id_member_bring)
                WHERE brings.deleted_at is NULL
                LIMIT :limit OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
            }else{
                $results['dataProduct'] = DB::select(DB::raw("
                SELECT brings.project_sub_id,brings.bring_id,brings.bring_type_project,brings.bring_type_other,brings.bring_type_manufacture,brings.bring_date,brings.bring_number,brings.group_bring_id,staffs.staff_firstname,staffs.staff_lastname,control_build_log.control_build_log_id
                FROM brings
                LEFT JOIN control_build_log on (control_build_log.bring_id=brings.bring_id)
                LEFT JOIN staffs on (staffs.staff_id=brings.id_member_bring)
                WHERE brings.deleted_at is NULL AND bring_number LIKE '%" . $search . "%'
                LIMIT :limit OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
            }
        } catch (Exception $e) {
            echo $e->getLine() . ':' . $e->getMessage();
        }
        //return
        return $results;
    }

    public function addProduct($productId,$productCount,$storeId,$productLocation)
    {
        //1. Variable declaration.
        $selectProduct=array();
        $dataProduct=Product::find($productId);
        $dataStore=Storename::find($storeId);
        //2. set Object.
        $selectProduct['id']=$productId;
        $selectProduct['productName']=$dataProduct['product_name'];
        $selectProduct['productLocation']=$productLocation;
        $selectProduct['count']=$productCount;
        $selectProduct['store']=$storeId;
        $selectProduct['storeName']=$dataStore['storename_name'];
        //3.return
        return $selectProduct;
    }
}