<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyMaintenance extends Model{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'company_maintenance';
    protected $primaryKey = 'company_maintenance_id';

    public static function getCompanyMaintenanceName($id){
        $companyMaintenance=CompanyMaintenance::find($id);
        if(!empty($companyMaintenance)){
            return $companyMaintenance->company_maintenance_name;
        }
    }
}
?>