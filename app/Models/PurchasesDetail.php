<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 8/5/2558
 * Time: 17:04 น.
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchasesDetail extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'purchases_detail';
    protected $primaryKey = 'purchasesdetail_id';

    public function findById($id)
    {
        $results=array();
        $results = DB::select(DB::raw("
            SELECT purchases_detail.purchasesdetail_id,purchases_detail.product_id,products.product_name,purchases_detail.purchasesdetail_productcount,purchases_detail.purchasesdetail_productprice,ref_store_name.storename_name
            FROM purchases_detail
            INNER JOIN products on (products.product_id=purchases_detail.product_id)
            INNER JOIN ref_store_name on (ref_store_name.storename_id=purchases_detail.storename_id)
            WHERE purchases_id=:id and purchases_detail.deleted_at IS NULL "),
            array('id' => $id));
        return $results;
    }
}
?>