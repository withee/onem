<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 8/5/2558
 * Time: 17:04 น.
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectsActivityLogDetail extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'projects_activity_log_detail';
    protected $primaryKey = 'projects_activity_log_detail_id';

    public function findByProjectsActivityLogId($id)
    {
        $results=array();
        $results = DB::select(DB::raw("
            SELECT projects_activity_log_detail.start_at,projects_activity_log_detail.end_at,projects_activity_log_detail.created_at,projects_activity_log_detail.projects_activity_log_detail_id,projects_activity_log_detail.projects_activity_log_detail_value,staffs.staff_firstname,staffs.staff_lastname,projects_activity.projects_activity_name,ref_projects_activity_units.ref_projects_activity_units_name
            FROM projects_activity_log_detail
            INNER JOIN staffs on (staffs.staff_id=projects_activity_log_detail.id_staff_works)
            INNER JOIN projects_activity on (projects_activity.projects_activity_id=projects_activity_log_detail.projects_activity_id)
            INNER JOIN ref_projects_activity_units on (ref_projects_activity_units.ref_projects_activity_units_id=projects_activity.projects_activity_unit)
            WHERE projects_activity_log_detail.projects_activity_log_id=:id and projects_activity_log_detail.deleted_at is NULL "),
            array('id' => $id));
        return $results;
    }

    public function deleteActivityLogDetail($id)
    {
        //1. Variable declaration.
        $result = array();
        $result['status']='';
        $result['desc']='';
        $result['id']='';
        //2. Find ByID.
        $activityLogDetail = ProjectsActivityLogDetail::find($id);
        if(!empty($activityLogDetail)){
            $activityLogDetail->delete();
            $result['id']=$activityLogDetail->projects_activity_log_id;
        }else {
            $result['status']='error';
            $result['desc']='ไม่สามารถ ลบข้อมูลสินค้าได้';
        }
        //3. Return $result to controller.
        return $result;
    }
}