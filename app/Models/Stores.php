<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 8/5/2558
 * Time: 17:04 น.
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Session;

class Stores extends Model{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'stores';
    protected $primaryKey = 'store_id';

    public function findByProduct($id){
        $results=array();
        $stores = DB::select(DB::raw("SELECT * FROM stores WHERE product_id='".$id."'"));
        if(false==empty($stores)){
            foreach($stores as $key=>$value){
                $results[$value->storename_id]=$value;
            }
        }
        return $results;
    }

    public function getAllData(){
        $results=array();
        $results = DB::select(DB::raw("
            SELECT stores.use_product,stores.total_product,stores.balance_product,products.product_name,ref_store_name.storename_name
            FROM stores
            INNER JOIN products on (products.product_id=stores.product_id)
            INNER JOIN ref_store_name on (ref_store_name.storename_id=stores.storename_id)"));
        return $results;
    }

    public function updateStore($product_id,$storename_id,$countProduct,$type){
        $checkStore=Stores::where('product_id', '=', $product_id)->where('storename_id', '=', $storename_id)->first();
        if(true==empty($checkStore)){
            $newStore=new Stores();
            $newStore->product_id=$product_id;
            $newStore->storename_id = $storename_id;
                if('minus'==$type) {
                    $newStore->use_product = $countProduct;
                    $newStore->total_product = $countProduct*-1;
                    $newStore->balance_product = $countProduct*-1;
                }elseif('plus'==$type){
                    $newStore->use_product = 0;
                    $newStore->total_product = $countProduct;
                    $newStore->balance_product = $countProduct;
                }
            $newStore->save();
        }else{
            $updateStore=Stores::find($checkStore->store_id);
            if('minus'==$type){
                $updateStore->use_product=$countProduct;
                $updateStore->total_product-=$countProduct;
                $updateStore->balance_product-=$countProduct;
            }elseif('plus'==$type){
                $updateStore->total_product+=$countProduct;
                $updateStore->balance_product+=$countProduct;
            }
            $updateStore->save();
        }
    }

    public static function CheckStoreCount($product_id,$storename_id){
        $checkStore=Stores::where('product_id', '=', $product_id)->where('storename_id', '=', $storename_id)->first();
        if(!empty($checkStore)){
//            if($checkStore->balance_product>$count){
                return $checkStore->balance_product;
//            }else{
//                return 0;
//            }
        }else{
            return 0;
        }
    }

    public static function CheckStore($product_id,$storename_id,$count){
        $checkStore=Stores::where('product_id', '=', $product_id)->where('storename_id', '=', $storename_id)->first();
        if(!empty($checkStore)){
            if($checkStore->balance_product>$count){
                return 'alert alert-info';
            }else{
                return 'alert alert-danger';
            }
        }else{
            return 'alert alert-danger';
        }
    }

    public static function CheckStoreStatus($product_id,$storename_id,$count){
        $checkStore=Stores::where('product_id', '=', $product_id)->where('storename_id', '=', $storename_id)->first();
        if(!empty($checkStore)){
            if($checkStore->balance_product>$count){
                return 'success';
            }else{
                return 'danger';
            }
        }else{
            return 'danger';
        }
    }

}