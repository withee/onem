<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 8/5/2558
 * Time: 17:04 น.
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectsActivityLogProblem extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'ref_projects_activity_log_problem';
    protected $primaryKey = 'ref_projects_activity_log_problem_id';
}