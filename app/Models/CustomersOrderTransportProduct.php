<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomersOrderTransportProduct extends Model {
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'customers_order_transport_product';
    protected $primaryKey = 'customers_order_transport_product_id';

    public static function getTransportProductCode($order_id,$customers_order_product_id){
        $results = DB::select(DB::raw("
               select customers_order_transport_product.customers_order_transport_product_id,customers_order_transport.customers_order_transport_code
from customers_order_transport_product
inner join customers_order_transport on customers_order_transport_product.customers_order_transport_id=customers_order_transport.customers_order_transport_id
where customers_order_transport_product.customers_order_transport_product_status!='complete' and customers_order_transport_product.customers_order_id=".$order_id." and customers_order_transport_product.customers_order_product_id=".$customers_order_product_id));
        return $results;
    }

    public static function getSumComplete($orderId,$order_product_id){
        $count = DB::select(DB::raw("select sum(customers_order_transport_product_complete) from customers_order_transport_product where customers_order_transport_product.deleted_at is NULL AND customers_order_id=".$orderId." AND customers_order_product_id=".$order_product_id));
        return intval($count[0]->{'sum(customers_order_transport_product_complete)'});
    }

    public static function getSumWaste($orderId,$order_product_id){
        $count = DB::select(DB::raw("select sum(customers_order_transport_product_waste) from customers_order_transport_product where customers_order_transport_product.deleted_at is NULL AND customers_order_id=".$orderId." AND customers_order_product_id=".$order_product_id));
        return intval($count[0]->{'sum(customers_order_transport_product_waste)'});
    }
}
