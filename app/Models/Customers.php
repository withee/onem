<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customers extends Model {
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'customers';
    protected $primaryKey = 'customers_id';

    public function getMaxRowCustomers($search){
        //1. Variable declaration.
        $count=array();
        //2. get max row.
        if(''==$search){
            $count = DB::select(DB::raw("select count(*) from customers where customers.deleted_at is NULL"));
        }else {
            $count = DB::select(DB::raw("select count(*) from customers WHERE customers.deleted_at is NULL and customers.customers_name LIKE '%" . $search . "%'"));
        }
        //3. Return
        return intval($count[0]->{'count(*)'});
    }

    public function findAllCustomersAndPaginate($countRow,$page,$search){
        //
        $results=array();
        $offset=intval($page-1)*$countRow;
        $results['page']=$page;
        $results['search']=$search;
        try{
            //get max row
            $results['max']=ceil(intval(self::getMaxRowCustomers($search))/$countRow);
            //select all Product
            if(''==$search){
                $results['data'] = DB::select(DB::raw("
                SELECT *
                FROM customers
                WHERE customers.deleted_at is NULL
                ORDER by customers.created_at DESC
                LIMIT :limit
                OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
            }else {
                $results['data'] = DB::select(DB::raw("
                SELECT *
                FROM customers
                WHERE customers.deleted_at is NULL AND customers.customers_name LIKE '%" . $search . "%'
                ORDER by customers.created_at DESC
                LIMIT :limit
                OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
            }

        } catch (Exception $e) {
            echo $e->getLine() . ':' . $e->getMessage();
        }
        //return
        return $results;
    }

    public static function Convert($number)
    {
        $txtnum1 = array('ศูนย์', 'หนึ่ง', 'สอง', 'สาม', 'สี่', 'ห้า', 'หก', 'เจ็ด', 'แปด', 'เก้า', 'สิบ');
        $txtnum2 = array('', 'สิบ', 'ร้อย', 'พัน', 'หมื่น', 'แสน', 'ล้าน', 'สิบ', 'ร้อย', 'พัน', 'หมื่น', 'แสน', 'ล้าน');
        $number = str_replace(",", "", $number);
        $number = str_replace(" ", "", $number);
        $number = str_replace("บาท", "", $number);
        $number = explode(".", $number);
        if (sizeof($number) > 2) {
            return 'ทศนิยมหลายตัวนะจ๊ะ';
            exit;
        }
        $strlen = strlen($number[0]);
        $convert = '';
        for ($i = 0; $i < $strlen; $i++) {
            $n = substr($number[0], $i, 1);
            if ($n != 0) {
                if ($i == ($strlen - 1) AND $n == 1) {
                    $convert .= 'เอ็ด';
                } elseif ($i == ($strlen - 2) AND $n == 2) {
                    $convert .= 'ยี่';
                } elseif ($i == ($strlen - 2) AND $n == 1) {
                    $convert .= '';
                } else {
                    $convert .= $txtnum1[$n];
                }
                $convert .= $txtnum2[$strlen - $i - 1];
            }
        }

        $convert .= 'บาท';
        if(!empty($number[1])) {
            if ($number[1] == '0' OR $number[1] == '00' OR
                $number[1] == ''
            ) {
                $convert .= 'ถ้วน';
            } else {
                $strlen = strlen($number[1]);
                for ($i = 0; $i < $strlen; $i++) {
                    $n = substr($number[1], $i, 1);
                    if ($n != 0) {
                        if ($i == ($strlen - 1) AND $n == 1) {
                            $convert
                                .= 'เอ็ด';
                        } elseif ($i == ($strlen - 2) AND
                            $n == 2
                        ) {
                            $convert .= 'ยี่';
                        } elseif ($i == ($strlen - 2) AND
                            $n == 1
                        ) {
                            $convert .= '';
                        } else {
                            $convert .= $txtnum1[$n];
                        }
                        $convert .= $txtnum2[$strlen - $i - 1];
                    }
                }
                $convert .= 'สตางค์';
            }
        }
        return $convert.='ถ้วน';
    }
}

