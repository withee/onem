<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderManufactureDetail extends Model{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'order_manufacture_detail';
    protected $primaryKey = 'order_manufacture_detail_id';
}
?>