<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class PrOrder extends Model {
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'pr_order';
    protected $primaryKey = 'PR_order_id';

    public function getMaxRowPrOrder($search){
        //1. Variable declaration.
        $count=array();
        //2. get max row.
        if(''==$search){
            $count = DB::select(DB::raw("select count(*) from pr_order where pr_order.deleted_at is NULL"));
        }else {
            $count = DB::select(DB::raw("select count(*) from pr_order WHERE pr_order.deleted_at is NULL and pr_order.PR_order_name LIKE '%" . $search . "%'"));
        }
        //3. Return
        return intval($count[0]->{'count(*)'});
    }

    public function findAllPrOrderAndPaginate($countRow,$page,$search){
        $results=array();
        $offset=intval($page-1)*$countRow;
        $results['page']=$page;
        $results['search']=$search;
        try{
            //get max row
            $results['max']=ceil(intval(self::getMaxRowPrOrder($search))/$countRow);
            //select all Product
            if(''==$search){
                $results['data'] = DB::select(DB::raw("
                SELECT *
                FROM pr_order
                WHERE pr_order.deleted_at is NULL
                ORDER by pr_order.created_at DESC
                LIMIT :limit
                OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
            }else {
                $results['data'] = DB::select(DB::raw("
                SELECT *
                FROM pr_order
                WHERE pr_order.deleted_at is NULL AND pr_order.PR_order_name LIKE '%" . $search . "%'
                ORDER by pr_order.created_at DESC
                LIMIT :limit
                OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
            }

        } catch (Exception $e) {
            echo $e->getLine() . ':' . $e->getMessage();
        }
        //return
        return $results;
    }

    public static function checkStatus($status){
        if($status=='New') {
            return'เปิดใหม่';
        }elseif($status=='confirmPurchasing'){
            return'ฝ่ายสั่งซื้อยืนยัน';
        }elseif($status=='confirmStore'){
            return'ฝ่ายคลังยืนยัน';
        }elseif($status=='confirmManager'){
            return'ผู้จัดการยืนยัน';
        }elseif($status=='confirm'){
            return'ยืนยัน';
        }
    }
}
