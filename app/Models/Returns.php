<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 8/5/2558
 * Time: 17:04 น.
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Returns extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'returns';
    protected $primaryKey = 'return_id';

    public function getMaxRowReturn($search){
        //get max row.
        if(''==$search){
            $count = DB::select(DB::raw("select count(*) from returns WHERE deleted_at is NULL"));
        }else {
            $count = DB::select(DB::raw("select count(*) from returns WHERE deleted_at is NULL and return_number LIKE '%" . $search . "%'"));
        }
        //Return
        return intval($count[0]->{'count(*)'});
    }

    public function findAllReturnAndPaginate($countRow,$page,$search){
        //Variable declaration.

        $results=array();
        $offset=intval($page-1)*$countRow;
        $results['page']=$page;
        $results['search']=$search;
        try {
            //get max row
            $results['maxProduct']=ceil(intval(self::getMaxRowReturn($search))/$countRow);
            //select all Product
            if(''==$search) {
                $results['dataProduct'] = DB::select(DB::raw("
                SELECT returns.return_date,returns.return_number,returns.return_detail,returns.return_id,returns.return_type_manufacture,returns.return_type_project,returns.return_type_other,staffs.staff_lastname,staffs.staff_firstname
                FROM returns
                INNER JOIN staffs on (staffs.staff_id=returns.id_member_return)
                WHERE returns.deleted_at is NULL
                LIMIT :limit OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
            }else{
                $results['dataProduct'] = DB::select(DB::raw("
                SELECT returns.return_date,returns.return_number,returns.return_detail,returns.return_id,returns.return_type_manufacture,returns.return_type_project,returns.return_type_other,staffs.staff_lastname,staffs.staff_firstname
                FROM returns
                INNER JOIN staffs on (staffs.staff_id=returns.id_member_return)
                WHERE returns.deleted_at is NULL and return_number LIKE '%" . $search . "%'
                LIMIT :limit OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
            }
        } catch (Exception $e) {
            echo $e->getLine() . ':' . $e->getMessage();
        }
        //return
        return $results;
    }

    public function getAllReturnProduct(){
        $results=array();
        $results = DB::select(DB::raw("
            SELECT returns.return_date,returns.return_number,returns.return_detail,returns.return_id,returns.return_type_manufacture,returns.return_type_project,returns.return_type_other,staffs.staff_lastname,staffs.staff_firstname
            FROM returns
            INNER JOIN staffs on (staffs.staff_id=returns.id_member_return)"));
        return $results;
    }

    public function addProduct($productId,$productCount,$storeId)
    {
        //1. Variable declaration.
        $selectProduct=array();
        $dataProduct=Product::find($productId);
        $dataStore=Storename::find($storeId);
        //2. set Object.
        $selectProduct['id']=$productId;
        $selectProduct['productName']=$dataProduct['product_name'];
        $selectProduct['count']=$productCount;
        $selectProduct['store']=$storeId;
        $selectProduct['storeName']=$dataStore['storename_name'];
        //3.return
        return $selectProduct;
    }
}