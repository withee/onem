<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomersOrderPlan extends Model {
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'customers_order_plan';
    protected $primaryKey = 'customers_order_plan_id';

    public static function getDatePlan($orderId,$status=null){
        $dataReturn=array();
        if(!empty($status)) {
            $dataReturn = DB::select(DB::raw("select customers_order_plan.customers_order_plan_date from customers_order_plan where customers_order_plan_status='" . $status . "' AND customers_order_plan.deleted_at is NULL AND customers_order_plan.customers_order_id=" . $orderId . " group by customers_order_plan.customers_order_plan_date"));
        }else{
            $dataReturn = DB::select(DB::raw("select customers_order_plan.customers_order_plan_date from customers_order_plan where customers_order_plan.deleted_at is NULL AND customers_order_plan.customers_order_id=" . $orderId . " group by customers_order_plan.customers_order_plan_date"));
        }
        return $dataReturn;
    }

    public static function getSumComplete($orderId,$order_product_id){
        $count = DB::select(DB::raw("select sum(customers_order_plan_complete) from customers_order_plan where customers_order_plan.deleted_at is NULL AND customers_order_plan_status='complete' AND customers_order_id=".$orderId." AND customers_order_product_id=".$order_product_id));
        return intval($count[0]->{'sum(customers_order_plan_complete)'});
    }

    public static function getSumWaste($orderId,$order_product_id){
        $count = DB::select(DB::raw("select sum(customers_order_plan_waste) from customers_order_plan where customers_order_plan.deleted_at is NULL AND customers_order_plan_status='complete' AND customers_order_id=".$orderId." AND customers_order_product_id=".$order_product_id));
        return intval($count[0]->{'sum(customers_order_plan_waste)'});
    }

}
