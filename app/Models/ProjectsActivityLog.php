<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 8/5/2558
 * Time: 17:04 น.
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class ProjectsActivityLog extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'projects_activity_log';
    protected $primaryKey = 'projects_activity_log_id';

    public function getProjectsActivityLogDetail($dataListProjectSub,$month=null,$order=null){

        if(true==empty($order)){
            $order='projects_activity_log.projects_activity_start_date';
        }

        $datetime =Carbon::now();
        if(false==empty($month)){
            $year=$datetime->format('Y');
            $maxDay=$datetime->format('t');
            $datetime->setdate($year,$month,1);
            $start_date=$datetime->format("Y-m-d 00:00:00");
            $datetime->setdate($year,$month,$maxDay);
            $end_date=$datetime->format("Y-m-d 23:59:59");

            $results = DB::select(DB::raw("select projects_activity_log.projects_activity_start_date,
                projects_activity_log_detail.start_at,
                projects_activity_log_detail.end_at,
                projects_sub.project_position,
                projects_activity.projects_activity_name,
                projects_activity_log_detail.projects_activity_log_detail_value,
                projects_activity_log.projects_activity_log_weather,
                projects_activity_log.projects_activity_log_problem,
                staffs.staff_firstname,
                staffs.staff_lastname
                from projects_activity_log
                INNER JOIN projects_activity_log_detail on(projects_activity_log_detail.projects_activity_log_id=projects_activity_log.projects_activity_log_id)
                INNER JOIN projects_activity on(projects_activity.projects_activity_id=projects_activity_log_detail.projects_activity_id)
                INNER JOIN staffs on (staffs.staff_id=projects_activity_log_detail.id_staff_works)
                INNER JOIN projects_sub on (projects_sub.project_sub_id=projects_activity_log.project_sub_id)
                where (projects_activity_log.project_sub_id=:id || projects_activity_log.project_sub_id=:id2) and projects_activity_log.projects_activity_start_date>='" . $start_date . "' and projects_activity_log.projects_activity_start_date<='" . $end_date . "'
                order by ".$order." DESC"), array('id' => $dataListProjectSub[0]->project_sub_id, 'id2' => $dataListProjectSub[1]->project_sub_id));
        }else {
            $results = DB::select(DB::raw("select projects_activity_log.projects_activity_start_date,
                projects_activity_log_detail.start_at,
                projects_activity_log_detail.end_at,
                projects_sub.project_position,
                projects_activity.projects_activity_name,
                projects_activity_log_detail.projects_activity_log_detail_value,
                projects_activity_log.projects_activity_log_weather,
                projects_activity_log.projects_activity_log_problem,
                staffs.staff_firstname,
                staffs.staff_lastname
                from projects_activity_log
                INNER JOIN projects_activity_log_detail on(projects_activity_log_detail.projects_activity_log_id=projects_activity_log.projects_activity_log_id)
                INNER JOIN projects_activity on(projects_activity.projects_activity_id=projects_activity_log_detail.projects_activity_id)
                INNER JOIN staffs on (staffs.staff_id=projects_activity_log_detail.id_staff_works)
                INNER JOIN projects_sub on (projects_sub.project_sub_id=projects_activity_log.project_sub_id)
                where projects_activity_log.project_sub_id=:id || projects_activity_log.project_sub_id=:id2
                order by ".$order." DESC"), array('id' => $dataListProjectSub[0]->project_sub_id, 'id2' => $dataListProjectSub[1]->project_sub_id));
        }
        return $results;
    }

    public function getActivityBySubProject($id){
        $results=array();
        $results = DB::select(DB::raw("
            select projects_activity_log_detail.created_at,projects_activity.projects_activity_name,projects_activity_log_detail.start_at,projects_activity_log_detail.end_at,projects_activity_log_detail.projects_activity_log_detail_value
            from projects_activity_log
            INNER JOIN projects_activity_log_detail on(projects_activity_log_detail.projects_activity_log_id=projects_activity_log.projects_activity_log_id)
            INNER JOIN projects_activity on(projects_activity.projects_activity_id=projects_activity_log_detail.projects_activity_id)
            where projects_activity_log.project_sub_id=:id"),
            array('id' => $id));
        return $results;
    }

    public function findWorkMonth($name,$month=null){
        $results=array();
        $datetime =Carbon::now();
        if(true==empty($month)){
            $month=$datetime->format('m');
        }

        $year=$datetime->format('Y');
        $maxDay=$datetime->format('t');
        $datetime->setdate($year,$month,1);
        $start_date=$datetime->format("Y-m-d 00:00:00");
        $datetime->setdate($year,$month,$maxDay);
        $end_date=$datetime->format("Y-m-d 23:59:59");

            $dataList = DB::select(DB::raw("select DAY(projects_activity_log.projects_activity_start_date) As ListDay,projects_sub.project_position,staffs.staff_firstname,staffs.staff_lastname,projects_activity_log_detail.projects_activity_id,projects_activity_log_detail.projects_activity_log_detail_value
            from projects_sub
            INNER JOIN projects_activity_log on (projects_activity_log.project_sub_id=projects_sub.project_sub_id)
            INNER JOIN projects_activity_log_detail on (projects_activity_log_detail.projects_activity_log_id=projects_activity_log.projects_activity_log_id)
            INNER JOIN staffs on (staffs.staff_id=projects_activity_log_detail.id_staff_works)
            where (projects_sub.project_sub_name='" . $name . "') and projects_activity_log.projects_activity_start_date>='" . $start_date . "' and projects_activity_log.projects_activity_start_date<='" . $end_date . "'"));

        foreach($dataList as $key=>$value){
            if(false==empty($results[$value->ListDay][$value->projects_activity_id])){
                $results[$value->ListDay][$value->projects_activity_id]['value']+=$value->projects_activity_log_detail_value;
                $results[$value->ListDay][$value->projects_activity_id]['text'].='<br>'.$value->staff_firstname.' '.$value->staff_lastname.'('.$value->projects_activity_log_detail_value.''.str_replace('ight','',str_replace('eft','',$value->project_position)).')';
            }else{
                $results[$value->ListDay][$value->projects_activity_id]['value']=$value->projects_activity_log_detail_value;
                $results[$value->ListDay][$value->projects_activity_id]['text']='ผู้ปฏิบติงาน<br>';
                $results[$value->ListDay][$value->projects_activity_id]['text'].=$value->staff_firstname.' '.$value->staff_lastname.'('.$value->projects_activity_log_detail_value.''.str_replace('ight','',str_replace('eft','',$value->project_position)).')';
            }
        }
        return $results;
    }

    public function findActivitySumByProjectSubId($id){
        $results=array();
        $results = DB::select(DB::raw("
            select projects_activity.projects_activity_name,sum(projects_activity_log_detail.projects_activity_log_detail_value) as sum
            from projects_activity_log
            INNER JOIN projects_activity_log_detail on (projects_activity_log_detail.projects_activity_log_id=projects_activity_log.projects_activity_log_id)
            INNER JOIN projects_activity on (projects_activity.projects_activity_id=projects_activity_log_detail.projects_activity_id)
            where projects_activity_log.project_sub_id=:id
            group by projects_activity_log_detail.projects_activity_id"),
            array('id' => $id));
        return $results;
    }

    public function findById($id){
        $results=array();
        $results = DB::select(DB::raw("
            SELECT projects_activity_log.projects_activity_log_id,projects_activity_log.projects_activity_start_date,projects_activity_log.projects_activity_end_date,staffs.staff_firstname,staffs.staff_lastname,projects.project_name
            FROM projects_activity_log
            LEFT JOIN staffs on (staffs.staff_id=projects_activity_log.id_staff_assign)
            LEFT JOIN projects on (projects.project_id=projects_activity_log.project_id)
            WHERE projects_activity_log.projects_activity_log_id=:id"),
            array('id' => $id));
        return $results;
    }

    public function findByProjectId($id){
        $projectsActivityLogDetail=new ProjectsActivityLogDetail();
        $results=array();
        $results = DB::select(DB::raw("
            SELECT projects_activity_log.deleted_at,projects_activity_log.projects_activity_log_id,projects_activity_log.projects_activity_start_date,projects_activity_log.projects_activity_end_date,staffs.staff_firstname,staffs.staff_lastname,projects.project_name
            FROM projects_activity_log
            LEFT JOIN staffs on (staffs.staff_id=projects_activity_log.id_staff_assign)
            LEFT JOIN projects on (projects.project_id=projects_activity_log.project_id)
            WHERE projects_activity_log.project_id=:id"),
            array('id' => $id));

        foreach($results as $key=>$value){
            $value->deleted_at=$projectsActivityLogDetail->findByProjectsActivityLogId($value->projects_activity_log_id);
        }
        return $results;
    }

    public function findByIdAll($id){
        $projectsActivityLogDetail=new ProjectsActivityLogDetail();
        $results=array();
        $results = DB::select(DB::raw("
            SELECT projects_activity_log.created_at,projects_activity_log.projects_activity_log_id,projects_activity_log.deleted_at,projects_activity_log.projects_activity_end_date,projects_activity_log.projects_activity_start_date,projects_activity_log.projects_activity_log_recommend,projects_activity_log.projects_activity_log_problem,projects_activity_log.projects_activity_log_weather,projects_activity_log.projects_activity_log_dailyEvents,projects_activity_log.projects_activity_log_province,projects_activity_log.projects_activity_log_amphoe,projects_activity_log.projects_activity_log_tambon,projects_activity_log.projects_activity_log_village,projects_activity_log.projects_activity_log_km,projects_activity_log.projects_activity_log_worker,staffs.staff_firstname,staffs.staff_lastname,projects.project_name
            FROM projects_activity_log
            LEFT JOIN staffs on (staffs.staff_id=projects_activity_log.id_staff_assign)
            LEFT JOIN projects on (projects.project_id=projects_activity_log.project_id)
            WHERE projects_activity_log.project_sub_id =:id"),
            array('id' => $id));

        foreach($results as $key=>$value){
            $value->deleted_at=$projectsActivityLogDetail->findByProjectsActivityLogId($value->projects_activity_log_id);
        }
        return $results;
    }

    public function findByProjectsActivityLogId($id){
        $projectsActivityLogDetail=new ProjectsActivityLogDetail();
        $results=array();
        $results = DB::select(DB::raw("
            SELECT projects_activity_log.deleted_at,projects_activity_log.projects_activity_log_id,projects_activity_log.projects_activity_start_date,projects_activity_log.projects_activity_end_date,staffs.staff_firstname,staffs.staff_lastname,projects.project_name
            FROM projects_activity_log
            LEFT JOIN staffs on (staffs.staff_id=projects_activity_log.id_staff_assign)
            LEFT JOIN projects on (projects.project_id=projects_activity_log.project_id)
            WHERE projects_activity_log.projects_activity_log_id=:id"),
            array('id' => $id));

        foreach($results as $key=>$value){
            $value->deleted_at=$projectsActivityLogDetail->findByProjectsActivityLogId($value->projects_activity_log_id);
        }
        return $results;
    }
}