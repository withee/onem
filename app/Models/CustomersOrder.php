<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomersOrder extends Model {
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'customers_order';
    protected $primaryKey = 'customers_order_id';

    public function getMaxRowCustomersOrder($search){
        //1. Variable declaration.
        $count=array();
        //2. get max row.
        if(''==$search){
            $count = DB::select(DB::raw("select count(*) from customers_order where customers_order.deleted_at is NULL"));
        }else {
            $count = DB::select(DB::raw("select count(*) from customers_order WHERE customers_order.deleted_at is NULL and customers_order.customers_order_name LIKE '%" . $search . "%'"));
        }
        //3. Return
        return intval($count[0]->{'count(*)'});
    }

    public function findAllCustomersAndPaginate($countRow,$page,$search){
        $results=array();
        $offset=intval($page-1)*$countRow;
        $results['page']=$page;
        $results['search']=$search;
        try{
            //get max row
            $results['max']=ceil(intval(self::getMaxRowCustomersOrder($search))/$countRow);
            //select all Product
            if(''==$search){
                $results['data'] = DB::select(DB::raw("
                SELECT *
                FROM customers_order
                WHERE customers_order.deleted_at is NULL
                ORDER by customers_order.created_at DESC
                LIMIT :limit
                OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
            }else {
                $results['data'] = DB::select(DB::raw("
                SELECT *
                FROM customers_order
                WHERE customers_order.deleted_at is NULL AND customers_order.customers_order_name LIKE '%" . $search . "%'
                ORDER by customers_order.created_at DESC
                LIMIT :limit
                OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
            }

        } catch (Exception $e) {
            echo $e->getLine() . ':' . $e->getMessage();
        }
        //return
        return $results;
    }

    public function getMaxRowCustomersOrderCustomers($search){
        //1. Variable declaration.
        $count=array();
        //2. get max row.
        if(''==$search){
            $count = DB::select(DB::raw("select count(*) from customers_order where customers_order.deleted_at is NULL AND customers_order_status='create'"));
        }else {
            $count = DB::select(DB::raw("select count(*) from customers_order WHERE customers_order.deleted_at is NULL AND customers_order_status='create' and customers_order.customers_order_name LIKE '%" . $search . "%'"));
        }
        //3. Return
        return intval($count[0]->{'count(*)'});
    }

    public function findAllCustomersAndPaginateCustomers($countRow,$page,$search){
        $results=array();
        $offset=intval($page-1)*$countRow;
        $results['page']=$page;
        $results['search']=$search;
        try{
            //get max row
            $results['max']=ceil(intval(self::getMaxRowCustomersOrderCustomers($search))/$countRow);
            //select all Product
            if(''==$search){
                $results['data'] = DB::select(DB::raw("
                SELECT *
                FROM customers_order
                WHERE customers_order.deleted_at is NULL AND (customers_order_status='markOrder' OR customers_order_status='plan' OR customers_order_status='checkQuality' OR customers_order_status='confirmation' OR customers_order_status='approve')
                ORDER by customers_order.created_at DESC
                LIMIT :limit
                OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
            }else {
                $results['data'] = DB::select(DB::raw("
                SELECT *
                FROM customers_order
                WHERE customers_order.deleted_at is NULL AND (customers_order_status='markOrder' OR customers_order_status='plan' OR customers_order_status='checkQuality' OR customers_order_status='confirmation' OR customers_order_status='approve') AND customers_order.customers_order_name LIKE '%" . $search . "%'
                ORDER by customers_order.created_at DESC
                LIMIT :limit
                OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
            }

        } catch (Exception $e) {
            echo $e->getLine() . ':' . $e->getMessage();
        }
        //return
        return $results;
    }

    public function getMaxRowCustomersOrderOffice($search){
        //1. Variable declaration.
        $count=array();
        //2. get max row.
        if(''==$search){
            $count = DB::select(DB::raw("select count(*) from customers_order where customers_order.deleted_at is NULL AND (customers_order_status='markOrder' OR customers_order_status='deal' OR customers_order_status='plan' OR customers_order_status='checkQuality' OR customers_order_status='confirmation' OR customers_order_status='approve')"));
        }else {
            $count = DB::select(DB::raw("select count(*) from customers_order WHERE customers_order.deleted_at is NULL AND (customers_order_status='markOrder' OR customers_order_status='deal' OR customers_order_status='plan' OR customers_order_status='checkQuality' OR customers_order_status='confirmation' OR customers_order_status='approve') and customers_order.customers_order_name LIKE '%" . $search . "%'"));
        }
        //3. Return
        return intval($count[0]->{'count(*)'});
    }

    public function findAllCustomersAndPaginateOffice($countRow,$page,$search){
        $results=array();
        $offset=intval($page-1)*$countRow;
        $results['page']=$page;
        $results['search']=$search;
        try{
            //get max row
            $results['max']=ceil(intval(self::getMaxRowCustomersOrder($search))/$countRow);
            //select all Product
            if(''==$search){
                $results['data'] = DB::select(DB::raw("
                SELECT *
                FROM customers_order
                WHERE customers_order.deleted_at is NULL AND (customers_order_status='create' OR customers_order_status='markOrder' OR customers_order_status='deal' OR customers_order_status='plan' OR customers_order_status='checkQuality' OR customers_order_status='confirmation' OR customers_order_status='approve')
                ORDER by FIELD(customers_order_status, 'markOrder','create') DESC
                LIMIT :limit
                OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
            }else {
                $results['data'] = DB::select(DB::raw("
                SELECT *
                FROM customers_order
                WHERE customers_order.deleted_at is NULL AND (customers_order_status='create' OR customers_order_status='markOrder' OR customers_order_status='deal' OR customers_order_status='plan' OR customers_order_status='checkQuality' OR customers_order_status='confirmation' OR customers_order_status='approve') AND customers_order.customers_order_name LIKE '%" . $search . "%'
                ORDER by FIELD(customers_order_status, 'markOrder','create') DESC
                LIMIT :limit
                OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
            }

        } catch (Exception $e) {
            echo $e->getLine() . ':' . $e->getMessage();
        }
        //return
        return $results;
    }

    public function getMaxRowCustomersOrderEngineer($search){
        //1. Variable declaration.
        $count=array();
        //2. get max row.
        if(''==$search){
            $count = DB::select(DB::raw("select count(*) from customers_order where customers_order.deleted_at is NULL AND (customers_order_status='deal' OR customers_order_status='plan' OR customers_order_status='checkQuality' OR customers_order_status='confirmation' OR customers_order_status='approve')"));
        }else {
            $count = DB::select(DB::raw("select count(*) from customers_order WHERE customers_order.deleted_at is NULL AND (customers_order_status='deal' OR customers_order_status='plan' OR customers_order_status='checkQuality' OR customers_order_status='confirmation' OR customers_order_status='approve') AND customers_order.customers_order_name LIKE '%" . $search . "%'"));
        }
        //3. Return
        return intval($count[0]->{'count(*)'});
    }

    public function findAllCustomersAndPaginateEngineer($countRow,$page,$search){
        $results=array();
        $offset=intval($page-1)*$countRow;
        $results['page']=$page;
        $results['search']=$search;
        try{
            //get max row
            $results['max']=ceil(intval(self::getMaxRowCustomersOrderEngineer($search))/$countRow);
            //select all Product
            if(''==$search){
                $results['data'] = DB::select(DB::raw("
                SELECT *
                FROM customers_order
                WHERE customers_order.deleted_at is NULL AND (customers_order_status='deal' OR customers_order_status='plan' OR customers_order_status='checkQuality' OR customers_order_status='confirmation' OR customers_order_status='approve')
                ORDER by customers_order.created_at DESC
                LIMIT :limit
                OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
            }else {
                $results['data'] = DB::select(DB::raw("
                SELECT *
                FROM customers_order
                WHERE customers_order.deleted_at is NULL AND (customers_order_status='deal' OR customers_order_status='plan' OR customers_order_status='checkQuality' OR customers_order_status='confirmation' OR customers_order_status='approve') AND customers_order.customers_order_name LIKE '%" . $search . "%'
                ORDER by customers_order.created_at DESC
                LIMIT :limit
                OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
            }

        } catch (Exception $e) {
            echo $e->getLine() . ':' . $e->getMessage();
        }
        //return
        return $results;
    }

    public function getMaxRowCustomersOrderManager($search){
        //1. Variable declaration.
        $count=array();
        //2. get max row.
        if(''==$search){
            $count = DB::select(DB::raw("select count(*) from customers_order where customers_order.deleted_at is NULL "));
        }else {
            $count = DB::select(DB::raw("select count(*) from customers_order WHERE customers_order.deleted_at is NULL AND customers_order.customers_order_name LIKE '%" . $search . "%'"));
        }
        //3. Return
        return intval($count[0]->{'count(*)'});
    }

    public function findAllCustomersAndPaginateManager($countRow,$page,$search){
        $results=array();
        $offset=intval($page-1)*$countRow;
        $results['page']=$page;
        $results['search']=$search;
        try{
            //get max row
            $results['max']=ceil(intval(self::getMaxRowCustomersOrderManager($search))/$countRow);
            //select all Product
            if(''==$search){
                $results['data'] = DB::select(DB::raw("
                SELECT *
                FROM customers_order
                WHERE customers_order.deleted_at is NULL
                ORDER by FIELD(customers_order_status, 'complete') ASC
                LIMIT :limit
                OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
            }else {
                $results['data'] = DB::select(DB::raw("
                SELECT *
                FROM customers_order
                WHERE customers_order.deleted_at is NULL AND customers_order.customers_order_name LIKE '%" . $search . "%'
                ORDER by FIELD(customers_order_status, 'complete') ASC
                LIMIT :limit
                OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
            }

        } catch (Exception $e) {
            echo $e->getLine() . ':' . $e->getMessage();
        }
        //return
        return $results;
    }

    public function getMaxRowCustomersOrderFactory($search){
        //1. Variable declaration.
        $count=array();
        //2. get max row.
        if(''==$search){
            $count = DB::select(DB::raw("select count(*) from customers_order where customers_order.deleted_at is NULL AND customers_order_status='approve'"));
        }else {
            $count = DB::select(DB::raw("select count(*) from customers_order WHERE customers_order.deleted_at is NULL AND customers_order_status='approve' AND customers_order.customers_order_name LIKE '%" . $search . "%'"));
        }
        //3. Return
        return intval($count[0]->{'count(*)'});
    }

    public function findAllCustomersAndPaginateFactory($countRow,$page,$search){
        $results=array();
        $offset=intval($page-1)*$countRow;
        $results['page']=$page;
        $results['search']=$search;
        try{
            //get max row
            $results['max']=ceil(intval(self::getMaxRowCustomersOrderFactory($search))/$countRow);
            //select all Product
            if(''==$search){
                $results['data'] = DB::select(DB::raw("
                SELECT *
                FROM customers_order
                WHERE customers_order.deleted_at is NULL AND (customers_order_status='approve' or customers_order_status='checkQuality')
                ORDER by customers_order.created_at DESC
                LIMIT :limit
                OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
            }else {
                $results['data'] = DB::select(DB::raw("
                SELECT *
                FROM customers_order
                WHERE customers_order.deleted_at is NULL AND (customers_order_status='approve' or customers_order_status='checkQuality') AND customers_order.customers_order_name LIKE '%" . $search . "%'
                ORDER by customers_order.created_at DESC
                LIMIT :limit
                OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
            }

        } catch (Exception $e) {
            echo $e->getLine() . ':' . $e->getMessage();
        }
        //return
        return $results;
    }

    public function getMaxRowCustomersOrderCheckQuality($search){
        //1. Variable declaration.
        $count=array();
        //2. get max row.
        if(''==$search){
            $count = DB::select(DB::raw("select count(*) from customers_order where customers_order.deleted_at is NULL AND customers_order_status='checkQuality'"));
        }else {
            $count = DB::select(DB::raw("select count(*) from customers_order WHERE customers_order.deleted_at is NULL AND customers_order_status='checkQuality' AND customers_order.customers_order_name LIKE '%" . $search . "%'"));
        }
        //3. Return
        return intval($count[0]->{'count(*)'});
    }

    public function findAllCustomersAndPaginateCheckQuality($countRow,$page,$search){
        $results=array();
        $offset=intval($page-1)*$countRow;
        $results['page']=$page;
        $results['search']=$search;
        try{
            //get max row
            $results['max']=ceil(intval(self::getMaxRowCustomersOrderCheckQuality($search))/$countRow);
            //select all Product
            if(''==$search){
                $results['data'] = DB::select(DB::raw("
                SELECT *
                FROM customers_order
                WHERE customers_order.deleted_at is NULL AND customers_order_status='checkQuality'
                ORDER by customers_order.created_at DESC
                LIMIT :limit
                OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
            }else {
                $results['data'] = DB::select(DB::raw("
                SELECT *
                FROM customers_order
                WHERE customers_order.deleted_at is NULL AND customers_order_status='checkQuality' AND customers_order.customers_order_name LIKE '%" . $search . "%'
                ORDER by customers_order.created_at DESC
                LIMIT :limit
                OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
            }

        } catch (Exception $e) {
            echo $e->getLine() . ':' . $e->getMessage();
        }
        //return
        return $results;
    }

    public function getMaxRowCustomersOrderTransport($search){
        //1. Variable declaration.
        $count=array();
        //2. get max row.
        if(''==$search){
            $count = DB::select(DB::raw("select count(*) from customers_order where customers_order.deleted_at is NULL AND (customers_order_status='deal' OR customers_order_status='plan' OR customers_order_status='checkQuality' OR customers_order_status='confirmation' OR customers_order_status='approve')"));
        }else {
            $count = DB::select(DB::raw("select count(*) from customers_order WHERE customers_order.deleted_at is NULL AND (customers_order_status='deal' OR customers_order_status='plan' OR customers_order_status='checkQuality' OR customers_order_status='confirmation' OR customers_order_status='approve') AND customers_order.customers_order_name LIKE '%" . $search . "%'"));
        }
        //3. Return
        return intval($count[0]->{'count(*)'});
    }

    public function findAllCustomersAndPaginateTransport($countRow,$page,$search){
        $results=array();
        $offset=intval($page-1)*$countRow;
        $results['page']=$page;
        $results['search']=$search;
        try{
            //get max row
            $results['max']=ceil(intval(self::getMaxRowCustomersOrderEngineer($search))/$countRow);
            //select all Product
            if(''==$search){
                $results['data'] = DB::select(DB::raw("
                SELECT *
                FROM customers_order
                WHERE customers_order.deleted_at is NULL AND (customers_order_status='deal' OR customers_order_status='plan' OR customers_order_status='checkQuality' OR customers_order_status='confirmation' OR customers_order_status='approve')
                ORDER by customers_order.created_at DESC
                LIMIT :limit
                OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
            }else {
                $results['data'] = DB::select(DB::raw("
                SELECT *
                FROM customers_order
                WHERE customers_order.deleted_at is NULL AND (customers_order_status='deal' OR customers_order_status='plan' OR customers_order_status='checkQuality' OR customers_order_status='confirmation' OR customers_order_status='approve') AND customers_order.customers_order_name LIKE '%" . $search . "%'
                ORDER by customers_order.created_at DESC
                LIMIT :limit
                OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
            }

        } catch (Exception $e) {
            echo $e->getLine() . ':' . $e->getMessage();
        }
        //return
        return $results;
    }

    public function getMaxRowCustomersOrderCompensate($search){
        //1. Variable declaration.
        $count=array();
        //2. get max row.
        if(''==$search){
            $count = DB::select(DB::raw("select count(*) from customers_order where customers_order.deleted_at is NULL AND (customers_order_status='plan' OR customers_order_status='checkQuality')"));
        }else {
            $count = DB::select(DB::raw("select count(*) from customers_order WHERE customers_order.deleted_at is NULL AND (customers_order_status='plan' OR customers_order_status='checkQuality') and customers_order.customers_order_name LIKE '%" . $search . "%'"));
        }
        //3. Return
        return intval($count[0]->{'count(*)'});
    }

    public function findAllCustomersAndPaginateCompensate($countRow,$page,$search){
        $results=array();
        $offset=intval($page-1)*$countRow;
        $results['page']=$page;
        $results['search']=$search;
        try{
            //get max row
            $results['max']=ceil(intval(self::getMaxRowCustomersOrderCompensate($search))/$countRow);
            //select all Product
            if(''==$search){
                $results['data'] = DB::select(DB::raw("
                SELECT *
                FROM customers_order
                WHERE customers_order.deleted_at is NULL AND (customers_order_status='plan' OR customers_order_status='checkQuality')
                ORDER by customers_order.created_at DESC
                LIMIT :limit
                OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
            }else {
                $results['data'] = DB::select(DB::raw("
                SELECT *
                FROM customers_order
                WHERE customers_order.deleted_at is NULL AND (customers_order_status='plan' OR customers_order_status='checkQuality') AND customers_order.customers_order_name LIKE '%" . $search . "%'
                ORDER by customers_order.created_at DESC
                LIMIT :limit
                OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
            }

        } catch (Exception $e) {
            echo $e->getLine() . ':' . $e->getMessage();
        }
        //return
        return $results;
    }

    public function GetSumCountProduct($id){
        $count=array();
        $count = DB::select(DB::raw("select sum(customers_order_product.customers_order_product_count) as sum
from customers_order_product
where customers_order_product.customers_order_id=".$id." and customers_order_product.deleted_at is NULL"));
        return intval($count[0]->{'sum'});
    }

    public function GetSumCountProductComplete($id){
        $count=array();
        $count = DB::select(DB::raw("select sum(customers_order_plan.customers_order_plan_complete) as sum
from customers_order_plan
where customers_order_plan.customers_order_plan_status='complete' and customers_order_plan.customers_order_id=".$id." and customers_order_plan.deleted_at is NULL"));
        return intval($count[0]->{'sum'});
    }

    public function GetSumPercentOrder($id){
        $countProduct=Self::GetSumCountProduct($id);
        $countProductComplete=Self::GetSumCountProductComplete($id);
           if(empty($countProductComplete)){
               return intval(0);
           }else {
               return intval(((($countProductComplete / $countProduct) * 100)));
           }
    }

    public function getMaxRowCustomersOrderStore($search){
        //1. Variable declaration.
        $count=array();
        //2. get max row.
        if(''==$search){
            $count = DB::select(DB::raw("select count(*) from customers_order where customers_order.deleted_at is NULL AND (customers_order_status='plan' OR customers_order_status='checkQuality' OR customers_order_status='confirmation' OR customers_order_status='approve')"));
        }else {
            $count = DB::select(DB::raw("select count(*) from customers_order WHERE customers_order.deleted_at is NULL AND (customers_order_status='plan' OR customers_order_status='checkQuality' OR customers_order_status='confirmation' OR customers_order_status='approve') AND customers_order.customers_order_name LIKE '%" . $search . "%'"));
        }
        //3. Return
        return intval($count[0]->{'count(*)'});
    }

    public function findAllCustomersAndPaginateStore($countRow,$page,$search){
        $results=array();
        $offset=intval($page-1)*$countRow;
        $results['page']=$page;
        $results['search']=$search;
        try{
            //get max row
            $results['max']=ceil(intval(self::getMaxRowCustomersOrderEngineer($search))/$countRow);
            //select all Product
            if(''==$search){
                $results['data'] = DB::select(DB::raw("
                SELECT *
                FROM customers_order
                WHERE customers_order.deleted_at is NULL AND (customers_order_status='plan' OR customers_order_status='checkQuality' OR customers_order_status='confirmation' OR customers_order_status='approve')
                ORDER by customers_order.created_at DESC
                LIMIT :limit
                OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
            }else {
                $results['data'] = DB::select(DB::raw("
                SELECT *
                FROM customers_order
                WHERE customers_order.deleted_at is NULL AND (customers_order_status='plan' OR customers_order_status='checkQuality' OR customers_order_status='confirmation' OR customers_order_status='approve') AND customers_order.customers_order_name LIKE '%" . $search . "%'
                ORDER by customers_order.created_at DESC
                LIMIT :limit
                OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
            }

        } catch (Exception $e) {
            echo $e->getLine() . ':' . $e->getMessage();
        }
        //return
        return $results;
    }

    public static function checkStatus($status){
        if($status=='create') {
           return'งานใหม่';
        }elseif($status=='deal'){
            return'อนุมัติผลิต';
        }elseif($status=='plan'){
            return'วางแผน';
        }elseif($status=='markOrder'){
            return'รอถอดเเบบ';
        }elseif($status=='checkQuality'){
            return'ส่งตรวจคุณภาพ';
        }elseif($status=='confirmation'){
            return'รออนุมัติแผน';
        }elseif($status=='approve'){
            return'แผนอนุมัติแล้ว';
        }elseif($status=='complete'){
            return'ปิดงาน';
        }
    }
}
