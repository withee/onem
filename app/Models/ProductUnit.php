<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 8/5/2558
 * Time: 17:04 น.
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductUnit extends Model{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'ref_product_units';
    protected $primaryKey = 'unit_id';

    public function deleteProductUnit($id){
        //1. Variable declaration.
        $result = array();
        $result['status']='';
        $result['desc']='';
        //2. find ById.
        $productUnit = ProductUnit::find($id);
        if(!empty($productUnit)) {
            $productUnit->delete();
        }else{
            $result['status']='error';
            $result['desc']='ไม่สามารถ ลบข้อมูลหน่วยสินค้า ได้';
        }
        //3. Return $result to controller.
        return $result;
    }

    public static function getNameUnit($id){
        //Find ById Companys.
        $results=ProductUnit::find($id);
        //return.
        return $results->ref_projects_activity_units_name;
    }

    public static function getNameUnitByProduct($id){
        $dataProduct=Product::find($id);
        //Find ById Companys.
        $results=ProductUnit::find($dataProduct->product_unit);
        //return.
        return $results->unit_name;
    }

    public static function getNameUnitByProductName($name){
        $dataProduct=Product::where('product_name','=',$name)->first();
        //Find ById Companys.
        if(!empty($dataProduct)) {
            $results = ProductUnit::find($dataProduct->product_unit);
            //return.
            if (!empty($results)) {
                return $results->unit_name;
            } else {
                return 'ไม่ข้อมูล';
            }
        }else{
            return '-';
        }
    }

    public function findByIdProductUnit($id)
    {
        //Find ById Companys.
        $results=ProductUnit::find($id);
        //return.
        return $results;
    }

    public function getMaxRow($search){
        //Count row companys.
        if(''==$search){
            $count = DB::select(DB::raw("select count(*) from ref_product_units WHERE ref_product_units.deleted_at is NULL "));
        }else {
            $count = DB::select(DB::raw("select count(*) from ref_product_units where ref_product_units.deleted_at is NULL AND ref_product_units.unit_name LIKE '%" . $search . "%'"));
        }
        //return.
        return intval($count[0]->{'count(*)'});
    }

    public function findAllProductUnitAndPaginate($countRow,$page,$search){
        //1. Variable declaration.
        $results=array();
        $offset=intval($page-1)*$countRow;
        $results['page']=$page;
        $results['search']=$search;
        //2.get max row
        $results['maxProduct']=ceil(intval(self::getMaxRow($search))/$countRow);
        //3.select company
        if($search!='') {
            $results['dataProductUnit'] = DB::select(DB::raw("
            SELECT ref_product_units.unit_id,ref_product_units.unit_name
            FROM ref_product_units
            WHERE ref_product_units.deleted_at is NULL AND ref_product_units.unit_name LIKE '%" . $search . "%'
            LIMIT :limit OFFSET :offset"),
                array('limit' => $countRow, 'offset' => $offset));
        }else{
            $results['dataProductUnit'] = DB::select(DB::raw("
            SELECT ref_product_units.unit_id,ref_product_units.unit_name
            FROM ref_product_units
            WHERE ref_product_units.deleted_at is NULL
            LIMIT :limit OFFSET :offset"),
                array('limit' => $countRow, 'offset' => $offset));
        }
        //4. return
        return $results;
    }



}