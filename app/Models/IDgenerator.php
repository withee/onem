<?php namespace App;
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class IDgenerator extends Model
{

    protected $table = 'idgenerators';
    protected $fillable = ['current_id'];

    //
    public function nextID()
    {
        //1. Variable declaration
        $id = 0;
        try {
            //2. Process
            $generator = IDgenerator::first();
            $id = (int)$generator->current_id;
            $generator->current_id = (int)$generator->current_id+1;
            //3. Use transaction before save
            DB::transaction(function () use ($generator) {
                $generator->save();
            });
        } catch (Exception $e) {
            echo $e->getLine() . ':' . $e->getMessage();
        }
        //4. Return current id
        return $id;
    }
}
