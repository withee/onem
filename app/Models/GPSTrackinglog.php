<?php
namespace App\Models;

use App\Gmap;
use Illuminate\Database\Eloquent\Model;


class GPSTrackinglog extends Model
{
    protected $table = 'gps_trackinglogs';

    public function getMap($vehicle, $currentdate, $showline, $showfreeze, $showstart)
    {
        //1. Check is show all vehicle or not
        $additioncondition = "";
        if (0 != (int)$vehicle) {
            $additioncondition = "AND v.id=$vehicle";
        }

        //2. Create query string
        $sql = "SELECT v.id,v.license_plate,v.represent_color,gps.latitude,gps.longitude,gps.id as tid,gps.parking
FROM vehicles v
LEFT JOIN gps_trackinglogs gps
	ON v.id=gps.vehicle
WHERE date(gps.created_at)='$currentdate'
$additioncondition
ORDER BY v.id,tid";
        //3. Process
        $logs = \DB::select(\DB::raw($sql));
        $gmap = new Gmap();
        $mapdata = $gmap->getMap($logs, $showline, $showfreeze, $showstart);

        //4. return
        return $mapdata;
    }

}
