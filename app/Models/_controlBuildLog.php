<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 8/5/2558
 * Time: 17:04 น.
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class ControlBuildLog extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'control_build_log';
    protected $primaryKey = 'control_build_log_id';

    public function findByBuildId($id)
    {
        $results=array();
        $results = DB::select(DB::raw("
            SELECT staffs.staff_firstname,staffs.staff_lastname,control_build_log.bring_id,control_build_log.control_build_log_type,control_build_log.created_at,control_build_log.control_build_log_detail
            FROM control_build_log
            INNER JOIN staffs on (staffs.staff_id=control_build_log.staff_id)
            WHERE control_build_id=:id"),
            array('id' => $id));
        return $results;
    }
}