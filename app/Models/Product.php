<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 8/5/2558
 * Time: 17:04 น.
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Symfony\Component\HttpKernel\HttpCache\Store;

class Product extends Model{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'products';
    protected $primaryKey = 'product_id';

    public function stores()
    {
        return $this->hasMany('Models\ProductStores', 'product_id','product_id');
    }

    public function findByIdProduct($id)
    {
        // Find ById Companys.
        $results=Product::find($id);
        //return.
        return $results;
    }

    public function deleteProduct($id)
    {
        //1. Variable declaration.
        $result = array();
        $result['status']='';
        $result['desc']='';
        //2. Find ByID.
        $product = Product::find($id);
        if(!empty($product)){
            $product->delete();
        }else {
            $result['status']='error';
            $result['desc']='ไม่สามารถ ลบข้อมูลสินค้าได้';
        }
        //3. Return $result to controller.
        return $result;
    }

    public function getMaxRowProductInventoryStore($idStore,$search){
        //get max row.
        if(''==$search){
            $count = DB::select(DB::raw("select count(*) from products INNER JOIN stores on (products.product_id=stores.product_id) where stores.storename_id= :idStore and products.deleted_at is NULL"), array('idStore' => $idStore));
        }else {
            $count = DB::select(DB::raw("select count(*) from products INNER JOIN stores on (products.product_id=stores.product_id) where products.deleted_at is NULL and stores.storename_id= :idStore and products.product_name LIKE '%" . $search . "%'"), array('idStore' => $idStore));
        }
        //Return
        return intval($count[0]->{'count(*)'});
    }

    public function findAllProductInventoryStoreAndPaginate($idStore,$countRow,$page,$search){
        //Variable declaration.
        $results=array();
        $offset=intval($page-1)*$countRow;
        $results['page']=$page;
        $results['id']=$idStore;
        $results['search']=$search;
        try {
            //get max row
            $results['maxProduct']=ceil(intval(self::getMaxRowProductInventoryStore($idStore,$search))/$countRow);
            //select all Product
            if(''==$search) {
                $results['dataProduct'] = DB::select(DB::raw("
                SELECT products.product_name,products.product_price,products.product_id,ref_product_units.unit_name,stores.balance_product
                FROM products
                INNER JOIN stores on (products.product_id=stores.product_id)
                INNER JOIN ref_product_units on (products.product_unit=ref_product_units.unit_id)
                WHERE stores.storename_id=:idStore and products.deleted_at is NULL
                LIMIT :limit OFFSET :offset"),
                    array('idStore' => $idStore, 'limit' => $countRow, 'offset' => $offset));
            }else{
                $results['dataProduct'] = DB::select(DB::raw("
                SELECT products.product_name,products.product_price,products.product_id,ref_product_units.unit_name,stores.balance_product
                FROM products
                INNER JOIN stores on (products.product_id=stores.product_id)
                INNER JOIN ref_product_units on (products.product_unit=ref_product_units.unit_id)
                WHERE products.deleted_at is NULL and stores.storename_id=:idStore and products.product_name LIKE '%" . $search . "%'
                LIMIT :limit OFFSET :offset"),
                    array('idStore' => $idStore, 'limit' => $countRow, 'offset' => $offset));
            }
        } catch (Exception $e) {
            echo $e->getLine() . ':' . $e->getMessage();
        }
        //return
        return $results;
    }

    public function getMaxRowProduct($search,$type=null){
        //1. Variable declaration.
        $count=array();
        //2. get max row.
        if(''==$search){
            if(!empty($type)){
                $count = DB::select(DB::raw("select count(*) from products where products.product_type='".$type."' and products.deleted_at is NULL"));
            }else {
                $count = DB::select(DB::raw("select count(*) from products where products.deleted_at is NULL"));
            }
        }else {
            if(!empty($type)){
                $count = DB::select(DB::raw("select count(*) from products WHERE products.product_type='".$type."' and products.deleted_at is NULL and products.product_name LIKE '%" . $search . "%'"));
            }else {
                $count = DB::select(DB::raw("select count(*) from products WHERE products.deleted_at is NULL and products.product_name LIKE '%" . $search . "%'"));
            }
        }
        //3. Return
        return intval($count[0]->{'count(*)'});
    }

    public function findAllProductAndPaginate($id,$countRow,$page,$search){
        //
        $store=new Stores();
        $results=array();
        $offset=intval($page-1)*$countRow;
        $results['page']=$page;
        $results['search']=$search;
        $results['id']=$id;
        try{
            //get max row
            $results['maxDataProduct']=ceil(intval(self::getMaxRowProduct($search))/$countRow);
            $results['maxDataProductMaterial']=ceil(intval(self::getMaxRowProduct($search,'material'))/$countRow);
            $results['maxDataProductManufacture']=ceil(intval(self::getMaxRowProduct($search,'manufacture'))/$countRow);
            $results['maxDataProductEquipment']=ceil(intval(self::getMaxRowProduct($search,'equipment'))/$countRow);
            //select all Product
            if(''==$search){
                $results['dataProduct'] = DB::select(DB::raw("
                SELECT products.minimum,products.product_type,products.product_name,products.product_price,products.product_id,ref_product_units.unit_name,products.deleted_at
                FROM products
                INNER JOIN ref_product_units on (products.product_unit=ref_product_units.unit_id)
                WHERE products.deleted_at is NULL
                ORDER by products.created_at DESC
                LIMIT :limit
                OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
                $results['dataProductMaterial'] = DB::select(DB::raw("
                SELECT products.minimum,products.product_type,products.product_name,products.product_price,products.product_id,ref_product_units.unit_name,products.deleted_at
                FROM products
                INNER JOIN ref_product_units on (products.product_unit=ref_product_units.unit_id)
                WHERE products.deleted_at is NULL and products.product_type='material'
                ORDER by products.created_at DESC
                LIMIT :limit
                OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
                $results['dataProductManufacture'] = DB::select(DB::raw("
                SELECT products.minimum,products.product_type,products.product_name,products.product_price,products.product_id,ref_product_units.unit_name,products.deleted_at
                FROM products
                INNER JOIN ref_product_units on (products.product_unit=ref_product_units.unit_id)
                WHERE products.deleted_at is NULL and products.product_type='manufacture'
                ORDER by products.created_at DESC
                LIMIT :limit
                OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
                $results['dataProductEquipment'] = DB::select(DB::raw("
                SELECT products.minimum,products.product_type,products.product_name,products.product_price,products.product_id,ref_product_units.unit_name,products.deleted_at
                FROM products
                INNER JOIN ref_product_units on (products.product_unit=ref_product_units.unit_id)
                WHERE products.deleted_at is NULL and products.product_type='equipment'
                ORDER by products.created_at DESC
                LIMIT :limit
                OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
            }else {
                $results['dataProduct'] = DB::select(DB::raw("
                SELECT products.minimum,products.product_type,products.product_name,products.product_price,products.product_id,ref_product_units.unit_name,products.deleted_at
                FROM products
                INNER JOIN ref_product_units on (products.product_unit=ref_product_units.unit_id)
                WHERE products.deleted_at is NULL AND products.product_name LIKE '%" . $search . "%'
                ORDER by products.created_at DESC
                LIMIT :limit
                OFFSET :offset"),
                array('limit' => $countRow, 'offset' => $offset));
                $results['dataProductMaterial'] = DB::select(DB::raw("
                SELECT products.minimum,products.product_type,products.product_name,products.product_price,products.product_id,ref_product_units.unit_name,products.deleted_at
                FROM products
                INNER JOIN ref_product_units on (products.product_unit=ref_product_units.unit_id)
                WHERE products.deleted_at is NULL AND products.product_name LIKE '%" . $search . "%' and products.product_type='material'
                ORDER by products.created_at DESC
                LIMIT :limit
                OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
                $results['dataProductManufacture'] = DB::select(DB::raw("
                SELECT products.minimum,products.product_type,products.product_name,products.product_price,products.product_id,ref_product_units.unit_name,products.deleted_at
                FROM products
                INNER JOIN ref_product_units on (products.product_unit=ref_product_units.unit_id)
                WHERE products.deleted_at is NULL AND products.product_name LIKE '%" . $search . "%' and products.product_type='manufacture'
                ORDER by products.created_at DESC
                LIMIT :limit
                OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
                $results['dataProductEquipment'] = DB::select(DB::raw("
                SELECT products.minimum,products.product_type,products.product_name,products.product_price,products.product_id,ref_product_units.unit_name,products.deleted_at
                FROM products
                INNER JOIN ref_product_units on (products.product_unit=ref_product_units.unit_id)
                WHERE products.deleted_at is NULL AND products.product_name LIKE '%" . $search . "%' and products.product_type='equipment'
                ORDER by products.created_at DESC
                LIMIT :limit
                OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
            }
            foreach($results['dataProduct'] as $key=>$value){
                $value->deleted_at=$store->findByProduct(''.$value->product_id);
            }
            foreach($results['dataProductMaterial'] as $key=>$value){
                $value->deleted_at=$store->findByProduct(''.$value->product_id);
            }
            foreach($results['dataProductManufacture'] as $key=>$value){
                $value->deleted_at=$store->findByProduct(''.$value->product_id);
            }
            foreach($results['dataProductEquipment'] as $key=>$value){
                $value->deleted_at=$store->findByProduct(''.$value->product_id);
            }
        } catch (Exception $e) {
            echo $e->getLine() . ':' . $e->getMessage();
        }
        //return
        return $results;
    }

    public static function GetNameProduct($id){
        $results=Product::find($id);
        return $results->product_name;
    }

    public static function GetIdProduct($name){
        $dataProduct=Product::where('product_name','=',$name)->first();
        return $dataProduct->product_id;
    }

    public static function GetPriceByNameProduct($name){
        $dataProduct=Product::where('product_name','=',$name)->first();
        if(!empty($dataProduct)) {
            return $dataProduct->product_price;
        }else{
            return 0;
        }
    }

}