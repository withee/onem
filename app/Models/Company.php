<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 8/5/2558
 * Time: 17:04 น.
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'companys';
    protected $primaryKey = 'company_id';

    public function deleteCompany($id){
        //1. Variable declaration.
        $result = array();
        $result['status']='';
        $result['desc']='';
        //2. Find ByID.
        $company = Company::find($id);
        if(!empty($company)) {
            $company->delete();
        }else{
            $result['status']='error';
            $result['desc']='ไม่สามารถ ลบข้อมูลตัวแทนจำหน่าย ได้';
        }
        //3. Return $result to controller.
        return $result;
    }

    public static function getNameCompany($id){
        $results=Company::find($id);
        //return.
        return $results->company_name;
    }

    public function findByIdCompany($id)
    {
        //Find ById Companys
        $results=Company::find($id);
        //return.
        return $results;
    }

    public function getMaxRow($search){
        //Count row companys.
        if(''==$search){
            $count = DB::select(DB::raw("select count(*) from companys WHERE companys.deleted_at is NULL"));
        }else {
            $count = DB::select(DB::raw("select count(*) from companys where companys.deleted_at is NULL and companys.company_name LIKE '%" . $search . "%'"));
        }
        //return.
        return intval($count[0]->{'count(*)'});
    }

    public function findAllCompanyPaginate($countRow,$page,$search){
        //1. Variable declaration.
        $results=array();
        $offset=intval($page-1)*$countRow;
        $results['page']=$page;
        $results['search']=$search;
        //2.get max row
        $results['maxProduct']=ceil(intval(self::getMaxRow($search))/$countRow);
        //3.select company
        if($search!='') {
            $results['dataCompanys'] = DB::select(DB::raw("
            SELECT *
            FROM companys
            WHERE companys.deleted_at is NULL and companys.company_name LIKE '%" . $search . "%'
            LIMIT :limit OFFSET :offset"),
                array('limit' => $countRow, 'offset' => $offset));
        }else{
            $results['dataCompanys'] = DB::select(DB::raw("
            SELECT *
            FROM companys
            WHERE companys.deleted_at is NULL
            LIMIT :limit OFFSET :offset"),
                array('limit' => $countRow, 'offset' => $offset));
        }
        //4. return
        return $results;
    }

}