<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class VehiclesMaintenance extends Model{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'vehicles_maintenance';
    protected $primaryKey = 'vehicles_maintenance_id';

}
?>