<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ObjectivePlan extends Model {


    protected $table = 'objective_plans';
    use SoftDeletes;

}
