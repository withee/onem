<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderManufacture extends Model{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'order_manufacture';
    protected $primaryKey = 'order_manufacture_id';

    public function GetProductBring($id){
        $data = DB::select(DB::raw("select order_manufacture.order_manufacture_id,order_manufacture.product_id as markproduct_id,order_manufacture_detail.product_id,sum(order_manufacture_detail.count) as count,sum(order_manufacture_detail.bring) as bring,sum(order_manufacture_detail.balance) as balance,order_manufacture_detail.storename_id
from order_manufacture
INNER JOIN order_manufacture_detail on (order_manufacture.order_manufacture_id=order_manufacture_detail.order_manufacture_id)
INNER JOIN products on (order_manufacture_detail.product_id=products.product_id)
where order_manufacture.project_id=".$id." and order_manufacture.order_manufacture_type='add' and order_manufacture_detail.`status`='approve' and order_manufacture_detail.deleted_at IS NULL and order_manufacture.deleted_at IS NULL
group by order_manufacture_detail.product_id
order by order_manufacture.order_manufacture_id ASC"));
        return $data;
    }

    public function GetCalculateBring($id){
        $data = DB::select(DB::raw("select products.product_name,sum(order_manufacture_detail.count) as total
from order_manufacture
INNER JOIN order_manufacture_detail on (order_manufacture.order_manufacture_id=order_manufacture_detail.order_manufacture_id)
INNER JOIN products on (order_manufacture_detail.product_id=products.product_id)
where order_manufacture_detail.deleted_at is null and order_manufacture.deleted_at is null and order_manufacture.project_id=".$id." and order_manufacture.order_manufacture_type='add'
GROUP BY order_manufacture_detail.product_id"));
        return $data;
    }
}
?>