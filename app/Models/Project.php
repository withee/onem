<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 8/5/2558
 * Time: 17:04 น.
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'projects';
    protected $primaryKey = 'project_id';

    public function getMaxRowProject($search)
    {
        //get max row.
        if ('' == $search) {
            $count = DB::select(DB::raw("select count(*) from projects WHERE deleted_at is NULL"));
        } else {
            $count = DB::select(DB::raw("select count(*) from projects WHERE  WHERE deleted_at is NULL and project_name LIKE '%" . $search . "%'"));
        }
        //Return
        return intval($count[0]->{'count(*)'});
    }

    public function findAllProjectAndPaginate($countRow, $page, $search)
    {
        //Variable declaration.
        $projectActivity = new ProjectsActivity();
        $projectProjectsActivityLog = new ProjectsActivityLog();
        $results = array();
        $offset = intval($page - 1) * $countRow;
        $results['page'] = $page;
        $results['search'] = $search;
        try {
            //get max row
            $results['maxProduct'] = ceil(intval(self::getMaxRowProject($search)) / $countRow);
            //select all Product
            if ('' == $search) {
                $results['dataProduct'] = DB::select(DB::raw("
                SELECT projects.project_id,projects.project_name,projects.deleted_at
                FROM projects
                WHERE deleted_at is NULL
                LIMIT :limit OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
            } else {
                $results['dataProduct'] = DB::select(DB::raw("
                SELECT projects.project_id,projects.project_name,projects.deleted_at
                FROM projects
                WHERE deleted_at is NULL AND project_name LIKE '%" . $search . "%'
                LIMIT :limit OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
            }
        } catch (Exception $e) {
            echo $e->getLine() . ':' . $e->getMessage();
        }

        foreach ($results['dataProduct'] as $key => $value) {
//            var_dump($value->deleted_at);
//            $value->updated_at=$projectProjectsActivityLog->findByProjectId($value->project_id);
            $value->deleted_at = $projectActivity->findByProjectId($value->project_id);
        }
        //return
        return $results;
    }

    public function autoCreate($project_id, $start, $stop)
    {
        if($start!=0 and $stop!=0) {
            $startfrag = explode('+', $start);
            $stopfrag = explode('+', $stop);
            $sp = new ProjectSub();
            $needsave = true;
            $maxdistance = 0;

            for ($i = (int)$startfrag[0]; $i <= (int)$stopfrag[0]; $i++) {
                $firstkm = 'กม.' . $i;
                if ($i == (int)$startfrag[0]) {
                    $firstkm .= "+" . $stopfrag[1];
                    $maxdistance = (int)$startfrag[1];
                } else {
                    $firstkm .= "+000";
                    $maxdistance = 0;
                }

                $lastkm = "กม.";
                if ($i == (int)$stopfrag[0]) {
                    $lastkm .= $i . "+" . $stopfrag[1];
                    $maxdistance = (int)$startfrag[1];
                } else {
                    $lastkm .= ($i + 1) . "+000";
                    $maxdistance = ($maxdistance == 0) ? 0 : $maxdistance;
                }
                if ($firstkm != $lastkm) {

                    $sp->autoCreated($project_id, "$firstkm - $lastkm", $maxdistance);
                }

            }
        }

    }
}