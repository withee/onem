<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 8/5/2558
 * Time: 17:04 น.
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Purchases extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'purchases';
    protected $primaryKey = 'purchases_id';

    public function getMaxRowPurchases($search){
        //get max row.
        if(''==$search){
            $count = DB::select(DB::raw("select count(*) from purchases WHERE deleted_at is NULL"));
        }else {
            $count = DB::select(DB::raw("select count(*) from purchases WHERE  WHERE deleted_at is NULL and purchases_number LIKE '%" . $search . "%'"));
        }
        //Return
        return intval($count[0]->{'count(*)'});
    }

    public function findAllPurchasesAndPaginate($countRow,$page,$search){
        //Variable declaration.
        $results=array();
        $offset=intval($page-1)*$countRow;
        $results['page']=$page;
        $results['search']=$search;
        try {
            //get max row
            $results['maxProduct']=ceil(intval(self::getMaxRowPurchases($search))/$countRow);
            //select all Product
            if(''==$search) {
                $results['dataProduct'] = DB::select(DB::raw("
                SELECT purchases_id,purchases_date,purchases_number,purchases_total,id_member_record,id_member_toget
                FROM purchases
                WHERE deleted_at is NULL
                LIMIT :limit OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
            }else{
                $results['dataProduct'] = DB::select(DB::raw("
                SELECT purchases_id,purchases_date,purchases_number,purchases_total,id_member_record,id_member_toget
                FROM purchases
                WHERE deleted_at is NULL AND purchases_number LIKE '%" . $search . "%'
                LIMIT :limit OFFSET :offset"),
                    array('limit' => $countRow, 'offset' => $offset));
            }
        } catch (Exception $e) {
            echo $e->getLine() . ':' . $e->getMessage();
        }
        //return
        return $results;
    }

    public function addProduct($id,$count,$productPrice,$store)
    {
        //1. Variable declaration.
        $selectProduct=array();
        $dataProduct=Product::find($id);
        $dataStore=Storename::find($store);
        //2. set Object.
        $selectProduct['id']=$id;
        $selectProduct['productName']=$dataProduct['product_name'];
        $selectProduct['productPrice']=$productPrice;
        $selectProduct['count']=$count;
        $selectProduct['store']=$store;
        $selectProduct['storeName']=$dataStore['storename_name'];
        $selectProduct['total']=$count*$productPrice;
        //3.return
        return $selectProduct;
    }
}