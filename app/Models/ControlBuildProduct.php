<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 8/5/2558
 * Time: 17:04 น.
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class ControlBuildProduct extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'control_build_product';
    protected $primaryKey = 'control_build_product_id';

    public function getProductlistBuildDefine($id){
        $results=array();
        $results = DB::select(DB::raw("
            SELECT products.product_id,products.product_name
            FROM control_build_product
            INNER JOIN products on (products.product_id=control_build_product.product_id)
            WHERE control_build_id=:id"),
            array('id' => $id));
        return $results;
    }

    public function findById($id)
    {
//        $Status=false;
        $dataReturn=array();
        $results=array();
        $resultsProduct = DB::select(DB::raw("
            SELECT products.product_name,control_build_product.product_id,control_build_product.control_build_product_count,control_build_product.control_build_product_type
            FROM control_build_product
            INNER JOIN products on (products.product_id=control_build_product.product_id)
            WHERE control_build_id=:id"),
            array('id' => $id));

        foreach($resultsProduct as $key=>$value){
            if(false==empty($results[$value->product_id])){;
                if(false==empty($results[$value->product_id][$value->control_build_product_type])){
                    $results[$value->product_id][$value->control_build_product_type]['count']+=$value->control_build_product_count;
                }else{
                    $results[$value->product_id][$value->control_build_product_type]['count']=$value->control_build_product_count;
                }
                $results[$value->product_id]['manufacture']['percent']=floor(($results[$value->product_id]['manufacture']['count']*100)/$results[$value->product_id]['define']['count']);
                $results[$value->product_id]['deteriorate']['percent']=floor(($results[$value->product_id]['deteriorate']['count']*100)/$results[$value->product_id]['define']['count']);
                if($results[$value->product_id]['manufacture']['percent']>=100){
                    $Status=true;
                }else{
                    $Status=false;
                }
            }else{
                $results[$value->product_id]['product_name']=$value->product_name;
                $results[$value->product_id]['control_build_product_count']=$value->control_build_product_count;
                $results[$value->product_id]['define']['count']=0;
                $results[$value->product_id]['manufacture']['count']=0;
                $results[$value->product_id]['deteriorate']['count']=0;
                $results[$value->product_id][$value->control_build_product_type]['count']+=$value->control_build_product_count;
                $results[$value->product_id]['manufacture']['percent']=floor(($results[$value->product_id]['manufacture']['count']*100)/$results[$value->product_id]['define']['count']);
                $results[$value->product_id]['deteriorate']['percent']=floor(($results[$value->product_id]['deteriorate']['count']*100)/$results[$value->product_id]['define']['count']);
                if($results[$value->product_id]['manufacture']['percent']>=100){
                    $Status=true;
                }else{
                    $Status=false;
                }
            }
        }
//        foreach($results as $key=>$value) {
//            var_dump($value);
//        }
//        exit;
        $dataReturn['status']=$Status;
        $dataReturn['data']=$results;
        return $dataReturn;
    }
}