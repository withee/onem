<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 8/5/2558
 * Time: 17:04 น.
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Storename extends Model{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'ref_store_name';
    protected $primaryKey = 'storename_id';

    public function deleteStoreName($id){
        //1. Variable declaration.
        $result = array();
        $result['status']='';
        $result['desc']='';

        //2. find ById.
        $storeName = Storename::find($id);
        if(!empty($storeName)) {
            $storeName->delete();
        }else{
            $result['status']='error';
            $result['desc']='ไม่สามารถ ลบข้อมูลหน่วยสินค้า ได้';
        }

        //3. Return $result to controller.
        return $result;
    }

    public function findByIdStoreName($id)
    {
        //Find ById Companys.
        $results=Storename::find($id);
        //return.
        return $results;
    }

    public function getMaxRow($search){
        // Count row companys.
        if(''==$search){
            $count = DB::select(DB::raw("select count(*) from ref_store_name where ref_store_name.deleted_at is NULL"));
        }else {
            $count = DB::select(DB::raw("select count(*) from ref_store_name where ref_store_name.deleted_at is NULL and ref_store_name.storename_name LIKE '%" . $search . "%'"));
        }
        //3.return.
        return intval($count[0]->{'count(*)'});
    }

    public function findAllStoreNameAndPaginate($countRow,$page,$search){
        //1. Variable declaration.
        $results=array();
        $offset=intval($page-1)*$countRow;
        $results['page']=$page;
        $results['search']=$search;

        //2.get max row
        $results['maxProduct']=ceil(intval(self::getMaxRow($search))/$countRow);
        //3.select company
        if($search!='') {
            $results['dataStoreName'] = DB::select(DB::raw("
            SELECT ref_store_name.storename_id,ref_store_name.storename_name
            FROM ref_store_name
            WHERE ref_store_name.deleted_at is NULL and ref_store_name.unit_name LIKE '%" . $search . "%'
            LIMIT :limit OFFSET :offset"),
                array('limit' => $countRow, 'offset' => $offset));
        }else{
            $results['dataStoreName'] = DB::select(DB::raw("
            SELECT ref_store_name.storename_id,ref_store_name.storename_name
            FROM ref_store_name
            WHERE ref_store_name.deleted_at is NULL
            LIMIT :limit OFFSET :offset"),
                array('limit' => $countRow, 'offset' => $offset));
        }
        //4. return
        return $results;
    }

    public static function GetNameStore($id) {
        $results=Storename::find($id);
        return $results->storename_name;
    }

    public static function getStoreName($id)
    {
        //Find ById Companys.
        $results=Storename::find($id);
        if(!empty($results)){
            return $results->storename_name;
        }
        //return.
//        return $results;
    }

}