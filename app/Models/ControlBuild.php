<?php
/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 8/5/2558
 * Time: 17:04 น.
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class ControlBuild extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'control_build';
    protected $primaryKey = 'control_build_id';

    public function allControlBuildClose()
    {
        $controlBuildProduct=new ControlBuildProduct();
        $controlBuildlog=new ControlBuildLog();
        $results = DB::select(DB::raw("SELECT * FROM control_build WHERE deleted_at is NOT NULL "));
        foreach($results as $key=>$value){
            $value->control_build_complete_date=$controlBuildProduct->findById($value->control_build_id);
            $value->lot_No=$controlBuildlog->findByBuildId($value->control_build_id);
        }
        return $results;
    }

    public function allControlBuild()
    {
        $controlBuildProduct=new ControlBuildProduct();
        $controlBuildlog=new ControlBuildLog();
        $results = DB::select(DB::raw("SELECT * FROM control_build WHERE deleted_at is NULL "));
        foreach($results as $key=>$value){
            $dateProduct=$controlBuildProduct->findById($value->control_build_id);
            $value->deleted_at=$dateProduct['status'];
            $value->control_build_complete_date=$dateProduct['data'];
            $value->lot_No=$controlBuildlog->findByBuildId($value->control_build_id);
        }
        return $results;
    }

    public function deleteControlBuild($id){
        //1. Variable declaration.
        $result = array();
        $result['status']='';
        $result['desc']='';
        //2. find ById.
//        $controlBuild = self::find($id);
        $controlBuild = ControlBuild::find($id);
        if(false==empty($controlBuild)) {
            $controlBuild->delete();
        }else{
            $result['status']='error';
            $result['desc']='ไม่สามารถ ลบข้อมูลหน่วยสินค้า ได้';
        }

        //3. Return $result to controller.
        return $result;
    }
}