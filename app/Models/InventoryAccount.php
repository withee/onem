<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class InventoryAccount extends Model {
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'inventory_accounts';
    protected $primaryKey = 'id';

    public function GetSumExpensesByProject($id){
        $sum = DB::select(DB::raw("select sum(expenses)AS Total from inventory_accounts where project_id=".$id));
        return intval($sum[0]->{'Total'});
    }

    public function GetSumReceiptsByProject($id){
        $sum = DB::select(DB::raw("select sum(income)AS Total from inventory_accounts where deleted_at IS NULL and project_id=".$id));
        return intval($sum[0]->{'Total'});
    }

    public function GetSumExpenses(){
        $sum = DB::select(DB::raw("select sum(expenses)AS Total from inventory_accounts where deleted_at IS NULL"));
        return intval($sum[0]->{'Total'});
    }

    public function GetSumReceipts(){
        $sum = DB::select(DB::raw("select sum(income)AS Total from inventory_accounts where deleted_at IS NULL"));
        return intval($sum[0]->{'Total'});
    }

    public function formatDate($date, $format) {
        $date = date_create($date);
        $date2 = date_format($date, $format);
        return $date2;
    }

    public function checkNumberOfDays($Year,$Month){
        $date =Carbon::now();
        $date->setDate($Year,$Month,1);
        return intval(date('t', strtotime($date->format('Y-m'))));
    }

    public function GetMonthSet($selectMonthSearch,$selectYearSearch){
        $datetimenow =Carbon::now();
        $datetimenow->setDate($selectYearSearch,$selectMonthSearch,1);
        $CountDayMonthNow=self::checkNumberOfDays(intval($datetimenow->format('Y')),intval($datetimenow->format('m')))-1;
        $dataReturn=array();
        $datetime =Carbon::now();
        $datetime->setDate(intval($datetimenow->format('Y')),intval($datetimenow->format('m')),1);
        $dataReturn['start']=$datetime->format('Y-m-d 00:00:00');
        $datetime->modify('+'.$CountDayMonthNow.' day');
        $dataReturn['end']=$datetime->format('Y-m-d 23:59:59');
        return $dataReturn;
    }

    public function getByMonthAndYear($Month,$Year){
        $results=array();
        $dataDate=self::GetMonthSet($Month,$Year);
        $string='select * from inventory_accounts where inventory_accounts.created_at>="'.$dataDate['start'].'" and inventory_accounts.created_at<="'.$dataDate['end'].'" order by inventory_accounts.updated_at DESC';
        $results = DB::select(DB::raw($string));
        return $results;
    }

    public function getByProjectId($id){
        $string='select * from inventory_accounts where deleted_at IS NULL and project_id="'.$id.'" order by inventory_accounts.updated_at DESC';
        $results = DB::select(DB::raw($string));
        return $results;
    }
}
