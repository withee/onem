<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductRentals extends Model {

    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'product_rentals';
    protected $primaryKey = 'product_rentals_id';

    public function formatDate($date, $format) {
        $date = date_create($date);
        $date2 = date_format($date, $format);
        return $date2;
    }

    public function checkNumberOfDays($Year,$Month){
        $date =Carbon::now();
        $date->setDate($Year,$Month,1);
        return intval(date('t', strtotime($date->format('Y-m'))));
    }

    public function GetMonthSet($selectMonthSearch,$selectYearSearch){
        $datetimenow =Carbon::now();
        $datetimenow->setDate($selectYearSearch,$selectMonthSearch,1);
        $CountDayMonthNow=self::checkNumberOfDays(intval($datetimenow->format('Y')),intval($datetimenow->format('m')))-1;
        $dataReturn=array();
        $datetime =Carbon::now();
        $datetime->setDate(intval($datetimenow->format('Y')),intval($datetimenow->format('m')),1);
        $dataReturn['start']=$datetime->format('Y-m-d 00:00:00');
        $datetime->modify('+'.$CountDayMonthNow.' day');
        $dataReturn['end']=$datetime->format('Y-m-d 23:59:59');
        return $dataReturn;
    }

    public function getByMonthAndYear($Month,$Year){
        $results=array();
        $dataDate=self::GetMonthSet($Month,$Year);
        $string='select * from product_rentals where deleted_at IS NULL and product_rentals.created_at>="'.$dataDate['start'].'" and product_rentals.created_at<="'.$dataDate['end'].'" order by product_rentals.created_at DESC';
        $results = DB::select(DB::raw($string));
        return $results;
    }
}
