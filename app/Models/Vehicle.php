<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vehicle extends Model
{
    protected $table = 'vehicles';
    use SoftDeletes;

    public function getByID($id)
    {
        //1. Variable declaration
        try {
            $vehicle = Vehicle::find($id);
        } catch (\Exception $e) {
            $msg = $e->getFile() . '|' . $e->getLine() . ':' . $e->getMessage();
            \Log::error($msg);
        }
        return $vehicle;
    }

    public function saveModel($model)
    {
        $rs = false;
        try {
            $rs = $model->save();
        } catch (\Exception $e) {
            $msg = $e->getFile() . '|' . $e->getLine() . ':' . $e->getMessage();
            \Log::error($msg);
        }
        return $rs;
    }


}
