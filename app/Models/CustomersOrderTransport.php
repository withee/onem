<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomersOrderTransport extends Model {
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'customers_order_transport';
    protected $primaryKey = 'customers_order_transport_id';
}
