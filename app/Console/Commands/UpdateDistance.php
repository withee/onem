<?php namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use App\Models;

class UpdateDistance extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'vehicle:distance';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        //
        $day = $this->option('day');
        $vehicles = Models\Vehicle::all()->toArray();
//        var_dump($vehicles);
//        exit;
        $round = 0;
        try {
            while (count($vehicles) > $round) {
                $lasrrecsql = "SELECT * FROM gps_trackinglogs
WHERE serial='{$vehicles[$round]['serial']}'
AND DATE(created_at)='$day'
AND distance_from_previous <>-1
ORDER BY id DESC
LIMIT 1";
                $lastrec = \DB::select(\DB::raw($lasrrecsql));
//                var_dump($lastrec);
//                exit;
                $sql = "SELECT id,latitude,longitude,created_at,serial,speed,distance_from_previous as distance FROM gps_trackinglogs
WHERE serial='{$vehicles[$round]['serial']}'
AND DATE(created_at)='$day'
#AND distance_from_previous<0";
                $tracklist = \DB::select(\DB::raw($sql));
                $trackindex = 0;
                while (count($tracklist) > $trackindex) {
                    $gpstrack = Models\GPSTrackinglog::find($tracklist[$trackindex]->id);
                    if ($trackindex == 0 && empty($lastrec)) {
                        $gpstrack->speed = 0.00;
                        $gpstrack->distance_from_previous = 0;
                        $gpstrack->save();
                    } else {
                        try {
                            if (array_key_exists($trackindex - 1,$tracklist)) {
//                                $origin = $tracklist[$trackindex - 1]->latitude . "," . $tracklist[$trackindex - 1]->longitude;
//                                $destination = $gpstrack->latitude . ',' . $gpstrack->longitude;
//                                $url = "http://maps.googleapis.com/maps/api/directions/json?origin=$origin&destination=$destination&language=th";
//                                echo $trackindex . ":" . $url . "\n";
//                                $json = file_get_contents($url);
//                                $distance = json_decode($json);
                                $start = Carbon::createFromFormat('Y-m-d H:i:s', $tracklist[$trackindex - 1]->created_at);
                                $stop = Carbon::createFromFormat('Y-m-d H:i:s', $gpstrack->created_at);
                                $timediff = $start->diffInSeconds($stop);
                                if ($timediff <= 0) {
                                    $timediff = 1;
                                }
                                //var_dump($distance);exit;
                                $lat1=$gpstrack->latitude;
                                $lng1=$gpstrack->longitude;
                                $lat2=$tracklist[$trackindex - 1]->latitude;
                                $lng2=$tracklist[$trackindex - 1]->longitude;
//                              $gpstrack->distance_from_previous = $distance->routes[0]->legs[0]->distance->value;
                                $gpstrack->distance_from_previous=$this->distanceCalculator($lat1,$lng1,$lat2,$lng2);
                                $speed = ($gpstrack->distance_from_previous / 1000.00) / ($timediff / 3600.00);
                                $speed = ($speed>150)?150:$speed;

                                echo $speed . " Km/h\n";
                                $gpstrack->speed = $speed;
                                $gpstrack->save();
                            } else {
//                                $origin = $lastrec[0]->latitude . "," . $lastrec[0]->longitude;
//                                $destination = $gpstrack->latitude . ',' . $gpstrack->longitude;
//                                $url = "http://maps.googleapis.com/maps/api/directions/json?origin=$origin&destination=$destination&language=th";
//                                echo $trackindex . ":" . $url . "\n";
//                                $json = file_get_contents($url);
//                                $distance = json_decode($json);
                                $start = Carbon::createFromFormat('Y-m-d H:i:s', $lastrec[0]->created_at);
                                $stop = Carbon::createFromFormat('Y-m-d H:i:s', $gpstrack->created_at);
                                $timediff = $start->diffInSeconds($stop);
                                if ($timediff <= 0) {
                                    $timediff = 1;
                                }
//                                //var_dump($distance);exit;
//                                $gpstrack->distance_from_previous = $distance->routes[0]->legs[0]->distance->value;
                                $lat1=$gpstrack->latitude;
                                $lng1=$gpstrack->longitude;
                                $lat2=$lastrec[0]->latitude;
                                $lng2=$lastrec[0]->longitude;

                                $gpstrack->distance_from_previous=$this->distanceCalculator($lat1,$lng1,$lat2,$lng2);
                                $speed = ($gpstrack->distance_from_previous / 1000.00) / ($timediff / 3600.00);
                                $speed = ($speed>150)?150:$speed;
                                echo $speed . " Km/h\n";
                                $gpstrack->speed = $speed;
                                $gpstrack->save();
                            }
                        } catch (\Exception $e) {
                            echo $e->getLine() . ':' . $e->getMessage() . "\n";
                            $trackindex--;

                        }
                    }
                    $trackindex++;
                }
                $round++;
            }
        } catch (\Exception $e) {
            echo $e->getLine() . ":" . $e->getMessage() . "\n";
        }

    }

    public function distanceCalculator($lat1, $lng1, $lat2, $lng2)
    {
        //convert degree to radian
        $R = 6373000; //earth radian in mater
        $lat1 = $this->deg2rad($lat1);
        $lat2 = $this->deg2rad($lat2);
        $lng1 = $this->deg2rad($lng1);
        $lng2 = $this->deg2rad($lng2);
        $dlng = $lng2 - $lng1;
        $dlat = $lat2 - $lat1;
        $a = pow(sin($dlat / 2), 2) + cos($lat1) * cos($lat2) * pow(sin($dlng / 2), 2);
        $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
        $d = $R * $c;
        return $d;

    }

    public function deg2rad($deg)
    {
        return $deg * (pi() / 180);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            //	['example', InputArgument::REQUIRED, 'An example argument.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['day', null, InputOption::VALUE_OPTIONAL, 'day of operate', date('Y-m-d')],
        ];
    }

}
