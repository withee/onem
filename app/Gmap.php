<?php namespace App;

use Illuminate\Database\Eloquent\Model;

//use Appitventures\Phpgmaps\Facades\Phpgmaps as Gmaps;

class Gmap extends Model
{

    public function getMap($traveldata, $showline, $showfreeze, $showstart)
    {
        //1. Declare variable
        $config['center'] = 'auto';
        $config['zoom'] = 'auto';
        \Gmaps::initialize($config);
        $polyline = array();
        $pointlist = array();
        $lastcoor = "";
        $nextline = false;
        $current_id = 0;
        $lastcolor = 'ff0000';
        $linelist = array();
        //maker: use while loop
        $key = 0;

        //2. Process
        //foreach ($traveldata as $key => $travel) {
        while ($key < count($traveldata)) {
            $travel = $traveldata[$key];
            //Collect all point of each car for draw line
            if (true == $showline) {
                $pointlist[] = "{$travel->latitude}, {$travel->longitude}";
                $polyline['points'][] = "{$travel->latitude}, {$travel->longitude}";
                $linelist[$travel->id]['points'][] = "{$travel->latitude}, {$travel->longitude}";
            }

            //Set first marker
            if (0 == $key && true == $showstart) {
                $marker = array();
                $marker['position'] = "{$travel->latitude}, {$travel->longitude}";
                $marker['icon'] = 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=S|' . $travel->represent_color . '|000000';
                \Gmaps::add_marker($marker);
            }

            //Set freeze marker
            //if ($lastcoor == "{$travel->latitude}, {$travel->longitude}" && true == $showfreeze) {
                if ($travel->parking>=6 && true == $showfreeze) {
                $marker = array();
                $marker['position'] = "{$travel->latitude}, {$travel->longitude}";
                $marker['icon'] = 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=P|' . $travel->represent_color . '|000000';
                \Gmaps::add_marker($marker);
            }

            //Set end point of old vehicle and set start point for new vehicle
            if (0 != $current_id && $current_id != $travel->id) {
                $marker = array();
                $marker['position'] = $lastcoor;
                $marker['icon'] = 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=C|' . $lastcolor . '|000000';
                \Gmaps::add_marker($marker);

                if (true == $showstart) {
                    $marker = array();
                    $marker['position'] = "{$travel->latitude}, {$travel->longitude}";
                    $marker['icon'] = 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=S|' . $travel->represent_color . '|000000';
                    \Gmaps::add_marker($marker);
                }
            }

            //Set last location,last color and last vehicle
            $lastcoor = "{$travel->latitude}, {$travel->longitude}";
            $lastcolor = $travel->represent_color;
            $current_id = $travel->id;

            //Set last marker
            if (count($traveldata) - 1 == $key) {
                $marker = array();
                $marker['position'] = $lastcoor;
                $marker['icon'] = 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=C|' . $lastcolor . '|000000';
                \Gmaps::add_marker($marker);
            }
            $key++;
        }


        //Draw line with while loop
        foreach ($linelist as $line) {
            \Gmaps::add_polyline($line);
        }

        //Return map
        $data['map'] = \Gmaps::create_map();
        return $data;
    }

}
