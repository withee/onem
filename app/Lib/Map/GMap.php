<?php
namespace App\Lib\Map;

use Illuminate\Database\Eloquent\Model;

class GMap extends Model
{
    public function getMap($traveldata)
    {
        //$this->load->library('googlemaps');
//        $config['center'] = 'auto';
//        $config['zoom'] = 'auto';
//        Gmaps::initialize($config);
//        $polyline = array();
//        $pointlist = array();
//        foreach ($traveldata as $travel) {
//            $marker = array();
//            $marker['position'] = "{$travel->lat}, {$travel->lng}";
//            Gmaps::add_marker($marker);
//            $pointlist[] = "{$travel->lat}, {$travel->lng}";
//        }
//        $polyline['points'] = $pointlist;
//        Gmaps::add_polyline($polyline);
//        $data['map'] = Gmaps::create_map();

        $config = array();
        $config['center'] = 'auto';
        $config['onboundschanged'] = 'if (!centreGot) {
            var mapCentre = map.getCenter();
            marker_0.setOptions({
                position: new google.maps.LatLng(mapCentre.lat(), mapCentre.lng())
            });
        }
        centreGot = true;';

        Gmaps::initialize($config);

        // set up the marker ready for positioning
        // once we know the users location
        $marker = array();
        Gmaps::add_marker($marker);
        $map = Gmaps::create_map();
        return $map;
    }
}
