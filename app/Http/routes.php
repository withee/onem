<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//
Route::get('/', function () {
    return view('index');
});
Route::get('/login', function () {
    return View::make('index');
});
//Staffs group.
Route::get('/staffs/manage', ['uses' => 'StaffController@openManage', 'middleware' => 'guest']);
Route::post('/staffs/add', ['uses' => 'StaffController@addStaff', 'middleware' => 'guest']);
Route::get('/staffs/add', ['uses' => 'StaffController@addStaff', 'middleware' => 'guest']);
Route::get('/staffs/edit', ['uses' => 'StaffController@editStaff', 'middleware' => 'guest']);
Route::post('/staffs/edit', ['uses' => 'StaffController@editStaff', 'middleware' => 'guest']);
Route::get('/staffs/displaybyid', ['uses' => 'StaffController@displayById', 'middleware' => 'guest']);
Route::get('/staffs/delete', ['uses' => 'StaffController@deleteById', 'middleware' => 'guest']);
Route::post('/staffs/login', 'StaffController@gotoLogin');
Route::get('/staffs/login', 'StaffController@gotoLogin');
Route::get('/staffs/addform', ['uses' => 'StaffController@openFormData', 'middleware' => 'guest']);
Route::get('/staffs/logout', 'StaffController@Logout');
//Projects group.
//Route::get('/projects/manage', ['uses' => 'ProjectController@displayAll', 'middleware' => 'guest']);

//Products group.
Route::get('/products/manage', ['uses' => 'ProductController@openManage', 'middleware' => 'guest']);
Route::get('/products/delete', ['uses' => 'ProductController@deleteById', 'middleware' => 'guest']);
Route::post('/products/validationManage', ['uses' => 'ProductController@addProduct', 'middleware' => 'guest']);
Route::post('/products/validationEditManage', ['uses' => 'ProductController@editProduct', 'middleware' => 'guest']);
Route::get('/products/search', ['uses' => 'ProductController@search', 'middleware' => 'guest']);
Route::get('/products/displaybyid', ['uses' => 'ProductController@displayById', 'middleware' => 'guest']);
Route::get('/products/productbystore', ['uses' => 'ProductController@findFromStore', 'middleware' => 'guest']);
Route::post('/products/fromallstore', ['uses' => 'ProductController@findFromAllStore', 'middleware' => 'guest']);

//ProductUnit group
Route::get('/productUnit/manage', ['uses' => 'ProductUnitController@openManage', 'middleware' => 'guest']);
Route::get('/productUnit/delete', ['uses' => 'ProductUnitController@deleteById', 'middleware' => 'guest']);
Route::post('/productUnit/validationManage', ['uses' => 'ProductUnitController@addProductUnit', 'middleware' => 'guest']);
Route::post('/productUnit/validationEditManage', ['uses' => 'ProductUnitController@editProductUnit', 'middleware' => 'guest']);
Route::get('/productUnit/search', ['uses' => 'ProductUnitController@search', 'middleware' => 'guest']);
Route::get('/productUnit/displaybyid', ['uses' => 'ProductUnitController@displayById', 'middleware' => 'guest']);

//StoreName group
Route::get('/storeName/manage', ['uses' => 'StorenameController@openManage', 'middleware' => 'guest']);
Route::get('/storeName/delete', ['uses' => 'StorenameController@deleteById', 'middleware' => 'guest']);
Route::post('/storeName/validationManage', ['uses' => 'StorenameController@addStoreName', 'middleware' => 'guest']);
Route::post('/storeName/validationEditManage', ['uses' => 'StorenameController@editStoreName', 'middleware' => 'guest']);
Route::get('/storeName/search', ['uses' => 'StorenameController@search', 'middleware' => 'guest']);
Route::get('/storeName/displaybyid', ['uses' => 'StorenameController@displayById', 'middleware' => 'guest']);

//Store group
Route::get('/store/view', ['uses' => 'StoreController@viewStore', 'middleware' => 'guest']);
Route::get('/store/search', ['uses' => 'StoreController@searchProduct', 'middleware' => 'guest']);
Route::get('/store/change', ['uses' => 'StoreController@changeStore', 'middleware' => 'guest']);
Route::post('/store/addLogStore', ['uses' => 'StoreController@addLogStore', 'middleware' => 'guest']);


//Assets group
Route::get('/assets/vehicles', ['uses' => 'AssetsController@openManage', 'middleware' => 'guest']);
Route::post('/assets/addvehicle', ['uses' => 'AssetsController@addVehicle', 'middleware' => 'guest']);
Route::get('/assets/getvehicle', ['uses' => 'AssetsController@getByID', 'middleware' => 'guest']);
Route::post('/assets/editvehicle', ['uses' => 'AssetsController@editVehicle', 'middleware' => 'guest']);
Route::get('/assets/deletevehicle', ['uses' => 'AssetsController@deleteVehicle', 'middleware' => 'guest']);

//Google maps
Route::get('/maps', ['uses' => 'GPSTrackingController@getAlllocation', 'middleware' => 'guest']);
Route::get('/speedchart', ['uses' => 'GPSTrackingController@getChartData', 'middleware' => 'guest']);
Route::get('/speedcharthour', ['uses' => 'GPSTrackingController@getChartDataInHour', 'middleware' => 'guest']);
Route::get('/map', function () {
    $config = array();
    $config['center'] = 'auto';
    $config['onboundschanged'] = 'if (!centreGot) {
            var mapCentre = map.getCenter();
            marker_0.setOptions({
                position: new google.maps.LatLng(mapCentre.lat(), mapCentre.lng())
            });
        }
        centreGot = true;';

    Gmaps::initialize($config);

    // set up the marker ready for positioning
    // once we know the users location
    $marker = array();
    Gmaps::add_marker($marker);

    $map = Gmaps::create_map();
    echo "<html><head><script type=\"text/javascript\">var centreGot = false;</script>" . $map['js'] . "</head><body>" . $map['html'] . "</body></html>";
});

//Company groupF
Route::get('/companys/manage', ['uses' => 'CompanyController@openManage', 'middleware' => 'guest']);
Route::get('/companys/delete', ['uses' => 'CompanyController@deleteById', 'middleware' => 'guest']);
Route::post('/companys/validationManage', ['uses' => 'CompanyController@addCompany', 'middleware' => 'guest']);
Route::post('/companys/validationEditManage', ['uses' => 'CompanyController@editCompany', 'middleware' => 'guest']);
Route::get('/companys/search', ['uses' => 'CompanyController@search', 'middleware' => 'guest']);
Route::get('/companys/displaybyid', ['uses' => 'CompanyController@displayById', 'middleware' => 'guest']);

//Purchases Group
Route::get('/purchases/manage', ['uses' => 'PurchasesController@openManage', 'middleware' => 'guest']);
Route::post('/purchases/addProduct', ['uses' => 'PurchasesController@addProduct', 'middleware' => 'guest']);
Route::get('/purchases/remove', ['uses' => 'PurchasesController@removeListProduct', 'middleware' => 'guest']);
Route::get('/purchases/viewProduct', ['uses' => 'PurchasesController@viewProduct', 'middleware' => 'guest']);
Route::post('/purchases/addPurchases', ['uses' => 'PurchasesController@addPurchases', 'middleware' => 'guest']);
Route::get('/purchases/viewPurchases', ['uses' => 'PurchasesController@viewPurchases', 'middleware' => 'guest']);
Route::get('/purchases/request', ['uses' => 'PurchasesController@PurchaseRequest', 'middleware' => 'guest']);
Route::get('/purchases/removePurchasesDetail', ['uses' => 'PurchasesController@removePurchasesDetail', 'middleware' => 'guest']);
Route::post('/purchases/prproduct', ['uses' => 'PurchasesController@getPRProductList', 'middleware' => 'guest']);
Route::post('/purchases/acceptpr', ['uses' => 'PurchasesController@acceptPR', 'middleware' => 'guest']);
Route::post('/purchases/delpr', ['uses' => 'PurchasesController@delPR', 'middleware' => 'guest']);
Route::post('/purchases/delprlist', ['uses' => 'PurchasesController@delPRList', 'middleware' => 'guest']);
Route::post('/purchases/getproduct', ['uses' => 'PurchasesController@getProductList', 'middleware' => 'guest']);
Route::post('/purchases/producttopr', ['uses' => 'PurchasesController@addProductToPR', 'middleware' => 'guest']);
Route::post('/purchases/addpr', ['uses' => 'PurchasesController@addPR', 'middleware' => 'guest']);
Route::get('/purchases/editPurchases', ['uses' => 'PurchasesController@editPurchases', 'middleware' => 'guest']);

//Bring Group ****
Route::get('/bring/viewProduct', ['uses' => 'BringController@openManage', 'middleware' => 'guest']);
Route::get('/bring/createBring', ['uses' => 'BringController@createBring', 'middleware' => 'guest']);
Route::post('/bring/addProduct', ['uses' => 'BringController@addProduct', 'middleware' => 'guest']);
Route::get('/bring/remove', ['uses' => 'BringController@removeListProduct', 'middleware' => 'guest']);
Route::post('/bring/addBring', ['uses' => 'BringController@addBring', 'middleware' => 'guest']);
Route::get('/bring/createPaperReturn', ['uses' => 'BringController@createPaperReturn', 'middleware' => 'guest']);
Route::get('/bring/createPaperBring', ['uses' => 'BringController@createPaperBring', 'middleware' => 'guest']);
Route::get('/bring/viewBring', ['uses' => 'BringController@viewBring', 'middleware' => 'guest']);
Route::get('/bring/viewPaperBring', ['uses' => 'BringController@viewPaperBring', 'middleware' => 'guest']);
Route::get('/bring/requisition', ['uses' => 'DocumentsController@downloadRequisition', 'middleware' => 'guest']);
Route::post('/bring/addProductDeteriorate', ['uses' => 'BringController@addProductDeteriorate', 'middleware' => 'guest']);

//return Group ****
Route::get('/return/viewReturn', ['uses' => 'ReturnController@viewReturn', 'middleware' => 'guest']);
Route::get('/return/viewProduct', ['uses' => 'ReturnController@openManage', 'middleware' => 'guest']);
Route::get('/return/createReturn', ['uses' => 'ReturnController@createReturn', 'middleware' => 'guest']);
Route::post('/return/addProduct', ['uses' => 'ReturnController@addProduct', 'middleware' => 'guest']);
Route::get('/return/remove', ['uses' => 'ReturnController@removeListProduct', 'middleware' => 'guest']);
Route::get('/return/removeReturnsDetail', ['uses' => 'ReturnController@removeReturnsDetail', 'middleware' => 'guest']);
Route::post('/return/addReturn', ['uses' => 'ReturnController@addReturn', 'middleware' => 'guest']);

//return ControlBuild ****
Route::post('/controlBuild/addProductBuild', ['uses' => 'ControlBuildController@addProductBuild', 'middleware' => 'guest']);
Route::get('/controlBuild/viewControlBuild', ['uses' => 'ControlBuildController@viewControlBuild', 'middleware' => 'guest']);
Route::get('/controlBuild/viewControlBuildClose', ['uses' => 'ControlBuildController@viewControlBuildClose', 'middleware' => 'guest']);
Route::get('/controlBuild/addProductDefine', ['uses' => 'ControlBuildController@addProductDefine', 'middleware' => 'guest']);
Route::get('/controlBuild/addBuildLog', ['uses' => 'ControlBuildController@addBuildLog', 'middleware' => 'guest']);
Route::post('/controlBuild/addControlBuild', ['uses' => 'ControlBuildController@addControlBuild', 'middleware' => 'guest']);
Route::post('/controlBuild/addProductManufacture', ['uses' => 'ControlBuildController@addProductManufacture', 'middleware' => 'guest']);
Route::get('/controlBuild/remove', ['uses' => 'ControlBuildController@remove', 'middleware' => 'guest']);
Route::post('/controlBuild/addProductDeteriorate', ['uses' => 'ControlBuildController@addProductDeteriorate', 'middleware' => 'guest']);

//return GroupBring Group *****
Route::get('/groupBring/viewGroupBring', ['uses' => 'GroupBringController@viewGroupBring', 'middleware' => 'guest']);
Route::post('/groupBring/addGroupBring', ['uses' => 'GroupBringController@addGroupBring', 'middleware' => 'guest']);
Route::get('/groupBring/remove', ['uses' => 'GroupBringController@remove', 'middleware' => 'guest']);

//return accounts Group *****
Route::post('/account/addAccount', ['uses' => 'AccountController@addAccount', 'middleware' => 'guest']);
Route::get('/account/remove', ['uses' => 'AccountController@remove', 'middleware' => 'guest']);
Route::get('/account/productRemove', ['uses' => 'AccountController@productRemove', 'middleware' => 'guest']);
Route::post('/account/search', ['uses' => 'AccountController@search', 'middleware' => 'guest']);
Route::post('/account/searchProduct', ['uses' => 'AccountController@searchProduct', 'middleware' => 'guest']);
Route::get('/account/manage', ['uses' => 'AccountController@manage', 'middleware' => 'guest']);
Route::get('/account/product', ['uses' => 'AccountController@product', 'middleware' => 'guest']);
Route::get('/account/rentals', ['uses' => 'AccountController@rentals', 'middleware' => 'guest']);
Route::post('/account/addProduct', ['uses' => 'AccountController@addProduct', 'middleware' => 'guest']);
Route::post('/account/addRentals', ['uses' => 'AccountController@addRentals', 'middleware' => 'guest']);
Route::post('/account/checkProduct', ['uses' => 'AccountController@checkProduct', 'middleware' => 'guest']);
Route::post('/account/checkEquipment', ['uses' => 'AccountController@checkEquipment', 'middleware' => 'guest']);
Route::post('/account/checkRepair', ['uses' => 'AccountController@checkRepair', 'middleware' => 'guest']);
Route::post('/account/checkManufacture', ['uses' => 'AccountController@checkManufacture', 'middleware' => 'guest']);
Route::get('/account/rentalsReturn', ['uses' => 'AccountController@rentalsReturn', 'middleware' => 'guest']);
Route::get('/account/rentalsRemove', ['uses' => 'AccountController@rentalsRemove', 'middleware' => 'guest']);
Route::post('/account/allReturn', ['uses' => 'AccountController@allReturn', 'middleware' => 'guest']);
Route::post('/account/allRepair', ['uses' => 'AccountController@allRepair', 'middleware' => 'guest']);
Route::post('/account/addRepair', ['uses' => 'AccountController@addRepair', 'middleware' => 'guest']);
Route::get('/account/rentalsDilapidated', ['uses' => 'AccountController@rentalsDilapidated', 'middleware' => 'guest']);
Route::get('/account/repair', ['uses' => 'AccountController@repair', 'middleware' => 'guest']);
Route::get('/account/repairRemove', ['uses' => 'AccountController@repairRemove', 'middleware' => 'guest']);
Route::get('/account/repairWaste', ['uses' => 'AccountController@repairWaste', 'middleware' => 'guest']);
Route::get('/account/repairReturn', ['uses' => 'AccountController@repairReturn', 'middleware' => 'guest']);
Route::post('/account/addProductPrice', ['uses' => 'AccountController@addProductPrice', 'middleware' => 'guest']);
Route::get('/account/listProductsAccounts', ['uses' => 'AccountController@listProductsAccounts', 'middleware' => 'guest']);
Route::get('/account/employeesDaily', ['uses' => 'AccountController@employeesDaily', 'middleware' => 'guest']);
Route::post('/account/addDaily', ['uses' => 'AccountController@addDaily', 'middleware' => 'guest']);
Route::get('/account/removeDaily', ['uses' => 'AccountController@removeDaily', 'middleware' => 'guest']);


//return Project Group *****
Route::post('/project/addProject', ['uses' => 'ProjectController@addProject', 'middleware' => 'guest']);
Route::post('/project/addSubProject', ['uses' => 'ProjectController@addSubProject', 'middleware' => 'guest']);
Route::post('/project/editSubProject', ['uses' => 'ProjectController@editSubProject', 'middleware' => 'guest']);
Route::post('/project/addActivity', ['uses' => 'ProjectController@addActivity', 'middleware' => 'guest']);
Route::post('/project/addProject', ['uses' => 'ProjectController@addProject', 'middleware' => 'guest']);
Route::post('/project/addActivityLog', ['uses' => 'ProjectController@addActivityLog', 'middleware' => 'guest']);
Route::post('/project/addActivityLogDetail', ['uses' => 'ProjectController@addActivityLogDetail', 'middleware' => 'guest']);
Route::get('/project/workTableChangeMonth', ['uses' => 'ProjectController@workTableChangeMonth', 'middleware' => 'guest']);
Route::get('/project/workChangeMonth', ['uses' => 'ProjectController@workChangeMonth', 'middleware' => 'guest']);
Route::get('/project/assignActivity', ['uses' => 'ProjectController@assignActivity', 'middleware' => 'guest']);
Route::get('/project/removeActivityLogDetail', ['uses' => 'ProjectController@removeActivityLogDetail', 'middleware' => 'guest']);
Route::get('/project/printPaperWork', ['uses' => 'ProjectController@printPaperWork', 'middleware' => 'guest']);
Route::get('/project/viewPaperWork', ['uses' => 'ProjectController@viewPaperWork', 'middleware' => 'guest']);
Route::get('/project/printPaperWorkProject', ['uses' => 'ProjectController@printPaperWorkProject', 'middleware' => 'guest']);
Route::get('/project/viewSubProject', ['uses' => 'ProjectController@viewSubProject', 'middleware' => 'guest']);
Route::get('/project/viewMainProject', ['uses' => 'ProjectController@viewMainProject', 'middleware' => 'guest']);
Route::get('/project/viewSendWork', ['uses' => 'ProjectController@viewSendWork', 'middleware' => 'guest']);
Route::get('/project/viewPlanWork', ['uses' => 'ProjectController@viewPlanWork', 'middleware' => 'guest']);
Route::get('/project/viewActivityLog', ['uses' => 'ProjectController@viewActivityLog', 'middleware' => 'guest']);
Route::get('/project/viewActivityWork', ['uses' => 'ProjectController@viewActivityWork', 'middleware' => 'guest']);
Route::get('/project/viewBring', ['uses' => 'ProjectController@viewBring', 'middleware' => 'guest']);
Route::get('/project/viewImageProjectSub', ['uses' => 'ProjectController@viewImageProjectSub', 'middleware' => 'guest']);
Route::get('/project/viewProject', ['uses' => 'ProjectController@viewProject', 'middleware' => 'guest']);
Route::get('/project/viewStaffWork', ['uses' => 'ProjectController@viewStaffWork', 'middleware' => 'guest']);
Route::get('/project/getChartBringProduct', ['uses' => 'ProjectController@getChartBringProduct', 'middleware' => 'guest']);
Route::get('/project/getChartActivitySubProject', ['uses' => 'ProjectController@getChartActivitySubProject', 'middleware' => 'guest']);
Route::post('/upload', ['uses' => 'FileController@upload', 'middleware' => 'guest']);
Route::get('/getimage', ['uses' => 'FileController@getImg', 'middleware' => 'guest']);
Route::get('/imgupload', function () {
    return view('project.imgupload');
});
Route::get('/project/orderManufacture', ['uses' => 'ProjectController@orderManufacture', 'middleware' => 'guest']);
Route::post('/project/addOrderManufacture', ['uses' => 'ProjectController@addOrderManufacture', 'middleware' => 'guest']);
Route::get('/project/RemoveOrderManufacture', ['uses' => 'ProjectController@RemoveOrderManufacture', 'middleware' => 'guest']);
Route::post('/project/addOrderManufactureDetail', ['uses' => 'ProjectController@addOrderManufactureDetail', 'middleware' => 'guest']);
Route::get('/project/GetStorkByProduct', ['uses' => 'ProjectController@GetStorkByProduct', 'middleware' => 'guest']);
Route::get('/project/GetOrderBring', ['uses' => 'ProjectController@GetOrderBring', 'middleware' => 'guest']);
Route::post('/project/ConfirmOrderManufactureDetail', ['uses' => 'ProjectController@ConfirmOrderManufactureDetail', 'middleware' => 'guest']);
Route::get('/project/bringOrderManufactureDetail', ['uses' => 'ProjectController@bringOrderManufactureDetail', 'middleware' => 'guest']);
Route::post('/project/BringProduct', ['uses' => 'ProjectController@BringProduct', 'middleware' => 'guest']);
Route::post('/project/addManufacture', ['uses' => 'ProjectController@addManufacture', 'middleware' => 'guest']);
Route::get('/project/reportProject', ['uses' => 'ProjectController@reportProject', 'middleware' => 'guest']);
Route::get('/project/printBring', ['uses' => 'ProjectController@printBring', 'middleware' => 'guest']);
Route::get('/project/removeOrderManufactureDetail', ['uses' => 'ProjectController@removeOrderManufactureDetail', 'middleware' => 'guest']);
Route::get('/project/calculateBring', ['uses' => 'ProjectController@calculateBring', 'middleware' => 'guest']);
//return vehicles *****
Route::get('/vehicle/companyMaintenance', ['uses' => 'VehicleController@companyMaintenance', 'middleware' => 'guest']);
Route::post('/vehicle/addCompanyMaintenance', ['uses' => 'VehicleController@addCompanyMaintenance', 'middleware' => 'guest']);
Route::get('/vehicle/removeCompanyMaintenance', ['uses' => 'VehicleController@removeCompanyMaintenance', 'middleware' => 'guest']);
Route::get('/vehicle/vehicleMaintenance', ['uses' => 'VehicleController@vehicleMaintenance', 'middleware' => 'guest']);
Route::post('/vehicle/addVehiclesMaintenance', ['uses' => 'VehicleController@addVehiclesMaintenance', 'middleware' => 'guest']);
Route::get('/vehicle/removeVehicleMaintenance', ['uses' => 'VehicleController@removeVehicleMaintenance', 'middleware' => 'guest']);
Route::get('/vehicle/vehiclesMaintenanceDetail', ['uses' => 'VehicleController@vehiclesMaintenanceDetail', 'middleware' => 'guest']);
Route::post('/vehicle/addVehiclesMaintenanceDetail', ['uses' => 'VehicleController@addVehiclesMaintenanceDetail', 'middleware' => 'guest']);
Route::get('/vehicle/removeVehiclesMaintenanceDetail', ['uses' => 'VehicleController@removeVehiclesMaintenanceDetail', 'middleware' => 'guest']);
Route::get('/vehicle/payVehiclesMaintenanceDetail', ['uses' => 'VehicleController@payVehiclesMaintenanceDetail', 'middleware' => 'guest']);
Route::get('/vehicle/clearMaintenanceDetail', ['uses' => 'VehicleController@clearMaintenanceDetail', 'middleware' => 'guest']);

//return Factory *****
Route::get('/factory/manageCustomers', ['uses' => 'FactoryController@manageCustomers', 'middleware' => 'guest']);
Route::get('/factory/checkMaterial', ['uses' => 'FactoryController@checkMaterial', 'middleware' => 'guest']);
Route::get('/factory/PlanManufacture', ['uses' => 'FactoryController@PlanManufacture', 'middleware' => 'guest']);
Route::get('/factory/transfer', ['uses' => 'FactoryController@transfer', 'middleware' => 'guest']);
Route::get('/factory/addOrder', ['uses' => 'FactoryController@addOrder', 'middleware' => 'guest']);
Route::get('/factory/PR', ['uses' => 'FactoryController@PR', 'middleware' => 'guest']);
Route::get('/factory/MarkOrder', ['uses' => 'FactoryController@MarkOrder', 'middleware' => 'guest']);
Route::get('/factory/MarkOrderEngineer', 'FactoryController@MarkOrderEngineer');
Route::get('/factory/MarkOrderFactory', ['uses' => 'FactoryController@MarkOrderFactory', 'middleware' => 'guest']);
Route::get('/factory/MarkOrderManager', ['uses' => 'FactoryController@MarkOrderManager', 'middleware' => 'guest']);
Route::get('/factory/MarkOrderCheckQuality', ['uses' => 'FactoryController@MarkOrderCheckQuality', 'middleware' => 'guest']);
Route::get('/factory/MarkOrderCompensate', ['uses' => 'FactoryController@MarkOrderCompensate', 'middleware' => 'guest']);
Route::get('/factory/MarkOrderStore', ['uses' => 'FactoryController@MarkOrderStore', 'middleware' => 'guest']);
Route::get('/factory/ApproveManufacture', ['uses' => 'FactoryController@ApproveManufacture', 'middleware' => 'guest']);
Route::get('/factory/CheckQuality', ['uses' => 'FactoryController@CheckQuality', 'middleware' => 'guest']);
Route::get('/factory/Compensate', ['uses' => 'FactoryController@Compensate', 'middleware' => 'guest']);
Route::get('/factory/Transport', ['uses' => 'FactoryController@Transport', 'middleware' => 'guest']);
Route::get('/factory/ReportTransport', ['uses' => 'FactoryController@ReportTransport', 'middleware' => 'guest']);
Route::get('/factory/BringManufacture', ['uses' => 'FactoryController@BringManufacture', 'middleware' => 'guest']);
Route::get('/factory/BringManufactureStore', ['uses' => 'FactoryController@BringManufactureStore', 'middleware' => 'guest']);
Route::post('/factory/addCustomers', ['uses' => 'FactoryController@addCustomers', 'middleware' => 'guest']);
Route::post('/factory/searchCustomers', ['uses' => 'FactoryController@searchCustomers', 'middleware' => 'guest']);
Route::get('/factory/deleteCustomers', ['uses' => 'FactoryController@deleteCustomers', 'middleware' => 'guest']);
Route::post('/factory/checkCustomers', ['uses' => 'FactoryController@checkCustomers', 'middleware' => 'guest']);
Route::post('/factory/createOrder', ['uses' => 'FactoryController@createOrder', 'middleware' => 'guest']);
Route::post('/factory/createOrderOffice', ['uses' => 'FactoryController@createOrderOffice', 'middleware' => 'guest']);
Route::post('/factory/editOrder', ['uses' => 'FactoryController@editOrder', 'middleware' => 'guest']);
Route::post('/factory/searchCustomersOrder', ['uses' => 'FactoryController@searchCustomersOrder', 'middleware' => 'guest']);
Route::get('/factory/deleteCustomersOrder', ['uses' => 'FactoryController@deleteCustomersOrder', 'middleware' => 'guest']);
Route::post('/factory/checkProductManufacture', ['uses' => 'FactoryController@checkProductManufacture', 'middleware' => 'guest']);
Route::post('/factory/checkProductMaterial', ['uses' => 'FactoryController@checkProductMaterial', 'middleware' => 'guest']);
Route::post('/factory/addProductOrder', ['uses' => 'FactoryController@addProductOrder', 'middleware' => 'guest']);
Route::post('/factory/addProductOrderCompensate', ['uses' => 'FactoryController@addProductOrderCompensate', 'middleware' => 'guest']);
Route::post('/factory/addProductOrderEngineer', ['uses' => 'FactoryController@addProductOrderEngineer', 'middleware' => 'guest']);
Route::post('/factory/addProductOrderCheckMaterial', ['uses' => 'FactoryController@addProductOrderCheckMaterial', 'middleware' => 'guest']);
Route::get('/factory/deleteCustomersOrderProduct', ['uses' => 'FactoryController@deleteCustomersOrderProduct', 'middleware' => 'guest']);
Route::get('/factory/deleteCustomersOrderProductCompensate', ['uses' => 'FactoryController@deleteCustomersOrderProductCompensate', 'middleware' => 'guest']);
Route::post('/factory/addMaterialOrder', ['uses' => 'FactoryController@addMaterialOrder', 'middleware' => 'guest']);
Route::post('/factory/addMaterialOrderCompensate', ['uses' => 'FactoryController@addMaterialOrderCompensate', 'middleware' => 'guest']);
Route::get('/factory/deleteCustomersOrderMaterial', ['uses' => 'FactoryController@deleteCustomersOrderMaterial', 'middleware' => 'guest']);
Route::get('/factory/deleteCustomersOrderMaterialCompensate', ['uses' => 'FactoryController@deleteCustomersOrderMaterialCompensate', 'middleware' => 'guest']);
Route::get('/factory/deleteCustomersOrderMarkOrder', ['uses' => 'FactoryController@deleteCustomersOrderMarkOrder', 'middleware' => 'guest']);
Route::post('/factory/searchCustomersOrderMarkOrder', ['uses' => 'FactoryController@searchCustomersOrderMarkOrder', 'middleware' => 'guest']);
Route::get('/factory/confirmDealCustomersOrder', ['uses' => 'FactoryController@confirmDealCustomersOrder', 'middleware' => 'guest']);
Route::get('/factory/checkMaterialMarkOrder', ['uses' => 'FactoryController@checkMaterialMarkOrder', 'middleware' => 'guest']);
Route::post('/factory/searchMarkOrderEngineer', ['uses' => 'FactoryController@searchMarkOrderEngineer', 'middleware' => 'guest']);
Route::post('/factory/addCustomersOrderPlan', ['uses' => 'FactoryController@addCustomersOrderPlan', 'middleware' => 'guest']);
Route::post('/factory/addCustomersOrderPlanMaterial', ['uses' => 'FactoryController@addCustomersOrderPlanMaterial', 'middleware' => 'guest']);
Route::get('/factory/getOrderMaterial', ['uses' => 'FactoryController@getOrderMaterial', 'middleware' => 'guest']);
Route::get('/factory/sendCheckPlan', ['uses' => 'FactoryController@sendCheckPlan', 'middleware' => 'guest']);
Route::post('/factory/addCustomersOrderPlanMaterialManage', ['uses' => 'FactoryController@addCustomersOrderPlanMaterialManage', 'middleware' => 'guest']);
Route::get('/factory/deletePlanMaterial', ['uses' => 'FactoryController@deletePlanMaterial', 'middleware' => 'guest']);
Route::get('/factory/deletePlanManage', ['uses' => 'FactoryController@deletePlanManage', 'middleware' => 'guest']);
Route::get('/factory/deletePlanEngineer', ['uses' => 'FactoryController@deletePlanEngineer', 'middleware' => 'guest']);
Route::get('/factory/sendCheckApprove', ['uses' => 'FactoryController@sendCheckApprove', 'middleware' => 'guest']);
Route::get('/factory/markOrderCustomersOrder', ['uses' => 'FactoryController@markOrderCustomersOrder', 'middleware' => 'guest']);
Route::get('/factory/PrintBringManufacture', ['uses' => 'FactoryController@PrintBringManufacture', 'middleware' => 'guest']);
Route::get('/factory/PrintBringManufactureStore', ['uses' => 'FactoryController@PrintBringManufactureStore', 'middleware' => 'guest']);
Route::get('/factory/checkQualityManufacture', ['uses' => 'FactoryController@checkQualityManufacture', 'middleware' => 'guest']);
Route::get('/factory/checkQualityManufactureAll', ['uses' => 'FactoryController@checkQualityManufactureAll', 'middleware' => 'guest']);
Route::post('/factory/confirmCheckQuality', ['uses' => 'FactoryController@confirmCheckQuality', 'middleware' => 'guest']);
Route::post('/factory/createTransport', ['uses' => 'FactoryController@createTransport', 'middleware' => 'guest']);
Route::post('/factory/addWasteTransport', ['uses' => 'FactoryController@addWasteTransport', 'middleware' => 'guest']);
Route::post('/factory/addReturnTransport', ['uses' => 'FactoryController@addReturnTransport', 'middleware' => 'guest']);
Route::post('/factory/addCompleteTransport', ['uses' => 'FactoryController@addCompleteTransport', 'middleware' => 'guest']);
Route::get('/factory/printTransport', ['uses' => 'FactoryController@printTransport', 'middleware' => 'guest']);
Route::get('/factory/closeCustomersOrder', ['uses' => 'FactoryController@closeCustomersOrder', 'middleware' => 'guest']);
Route::get('/factory/ConfirmBringMaterial', ['uses' => 'FactoryController@ConfirmBringMaterial', 'middleware' => 'guest']);
Route::get('/factory/PrintQuotation', ['uses' => 'FactoryController@PrintQuotation', 'middleware' => 'guest']);
Route::get('/factory/PrintDeposits', ['uses' => 'FactoryController@PrintDeposits', 'middleware' => 'guest']);
//return PR *****
Route::get('/Pr/deletePrOrder', ['uses' => 'PrController@deletePrOrder', 'middleware' => 'guest']);
Route::get('/Pr/listPr', ['uses' => 'PrController@listPr', 'middleware' => 'guest']);
Route::post('/Pr/createPr', ['uses' => 'PrController@createPr', 'middleware' => 'guest']);
Route::get('/Pr/listPrOrder', ['uses' => 'PrController@listPrOrder', 'middleware' => 'guest']);
Route::post('/Pr/addPrMaterial', ['uses' => 'PrController@addPrMaterial', 'middleware' => 'guest']);
Route::get('/Pr/deletePrOrderDetail', ['uses' => 'PrController@deletePrOrderDetail', 'middleware' => 'guest']);
Route::get('/Pr/confirmPrOrder', ['uses' => 'PrController@confirmPrOrder', 'middleware' => 'guest']);
Route::get('/Pr/PrintPr', ['uses' => 'PrController@PrintPr', 'middleware' => 'guest']);
Route::get('/Pr/PrintPo', ['uses' => 'PrController@PrintPo', 'middleware' => 'guest']);
Route::post('/Pr/addPrice', ['uses' => 'PrController@addPrice', 'middleware' => 'guest']);
