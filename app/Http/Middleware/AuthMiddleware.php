<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;

class AuthMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user = Session::get('user');
        if (true == empty($user)) {
            return view('index');
        }elseif(true == preg_match("/staffs/", $request->getPathInfo())){
            if($user->staff_usertype !=1){
                return view('layouts.forbidden');
            }else{
                return $next($request);
            }

        } else if ($user->staff_usertype > 6) {
            if (true == preg_match("/store/", $request->getPathInfo())) {
                return view('layouts.forbidden');
            }elseif(true == preg_match("/controlBuild/", $request->getPathInfo())){
                return view('layouts.forbidden');
            } else {
                return $next($request);
            }

        } else {
            return $next($request);
        }
    }

}
