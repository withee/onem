<?php namespace App\Http\Controllers;
/**
 * Created by PhpStorm.
 * User: sootipong
 * Date: 5/26/2015
 * Time: 3:48 AM
 */
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models;
use Illuminate\Support\Facades\Response;
use Request;
use Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class ReturnController extends Controller
{
    public function viewReturn(){
        $id=Input::get('id',0);
        $search = Input::get('search', '');
        $page = Input::get('page', 1);
        $countRow = 30;
        $Return=array();
        $return=new Models\Returns();
        $dataReturn=$return->findAllReturnAndPaginate($countRow,$page,$search);
        $Return=Models\Returns::find($id);
        $dataStaff = Models\Staffs::all();
        $dataBring=Models\Bring::all();
        //3. Return to view
        return view('return.viewReturn', array('Return'=>$Return,'dataBring'=>$dataBring,'dataReturn'=>$dataReturn,'dataStaff' => $dataStaff));
    }

    public function removeReturnsDetail(){
        $id=Input::get('id',0);
        $store=new Models\Stores();
        $returnsDetail=Models\ReturnDetail::find($id);
        if(false==empty($returnsDetail)){
            $store->updateStore($returnsDetail->product_id, $returnsDetail->storename_id, $returnsDetail->return_detail_count, 'plus');
            $returnsDetail->delete();
            return Redirect::to('/return/viewProduct?id='.$returnsDetail->return_id);
        }
    }

    public function removeListProduct(){
        //1. Get input  and declare variable
        $id=Input::get('id',0);
        $listProduct=array();
        if (Session::has('listProduct') ) {
            $listProduct = Session::get('listProduct');
        }

        //2. Process
        if(false==empty($listProduct[$id])){
            array_splice($listProduct, $id, 1);
            Session::set('listProduct', $listProduct);
            $dataProduct=Models\Product::all();
            $dataStoreName=Models\Storename::all();
            $dataStaff=Models\Staffs::all();
            $dataBring=Models\Bring::all();
            //3. Return to view
            return view('return.manage', array('dataBring'=>$dataBring,'dataStaff'=>$dataStaff,'dataStoreName'=>$dataStoreName,'dataProduct'=>$dataProduct,'listProduct'=>$listProduct));
        }else{
            //false Redirect
            return Redirect::to('/return/openManage')->with('error', 'ไม่สามารถ แก้ไขข้อมูลสินค้า สำเร็จ');
        }
    }

    public function openManage()
    {
        //1. Get input  and declare variable
        $returnDetail=new Models\ReturnDetail();
        $bringDetail=new Models\BringDetail();
        $id = Input::get('id', 0);
        $listProduct=array();

        // Process
        $dataProduct = Models\Product::all();
        $dataStoreName = Models\Storename::all();
        $dataStaff = Models\Staffs::all();
        $dataBring=Models\Bring::all();

        if (false==empty($id) ) {
            $dataReturn=Models\Returns::find($id);
            $dataBringId=Models\Bring::where('bring_number','=',$dataReturn->bring_number)->first();
            if(false==empty($dataBringId)) {
                $dataProduct = $bringDetail->findProductByBringId($dataBringId->bring_id);
            }
            $listProduct=$returnDetail->findById($id);
        }
        //3. Return to view
        return view('return.manage', array('dataReturn'=>$dataReturn,'dataBring'=>$dataBring,'id'=>$id,'listProduct'=>$listProduct,'dataStaff' => $dataStaff, 'dataStoreName' => $dataStoreName,'dataProduct' => $dataProduct));
    }

    public function createReturn()
    {
        //1. Get input  and declare variable
        $listProduct=array();
        if (Session::has('listProduct') ) {
            $listProduct = Session::get('listProduct');

        }
        //2. Process
        $dataProduct=Models\Product::all();
        $dataStoreName=Models\Storename::all();
        $dataStaff=Models\Staffs::all();
        $dataBring=Models\Bring::all();
        //3. Return to view
        return view('return.manage', array('dataBring'=>$dataBring,'dataStaff'=>$dataStaff,'dataStoreName'=>$dataStoreName,'dataProduct'=>$dataProduct,'listProduct'=>$listProduct));
    }

    public function addReturn(){
        //Get input  and declare variable
        $store=new Models\Stores();
        $return_id = Input::get('return_id', 0);
        $return_number = Input::get('return_number', 0);
        $bring_number = Input::get('bring_number', 0);
        $return_type_manufacture = Input::get('return_type_manufacture', false);
        $return_type_project = Input::get('return_type_project', false);
        $return_type_other = Input::get('return_type_other', false);
        $id_member_return = Input::get('id_member_return', 0);
        $id_member_approve_return= Input::get('id_member_approve_return', 0);
        $id_member_approve_pay= Input::get('id_member_approve_pay', 0);
        $id_member_store= Input::get('id_member_store', 0);
        $id_member_account= Input::get('id_member_account', 0);
        $id_member_driver= Input::get('id_member_driver', 0);
        $member_return_date= Input::get('member_return_date', '');
        $member_approve_return_date= Input::get('member_approve_return_date', '');
        $member_approve_pay_date= Input::get('member_approve_pay_date', '');
        $member_store_date= Input::get('member_store_date', '');
        $member_account_date= Input::get('member_account_date', '');
        $member_driver_date= Input::get('member_driver_date', '');
        $return_vehicles_id= Input::get('return_vehicles_id', 0);
        $return_date= Input::get('return_date', '');
        $return_depertment= Input::get('return_depertment', '');
        $return_detail= Input::get('return_detail', '');
        $return_note= Input::get('return_note', '');
        //set rules
        $rules = array(
            'bring_number' => 'required',
            'return_number' => 'required',
            'return_date' => 'required',
            'member_return_date' => 'required',
            'id_member_return' => 'required',
        );
        //validate input
        $validator = Validator::make(Input::all(), $rules);
        //check validate
        if ($validator->fails()) {
            // false validator Redirect
            return Redirect::to('/return/viewReturn')
                ->withErrors($validator)
                ->withInput(Input::except('id_member_return','return_date','member_return_date','return_number'));
        } else {
            //true validator save data purchases
            //create Bring
            if(false==empty($return_id)){
                $newReturn=Models\Returns::find($return_id);
            }else {
                $newReturn = new Models\Returns();
            }
            $newReturn->return_number=$return_number;
            if(false==empty($return_type_manufacture)){
                $newReturn->return_type_manufacture='Y';
            }
            if(false==empty($return_type_project)){
                $newReturn->return_type_project='Y';
            }
            if(false==empty($return_type_other)){
                $newReturn->return_type_other='Y';
            }
            $staff = Session::get('user');
            $newReturn->id_member_record=$staff->staff_id;
            $newReturn->bring_number=(false==empty($bring_number)?$bring_number:null);
            $newReturn->id_member_return=(false==empty($id_member_return)?$id_member_return:null);
            $newReturn->id_member_approve_return=(false==empty($id_member_approve_return)?$id_member_approve_return:null);
            $newReturn->id_member_approve_pay=(false==empty($id_member_approve_pay)?$id_member_approve_pay:null);
            $newReturn->id_member_store=(false==empty($id_member_store)?$id_member_store:null);
            $newReturn->id_member_account=(false==empty($id_member_account)?$id_member_account:null);
            $newReturn->id_member_driver=(false==empty($id_member_driver)?$id_member_driver:null);
            $newReturn->member_return_date= (false==empty($member_return_date)?$member_return_date:null);
            $newReturn->member_approve_return_date=(false==empty($member_approve_return_date)?$member_approve_return_date:null);
            $newReturn->member_approve_pay_date=(false==empty($member_approve_pay_date)?$member_approve_pay_date:null);
            $newReturn->member_store_date=(false==empty($member_store_date)?$member_store_date:null);
            $newReturn->member_account_date=(false==empty($member_account_date)?$member_account_date:null);
            $newReturn->member_driver_date=(false==empty($member_driver_date)?$member_driver_date:null);
            $newReturn->return_date=(false==empty($return_date)?$return_date:null);
            $newReturn->return_depertment=(false==empty($return_depertment)?$return_depertment:null);
            $newReturn->return_detail=(false==empty($return_detail)?$return_detail:null);
            $newReturn->return_note=(false==empty($return_note)?$return_note:null);
            $newReturn->save();
            //Redirect
            return Redirect::to('/return/viewReturn');
        }
    }

    public function addProduct()
    {
        //get ProductList
        $store=new Models\Stores();
        $listProduct=array();
        //set Session 'listProduct'
        if (Session::has('listProduct') ) {
            //get data Session 'listProduct'
            $listProduct = Session::get('listProduct');
        }
        //Get input  and declare variable
        $productId = Input::get('productId', 0);
        $productCount = Input::get('productCount', 0);
        $storeId = Input::get('storeId', 0);
        $return_id= Input::get('return_id', 0);
        //set rules
        $rules = array(
            'productId' => 'required',
            'productCount' => 'required',
            'storeId' => 'required',
        );
        //validate input
        $validator = Validator::make(Input::all(), $rules);
        //check validate
        if ($validator->fails()) {
            // false Redirect
            $messages = $validator->messages();
            return Redirect::to('/return/viewProduct?id='.$return_id)
                ->withErrors($validator)
                ->withInput(Input::except('productId','productCount','storeId'));
        } else {
            $newReturnDetail=new Models\ReturnDetail();
            $newReturnDetail->return_id=$return_id;
            $newReturnDetail->product_id=$productId;
            $newReturnDetail->storename_id=$storeId;
            $newReturnDetail->return_detail_count=$productCount;
            if($newReturnDetail->save()){
                    $store->updateStore($productId,$storeId,$productCount,'plus');
            }
            return Redirect::to('/return/viewProduct?id='.$return_id);
        }
    }
}
