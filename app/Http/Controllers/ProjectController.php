<?php namespace App\Http\Controllers;

/**
 * Created by PhpStorm.
 * User: sootipong
 * Date: 5/26/2015
 * Time: 3:48 AM
 */
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models;
use Carbon\Carbon;
use Illuminate\Support\Facades\Response;
use PhpParser\Node\Scalar\MagicConst\File;
use Request;
use Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;

class ProjectController extends Controller
{
    public function viewPaperWork(){
//        $id = Input::get('id', 0);
//        $projectsActivityLogDetail=New Models\ProjectsActivityLogDetail();
//        $projectsActivityLog=new Models\ProjectsActivityLog();
//        $dataProjectsActivityLog=$projectsActivityLog->findById($id);
//        $dataProjectsActivityLogDetail=$projectsActivityLogDetail->findByProjectsActivityLogId($id);
//        return view('project.PaperWork',array('dataProjectsActivityLogDetail'=>$dataProjectsActivityLogDetail,'dataProjectsActivityLog'=>$dataProjectsActivityLog));

        $id = Input::get('id', 0);
        $projectsActivity=new Models\ProjectsActivity();
        $dataProjectsActivity=$projectsActivity->findByProjectId($id);
        $dataProblem=Models\ProjectsActivityLogProblem::orderBy('ref_projects_activity_log_problem_id', 'DESC')->get();
        $dataWeather=Models\ProjectActivityLogWeather::orderBy('ref_project_activity_log_weather_id', 'DESC')->get();
        return view('project.PaperWorkProject',array('dataProjectsActivity'=>$dataProjectsActivity,'dataWeather'=>$dataWeather,'dataProblem'=>$dataProblem));
    }

    public function getChartActivitySubProject(){
        $id = Input::get('id', 0);
        $projectActivityLog=new Models\ProjectsActivityLog();
        $projectActivity=$projectActivityLog->findActivitySumByProjectSubId($id);
        echo json_encode(array(
            'status' => 'success',
            'data' => $projectActivity,
        ));
    }

    public function getChartBringProduct(){
        $id = Input::get('id', 0);
        $bring=new Models\Bring();
        $dateChart=array();
        $dateListProduct=$bring->findListProductBring($id);
        $dataProduct=$bring->findBringProductByDate($id);
        echo json_encode(array(
            'status' => 'success',
            'product' => $dateListProduct,
            'dataProduct' => $dataProduct
        ));
    }

    public function printPaperWorkProject(){
        $id = Input::get('id', 0);
        $projectsActivity=new Models\ProjectsActivity();
        $dataProjectsActivity=$projectsActivity->findByProjectId($id);
        $dataProblem=Models\ProjectsActivityLogProblem::orderBy('ref_projects_activity_log_problem_id', 'DESC')->get();
        $dataWeather=Models\ProjectActivityLogWeather::orderBy('ref_project_activity_log_weather_id', 'DESC')->get();
        $pdf = \PDF::loadView('project.PaperWorkProject',array('dataProjectsActivity'=>$dataProjectsActivity,'dataWeather'=>$dataWeather,'dataProblem'=>$dataProblem));
        return $pdf->download('PaperWorkProject_'.$id.'.pdf');
    }

    public function printPaperWork(){
        //1. Get input  and declare variable
        $id = Input::get('id', 0);
        $projectsActivityLogDetail=New Models\ProjectsActivityLogDetail();
        $projectsActivityLog=new Models\ProjectsActivityLog();
        $dataProjectsActivityLog=$projectsActivityLog->findById($id);
        $dataProjectsActivityLogDetail=$projectsActivityLogDetail->findByProjectsActivityLogId($id);
        $pdf = \PDF::loadView('project.PaperWork',array('dataProjectsActivityLogDetail'=>$dataProjectsActivityLogDetail,'dataProjectsActivityLog'=>$dataProjectsActivityLog))->setPaper('a4')->setOrientation('landscape');
        return $pdf->download('PaperWork_'.$id.'.pdf');
    }

    public function viewActivityLog(){
        $id = Input::get('id', 0);
        $activityLog=new Models\ProjectsActivityLog();
        $dataSubProject=Models\ProjectSub::find($id);
        $dataActivityLog=$activityLog->findByIdAll($id);
        return view('project.viewActivityLog', array('dataActivityLog'=>$dataActivityLog,'dataSubProject'=>$dataSubProject));
    }

    public function viewBring(){
        $id = Input::get('id', 0);
        $bring=new Models\Bring();
        $activityLog=new Models\ProjectsActivityLog();
        $dataSubProject=Models\ProjectSub::find($id);
        $dataProductBring=$bring->findByProjectSubId($id);
        return view('project.viewBring', array('dataProductBring'=>$dataProductBring,'dataSubProject'=>$dataSubProject));
    }

    public function removeActivityLogDetail(){
        //1. Get input  and declare variable
        $id = Input::get('id', 0);
        $result = array();

        //2. Process
        $activityLogDetail = new Models\ProjectsActivityLogDetail();
        $result = $activityLogDetail->deleteActivityLogDetail($id);

        //3. Redirect
        if ('error' == $result['status']) {
            return Redirect::to('/project/assignActivity?id='.$result['id'])->with('error', 'ไม่สามารถ ลบข้อมูลหน่วยสินค้า สำเร็จ');
        } else {
            return Redirect::to('/project/assignActivity?id='.$result['id'])->with($result['status'], $result['desc']);
        }
    }

    public function assignActivity(){
        $id = Input::get('id', 0);
        $projectsActivityLogDetail=new Models\ProjectsActivityLogDetail();
        $dataProjectsActivity=array();
        $dataProjectsActivityLogDetail=array();

        $dataProjectsActivityLog=Models\ProjectsActivityLog::find($id);
        $dataSubProject=Models\ProjectSub::find($dataProjectsActivityLog->project_sub_id);
        $projectsActivity=new Models\ProjectsActivity();
        if(false==empty($dataProjectsActivityLog)) {
            $dataProjectsActivity = $projectsActivity->findByProjectId($dataProjectsActivityLog->project_id);
            $dataProjectsActivityLogDetail=$projectsActivityLogDetail->findByProjectsActivityLogId($dataProjectsActivityLog->projects_activity_log_id);
        }
        $dataStaff=Models\Staffs::all();
        return view('project.assignActivity', array('dataSubProject'=>$dataSubProject,'dataProjectsActivityLog'=>$dataProjectsActivityLog,'id'=>$id,'dataProjectsActivity'=>$dataProjectsActivity,'dataStaff'=>$dataStaff,'dataProjectsActivityLogDetail'=>$dataProjectsActivityLogDetail));
    }

    public function viewPlanWork(){
        $id = Input::get('id', 0);
        return view('project.viewPlanWork',array());
    }

    public function viewSendWork(){
        $id = Input::get('id', 0);
        $name = Input::get('project_sub_name', '');
        $type = Input::get('type', '');
        $projectSub=new Models\ProjectSub();
        if(!empty($name) && !empty($type)){
            $name=str_replace('_','+',$name);
            $projectSub=$projectSub::where('project_sub_name','=',$name)->where('project_position','=',$type)->first();
        }
        $dataProjectSub=$projectSub->findAllProjectSubByKm($id);
        return view('project.viewSendWork',array('type'=>$type,'dataProjectSub'=>$dataProjectSub,'projectSub'=>$projectSub,'id'=>$id));
    }

    public function viewMainProject(){
        $id = Input::get('id', 0);
        $name = Input::get('project_sub_name', '');
        $type = Input::get('type', '');
        $projectSub=new Models\ProjectSub();
        if(!empty($name) && !empty($type)){
            $name=str_replace('_','+',$name);
            $projectSub=$projectSub::where('project_sub_name','=',$name)->where('project_position','=',$type)->first();
        }
        $dataProjectSub=$projectSub->findAllProjectSubByKm($id);
        return view('project.viewMainProject',array('type'=>$type,'dataProjectSub'=>$dataProjectSub,'projectSub'=>$projectSub,'id'=>$id));
    }

    public function viewActivityWork(){
        $id = Input::get('id', 0);
        $datetime =Carbon::now();
        $projectsActivityLog=new Models\ProjectsActivityLog();
        $dataProjectSub=Models\ProjectSub::find($id);
        $dataProjectsActivity=Models\ProjectsActivity::where('project_id','=',$dataProjectSub->project_id)->get();
        $dataWorkMonth=$projectsActivityLog->findWorkMonth($id);
        return view('project.viewActivityWork',array('dataWorkMonth'=>$dataWorkMonth,'countDay'=>$datetime->format('t'),'dataProjectSub'=>$dataProjectSub,'dataProjectsActivity'=>$dataProjectsActivity));
    }

    public function viewSubProject(){
        $id = Input::get('id', 0);
        $projectSub=new Models\ProjectSub();
        $dataProjectSub=$projectSub->find($id);
        $projectsActivity=new Models\ProjectsActivity();
        $dataProjectActivity=$projectsActivity->findByProjectId($id);
        $dataStaff=Models\Staffs::all();
        $dataProblem=Models\ProjectsActivityLogProblem::orderBy('ref_projects_activity_log_problem_id', 'DESC')->get();
        $dataWeather=Models\ProjectActivityLogWeather::orderBy('ref_project_activity_log_weather_id', 'DESC')->get();
        $dataSubProject=$projectSub->findProjectSubByProjectIdAndProjectSubName($dataProjectSub->project_id,$dataProjectSub->project_sub_name);
        $datetime =Carbon::now();
        $projectsActivityLog=new Models\ProjectsActivityLog();
        $dataProjectsActivity=Models\ProjectsActivity::where('project_id','=',$dataProjectSub->project_id)->get();
        $dataWorkMonth=$projectsActivityLog->findWorkMonth($dataProjectSub->project_sub_name);
        $monthNames = array("","มกราคม","กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
        return view('project.viewSubProject', array('monthNames'=>$monthNames,'countMonth'=>intval($datetime->format('m')),'dataWorkMonth'=>$dataWorkMonth,'countDay'=>$datetime->format('t'),'dataProjectsActivity'=>$dataProjectsActivity,'dataProjectSub'=>$dataProjectSub,'dataWeather'=>$dataWeather,'dataProblem'=>$dataProblem,'dataProjectActivity'=>$dataProjectActivity,'dataStaff'=>$dataStaff,'dataSubProject'=>$dataSubProject,'id'=>$id));
    }

    public function workTableChangeMonth(){
        $id = Input::get('id', 0);
        $month = Input::get('month', 0);
        $order = Input::get('order', '');
        $dataProjectSub=new Models\ProjectSub();
        $dataProjectSub=$dataProjectSub->find($id);
        $dataListProjectSub=$dataProjectSub->findBySubName($dataProjectSub->project_sub_name);
        $projectsActivityLog=new Models\ProjectsActivityLog();
        $dataActivityLog=$projectsActivityLog->GetProjectsActivityLogDetail($dataListProjectSub,$month,$order);
        $string="";
        $string.="<table class='table table-bordered'><thead><tr><th>#</th>";
        $string.="<th><a id='header_table' order='projects_activity_log.projects_activity_start_date' style='ursor: pointer;'>วันที่</a></th>";
        $string.="<th><a id='header_table' order='projects_activity_log_detail.start_at' style='ursor: pointer;'>ตำแหน่ง</a></th>";
        $string.="<th><a id='header_table' order='projects_activity.projects_activity_name' style='ursor: pointer;'>กิจกรรม</a></th>";
        $string.="<th><a id='header_table' order='projects_activity_log_detail.projects_activity_log_detail_value' style='ursor: pointer;'>จำนวน</a></th>";
        $string.="<th><a id='header_table' order='projects_activity_log.projects_activity_log_weather' style='ursor: pointer;'>สภาพอากาศ</a></th>";
        $string.="<th><a id='header_table' order='projects_activity_log.projects_activity_log_problem' style='ursor: pointer;'>ปัญหา</a></th>";
        $string.="<th><a id='header_table' order='staffs.staff_firstname' style='ursor: pointer;'>ผู้ปฏิบัติงาน</a></th>";
        $string.="</tr></thead><tbody>";
        foreach($dataActivityLog as $key=>$value) {
            $string .= "<tr><td>" . ($key + 1) . "</td><td>" . $value->projects_activity_start_date . "</td><td>+" . $value->start_at . " - +" . $value->end_at . " (" . $value->project_position . ")</td><td>" . $value->projects_activity_name . "</td><td style='text-align: right;'>" . number_format($value->projects_activity_log_detail_value, 2) . "</td>";
            $string .= "<td>".$value->projects_activity_log_weather."</td><td>".$value->projects_activity_log_problem."</td><td>".$value->staff_firstname." ".$value->staff_lastname."</td></tr>";
        }
        $string .= "</tbody></table>";
        return $string;
    }

    public function workChangeMonth(){
        $id = Input::get('id', 0);
        $month = Input::get('month', 0);
        $monthNames = array("เลือกเดือน","มกราคม","กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
        $datetime =Carbon::now();
        $year=$datetime->format('Y');
        if(!empty($month)) {
            $datetime->setdate($year, $month, 1);
        }
        $countDay=$datetime->format('t');
        $countMonth=intval($datetime->format('m'));
        $dataProjectSub=new Models\ProjectSub();
        $dataProjectSub=$dataProjectSub->find($id);
        $projectsActivityLog=new Models\ProjectsActivityLog();
        $dataProjectsActivity=Models\ProjectsActivity::where('project_id','=',$dataProjectSub->project_id)->get();
        $dataWorkMonth=$projectsActivityLog->findWorkMonth($dataProjectSub->project_sub_name,$month);

        $string="";
        $string.="<thead><tr><th rowspan='2'>กิจกรรม</th><th style='text-align: center;' colspan='".$countDay."'>เดือน ".$monthNames[$countMonth]."</th><th rowspan='2'>รวม</th></tr><tr>";
        for($i = 1; $i <=$countDay; $i++) {
            $string .= "<th>".$i."</th>";
        }
        $string.="</tr></thead><tbody>";
        foreach($dataProjectsActivity as $key=>$value){
            $sum=0;
            $string.="<tr><td style='padding: 0px;'>".$value->projects_activity_name."</td>";
            for($i = 1; $i <=$countDay; $i++){
                $string.="<td style='padding: 0px;text-align: center;'>";
                if(false==empty($dataWorkMonth[$i][$value->projects_activity_id])){
                    $sum+=$dataWorkMonth[$i][$value->projects_activity_id]['value'];
                    $string.="<span data-toggle='tooltip' data-placement='top' title='".$dataWorkMonth[$i][$value->projects_activity_id]['text']."' class='glyphicon glyphicon-road' aria-hidden='true'></span>";
                }
            }
            $string.="<td style='padding: 0px;text-align: right;font-weight: bolder;'>".number_format($sum,2)."</td></tr>";
        }
        $string.="<tr><td style='padding: 0px;font-weight: bolder;'>สภาพอากาศ</td>";
        for($i = 1; $i <=$countDay; $i++){
            $string.="<td style='padding: 0px;text-align: center;'></td>";
        }
        $string.="<td style='padding: 0px;text-align: center;font-weight: bolder;'></td></tr></tbody>";
        return $string;
    }

    public function viewImageProjectSub(){
        $id = Input::get('id', 0);
        $dataProjectSub=new Models\ProjectSub();
        $dataFile=new Models\Files();
        $dataProjectSub=$dataProjectSub->find($id);
        $dataListProjectSub=$dataProjectSub->findBySubName($dataProjectSub->project_sub_name);
        $dataImage=$dataFile->getImg($dataProjectSub->project_sub_name);
        $datetime =Carbon::now();
        $dataStaff=Models\Staffs::all();
        $dataProblem=Models\ProjectsActivityLogProblem::orderBy('ref_projects_activity_log_problem_id', 'DESC')->get();
        $dataWeather=Models\ProjectActivityLogWeather::orderBy('ref_project_activity_log_weather_id', 'DESC')->get();
        $projectsActivityLog=new Models\ProjectsActivityLog();
        $dataProjectsActivity=Models\ProjectsActivity::where('project_id','=',$dataProjectSub->project_id)->get();
        $dataWorkMonth=$projectsActivityLog->findWorkMonth($dataProjectSub->project_sub_name,intval($datetime->format('m')));
        $dataActivityLog=$projectsActivityLog->GetProjectsActivityLogDetail($dataListProjectSub);
        $monthNames = array("เลือกเดือน","มกราคม","กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
        return view('project.viewImageProjectSub',array('id'=>$id,'dataActivityLog'=>$dataActivityLog,'dataWeather'=>$dataWeather,'dataProblem'=>$dataProblem,'dataStaff'=>$dataStaff,'monthNames'=>$monthNames,'countMonth'=>intval($datetime->format('m')),'dataWorkMonth'=>$dataWorkMonth,'countDay'=>$datetime->format('t'),'dataProjectsActivity'=>$dataProjectsActivity,'dataImage'=>$dataImage,'dataProjectSub'=>$dataProjectSub,'dataListProjectSub'=>$dataListProjectSub));
    }

    public function viewProject()
    {
        $search = Input::get('search', '');
        $page = Input::get('page', 1);
        $countRow = 30;
        $project=new Models\Project();
        $dataProjectsActivityUnits=Models\ProjectsActivityUnits::all();
        $dataProject=$project->findAllProjectAndPaginate($countRow,$page,$search);
        $dataStaff=Models\Staffs::all();

        return view('project.viewProject', array('dataStaff'=>$dataStaff,'dataProjectsActivityUnits'=>$dataProjectsActivityUnits,'dataProject'=>$dataProject));
    }

    public function viewStaffWork(){
        $id = Input::get('id', 0);
        $staff=new Models\Staffs();
        $projectSub=new Models\ProjectSub();
        $datetime =Carbon::now();
        $dataStaff=Models\Staffs::where('staff_usertype','=',4)->get();
        $monthNames = array("","มกราคม","กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
        $dataWorkMonth=$staff->getActivityByProjectId($id);
        return view('project.viewStaffWork', array('dataWorkMonth'=>$dataWorkMonth,'monthNames'=>$monthNames,'countMonth'=>intval($datetime->format('m')),'countDay'=>$datetime->format('t'),'dataStaff'=>$dataStaff));
    }

    public function addActivityLogDetail(){
        //Get input  and declare variable
        $id = Input::get('id', 0);
        $projects_activity_id=Input::get('projects_activity_id',0);
        $projects_activity_log_id=Input::get('projects_activity_log_id',0);
        $projects_activity_log_value=Input::get('projects_activity_log_value',0);
        $start_at=Input::get('start_at',0);
        $end_at=Input::get('end_at',0);
        $id_staff_works=Input::get('id_staff_works',0);

        //set rules
        $rules = array(
            'projects_activity_id' => 'required',
            'projects_activity_log_value' => 'required|integer',
            'id_staff_works' => 'required',
            'start_at' => 'required',
            'end_at' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);
        //check validate
        if ($validator->fails()) {
            // false Redirect
            $messages = $validator->messages();
            return Redirect::to('/project/assignActivity?id='.$id)
                ->withErrors($validator)
                ->withInput(Input::except('projects_activity_id','projects_activity_log_value','id_staff_works','start_at','end_at'));
        } else {
            //add data
            $staff = Session::get('user');
            $newProjectActivityLogDetail=new Models\ProjectsActivityLogDetail();
            $newProjectActivityLogDetail->projects_activity_log_id=$projects_activity_log_id;
            $newProjectActivityLogDetail->projects_activity_id=$projects_activity_id;
            $newProjectActivityLogDetail->id_staff_record=$staff->staff_id;
            $newProjectActivityLogDetail->id_staff_works=$id_staff_works;
            $newProjectActivityLogDetail->projects_activity_log_detail_value=$projects_activity_log_value;
            $newProjectActivityLogDetail->projects_activity_log_detail_type='Work';
            $newProjectActivityLogDetail->start_at=$start_at;
            $newProjectActivityLogDetail->end_at=$end_at;
            $newProjectActivityLogDetail->save();
            return Redirect::to('/project/assignActivity?id='.$id);
        }
    }

    public function addActivityLog(){
        //Get input  and declare variable
        $project_id=Input::get('project_id',0);
        $project_sub_id=Input::get('project_sub_id',0);
        $id_staff_assign=Input::get('id_staff_assign','');
        $id_staff_check=Input::get('id_staff_check','');
        $projects_activity_log_worker=Input::get('projects_activity_log_worker',0);
        $projects_activity_log_km=Input::get('projects_activity_log_km',0);
        $projects_activity_log_village=Input::get('projects_activity_log_village','');
        $projects_activity_log_tambon=Input::get('projects_activity_log_tambon','');
        $projects_activity_log_amphoe=Input::get('projects_activity_log_amphoe','');
        $projects_activity_log_province=Input::get('projects_activity_log_province','');
        $projects_activity_log_dailyEvents=Input::get('projects_activity_log_dailyEvents','');
//        $projects_activity_log_problem=Input::get('projects_activity_log_problem','');
//        $projects_activity_log_weather=Input::get('projects_activity_log_weather','');
        $projects_activity_log_recommend=Input::get('projects_activity_log_recommend','');
        $projects_activity_start_date=Input::get('projects_activity_start_date','');
        $projects_activity_end_date=Input::get('projects_activity_end_date','');
        $activity_log_weather_other=Input::get('activity_log_weather_other','');
        $activity_log_problem_other=Input::get('activity_log_problem_other','');
        $projects_activity_log_weather=Input::get('projects_activity_log_weather','');
        $projects_activity_log_problem=Input::get('projects_activity_log_problem','');
        $projects_activity_log_start_telegraph_poles=Input::get('projects_activity_log_start_telegraph_poles',0);
        $projects_activity_log_start_telegraph_poles_total=Input::get('projects_activity_log_start_telegraph_poles_total',0);
        $projects_activity_log_end_telegraph_poles=Input::get('projects_activity_log_end_telegraph_poles',0);
        $projects_activity_log_end_telegraph_poles_total=Input::get('projects_activity_log_end_telegraph_poles_total',0);

        $stringWeather="";
        $stringProblem="";

        //set rules
        $rules = array(
            'id_staff_assign' => 'required',
            'id_staff_check' => 'required',
            'projects_activity_log_km' => 'required',
            'projects_activity_log_dailyEvents' => 'required',
            'projects_activity_start_date'=> 'required',
//            'projects_activity_log_start_telegraph_poles'=>'required',
//            'projects_activity_log_start_telegraph_poles_total'=>'required',
//            'projects_activity_log_end_telegraph_poles'=>'required',
//            'projects_activity_log_end_telegraph_poles_total'=>'required'
        );
        $validator = Validator::make(Input::all(), $rules);
        //check validate
        if ($validator->fails()) {
            // false Redirect
            $messages = $validator->messages();
            return Redirect::to('/project/viewImageProjectSub?id='.$project_sub_id)
                ->withErrors($validator)
                ->withInput(Input::except('projects_activity_start_date','id_staff_assign','id_staff_check','projects_activity_log_km','projects_activity_log_dailyEvents','projects_activity_log_end_telegraph_poles_total','projects_activity_log_end_telegraph_poles','projects_activity_log_start_telegraph_poles_total','projects_activity_log_start_telegraph_poles'));
        } else {

            if(false==empty($activity_log_problem_other)){
                $newProjectsActivityLogProblem=new Models\ProjectsActivityLogProblem();
                $newProjectsActivityLogProblem->ref_projects_activity_log_problem_name=$activity_log_problem_other;
                $newProjectsActivityLogProblem->save();
                $stringProblem=$activity_log_problem_other;
                if(false==empty($projects_activity_log_problem)) {
                    foreach ($projects_activity_log_problem as $key => $value) {
                        $stringProblem .= ",";
                        $stringProblem .= $value;
                    }
                }
            }else{
                if(false==empty($projects_activity_log_problem)) {
                    $index = 0;
                    foreach ($projects_activity_log_problem as $key => $value) {
                        if (0 !== $index) {
                            $stringProblem .= ",";
                        }
                        $stringProblem .= $value;
                        $index++;
                    }
                }
            }

            if(false==empty($activity_log_weather_other)){
                $newProjectActivityLogWeather=new Models\ProjectActivityLogWeather();
                $newProjectActivityLogWeather->ref_project_activity_log_weather_name=$activity_log_weather_other;
                $newProjectActivityLogWeather->save();
                $stringWeather=$activity_log_weather_other;
                $stringWeather.=$activity_log_weather_other;
                if(false==empty($projects_activity_log_weather)) {
                    foreach ($projects_activity_log_weather as $key => $value) {
                        $stringWeather .= ",";
                        $stringWeather .= $value;
                    }
                }
            }else{
                if(false==empty($projects_activity_log_weather)) {
                    $index = 0;
                    foreach ($projects_activity_log_weather as $key => $value) {
                        if (0 !== $index) {
                            $stringWeather .= ",";
                        }
                        $stringWeather .= $value;
                        $index++;
                    }
                }
            }

            //add GroupBring
            $staff = Session::get('user');
            $newProjectActivityLog=new Models\ProjectsActivityLog();
            $newProjectActivityLog->project_id=$project_id;
            $newProjectActivityLog->project_sub_id=$project_sub_id;
            $newProjectActivityLog->id_staff_record=$staff->staff_id;
            $newProjectActivityLog->id_staff_assign=$id_staff_assign;
            $newProjectActivityLog->id_staff_check=$id_staff_check;
            $newProjectActivityLog->projects_activity_log_worker=(false==empty($projects_activity_log_worker)?$projects_activity_log_worker:null);
            $newProjectActivityLog->projects_activity_log_km=intval($projects_activity_log_km);
            $newProjectActivityLog->projects_activity_log_village=(false==empty($projects_activity_log_village)?$projects_activity_log_village:null);
            $newProjectActivityLog->projects_activity_log_tambon=(false==empty($projects_activity_log_tambon)?$projects_activity_log_tambon:null);
            $newProjectActivityLog->projects_activity_log_amphoe=(false==empty($projects_activity_log_amphoe)?$projects_activity_log_amphoe:null);
            $newProjectActivityLog->projects_activity_log_province=(false==empty($projects_activity_log_province)?$projects_activity_log_province:null);
            $newProjectActivityLog->projects_activity_log_dailyEvents=(false==empty($projects_activity_log_dailyEvents)?$projects_activity_log_dailyEvents:null);
            $newProjectActivityLog->projects_activity_log_weather=(false==empty($stringWeather)?$stringWeather:null);
            $newProjectActivityLog->projects_activity_log_problem=(false==empty($stringProblem)?$stringProblem:null);
            $newProjectActivityLog->projects_activity_log_recommend=(false==empty($projects_activity_log_recommend)?$projects_activity_log_recommend:null);
            $newProjectActivityLog->projects_activity_start_date=(false==empty($projects_activity_start_date)?$projects_activity_start_date:null);
            $newProjectActivityLog->projects_activity_end_date=(false==empty($projects_activity_end_date)?$projects_activity_end_date:null);
            $newProjectActivityLog->projects_activity_log_start_telegraph_poles=(false==empty($projects_activity_log_start_telegraph_poles)?$projects_activity_log_start_telegraph_poles:null);
            $newProjectActivityLog->projects_activity_log_start_telegraph_poles_total=(false==empty($projects_activity_log_start_telegraph_poles_total)?$projects_activity_log_start_telegraph_poles_total:null);
            $newProjectActivityLog->projects_activity_log_end_telegraph_poles=(false==empty($projects_activity_log_end_telegraph_poles)?$projects_activity_log_end_telegraph_poles:null);
            $newProjectActivityLog->projects_activity_log_end_telegraph_poles_total=(false==empty($projects_activity_log_end_telegraph_poles_total)?$projects_activity_log_end_telegraph_poles_total:null);
            if($newProjectActivityLog->save()){
                $subp = Models\ProjectSub::find($project_sub_id);
                $subp->updated_at=date('Y-m-d H:i:s');
                $subp->save();
            }

            return Redirect::to('/project/assignActivity?id='.$newProjectActivityLog->projects_activity_log_id);
        }
    }

    public function addActivity(){
        $id = Input::get('id', 0);

        //Get input  and declare variable
        $project_id=Input::get('project_id',0);
        $projects_activity_name=Input::get('projects_activity_name','');
        $projects_activity_value=Input::get('projects_activity_value',0);
        $projects_activity_unit=Input::get('projects_activity_unit',0);
        $projects_activity_convert_mm=Input::get('projects_activity_convert_mm',0);
        //set rules
        $rules = array(
            'projects_activity_name' => 'required',
            'projects_activity_value' => 'required|integer',
            'projects_activity_convert_mm' => 'required|numeric',
            'projects_activity_unit' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);
        //check validate
        if ($validator->fails()) {
            // false Redirect
            $messages = $validator->messages();
            return Redirect::to('/project/viewProject')
                ->withErrors($validator)
                ->withInput(Input::except('projects_activity_name','projects_activity_value','projects_activity_unit','projects_activity_convert_mm'));
        } else {
            //add GroupBring
            $staff = Session::get('user');
            $newProjectActivity=new Models\ProjectsActivity();
            $newProjectActivity->project_id=$project_id;
            $newProjectActivity->id_staff_record=$staff->staff_id;
            $newProjectActivity->projects_activity_name=$projects_activity_name;
            $newProjectActivity->projects_activity_value=$projects_activity_value;
            $newProjectActivity->projects_activity_unit=$projects_activity_unit;
            $newProjectActivity->projects_activity_convert_mm=$projects_activity_convert_mm;
            $newProjectActivity->save();
            return Redirect::to('/project/viewProject');
        }
    }

    public function editSubProject(){
        //Get input  and declare variable
        $project_id=Input::get('project_id',0);
        $project_sub_id=Input::get('project_sub_id',0);
        $project_name=Input::get('project_sub_name','');
        $amount_POR=Input::get('amount_POR',0);
        $amount_survey=Input::get('amount_survey',0);
        $type=Input::get('type','');
//        $project_position=Input::get('project_position','');
//        $end_at=Input::get('end_at','');
//        $start_at=Input::get('start_at','');

        //set rules
        $rules = array(
            'project_sub_name' => 'required',
            'amount_POR' => 'required|regex:/^\d*(\.\d{2})?$/',
            'amount_survey' => 'required|regex:/^\d*(\.\d{2})?$/',
//            'start_at' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);
        $newProjectSub=Models\ProjectSub::find($project_sub_id);
        //check validate
        if ($validator->fails()) {
            // false Redirect
            $messages = $validator->messages();
            return Redirect::to('/project/viewMainProject?id='.$newProjectSub->project_id.'&project_sub_name='.str_replace('+','_',$project_name).'&type='.$type)
                ->withErrors($validator)
                ->withInput(Input::except('project_sub_name','amount_POR','amount_survey'));
        } else {
            //add GroupBring
            $staff = Session::get('user');
//            $newProjectSub=Models\ProjectSub::find($project_sub_id);
            $newProjectSub->project_sub_name=$project_name;
            $newProjectSub->amount_POR=$amount_POR;
            $newProjectSub->amount_survey=$amount_survey;
            $newProjectSub->save();
            return Redirect::to('/project/viewMainProject?id='.$newProjectSub->project_id);
        }
    }

    public function addSubProject(){
        //Get input  and declare variable
        $project_id=Input::get('project_id',0);
        $project_name=Input::get('project_sub_name','');
//        $project_position=Input::get('project_position','');
//        $end_at=Input::get('end_at','');
//        $start_at=Input::get('start_at','');

        //set rules
        $rules = array(
            'project_sub_name' => 'required',
//            'project_position' => 'required',
//            'end_at' => 'required',
//            'start_at' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);
        //check validate
        if ($validator->fails()) {
            // false Redirect
            $messages = $validator->messages();
            return Redirect::to('/project/viewSubProject?id='.$project_id)
                ->withErrors($validator)
                ->withInput(Input::except('project_sub_name','project_position','start_at','end_at'));
        } else {
            //add GroupBring
            $staff = Session::get('user');
            $newProjectSub=new Models\ProjectSub();
            $newProjectSub->autoCreated($project_id,$project_name);
            return Redirect::to('/project/viewSubProject?id='.$project_id);
        }
    }

    public function addProject(){
        //Get input  and declare variable
        $project_name=Input::get('project_name',0);
        $start_km=Input::get('start_km',0);
        $end_km=Input::get('end_km',0);
        //set rules
        $rules = array(
            'project_name' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);
        //check validate
        if ($validator->fails()) {
            // false Redirect
            $messages = $validator->messages();
            return Redirect::to('/project/viewProject')
                ->withErrors($validator)
                ->withInput(Input::except('project_name'));
        } else {
            //add GroupBring
            $newProject=new Models\Project();
            $newProject->project_name=$project_name;
            if($newProject->save()){
                $newProject->autoCreate($newProject->project_id,$start_km,$end_km);
            }
            return Redirect::to('/project/viewProject');
        }
    }

    public function calculateBring(){
        $id=Input::get('id',0);
        $orderManufacture=new Models\OrderManufacture();
        $dataCalculateBring=$orderManufacture->GetCalculateBring($id);
        return view('project.calculateBring',array('id'=>$id,'dataCalculateBring'=>$dataCalculateBring));
    }

    public function orderManufacture(){
        $id=Input::get('id',0);
        $user = Session::get('user');
        $orderManufacture=new Models\OrderManufacture();
        $orderManufacture->staff_id_asset=$user->staff_username;
        $staff=Models\Staffs::where('staff_usertype', '!=', 1)->where('staff_usertype', '!=',2)->where('staff_usertype', '!=',8)->get();
        $dataOrderManufacture=$orderManufacture::where('project_id', '=', $id)->orderBy('order_manufacture_type', 'ASC')->get();
        $storeName=Models\Storename::all();
        $Product=Models\Product::all();
        return view('project.orderManufacture',array('Product'=>$Product,'user'=>$user,'storeName'=>$storeName,'dataOrderManufacture'=>$dataOrderManufacture,'orderManufacture'=>$orderManufacture,'id'=>$id,'staff'=>$staff));
    }

    public function RemoveOrderManufacture(){
        $order_manufacture_id = Input::get('id', 0);
        if(!empty($order_manufacture_id)){
            $data=Models\OrderManufacture::find($order_manufacture_id);
            if(!empty($data))
                $data->delete();

            return Redirect::to('/project/orderManufacture?id='.$data->project_id);
        }
    }

    public function addManufacture(){
        if(!empty($_POST)) {
            $dataOrder=Models\OrderManufacture::find($_POST['order_manufacture_id']);
            $product=Models\Product::find($dataOrder->product_id);
            $Stores=new Models\Stores();
            $newAccount = new Models\ProductsAccount();
            $newAccount->project_id = $_POST['project_id'];
            $newAccount->payee = $_POST['payee'];
//            $newAccount->remark = $_POST['remark'];
            $newAccount->product_id = $dataOrder->product_id;
            $newAccount->storename_id=$_POST['storename_id'];
            $newAccount->income_product_id = $product->product_name;
            $newAccount->income = $_POST['count'];
            $Stores->updateStore($product->product_id,$_POST['storename_id'],$_POST['count'],'plus');
            $dataStore=Models\Stores::where('product_id', '=', $product->product_id)->where('storename_id', '=',$_POST['storename_id'])->first();
            if(!empty($dataStore)){
                $newAccount->balance = $dataStore->balance_product;
                $dataOrder->order_manufacture_balance=$dataOrder->order_manufacture_count-(intval($dataOrder->order_manufacture_finished)+$_POST['count']);
                $dataOrder->order_manufacture_finished+=$_POST['count'];
                if($dataOrder->order_manufacture_finished>=$dataOrder->order_manufacture_count)
                    $dataOrder->order_manufacture_type='finish';

                if ($newAccount->save()) {
                    if($dataOrder->save()){
                        return Redirect::to('/project/orderManufacture?id='.$_POST['project_id']);
                    }
                }
            }
        }
    }

    public function BringProduct(){
        if(!empty($_POST)) {
            foreach($_POST['order'] as $key=>$value){
                $dataOrder=Models\OrderManufactureDetail::find($key);
                $product=Models\Product::find($dataOrder->product_id);
                $Stores=new Models\Stores();
                $newAccount = new Models\ProductsAccount();
                $newAccount->project_id = $_POST['project_id'];
                $newAccount->payee = $_POST['payee'];
                $newAccount->remark = $_POST['remark'];
                $newAccount->product_id = $dataOrder->product_id;
                $newAccount->storename_id=$dataOrder->storename_id;
                $newAccount->expenses_product_id = $product->product_name;
                $newAccount->expenses = $value;
                $Stores->updateStore($product->product_id,$dataOrder->storename_id,$value,'minus');
                $dataStore=Models\Stores::where('product_id', '=', $product->product_id)->where('storename_id', '=',$dataOrder->storename_id)->first();
                    if(!empty($dataStore)){
                        $newAccount->balance = $dataStore->balance_product;
                        $dataOrder->balance=$dataOrder->count-(intval($dataOrder->bring)+$value);
                        $dataOrder->bring+=$value;
                        if ($newAccount->save()) {
                            $dataOrder->save();
                        }
                    }
            }
            return Redirect::to('/project/orderManufacture?id='.$_POST['project_id']);
        }
    }

    public function GetOrderBring(){
        $id=Input::get('id',0);
        $dataOrderManufactureDetail=Models\OrderManufactureDetail::where('order_manufacture_id', '=',$id)->where('status', '=','approve')->get();
        $string="
        <table class='table table-bordered'>
          <thead>
            <tr>
              <th>#</th>
              <th>ชื่อสินค้า</th>
              <th>จำนวน</th>
              <th>เบิกไปเเล้ว</th>
              <th>เบิก</th>
            </tr>
          </thead>
          <tbody>";
            foreach($dataOrderManufactureDetail as $key=>$value) {
                $string .= "<tr>
                              <td>".($key+1)."</td>
                              <td>".Models\Product::GetNameProduct($value->product_id)."</td>
                              <td>".$value->count."</td>
                              <td>".(!empty($value->bring)?$value->bring:'0')."</td>
                              <td><input type='text' name=order[".$value->order_manufacture_detail_id."] class='form-control' value='".(!empty($value->bring)?($value->count-$value->bring):$value->count)."' /></td>
                            </tr>";
            }
        $string.="</tbody></table>";
        echo $string;
    }

    public function GetStorkByProduct(){
        $id=Input::get('id',0);
        $dataStoreName=Models\Storename::all();
        $string="";
        foreach($dataStoreName as $key=>$value){
            $dataStork=Models\Stores::where('product_id', '=',$id)->where('storename_id', '=',$value->storename_id)->first();
            if(!empty($dataStork)){
                $string.="<option value='".$value->storename_id."'>".$value->storename_name." คงเหลือ ".$dataStork->balance_product."</option>";
            }else{
                $string.="<option value='".$value->storename_id."'>".$value->storename_name." ไม่มีสินค้า </option>";
            }
        }
        echo $string;
    }

    public function ConfirmOrderManufactureDetail(){
        if(!empty($_POST)) {
            $order_manufacture_detail_id = Input::get('order_manufacture_detail_id', 0);
            $project_id = Input::get('project_id', 0);
            $product_id = Input::get('product_id', 0);
            $count = Input::get('count', 0);
            $storename_id = Input::get('storename_id', 0);
            //set rules
            $rules = array(
                'product_id' => 'required',
                'count' => 'required',
                'storename_id' => 'required',
            );
            //validate input
            $validator = Validator::make(Input::all(), $rules);
            //check validate
            if ($validator->fails()) {
                // false Redirect
                $messages = $validator->messages();
                return Redirect::to('/project/orderManufacture?id=' . $project_id)
                    ->withErrors($validator)
                    ->withInput(Input::except('$product_id', 'count', 'storename_id'));
            } else {
                $newAccount=Models\OrderManufactureDetail::find($order_manufacture_detail_id);
                $dataStork=Models\Stores::where('product_id', '=',$product_id)->where('storename_id', '=',$storename_id)->first();
                if(!empty($newAccount)){
                    $newAccount->product_id = $product_id;
                    $newAccount->storename_id = $storename_id;
                    $newAccount->count = $count;
                    if($dataStork->balance_product>$count) {
                        $newAccount->status = 'approve';
                    }else{
                        $newAccount->status = 'bring';
                    }
                    if ($newAccount->save()) {
                        return Redirect::to('/project/orderManufacture?id='.$project_id);
                    }
                }
            }
        }
    }

    public function bringOrderManufactureDetail(){
        $project_id = Input::get('id', 0);
        $data=array();
        $dataProject=Models\Project::find($project_id);
        $dataOrderManufactureDetail=Models\OrderManufactureDetail::where('project_id','=',$project_id)->where('status','=','bring')->get();
        if(!empty($dataOrderManufactureDetail)){
            foreach($dataOrderManufactureDetail as $key=>$value){
                $dataStork=Models\Stores::where('product_id', '=',$value->product_id)->where('storename_id', '=',$value->storename_id)->first();
                $Product=array();
                $Product['name']=Models\Product::GetNameProduct($value->product_id);
                $Product['storename']=Models\Storename::GetNameStore($value->storename_id);
                if(!empty($dataStork)){
                    $Product['count'] =$value->count-$dataStork->balance_product;
                }else {
                    $Product['count'] =$value->count;
                }
                array_push($data,$Product);
            }
        }
        return view('project.bringOrderManufactureDetail',array('data'=>$data,'dataProject'=>$dataProject));
    }

    public function printBring(){
        $id = Input::get('id', 0);
        $dataProject=Models\Project::find($id);
        $orderManufacture=new Models\OrderManufacture();
        $data=$orderManufacture->GetProductBring($id);
        return view('project.printBring',array('data'=>$data,'dataProject'=>$dataProject));
    }

    public function addOrderManufactureDetail(){
        if(!empty($_POST)) {
            $project_id = Input::get('project_id', 0);
            $count = Input::get('count', 0);
            $storename_id = Input::get('storename_id', 0);
            $name=Input::get('name', '');
            $order_manufacture_id=Input::get('order_manufacture_id',0);
            //set rules
            //set rules
            $rules = array(
                'name' => 'required',
                'count' => 'required',
                'storename_id'=>'required',
            );
            //validate input
            $validator = Validator::make(Input::all(), $rules);
            //check validate
            if ($validator->fails()) {
                // false Redirect
                $messages = $validator->messages();
                return Redirect::to('/project/orderManufacture?id='.$project_id)
                    ->withErrors($validator)
                    ->withInput(Input::except('name', 'count', 'staff_id_asset'));
            } else {
                $name=explode('::',$name);
                $product=Models\Product::where('product_name', '=', $name[0])->first();
                $newAccount=Models\OrderManufactureDetail::where('product_id', '=', $product->product_id)->where('order_manufacture_id', '=', $order_manufacture_id)->where('storename_id', '=', $storename_id)->where('project_id', '=', $project_id)->first();
                if(!empty($product)){
                    $user = Session::get('user');
                    if(!empty($newAccount->order_manufacture_detail_id)){
                        $newAccount->count += $count;
                    }else {
                        $newAccount = new Models\OrderManufactureDetail();
                        $newAccount->project_id = $project_id;
                        $newAccount->product_id = $product->product_id;
                        $newAccount->storename_id = $storename_id;
                        $newAccount->count = $count;
                        $newAccount->staff_id = $user->staff_id;
                        $newAccount->status = 'add';
                        $newAccount->order_manufacture_id = $order_manufacture_id;
                    }
                    if ($newAccount->save()) {
                        return Redirect::to('/project/orderManufacture?id='.$project_id);
                    }
                }else{
                    echo 'Not data Product';
                    exit;
                }
            }
        }
    }

    public function addOrderManufacture(){
        if(!empty($_POST)) {
            $project_id = Input::get('project_id', 0);
            $count = Input::get('count', 0);
            $staff_id_asset = Input::get('staff_id_asset', 0);
            $name=Input::get('name', '');
            //set rules
            $rules = array(
                'name' => 'required',
                'count' => 'required|integer',
                'staff_id_asset'=>'required',
            );
            //validate input
            $validator = Validator::make(Input::all(), $rules);
            //check validate
            if ($validator->fails()) {
                // false Redirect
                $messages = $validator->messages();
                return Redirect::to('/project/orderManufacture?id='.$project_id)
                    ->withErrors($validator)
                    ->withInput(Input::except('name', 'count', 'staff_id_asset'));
            } else{
                $user = Session::get('user');
                $name=explode('::',$name);
                $product=Models\Product::where('product_name', '=', $name[0])->first();
                if(!empty($product)) {
                    $newAccount = new Models\OrderManufacture();
                    $newAccount->project_id=$project_id;
                    $newAccount->product_id = $product->product_id;
                    $newAccount->staff_id_asset=$staff_id_asset;
                    $newAccount->staff_id_add=$user->staff_id;
                    $newAccount->order_manufacture_count=$count;
                    $newAccount->order_manufacture_finished=0;
                    $newAccount->order_manufacture_balance=0;
                    $newAccount->order_manufacture_type='add';
                    if ($newAccount->save()) {
                        return Redirect::to('/project/orderManufacture?id='.$project_id);
                    }
                }else{
                    echo 'Not data Product';
                    exit;
                }
            }
        }
    }

    public function reportProject(){
        $id = Input::get('id', 0);
        $accounts=new Models\ProductsAccount();
        $dataAccounts=$accounts->getByProjectId($id);
        $inventoryAccounts=new Models\InventoryAccount();
        $dataInventoryAccounts=$inventoryAccounts->getByProjectId($id);
        $sumIncome=$inventoryAccounts->GetSumReceiptsByProject($id);
        $sumExpenses=$inventoryAccounts->GetSumExpensesByProject($id);
        return view('project.reportProject',array('id'=>$id,'sumExpenses'=>$sumExpenses,'sumIncome'=>$sumIncome,'inventoryAccounts'=>$inventoryAccounts,'dataInventoryAccounts'=>$dataInventoryAccounts,'accounts'=>$accounts,'dataAccounts'=>$dataAccounts));
    }

    public function removeOrderManufactureDetail(){
        $id = Input::get('id', 0);
        $dataOrderManufactureDetail=Models\OrderManufactureDetail::find($id);
        if(!empty($dataOrderManufactureDetail)){
            $dataOrderManufactureDetail->delete();
            return Redirect::to('/project/orderManufacture?id='.$dataOrderManufactureDetail->project_id);
        }
    }
}