<?php namespace App\Http\Controllers;

/**
 * Created by PhpStorm.
 * User: sootipong
 * Date: 5/26/2015
 * Time: 3:48 AM
 */
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models;
use Illuminate\Support\Facades\Response;
use Request;
use Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class BringController extends Controller
{
    public function viewPaperBring()
    {
        return view('bring.PaperBring');
    }

    public function createPaperBring()
    {
        try {
            $pdf = \PDF::loadView('bring.PaperBring');
            return $pdf->stream();
        } catch (Exception $e) {
            echo $e->getLine() . ':' . $e->getMessage();
        } finally {

        }
    }

    public function createPaperReturn()
    {
        $pdf = \PDF::loadView('bring.PaperReturn');
        return $pdf->download('PaperReturn.pdf');
    }

    public function viewBring()
    {
        $id = Input::get('id', 0);
        $search = Input::get('search', '');
        $page = Input::get('page', 1);
        $countRow = 30;
        $Bring = array();
        if (false == empty($id)) {
            $Bring = Models\Bring::find($id);
        }
        $bring = new Models\Bring();
        $dataBring = $bring->findAllBringAndPaginate($countRow, $page, $search);
        $dataGroupBring = Models\GroupBring::all();
        $dataCompany = Models\Company::all();
        $dataProduct = Models\Product::all();
        $dataStoreName = Models\Storename::where('storename_id', '!=', 1)->get();
        $dataStaff = Models\Staffs::all();
        $dataVehicles = Models\Vehicle::all();
        return view('bring.viewBring', array('dataGroupBring' => $dataGroupBring, 'Bring' => $Bring, 'dataVehicles' => $dataVehicles, 'dataStaff' => $dataStaff, 'dataStoreName' => $dataStoreName, 'dataCompany' => $dataCompany, 'dataProduct' => $dataProduct, 'dataBring' => $dataBring));
    }

    public function removeListProduct()
    {
        //1. Get input  and declare variable
        $id = Input::get('id', 0);
        $listProduct = array();
        if (Session::has('listProduct')) {
            $listProduct = Session::get('listProduct');
        }

        //2. Process
        if (false == empty($listProduct[$id])) {
            array_splice($listProduct, $id, 1);
            Session::set('listProduct', $listProduct);
            $dataCompany = Models\Company::all();
            $dataProduct = Models\Product::all();
            $dataStoreName = Models\Storename::where('storename_id', '!=', 1)->get();
            $dataStaff = Models\Staffs::all();
            $dataVehicles = Models\Vehicle::all();
            //3. Return to view
            return view('bring.manage', array('id' => $id, 'dataVehicles' => $dataVehicles, 'dataStaff' => $dataStaff, 'dataStoreName' => $dataStoreName, 'dataCompany' => $dataCompany, 'dataProduct' => $dataProduct, 'listProduct' => $listProduct));
        } else {
            //false Redirect
            return Redirect::to('/bring/openManage')->with('error', 'ไม่สามารถ แก้ไขข้อมูลสินค้า สำเร็จ');
        }
    }

    public function openManage()
    {
        //1. Get input  and declare variable
        $bringDetail = New Models\BringDetail();
        $id = Input::get('id', 0);
        $dataBring=Models\Bring::find($id);
        $listProduct = array();
        $ControlBuildLog = array();
        $Bring = array();
        $dataSubProject = array();
        try {
            if (false == empty($id)) {
                $listProduct = $bringDetail->findById($id);
                $ControlBuildLog = Models\ControlBuildLog::where('bring_id', '=', $id)->first();
                $Bring = Models\Bring::find($id);
                if (false == empty($Bring->project_sub_id)) {
                    $dataSubProject = Models\ProjectSub::find($Bring->project_sub_id);
                }
            }
            //2. Process
            $dataCompany = Models\Company::all();
            $dataProduct = Models\Product::all();
            $dataStoreName = Models\Storename::where('storename_id', '!=', 1)->get();
            $dataStaff = Models\Staffs::all();
            $dataVehicles = Models\Vehicle::all();
        } catch (\Exception $e) {
            echo $e->getLine() . ":" . $e->getMessage();
        }
        //3. Return to view
        return view('bring.manage', array('dataBring'=>$dataBring,'dataSubProject' => $dataSubProject, 'Bring' => $Bring, 'id' => $id, 'ControlBuildLog' => $ControlBuildLog, 'id' => $id, 'listProduct' => $listProduct, 'dataVehicles' => $dataVehicles, 'dataStaff' => $dataStaff, 'dataStoreName' => $dataStoreName, 'dataCompany' => $dataCompany, 'dataProduct' => $dataProduct));
    }

    public function createBring()
    {
        //1. Get input  and declare variable
        $listProduct = array();
        if (Session::has('listProduct')) {
            $listProduct = Session::get('listProduct');
        }
        //2. Process
        $dataCompany = Models\Company::all();
        $dataProduct = Models\Product::all();
        $dataStoreName = Models\Storename::where('storename_id', '!=', 1)->get();
        $dataStaff = Models\Staffs::all();
        $dataVehicles = Models\Vehicle::all();
        //3. Return to view
        return view('bring.manage', array('dataVehicles' => $dataVehicles, 'dataStaff' => $dataStaff, 'dataStoreName' => $dataStoreName, 'dataCompany' => $dataCompany, 'dataProduct' => $dataProduct, 'listProduct' => $listProduct));
    }

    public function addProductDeteriorate()
    {
        //Get input  and declare variable
        $store = new Models\Stores();
        $deteriorate_bring_id = Input::get('deteriorate_bring_id', 0);
        $deteriorateProduct_id = Input::get('deteriorateProduct_id', 0);
        $deteriorateProduct_count = Input::get('deteriorateProduct_count', 0);
        $deteriorateProduct_detail = Input::get('deteriorateProduct_detail', '');
        //set rules
        $rules = array(
            'deteriorate_bring_id' => 'required',
            'deteriorateProduct_id' => 'required',
            'deteriorateProduct_detail' => 'required',
            'deteriorateProduct_count' => 'required|integer',
        );
        //validate input
        $validator = Validator::make(Input::all(), $rules);
        //check validate
        if ($validator->fails()) {
            // false Redirect
            $messages = $validator->messages();
            return Redirect::to('/bring/viewProduct?id=' . $deteriorate_bring_id)
                ->withErrors($validator)
                ->withInput(Input::except('deteriorate_bring_id', 'deteriorateProduct_id', 'deteriorateProduct_detail', 'deteriorateProduct_count'));
        } else {
            //add BringProduct
            $newBringDetail = new Models\BringDetail();
            $newBringDetail->bring_id = $deteriorate_bring_id;
            $newBringDetail->product_id = $deteriorateProduct_id;
            $newBringDetail->storename_id = 1;
            $newBringDetail->bring_detail_count = $deteriorateProduct_count;
            $newBringDetail->bring_detail_price = null;
            $newBringDetail->bring_type = 'deteriorate';
            $newBringDetail->bring_detail_location = $deteriorateProduct_detail;
            if ($newBringDetail->save()) {
                $store->updateStore($deteriorateProduct_id, 1, $deteriorateProduct_count, 'plus');
            }
            return Redirect::to('/bring/viewProduct?id=' . $deteriorate_bring_id);
        }
    }

    public function addBring()
    {
        //Get input  and declare variable
        $store = new Models\Stores();
        $group_bring_id = Input::get('group_bring_id', 0);
        $project_sub_id = Input::get('project_sub_id', 0);
        $bring_id = Input::get('bring_id', 0);
        $control_build_id = Input::get('control_build_id', 0);
        $bring_number = Input::get('bring_number', 0);
        $bring_type_manufacture = Input::get('bring_type_manufacture', false);
        $bring_type_project = Input::get('bring_type_project', false);
        $bring_type_other = Input::get('bring_type_other', false);
        $id_member_bring = Input::get('id_member_bring', 0);
        $id_member_approve_bring = Input::get('id_member_approve_bring', 0);
        $id_member_approve_pay = Input::get('id_member_approve_pay', 0);
        $id_member_store = Input::get('id_member_store', 0);
        $id_member_account = Input::get('id_member_account', 0);
        $id_member_driver = Input::get('id_member_driver', 0);
        $member_bring_date = Input::get('member_bring_date', '');
        $member_approve_bring_date = Input::get('member_approve_bring_date', '');
        $member_approve_pay_date = Input::get('member_approve_pay_date', '');
        $member_store_date = Input::get('member_store_date', '');
        $member_account_date = Input::get('member_account_date', '');
        $member_driver_date = Input::get('member_driver_date', '');
        $bring_vehicles_id = Input::get('bring_vehicles_id', 0);
        $bring_date = Input::get('bring_date', '');
        $bring_depertment = Input::get('bring_depertment', '');
        $bring_detail = Input::get('bring_detail', '');
        $bring_note = Input::get('bring_note', '');
        //set rules
        $rules = array(
            'bring_number' => 'required',
            'bring_date' => 'required',
            'member_bring_date' => 'required',
            'id_member_bring' => 'required',
        );
        //validate input
        $validator = Validator::make(Input::all(), $rules);
        //check validate
        if ($validator->fails()) {
            // false validator Redirect
            if (false == empty($control_build_id)) {
                return Redirect::to('/controlBuild/viewControlBuild')
                    ->withErrors($validator)
                    ->withInput(Input::except('bring_note', 'bring_detail', 'bring_depertment', 'bring_date', 'bring_vehicles_id', 'member_driver_date', 'member_account_date', 'member_store_date', 'member_approve_pay_date', 'member_approve_bring_date', 'member_bring_date', 'id_member_driver', 'id_member_account', 'id_member_store', 'id_member_approve_pay', 'id_member_approve_bring', 'id_member_bring', 'bring_type_other', 'bring_type_project', 'bring_type_manufacture', 'bring_number'));
            } elseif ($project_sub_id) {
                $dataProjectSub = Models\ProjectSub::find($project_sub_id);
                return Redirect::to('/project/viewSubProject?id=' . $project_sub_id)
                    ->withErrors($validator)
                    ->withInput(Input::except('bring_note', 'bring_detail', 'bring_depertment', 'bring_date', 'bring_vehicles_id', 'member_driver_date', 'member_account_date', 'member_store_date', 'member_approve_pay_date', 'member_approve_bring_date', 'member_bring_date', 'id_member_driver', 'id_member_account', 'id_member_store', 'id_member_approve_pay', 'id_member_approve_bring', 'id_member_bring', 'bring_type_other', 'bring_type_project', 'bring_type_manufacture', 'bring_number'));
            } else {
                return Redirect::to('/bring/viewBring')
                    ->withErrors($validator)
                    ->withInput(Input::except('bring_note', 'bring_detail', 'bring_depertment', 'bring_date', 'bring_vehicles_id', 'member_driver_date', 'member_account_date', 'member_store_date', 'member_approve_pay_date', 'member_approve_bring_date', 'member_bring_date', 'id_member_driver', 'id_member_account', 'id_member_store', 'id_member_approve_pay', 'id_member_approve_bring', 'id_member_bring', 'bring_type_other', 'bring_type_project', 'bring_type_manufacture', 'bring_number'));
            }
        } else {
            //true validator save data purchases
            //create Bring
            if (false == empty($bring_id)) {
                $newBring = Models\Bring::find($bring_id);
            } else {
                $newBring = new Models\Bring();
            }
            $newBring->bring_number = $bring_number;
            if (false == empty($bring_type_manufacture)) {
                $newBring->bring_type_manufacture = 'Y';
            }
            if (false == empty($bring_type_project)) {
                $newBring->bring_type_project = 'Y';
            }
            if (false == empty($bring_type_other)) {
                $newBring->bring_type_other = 'Y';
            }
            $staff = Session::get('user');
            $newBring->id_member_record = $staff->staff_id;
            $newBring->project_sub_id = (false == empty($project_sub_id) ? $project_sub_id : null);
            $newBring->group_bring_id = (false == empty($group_bring_id) ? $group_bring_id : null);
            $newBring->id_member_bring = (false == empty($id_member_bring) ? $id_member_bring : null);
            $newBring->id_member_approve_bring = (false == empty($id_member_approve_bring) ? $id_member_approve_bring : null);
            $newBring->id_member_approve_pay = (false == empty($id_member_approve_pay) ? $id_member_approve_pay : null);
            $newBring->id_member_store = (false == empty($id_member_store) ? $id_member_store : null);
            $newBring->id_member_account = (false == empty($id_member_account) ? $id_member_account : null);
            $newBring->id_member_driver = (false == empty($id_member_driver) ? $id_member_driver : null);
            $newBring->member_bring_date = (false == empty($member_bring_date) ? $member_bring_date : null);
            $newBring->member_approve_bring_date = (false == empty($member_approve_bring_date) ? $member_approve_bring_date : null);
            $newBring->member_approve_pay_date = (false == empty($member_approve_pay_date) ? $member_approve_pay_date : null);
            $newBring->member_store_date = (false == empty($member_store_date) ? $member_store_date : null);
            $newBring->member_account_date = (false == empty($member_account_date) ? $member_account_date : null);
            $newBring->member_driver_date = (false == empty($member_driver_date) ? $member_driver_date : null);
            $newBring->bring_vehicles_id = (false == empty($bring_vehicles_id) ? $bring_vehicles_id : null);
            $newBring->bring_date = (false == empty($bring_date) ? $bring_date : null);
            $newBring->bring_depertment = (false == empty($bring_depertment) ? $bring_depertment : null);
            $newBring->bring_detail = (false == empty($bring_detail) ? $bring_detail : null);
            $newBring->bring_note = (false == empty($bring_note) ? $bring_note : null);
            if (true == $newBring->save()) {
                if (false == empty($control_build_id)) {
                    $newControlBuildLog = new Models\ControlBuildLog();
                    $newControlBuildLog->control_build_id = $control_build_id;
                    $newControlBuildLog->bring_id = $newBring->bring_id;
                    $newControlBuildLog->control_build_log_type = 'bring';
                    $newControlBuildLog->control_build_log_date = null;
                    $newControlBuildLog->control_build_product_id = null;
                    $newControlBuildLog->staff_id = (false == empty($id_member_bring) ? $id_member_bring : null);
                    $newControlBuildLog->control_build_log_detail = (false == empty($bring_detail) ? $bring_detail : null);
                    if (true == $newControlBuildLog->save()) {
                        return Redirect::to('/bring/viewProduct?id=' . $newBring->bring_id);
                    }
                } elseif ($project_sub_id) {
                    return Redirect::to('/bring/viewProduct?id=' . $newBring->bring_id);
                } elseif ($group_bring_id) {
                    return Redirect::to('/bring/viewProduct?id=' . $newBring->bring_id);
                } else {
                    //Redirect
                    return Redirect::to('/bring/viewBring');
                }
            }
        }
    }

    public function addProduct()
    {
        //get ProductList
        $listProduct = array();
        $store = new Models\Stores();
        //set Session 'listProduct'
        if (Session::has('listProduct')) {
            //get data Session 'listProduct'
            $listProduct = Session::get('listProduct');
        }
        //Get input  and declare variable
        $productId = Input::get('productId', null);
        $productCount = Input::get('productCount', null);
        $storeId = Input::get('storeId', null);
        $productLocation = Input::get('productLocation', null);
        $bring_id = Input::get('bring_id', null);
        //set rules
        $rules = array(
            'productId' => 'required',
            'productCount' => 'required',
            'storeId' => 'required',
            'productLocation' => 'required'
        );
        //validate input
        $validator = Validator::make(Input::all(), $rules);
        //check validate
        if ($validator->fails()) {
            // false Redirect
            $messages = $validator->messages();
            return Redirect::to('/bring/viewProduct?id='.$bring_id)
                ->withErrors($validator)
                ->withInput(Input::except('productId', 'productCount', 'storeId', 'productLocation'));
        } else {
            $newBringDetail = new Models\BringDetail();
            $newBringDetail->bring_id = $bring_id;
            $newBringDetail->product_id = $productId;
            $newBringDetail->storename_id = $storeId;
            $newBringDetail->bring_detail_count = $productCount;
            $newBringDetail->bring_detail_price = null;
            $newBringDetail->bring_type = 'bring';
            $newBringDetail->bring_detail_location = $productLocation;
            if ($newBringDetail->save()) {
                $store->updateStore($productId, $storeId, $productCount, 'minus');
            }
            return Redirect::to('/bring/viewProduct?id=' . $bring_id);
        }
    }
}
