<?php namespace App\Http\Controllers;

/**
 * Created by PhpStorm.
 * User: sootipong
 * Date: 5/26/2015
 * Time: 3:48 AM
 */
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models;
use Illuminate\Session\Store;
use Illuminate\Support\Facades\Response;
use Request;
use Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;

class PurchasesController extends Controller
{
    public function editPurchases(){
        $id = Input::get('id', 0);
        $search = Input::get('search', '');
        $page = Input::get('page', 1);
        $countRow = 30;
        // Process
        $purchases=new Models\Purchases();
        $dataPurchases = $purchases->findAllPurchasesAndPaginate($countRow,$page,$search);
        $dataCompany = Models\Company::all();
        $dataProduct = Models\Product::all();
        $dataStoreName = Models\Storename::all();
        $dataStaff = Models\Staffs::all();
        if(false==empty($id)){
            $purchases=Models\Purchases::find($id);
        }
        // Return to view
        return view('purchases.viewPurchases', array('purchases'=>$purchases,'dataStaff' => $dataStaff, 'dataPurchases' => $dataPurchases, 'dataStoreName' => $dataStoreName, 'dataCompany' => $dataCompany, 'dataProduct' => $dataProduct));
    }

    public function removePurchasesDetail(){
        $id = Input::get('id', 0);
        $sumTotalPrice=0;
        $dataPurchasesDetail=Models\PurchasesDetail::find($id);
        $store=new Models\Stores();
        $sumTotalPrice -= ($dataPurchasesDetail->purchasesdetail_productcount * $dataPurchasesDetail->purchasesdetail_productprice);
        $store->updateStore($dataPurchasesDetail->product_id, $dataPurchasesDetail->storename_id, $dataPurchasesDetail->purchasesdetail_productcount, 'plus');
        $newPurchases = Models\Purchases::find($dataPurchasesDetail->purchases_id);
        $newPurchases->purchases_sum +=$sumTotalPrice;
        $newPurchases->purchases_total = ($newPurchases->purchases_sum + (($newPurchases->purchases_sum * $newPurchases->purchases_vat) / 100)) - $newPurchases->purchases_discount;
        $newPurchases->save();
        $dataPurchasesDetail->delete();
        return Redirect::to('/purchases/viewProduct?id='.$dataPurchasesDetail->purchases_id);
    }

    public function removeListProduct()
    {
        //1. Get input  and declare variable
        $id = Input::get('id', 0);
        $listProduct = array();
        if (Session::has('listProduct')) {
            $listProduct = Session::get('listProduct');
        }

        //2. Process
        if (false == empty($listProduct[$id])) {
            array_splice($listProduct, $id, 1);
            Session::set('listProduct', $listProduct);
            $dataCompany = Models\Company::all();
            $dataProduct = Models\Product::all();
            $dataStoreName = Models\Storename::all();
            $dataStaff = Models\Staffs::all();
            //3. Return to view
            return view('purchases.manage', array('dataStaff' => $dataStaff, 'dataStoreName' => $dataStoreName, 'dataCompany' => $dataCompany, 'dataProduct' => $dataProduct, 'listProduct' => $listProduct));
        } else {
            //false Redirect
            return Redirect::to('/purchases/openManage')->with('error', 'ไม่สามารถ แก้ไขข้อมูลสินค้า สำเร็จ');
        }
    }

    public function viewPurchases()
    {
        $search = Input::get('search', '');
        $page = Input::get('page', 1);
        $countRow = 30;
        // Process
        $purchases=new Models\Purchases();
        $dataPurchases = $purchases->findAllPurchasesAndPaginate($countRow,$page,$search);

        $dataCompany = Models\Company::all();
        $dataProduct = Models\Product::all();
        $dataStoreName = Models\Storename::all();
        $dataStaff = Models\Staffs::all();
        // Return to view
        return view('purchases.viewPurchases', array('dataStaff' => $dataStaff, 'dataPurchases' => $dataPurchases, 'dataStoreName' => $dataStoreName, 'dataCompany' => $dataCompany, 'dataProduct' => $dataProduct));
    }

    public function openManage()
    {
        //1. Get input  and declare variable
        Session::forget('listProduct');
        //2. Process
        $dataCompany = Models\Company::all();
        $dataProduct = Models\Product::all();
        $dataStoreName = Models\Storename::all();
        $dataStaff = Models\Staffs::all();
        //3. Return to view
        return view('purchases.manage', array('dataStaff' => $dataStaff, 'dataStoreName' => $dataStoreName, 'dataCompany' => $dataCompany, 'dataProduct' => $dataProduct));
    }

    public function viewProduct()
    {
        //1. Get input  and declare variable
        $purchasesDetail = New Models\PurchasesDetail();
        $id = Input::get('id', 0);
        $listProduct = array();
        if (false == empty($id)) {
            $listProduct = $purchasesDetail->findById($id);
        }
        //2. Process
        $dataCompany = Models\Company::all();
        $dataProduct = Models\Product::all();
        $dataStoreName = Models\Storename::all();
        $dataStaff = Models\Staffs::all();
        $dataPurchases=Models\Purchases::find($id);
        //3. Return to view
        return view('purchases.manage', array('dataPurchases'=>$dataPurchases,'id' => $id, 'dataStaff' => $dataStaff, 'dataStoreName' => $dataStoreName, 'dataCompany' => $dataCompany, 'dataProduct' => $dataProduct, 'listProduct' => $listProduct));
    }

    public function addPurchases()
    {
        //Get input  and declare variable
        $store = new Models\Stores();
        $sumTotalPrice = 0;
        $purchases_id = Input::get('purchases_id', 0);
        $purchases_detail = Input::get('purchases_detail', '');
        $purchases_vat = Input::get('purchases_vat', 0);
        $purchases_discount = Input::get('purchases_discount', 0);
        $purchases_number = Input::get('purchases_number', 0);
        $company_id = Input::get('company_id', 0);
        $id_member_record = Input::get('id_member_record', '');
        $id_member_toget = Input::get('id_member_toget', '');
        $member_toget_date = Input::get('member_toget_date', '');
        $purchases_date = Input::get('purchases_date', '');
        //set rules
        $rules = array(
            'purchases_vat' => 'integer',
            'purchases_discount' => 'integer',
            'purchases_number' => 'required',
            'company_id' => 'required',
            'id_member_record' => 'required',
            'id_member_toget' => 'required',
            'member_toget_date' => 'required',
            'purchases_date' => 'required',
        );
        //validate input
        $validator = Validator::make(Input::all(), $rules);
        //check validate
        if ($validator->fails()) {
            // false validator Redirect
            return Redirect::to('/purchases/viewPurchases')
                ->withErrors($validator)
                ->withInput();
        } else {
            //true validator save data purchases
            //create Purchases
            if(false==empty($purchases_id)){
                $newPurchases=Models\Purchases::find($purchases_id);
            }else {
                $newPurchases = new Models\Purchases();
            }
            $newPurchases->company_id = $company_id;
            $newPurchases->id_member_record = $id_member_record;
            $newPurchases->id_member_toget = $id_member_toget;
            $newPurchases->purchases_date = $purchases_date;
            $newPurchases->member_toget_date = $member_toget_date;
            $newPurchases->purchases_number = $purchases_number;
            $newPurchases->purchases_vat = (false==empty($purchases_vat)?$purchases_vat:0);
            $newPurchases->purchases_discount = (false==empty($purchases_discount)?$purchases_discount:0);
            $newPurchases->purchases_detail = $purchases_detail;
            $newPurchases->purchases_sum = 0;
            $newPurchases->purchases_total = 0;
            $newPurchases->purchases_note = '';
            $newPurchases->save();
            return Redirect::to('/purchases/viewPurchases');
        }
    }

    public function addProduct()
    {
        //get ProductList
        //Get input  and declare variable
        $productId = Input::get('productId', 0);
        $productCount = Input::get('productCount', 0);
        $storeId = Input::get('storeId', 0);
        $productPrice = Input::get('productPrice', 0);
        $purchases_id = Input::get('purchases_id', 0);
        //set rules
        $rules = array(
            'productId' => 'required',
            'productCount' => 'required',
            'storeId' => 'required',
            'productPrice' => 'required',
        );
        //validate input
        $validator = Validator::make(Input::all(), $rules);
        //check validate
        if ($validator->fails()) {
            // false Redirect
            $messages = $validator->messages();
            return Redirect::to('/purchases/viewProduct?id='.$purchases_id)
                ->withErrors($validator)
                ->withInput(Input::except('productId', 'productCount', 'productPrice', 'storeId'));
        } else {
            //true save data addProduct
            $store = new Models\Stores();
            $sumTotalPrice = 0;
            $newPurchasesDetail = new Models\PurchasesDetail();
            $newPurchasesDetail->purchases_id = $purchases_id;
            $newPurchasesDetail->product_id = $productId;
            $newPurchasesDetail->storename_id = $storeId;
            $newPurchasesDetail->id_member_record = null;
            $newPurchasesDetail->purchasesdetail_dateproduction = null;
            $newPurchasesDetail->purchasesdetail_dateexpiration = null;
            $newPurchasesDetail->purchasesdetail_dateproductin = date('Y-m-d H:i:s');;
            $newPurchasesDetail->purchasesdetail_productcount = $productCount;
            $newPurchasesDetail->purchasesdetail_productprice = $productPrice;
            if ($newPurchasesDetail->save()) {
                $sumTotalPrice += ($productCount * $productPrice);
                $store->updateStore($productId, $storeId, $productCount, 'plus');
            }
            $newPurchases = Models\Purchases::find($purchases_id);
            $newPurchases->purchases_sum +=$sumTotalPrice;
            $newPurchases->purchases_total = ($newPurchases->purchases_sum + (($newPurchases->purchases_sum * $newPurchases->purchases_vat) / 100)) - $newPurchases->purchases_discount;
            $newPurchases->save();
//            array_push($listProduct,$dataPurchase->addProduct($productId,$productCount,$productPrice,$storeId));
            //set Session
//            Session::set('listProduct', $listProduct);
            // Redirect
            return Redirect::to('/purchases/viewProduct?id=' . $purchases_id);
        }
    }

    public function PurchaseRequest()
    {
        //1. Get input data
        $page = Input::get('page', 1);
        $limit = Input::get('limit', 10);

        //2. Variable declaration
        $result = array('data' => array(), 'page' => 1, 'limit' => 10);
        $start = ($page - 1) * $limit;

        //3. Process
        try {
            $sql = "SELECT pr.*,s.staff_firstname,s.staff_lastname,r.staff_firstname purchase_firstname,r.staff_lastname purchase_lastname FROM purchase_requests pr
LEFT JOIN staffs s ON pr.requester=s.staff_id
LEFT JOIN staffs r ON pr.recipient=r.staff_id
WHERE pr.deleted_at IS NULL
ORDER BY pr.accept,pr.created_at DESC
LIMIT $start,$limit";
            $data = DB::select(DB::raw($sql));
            $result['data'] = $data;
        } catch (\Exception $e) {
            $msg = $e->getFile() . '|' . $e->getLine() . ':' . $e->getMessage();
            $result['desc'] = $msg;
            \Log::error($msg);
        }
        //4. Return
        return view('purchases.request', $result);
    }

    public function getPRProductList()
    {
        //1. Get input
        $id = Input::get('id', 0);

        //2. Variable declaration
        $result = array('success' => true, 'desc' => 'Operation Success!!', 'data' => array());

        //3. Process
        try {
            $sql = "SELECT prl.*,p.product_name,pu.unit_name FROM  pr_lists prl
LEFT JOIN products p ON prl.product_id=p.product_id
LEFT JOIN ref_product_units pu ON p.product_unit = pu.unit_id
WHERE prl.pr_id=$id
AND prl.deleted_at IS NULL";
            $data = DB::select(DB::raw($sql));
            $result['data'] = $data;
        } catch (\Exception $e) {
            $result['success'] == false;
            $msg = $e->getFile() . '|' . $e->getLine() . ':' . $e->getMessage();
            $result['desc'] = $msg;
            \Log::error($msg);
        }
        return json_encode($result);
    }

    public function acceptPR()
    {
        //1. Get input
        $id = Input::get('id', 0);

        //2. Variable declaration
        $result = array('success' => true, 'desc' => 'Operation Success!!', 'data' => array());

        //3. Process
        try {
            $staff = Session::get('user');
            $pr = Models\PurchaseRequest::find($id);
            $pr->accept = 'Y';
            $pr->recipient=$staff->staff_id;
            $pr->save();
        } catch (\Exception $e) {
            $result['success'] == false;
            $msg = $e->getFile() . '|' . $e->getLine() . ':' . $e->getMessage();
            $result['desc'] = $msg;
            \Log::error($msg);
        }

        //4. Return
        return json_encode($result);
    }

    public function delPR()
    {
        //1. Get input
        $id = Input::get('id', 0);

        //2. Variable declaration
        $result = array('success' => true, 'desc' => 'Operation Success!!', 'data' => array());

        //3. Process
        try {
            $staff = Session::get('user');
            $pr = Models\PurchaseRequest::find($id);
            $pr->delete();
        } catch (\Exception $e) {
            $result['success'] == false;
            $msg = $e->getFile() . '|' . $e->getLine() . ':' . $e->getMessage();
            $result['desc'] = $msg;
            \Log::error($msg);
        }

        //4. Return
        return json_encode($result);
    }

    public function delPRList()
    {
        //1. Get input
        $id = Input::get('id', 0);

        //2. Variable declaration
        $result = array('success' => true, 'desc' => 'Operation Success!!', 'data' => array());

        //3. Process
        try {
            $staff = Session::get('user');
            $pr = Models\PRList::find($id);
            $pr->delete();
        } catch (\Exception $e) {
            $result['success'] == false;
            $msg = $e->getFile() . '|' . $e->getLine() . ':' . $e->getMessage();
            $result['desc'] = $msg;
            \Log::error($msg);
        }

        //4. Return
        return json_encode($result);
    }

    public function getProductList()
    {

        //1. Variable declaration
        $result = array('success' => true, 'desc' => 'Operation Success!!', 'data' => array());

        //2. Process
        try {
            $data = Models\Product::all();
            $result['data'] = $data;
            $sql = "SELECT p.product_id,p.product_name,pu.unit_name FROM products p
LEFT JOIN ref_product_units pu
ON pu.unit_id=p.product_unit";
            $data = DB::select(DB::raw($sql));
            $result['data']=$data;
        } catch (\Exception $e) {
            $result['success'] == false;
            $msg = $e->getFile() . '|' . $e->getLine() . ':' . $e->getMessage();
            $result['desc'] = $msg;
            \Log::error($msg);
        }

        //3. Return
        return json_encode($result);
    }

    public function addProductToPR()
    {
        //1. Get input
        $id = Input::get('id', 0);
        $pid = Input::get('product', '000');
        $quantity = Input::get('quantity', 0);

        //2. Variable declaration
        $result = array('success' => true, 'desc' => 'Operation Success!!', 'data' => array());

        //3. Process
        try {
            $prproduct = new Models\PRList();
            $prproduct->pr_id = $id;
            $prproduct->product_id = $pid;
            $prproduct->quantity = $quantity;
            $prproduct->save();
        } catch (\Exception $e) {
            $result['success'] == false;
            $msg = $e->getFile() . '|' . $e->getLine() . ':' . $e->getMessage();
            $result['desc'] = $msg;
            \Log::error($msg);
        }

        //4. Return
        return json_encode($result);
    }

    public function addPR()
    {
        //1. Get input
        $purpose = Input::get('purpose', 0);
        $staff = Session::get('user');

        //2. Variable declaration
        $result = array('success' => true, 'desc' => 'Operation Success!!', 'data' => array());

        //3. Process
        try {
            $pr = new Models\PurchaseRequest();
            $pr->purpose = $purpose;
            $pr->requester = $staff->staff_id;
            $pr->recipient = $staff->staff_id;
            $pr->save();
        } catch (\Exception $e) {
            $result['success'] == false;
            $msg = $e->getFile() . '|' . $e->getLine() . ':' . $e->getMessage();
            $result['desc'] = $msg;
            \Log::error($msg);
        }

        //4. Return
        return json_encode($result);
    }

}