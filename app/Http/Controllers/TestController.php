<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Appitventures\Phpgmaps\Facades\Phpgmaps as Gmaps;

class TestController extends Controller {

    public function map() {
        $config = array();
        $config['center'] = 'auto';
        $config['onboundschanged'] = 'if (!centreGot) {
            var mapCentre = map.getCenter();
            marker_0.setOptions({
                position: new google.maps.LatLng(mapCentre.lat(), mapCentre.lng())
            });
        }
        centreGot = true;';

        Gmaps::initialize($config);

        // set up the marker ready for positioning
        // once we know the users location
        $marker = array();
        Gmaps::add_marker($marker);

        $map = Gmaps::create_map();
        echo "<html><head><script type=\"text/javascript\">var centreGot = false;</script>".$map['js']."</head><body>".$map['html']."</body></html>";
    }

}