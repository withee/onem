<?php namespace App\Http\Controllers;
/**
 * Created by PhpStorm.
 * User: sootipong
 * Date: 5/26/2015
 * Time: 1:55 AM
 */
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models;
use Illuminate\Support\Facades\Response;
use Request;
use Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class ControlBuildController extends Controller
{
    public function remove(){
        //1. Get input  and declare variable
        $id=Input::get('id',0);
        $result = array();
        //2. Process
        $deleteControlBuild=new Models\ControlBuild();
        $result=$deleteControlBuild->deleteControlBuild($id);
        //3. Redirect
        if('error'==$result['status']){
            return Redirect::to('/controlBuild/viewControlBuild')->with('error', 'ไม่สามารถ ลบข้อมูลงานผลิตเเละโครงการ สำเร็จ');
        } else {
            return Redirect::to('/controlBuild/viewControlBuild')->with($result['status'],$result['desc']);
        }
    }

    public function addBuildLog(){
        $id = Input::get('id', 0);
        if(false==empty($id)){
            $dataStaff = Models\Staffs::all();
            return view('controlBuild.addBuildLog', array('dataStaff'=>$dataStaff));
        }else{
            return Redirect::to('/controlBuild/viewControlBuild');
        }
    }

    public function viewControlBuildClose()
    {
//        $controlBuildProduct=new Models\ControlBuildProduct();
//        $listProduct=array();
//        $listProduct=$controlBuildProduct->getProductlistBuildDefine()
        $ControlBuild=new Models\ControlBuild();
        $dataStaff = Models\Staffs::all();
        $dataProduct=Models\Product::all();
        $dataControlBuild=$ControlBuild->allControlBuildClose();
        return view('controlBuild.viewControlBuildClose', array('dataStaff'=>$dataStaff,'dataControlBuild'=>$dataControlBuild,'dataProduct'=>$dataProduct));
    }

    public function viewControlBuild()
    {
//        $controlBuildProduct=new Models\ControlBuildProduct();
//        $listProduct=array();
//        $listProduct=$controlBuildProduct->getProductlistBuildDefine()
        $ControlBuild=new Models\ControlBuild();
        $dataStaff = Models\Staffs::all();
        $dataProduct=Models\Product::all();
        $dataControlBuild=$ControlBuild->allControlBuild();
        return view('controlBuild.viewControlBuild', array('dataStaff'=>$dataStaff,'dataControlBuild'=>$dataControlBuild,'dataProduct'=>$dataProduct));
    }

    public function addProductDefine(){
        $id = Input::get('id', 0);

        $controlBuildProduct=new Models\ControlBuildProduct();
        if(false==empty($id)){
            $dataProduct=Models\Product::all();
            $listProduct=$controlBuildProduct->findById($id);
            //var_dump($listProduct);exit;
            return view('controlBuild.manage', array('listProduct'=>$listProduct,'dataProduct'=>$dataProduct,'id'=>$id));
        }else{
            return Redirect::to('/controlBuild/viewControlBuild');
        }
    }

    public function addProductDeteriorate(){
        //Get input  and declare variable
        $store = new Models\Stores();
        $deteriorate_control_build_id=Input::get('deteriorate_control_build_id',0);
        $deteriorateProduct_id = Input::get('deteriorateProduct_id', 0);
        $deteriorateProduct_count = Input::get('deteriorateProduct_count', 0);
        $deteriorateProduct_detail=Input::get('deteriorateProduct_detail','');
        //set rules
        $rules = array(
            'deteriorate_control_build_id' => 'required',
            'deteriorateProduct_id' => 'required',
            'deteriorateProduct_detail' => 'required',
            'deteriorateProduct_count' => 'required|integer',
        );
        //validate input
        $validator = Validator::make(Input::all(), $rules);
        //check validate
        if ($validator->fails()) {
            // false Redirect
            $messages = $validator->messages();
            return Redirect::to('/controlBuild/viewControlBuild')
                ->withErrors($validator)
                ->withInput(Input::except('deteriorate_control_build_id','deteriorateProduct_id','deteriorateProduct_count','deteriorateProduct_detail'));
        } else {
            if (Session::has('user')) {
                $user = Session::get('user');
            }
            $findProduct=Models\Product::find($deteriorateProduct_id);
            $newControlBuildLog=new Models\ControlBuildLog();
            $newControlBuildLog->control_build_id=$deteriorate_control_build_id;
            $newControlBuildLog->control_build_log_type='deteriorate';
            $newControlBuildLog->staff_id=$user['staff_id'];
            $newControlBuildLog->bring_id=null;
            $newControlBuildLog->control_build_product_id=null;
            $newControlBuildLog->control_build_log_date=null;
            $newControlBuildLog->control_build_log_detail='ชำรุด! '.$findProduct->product_name.' จำนวน '.$deteriorateProduct_count.' สาเหตุ '.$deteriorateProduct_detail;
            if($newControlBuildLog->save()){
                $newControlBuildProduct=new Models\ControlBuildProduct();
                $newControlBuildProduct->control_build_id=$deteriorate_control_build_id;
                $newControlBuildProduct->product_id=$deteriorateProduct_id;
                $newControlBuildProduct->control_build_product_count=$deteriorateProduct_count;
                $newControlBuildProduct->control_build_product_type='deteriorate';
                if( $newControlBuildProduct->save()) {
                    $store->updateStore($deteriorateProduct_id, 1, $deteriorateProduct_count, 'plus');
                }
                return Redirect::to('/controlBuild/viewControlBuild');
            }
        }

    }

    public function addProductManufacture(){
        //Get input  and declare variable
        $store = new Models\Stores();
        $manufactureProduct_id = Input::get('manufactureProduct_id', 0);
        $manufactureProduct_count = Input::get('manufactureProduct_count', 0);
        $manufacture_control_build_id=Input::get('manufacture_control_build_id',0);
        //set rules
        $rules = array(
            'manufactureProduct_id' => 'required',
            'manufactureProduct_count' => 'required|integer',
        );
        //validate input
        $validator = Validator::make(Input::all(), $rules);
        //check validate
        if ($validator->fails()) {
            // false Redirect
            $messages = $validator->messages();
            return Redirect::to('/controlBuild/viewControlBuild')
                ->withErrors($validator)
                ->withInput(Input::except('manufactureProduct_id','manufactureProduct_count'));
        } else {
            //create control_build_log
            if (Session::has('user')) {
                $user = Session::get('user');
            }
            $findProduct=Models\Product::find($manufactureProduct_id);
            $newControlBuildLog=new Models\ControlBuildLog();
            $newControlBuildLog->control_build_id=$manufacture_control_build_id;
            $newControlBuildLog->control_build_log_type='manufacture';
            $newControlBuildLog->staff_id=$user['staff_id'];
            $newControlBuildLog->bring_id=null;
            $newControlBuildLog->control_build_product_id=null;
            $newControlBuildLog->control_build_log_date=null;
            $newControlBuildLog->control_build_log_detail='รับ '.$findProduct->product_name.' ผลิตเสร็จ จำนวน '.$manufactureProduct_count;
            if($newControlBuildLog->save()){
                $newControlBuildProduct=new Models\ControlBuildProduct();
                $newControlBuildProduct->control_build_id=$manufacture_control_build_id;
                $newControlBuildProduct->product_id=$manufactureProduct_id;
                $newControlBuildProduct->control_build_product_count=$manufactureProduct_count;
                $newControlBuildProduct->control_build_product_type='manufacture';
                if($newControlBuildProduct->save()) {
                    $store->updateStore($manufactureProduct_id, 2, $manufactureProduct_count, 'plus');
                }
                return Redirect::to('/controlBuild/viewControlBuild');
            }
        }
    }

    public function addProductBuild(){
        //Get input  and declare variable
        $control_build_id = Input::get('control_build_id', '');
        $productId = Input::get('productId', 0);
        $productCount = Input::get('productCount', 0);
        //set rules
        $rules = array(
            'productId' => 'required',
            'productCount' => 'required|integer',
        );
        //validate input
        $validator = Validator::make(Input::all(), $rules);
        //check validate
        if ($validator->fails()) {
            // false Redirect
            $messages = $validator->messages();
            return Redirect::to('/controlBuild/addProductDefine?id='.$control_build_id)
                ->withErrors($validator)
                ->withInput(Input::except('productId','productCount'));

        } else {
//            add ControlBuildProduct
            $newControlBuildProduct=new Models\ControlBuildProduct();
            $newControlBuildProduct->control_build_id=$control_build_id;
            $newControlBuildProduct->product_id=$productId;
            $newControlBuildProduct->control_build_product_count=$productCount;
            $newControlBuildProduct->control_build_product_type='define';
            $newControlBuildProduct->save();
            //Redirect
            return Redirect::to('/controlBuild/addProductDefine?id='.$control_build_id);
        }
    }

    public function addControlBuild(){
        //get ProductList
        //Get input  and declare variable
        $status=true;
        $build_type='';
        $control_build_create_date = Input::get('control_build_create_date', '');
        $control_build_type_Mf = Input::get('control_build_type_Mf', false);
        $control_build_type_IS = Input::get('control_build_type_IS', false);
        $control_build_type_OT = Input::get('control_build_type_OT', false);
        $control_build_name = Input::get('control_build_name', '');
        $control_build_detail = Input::get('control_build_detail', '');
        $product_id=Input::get('product_id',0);
        $product_count=Input::get('product_count',0);
        //set rules
        $rules = array(
            'product_id' => 'required',
            'product_count' => 'required|integer',
            'control_build_create_date' => 'required',
            'control_build_name' => 'required',
        );
        //validate input
        $validator = Validator::make(Input::all(), $rules);
        //check validate
        if ($validator->fails()) {
            // false Redirect
            $messages = $validator->messages();
            return Redirect::to('/controlBuild/viewControlBuild')
                ->withErrors($validator)
                ->withInput(Input::except('control_build_create_date', 'control_build_name'));
        } else {
            //new ControlBuild
            $newControlBuild=new Models\ControlBuild();
            $newControlBuild->control_build_create_date=$control_build_create_date;

            if(false==empty($control_build_type_Mf)){
                if(!$status){
                   $build_type.=',';
                }else{
                    $status=false;
                }
                $build_type.='ผลิต';
            }

            if(false==empty($control_build_type_IS)){
                if(!$status){
                    $build_type.=',';
                }else{
                    $status=false;
                }
                $build_type.='ติดตั้ง';
            }

            if(false==empty($control_build_type_OT)){
                if(!$status){
                    $build_type.=',';
                }else{
                    $status=false;
                }
                $build_type.='อื่น';
            }
            $newControlBuild->control_build_type=$build_type;
            $newControlBuild->control_build_name=$control_build_name;
            $newControlBuild->control_build_detail=$control_build_detail;
            $newControlBuild->lot_No=null;
            $newControlBuild->control_build_complete_date=null;
            if($newControlBuild->save()){
                $newControlBuildProduct=new Models\ControlBuildProduct();
                $newControlBuildProduct->control_build_id=$newControlBuild->control_build_id;
                $newControlBuildProduct->product_id=$product_id;
                $newControlBuildProduct->control_build_product_count=$product_count;
                $newControlBuildProduct->control_build_product_type='define';
                $newControlBuildProduct->save();
            }
            //Redirect
            return Redirect::to('/controlBuild/viewControlBuild');
        }
    }
}