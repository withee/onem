<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models;
use Illuminate\Support\Facades\Response;
use Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;


class StoreNameController extends Controller
{
    public function deleteById()
    {
        //1. Get input  and declare variable
        $id=Input::get('id',0);
        $result = array();
        //2. Process
        $deleteStoreName=new Models\Storename();
        $result=$deleteStoreName->deleteStoreName($id);
        //3. Redirect
        if('error'==$result['status']){
            return Redirect::to('/storeName/manage')->with('error', 'ไม่สามารถ ลบข้อมูลหน่วยสินค้า สำเร็จ');
        } else {
            return Redirect::to('/storeName/manage')->with($result['status'],$result['desc']);
        }
    }

    public function search(){
        //1. Get input  and declare variable
        $search=Input::get('search','');
        $page=Input::get('page',1);
        $dataStoreName=new Models\Storename();
        $countRow=30;
        //2. Process
        $storeName=$dataStoreName->findAllStoreNameAndPaginate($countRow,$page,$search);
        //3. Return to view
        return view('storename.manage', array('storeName' => $storeName,'page'=>$page,'search'=>$search));
    }

    public function displayById()
    {
        //1. Get input  and declare variable
        $id=Input::get('id',0);
        $page=Input::get('page',1);
        $search=Input::get('search','');
        $editStoreName=new Models\Storename();
        $countRow=30;
        //2. Process
        if (false==empty($id)) {
            $dataStoreName = $editStoreName->findByIdStoreName($id);
        }
        $storeName=$editStoreName->findAllStoreNameAndPaginate($countRow,$page,$search);
        //3. Return to view
        return view('storename.manage', array('dataStoreName' => $dataStoreName,'storeName' => $storeName,'page'=>$page,'search'=>$search));
    }

    public function openManage()
    {
        //1. Get input  and declare variable
        $page=Input::get('page',1);
        $search=Input::get('search','');
        $editStoreName=new Models\Storename();
        $countRow=30;
        //2. Process
        $storeName=$editStoreName->findAllStoreNameAndPaginate($countRow,$page,$search);
        //3. Return to view
        return view('storename.manage', array('storeName' => $storeName,'page'=>$page,'search'=>$search));
    }

    public function addStoreName()
    {
        //set Rules
        $rules = array(
            'storename_name' => 'required|unique:ref_store_name',
        );
        //validate input
        $validator = Validator::make(Input::all(), $rules);
        //check validate
        if ($validator->fails()) {
            $messages = $validator->messages();
            // false Redirect
            return Redirect::to('/storeName/manage')
                ->withErrors($validator)
                ->withInput(Input::except('storename_name'));
        } else {
            //true save data StoreName
            $newStoreName = new Models\Storename();
            $newStoreName->storename_name = Input::get('storename_name');
            $newStoreName->save();
            return Redirect::to('/storeName/manage');
        }
    }

    public function editStoreName()
    {
        // Get input  and declare variable
        $page=Input::get('page',1);
        $search=Input::get('search','');
        $id=Input::get('id');
        if (false==empty($id)) {
            //set Rules
            $rules = array(
                'storename_name' => 'required',
            );
            //validate input
            $validator = Validator::make(Input::all(), $rules);
            //check validate
            if ($validator->fails()) {
                $messages = $validator->messages();
                // false Redirect
                if(false==empty($page)&&false==empty($search)) {
                    return Redirect::to('/storeName/manage?id=' . $id . '&page=' . $page . '&search=' . $search)
                        ->withErrors($validator)
                        ->withInput(Input::except('storename_name'));
                }elseif(false==empty($page)) {
                    return Redirect::to('/storeName/manage?id=' . $id.'&page=' . $page)
                        ->withErrors($validator)
                        ->withInput(Input::except('storename_name'));
                }elseif(false==empty($search)) {
                    return Redirect::to('/storeName/manage?id=' . $id.'&search=' . $search)
                        ->withErrors($validator)
                        ->withInput(Input::except('storename_name'));
                }else {
                    return Redirect::to('/storeName/manage?id=' . $id)
                        ->withErrors($validator)
                        ->withInput(Input::except('storename_name'));
                }
            } else {
                //true save data StoreName
                $editStoreName=new Models\Storename();
                //find StroreName
                $newStoreName = $editStoreName->findByIdStoreName($id);
                if(!empty($newStoreName)){
                    //set
                    $newStoreName->storename_name = Input::get('storename_name');
                    //save
                    $newStoreName->save();
                    //Redirect
                    if(false==empty($page)&&false==empty($search)) {
                        return Redirect::to('/storeName/manage?page=' . $page . '&search=' . $search);
                    }elseif(false==empty($page)) {
                        return Redirect::to('/storeName/manage?page=' . $page);
                    }elseif(false==empty($search)) {
                        return Redirect::to('/storeName/manage?search=' . $search);
                    }else{
                        return Redirect::to('/storeName/manage');
                    }
                }else{
                    return Redirect::to('/storeName/manage')->with('error', 'ไม่สามารถ แก้ไขข้อมูลชื่อคลังสินค้าได้');
                }
            }
        }
    }
}

?>