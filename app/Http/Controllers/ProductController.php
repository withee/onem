<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models;
use Illuminate\Support\Facades\Response;
use Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;


class ProductController extends Controller
{
    public function deleteById()
    {
        //1. Get input  and declare variable
        $id = Input::get('id', 0);
        $result = array();

        //2. Process
        $deleteProduct = new Models\Product();
        $result = $deleteProduct->deleteProduct($id);

        //3. Redirect
        if ('error' == $result['status']) {
            return Redirect::to('/products/manage')->with('error', 'ไม่สามารถ ลบข้อมูลหน่วยสินค้า สำเร็จ');
        } else {
            return Redirect::to('/products/manage')->with($result['status'], $result['desc']);
        }
    }

    public function search()
    {
        //1. Get input  and declare variable
        $id = Input::get('id', 0);
        $search = Input::get('search', '');
        $page = Input::get('page', 1);
        $type = Input::get('type', 'all');
        $newProduct = new Models\Product();
        $countRow = 30;

        //2. Process
        $product = $newProduct->findAllProductAndPaginate($id, $countRow, $page, $search);
        $dataProductUnit = Models\ProductUnit::all();
        $dataProduct = new Models\Product();
        $storeName = Models\Storename::all();
        //3. Return to view
        return view('product.manage', array('type'=>$type,'page'=>$page,'storeName'=>$storeName,'dataProduct' => $dataProduct, 'product' => $product, 'dataProductUnit' => $dataProductUnit));
    }

    public function displayById()
    {
        //1. Get input  and declare variable
        $id = Input::get('id', 0);
        $page = Input::get('page', 1);
        $search = Input::get('search', '');
        $type = Input::get('type', '');
        $newProduct = new Models\Product();
        $countRow = 30;
        //2. Process
        if (false == empty($id)) {
            $dataProduct = $newProduct->findByIdProduct($id);
        }
        $product = $newProduct->findAllProductAndPaginate($id, $countRow, $page, $search);
        $storeName = Models\Storename::all();
        $dataProductUnit = Models\ProductUnit::all();

        //3. Return to view
        return view('product.manage', array('type'=>$type,'dataProductUnit' => $dataProductUnit, 'storeName' => $storeName, 'dataProduct' => $dataProduct, 'product' => $product, 'page' => $page, 'search' => $search));
    }

    public function openManage()
    {
        //1. Get input  and declare variable
        $id = Input::get('id', 0);
        $search = Input::get('search', '');
        $page = Input::get('page', 1);
        $type = Input::get('type', 'all');
        $dataProduct = new Models\Product();
        $dataProductUnit = Models\ProductUnit::all();
        $storeName=Models\Storename::all();
        $countRow = 30;

        //2. Process
        $newProduct = new Models\Product();
        $product = $newProduct->findAllProductAndPaginate($id, $countRow, $page, $search);
        //3. Return to view
        return view('product.manage', array('type'=>$type,'page'=>$page,'storeName'=>$storeName,'dataProduct' => $dataProduct, 'dataProductUnit' => $dataProductUnit, 'product' => $product));
    }

    public function editProduct()
    {
        //Get input  and declare variable
        $page = Input::get('page', 1);
        $search = Input::get('search', '');
        $id = Input::get('id');
        //set rules
        if (false == empty($id)) {
            $rules = array(
                'product_id' => 'required',
                'product_name' => 'required',
                'product_price' => 'required|integer',
                'product_unit' => 'required'
            );
            //validate input
            $validator = Validator::make(Input::all(), $rules);
            //check validate
            if ($validator->fails()) {
                // false Redirect
                $messages = $validator->messages();
                if (false == empty($page) && false == empty($search)) {
                    return Redirect::to('/products/manage?id=' . $id . '&page=' . $page . '&search=' . $search)
                        ->withErrors($validator)
                        ->withInput(Input::except('product_id', 'product_name', 'product_price', 'product_unit'));
                } elseif (false == empty($page)) {
                    return Redirect::to('/products/manage?id=' . $id . '&page=' . $page)
                        ->withErrors($validator)
                        ->withInput(Input::except('product_id', 'product_name', 'product_price', 'product_unit'));
                } elseif (false == empty($search)) {
                    return Redirect::to('/products/manage?id=' . $id . '&search=' . $search)
                        ->withErrors($validator)
                        ->withInput(Input::except('product_id', 'product_name', 'product_price', 'product_unit'));
                } else {
                    return Redirect::to('/products/manage?id=' . $id)
                        ->withErrors($validator)
                        ->withInput(Input::except('product_id', 'product_name', 'product_price', 'product_unit'));
                }
            } else {
                //true save data Product
                $editProduct = new Models\Product();
                $newProduct = $editProduct->findByIdProduct($id);
                if (false == empty($newProduct)) {
                    $newProduct->product_id = Input::get('product_id');
                    $newProduct->product_name = Input::get('product_name');
                    $newProduct->product_price = Input::get('product_price');
                    $newProduct->product_unit = Input::get('product_unit');
                    $newProduct->product_type = Input::get('product_type');
                    $newProduct->save();
                    //Redirect
                    if (false == empty($page) && false == empty($search)) {
                        return Redirect::to('/products/manage?page=' . $page . '&search=' . $search);
                    } elseif (false == empty($page)) {
                        return Redirect::to('/products/manage?page=' . $page);
                    } elseif (false == empty($search)) {
                        return Redirect::to('/products/manage?search=' . $search);
                    } else {
                        return Redirect::to('/products/manage');
                    }
                } else {
                    //Redirect
                    return Redirect::to('/products/manage')->with('error', 'ไม่สามารถ แก้ไขข้อมูลสินค้า สำเร็จ');
                }
            }
        }
    }

    public function addProduct()
    {
        //set rules
        $rules = array(
            'product_id' => 'required|unique:products',
            'product_name' => 'required|unique:products',
            'product_price' => 'required|integer',
            'product_unit' => 'required',
            'product_type' => 'required',
            'minimum' => 'required'
        );
        //validate input
        $validator = Validator::make(Input::all(), $rules);
        //check validate
        if ($validator->fails()) {
            // false validator Redirect
            return Redirect::to('/products/manage')
                ->withErrors($validator)
                ->withInput(Input::except('product_id', 'product_name', 'product_price', 'product_unit', 'product_type'));
        } else {
            //true validator save data company
            $newProduct = new Models\Product();
            $newProduct->product_id = Input::get('product_id');
            $newProduct->product_name = Input::get('product_name');
            $newProduct->product_price = Input::get('product_price');
            $newProduct->product_unit = Input::get('product_unit');
            $newProduct->product_type = Input::get('product_type');
            $newProduct->minimum=Input::get('minimum');
            $newProduct->save();
            //Redirect
            return Redirect::to('/products/manage');
        }
    }

    public function findFromStore()
    {
        //1. Get Input
        $product_id = Input::get('pid', 0);

        //2. Variable declaration
        $result = array('success' => true, 'desc' => 'Operation success!', 'data' => array());

        //3. Process
        try {
            $data = Models\Product::all();
            $result['data'] = $data;
        } catch (\Exception $e) {
            $result['success'] == false;
            $msg = $e->getFile() . '|' . $e->getLine() . ':' . $e->getMessage();
            $result['desc'] = $msg;
            \Log::error($msg);
        }

        //4. Return to view
        return view('product.findfromstore', $result);
    }


    public function findFromAllStore()
    {
        //1. Get Input
        $pid = Input::get('pid', 0);

        //2. Variable declaration
        $result = array('success' => true, 'desc' => 'Operation success!', 'data' => array());

        //3. Process
        try {
            $sql = "SELECT st.*,sn.storename_name FROM stores st
LEFT JOIN ref_store_name sn ON sn.storename_id=st.storename_id
WHERE st.product_id=$pid";
            $data = DB::select(DB::raw($sql));
            $result['data'] = $data;
        } catch (\Exception $e) {
            $result['success'] == false;
            $msg = $e->getFile() . '|' . $e->getLine() . ':' . $e->getMessage();
            $result['desc'] = $msg;
            \Log::error($msg);
        }

        //4. Return to view
        return json_encode($result);
    }
}

?>