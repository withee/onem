<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;


class StaffController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    private $rules = array(
        'staff_username' => 'required|unique:staffs',
        'staff_userpwd' => 'required|confirmed|min:4',
        'staff_email' => 'required|email|unique:staffs',
        'staff_firstname' => 'required',
        'staff_lastname' => 'required',
        'staff_nickname' => 'required',
        'staff_phonenumber' => 'required|numeric|unique:staffs'
    );

    private $loginrules = array(
        'staff_username' => 'required',
        'staff_userpwd' => 'required',
    );

    public function index()
    {
        //
    }

//ADD COMMENT STEP
    public function openManage()
    {
        //1. Variable declaration
        $type = new Models\UserRole();

        //2. Process
        $paging = Models\Staffs::paginate(10);
        $usertype = $type->getWithKey();
        $data = array('usertype' => $usertype, 'paging' => $paging);

        //3. Return
        return view('staffs.staffs_manage', $data);
    }

    public function openFormData()
    {
        //1. Variable declaration
        $type = new Models\UserRole();

        //2. Process
        $usertype = $type->getWithKey();
        $data = array('usertype' => $usertype);

        //3. ReTurn
        return view('staffs.addstaff', $data);
    }

    public function addStaff()
    {

        //1.Get input and declare variable
        $data = Request::all();
        $staff = new Models\Staffs();
        $result = array('success' => true, 'desc' => 'Operation Success!!', 'data' => array());

        //2. Validate
        $validator = Validator::make($data, $this->rules);
        if (true == $validator->fails()) {
            $messages = $validator->messages();
            $result['success'] = false;
            $result['desc'] = 'validate fail';
            $result['data'] = $messages;

        } else {

            //3. Process
            try {
                $staff->staff_username = $data['staff_username'];
                $staff->staff_userpwd = $data['staff_userpwd'];
                $staff->staff_firstname = $data['staff_firstname'];
                $staff->staff_lastname = $data['staff_lastname'];
                $staff->staff_nickname = $data['staff_nickname'];
                $staff->staff_email = $data['staff_email'];
                $staff->staff_phonenumber = $data['staff_phonenumber'];
                $staff->staff_usertype = $data['staff_usertype'];
                $staff->staff_userpwd = md5($data['staff_userpwd']);
                $staff->staff_displayname = $data['staff_nickname'];
                $staff->staff_id = Models\Staffs::initID();
                $staff->save();
            } catch (\Exception $e) {
                $result['success'] = false;
                $result['desc'] = $e->getLine() . ':' . $e->getMessage();
                //echo $e->getLine() . ':' . $e->getMessage();
            } finally {

            }
        }

        //4. Return
        //return Redirect::to('/staffs/addform')->with('success', 'เพิมข้อมูลสำเร็จ');
        return json_encode($result);

    }

    public function editStaff()
    {
        //1. Get input  and declare variable
        $data = Request::all();
        $result = array('success' => true, 'desc' => 'Operation Success!!', 'data' => array());

        //2. Process
        try {
            // Find staff object.
            $staff = Models\Staffs::where('staff_id', '=', $data['staff_id'])->firstOrFail();
            // Assigned value to staff object.
            $staff->staff_username = $data['staff_username'];
            $staff->staff_firstname = $data['staff_firstname'];
            $staff->staff_lastname = $data['staff_lastname'];
            $staff->staff_nickname = $data['staff_nickname'];
            $staff->staff_email = $data['staff_email'];
            $staff->staff_phonenumber = $data['staff_phonenumber'];
            $staff->staff_usertype = $data['staff_usertype'];
            // Compare new userpwd with old one.
            if ($data['staff_userpwd'] != $staff->staff_userpwd) {
                $staff->staff_userpwd = md5($data['staff_userpwd']);
            }
            $staff->staff_displayname = $data['staff_nickname'];

            // Save staff object to DB.
            $staff->save();
        } catch (\Exception $e) {
            $result['success'] = false;
            $result['desc'] = $e->getLine() . ':' . $e->getMessage();
        } finally {

        }

        //3. Return to view
        //return Redirect::to('/staffs/manage');
        return json_encode($result);

    }

    public function displayById()
    {
        //1. Get input adn declare variable
        $uid = Request::input('uid');
        $result = array('success' => true, 'desc' => 'Operation Success!!', 'data' => array());
        $type = new Models\UserRole();

        try {

            //2. Process
            $staff = Models\Staffs::where('staff_id', '=', $uid)->firstOrFail();// add this method to model.
            $result['data'] = $staff;
            $usertype = $type->getWithKey();
            $data = array('usertype' => $usertype, 'user' => $staff);
        } catch (\Exception $e) {
            //echo $e->getMessage();
            $result['success'] = false;
            $result['desc'] = $e->getMessage();
        } finally {

        }

        //3. Return to view
        //return view('staffs.editstaff', $data);
        return json_encode($result);
    }

    public function deleteById()
    {
        //1. Get input and declare variable
        $uid = Request::input('uid');
        $result = array('success' => true, 'desc' => 'Operation Success!!', 'data' => array());

        try {

            //2. Process
            $staff = new Models\Staffs();
            $staff->deleteStaff($uid);

        } catch (\Exception $e) {
            $result['success'] = false;
            $result['desc'] = $e->getLine() . ':' . $e->getMessage();
        } finally {

        }
        //3. Redirect to route
        return Redirect::to('/staffs/manage');
    }

    public function gotoLogin()
    {
        //1. Get input
        $data = Request::all();
        $result = array('success' => true, 'desc' => 'Operation Success!!', 'data' => array());

        //2. Login process.
        $user = new Models\Staffs();
        $validator = Validator::make($data, $this->loginrules);
        if (true == $validator->fails()) {
            $result['data'] = $validator->messages();
            $result['success'] = false;
            $result['desc'] = 'validate failed.';
        } else {
            $result = $user->verifiedByUser($data['staff_username'], $data['staff_userpwd']);
        }
        //3. Response to view.
        return json_encode($result);
//        if (true == $result['success']) {
//            return Redirect::to('/products/manage');
//        } else {
//            return view('index', $result);
//        }
    }

    public function Logout()
    {
        //1. Get input
//        $data = Request::all();
//        $result = array('success' => true, 'desc' => 'Operation Success!!', 'data' => array());
//
//        //2. Login process.
//        $user = new Models\Staffs();
//        $validator = Validator::make($data, $this->loginrules);
//        if (true == $validator->fails()) {
//            $result['data'] = $validator->messages();
//            $result['success'] = false;
//            $result['desc'] = 'validate failed.';
//        } else {
//            $result = $user->verifiedByUser($data['staff_username'], $data['staff_userpwd']);
//        }
//        //3. Response to view.
//        return json_encode($result);
        Session::forget('user');
        return view('index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}
