<?php namespace App\Http\Controllers;

/**
 * Created by PhpStorm.
 * User: sootipong
 * Date: 11/17/2015
 * Time: 10:35
 */
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Models;
use Illuminate\Support\Facades\Response;
use Request;
use Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class FactoryController extends Controller
{
    public function manageCustomers()
    {
        $id = Input::get('id', '');
        $search = Input::get('search', '');
        $page = Input::get('page', 1);
        $customers = new Models\Customers();
        if (!empty($id)) {
            $customers = Models\Customers::find($id);
        }
        $countRow = 30;
        $dataCustomers = $customers->findAllCustomersAndPaginate($countRow, $page, $search);
        return view('factory.manageCustomers', array('customers' => $customers, 'dataCustomers' => $dataCustomers));
    }

    public function searchCustomers()
    {
        $search = Input::get('search', '');
        $page = Input::get('page', 1);
        $customers = new Models\Customers();
        $countRow = 30;
        $dataCustomers = $customers->findAllCustomersAndPaginate($countRow, $page, $search);
        return view('factory.manageCustomers', array('customers' => $customers, 'dataCustomers' => $dataCustomers));
    }

    public function deleteCustomers()
    {
        $id = Input::get('id', 0);
        if (!empty($id)) {
            $customers = Models\Customers::find($id);
            if (!empty($customers)) {
                $customers->delete();
                return Redirect::to('/factory/manageCustomers');
            } else {
                return Redirect::to('/factory/manageCustomers');
            }
        } else {
            return Redirect::to('/factory/manageCustomers');
        }
    }

    public function addCustomers()
    {
        if (!empty($_POST)) {
            $customers_id = Input::get('customers_id', 0);
            $customers_name = Input::get('customers_name', '');
            $customers_telephone = Input::get('customers_telephone', '');
            $customers_address = Input::get('customers_address', '');
            //set rules
            $rules = array(
                'customers_name' => 'required',
//                'customers_telephone' => 'required',
                'customers_address' => 'required',
            );
            //validate input
            $validator = Validator::make(Input::all(), $rules);
            //check validate
            if ($validator->fails()) {
                // false Redirect
                $messages = $validator->messages();
                return Redirect::to('/factory/manageCustomers')
                    ->withErrors($validator)
                    ->withInput(Input::except('customers_name', 'customers_telephone', 'customers_address'));
            } else {
                $newCustomers = new Models\Customers();
                if (!empty($customers_id)) {
                    $newCustomers = Models\Customers::find($customers_id);
                }
                $newCustomers->customers_name = $customers_name;
                $newCustomers->customers_telephone = $customers_telephone;
                $newCustomers->customers_address = $customers_address;
                if ($newCustomers->save()) {
                    return Redirect::to('/factory/manageCustomers');
                }
            }
        }
    }

    public function checkMaterial()
    {
        $orderId = Input::get('orderId', 0);
        $materialId = Input::get('materialId', 0);
        $orderProductId=Input::get('orderProductId',0);
        $search = Input::get('search', '');
        $page = Input::get('page', 1);
        $countRow = 30;
        $CustomersOrderMaterial=new Models\CustomersOrderMaterial();
        if(!empty($materialId)){
            $CustomersOrderMaterial=Models\CustomersOrderMaterial::find($materialId);
        }
        $CustomersOrderProduct = new Models\CustomersOrderProduct();
        $dataCustomersOrderProduct = $CustomersOrderProduct->findAllCustomersAndPaginate($countRow, $page, $orderId, $search);
        $order = Models\CustomersOrder::find($orderId);
        return view('factory.checkMaterial', array('CustomersOrderMaterial'=>$CustomersOrderMaterial,'dataCustomersOrderProduct' => $dataCustomersOrderProduct,'orderProductId'=>$orderProductId, 'orderId' => $orderId, 'CustomersOrderProduct' => $CustomersOrderProduct, 'order' => $order));
    }

    public function deleteCustomersOrderProduct()
    {
        $id = Input::get('id', 0);
        $orderId = Input::get('orderId', 0);
        if (!empty($id)) {
            $CustomersOrderProduct = Models\CustomersOrderProduct::find($id);
            if (!empty($CustomersOrderProduct)) {
                $CustomersOrderProduct->delete();
                return Redirect::to('/factory/checkMaterial?orderId=' . $orderId);
            } else {
                return Redirect::to('/factory/checkMaterial?orderId=' . $orderId);
            }
        } else {
            return Redirect::to('/factory/checkMaterial?orderId=' . $orderId);
        }
    }

    public function deleteCustomersOrderProductCompensate()
    {
        $id = Input::get('id', 0);
        $orderId = Input::get('orderId', 0);
        if (!empty($id)) {
            $CustomersOrderProduct = Models\CustomersOrderProduct::find($id);
            if (!empty($CustomersOrderProduct)) {
                $CustomersOrderProduct->delete();
                return Redirect::to('/factory/Compensate?orderId=' . $orderId);
            } else {
                return Redirect::to('/factory/Compensate?orderId=' . $orderId);
            }
        } else {
            return Redirect::to('/factory/Compensate?orderId=' . $orderId);
        }
    }

    public function deleteCustomersOrderMaterial()
    {
        $id = Input::get('id', 0);
        $orderId = Input::get('orderId', 0);
        if (!empty($id)) {
            $CustomersOrderMaterial = Models\CustomersOrderMaterial::find($id);
            if (!empty($CustomersOrderMaterial)) {
                $CustomersOrderMaterial->delete();
                return Redirect::to('/factory/checkMaterial?orderId=' . $orderId);
            } else {
                return Redirect::to('/factory/checkMaterial?orderId=' . $orderId);
            }
        } else {
            return Redirect::to('/factory/checkMaterial?orderId=' . $orderId);
        }
    }

    public function deleteCustomersOrderMaterialCompensate()
    {
        $id = Input::get('id', 0);
        $orderId = Input::get('orderId', 0);
        if (!empty($id)) {
            $CustomersOrderMaterial = Models\CustomersOrderMaterial::find($id);
            if (!empty($CustomersOrderMaterial)) {
                $CustomersOrderMaterial->delete();
                return Redirect::to('/factory/Compensate?orderId=' . $orderId);
            } else {
                return Redirect::to('/factory/Compensate?orderId=' . $orderId);
            }
        } else {
            return Redirect::to('/factory/Compensate?orderId=' . $orderId);
        }
    }

    public function addMaterialOrder()
    {
        if (!empty($_POST)) {
            $customers_order_material_id = Input::get('customers_order_material_id', 0);
            $customers_order_product_id = Input::get('customers_order_product_id', '');
            $customers_order_material_name = Input::get('customers_order_material_name', '');
            $customers_order_material_count = Input::get('customers_order_material_count', 0);
            $customers_order_id = Input::get('customers_order_id', '');
            $storename_id = Input::get('storename_id', '');
            //set rules
            $rules = array(
                'customers_order_material_name' => 'required',
                'storename_id' => 'required',
                'customers_order_material_count' => 'required|numeric',
            );
            //validate input
            $validator = Validator::make(Input::all(), $rules);
            //check validate
            if ($validator->fails()) {
                // false Redirect
                $messages = $validator->messages();
                return Redirect::to('/factory/checkMaterial?orderId=' . $customers_order_id.'&orderProductId='.$customers_order_product_id)
                    ->withErrors($validator)
                    ->withInput(Input::except('storename_id', 'customers_order_material_name', 'customers_order_material_count'));
            } else {
                $material = explode(":",$customers_order_material_name);
                $user = Session::get('user');
                $newCustomersOrderMaterial = new Models\CustomersOrderMaterial();
                if (!empty($customers_order_material_id)) {
                    $newCustomersOrderMaterial = Models\CustomersOrderMaterial::find($customers_order_material_id);
                }
                $newCustomersOrderMaterial->storename_id = $storename_id;
                $newCustomersOrderMaterial->customers_order_product_id = $customers_order_product_id;
                $newCustomersOrderMaterial->customers_order_material_name = $material[0];
                $newCustomersOrderMaterial->customers_order_material_count = $customers_order_material_count;
                $newCustomersOrderMaterial->customers_order_material_staff = $user->staff_username;
                $newCustomersOrderMaterial->customers_order_material_status = 'New';
                if ($newCustomersOrderMaterial->save()) {
                    return Redirect::to('/factory/checkMaterial?orderId=' . $customers_order_id);
                }
            }
        }
    }

    public function addMaterialOrderCompensate()
    {
        if (!empty($_POST)) {
            $customers_order_material_id = Input::get('customers_order_material_id', 0);
            $customers_order_product_id = Input::get('customers_order_product_id', '');
            $customers_order_material_name = Input::get('customers_order_material_name', '');
            $customers_order_material_count = Input::get('customers_order_material_count', 0);
            $customers_order_id = Input::get('customers_order_id', '');
            $storename_id = Input::get('storename_id', '');
            //set rules
            $rules = array(
                'customers_order_material_name' => 'required',
                'storename_id' => 'required',
                'customers_order_material_count' => 'required|numeric',
            );
            //validate input
            $validator = Validator::make(Input::all(), $rules);
            //check validate
            if ($validator->fails()) {
                // false Redirect
                $messages = $validator->messages();
                return Redirect::to('/factory/Compensate?orderId=' . $customers_order_id)
                    ->withErrors($validator)
                    ->withInput(Input::except('storename_id', 'customers_order_material_name', 'customers_order_material_count'));
            } else {
                $material = explode(":",$customers_order_material_name);
                $user = Session::get('user');
                $newCustomersOrderMaterial = new Models\CustomersOrderMaterial();
                if (!empty($customers_order_material_id)) {
                    $newCustomersOrderMaterial = Models\CustomersOrderMaterial::find($customers_order_product_id);
                }
                $newCustomersOrderMaterial->storename_id = $storename_id;
                $newCustomersOrderMaterial->customers_order_product_id = $customers_order_product_id;
                $newCustomersOrderMaterial->customers_order_material_name = $material[0];
                $newCustomersOrderMaterial->customers_order_material_count = $customers_order_material_count;
                $newCustomersOrderMaterial->customers_order_material_staff = $user->staff_username;
                $newCustomersOrderMaterial->customers_order_material_status = 'New';
                if ($newCustomersOrderMaterial->save()) {
                    return Redirect::to('/factory/Compensate?orderId=' . $customers_order_id);
                }
            }
        }
    }

    public function addProductOrderCompensate()
    {
        if (!empty($_POST)) {
            $customers_order_product_id = Input::get('customers_order_product_id', 0);
            $customers_order_id = Input::get('customers_order_id', '');
            $customers_order_product_name = Input::get('customers_order_product_name', '');
            $customers_order_product_count = Input::get('customers_order_product_count', 0);
            $customers_order_product_type = Input::get('customers_order_product_type', '');
            $customers_order_product_price= Input::get('customers_order_product_price', 0);
            //set rules
            $rules = array(
                'customers_order_product_name' => 'required',
                'customers_order_product_count' => 'required|numeric',
                'customers_order_product_price' => 'required|numeric',
                'customers_order_product_type' => 'required',
            );
            //validate input
            $validator = Validator::make(Input::all(), $rules);
            //check validate
            if ($validator->fails()) {
                // false Redirect
                $messages = $validator->messages();
                return Redirect::to('/factory/Compensate?orderId=' . $customers_order_id)
                    ->withErrors($validator)
                    ->withInput(Input::except('customers_order_product_name', 'customers_order_product_count','customers_order_product_type','customers_order_product_price'));
            } else {
                $user = Session::get('user');
                $material = explode(":",$customers_order_product_name);
                $newCustomersOrderProduct = new Models\CustomersOrderProduct();
                if (!empty($customers_order_product_id)) {
                    $newCustomersOrderProduct = Models\CustomersOrderProduct::find($customers_order_product_id);
                }
                $newCustomersOrderProduct->customers_order_product_type=$customers_order_product_type;
                $newCustomersOrderProduct->customers_order_id = $customers_order_id;
                $newCustomersOrderProduct->customers_order_product_name = $material[0];
                $newCustomersOrderProduct->customers_order_product_count = $customers_order_product_count;
                $newCustomersOrderProduct->customers_order_product_staff = $user->staff_username;
                $newCustomersOrderProduct->customers_order_product_price=$customers_order_product_price;
                $newCustomersOrderProduct->customers_order_product_total=($customers_order_product_price*$customers_order_product_count);
                $newCustomersOrderProduct->customers_order_product_status = 'New';
                if ($newCustomersOrderProduct->save()) {
                    return Redirect::to('/factory/Compensate?orderId=' . $customers_order_id);
                }
            }
        }
    }

    public function addProductOrderEngineer()
    {
        if (!empty($_POST)) {
            $customers_order_product_id = Input::get('customers_order_product_id', 0);
            $customers_order_id = Input::get('customers_order_id', '');
            $customers_order_product_name = Input::get('customers_order_product_name', '');
            $customers_order_product_count = Input::get('customers_order_product_count', 0);
            $customers_order_product_type = Input::get('customers_order_product_type', '');
            $customers_order_product_price= Input::get('customers_order_product_price', 0);
            //set rules
            $rules = array(
                'customers_order_product_name' => 'required',
                'customers_order_product_count' => 'required|numeric',
                'customers_order_product_price' => 'required|numeric',
                'customers_order_product_type' => 'required',
            );
            //validate input
            $validator = Validator::make(Input::all(), $rules);
            //check validate
            if ($validator->fails()) {
                // false Redirect
                $messages = $validator->messages();
                return Redirect::to('/factory/PlanManufacture?orderId=' . $customers_order_id)
                    ->withErrors($validator)
                    ->withInput(Input::except('customers_order_product_name', 'customers_order_product_count','customers_order_product_type','customers_order_product_price'));
            } else {
                $user = Session::get('user');
                $material = explode(":",$customers_order_product_name);
                $newCustomersOrderProduct = new Models\CustomersOrderProduct();
                if (!empty($customers_order_product_id)) {
                    $newCustomersOrderProduct = Models\CustomersOrderProduct::find($customers_order_product_id);
                }
                $newCustomersOrderProduct->customers_order_product_type=$customers_order_product_type;
                $newCustomersOrderProduct->customers_order_id = $customers_order_id;
                $newCustomersOrderProduct->customers_order_product_name = $material[0];
                $newCustomersOrderProduct->customers_order_product_count = $customers_order_product_count;
                $newCustomersOrderProduct->customers_order_product_staff = $user->staff_username;
                $newCustomersOrderProduct->customers_order_product_price=$customers_order_product_price;
                $newCustomersOrderProduct->customers_order_product_total=($customers_order_product_price*$customers_order_product_count);
                $newCustomersOrderProduct->customers_order_product_status = 'New';
                if ($newCustomersOrderProduct->save()) {
                    return Redirect::to('/factory/PlanManufacture?orderId=' . $customers_order_id);
                }
            }
        }
    }

    public function addProductOrder()
    {
        if (!empty($_POST)) {
            $customers_order_product_id = Input::get('customers_order_product_id', 0);
            $customers_order_id = Input::get('customers_order_id', '');
            $customers_order_product_name = Input::get('customers_order_product_name', '');
            $customers_order_product_count = Input::get('customers_order_product_count', 0);
            $customers_order_product_type = Input::get('customers_order_product_type', '');
            $customers_order_product_price= Input::get('customers_order_product_price', 0);
            //set rules
            $rules = array(
                'customers_order_product_name' => 'required',
                'customers_order_product_count' => 'required|numeric',
                'customers_order_product_price' => 'required|numeric',
                'customers_order_product_type' => 'required',
            );
            //validate input
            $validator = Validator::make(Input::all(), $rules);
            //check validate
            if ($validator->fails()) {
                // false Redirect
                $messages = $validator->messages();
                return Redirect::to('/factory/checkMaterial?orderId=' . $customers_order_id)
                    ->withErrors($validator)
                    ->withInput(Input::except('customers_order_product_name', 'customers_order_product_count','customers_order_product_type','customers_order_product_price'));
            } else {
                $user = Session::get('user');
                $material = explode(":",$customers_order_product_name);
                $newCustomersOrderProduct = new Models\CustomersOrderProduct();
                if (!empty($customers_order_product_id)) {
                    $newCustomersOrderProduct = Models\CustomersOrderProduct::find($customers_order_product_id);
                }
                $newCustomersOrderProduct->customers_order_product_type=$customers_order_product_type;
                $newCustomersOrderProduct->customers_order_id = $customers_order_id;
                $newCustomersOrderProduct->customers_order_product_name = $material[0];
                $newCustomersOrderProduct->customers_order_product_count = $customers_order_product_count;
                $newCustomersOrderProduct->customers_order_product_staff = $user->staff_username;
                $newCustomersOrderProduct->customers_order_product_price=$customers_order_product_price;
                $newCustomersOrderProduct->customers_order_product_total=($customers_order_product_price*$customers_order_product_count);
                $newCustomersOrderProduct->customers_order_product_status = 'New';
                if ($newCustomersOrderProduct->save()) {
                    return Redirect::to('/factory/checkMaterial?orderId=' . $customers_order_id);
                }
            }
        }
    }

    public function addProductOrderCheckMaterial()
    {
        if (!empty($_POST)) {
            $customers_order_product_id = Input::get('customers_order_product_id', 0);
            $customers_order_id = Input::get('customers_order_id', '');
            $customers_order_product_name = Input::get('customers_order_product_name', '');
            $customers_order_product_count = Input::get('customers_order_product_count', 0);
            $customers_order_product_type = Input::get('customers_order_product_type', '');
            $customers_order_product_price= Input::get('customers_order_product_price', 0);
            //set rules
            $rules = array(
                'customers_order_product_name' => 'required',
                'customers_order_product_count' => 'required|numeric',
                'customers_order_product_price' => 'required|numeric',
                'customers_order_product_type' => 'required',
            );
            //validate input
            $validator = Validator::make(Input::all(), $rules);
            //check validate
            if ($validator->fails()) {
                // false Redirect
                $messages = $validator->messages();
                return Redirect::to('/factory/checkMaterialMarkOrder?orderId=' . $customers_order_id)
                    ->withErrors($validator)
                    ->withInput(Input::except('customers_order_product_name', 'customers_order_product_count','customers_order_product_type','customers_order_product_price'));
            } else {
                $user = Session::get('user');
                $material = explode(":",$customers_order_product_name);
                $newCustomersOrderProduct = new Models\CustomersOrderProduct();
                if (!empty($customers_order_product_id)) {
                    $newCustomersOrderProduct = Models\CustomersOrderProduct::find($customers_order_product_id);
                }
                $newCustomersOrderProduct->customers_order_product_type=$customers_order_product_type;
                $newCustomersOrderProduct->customers_order_id = $customers_order_id;
                $newCustomersOrderProduct->customers_order_product_name = $material[0];
                $newCustomersOrderProduct->customers_order_product_count = $customers_order_product_count;
                $newCustomersOrderProduct->customers_order_product_staff = $user->staff_username;
                $newCustomersOrderProduct->customers_order_product_price=$customers_order_product_price;
                $newCustomersOrderProduct->customers_order_product_total=($customers_order_product_price*$customers_order_product_count);
                $newCustomersOrderProduct->customers_order_product_status = 'New';
                if ($newCustomersOrderProduct->save()) {
                    return Redirect::to('/factory/checkMaterialMarkOrder?orderId=' . $customers_order_id);
                }
            }
        }
    }

//    public function planManufacture(){
//        $orderManufacture=new Models\OrderManufacture();
//        $user = Session::get('user');
//        $dataOrderManufacture=$orderManufacture::where('project_id', '=', 13)->orderBy('order_manufacture_type', 'ASC')->get();
//        return view('factory.planManufacture', array('user'=>$user,'dataOrderManufacture'=>$dataOrderManufacture));
//    }

    public function transfer()
    {
        $orderManufacture = new Models\OrderManufacture();
        $user = Session::get('user');
        $dataOrderManufacture = $orderManufacture::where('project_id', '=', 13)->orderBy('order_manufacture_type', 'ASC')->get();
        return view('factory.transfer', array('user' => $user, 'dataOrderManufacture' => $dataOrderManufacture));
    }

    public function checkCustomers()
    {
        $Customers = array();

        foreach (Models\Customers::all() as $key => $value) {
            array_push($Customers, $value->customers_id . '::' . $value->customers_name);
        }
        return json_encode($Customers);
    }

    public function checkProductManufacture()
    {
        $Product = array();

        foreach (Models\Product::where('product_type', '=', 'manufacture')->get() as $key => $value) {
            array_push($Product, $value->product_name.':'.Models\ProductUnit::getNameUnitByProduct($value->product_id));
        }
        return json_encode($Product);
    }

    public function checkProductMaterial()
    {
        $Product = array();
        foreach (Models\Product::where('product_type', '=', 'material')->get() as $key => $value) {
            array_push($Product, $value->product_name.':'.Models\ProductUnit::getNameUnitByProduct($value->product_id));
        }
        return json_encode($Product);
    }

    public function addOrder()
    {
        $search = Input::get('search', '');
        $page = Input::get('page', 1);
        $customersOrder = new Models\CustomersOrder();
        $countRow = 30;
        $dataCustomersOrder = $customersOrder->findAllCustomersAndPaginateCustomers($countRow, $page, $search);
        $customersOrder = new Models\CustomersOrder();
        return view('factory.addOrder', array('customersOrder' => $customersOrder, 'dataCustomersOrder' => $dataCustomersOrder));
    }

    public function markOrderCustomersOrder()
    {
        $id = Input::get('id', 0);
        if (!empty($id)) {
            $customersOrder = Models\CustomersOrder::find($id);
            if (!empty($customersOrder)) {
                $customersOrder->customers_order_status = 'markOrder';
                if ($customersOrder->save()) {
                    return Redirect::to('/factory/addOrder');
                }
            } else {
                return Redirect::to('/factory/addOrder');
            }
        } else {
            return Redirect::to('/factory/addOrder');
        }
    }

    public function deleteCustomersOrder()
    {
        $id = Input::get('id', 0);
        if (!empty($id)) {
            $customersOrder = Models\CustomersOrder::find($id);
            if (!empty($customersOrder)) {
                $customersOrder->delete();
                return Redirect::to('/factory/addOrder');
            } else {
                return Redirect::to('/factory/addOrder');
            }
        } else {
            return Redirect::to('/factory/addOrder');
        }
    }

    public function searchCustomersOrder()
    {
        $search = Input::get('search', '');
        $page = Input::get('page', 1);
        $customersOrder = new Models\CustomersOrder();
        $countRow = 30;
        $dataCustomersOrder = $customersOrder->findAllCustomersAndPaginateCustomers($countRow, $page, $search);
        $customersOrder = new Models\CustomersOrder();
        return view('factory.addOrder', array('customersOrder' => $customersOrder, 'dataCustomersOrder' => $dataCustomersOrder));
    }

    public function editOrder()
    {
        if (!empty($_POST)) {
            $customers_order_id = Input::get('customers_order_id', 0);
            $customers_order_customers = Input::get('customers_order_customers', '');
            $customers_order_tax_number = Input::get('customers_order_tax_number', '');
            $customers_order_number = Input::get('customers_order_number', '');
            $customers_order_date = Input::get('customers_order_date', '');
            $customers_order_tax = Input::get('customers_order_tax', 0);
            $customers_order_discount = Input::get('customers_order_discount', 0);
            $customers_order_deposit = Input::get('customers_order_deposit', 0);
            $customers_order_deposit_date_set = Input::get('customers_order_deposit_date_set', '');
            $customers_order_deposit_date_limit = Input::get('customers_order_deposit_date_limit', '');
            $customers_order_type_discount = Input::get('customers_order_type_discount', '');
            //set rules
            $rules = array(
                'customers_order_tax' => 'required|numeric',
                'customers_order_discount' => 'required|numeric'
            );
            //validate input
            $validator = Validator::make(Input::all(), $rules);
            //check validate
            if ($validator->fails()) {
                // false Redirect
                $messages = $validator->messages();
                return Redirect::to('/factory/checkMaterialMarkOrder?orderId='.$customers_order_id)
                    ->withErrors($validator)
                    ->withInput(Input::except('customers_order_tax','customers_order_discount'));
            } else {
                if (!empty($customers_order_id)) {
                    $customersOrder = Models\CustomersOrder::find($customers_order_id);
                    $customersOrder->customers_order_tax_number=$customers_order_tax_number;
                    $customersOrder->customers_order_number=$customers_order_number;
                    if(!empty($customers_order_date)) {
                        $customersOrder->customers_order_date = $customers_order_date;
                    }
                    if(!empty($customers_order_deposit_date_set)){
                        $customersOrder->customers_order_deposit_date_set=$customers_order_deposit_date_set;
                    }
                    if(!empty($customers_order_deposit_date_limit)){
                        $customersOrder->customers_order_deposit_date_limit=$customers_order_deposit_date_limit;
                    }
                    $customersOrder->customers_order_deposit=$customers_order_deposit;
                    $customersOrder->customers_order_tax=$customers_order_tax;
                    $customersOrder->customers_order_discount=$customers_order_discount;
                    $customersOrder->customers_order_type_discount=$customers_order_type_discount;
                    if ($customersOrder->save()) {
                        return Redirect::to('/factory/checkMaterialMarkOrder?orderId='.$customers_order_id);
                    }
                }
            }
        }
    }

    public function createOrderOffice()
    {
        if (!empty($_POST)) {
            $customers_order_id = Input::get('customers_order_id', 0);
            $customers_order_customers = Input::get('customers_order_customers', '');
            $customers_order_name = Input::get('customers_order_name', '');
            $customers_order_tax_number = Input::get('customers_order_tax_number', '');
            $customers_order_number = Input::get('customers_order_number', '');
            $customers_order_date = Input::get('customers_order_date', '');
            $customers_order_tax = Input::get('customers_order_tax', 0);
            $customers_order_discount = Input::get('customers_order_discount', 0);
            //set rules
            $rules = array(
                'customers_order_customers' => 'required',
                'customers_order_name' => 'required',
                'customers_order_tax' => 'required|numeric',
                'customers_order_discount' => 'required|numeric'
            );
            //validate input
            $validator = Validator::make(Input::all(), $rules);
            //check validate
            if ($validator->fails()) {
                // false Redirect
                $messages = $validator->messages();
                return Redirect::to('/factory/MarkOrder')
                    ->withErrors($validator)
                    ->withInput(Input::except('customers_order_customers', 'customers_order_name','customers_order_tax','customers_order_discount'));
            } else {
                $customers_id = explode("::", $customers_order_customers);
                $user = Session::get('user');
                $customersOrder = new Models\CustomersOrder();
                if (!empty($customers_order_id)) {
                    $customersOrder = Models\CustomersOrder::find($customers_order_id);
                }
                $customersOrder->customers_id = $customers_id[0];
                $customersOrder->customers_order_customers = $customers_id[1];
                $customersOrder->customers_order_name = $customers_order_name;
                $customersOrder->customers_order_tax_number=$customers_order_tax_number;
                $customersOrder->customers_order_number=$customers_order_number;
                if(!empty($customers_order_date)) {
                    $customersOrder->customers_order_date = $customers_order_date;
                }
                $customersOrder->customers_order_tax=$customers_order_tax;
                $customersOrder->customers_order_discount=$customers_order_discount;
                $customersOrder->customers_order_staff = $user->staff_username;
                $customersOrder->customers_order_status = 'create';
                if ($customersOrder->save()) {
                    return Redirect::to('/factory/MarkOrder');
                }
            }
        }
    }

    public function createOrder()
    {
        if (!empty($_POST)) {
            $customers_order_id = Input::get('customers_order_id', 0);
            $customers_order_customers = Input::get('customers_order_customers', '');
            $customers_order_name = Input::get('customers_order_name', '');
            $customers_order_tax_number = Input::get('customers_order_tax_number', '');
            $customers_order_number = Input::get('customers_order_number', '');
            $customers_order_date = Input::get('customers_order_date', '');
            $customers_order_tax = Input::get('customers_order_tax', 0);
            $customers_order_discount = Input::get('customers_order_discount', 0);
            //set rules
            $rules = array(
                'customers_order_customers' => 'required',
                'customers_order_name' => 'required',
                'customers_order_tax' => 'required|numeric',
                'customers_order_discount' => 'required|numeric'
            );
            //validate input
            $validator = Validator::make(Input::all(), $rules);
            //check validate
            if ($validator->fails()) {
                // false Redirect
                $messages = $validator->messages();
                return Redirect::to('/factory/addOrder')
                    ->withErrors($validator)
                    ->withInput(Input::except('customers_order_customers', 'customers_order_name','customers_order_tax','customers_order_discount'));
            } else {
                $customers_id = explode("::", $customers_order_customers);
                $user = Session::get('user');
                $customersOrder = new Models\CustomersOrder();
                if (!empty($customers_order_id)) {
                    $customersOrder = Models\CustomersOrder::find($customers_order_id);
                }
                $customersOrder->customers_id = $customers_id[0];
                $customersOrder->customers_order_customers = $customers_id[1];
                $customersOrder->customers_order_name = $customers_order_name;
                $customersOrder->customers_order_tax_number=$customers_order_tax_number;
                $customersOrder->customers_order_number=$customers_order_number;
                if(!empty($customers_order_date)) {
                    $customersOrder->customers_order_date = $customers_order_date;
                }
                $customersOrder->customers_order_tax=$customers_order_tax;
                $customersOrder->customers_order_discount=$customers_order_discount;
                $customersOrder->customers_order_staff = $user->staff_username;
                $customersOrder->customers_order_status = 'create';
                if ($customersOrder->save()) {
                    return Redirect::to('/factory/addOrder');
                }
            }
        }
    }

    public function PR()
    {
        return view('factory.PR', array());
    }

    public function MarkOrder()
    {
        $search = Input::get('search', '');
        $page = Input::get('page', 1);
        $customersOrder = new Models\CustomersOrder();
        $countRow = 30;
        $dataCustomersOrder = $customersOrder->findAllCustomersAndPaginateOffice($countRow, $page, $search);
        $customersOrder = new Models\CustomersOrder();
        return view('factory.MarkOrder', array('customersOrder' => $customersOrder, 'dataCustomersOrder' => $dataCustomersOrder));
    }

    public function searchCustomersOrderMarkOrder()
    {
        $search = Input::get('search', '');
        $page = Input::get('page', 1);
        $customersOrder = new Models\CustomersOrder();
        $countRow = 30;
        $dataCustomersOrder = $customersOrder->findAllCustomersAndPaginateOffice($countRow, $page, $search);
        $customersOrder = new Models\CustomersOrder();
        return view('factory.MarkOrder', array('customersOrder' => $customersOrder, 'dataCustomersOrder' => $dataCustomersOrder));
    }

    public function checkMaterialMarkOrder()
    {
        $orderId = Input::get('orderId', 0);
        $productId = Input::get('productId', 0);
        $search = Input::get('search', '');
        $page = Input::get('page', 1);
        $countRow = 30;
        $sum=0;
        $CustomersOrderProduct = new Models\CustomersOrderProduct();
        $dataCustomersOrderProduct = Models\CustomersOrderProduct::where('customers_order_id','=',$orderId)->where('customers_order_product_type','=','Normal')->get();
        $dataCustomersOrder=Models\CustomersOrder::find($orderId);
        if(!empty($productId)){
            $CustomersOrderProduct=$CustomersOrderProduct::find($productId);
        }
        return view('factory.checkMaterialMarkOrder', array('sum'=>$sum,'dataCustomersOrder'=>$dataCustomersOrder,'dataCustomersOrderProduct' => $dataCustomersOrderProduct, 'orderId' => $orderId, 'CustomersOrderProduct' => $CustomersOrderProduct));
    }

    public function deleteCustomersOrderMarkOrder()
    {
        $id = Input::get('id', 0);
        if (!empty($id)) {
            $customersOrder = Models\CustomersOrder::find($id);
            if (!empty($customersOrder)) {
                $customersOrder->delete();
                return Redirect::to('/factory/MarkOrder');
            } else {
                return Redirect::to('/factory/MarkOrder');
            }
        } else {
            return Redirect::to('/factory/MarkOrder');
        }
    }

    public function confirmDealCustomersOrder()
    {
        $id = Input::get('id', 0);
        if (!empty($id)) {
            $customersOrder = Models\CustomersOrder::find($id);
            if (!empty($customersOrder)) {
                $customersOrder->customers_order_status = 'deal';
                if ($customersOrder->save()) {
                    return Redirect::to('/factory/addOrder');
                }
            } else {
                return Redirect::to('/factory/addOrder');
            }
        } else {
            return Redirect::to('/factory/addOrder');
        }
    }

    public function MarkOrderEngineer()
    {
        $search = Input::get('search', '');
        $page = Input::get('page', 1);
        $customersOrder = new Models\CustomersOrder();
        $countRow = 30;
        $dataCustomersOrder = $customersOrder->findAllCustomersAndPaginateEngineer($countRow, $page, $search);
        return view('factory.MarkOrderEngineer', array('customersOrder' => $customersOrder, 'dataCustomersOrder' => $dataCustomersOrder));
    }

    public function searchMarkOrderEngineer()
    {
        $search = Input::get('search', '');
        $page = Input::get('page', 1);
        $customersOrder = new Models\CustomersOrder();
        $countRow = 30;
        $dataCustomersOrder = $customersOrder->findAllCustomersAndPaginateEngineer($countRow, $page, $search);
        $customersOrder = new Models\CustomersOrder();
        return view('factory.MarkOrderEngineer', array('customersOrder' => $customersOrder, 'dataCustomersOrder' => $dataCustomersOrder));
    }

    public function MarkOrderFactory()
    {
        $search = Input::get('search', '');
        $page = Input::get('page', 1);
        $customersOrder = new Models\CustomersOrder();
        $countRow = 30;
        $dataCustomersOrder = $customersOrder->findAllCustomersAndPaginateFactory($countRow, $page, $search);
        $customersOrder = new Models\CustomersOrder();
        return view('factory.MarkOrderFactory', array('customersOrder' => $customersOrder, 'dataCustomersOrder' => $dataCustomersOrder));
    }

    public function MarkOrderCheckQuality()
    {
        $search = Input::get('search', '');
        $page = Input::get('page', 1);
        $customersOrder = new Models\CustomersOrder();
        $countRow = 30;
        $dataCustomersOrder = $customersOrder->findAllCustomersAndPaginateCheckQuality($countRow, $page, $search);
        return view('factory.MarkOrderCheckQuality', array('dataCustomersOrder'=>$dataCustomersOrder));
    }

    public function MarkOrderManager()
    {
        $search = Input::get('search', '');
        $page = Input::get('page', 1);
        $customersOrder = new Models\CustomersOrder();
        $countRow = 30;
        $dataCustomersOrder = $customersOrder->findAllCustomersAndPaginateManager($countRow, $page, $search);
        $customersOrder = new Models\CustomersOrder();
        return view('factory.MarkOrderManager', array('customersOrder' => $customersOrder, 'dataCustomersOrder' => $dataCustomersOrder));
    }

    public function MarkOrderCompensate()
    {
        $search = Input::get('search', '');
        $page = Input::get('page', 1);
        $customersOrder = new Models\CustomersOrder();
        $countRow = 30;
        $dataCustomersOrder = $customersOrder->findAllCustomersAndPaginateCompensate($countRow, $page, $search);
        $customersOrder = new Models\CustomersOrder();
        return view('factory.MarkOrderCompensate', array('customersOrder' => $customersOrder, 'dataCustomersOrder' => $dataCustomersOrder));
    }

    public function getOrderMaterial(){
        $orderId = Input::get('orderId', 0);
        $material = Input::get('material', "");
        $dataOrderMaterial=Models\CustomersOrderMaterial::getOrderMaterialByOrderId($orderId);
        if(!empty($dataOrderMaterial)){
            $string="";
            foreach($dataOrderMaterial as $key=>$value){
                if(empty($material)) {
                    $string .= "<option value='" . $value->customers_order_material_id . "'>" . $value->customers_order_material_name . ":" . $value->storename_name . "::" . Models\ProductUnit::getNameUnitByProductName($value->customers_order_material_name) . "</option>";
                }else{
                    if($value->customers_order_material_name==$material){
                        $string .= "<option selected value='" . $value->customers_order_material_id . "'>" . $value->customers_order_material_name . ":" . $value->storename_name . "::" . Models\ProductUnit::getNameUnitByProductName($value->customers_order_material_name) . "</option>";
                    }else{
                        $string .= "<option value='" . $value->customers_order_material_id . "'>" . $value->customers_order_material_name . ":" . $value->storename_name . "::" . Models\ProductUnit::getNameUnitByProductName($value->customers_order_material_name) . "</option>";
                    }
                }
            }
            echo $string;
        }else{
            echo "<option>เลือก</option>";
        }
    }

    public function PlanManufacture(){
        $orderId = Input::get('orderId', 0);
        $order_product_id= Input::get('order_product_id', 0);
        $order_plan_id= Input::get('order_plan_id', 0);
        $dateSelect= Input::get('dateSelect', '');
        $dataCustomersOrderMaterial=array();
        $newCustomersOrderPlanMaterial=new Models\CustomersOrderPlanMaterial();
        $CustomersOrderProduct = new Models\CustomersOrderProduct();
        if(!empty($order_product_id)){
            $dataCustomersOrderMaterial=Models\CustomersOrderMaterial::getOrderMaterialByOrderId($order_product_id);
            $newCustomersOrderPlanMaterial->order_product_id=$order_product_id;
            $newCustomersOrderPlanMaterial->customers_order_id=$orderId;
            $newCustomersOrderPlanMaterial->customers_order_plan_id=$order_plan_id;
        }
        if(!empty($orderId)){
            $dataOrderProduct=Models\CustomersOrderProduct::where('customers_order_id','=',$orderId)->get();
            $dataCustomersOrder=Models\CustomersOrder::find($orderId);
            $dataDateCustomersOrderPlan=Models\CustomersOrderPlan::getDatePlan($orderId);
            if(!empty($dateSelect)) {
                $dataCustomersOrderPlan = Models\CustomersOrderPlan::where('customers_order_id', '=', $orderId)->where('customers_order_plan_date', '=', $dateSelect)->get();
            }else{
                $dataCustomersOrderPlan = Models\CustomersOrderPlan::where('customers_order_id', '=', $orderId)->get();
            }
        }
        return view('factory.PlanManufacture', array('CustomersOrderProduct'=>$CustomersOrderProduct,'newCustomersOrderPlanMaterial'=>$newCustomersOrderPlanMaterial,'dataCustomersOrderMaterial'=>$dataCustomersOrderMaterial,'orderId'=>$orderId,'dataCustomersOrderPlan'=>$dataCustomersOrderPlan,'dataDateCustomersOrderPlan'=>$dataDateCustomersOrderPlan,'dataCustomersOrder'=>$dataCustomersOrder,'dataOrderProduct'=>$dataOrderProduct,'orderId'=>$orderId));
    }

    public function sendCheckPlan(){
        $orderId = Input::get('orderId', 0);
        $planId = Input::get('planId', 0);
        $dataPlan=Models\CustomersOrderPlan::find($planId);
        $customersOrder=Models\CustomersOrder::find($orderId);
        if(!empty($dataPlan)){
            $dataPlan->customers_order_plan_status='confirmation';
            if($dataPlan->save()){
                if($customersOrder->customers_order_status!='confirmation'){
                    $customersOrder->customers_order_status='confirmation';
                    $customersOrder->save();
                }
                return Redirect::to('/factory/PlanManufacture?orderId='.$orderId);
            }
        }
    }

    public function sendCheckApprove(){
        $orderId = Input::get('orderId', 0);
        $planId = Input::get('planId', 0);
        $dataPlan=Models\CustomersOrderPlan::find($planId);
        if(!empty($dataPlan)){
            $dataPlan->customers_order_plan_status='approve';
            if($dataPlan->save()){
                $customersOrderPlanAll=Models\CustomersOrderPlan::where('customers_order_plan_status','=','confirmation')->where('customers_order_id','=',$orderId)->get();
                if(count($customersOrderPlanAll)==0){
                    $customersOrder=Models\CustomersOrder::find($orderId);
                    $customersOrder->customers_order_status='approve';
                    $customersOrder->save();
                }

                return Redirect::to('/factory/ApproveManufacture?orderId='.$orderId);
            }
        }
    }

    public function addCustomersOrderPlan()
    {
        if (!empty($_POST)) {
            $customers_order_id = Input::get('customers_order_id', 0);
            $customers_order_plan_id = Input::get('customers_order_plan_id', 0);
            $customers_order_plan_date = Input::get('customers_order_plan_date', '');
            $customers_order_product_id = Input::get('customers_order_product_id', '');
            $customers_order_plan_count = Input::get('customers_order_plan_count', 0);
            $customers_order_plan_type=Input::get('customers_order_plan_type', '');
            //set rules
            $rules = array(
                'customers_order_plan_date' => 'required',
                'customers_order_product_id' => 'required',
                'customers_order_plan_count' => 'required|numeric'
            );
            //validate input
            $validator = Validator::make(Input::all(), $rules);
            //check validate
            if ($validator->fails()) {
                // false Redirect
                $messages = $validator->messages();
                return Redirect::to('/factory/PlanManufacture?orderId=' . $customers_order_id)
                    ->withErrors($validator)
                    ->withInput(Input::except('customers_order_plan_date', 'customers_order_product_id','customers_order_plan_count'));
            } else {
                $user = Session::get('user');
                $customersOrderPlan=new Models\CustomersOrderPlan();
                if(!empty($customers_order_plan_id)) {
                    $customersOrderPlan = Models\CustomersOrderPlan::find($customers_order_plan_id);
                }
                $customersOrderPlan->customers_order_plan_type=$customers_order_plan_type;
                $customersOrderPlan->customers_order_id=$customers_order_id;
                $customersOrderPlan->customers_order_product_id=$customers_order_product_id;
                $customersOrderPlan->customers_order_plan_date=$customers_order_plan_date;
                $customersOrderPlan->customers_order_plan_count=$customers_order_plan_count;
                $customersOrderPlan->customers_order_plan_staff=$user->staff_username;
                $customersOrderPlan->customers_order_plan_status='create';
                if($customersOrderPlan->save()){
                    $dataCustomersOrder=Models\CustomersOrder::find($customers_order_id);
                    if(!empty($dataCustomersOrder)){
                        if($dataCustomersOrder->customers_order_status!=='plan'){
                            $dataCustomersOrder->customers_order_status='plan';
                            $dataCustomersOrder->save();
                        }
                    }
                    return Redirect::to('/factory/PlanManufacture?orderId='.$customers_order_id);
                }
            }
        }
    }

    public function addCustomersOrderPlanMaterial(){
        if(!empty($_POST)) {
            $customers_order_plan_material_id = Input::get('customers_order_plan_material_id', 0);
            $customers_order_product_id = Input::get('customers_order_product_id', 0);
            $customers_order_id = Input::get('customers_order_id', 0);
            $customers_order_plan_id = Input::get('customers_order_plan_id', 0);
            $customers_order_material_id = Input::get('customers_order_material_id', 0);
            $customers_order_material_count = Input::get('customers_order_material_count', 0);
            //set rules
            $rules = array(
                'customers_order_material_id' => 'required',
                'customers_order_material_count' => 'required|numeric'
            );
            //validate input
            $validator = Validator::make(Input::all(), $rules);
            //check validate
            if ($validator->fails()) {
                // false Redirect
                $messages = $validator->messages();
                return Redirect::to('/factory/PlanManufacture?orderId='.$customers_order_id.'&order_product_id='.$customers_order_product_id.'&order_plan_id='.$customers_order_plan_id)
                    ->withErrors($validator)
                    ->withInput(Input::except('customers_order_material_id', 'customers_order_material_count'));
            } else {
                $user = Session::get('user');
                $customersOrderPlanMaterial=new Models\CustomersOrderPlanMaterial();
                if(!empty($customers_order_plan_material_id)) {
                    $customersOrderPlanMaterial = Models\CustomersOrderPlanMaterial::find($customers_order_plan_material_id);
                }
                $customersOrderPlanMaterial->customers_order_id=$customers_order_id;
                $customersOrderPlanMaterial->customers_order_plan_id=$customers_order_plan_id;
                $customersOrderPlanMaterial->customers_order_material_id=$customers_order_material_id;
                $customersOrderPlanMaterial->customers_order_material_count=$customers_order_material_count;
                $customersOrderPlanMaterial->customers_order_plan_staff=$user->staff_username;
                $customersOrderPlanMaterial->customers_order_plan_status='create';
                if($customersOrderPlanMaterial->save()){
                    return Redirect::to('/factory/PlanManufacture?orderId='.$customers_order_id);
                }
            }
        }
    }

    public function BringManufacture()
    {
        $orderId = Input::get('orderId', 0);
        $order_product_id= Input::get('order_product_id', 0);
        $order_plan_id= Input::get('order_plan_id', 0);
        $dateSelect= Input::get('dateSelect', '');
        if(!empty($orderId)){
            $dataOrderProduct=Models\CustomersOrderProduct::where('customers_order_id','=',$orderId)->get();
            $dataCustomersOrder=Models\CustomersOrder::find($orderId);
            $dataDateCustomersOrderPlan=Models\CustomersOrderPlan::getDatePlan($orderId,'approve');
            if(!empty($dateSelect)) {
                $dataCustomersOrderPlan = Models\CustomersOrderPlan::where('customers_order_id', '=', $orderId)->where('customers_order_plan_status', '=', 'approve')->where('customers_order_plan_date', '=', $dateSelect)->get();
            }else{
                $dataCustomersOrderPlan = Models\CustomersOrderPlan::where('customers_order_id', '=', $orderId)->where('customers_order_plan_status', '=', 'approve')->get();
            }
        }
        return view('factory.BringManufacture', array('orderId'=>$orderId,'dataCustomersOrderPlan'=>$dataCustomersOrderPlan,'dataDateCustomersOrderPlan'=>$dataDateCustomersOrderPlan,'dataCustomersOrder'=>$dataCustomersOrder,'dataOrderProduct'=>$dataOrderProduct,'orderId'=>$orderId));
    }

    public function PrintBringManufacture(){
        $orderId = Input::get('orderId', 0);
        $dataCustomersOrderPlan=Models\CustomersOrderPlan::find($orderId);
        $dataCustomersOrderProduct=Models\CustomersOrderProduct::find($dataCustomersOrderPlan->customers_order_product_id);
        $dataCustomersOrderPlanMaterial=Models\CustomersOrderPlanMaterial::where('customers_order_plan_id','=',$dataCustomersOrderPlan->customers_order_plan_id)->get();
        $thai_month_arr=array(
            "0"=>"",
            "1"=>"มกราคม",
            "2"=>"กุมภาพันธ์",
            "3"=>"มีนาคม",
            "4"=>"เมษายน",
            "5"=>"พฤษภาคม",
            "6"=>"มิถุนายน",
            "7"=>"กรกฎาคม",
            "8"=>"สิงหาคม",
            "9"=>"กันยายน",
            "10"=>"ตุลาคม",
            "11"=>"พฤศจิกายน",
            "12"=>"ธันวาคม"
        );
        try {
            $pdf = \PDF::loadView('factory.PaperBringManufacture',array('thai_month_arr'=>$thai_month_arr,'dataCustomersOrderPlanMaterial'=>$dataCustomersOrderPlanMaterial,'dataCustomersOrderProduct'=>$dataCustomersOrderProduct,'dataCustomersOrderPlan'=>$dataCustomersOrderPlan));
            return $pdf->stream();
        } catch (Exception $e) {
            echo $e->getLine() . ':' . $e->getMessage();
        } finally {

        }
    }

    public function PrintBringManufactureStore(){
        $orderId = Input::get('orderId', 0);
        $dataCustomersOrderPlan=Models\CustomersOrderPlan::find($orderId);
        $dataCustomersOrderProduct=Models\CustomersOrderProduct::find($dataCustomersOrderPlan->customers_order_product_id);
        $dataCustomersOrderPlanMaterial=Models\CustomersOrderPlanMaterial::where('customers_order_plan_id','=',$dataCustomersOrderPlan->customers_order_plan_id)->get();
        try {
            $pdf = \PDF::loadView('factory.PaperBringManufactureStore',array('dataCustomersOrderPlanMaterial'=>$dataCustomersOrderPlanMaterial,'dataCustomersOrderProduct'=>$dataCustomersOrderProduct,'dataCustomersOrderPlan'=>$dataCustomersOrderPlan));
            return $pdf->stream();
        } catch (Exception $e) {
            echo $e->getLine() . ':' . $e->getMessage();
        } finally {

        }
    }

    public function checkQualityManufactureAll(){
        $orderId = Input::get('orderId', 0);
        $dataCustomersOrder=Models\CustomersOrder::find($orderId);
        $dataCustomersOrderPlan=Models\CustomersOrderPlan::where('customers_order_id','=',$orderId)->where('customers_order_plan_status','=','approve')->get();
        if(!empty($dataCustomersOrderPlan)){
            foreach($dataCustomersOrderPlan as $key=>$value){
                $dataPlan=Models\CustomersOrderPlan::find($value->customers_order_plan_id);
                if(!empty($dataPlan)){
                    $dataPlan->customers_order_plan_status='checkQuality';
                    $dataPlan->save();
                }
            }
        }
        if(!empty($dataCustomersOrder)){
            if($dataCustomersOrder->customers_order_status!='checkQuality'){
                $dataCustomersOrder->customers_order_status='checkQuality';
            }
            if($dataCustomersOrder->save()){
                return Redirect::to('/factory/MarkOrderFactory');
            }
        }else{
            return Redirect::to('/factory/MarkOrderFactory');
        }
    }

    public function checkQualityManufacture(){
        $orderId = Input::get('orderId', 0);
        $dataCustomersOrderPlan=Models\CustomersOrderPlan::find($orderId);
        if(!empty($dataCustomersOrderPlan)){
            $dataCustomersOrder=Models\CustomersOrder::find($dataCustomersOrderPlan->customers_order_id);
            if($dataCustomersOrder->customers_order_status!='checkQuality'){
                $dataCustomersOrder->customers_order_status='checkQuality';
                $dataCustomersOrder->save();
            }
            $dataCustomersOrderPlan->customers_order_plan_status='checkQuality';
            if($dataCustomersOrderPlan->save()){
                return Redirect::to('/factory/MarkOrderFactory');
            }
        }else{
            return Redirect::to('/factory/MarkOrderFactory');
        }
    }

    public function ApproveManufacture()
    {
        $orderId = Input::get('orderId', 0);
        $order_product_id= Input::get('order_product_id', 0);
        $order_plan_id= Input::get('order_plan_id', 0);
        $dateSelect= Input::get('dateSelect', '');
        $dataCustomersOrderMaterial=array();
        $newCustomersOrderPlanMaterial=new Models\CustomersOrderPlanMaterial();
        if(!empty($order_product_id)){
            $dataCustomersOrderMaterial=Models\CustomersOrderMaterial::getOrderMaterialByOrderId($order_product_id);
            $newCustomersOrderPlanMaterial->order_product_id=$order_product_id;
            $newCustomersOrderPlanMaterial->customers_order_id=$orderId;
            $newCustomersOrderPlanMaterial->customers_order_plan_id=$order_plan_id;
        }
        if(!empty($orderId)){
            $dataOrderProduct=Models\CustomersOrderProduct::where('customers_order_id','=',$orderId)->get();
            $dataCustomersOrder=Models\CustomersOrder::find($orderId);
            $dataDateCustomersOrderPlan=Models\CustomersOrderPlan::getDatePlan($orderId,'confirmation');
            if(!empty($dateSelect)) {
                $dataCustomersOrderPlan = Models\CustomersOrderPlan::where('customers_order_id', '=', $orderId)->where('customers_order_plan_status', '=', 'confirmation')->where('customers_order_plan_date', '=', $dateSelect)->get();
            }else{
                $dataCustomersOrderPlan = Models\CustomersOrderPlan::where('customers_order_id', '=', $orderId)->where('customers_order_plan_status', '=', 'confirmation')->get();
            }
        }
        return view('factory.ApproveManufacture', array('newCustomersOrderPlanMaterial'=>$newCustomersOrderPlanMaterial,'dataCustomersOrderMaterial'=>$dataCustomersOrderMaterial,'orderId'=>$orderId,'dataCustomersOrderPlan'=>$dataCustomersOrderPlan,'dataDateCustomersOrderPlan'=>$dataDateCustomersOrderPlan,'dataCustomersOrder'=>$dataCustomersOrder,'dataOrderProduct'=>$dataOrderProduct,'orderId'=>$orderId));
    }

    public function deletePlanMaterial(){
        $orderId = Input::get('orderId', 0);
        $materialId = Input::get('materialId', 0);
        $dataPlanMaterial=Models\CustomersOrderPlanMaterial::find($materialId);
        if(!empty($dataPlanMaterial)){
            $dataPlanMaterial->delete();
            return Redirect::to('/factory/ApproveManufacture?orderId='.$orderId);
        }else{
            return Redirect::to('/factory/ApproveManufacture?orderId='.$orderId);
        }
    }

    public function deletePlanEngineer(){
        $orderId = Input::get('orderId', 0);
        $planId = Input::get('planId', 0);
        $dataPlan=Models\CustomersOrderPlan::find($planId);
        if(!empty($dataPlan)){
            $dataPlan->delete();
            return Redirect::to('/factory/PlanManufacture?orderId='.$orderId);
        }else{
            return Redirect::to('/factory/PlanManufacture?orderId='.$orderId);
        }
    }

    public function deletePlanManage(){
        $orderId = Input::get('orderId', 0);
        $planId = Input::get('planId', 0);
        $dataPlan=Models\CustomersOrderPlan::find($planId);
        if(!empty($dataPlan)){
            $dataPlan->delete();
            return Redirect::to('/factory/ApproveManufacture?orderId='.$orderId);
        }else{
            return Redirect::to('/factory/ApproveManufacture?orderId='.$orderId);
        }
    }

    public function addCustomersOrderPlanMaterialManage(){
        if(!empty($_POST)) {
            $customers_order_plan_material_id = Input::get('customers_order_plan_material_id', 0);
            $customers_order_product_id = Input::get('customers_order_product_id', 0);
            $customers_order_id = Input::get('customers_order_id', 0);
            $customers_order_plan_id = Input::get('customers_order_plan_id', 0);
            $customers_order_material_id = Input::get('customers_order_material_id', 0);
            $customers_order_material_count = Input::get('customers_order_material_count', 0);
            //set rules
            $rules = array(
                'customers_order_material_id' => 'required',
                'customers_order_material_count' => 'required|numeric'
            );
            //validate input
            $validator = Validator::make(Input::all(), $rules);
            //check validate
            if ($validator->fails()) {
                // false Redirect
                $messages = $validator->messages();
                return Redirect::to('/factory/ApproveManufacture?orderId='.$customers_order_id.'&order_product_id='.$customers_order_product_id.'&order_plan_id='.$customers_order_plan_id)
                    ->withErrors($validator)
                    ->withInput(Input::except('customers_order_material_id', 'customers_order_material_count'));
            } else {
                $user = Session::get('user');
                $customersOrderPlanMaterial = Models\CustomersOrderPlanMaterial::where('customers_order_id','=',$customers_order_id)->where('customers_order_plan_id','=',$customers_order_plan_id)->where('customers_order_material_id','=',$customers_order_material_id)->firstOrFail();

                //print_r($customersOrderPlanMaterial);exit;
                //$customersOrderPlanMaterial=new Models\CustomersOrderPlanMaterial();
                if(!empty($customers_order_plan_material_id)) {
                    //$customersOrderPlanMaterial = Models\CustomersOrderPlanMaterial::find($customers_order_plan_material_id);
                    $customersOrderPlanMaterial=new Models\CustomersOrderPlanMaterial();
                }
                $customersOrderPlanMaterial->customers_order_id=$customers_order_id;
                $customersOrderPlanMaterial->customers_order_plan_id=$customers_order_plan_id;
                $customersOrderPlanMaterial->customers_order_material_id=$customers_order_material_id;
                $customersOrderPlanMaterial->customers_order_material_count=$customers_order_material_count;
                $customersOrderPlanMaterial->customers_order_plan_staff=$user->staff_username;
                $customersOrderPlanMaterial->customers_order_plan_status='create';
                if($customersOrderPlanMaterial->save()){
                    return Redirect::to('/factory/ApproveManufacture?orderId='.$customers_order_id);
                }
            }
        }
    }

    public function CheckQuality()
    {
        $orderId = Input::get('orderId', 0);
        $dataCustomersOrderPlan=Models\CustomersOrderPlan::where('customers_order_id','=',$orderId)->where('customers_order_plan_status','=','checkQuality')->get();
        return view('factory.CheckQuality', array('orderId'=>$orderId,'dataCustomersOrderPlan'=>$dataCustomersOrderPlan));
    }

    public function confirmCheckQuality(){
        if(!empty($_POST)) {
            $customers_order_id = Input::get('customers_order_id', 0);
            $customers_order_plan_id = Input::get('customers_order_plan_id', 0);
            $customers_order_plan_complete = Input::get('customers_order_plan_complete', 0);
            $customers_order_plan_waste = Input::get('customers_order_plan_waste', 0);
            //set rules
            $rules = array(
                'customers_order_plan_complete' => 'required|numeric',
                'customers_order_plan_waste' => 'required|numeric'
            );
            //validate input
            $validator = Validator::make(Input::all(), $rules);
            //check validate
            if ($validator->fails()) {
                // false Redirect
                $messages = $validator->messages();
                return Redirect::to('/factory/CheckQuality?orderId='.$customers_order_id)
                    ->withErrors($validator)
                    ->withInput(Input::except('customers_order_plan_complete', 'customers_order_plan_waste'));
            } else {
                $user = Session::get('user');
                $store=new Models\Stores();
                $customersOrder=Models\CustomersOrder::find($customers_order_id);
                $customersOrderPlan=Models\CustomersOrderPlan::find($customers_order_plan_id);
                $customersOrderPlan->customers_order_plan_status='complete';
                $customersOrderPlan->customers_order_plan_complete=$customers_order_plan_complete;
                $customersOrderPlan->customers_order_plan_waste=$customers_order_plan_waste;
                $customersOrderProduct=Models\CustomersOrderProduct::find($customersOrderPlan->customers_order_product_id);
                if(!empty($customersOrderProduct)) {
                    $Product = Models\Product::where('product_name', '=', $customersOrderProduct->customers_order_product_name)->first();
                    if (!empty($Product)) {
                        //                อัพเดท สิ้นค้าลงครัง oneM
                        if (!empty($customers_order_plan_complete)) {
                            $store->updateStore($Product->product_id, 2, $customers_order_plan_complete, 'plus');
                        }
                        //                อัพเดท สิ้นค้าลงครัง ชำรุด
                        if (!empty($customers_order_plan_waste)) {
                            $store->updateStore($Product->product_id, 1, $customers_order_plan_complete, 'plus');
                        }
                    }
                }
                if($customersOrderPlan->save()){
                    $customersOrderPlanAllQuality=Models\CustomersOrderPlan::where('customers_order_plan_status','=','checkQuality')->where('customers_order_id','=',$customers_order_id)->get();
                    $customersOrderPlanAll=Models\CustomersOrderPlan::where('customers_order_plan_status','!=','complete')->where('customers_order_id','=',$customers_order_id)->get();
                    if(count($customersOrderPlanAllQuality)>0){
                        if($customersOrder->customers_order_status!='checkQuality'){
                            $customersOrder->customers_order_status = 'checkQuality';
                            $customersOrder->save();
                        }
                    }else if(count($customersOrderPlanAll)==0){
                        $customersOrder->customers_order_status='plan';
                        $customersOrder->save();
                    }
                    return Redirect::to('/factory/MarkOrderCheckQuality');
                }
            }
        }
    }

    public  function  addPRImmediately(){
        $pname = Input::get('pname', 0);
        $jname = Input::get('jname', 0);
        $canme = Input::get('cname', 0);
        $quantity = Input::get('quantity', 0);
        $user = Session::get('user');
        $results =array('success'=>false);

        $pr = new Models\PrOrder();
        $pr->pr_order_name = $pname." ของโครงการ ".$jname." ".$canme;
        $pr->pr_order_staff=$user->staff_username;
        $pr->pr_order_status="wait";
        if($pr->save()){
            $prdetail = new Models\PrOrderDetail();
            $prdetail->PR_order_id=$pr->PR_order_id;
            $prdetail->PR_order_detail_name=$pname;
            $prdetail->PR_order_detail_count=$quantity;
            $prdetail->PR_order_detail_staff=$user->staff_username;
            $prdetail->PR_order_detail_status='new';
            if($prdetail->save()){
                $results['success']=true;
            }
        }
        return json_encode($results);
    }

    public function Compensate()
    {
        $orderId = Input::get('orderId', 0);
        $search = Input::get('search', '');
        $page = Input::get('page', 1);
        $countRow = 30;
        $CustomersOrderProduct = new Models\CustomersOrderProduct();
        $dataCustomersOrderProduct = $CustomersOrderProduct->findAllCustomersAndPaginate($countRow, $page, $orderId, $search);
        $order = Models\CustomersOrder::find($orderId);
        return view('factory.Compensate', array('dataCustomersOrderProduct' => $dataCustomersOrderProduct, 'orderId' => $orderId, 'CustomersOrderProduct' => $CustomersOrderProduct, 'order' => $order));
    }

    public function printTransport(){
        $orderId = Input::get('orderId', 0);
        $customersOrderTransport=Models\CustomersOrderTransport::find($orderId);
        $customersOrderTransportProduct=Models\CustomersOrderTransportProduct::where('customers_order_transport_id','=',$customersOrderTransport->customers_order_transport_id)->get();
        $thai_month_arr=array(
            "0"=>"",
            "1"=>"มกราคม",
            "2"=>"กุมภาพันธ์",
            "3"=>"มีนาคม",
            "4"=>"เมษายน",
            "5"=>"พฤษภาคม",
            "6"=>"มิถุนายน",
            "7"=>"กรกฎาคม",
            "8"=>"สิงหาคม",
            "9"=>"กันยายน",
            "10"=>"ตุลาคม",
            "11"=>"พฤศจิกายน",
            "12"=>"ธันวาคม"
        );
        try {
            $pdf = \PDF::loadView('factory.PaperTransport',array('thai_month_arr'=>$thai_month_arr,'customersOrderTransport'=>$customersOrderTransport,'customersOrderTransportProduct'=>$customersOrderTransportProduct));
            return $pdf->stream();
        } catch (Exception $e) {
            echo $e->getLine() . ':' . $e->getMessage();
        } finally {

        }
    }

    public function addReturnTransport(){
        if(!empty($_POST)) {
            $customers_order_id = Input::get('customers_order_id', 0);
            $complete = Input::get('complete', '');
            if(!empty($complete)){
                foreach ($complete as $key => $value) {
                    if (!empty($value['status']) and $value['status'] == "on" and $value['customers_order_transport_product_id']!="") {
                        $customersOrderTransportProduct=Models\CustomersOrderTransportProduct::find($value['customers_order_transport_product_id']);
                        $customersOrderTransportProduct->customers_order_transport_product_return+=$value['count'];
                        if($customersOrderTransportProduct->save()){
                            $store=new Models\Stores();
                            $customersOrderProduct=Models\CustomersOrderProduct::find($customersOrderTransportProduct->customers_order_product_id);
                            $Product = Models\Product::where('product_name', '=', $customersOrderProduct->customers_order_product_name)->first();
                            //                อัพเดท สิ้นค้าลงครัง oneM
                            if (!empty($value['count'])) {
                                $store->updateStore($Product->product_id, 2, $value['count'], 'plus');
                            }
                        }
                    }
                }
            }
//            $customersOrderTransport=Models\CustomersOrderTransport::where('customers_order_id','=',$customers_order_id);
//            foreach($customersOrderTransport as $key=>$value){
//                $customersOrderTransportProduct=Models\CustomersOrderTransportProduct::where('customers_order_transport_id','=',$value->customers_order_transport_id)->get();
//                $status=true;
//                foreach($customersOrderTransportProduct as $key=>$value){
//                    if($value->customers_order_transport_product_status!='complete'){
//                        $status=false;
//                    }
//                }
//                if($status){
//                    $customersOrderTransport=Models\CustomersOrderTransport::find($value->customers_order_transport_id);
//                    $customersOrderTransport->customers_order_transport_status='complete';
//                    $customersOrderTransport->save();
//                }
//            }
            return Redirect::to('/factory/ReportTransport?orderId='.$customers_order_id);
        }
    }

    public function addWasteTransport(){
        if(!empty($_POST)) {
            $customers_order_id = Input::get('customers_order_id', 0);
            $complete = Input::get('complete', '');
            if(!empty($complete)){
                foreach ($complete as $key => $value) {
                    if (!empty($value['status']) and $value['status'] == "on" and $value['customers_order_transport_product_id']!="") {
                        $customersOrderTransportProduct=Models\CustomersOrderTransportProduct::find($value['customers_order_transport_product_id']);
                        if($customersOrderTransportProduct->customers_order_product_count==(($customersOrderTransportProduct->customers_order_transport_product_waste+$customersOrderTransportProduct->customers_order_transport_product_complete)+$value['count'])){
                            $customersOrderTransportProduct->customers_order_transport_product_status='complete';
                        }else{
                            $customersOrderTransportProduct->customers_order_transport_product_status='send';
                        }
                        $customersOrderTransportProduct->customers_order_transport_product_waste=$value['count'];
                        $customersOrderTransportProduct->save();
                    }
                }
            }

            $customersOrderTransport=Models\CustomersOrderTransport::where('customers_order_id','=',$customers_order_id);
            foreach($customersOrderTransport as $key=>$value){
                $customersOrderTransportProduct=Models\CustomersOrderTransportProduct::where('customers_order_transport_id','=',$value->customers_order_transport_id)->get();
                $status=true;
                foreach($customersOrderTransportProduct as $key=>$value){
                    if($value->customers_order_transport_product_status!='complete'){
                        $status=false;
                    }
                }
                if($status){
                    $customersOrderTransport=Models\CustomersOrderTransport::find($value->customers_order_transport_id);
                    $customersOrderTransport->customers_order_transport_status='complete';
                    $customersOrderTransport->save();
                }
            }
            return Redirect::to('/factory/ReportTransport?orderId='.$customers_order_id);
        }
    }

    public function addCompleteTransport(){
        if(!empty($_POST)) {
            $customers_order_id = Input::get('customers_order_id', 0);
            $complete = Input::get('complete', '');
            if(!empty($complete)){
                foreach ($complete as $key => $value) {
                    if (!empty($value['status']) and $value['status'] == "on" and $value['customers_order_transport_product_id']!="") {
                        $customersOrderTransportProduct=Models\CustomersOrderTransportProduct::find($value['customers_order_transport_product_id']);
                        if($customersOrderTransportProduct->customers_order_product_count==(($customersOrderTransportProduct->customers_order_transport_product_waste+$customersOrderTransportProduct->customers_order_transport_product_complete)+$value['count'])){
                            $customersOrderTransportProduct->customers_order_transport_product_status='complete';
                        }else{
                            $customersOrderTransportProduct->customers_order_transport_product_status='send';
                        }
                        $customersOrderTransportProduct->customers_order_transport_product_complete=$value['count'];
                        $customersOrderTransportProduct->save();
                    }
                }
            }

            $customersOrderTransport=Models\CustomersOrderTransport::where('customers_order_id','=',$customers_order_id);
            foreach($customersOrderTransport as $key=>$value){
                $customersOrderTransportProduct=Models\CustomersOrderTransportProduct::where('customers_order_transport_id','=',$value->customers_order_transport_id)->get();
                $status=true;
                foreach($customersOrderTransportProduct as $key=>$value){
                    if($value->customers_order_transport_product_status!='complete'){
                        $status=false;
                    }
                }
                if($status){
                    $customersOrderTransport=Models\CustomersOrderTransport::find($value->customers_order_transport_id);
                    $customersOrderTransport->customers_order_transport_status='complete';
                    $customersOrderTransport->save();
                }
            }
            return Redirect::to('/factory/ReportTransport?orderId='.$customers_order_id);
        }
    }

    public function createTransport(){
        if(!empty($_POST)) {
            $customers_order_id = Input::get('customers_order_id', 0);
            $transport = Input::get('transport', '');
            if(!empty($transport)){
                $user = Session::get('user');
                $customersOrderTransport=new Models\CustomersOrderTransport();
                $customersOrderTransport->customers_order_id=$customers_order_id;
                $customersOrderTransport->customers_order_transport_code=date('YmdHms');
                $customersOrderTransport->customers_order_transport_staff=$user->staff_username;
                $customersOrderTransport->customers_order_transport_status='create';
                if($customersOrderTransport->save()) {
                    foreach ($transport as $key => $value) {
                        if (!empty($value['status']) and $value['status'] == "on") {
                            $vehicles=Models\Vehicle::find($value['vehicles_id']);
                            $customersOrderTransportProduct=new Models\CustomersOrderTransportProduct();
                            $customersOrderTransportProduct->vehicles_id=$value['vehicles_id'];
                            if(!empty($vehicles)) {
                                $customersOrderTransportProduct->vehicles_name = $vehicles->license_plate;
                            }
                            $customersOrderTransportProduct->customers_order_id=$customers_order_id;
                            $customersOrderTransportProduct->customers_order_transport_id=$customersOrderTransport->customers_order_transport_id;
                            $customersOrderTransportProduct->customers_order_product_count=$value['count'];
                            $customersOrderTransportProduct->customers_order_product_id=$key;
                            $customersOrderTransportProduct->customers_order_transport_product_staff=$user->staff_username;
                            $customersOrderTransportProduct->customers_order_transport_product_status='create';
                            $customersOrderTransportProduct->save();
                        }
                    }
                }
                return Redirect::to('/factory/printTransport?orderId='.$customersOrderTransport->customers_order_transport_id);
            }
        }
    }

    public function closeCustomersOrder(){
        $orderId = Input::get('orderId', 0);
        $customersOrder=Models\CustomersOrder::find($orderId);
        if(!empty($customersOrder)){
            $customersOrder->customers_order_status='complete';
            if($customersOrder->save()){
                return Redirect::to('/factory/MarkOrderManager');
            }
        }
    }

    public function Transport()
    {
        $search = Input::get('search', '');
        $page = Input::get('page', 1);
        $customersOrder = new Models\CustomersOrder();
        $countRow = 30;
        $dataCustomersOrder = $customersOrder->findAllCustomersAndPaginateTransport($countRow, $page, $search);
//        return view('factory.MarkOrderCheckQuality', array('dataCustomersOrder'=>$dataCustomersOrder));
        return view('factory.Transport', array('dataCustomersOrder'=>$dataCustomersOrder));
    }

    public function ReportTransport()
    {
        $orderId = Input::get('orderId', 0);
        $customersOrderProduct=Models\CustomersOrderProduct::where('customers_order_id','=',$orderId)->get();
        $vehicles=Models\Vehicle::all();
        return view('factory.ReportTransport', array('vehicles'=>$vehicles,'customersOrderProduct'=>$customersOrderProduct,'orderId'=>$orderId));
    }

    public function MarkOrderStore(){
        $search = Input::get('search', '');
        $page = Input::get('page', 1);
        $customersOrder = new Models\CustomersOrder();
        $countRow = 30;
        $dataCustomersOrder = $customersOrder->findAllCustomersAndPaginateStore($countRow, $page, $search);
        return view('factory.MarkOrderStore', array('customersOrder' => $customersOrder, 'dataCustomersOrder' => $dataCustomersOrder));
    }

    public function BringManufactureStore()
    {
        $orderId = Input::get('orderId', 0);
        $order_product_id= Input::get('order_product_id', 0);
        $order_plan_id= Input::get('order_plan_id', 0);
        $dateSelect= Input::get('dateSelect', '');
        $status=false;
        if(!empty($orderId)){
            $dataOrderProduct=Models\CustomersOrderProduct::where('customers_order_id','=',$orderId)->get();
            $dataCustomersOrder=Models\CustomersOrder::find($orderId);
            $dataDateCustomersOrderPlan=Models\CustomersOrderPlan::getDatePlan($orderId,'approve');
            if(!empty($dateSelect)) {
                $dataCustomersOrderPlan = Models\CustomersOrderPlan::where('customers_order_id', '=', $orderId)->where('customers_order_plan_status', '=', 'approve')->where('customers_order_plan_date', '=', $dateSelect)->get();
            }else{
                $dataCustomersOrderPlan = Models\CustomersOrderPlan::where('customers_order_id', '=', $orderId)->where('customers_order_plan_status', '=', 'approve')->get();
            }
        }
        return view('factory.BringManufactureStore', array('status'=>$status,'orderId'=>$orderId,'dataCustomersOrderPlan'=>$dataCustomersOrderPlan,'dataDateCustomersOrderPlan'=>$dataDateCustomersOrderPlan,'dataCustomersOrder'=>$dataCustomersOrder,'dataOrderProduct'=>$dataOrderProduct,'orderId'=>$orderId));
    }

    public function ConfirmBringMaterial(){
        $orderId = Input::get('orderId', 0);
        $customersOrderPlan=Models\CustomersOrderPlan::find($orderId);
        $customersOrderPlanMaterialAll=Models\CustomersOrderPlanMaterial::where('customers_order_plan_id','=',$orderId)->get();
        $user = Session::get('user');
        if(!empty($customersOrderPlanMaterialAll)){
            foreach($customersOrderPlanMaterialAll as $key=>$value){
                $customersOrderPlanMaterial=Models\CustomersOrderPlanMaterial::find($value->customers_order_plan_material_id);
                $customersOrderPlanMaterial->customers_order_plan_status='Bring';
                $customersOrderPlanMaterial->customers_order_plan_material_bring=$customersOrderPlanMaterial->customers_order_material_count;
                $customersOrderPlanMaterial->customers_order_plan_material_balance=0;
                $customersOrderPlanMaterial->customers_order_plan_material_staff_bring=$user->staff_username;
                $customersOrderPlanMaterial->save();
            }
            return Redirect::to('/factory/BringManufactureStore?orderId='.$customersOrderPlan->customers_order_id);
        }else{
            return Redirect::to('/factory/BringManufactureStore?orderId='.$customersOrderPlan->customers_order_id);
        }

    }

    public function PrintQuotation(){
        $orderId = Input::get('orderId', 0);
        $customersOrder=Models\CustomersOrder::find($orderId);
        $customersOrderProduct=Models\CustomersOrderProduct::where('customers_order_id','=',$orderId)->where('customers_order_product_type','=','Normal')->get();
        $customers=Models\Customers::find($customersOrder->customers_id);
        $sum=0;
        try {
            $pdf = \PDF::loadView('factory.PaperQuotation',array('sum'=>$sum,'customersOrderProduct'=>$customersOrderProduct,'customers'=>$customers,'customersOrder'=>$customersOrder));
            return $pdf->stream();
        } catch (Exception $e) {
            echo $e->getLine() . ':' . $e->getMessage();
        } finally {

        }
    }

    public function PrintDeposits(){
        $orderId = Input::get('orderId', 0);
        $customersOrder=Models\CustomersOrder::find($orderId);
        $customers=Models\Customers::find($customersOrder->customers_id);
        try {
            $pdf = \PDF::loadView('factory.PaperDeposits',array('customers'=>$customers,'customersOrder'=>$customersOrder));
            return $pdf->stream();
        } catch (Exception $e) {
            echo $e->getLine() . ':' . $e->getMessage();
        } finally {

        }
    }
}