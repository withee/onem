<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models;
use Carbon\Carbon;
use Request;


class GPSTrackingController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function getAlllocation()
    {
        //1. Get input
        $vehicle = Request::input('vehicle', 1);
        $currentdate = Request::input('current-date', date('Y-m-d'));
        $showline = Request::input('show-line', false);
        $showfreeze = Request::input('show-freeze', false);
        $showstart = Request::input('show-startstop', false);
        //2. Declare variable
        $gpstracking = new Models\GPSTrackinglog();
        $vehiclelist = Models\Vehicle::all();

        //3. Process
        $map = $gpstracking->getMap($vehicle, $currentdate, $showline, $showfreeze, $showstart);
        $data['currentdate'] = $currentdate;
        $data ['showline'] = ($showline) ? 'checked' : '';
        $data['showfreeze'] = ($showfreeze) ? 'checked' : '';
        $data['showstart'] = ($showstart) ? 'checked' : '';
        $data['maps'] = $map;
        $data['vehiclelist'] = $vehiclelist;
        $data['vehicle'] = $vehicle;


        //4. Return
        return view('maps.gpstracking', $data);
    }

    public function getChartData()
    {
        $vehicle = Request::input('vehicle', 1);
        $currentdate = Request::input('current-date', date('Y-m-d'));
        $result = array();
        try {
            $dayinchart = Carbon::createFromFormat('Y-m-d H:i:s', $currentdate . ' 00:00:00');
            $endday = $dayinchart->copy()->addDay();


            $gps = Models\Vehicle::find($vehicle);

            while ($dayinchart->diffInHours($endday, false) > 0) {
                $previousminuet = $dayinchart->copy();
                $dayinchart->addHours(1);
                $sql = "SELECT COUNT(id) as ids, SUM(speed) as speed FROM gps_trackinglogs
WHERE serial='{$gps->serial}'
AND created_at BETWEEN '$previousminuet' AND '$dayinchart'";
                $tracklist = \DB::select(\DB::raw($sql));
                $totalspeed = empty($tracklist[0]->speed) ? 0 : $tracklist[0]->speed;
                $rows = empty($tracklist[0]->ids) ? 1 : $tracklist[0]->ids;
                $avgspeed = (int)($totalspeed / $rows);
                $dataset = array($dayinchart->toTimeString(), $avgspeed);
                $result[] = $dataset;

            }
        } catch (\Exception $e) {
            echo $e->getLine() . ':' . $e->getMessage();
        }
        return json_encode($result);

    }

    public function getChartDataInHour()
    {
        $vehicle = Request::input('vehicle', 1);
        $currentdate = Request::input('current-date', date('Y-m-d'));
        $currenthour = Request::input('current-hour', date('H:i:s'));
        $result = array();
        try {

            $endday = Carbon::createFromFormat('Y-m-d H:i:s', $currentdate . " $currenthour");
            $dayinchart = $endday->copy()->subHour();
            $gps = Models\Vehicle::find($vehicle);
            while ($dayinchart->diffInMinutes($endday, false) > 0) {
                $previousminuet = $dayinchart->copy();
                $dayinchart->addMinutes(5);
                $sql = "SELECT COUNT(id) as ids, SUM(speed) as speed FROM gps_trackinglogs
WHERE serial='{$gps->serial}'
AND created_at BETWEEN '$previousminuet' AND '$dayinchart'";
                $tracklist = \DB::select(\DB::raw($sql));
                $totalspeed = empty($tracklist[0]->speed) ? 0 : $tracklist[0]->speed;
                $rows = empty($tracklist[0]->ids) ? 1 : $tracklist[0]->ids;
                $avgspeed = (int)($totalspeed / $rows);
                $dataset = array($dayinchart->toTimeString(), $avgspeed);
                $result[] = $dataset;

            }
        } catch (\Exception $e) {
            echo $e->getLine() . ':' . $e->getMessage();
        }
        return json_encode($result);

    }
}
