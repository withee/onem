<?php namespace App\Http\Controllers;

/**
 * Created by PhpStorm.
 * User: sootipong
 * Date: 5/18/2015
 * Time: 1:11 AM
 */

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models;
use Illuminate\Support\Facades\Response;
use Request;
use Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Paginator;

class StoreController extends Controller
{
    public function addLogStore()
    {
        //Get input  and declare variable
        $store = new Models\Stores();
//        $id_member = Input::get('id_member', '');
        $storename_id = Input::get('storename_id', 0);
        $product_id = Input::get('product_id', '');
        $log_stores_count = Input::get('log_stores_count', 0);
        $log_stores_type = Input::get('log_stores_type', '');
        $log_return_detail = Input::get('log_return_detail', '');

        //set rules
        $rules = array(
//            'id_member' => 'required',
            'storename_id' => 'required|integer',
            'product_id' => 'required',
            'log_stores_count' => 'required|integer',
            'log_stores_type' => 'required',
            'log_return_detail' => 'required',
        );
        //validate input
        $validator = Validator::make(Input::all(), $rules);
        //check validate
        if ($validator->fails()) {
            // false validator Redirect
            return Redirect::to('/store/change')
                ->withErrors($validator)
                ->withInput(Input::except('id_member', 'storename_id', 'product_id', 'log_stores_count', 'log_stores_type', 'log_return_detail'));
        } else {
            //true validator save data purchases
            //create log_store
            if (Session::has('user')) {
                $user = Session::get('user');
            }
            $logStore = new Models\LogStores();
            $logStore->id_member = $user['staff_id'];
            $logStore->storename_id = $storename_id;
            $logStore->product_id = $product_id;
            $logStore->log_stores_count = $log_stores_count;
            $logStore->log_stores_type = $log_stores_type;
            $logStore->log_return_detail = $log_return_detail;
            if ($logStore->save()) {
                $store->updateStore($product_id, $storename_id, $log_stores_count, $log_stores_type);
            }
            return Redirect::to('/store/change');
        }
    }

    public function changeStore()
    {
        try {
            $logStore = new Models\LogStores();
            $dataStaff = Models\Staffs::all();
            $sql = "SELECT p.product_id,p.product_name,pu.unit_name FROM products p
LEFT JOIN ref_product_units pu
ON pu.unit_id=p.product_unit";
            $data = \DB::select(\DB::raw($sql));
            //var_dump($data);exit();
            $dataProduct = $data;

            $dataStoreName = Models\Storename::all();
            $dataStore = $logStore->getAllData();
            return view('store.changeStore', array('dataStaff' => $dataStaff, 'dataStore' => $dataStore, 'dataStoreName' => $dataStoreName, 'dataProduct' => $dataProduct));
        } catch (\Exception $e) {
            echo $e->getLine() . ":" . $e->getMessage();
        }
    }

    public function searchProduct()
    {
        //1. Get input  and declare variable
        $id = Input::get('id', 0);
        $search = Input::get('search', '');
        $page = Input::get('page', 1);
        $product = Array();
        $dataProduct = new Models\Product();
        $countRow = 30;
        //2. Process
        if (false == empty($id) && false == empty($search)) {
            $product = $dataProduct->findAllProductInventoryStoreAndPaginate($id, $countRow, $page, $search);
        }
        //3. Return to view
        return view('store.view', array('product' => $product, 'id' => $id, 'search' => $search));
    }

    public function viewStore()
    {
        //1. Get input  and declare variable
        $id = Input::get('id', 0);
        $search = Input::get('search', '');
        $page = Input::get('page', 1);
        $product = Array();
        $dataProduct = new Models\Product();
        $countRow = 30;
        //2. Process
        if (false == empty($id)) {
            $product = $dataProduct->findAllProductInventoryStoreAndPaginate($id, $countRow, $page, $search);
        }
        //3. Return to view
        return view('store.view', array('product' => $product, 'id' => $id, 'search' => $search));
    }
}