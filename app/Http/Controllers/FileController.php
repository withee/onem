<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Request;
use App\Models;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Contracts\Filesystem\Factory;
use Illuminate\Support\Facades\Redirect;

class FileController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function upload()
    {
        $files = Request::file('files');
        $id = Request::input('id');
        $name = Request::input('filename');
        $result = array('success' => true, 'desc' => 'Operation Success!!', 'data' => array());
        try {
            $extension = $files->getClientOriginalExtension();
            $path = $files->getFilename() . '.' . $extension;
            $storage = Storage::disk('images')->put($path, File::get($files));
            $newfile = new Models\Files();
            $newfile->path = "/images/" . $path;
            $newfile->name = $name;
            $newfile->save();
            $result['data'] = $newfile;
        } catch (\Exception $e) {
            //echo $e->getLine() . ":" . $e->getMessage();
            $result['success'] = false;
            $result['desc'] = $e->getLine() . ":" . $e->getMessage();
        }

        return Redirect::to('/project/viewImageProjectSub?id='.$id);
    }

    public function getImg()
    {
        $result = array('success' => true, 'desc' => 'Operation Success!!', 'data' => array());
        $name = Request::input('filename');

        try {

            $file = Models\Files::where('name', '=', "$name")->orderby('id', 'DESC')->first();

            if (empty($file)) {
                $img = new Models\Files();
                $img->path = "/images/default.jpg";
                $img->name = 'default';
                $result['data'] = $img->path;
            }else{
                $result['data'] = $file->path;
            }
        } catch (\Exception $e) {
            //echo $e->getLine() . ":" . $e->getMessage();
            $result['success'] = false;
            $result['desc'] = $e->getLine() . ":" . $e->getMessage();
        }

        //var_dump($result['data']);exit;
        echo "<img src='{$result['data']}'>";
        //return json_encode($result);
    }

}
