<?php namespace App\Http\Controllers;
/**
 * Created by PhpStorm.
 * User: sootipong
 * Date: 5/26/2015
 * Time: 3:48 AM
 */
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models;
use Illuminate\Support\Facades\Response;
use Request;
use Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class GroupBringController extends Controller
{
    public function remove(){
        //1. Get input  and declare variable
        $id=Input::get('id',0);
        $result = array();
        //2. Process
        $deleteControlGroupBuild=new Models\GroupBring();
        $result=$deleteControlGroupBuild->deleteControlGroupBuild($id);
        //3. Redirect
        if('error'==$result['status']){
            return Redirect::to('/groupBring/viewGroupBring')->with('error', 'ไม่สามารถ ลบข้อมูลงานผลิตเเละโครงการ สำเร็จ');
        } else {
            return Redirect::to('/groupBring/viewGroupBring')->with($result['status'],$result['desc']);
        }
    }

    public function viewGroupBring(){
        $id = Input::get('id', 0);
        $search = Input::get('search', '');
        $page = Input::get('page', 1);
        $countRow = 30;
        $groupBring=new Models\GroupBring();
        $listGroupBring=Models\GroupBring::all();
        $dataStaff = Models\Staffs::all();
        $dataVehicles=Models\Vehicle::all();
        $dataGroupBring=$groupBring->findAllGroupBringAndPaginate($countRow,$page,$search);
        return view('groupBring.viewGroupBring',array('dataVehicles'=>$dataVehicles,'dataStaff'=>$dataStaff,'listGroupBring'=>$listGroupBring,'dataGroupBring'=>$dataGroupBring));
    }

    public function addGroupBring(){
        //Get input  and declare variable
        $group_bring_name=Input::get('group_bring_name',0);
        //set rules
        $rules = array(
            'group_bring_name' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);
        //check validate
        if ($validator->fails()) {
            // false Redirect
            $messages = $validator->messages();
            return Redirect::to('/groupBring/viewGroupBring')
                ->withErrors($validator)
                ->withInput(Input::except('group_bring_name'));
        } else {
            //add GroupBring
            $newGroupBring=new Models\GroupBring();
            $newGroupBring->group_bring_name=$group_bring_name;
            $newGroupBring->save();
            return Redirect::to('/groupBring/viewGroupBring');
        }
    }
}