<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Models;
use Illuminate\Support\Facades\Response;
use Request;
use Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class VehicleController extends Controller
{
    public function companyMaintenance(){
        $id = Input::get('id', 0);
        if(!empty($id)){
            $company=Models\CompanyMaintenance::find($id);
        }else{
            $company=New Models\CompanyMaintenance();
        }
        $dataCompany=Models\CompanyMaintenance::all();
        return view('vehicle.companyMaintenance', array('company'=>$company,'dataCompany'=>$dataCompany));
    }

    public function addCompanyMaintenance(){
        if(!empty($_POST)) {
            $company_maintenance_id = Input::get('company_maintenance_id', 0);
            $company_maintenance_name = Input::get('company_maintenance_name', '');
            $company_maintenance_tel = Input::get('company_maintenance_tel', '');
            $company_maintenance_address = Input::get('company_maintenance_address', '');
            //set rules
            $rules = array(
                'company_maintenance_name' => 'required',
            );
            //validate input
            $validator = Validator::make(Input::all(), $rules);
            //check validate
            if ($validator->fails()) {
                // false Redirect
                $messages = $validator->messages();
                return Redirect::to('/vehicle/companyMaintenance')
                    ->withErrors($validator)
                    ->withInput(Input::except('company_maintenance_name'));
            } else {
                if(!empty($company_maintenance_id) and $company_maintenance_id!=''){
                    $company=Models\CompanyMaintenance::find($company_maintenance_id);
                }else{
                    $company=new Models\CompanyMaintenance();
                }
                $company->company_maintenance_name=$company_maintenance_name;
                $company->company_maintenance_tel=$company_maintenance_tel;
                $company->company_maintenance_address=$company_maintenance_address;
                if ($company->save()) {
                    return Redirect::to('/vehicle/companyMaintenance');
                }
            }
        }
    }

    public function removeCompanyMaintenance(){
        $id = Input::get('id', 0);
        if(!empty($id)) {
            $company = Models\CompanyMaintenance::find($id);
            if($company){
                $company->delete();
                return Redirect::to('/vehicle/companyMaintenance');
            }
        }
    }

    public function vehicleMaintenance(){
        $vehicles_id = Input::get('vehicles_id', 0);
        $id = Input::get('id', 0);
        if(!empty($id)){
            $vehiclesMaintenance=Models\VehiclesMaintenance::find($id);
        }else{
            $vehiclesMaintenance=new Models\VehiclesMaintenance();
            $vehiclesMaintenance->vehicles_id=$vehicles_id;
        }
        $vehicles=Models\Vehicle::find($vehicles_id);
        $companyMaintenance=Models\CompanyMaintenance::all();
        $staff=Models\Staffs::where('staff_usertype', '!=', 1)->where('staff_usertype', '!=',2)->where('staff_usertype', '!=',8)->get();
        $dataVehiclesMaintenance=Models\VehiclesMaintenance::where('vehicles_id','=',$vehicles_id)->orderBy('vehicles_maintenance_status', 'ASC')->get();
        return view('vehicle.vehiclesMaintenance', array('dataVehiclesMaintenance'=>$dataVehiclesMaintenance,'vehicles'=>$vehicles,'staff'=>$staff,'companyMaintenance'=>$companyMaintenance,'vehiclesMaintenance'=>$vehiclesMaintenance));
    }

    public function addVehiclesMaintenance(){
        if(!empty($_POST)) {
            $vehicles_maintenance_id = Input::get('vehicles_maintenance_id', 0);
            $company_maintenance_id = Input::get('company_maintenance_id', 0);
            $vehicles_maintenance_bill_id = Input::get('vehicles_maintenance_bill_id', '');
            $vehicles_id = Input::get('vehicles_id', 0);
            $staff_id = Input::get('staff_id', '');
            $vehicles_maintenance_symptom = Input::get('vehicles_maintenance_symptom', '');
            $vehicles_maintenance_remark = Input::get('vehicles_maintenance_remark', '');
            //set rules
            $rules = array(
                'company_maintenance_id' => 'required',
                'staff_id' => 'required',
                'vehicles_maintenance_symptom' => 'required',
            );
            //validate input
            $validator = Validator::make(Input::all(), $rules);
            //check validate
            if ($validator->fails()) {
                // false Redirect
                $messages = $validator->messages();
                return Redirect::to('/vehicle/vehicleMaintenance?vehicles_id='.$vehicles_id)
                    ->withErrors($validator)
                    ->withInput(Input::except('company_maintenance_id','vehicles_maintenance_bill_id','staff_id','vehicles_maintenance_symptom'));
            } else {
                if(!empty($vehicles_maintenance_id) and $vehicles_maintenance_id!=''){
                    $vehiclesMaintenance=Models\VehiclesMaintenance::find($vehicles_maintenance_id);
                }else{
                    $vehiclesMaintenance=new Models\VehiclesMaintenance();
                }
                $vehiclesMaintenance->company_maintenance_id=$company_maintenance_id;
                $vehiclesMaintenance->vehicles_maintenance_bill_id=$vehicles_maintenance_bill_id;
                $vehiclesMaintenance->vehicles_id=$vehicles_id;
                $vehiclesMaintenance->staff_id=$staff_id;
                $vehiclesMaintenance->vehicles_maintenance_symptom=$vehicles_maintenance_symptom;
                $vehiclesMaintenance->vehicles_maintenance_remark=$vehicles_maintenance_remark;
                $vehiclesMaintenance->vehicles_maintenance_status='repair';
                if ($vehiclesMaintenance->save()) {
                    return Redirect::to('/vehicle/vehicleMaintenance?vehicles_id='.$vehicles_id);
                }
            }
        }
    }

    public function removeVehicleMaintenance(){
        $id = Input::get('id', 0);
        if(!empty($id)) {
            $vehiclesMaintenance = Models\VehiclesMaintenance::find($id);
            if(!empty($vehiclesMaintenance)){
                $vehiclesMaintenance->delete();
                return Redirect::to('/vehicle/vehicleMaintenance?vehicles_id='.$vehiclesMaintenance->vehicles_id);
            }
        }
    }

    public function vehiclesMaintenanceDetail(){
        $vehicles_maintenance_id = Input::get('vehicles_maintenance_id', 0);
        $id = Input::get('id', 0);
        if(!empty($id)){
            $vehiclesMaintenanceDetail=Models\VehiclesMaintenanceDetail::find($id);
        }else{
            $vehiclesMaintenanceDetail=new Models\VehiclesMaintenanceDetail();
            $vehiclesMaintenanceDetail->vehicles_maintenance_id=$vehicles_maintenance_id;
        }
        $vehiclesMaintenance=Models\VehiclesMaintenance::find($vehicles_maintenance_id);
        $companyMaintenance=Models\CompanyMaintenance::find($vehiclesMaintenance->company_maintenance_id);
        $vehicles=Models\Vehicle::find($vehiclesMaintenance->vehicles_id);
        $dataVehiclesMaintenanceDetail=Models\VehiclesMaintenanceDetail::where('vehicles_maintenance_id','=',$vehicles_maintenance_id)->orderBy('vehicles_maintenance_detail_status', 'ASC')->get();
        return view('vehicle.vehiclesMaintenanceDetail', array('companyMaintenance'=>$companyMaintenance,'vehicles'=>$vehicles,'dataVehiclesMaintenanceDetail'=>$dataVehiclesMaintenanceDetail,'vehiclesMaintenance'=>$vehiclesMaintenance,'vehiclesMaintenanceDetail'=>$vehiclesMaintenanceDetail));
    }

    public function addVehiclesMaintenanceDetail(){
        if(!empty($_POST)) {
            $vehicles_maintenance_detail_id = Input::get('vehicles_maintenance_detail_id', 0);
            $vehicles_maintenance_id = Input::get('vehicles_maintenance_id', 0);
            $vehicles_maintenance_detail_count = Input::get('vehicles_maintenance_detail_count', 0);
            $vehicles_maintenance_detail_name = Input::get('vehicles_maintenance_detail_name', '');
            $vehicles_maintenance_detail_price = Input::get('vehicles_maintenance_detail_price', 0);
            $vehicles_maintenance_detail_status= Input::get('vehicles_maintenance_detail_status', 0);
            //set rules
            $rules = array(
                'vehicles_maintenance_detail_count' => 'required',
                'vehicles_maintenance_detail_name' => 'required',
                'vehicles_maintenance_detail_price' => 'required',
                'vehicles_maintenance_detail_status' => 'required',
            );
            //validate input
            $validator = Validator::make(Input::all(), $rules);
            //check validate
            if ($validator->fails()) {
                // false Redirect
                $messages = $validator->messages();
                return Redirect::to('/vehicle/vehiclesMaintenanceDetail?vehicles_maintenance_id='.$vehicles_maintenance_id)
                    ->withErrors($validator)
                    ->withInput(Input::except('vehicles_maintenance_detail_count','vehicles_maintenance_detail_name','vehicles_maintenance_detail_price'));
            } else {
                if(!empty($vehicles_maintenance_detail_id) and $vehicles_maintenance_detail_id!=''){
                    $vehiclesMaintenanceDetail=Models\VehiclesMaintenanceDetail::find($vehicles_maintenance_detail_id);
                }else{
                    $vehiclesMaintenanceDetail=new Models\VehiclesMaintenanceDetail();
                }
                $vehiclesMaintenanceDetail->vehicles_maintenance_id=$vehicles_maintenance_id;
                $vehiclesMaintenanceDetail->vehicles_maintenance_detail_count=$vehicles_maintenance_detail_count;
                $vehiclesMaintenanceDetail->vehicles_maintenance_detail_name=$vehicles_maintenance_detail_name;
                $vehiclesMaintenanceDetail->vehicles_maintenance_detail_price=$vehicles_maintenance_detail_price;
                $vehiclesMaintenanceDetail->vehicles_maintenance_detail_total=$vehicles_maintenance_detail_count*$vehicles_maintenance_detail_price;
                $vehiclesMaintenanceDetail->vehicles_maintenance_detail_status=$vehicles_maintenance_detail_status;
                if ($vehiclesMaintenanceDetail->save()) {
                    return Redirect::to('/vehicle/vehiclesMaintenanceDetail?vehicles_maintenance_id='.$vehicles_maintenance_id);
                }
            }
        }
    }

    public function removeVehiclesMaintenanceDetail(){
        $id = Input::get('id', 0);
        if(!empty($id)) {
            $vehiclesMaintenanceDetail = Models\VehiclesMaintenanceDetail::find($id);
            if(!empty($vehiclesMaintenanceDetail)){
                $vehiclesMaintenanceDetail->delete();
                return Redirect::to('/vehicle/vehiclesMaintenanceDetail?vehicles_maintenance_id='.$vehiclesMaintenanceDetail->vehicles_maintenance_id);
            }
        }
    }

    public function payVehiclesMaintenanceDetail(){
        $id = Input::get('id', 0);
        if(!empty($id)) {
            $vehiclesMaintenanceDetail = Models\VehiclesMaintenanceDetail::find($id);
            if(!empty($vehiclesMaintenanceDetail)) {
                $vehiclesMaintenanceDetail->vehicles_maintenance_detail_status='pay';
                if($vehiclesMaintenanceDetail->save()){
                    return Redirect::to('/vehicle/vehiclesMaintenanceDetail?vehicles_maintenance_id='.$vehiclesMaintenanceDetail->vehicles_maintenance_id);
                }
            }
        }
    }

    public function clearMaintenanceDetail(){
        $id = Input::get('id', 0);
        if(!empty($id)) {
            $vehiclesMaintenance=Models\VehiclesMaintenance::find($id);
            $vehiclesMaintenanceDetail = Models\VehiclesMaintenanceDetail::where('vehicles_maintenance_id','=',$id)->get();
            if(!empty($vehiclesMaintenanceDetail) and !empty($vehiclesMaintenance)){
                foreach($vehiclesMaintenanceDetail as $key=>$value){
                    $vehiclesMaintenanceDetail = Models\VehiclesMaintenanceDetail::find($value->vehicles_maintenance_detail_id);
                    if(!empty($vehiclesMaintenanceDetail)) {
                        $vehiclesMaintenanceDetail->vehicles_maintenance_detail_status='pay';
                        $vehiclesMaintenanceDetail->save();
                    }
                }
                $vehiclesMaintenance->vehicles_maintenance_status='pay';
                if($vehiclesMaintenance->save()) {
                    return Redirect::to('/vehicle/vehiclesMaintenanceDetail?vehicles_maintenance_id=' . $vehiclesMaintenance->vehicles_maintenance_id);
                }
            }
        }
    }
}