<?php namespace App\Http\Controllers;

/**
 * Created by PhpStorm.
 * User: sootipong
 * Date: 11/17/2015
 * Time: 10:35
 */
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Models;
use Illuminate\Support\Facades\Response;
use Request;
use Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class PrController extends Controller
{
    public function listPr(){
        $id = Input::get('id', 0);
        $orderId = Input::get('orderId', 0);
        $productId = Input::get('productId', 0);
        $orderMaterial = Input::get('orderMaterial', '');
        $search = Input::get('search', '');
        $page = Input::get('page', 1);
        $countRow = 30;
        $PrOrder=new Models\PrOrder();
        if(!empty($id)){
            $PrOrder=Models\PrOrder::find($id);
        }else{
            $PrOrder->pr_order_number=date('YmdHms', strtotime('+1 Seconds'));
        }
        $user = Session::get('user');
        if(!empty($orderId)and!empty($productId)) {
//            $customersOrder=Models\CustomersOrder::find($orderId);
            $customersOrderProduct=Models\CustomersOrderProduct::find($productId);
            $customersOrderMaterial=Models\CustomersOrderMaterial::find($orderMaterial);
            $PrOrder=$PrOrder::where('customers_order_id','=',$orderId)->where('customers_order_product_id','=',$productId)->first();
            if(!empty($PrOrder)){
                $PrOrderDetail=Models\PrOrderDetail::where('PR_order_id','=',$PrOrder->PR_order_id)->where('PR_order_detail_name','=',$customersOrderMaterial->customers_order_material_name)->first();
                if(empty($PrOrderDetail)) {
                    $newPrOrderDetail = new Models\PrOrderDetail();
                    $newPrOrderDetail->PR_order_id = $PrOrder->PR_order_id;
                    $newPrOrderDetail->PR_order_detail_name = $customersOrderMaterial->customers_order_material_name;
                    $newPrOrderDetail->PR_order_detail_count = 0;
                    $newPrOrderDetail->PR_order_detail_staff = $user->staff_username;
                    $newPrOrderDetail->PR_order_detail_status = 'New';
                    if ($newPrOrderDetail->save()) {
                        return Redirect::to('/Pr/listPrOrder?orderId=' . $PrOrder->PR_order_id);
                    }
                }else{
                    return Redirect::to('/Pr/listPrOrder?orderId=' . $PrOrder->PR_order_id);
                }
            }else{
                $newPrOrder=new Models\PrOrder();
                $newPrOrder->PR_order_name='ผลิต '.$customersOrderProduct->customers_order_product_name;
                $newPrOrder->pr_order_number=date('YmdHms', strtotime('+1 Seconds'));
                $newPrOrder->customers_order_id=$orderId;
                $newPrOrder->customers_order_product_id=$productId;
                $newPrOrder->PR_order_staff = $user->staff_username;
                $newPrOrder->PR_order_status = 'New';
                if($newPrOrder->save()){
                    $PrOrderDetail=Models\PrOrderDetail::where('PR_order_id','=',$PrOrder->PR_order_id)->where('PR_order_detail_name','=',$customersOrderMaterial->customers_order_material_name)->first();
                    if(empty($PrOrderDetail)) {
                        $newPrOrderDetail = new Models\PrOrderDetail();
                        $newPrOrderDetail->PR_order_id = $newPrOrder->PR_order_id;
                        $newPrOrderDetail->PR_order_detail_name = $customersOrderMaterial->customers_order_material_name;
                        $newPrOrderDetail->PR_order_detail_count = 0;
                        $newPrOrderDetail->PR_order_detail_staff = $user->staff_username;
                        $newPrOrderDetail->PR_order_detail_status = 'New';
                        if ($newPrOrderDetail->save()) {
                            return Redirect::to('/Pr/listPrOrder?orderId=' . $newPrOrder->PR_order_id);
                        }
                    }else{
                        return Redirect::to('/Pr/listPrOrder?orderId=' . $newPrOrder->PR_order_id);
                    }
                }
            }
        }
        $dataPrOrder=$PrOrder->findAllPrOrderAndPaginate($countRow,$page,$search);
        $user = Session::get('user');
        return view('pr.listPr', array('user'=>$user,'PrOrder'=>$PrOrder,'dataPrOrder'=>$dataPrOrder));
    }

    public function deletePrOrder(){
        $id = Input::get('id', 0);
        if (!empty($id)) {
            $PrOrder = Models\PrOrder::find($id);
            if (!empty($PrOrder)) {
                $PrOrder->delete();
                return Redirect::to('/Pr/listPr');
            } else {
                return Redirect::to('/Pr/listPr');
            }
        } else {
            return Redirect::to('/Pr/listPr');
        }
    }

    public function createPr(){
        if (!empty($_POST)) {
            $PR_order_id = Input::get('PR_order_id', '');
            $PR_order_name = Input::get('PR_order_name', '');
            $pr_order_tax_number = Input::get('pr_order_tax_number', '');
            $pr_order_number = Input::get('pr_order_number', '');
            $pr_order_date = Input::get('pr_order_date', '');
            $pr_order_tax = Input::get('pr_order_tax', '');
            $pr_order_discount = Input::get('pr_order_discount', '');
            //set rules
            $rules = array(
                'PR_order_name' => 'required',
            );
            //validate input
            $validator = Validator::make(Input::all(), $rules);
            //check validate
            if ($validator->fails()) {
                // false Redirect
                $messages = $validator->messages();
                return Redirect::to('/Pr/listPr')
                    ->withErrors($validator)
                    ->withInput(Input::except('PR_order_name'));
            } else {
                $user = Session::get('user');
                $newPrOrder = new Models\PrOrder();
                if (!empty($PR_order_id)) {
                    $newPrOrder = Models\PrOrder::find($PR_order_id);
                }
                $newPrOrder->PR_order_name = $PR_order_name;
                $newPrOrder->pr_order_tax_number=$pr_order_tax_number;
                $newPrOrder->pr_order_number=$pr_order_number;
                $newPrOrder->pr_order_date=$pr_order_date;
                $newPrOrder->pr_order_tax=$pr_order_tax;
                $newPrOrder->pr_order_discount=$pr_order_discount;
                $newPrOrder->PR_order_staff = $user->staff_username;
                $newPrOrder->PR_order_status = 'New';
                if ($newPrOrder->save()) {
                    return Redirect::to('/Pr/listPr');
                }
            }
        }
    }

    public function listPrOrder(){
        $PR_order_id = Input::get('orderId', 0);
        $customersOrder=Models\PrOrder::find($PR_order_id);
        $dataOrderDetail=Models\PrOrderDetail::where('PR_order_id','=',$PR_order_id)->get();
        return view('pr.listPrOrder', array('customersOrder'=>$customersOrder,'PR_order_id'=>$PR_order_id,'dataOrderDetail'=>$dataOrderDetail));
    }

    public function deletePrOrderDetail(){
        $id = Input::get('id', 0);
        $orderId = Input::get('orderId', 0);
        if (!empty($id)) {
            $PrOrderDetail = Models\PrOrderDetail::find($id);
            if (!empty($PrOrderDetail)) {
                $PrOrderDetail->delete();
                return Redirect::to('/Pr/listPrOrder?orderId='.$orderId);
            } else {
                return Redirect::to('/Pr/listPrOrder?orderId='.$orderId);
            }
        } else {
            return Redirect::to('/Pr/listPrOrder?orderId='.$orderId);
        }
    }

    public function confirmPrOrder(){
        $id = Input::get('id', 0);
        if (!empty($id)) {
            $user = Session::get('user');
            $PrOrder = Models\PrOrder::find($id);
            if (!empty($PrOrder)) {
                $PrOrder->PR_order_staff = $user->staff_username;
                if($user->staff_usertype=='5'){
                    $PrOrder->PR_order_status='confirmPurchasing';
                }elseif($user->staff_usertype=='6'){
                    $PrOrder->PR_order_status='confirmStore';
                }elseif($user->staff_usertype=='2'){
                    $PrOrder->PR_order_status='confirmManager';
                }else{
                    $PrOrder->PR_order_status='confirm';
                }
                if($PrOrder->save()) {
                    return Redirect::to('/Pr/listPr');
                }
            } else {
                return Redirect::to('/Pr/listPr');
            }
        } else {
            return Redirect::to('/Pr/listPr');
        }
    }

    public function addPrMaterial(){
        if (!empty($_POST)) {
            $PR_order_detail_id = Input::get('PR_order_detail_id', '');
            $PR_order_id = Input::get('PR_order_id', '');
            $PR_order_detail_name = Input::get('PR_order_detail_name', '');
            $PR_order_detail_count = Input::get('PR_order_detail_count', 0);
            //set rules
            $rules = array(
                'PR_order_detail_name' => 'required',
                'PR_order_detail_count' => 'required|numeric',
            );
            //validate input
            $validator = Validator::make(Input::all(), $rules);
            //check validate
            if ($validator->fails()) {
                // false Redirect
                $messages = $validator->messages();
                return Redirect::to('/Pr/listPrOrder?orderId='.$PR_order_id)
                    ->withErrors($validator)
                    ->withInput(Input::except('PR_order_detail_name','PR_order_detail_count'));
            } else {
                $material = explode(":",$PR_order_detail_name);
                $user = Session::get('user');
                $newPrOrderDetail = new Models\PrOrderDetail();
                if (!empty($PR_order_detail_id)) {
                    $newPrOrderDetail = Models\PrOrderDetail::find($PR_order_detail_id);
                }
                $newPrOrderDetail->PR_order_id=$PR_order_id;
                $newPrOrderDetail->PR_order_detail_name = $material[0];
                $newPrOrderDetail->PR_order_detail_count = $PR_order_detail_count;
                $newPrOrderDetail->PR_order_detail_staff = $user->staff_username;
                $newPrOrderDetail->PR_order_detail_status = 'New';
                if ($newPrOrderDetail->save()) {
                    return Redirect::to('/Pr/listPrOrder?orderId='.$PR_order_id);
                }
            }
        }
    }

    public function PrintPr(){
        $orderId = Input::get('orderId', 0);
        $prOrder=Models\PrOrder::find($orderId);
        $prOrderDetail=Models\PrOrderDetail::where('PR_order_id','=',$orderId)->get();
        $customersOrder=new Models\CustomersOrder();
        $customers=new Models\Customers();
        if(!empty($prOrder->customers_order_id)){
            $customersOrder=Models\CustomersOrder::find($prOrder->customers_order_id);
            $customers=Models\Customers::find($customersOrder->customers_id);
        }
        $sum=0;
        try {
            $pdf = \PDF::loadView('pr.PaperPr',array('sum'=>$sum,'prOrder'=>$prOrder,'prOrderDetail'=>$prOrderDetail,'customersOrder'=>$customersOrder,'customers'=>$customers));
            return $pdf->stream();
        } catch (Exception $e) {
            echo $e->getLine() . ':' . $e->getMessage();
        } finally {

        }
    }

    public function PrintPo(){
        $orderId = Input::get('orderId', 0);
        $prOrder=Models\PrOrder::find($orderId);
        $prOrderDetail=Models\PrOrderDetail::where('PR_order_id','=',$orderId)->get();
        $customersOrder=new Models\CustomersOrder();
        $customers=new Models\Customers();
        if(!empty($prOrder->customers_order_id)){
            $customersOrder=Models\CustomersOrder::find($prOrder->customers_order_id);
            $customers=Models\Customers::find($customersOrder->customers_id);
        }
        $sum=0;
        try {
            $pdf = \PDF::loadView('pr.PaperPo',array('sum'=>$sum,'prOrder'=>$prOrder,'prOrderDetail'=>$prOrderDetail,'customersOrder'=>$customersOrder,'customers'=>$customers));
            return $pdf->stream();
        } catch (Exception $e) {
            echo $e->getLine() . ':' . $e->getMessage();
        } finally {

        }
    }

    public function addPrice(){
        $PR_order_id = Input::get('PR_order_id', 0);
        $order = Input::get('order', 0);
        if(!empty($order)){
            foreach($order as $key=>$value){
                $prOrderDetail=Models\PrOrderDetail::find($key);
                $prOrderDetail->PR_order_detail_accepted=$value['count'];
                $prOrderDetail->pr_order_detail_price=$value['price'];
                $prOrderDetail->save();
            }
            return Redirect::to('/Pr/listPrOrder?orderId='.$PR_order_id);
        }else{
            return Redirect::to('/Pr/listPrOrder?orderId='.$PR_order_id);
        }
    }
}