<?php namespace App\Http\Controllers;

/**
 * Created by PhpStorm.
 * User: sootipong
 * Date: 5/26/2015
 * Time: 3:48 AM
 */
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Models;
use Illuminate\Support\Facades\Response;
use Request;
use Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class AccountController extends Controller
{
    public function addAccount(){
        if(!empty($_POST)) {
            $id = Input::get('id', 0);
            $name = Input::get('name', '');
            $count = Input::get('count', 0);
            $type = Input::get('type', '');
            $payee = Input::get('payee', '');
            $remark = Input::get('remark', '');
            $project_id = Input::get('project_id', '');
            //set rules
            $rules = array(
                'name' => 'required',
                'type' => 'required',
                'payee' => 'required',
                'project_id' => 'required',
                'count' => 'required|numeric',
            );
            //validate input
            $validator = Validator::make(Input::all(), $rules);
            //check validate
            if ($validator->fails()) {
                // false Redirect
                $messages = $validator->messages();
                return Redirect::to('/account/manage')
                    ->withErrors($validator)
                    ->withInput(Input::except('name', 'type', 'payee', 'remark', 'count'));
            } else {
                $newAccount=new Models\InventoryAccount();
                $dataAccount=Models\InventoryAccount::orderBy('updated_at', 'DESC')->first();
                if($type=='Income'){
                    if(!empty($dataAccount)){
                        $newAccount->balance=$dataAccount->balance+=$count;
                    }else{
                        $newAccount->balance=$count;
                    }
                    $newAccount->income_list=$name;
                    $newAccount->income=$count;
                }elseif($type=='Expenses'){
                    if(!empty($dataAccount)){
                        $newAccount->balance=$dataAccount->balance-=$count;
                    }else{
                        $newAccount->balance=$count*-1;
                    }
                    $newAccount->expenses_list=$name;
                    $newAccount->expenses=$count;
                }
                $newAccount->project_id=$project_id;
                $newAccount->payee=$payee;
                $newAccount->remark=$remark;
                if($newAccount->save()){
                    return Redirect::to('/account/manage');
                }
            }
        }
    }

    public function repairRemove(){
        $id = Input::get('id', 0);
        $account = Models\RepairAccounts::find($id);
        $Store=new Models\Stores();
        if($account->repair_accounts_type='repair') {
            $Store->updateStore($account->product_id, 1, 1, 'plus');
        }
        if(!empty($account))
            $account->delete();

        return Redirect::to('/account/repair');
    }

    public function repairWaste(){
        $id = Input::get('id', 0);
        $account = Models\RepairAccounts::find($id);
        $account->repair_accounts_type='waste';
        $dataStore=Models\Stores::where('product_id', '=', $account->product_id)->where('storename_id', '=',2)->first();
        if(!empty($dataStore))
            $account->repair_accounts_balance =$dataStore->balance_product;

        if(!empty($account))
            $account->save();

        return Redirect::to('/account/repair');
    }

    public function repairReturn(){
        $id = Input::get('id', 0);
        $account = Models\RepairAccounts::find($id);
        $account->repair_accounts_type='return';
        $Store=new Models\Stores();
        $Store->updateStore($account->product_id,2,1,'plus');
        $dataStore=Models\Stores::where('product_id', '=', $account->product_id)->where('storename_id', '=',2)->first();
        if(!empty($dataStore))
            $account->repair_accounts_balance =$dataStore->balance_product;

        if(!empty($account))
            $account->save();

        return Redirect::to('/account/repair');
    }

    public function rentalsReturn(){
        $id = Input::get('id', 0);
        $account = Models\ProductRentals::find($id);
        $account->product_rentals_type='return';
        $Store=new Models\Stores();
        $Store->updateStore($account->product_id,$account->storename_id,$account->product_rentals_count,'plus');
        $dataStore=Models\Stores::where('product_id', '=', $account->product_id)->where('storename_id', '=',$account->storename_id)->first();
        if(!empty($dataStore))
            $account->product_rentals_balance =$dataStore->balance_product;

        if(!empty($account))
            $account->save();

        return Redirect::to('/account/rentals');
    }

    public function rentalsDilapidated(){
        $id = Input::get('id', 0);
        $Store=new Models\Stores();
        $account = Models\ProductRentals::find($id);
        $account->product_rentals_type='dilapidated';
//        $Store->updateStore($account->product_id,$account->storename_id,$account->product_rentals_count,'minus');
        $Store->updateStore($account->product_id,1,$account->product_rentals_count,'plus');
        if(!empty($account))
            $account->save();

        return Redirect::to('/account/rentals');
    }

    public function rentalsRemove(){
        $id = Input::get('id', 0);
        $account = Models\ProductRentals::find($id);
        $Store=new Models\Stores();

        if($account->product_rentals_type=='borrow')
            $Store->updateStore($account->product_id,$account->storename_id,$account->product_rentals_count,'plus');

        if(!empty($account)){
            $account->delete();
        }
        return Redirect::to('/account/rentals');
    }

    public function productRemove(){
        $id = Input::get('id', 0);
        $account = Models\ProductsAccount::find($id);
        $Store=new Models\Stores();
        if(!empty($account->income_product_id)){
            $Store->updateStore($account->product_id,$account->storename_id,$account->income,'minus');
            $account->balance=$account->balance-$account->income;
        }elseif(!empty($account->expenses_product_id)){
            $Store->updateStore($account->product_id,$account->storename_id,$account->expenses,'plus');
            $account->balance=$account->balance+$account->expenses;
        }
        if(!empty($account)){
            if($account->save()) {
                $account->delete();
            }
        }
        return Redirect::to('/account/product');
    }

    public function remove(){
        $id = Input::get('id', 0);
        $account = Models\InventoryAccount::find($id);
        $dataAccount=Models\InventoryAccount::orderBy('created_at', 'DESC')->first();
        if(!empty($account->income_list)){
            $dataAccount->balance=$dataAccount->balance-=$account->income;
        }elseif(!empty($account->expenses_list)){
            $dataAccount->balance=$dataAccount->balance+=$account->expenses;
        }
        $dataAccount->save();
        if(!empty($account)){
            $account->delete();
        }
        return Redirect::to('/account/manage');
    }

    public function search(){
        $month = Input::get('month', 0);
        $year = Input::get('year', 0);
        $account=new Models\InventoryAccount();
        $datetime =Carbon::now();
        $user = Session::get('user');
        if(!empty($month)){
            $selectMonth=$month;
        }else{
            $selectMonth=intval($datetime->format('m'));
        }
        if(!empty($year)){
            $selectYear=$year;
        }else{
            $selectYear=intval($datetime->format('Y'));
        }
        $accounts=new Models\InventoryAccount();
        $dataAccounts=$account->getByMonthAndYear($selectMonth,$selectYear);
        $dataStaffs=Models\Staffs::all();
        $sumIncome=$account->GetSumReceipts();
        $sumExpenses=$account->GetSumExpenses();
        $project=Models\Project::orderBy("created_at","DESC")->get();
        $staff=Models\Staffs::where('staff_usertype', '!=', 1)->where('staff_usertype', '!=',2)->where('staff_usertype', '!=',8)->get();
        $monthNames = array("","มกราคม","กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
        return view('account.manage', array('user'=>$user,'staff'=>$staff,'project'=>$project,'sumExpenses'=>$sumExpenses,'sumIncome'=>$sumIncome,'selectYear'=>$selectYear,'selectMonth'=>$selectMonth,'monthNames'=>$monthNames,'accounts'=>$accounts,'dataAccounts'=>$dataAccounts,'dataStaffs'=>$dataStaffs));
    }

    public function manage()
    {
        $id = Input::get('id', 0);
        $user = Session::get('user');
        $account=new Models\InventoryAccount();
        $datetime =Carbon::now();
        $selectMonth=intval($datetime->format('m'));
        $selectYear=intval($datetime->format('Y'));
        $accounts=new Models\InventoryAccount();
        $accounts->payee=$user->staff_username;
        $dataAccounts=$account->getByMonthAndYear($selectMonth,$selectYear);
        $dataStaffs=Models\Staffs::all();
        $sumIncome=$account->GetSumReceipts();
        $sumExpenses=$account->GetSumExpenses();
        if(!empty($id)){
            $accounts=$accounts::find($id);
        }
        $staff=Models\Staffs::where('staff_usertype', '!=', 1)->where('staff_usertype', '!=',2)->where('staff_usertype', '!=',8)->get();
        $project=Models\Project::orderBy("created_at","DESC")->get();
        $monthNames = array("","มกราคม","กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
        return view('account.manage', array('user'=>$user,'staff'=>$staff,'project'=>$project,'sumExpenses'=>$sumExpenses,'sumIncome'=>$sumIncome,'selectYear'=>$selectYear,'selectMonth'=>$selectMonth,'monthNames'=>$monthNames,'accounts'=>$accounts,'dataAccounts'=>$dataAccounts,'dataStaffs'=>$dataStaffs));
    }

    public function searchProduct(){
        $product_id = Input::get('product_id', null);
        $project_id = Input::get('project_id', null);
        $selectMonth = Input::get('month', 0);
        $selectYear = Input::get('year', 0);
        $datetime =Carbon::now();
//        if(!empty($month)){
//            $selectMonth=$month;
//        }else{
//            $selectMonth=intval($datetime->format('m'));
//        }
//        if(!empty($year)){
//            $selectYear=$year;
//        }else{
//            $selectYear=intval($datetime->format('Y'));
//        }
        $accounts=new Models\ProductsAccount();
        $dataAccounts=$accounts->getByMonthAndYear($selectYear,$selectMonth,$product_id,$project_id);
        $staff=Models\Staffs::where('staff_usertype', '!=', 1)->where('staff_usertype', '!=',2)->where('staff_usertype', '!=',8)->get();
        $project=Models\Project::orderBy("created_at","DESC")->get();
        $storeName=Models\Storename::all();
        $monthNames = array("","มกราคม","กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
        $user = Session::get('user');
        $product=Models\Product::all();
        return view('account.product', array('product_id'=>$product_id,'product'=>$product,'user'=>$user,'dataAccounts'=>$dataAccounts,'storeName'=>$storeName,'project'=>$project,'staff'=>$staff,'accounts'=>$accounts,'monthNames'=>$monthNames,'selectMonth'=>$selectMonth,'selectYear'=>$selectYear));
    }


    public function product(){
        $id = Input::get('id', 0);
        $datetime =Carbon::now();
        $selectMonth=intval($datetime->format('m'));
        $selectYear=intval($datetime->format('Y'));
        $accounts=new Models\ProductsAccount();
        if(!empty($id)){
            $accounts=$accounts::find($id);
        }
        $dataAccounts=$accounts->getByMonthAndYear($selectYear,$selectMonth);
        $staff=Models\Staffs::where('staff_usertype', '!=', 1)->where('staff_usertype', '!=',2)->where('staff_usertype', '!=',8)->get();
        $project=Models\Project::orderBy("created_at","DESC")->get();
        $storeName=Models\Storename::all();
        $product=Models\Product::all();
        $monthNames = array("","มกราคม","กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
        $user = Session::get('user');
        return view('account.product', array('product'=>$product,'user'=>$user,'dataAccounts'=>$dataAccounts,'storeName'=>$storeName,'project'=>$project,'staff'=>$staff,'accounts'=>$accounts,'monthNames'=>$monthNames,'selectMonth'=>$selectMonth,'selectYear'=>$selectYear));
    }

    public function addProductPrice(){
        if(!empty($_POST)) {
            foreach($_POST['price'] as $key=>$value){
                $ProductsAccounts=Models\ProductsAccount::find($key);
                if(!empty($ProductsAccounts)){
                    $ProductsAccounts->price=$value;
                    $ProductsAccounts->save();
                }
            }
        }
        return Redirect::to('/account/product');
    }

    public function addProduct(){
        if(!empty($_POST)) {
            $id = Input::get('id', 0);
            $name = Input::get('name', '');
            $count = Input::get('count', 0);
            $storeName = Input::get('storeName', 0);
            $type = Input::get('type', '');
            $payee = Input::get('payee', '');
            $remark = Input::get('remark', "");
            $project_id = Input::get('project_id', '');
            //set rules
            $rules = array(
                'name' => 'required',
                'type' => 'required',
                'payee' => 'required',
                'project_id' => 'required',
                'count' => 'required|numeric',
                'storeName'=>'required',
            );
            //validate input
            $validator = Validator::make(Input::all(), $rules);
            //check validate
            if ($validator->fails()) {
                // false Redirect
                $messages = $validator->messages();
                return Redirect::to('/account/product')
                    ->withErrors($validator)
                    ->withInput(Input::except('name', 'type', 'payee', 'remark', 'count'));
            } else {
                $name=explode('::',$name);
                $product=Models\Product::where('product_name', '=', $name[0])->first();
                if(!empty($product)) {
                    $Stores=new Models\Stores();
                    $newAccount = new Models\ProductsAccount();
                    $newAccount->project_id = $project_id;
                    $newAccount->payee = $payee;
                    $newAccount->remark = $remark;
                    $newAccount->product_id = $product->product_id;
                    $newAccount->storename_id=$storeName;
                    if ($type == 'Income') {
                        $newAccount->income_product_id = $name[0];
                        $newAccount->income = $count;
                        $Stores->updateStore($product->product_id,$storeName,$count,'plus');
                        $dataStore=Models\Stores::where('product_id', '=', $product->product_id)->where('storename_id', '=',$storeName)->first();
                        if(!empty($dataStore)){
                            $newAccount->balance=$dataStore->balance_product;
                        }
                    } elseif ($type == 'Bring') {
                        $newAccount->expenses_product_id = $name[0];
                        $newAccount->expenses = $count;
                        $Stores->updateStore($product->product_id,$storeName,$count,'minus');
                        $dataStore=Models\Stores::where('product_id', '=', $product->product_id)->where('storename_id', '=',$storeName)->first();
                        if(!empty($dataStore)){
                            $newAccount->balance=$dataStore->balance_product;
                        }
                    }
                    if ($newAccount->save()) {
                        return Redirect::to('/account/product');
                    }
                }else{
                    return Redirect::to('/account/product');
                }
            }
        }
    }

    public function checkManufacture(){
        $product=array();
        foreach(Models\Product::where('product_type', '=', 'manufacture')->get() as $key=>$value){
            $results=Models\ProductUnit::find($value->product_unit);
            array_push($product,$value->product_name.'::'.$results->unit_name);
        }
        return json_encode($product);
    }

    public function checkProduct(){
        $product=array();

        foreach(Models\Product::all() as $key=>$value){
            $results=Models\ProductUnit::find($value->product_unit);
            array_push($product,$value->product_name.'::'.$results->unit_name);
        }
        return json_encode($product);
    }

    public function checkEquipment(){
        $product=array();
        foreach(Models\Product::where('product_type', '=', 'equipment')->get() as $key=>$value){
            $results=Models\ProductUnit::find($value->product_unit);
            array_push($product,$value->product_name.'::'.$results->unit_name);
        }
        return json_encode($product);
    }

    public function checkRepair(){
        $product=array();
        foreach(Models\Stores::where('storename_id','=',1)->where('balance_product','>=',1)->get() as $key=>$value){
            $dataProduct=Models\Product::find($value->product_id);
            $results=Models\ProductUnit::find($dataProduct->product_unit);
            array_push($product,$dataProduct->product_name.'::'.$results->unit_name);
        }
        return json_encode($product);
    }

    public function addRentals(){
        if(!empty($_POST)) {
            $project_id = Input::get('project_id', 0);
            $name = Input::get('name', '');
            $count = Input::get('count', 0);
            $storeName = Input::get('storeName', 0);
            $type = Input::get('type', '');
            $payee = Input::get('payee', '');
            $remark = Input::get('remark', "");
            $project_id = Input::get('project_id', '');
            //set rules
            $rules = array(
                'name' => 'required',
                'payee' => 'required',
                'count' => 'required|numeric',
                'storeName'=>'required',
            );
            //validate input
            $validator = Validator::make(Input::all(), $rules);
            //check validate
            if ($validator->fails()) {
                // false Redirect
                $messages = $validator->messages();
                return Redirect::to('/account/rentals')
                    ->withErrors($validator)
                    ->withInput(Input::except('name', 'type', 'payee', 'remark', 'count'));
            } else {
                $name=explode('::',$name);
                $product=Models\Product::where('product_name', '=', $name[0])->first();
                if(!empty($product)) {
                    $Stores=new Models\Stores();
                    $newAccount = new Models\ProductRentals();

                    if(!empty($project_id))
                    $newAccount->project_id = $project_id;

                    $newAccount->product_name=$name[0];
                    $newAccount->payee = $payee;
                    $newAccount->remark = $remark;
                    $newAccount->product_id = $product->product_id;
                    $newAccount->storename_id=$storeName;
                    $newAccount->product_rentals_count = $count;
                    $Stores->updateStore($product->product_id,$storeName,$count,'minus');
                    $dataStore=Models\Stores::where('product_id', '=', $product->product_id)->where('storename_id', '=',$storeName)->first();
                    if(!empty($dataStore)){
                       $newAccount->product_rentals_balance=$dataStore->balance_product;
                    }
                    if ($newAccount->save()) {
                        return Redirect::to('/account/rentals');
                    }
                }else{
                    return Redirect::to('/account/rentals');
                }
            }
        }
    }

    public function rentals(){
        $id = Input::get('id', 0);
        $datetime =Carbon::now();
        $month = Input::get('month', 0);
        $year = Input::get('year', 0);
        if(!empty($month)and !empty($year)){
            $selectMonth = $month;
            $selectYear = $year;
        }else {
            $selectMonth = intval($datetime->format('m'));
            $selectYear = intval($datetime->format('Y'));
        }
        $accounts=new Models\ProductRentals();
        if(!empty($id)){
            $accounts=$accounts::find($id);
        }
        $dataAccounts=$accounts->getByMonthAndYear($selectMonth,$selectYear);
        $staff=Models\Staffs::where('staff_usertype', '!=', 1)->where('staff_usertype', '!=',2)->where('staff_usertype', '!=',8)->get();
        $project=Models\Project::orderBy("created_at","DESC")->get();
        $storeName=Models\Storename::all();
        $monthNames = array("","มกราคม","กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
        $user = Session::get('user');
        return view('account.rentals', array('user'=>$user,'dataAccounts'=>$dataAccounts,'storeName'=>$storeName,'project'=>$project,'staff'=>$staff,'accounts'=>$accounts,'monthNames'=>$monthNames,'selectMonth'=>$selectMonth,'selectYear'=>$selectYear));
    }

    public function allReturn(){
        if(!empty($_POST)) {
            foreach($_POST['rentals'] as $key=>$value){
                $account = Models\ProductRentals::find($value);
                $account->product_rentals_type='return';
                $Store=new Models\Stores();
                $Store->updateStore($account->product_id,$account->storename_id,$account->product_rentals_count,'plus');
                $dataStore=Models\Stores::where('product_id', '=', $account->product_id)->where('storename_id', '=',$account->storename_id)->first();
                if(!empty($dataStore))
                    $account->product_rentals_balance =$dataStore->balance_product;

                if(!empty($account))
                    $account->save();
            }
        }
        return Redirect::to('/account/rentals');
    }

    public function repair(){
        $id = Input::get('id', 0);
        $datetime =Carbon::now();
        $selectMonth=intval($datetime->format('m'));
        $selectYear=intval($datetime->format('Y'));
        $accounts=new Models\RepairAccounts();
        if(!empty($id)){
            $accounts=$accounts::find($id);
        }
        $dataAccounts=$accounts->getByMonthAndYear($selectMonth,$selectYear);
        $staff=Models\Staffs::where('staff_usertype', '!=', 1)->where('staff_usertype', '!=',2)->where('staff_usertype', '!=',8)->get();
        $project=Models\Project::orderBy("created_at","DESC")->get();
        $storeName=Models\Storename::all();
        $monthNames = array("","มกราคม","กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
        return view('account.repair', array('dataAccounts'=>$dataAccounts,'storeName'=>$storeName,'project'=>$project,'staff'=>$staff,'accounts'=>$accounts,'monthNames'=>$monthNames,'selectMonth'=>$selectMonth,'selectYear'=>$selectYear));
    }

    public function addRepair(){
        if(!empty($_POST)) {
            $id = Input::get('id', 0);
            $name = Input::get('name', '');
            $count = Input::get('count', 0);
            $storeName = Input::get('storeName', 0);
            $type = Input::get('type', '');
            $payee = Input::get('payee', '');
            $remark = Input::get('remark', "");
            $project_id = Input::get('project_id', '');
            $symptom=Input::get('symptom', '');
            //set rules
            $rules = array(
                'name' => 'required',
                'payee' => 'required',
                'count' => 'required|numeric',
//                'storeName'=>'required',
                'symptom'=>'required',
            );
            //validate input
            $validator = Validator::make(Input::all(), $rules);
            //check validate
            if ($validator->fails()) {
                // false Redirect
                $messages = $validator->messages();
                return Redirect::to('/account/repair')
                    ->withErrors($validator)
                    ->withInput(Input::except('name', 'type', 'payee', 'remark', 'count','symptom'));
            } else {
                $name=explode('::',$name);
                $product=Models\Product::where('product_name', '=', $name[0])->first();
                if(!empty($product)) {
                    $Stores=new Models\Stores();
                    $newAccount = new Models\RepairAccounts();
                    $newAccount->repair_accounts_name=$name[0];
                    $newAccount->payee = $payee;
                    $newAccount->remark = $remark;
                    $newAccount->product_id = $product->product_id;
                    $newAccount->storename_id=1;
                    $newAccount->repair_accounts_symptom=$symptom;
                    $newAccount->repair_accounts_price=$count;
                    $newAccount->repair_accounts_type='repair';
                    $Stores->updateStore($product->product_id,1,1,'minus');
                    $dataStore=Models\Stores::where('product_id', '=', $product->product_id)->where('storename_id', '=',1)->first();
                    if(!empty($dataStore))
                        $newAccount->repair_accounts_balance=$dataStore->balance_product;

                    if ($newAccount->save())
                        return Redirect::to('/account/repair');
                }else{
                    return Redirect::to('/account/repair');
                }
            }
        }
    }

    public function allRepair(){
        if(!empty($_POST)) {
            foreach($_POST['rentals'] as $key=>$value){
                $account = Models\RepairAccounts::find($value);
                $account->repair_accounts_type='return';
                $Store=new Models\Stores();
                $Store->updateStore($account->product_id,2,1,'plus');
                $dataStore=Models\Stores::where('product_id', '=', $account->product_id)->where('storename_id', '=',2)->first();
                if(!empty($dataStore))
                    $account->repair_accounts_balance =$dataStore->balance_product;

                if(!empty($account))
                    $account->save();
            }
        }
        return Redirect::to('/account/repair');
    }

    public function listProductsAccounts(){
        $id=Input::get('id',0);
        $dataProductsAccount=Models\ProductsAccount::where('product_id', '=',$id)->get();
        $count=0;
        $sum=0;
        $string = "
        <table class='table table-bordered'>
          <thead>
            <tr>
              <th>#</th>
              <th>ชื่อสินค้า</th>
              <th style='text-align: right'>จำนวน</th>
              <th style='text-align: right'>ราคา</th>
              <th style='text-align: right'>ต่อหน่วย</th>
            </tr>
          </thead>
          <tbody>";
            foreach ($dataProductsAccount as $key => $value) {
                if (!empty($value->income)) {
                    if (!empty($value->price)) {
                        $count += $value->income;
                        $sum += $value->price;
                    }
                    $string .= "<tr>
                              <td>" . $value->created_at . "</td>
                              <td>" . Models\Product::GetNameProduct($value->product_id) . "</td>
                              <td style='text-align: right'>" . $value->income . "</td>
                              <td style='text-align: right'>" . (!empty($value->price) ? $value->price : '0') . "</td>
                              <td style='text-align: right'>" . (!empty($value->price) ? ($value->price / $value->income) : '0') . "</td>
                            </tr>";
                }
            }
        if($count!=0 and $sum!=0 ) {
            $string .= "<tr><td colspan='2' style='font-weight: bolder;'>รวม</td><td style='text-align: right;font-weight: bolder;'>" . $count . "</td><td style='text-align: right;font-weight: bolder;'>" . $sum . "</td><td style='text-align: right;font-weight: bolder;'>" . ($sum / $count) . "</td></tr>";
        }
        $string .= "</tbody></table>";
        echo $string;
    }

    public function employeesDaily(){
        $datetime = Carbon::now();
        $datetimeEnd = Carbon::now();
        $selectMonth = Input::get('month', 0);
        $selectYear = Input::get('year', 0);
        $selectDay= intval($datetime->format('d'));
        $monthNow=$datetime->format('m');
        if(empty($selectMonth)&&empty($selectYear)) {
            $selectMonth = intval($datetime->format('m'));
            $selectYear = intval($datetime->format('Y'));
        }
        $datetime->setDate($selectYear,$selectMonth,1);
        $datetimeEnd->setDate($selectYear,$selectMonth,intval($datetime->format('t')));
        $countDay=intval($datetime->format('t'));
        $countDay+=intval($datetime->format('w'));
        $countDay+=(7-intval($datetimeEnd->format('w')));
        $datetime->modify('-'.intval($datetime->format('w')).' day');
        $monthNames = array("","มกราคม","กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
        $selectHour=array('00'=>'00','01'=>'01','02'=>'02','03'=>'03','04'=>'04','05'=>'05','06'=>'06','07'=>'07','08'=>'08','09'=>'09','10'=>'10','11'=>'11','12'=>'12','13'=>'13','14'=>'14','15'=>'15','16'=>'16','17'=>'17','18'=>'18','19'=>'19','20'=>'20','21'=>'21','22'=>'22','23'=>'23');
        $selectMinute=array('00'=>'00','01'=>'01','02'=>'02','03'=>'03','04'=>'04','05'=>'05','06'=>'06','07'=>'07','08'=>'08','09'=>'09','10'=>'10','11'=>'11','12'=>'12','13'=>'13','14'=>'14','15'=>'15','16'=>'16','17'=>'17','18'=>'18','19'=>'19','20'=>'20','20'=>'20','21'=>'21','22'=>'22','23'=>'23','24'=>'24','25'=>'25','26'=>'26','27'=>'27','28'=>'28','29'=>'29','30'=>'30','31'=>'31','32'=>'32','33'=>'33','34'=>'34','35'=>'35','36'=>'36','37'=>'37','38'=>'38','38'=>'39','40'=>'40','41'=>'41','42'=>'42','43'=>'43','44'=>'44','45'=>'45','46'=>'46','47'=>'47','48'=>'48','49'=>'49','50'=>'50','51'=>'51','52'=>'52','53'=>'53','54'=>'54','55'=>'55','56'=>'56','57'=>'57','58'=>'58','59'=>'59');
        $project=Models\Project::orderBy("created_at","DESC")->get();
        $EmployeesDaily=new Models\EmployeesDaily();
        $dataEmployeesDaily=$EmployeesDaily->GetCalendar($selectMonth,$selectYear);
        return view('account.employeesDaily', array('monthNow'=>$monthNow,'dataEmployeesDaily'=>$dataEmployeesDaily,'project'=>$project,'selectMinute'=>$selectMinute,'selectHour'=>$selectHour,'selectDay'=>$selectDay,'countDay'=>$countDay,'datetime'=>$datetime,'monthNames'=>$monthNames,'selectMonth'=>$selectMonth,'selectYear'=>$selectYear));
    }

    public function removeDaily(){
        $id = Input::get('id', 0);
        $daily = Models\EmployeesDaily::find($id);
        if(!empty($daily)){
            $daily->delete();
        }
        return Redirect::to('/account/employeesDaily');
    }

    public function addDaily(){
        if(!empty($_POST)) {
//            $id = Input::get('id', 0);
            $project_id= Input::get('project_id', 0);
            $day = Input::get('day', 0);
            $hour = Input::get('hour', '');
            $minute = Input::get('minute', 0);
            $man = Input::get('man', 0);
            $lady = Input::get('lady', '');
            $detail = Input::get('detail', '');
            //set rules
            $rules = array(
                'day' => 'required',
                'hour' => 'required',
                'minute' => 'required',
                'detail'=>'required',
            );
            //validate input
            $validator = Validator::make(Input::all(), $rules);
            //check validate
            if ($validator->fails()) {
                // false Redirect
                $messages = $validator->messages();
                return Redirect::to('/account/employeesDaily')
                    ->withErrors($validator)
                    ->withInput(Input::except('project_id','day', 'hour', 'minute', 'detail'));
            } else {
                $datetime = Carbon::now();
                $datetime->setDateTime(intval($datetime->format('Y')),intval($datetime->format('m')),$day,$hour,$minute);
                $user = Session::get('user');
                $newdaily=new Models\EmployeesDaily();
                if(!empty($project_id)) {
                    $newdaily->project_id = $project_id;
                }
                $newdaily->employees_daily_record=$user->staff_username;
                $newdaily->employees_daily_man=$man;
                $newdaily->employees_daily_price_man=0;
                $newdaily->employees_daily_lady=$lady;
                $newdaily->employees_daily_price_lady=$lady;
                $newdaily->employees_daily_work=$datetime->format("Y-m-d H:i:s");
                $newdaily->employees_daily_detail=$detail;
                if ($newdaily->save())
                    return Redirect::to('/account/employeesDaily');
            }
        }
    }

}