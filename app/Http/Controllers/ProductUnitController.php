<?php namespace App\Http\Controllers;
/**
 * Created by PhpStorm.
 * User: sootipong
 * Date: 5/18/2015
 * Time: 1:11 AM
 */

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models;
use Illuminate\Support\Facades\Response;
use Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class ProductUnitController extends Controller
{
    public function deleteById()
    {
        //1. Get input  and declare variable
        $id=Input::get('id',0);
        $result = array();
        //2. Process
        $deleteProductUnit=new Models\ProductUnit();
        $result=$deleteProductUnit->deleteProductUnit($id);
        //3. Redirect
        if('error'==$result['status']){
            return Redirect::to('/productUnit/manage')->with('error', 'ไม่สามารถ ลบข้อมูลหน่วยสินค้า สำเร็จ');
        } else {
            return Redirect::to('/productUnit/manage')->with($result['status'],$result['desc']);
        }
    }

    public function search(){
        //1. Get input  and declare variable
        $search=Input::get('search','');
        $page=Input::get('page',1);
        $dataProductUnit=new Models\ProductUnit();
        $countRow=1;
        //2. Process
        $productUnit = $dataProductUnit->findAllProductUnitAndPaginate($countRow,$page,$search);
        //3. Return to view
        return view('productUnit.manage', array('productUnit' => $productUnit,'page'=>$page,'search'=>$search));
    }

    public function displayById()
    {
        //1. Get input  and declare variable
        $id=Input::get('id',0);
        $page=Input::get('page',1);
        $search=Input::get('search','');
        $editProductUnit=new Models\ProductUnit();
        $countRow=30;
        //2. Process
        if (false==empty($id)) {
            $dataProductUnit =$editProductUnit->findByIdProductUnit($id);
        }
        $productUnit = $dataProductUnit->findAllProductUnitAndPaginate($countRow,$page,$search);
        //3. Return to view
        return view('productUnit.manage', array('productUnit' => $productUnit,'dataProductUnit' => $dataProductUnit,'page'=>$page,'search'=>$search));
    }

    public function openManage()
    {
        //1. Get input  and declare variable
        $page=Input::get('page',1);
        $search=Input::get('search','');
        $dataProductUnit=new Models\ProductUnit();
        $countRow=30;
        //2. Process
        $productUnit = $dataProductUnit->findAllProductUnitAndPaginate($countRow,$page,$search);
        //3. Return to view
        return view('productUnit.manage', array('productUnit' => $productUnit,'page'=>$page,'search'=>$search));
    }

    public function addProductUnit()
    {
        //set rules
        $rules = array(
            'unit_name' => 'required|unique:ref_product_units',
        );
        //validate input
        $validator = Validator::make(Input::all(), $rules);
        //check validate
        if ($validator->fails()) {
            // false Redirect
            $messages = $validator->messages();
            return Redirect::to('/productUnit/manage')
                ->withErrors($validator)
                ->withInput(Input::except('unit_name'));
        } else {
            //true save data ProductUnit
            $newProductUnit = new Models\ProductUnit();
            $newProductUnit->unit_name = Input::get('unit_name');
            $newProductUnit->save();
            //Redirect
            return Redirect::to('/productUnit/manage');
        }
    }

    public function editProductUnit()
    {
        //Get input  and declare variable
        $page=Input::get('page',1);
        $search=Input::get('search','');
        $id=Input::get('id');
        $editProductUnit=new Models\ProductUnit();
        if (!empty(Input::get('id'))) {
            //set rules
            $rules = array(
                'unit_name' => 'required|unique:ref_product_units',
            );
            //validate input
            $validator = Validator::make(Input::all(), $rules);
            //check validate
            if ($validator->fails()) {
                // false Redirect
                $messages = $validator->messages();
                if(false==empty($page)&&false==empty($search)) {
                    return Redirect::to('/productsUnit/manage?id=' . $id . '&page=' . $page . '&search=' . $search)
                        ->withErrors($validator)
                        ->withInput(Input::except('unit_name'));
                }elseif(false==empty($page)) {
                    return Redirect::to('/productsUnit/manage?id=' . $id.'&page=' . $page)
                        ->withErrors($validator)
                        ->withInput(Input::except('unit_name'));
                }elseif(false==empty($search)) {
                    return Redirect::to('/productsUnit/manage?id=' . $id.'&search=' . $search)
                        ->withErrors($validator)
                        ->withInput(Input::except('unit_name'));
                }else {
                    return Redirect::to('/productsUnit/manage?id=' . Input::get('id'))
                        ->withErrors($validator)
                        ->withInput(Input::except('unit_name'));
                }
            } else {
                //true save data Product
                $newProductUnit = $editProductUnit->findByIdProductUnit($id);
                if(false==empty($newProductUnit)){
                    //set data
                    $newProductUnit->unit_name = Input::get('unit_name');
                    //save
                    $newProductUnit->save();
                    //Redirect
                    if(false==empty($page)&&false==empty($search)) {
                        return Redirect::to('/productUnit/manage?page=' . $page . '&search=' . $search);
                    }elseif(false==empty($page)) {
                        return Redirect::to('/productUnit/manage?page=' . $page);
                    }elseif(false==empty($search)) {
                        return Redirect::to('/productUnit/manage?search=' . $search);
                    }else {
                        return Redirect::to('/productUnit/manage');
                    }
                }else{
                    //Redirect
                    return Redirect::to('/productUnit/manage')->with('error', 'ไม่สามารถ แก้ไขข้อมูลหน่วยสินค้าได้');
                }
            }
        }
    }
}
?>