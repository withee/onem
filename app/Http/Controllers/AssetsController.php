<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Request;
use App\Models;
use Illuminate\Support\Facades\Validator;

class AssetsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    private $rules = array(
        'brands' => 'required',
        'series' => 'required',
        'color' => 'required',
        'license_plate' => 'required',
        'engine_number' => 'required',
        'represent_color' => 'required'
    );

    public function openManage()
    {
        //1. Variable declaration
        //$type = new Models\UserRole();
        //2. Process
        $paging = Models\Vehicle::paginate(10);
        $data = array('paging' => $paging);
        //3. Return
        return view('assets.vehicles', $data);
    }

    public function addVehicle()
    {
        //1. Variable declaration
        $data = Request::all();
        $result = array('success' => true, 'desc' => 'Operation Success!!', 'data' => array());
        //2. Validate
        $validator = Validator::make($data, $this->rules);
        if (true == $validator->fails()) {
            $messages = $validator->messages();
            $result['success'] = false;
            $result['desc'] = 'validate fail';
            $result['data'] = $messages;

        } else {
            //3. Process
            try {
                //save in model;
                $vehicle = new Models\Vehicle();
                $vehicle->brands = $data['brands'];
                $vehicle->series = $data['series'];
                $vehicle->color = $data['color'];
                $vehicle->license_plate = $data['license_plate'];
                $vehicle->serial = $data['engine_number'];
                $vehicle->represent_color = $data['represent_color'];
                $tmp_rs = $vehicle->saveModel($vehicle);
                if (false == $tmp_rs) {
                    $result['success'] = false;
                    $result['desc'] = "Model error.";
                }

            } catch (\Exception $e) {
                $result['success'] = true;
                $msg = $e->getFile() . '|' . $e->getLine() . ':' . $e->getMessage();
                $result['desc'] = $msg;
                \Log::error($msg);
            } finally {

            }
        }
        //4. Return
        return json_encode($result);
    }

    public function getByID()
    {
        //1. Variable declaration
        $id = Request::input('id');
        $result = array('success' => true, 'desc' => 'Operation Success!!', 'data' => array());

        //2. Process
        try {
            $vehicles = new Models\Vehicle();
            $vehicle = $vehicles->getByID($id);
            if (true == empty($vehicle)) {
                $result['success'] = false;
                $result['desc'] = 'Model not found.';
            } else {
                $result['data'] = $vehicle;
            }
        } catch (\Exception $e) {
            $result['success'] = false;
            $msg = $e->getFile() . '|' . $e->getLine() . ':' . $e->getMessage();
            $result['desc'] = $msg;
            \Log::error($msg);
        } finally {

        }

        //3. Return
        return json_encode($result);
    }

    public function editVehicle()
    {
        //1. Variable declaration
        $data = Request::all();
        $result = array('success' => true, 'desc' => 'Operation Success!!', 'data' => array());
        //2. Validate
        $validator = Validator::make($data, $this->rules);
        if (true == $validator->fails()) {
            $messages = $validator->messages();
            $result['success'] = false;
            $result['desc'] = 'validate fail';
            $result['data'] = $messages;

        } else {
            //3. Process
            try {

                $vehicles = new Models\Vehicle();
                $vehicle = $vehicles->getByID($data['vehicle_id']);
                if (true == empty($vehicle)) {
                    $result['success'] = false;
                    $result['desc'] = 'Model not found.';
                } else {
                    $vehicle->brands = $data['brands'];
                    $vehicle->series = $data['series'];
                    $vehicle->color = $data['color'];
                    $vehicle->license_plate = $data['license_plate'];
                    $vehicle->serial = $data['engine_number'];
                    $vehicle->represent_color = $data['represent_color'];
                    $tmp_rs = $vehicle->saveModel($vehicle);
                    if (false == $tmp_rs) {
                        $result['success'] = false;
                        $result['desc'] = "Model error.";
                    } else {
                        $result['data'] = $vehicle;
                    }
                }
            } catch (\Exception $e) {
                $result['success'] = true;
                $msg = $e->getFile() . '|' . $e->getLine() . ':' . $e->getMessage();
                $result['desc'] = $msg;
                \Log::error($msg);
            } finally {

            }
        }
        //4. Return
        return json_encode($result);
    }

    public function deleteVehicle()
    {
        //1. Variable declaration
        $id = Request::input('id');
        $result = array('success' => true, 'desc' => 'Operation Success!!', 'data' => array());

        //2. Process
        try {
            $vehicles = new Models\Vehicle();
            $vehicle = $vehicles->getByID($id);
            if (true == empty($vehicle)) {
                $result['success'] = false;
                $result['desc'] = 'Model not found.';
            } else {
                $vehicle->delete();
            }
        } catch (\Exception $e) {
            $result['success'] = false;
            $msg = $e->getFile() . '|' . $e->getLine() . ':' . $e->getMessage();
            $result['desc'] = $msg;
            \Log::error($msg);
        } finally {

        }

        //3. Return
        return json_encode($result);
    }


}
