<?php namespace App\Http\Controllers;

/**
 * Created by PhpStorm.
 * User: sootipong
 * Date: 5/26/2015
 * Time: 1:55 AM
 */
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models;
use Illuminate\Support\Facades\Response;
use Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class CompanyController extends Controller
{
    public function deleteById()
    {
        //1. Get input  and declare variable
        $id = Input::get('id', 0);
        $result = array();
        //2. Process
        $deleteCompany = new Models\Company();
        $result = $deleteCompany->deleteCompany($id);
        //3. Redirect
        if ('error' == $result['status']) {
            return Redirect::to('/companys/manage')->with('error', 'ไม่สามารถ ลบข้อมูลหน่วยสินค้า สำเร็จ');
        } else {
            return Redirect::to('/companys/manage')->with($result['status'], $result['desc']);
        }
    }

    public function search()
    {
        //1. Get input  and declare variable
        $search = Input::get('search', '');
        $page = Input::get('page', 1);
        $countRow = 30;
        //2. Process
        $dataCompany = new Models\Company();
        $company = $dataCompany->findAllCompanyPaginate($countRow, $page, $search);

        //3. Return to view
        return view('company.manage', array('company' => $company, 'page' => $page, 'search' => $search));
    }

    public function displayById()
    {
        //1. Get input  and declare variable
        $dataCompany = new Models\Company();
        $id = Input::get('id', 0);
        $page = Input::get('page', 1);
        $search = Input::get('search', '');
        $countRow = 30;
        //2. Process
        if (false == empty($id)) {
            $dataCompany = Models\Company::find(Input::get('id'));
        }
        $company = $dataCompany->findAllCompanyPaginate($countRow, $page, $search);
        //3. Return to view
        return view('company.manage', array('company' => $company, 'dataCompany' => $dataCompany, 'page' => $page, 'search' => $search));
    }

    public function openManage()
    {
        //1. Get input  and declare variable
        $page = Input::get('page', 1);
        $search = Input::get('search', '');
        $countRow = 30;
        //2. Process
        $dataCompany = new Models\Company();
        $company = $dataCompany->findAllCompanyPaginate($countRow, $page, $search);
        //3. Return to view
        return view('company.manage', array('company' => $company, 'page' => $page, 'search' => $search));
    }

    public function addCompany()
    {
        //set rules
        $rules = array(
            'company_name' => 'required|unique:companys',
        );
        //validate input
        $validator = Validator::make(Input::all(), $rules);
        //check validate
        if ($validator->fails()) {
            // false validator
            $messages = $validator->messages();
            //Redirect
            return Redirect::to('/companys/manage')
                ->withErrors($validator)
                ->withInput(Input::except('company_name'));
        } else {
            //true validator save data company
            $newCompany = new Models\Company();
            $newCompany->company_name = Input::get('company_name');
            $newCompany->location = Input::get('location');
            $newCompany->phone = Input::get('phone');
            $newCompany->contractor = Input::get('contractor');
            $newCompany->save();
            //Redirect
            return Redirect::to('/companys/manage');
        }
    }

    public function editCompany()
    {
        // Get input  and declare variable
        $page = Input::get('page', 1);
        $search = Input::get('search', '');
        $id = Input::get('id');
        $dataEditCompany = new Models\Company();
        if (!empty(Input::get('id'))) {
            //set rules
            $rules = array(
                'company_name' => 'required',
            );
            //check validate
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                // false validator
                $messages = $validator->messages();
                //Redirect
                if (false == empty($page) && false == empty($search)) {
                    return Redirect::to('/companys/manage?id=' . $id . '&page=' . $page . '&search=' . $search)
                        ->withErrors($validator)
                        ->withInput(Input::except('company_name'));
                } elseif (false == empty($page)) {
                    return Redirect::to('/companys/manage?id=' . $id . '&page=' . $page)
                        ->withErrors($validator)
                        ->withInput(Input::except('company_name'));
                } elseif (false == empty($search)) {
                    return Redirect::to('/companys/manage?id=' . $id . '&search=' . $search)
                        ->withErrors($validator)
                        ->withInput(Input::except('company_name'));
                } else {
                    return Redirect::to('/companys/manage?id=' . Input::get('id'))
                        ->withErrors($validator)
                        ->withInput(Input::except('company_name'));
                }
            } else {
                //true validator save data company
                $newCompany = $dataEditCompany->findByIdCompany($id);
                if (!empty($newCompany)) {
                    $newname = Input::get('company_name');
                    if ($newCompany->company_name != $newname) {
                        $newCompany->company_name = $newname;
                    }
                    $newCompany->location = Input::get('location');
                    $newCompany->phone = Input::get('phone');
                    $newCompany->contractor = Input::get('contractor');
                    $newCompany->save();
                    //Redirect
                    if (false == empty($page) && false == empty($search)) {
                        return Redirect::to('/companys/manage?page=' . $page . '&search=' . $search);
                    } elseif (false == empty($page)) {
                        return Redirect::to('/companys/manage?page=' . $page);
                    } elseif (false == empty($search)) {
                        return Redirect::to('/companys/manage?search=' . $search);
                    } else {
                        return Redirect::to('/companys/manage');
                    }
                } else {
                    //Redirect
                    return Redirect::to('/companys/manage')->with('error', 'ไม่สามารถ แก้ไขข้อมูลหน่วยสินค้าได้');
                }
            }


        }
    }

}

?>