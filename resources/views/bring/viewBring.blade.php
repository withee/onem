@extends('layouts.layouts')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h3>จัดการใบเบิกสินค้า</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-3" style="padding: 0px;margin: 0px;">
            <form style="float: left;" class="form-inline" method="GET" action="/bring/viewBring" novalidate>
                <div class="input-group input-group-sm">
                    <span class="input-group-addon" ><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></span>
                    <input type="text" class="form-control" name="search" value="{{$dataBring['search']}}" placeholder="ค้นหา หมายเลขใบสินค้า">
                </div>
                <button type="submit"  class="btn btn-sm"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
            </form>
        </div>
        <div class="col-lg-6"></div>
        <div class="col-lg-3" style="padding-bottom: 10px;">
            <button style="" type="button" class="btn btn-info" data-toggle="modal" data-target="#myModalGroupBring">
                เพิ่มกลุ่มใบเบิก
            </button>
            <button style="" type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                เพิ่มใบเบิก
            </button>
        </div>
    </div>

    <div class="row" style="float: right;">
        <div class="col-lg-12">
            <nav>
                <ul class="pagination" style="margin: 0px;padding: 0px;padding-top: 3px;">
                    @if($dataBring['page']>1)
                        <li><a href="/bring/viewBring?page={{$dataBring['page']-1}}@if(false==empty($dataBring['search']))&search={{$dataBring['search']}}@endif">Prev</a></li>
                    @endif

                    @for($x = 1; $x <= $dataBring['maxProduct']; $x++)
                        @if($x==$dataBring['page'])
                            <li class="active" ><a href="/bring/viewBring?page={{$x}}@if(false==empty($dataBring['search']))&search={{$dataBring['search']}}@endif">{{$x}}</a></li>
                        @else
                            <li ><a href="/bring/viewBring?page={{$x}}@if(false==empty($dataBring['search']))&search={{$dataBring['search']}}@endif">{{$x}}</a></li>
                        @endif
                    @endfor

                    @if($dataBring['page']<$dataBring['maxProduct'])
                        <li><a href="/bring/viewBring?page={{$dataBring['page']+1}}@if(false==empty($dataBring['search']))&search={{$dataBring['search']}}@endif">Next</a></li>
                    @endif
                </ul>
            </nav>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#
                    <th>วันที่</th>
                    <th>รหัสใบรับสินค้า</th>
                    <th>ผู้เบิก</th>
                    <th>รูปแบบใบเบิก</th>
                    <th>ประเภท</th>
                    <th>จัดการ</th>
                </tr>
                </thead>
                <tbody>
                @if(false==empty($dataBring))
                    @foreach($dataBring['dataProduct'] as $key=>$value)
                        <tr>
                            <th scope="row">{{$key+1}}</th>
                            <td>{{$value->bring_date}}</td>
                            <td>{{$value->bring_number}}</td>
                            <td>{{$value->staff_firstname}} {{$value->staff_lastname}}</td>
                            <td>
                                @if(false==empty($value->group_bring_id))
                                    มีกลุ่มใบเบิก
                                @elseif(false==empty($value->project_sub_id))
                                    ใช้ในโครงการ
                                @elseif(false==empty($value->control_build_log_id))
                                    เพื่อผลิต
                                @else
                                    ทั่วไป
                                @endif
                            </td>
                            <td>
                                @if($value->bring_type_manufacture=='Y')
                                    ใช้เพื่อผลิต
                                @endif
                                @if($value->bring_type_project=='Y')
                                    ,ใช้เพื่อโครงการ
                                @endif
                                @if($value->bring_type_other=='Y')
                                    ,อื่น
                                @endif
                            </td>
                            <td>
                                <a href="/bring/viewProduct?id={{$value->bring_id}}" class="btn btn-success" >เพิ่มสินค้า</a>
                                <a href="/bring/viewBring?id={{$value->bring_id}}" class="btn btn-warning" >เพิ่มข้อมูลใบเบิกสินค้า</a>
                                {{--<a onclick="return confirm('ยืนยันการลบข้อมูล สินค้านี้ ?')" href="/purchases/remove?id={{$key}}" class="btn btn-danger" >ลบ</a>--}}
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade @if(true==$errors->has('group_bring_name')) in @endif" style="@if(true==$errors->has('group_bring_name')) display: block; @endif" id="myModalGroupBring" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <form method="POST" action="/bring/addGroupBring" >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <a href="/bring/viewBring" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title" id="myModalLabel">สร้างกลุ่มใบเบิก</h4>
                </div>
                <div class="modal-body">
                    @if ($errors->has())
                        <div class="alert alert-danger" style="padding: 0px;">
                            @foreach ($errors->all() as $error)
                                {{ $error }}
                            @endforeach
                        </div>
                    @endif
                        <div class="form-horizontal">
                        <div class="form-group @if($errors->has('group_bring_name')) has-error @endif">
                            <label for="inputEmail3" class="col-lg-3 control-label">หมายเลขใบเบิก</label>
                            <div class="col-lg-9">
                                <input type="text" value="@if(false==empty(Input::old('group_bring_name'))){{Input::old('group_bring_name')}}@elseif(false==empty($Bring['group_bring_name'])){{$Bring['group_bring_name']}}@endif" name="group_bring_name" class="form-control" id="inputEmail3" placeholder="ชื่อกลุ่มใบเบิก">
                            </div>
                        </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button style="width: 100%;" type="submit" class="btn btn-primary">สร้าง</button>
                </div>
            </div>
        </div>
        </form>
    </div>

    <div class="modal fade @if(false==empty($Bring['bring_id'])||true==$errors->has('bring_number')||true==$errors->has('bring_date')||true==$errors->has('member_bring_date')||true==$errors->has('id_member_bring')) in @endif" style="@if(false==empty($Bring['bring_id'])||true==$errors->has('bring_number')||true==$errors->has('bring_date')||true==$errors->has('member_bring_date')||true==$errors->has('id_member_bring')) display: block; @endif" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <form method="POST" action="/bring/addBring" >
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <a href="/bring/viewBring" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                        <h4 class="modal-title" id="myModalLabel">กรอกรายละเอียดใบเบิกสินค้า</h4>
                    </div>
                    <div class="modal-body">
                        @if ($errors->has())
                            <div class="alert alert-danger" style="padding: 0px;">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}
                                @endforeach
                            </div>
                        @endif
                        <div class="form-horizontal">
                            <input type="hidden" name="bring_id" value="@if(false==empty($Bring['bring_id'])){{$Bring['bring_id']}}@elseif(false==empty(Input::old('bring_id'))){{Input::old('bring_id')}}@endif">
                            <div class="form-group @if($errors->has('bring_number')) has-error @endif">
                                <label for="inputEmail3" class="col-lg-3 control-label">หมายเลขใบเบิก</label>
                                <div class="col-lg-9">
                                    <input type="text" value="@if(false==empty(Input::old('bring_number'))){{Input::old('bring_number')}}@elseif(false==empty($Bring['bring_number'])){{$Bring['bring_number']}}@endif" name="bring_number" class="form-control" id="inputEmail3" placeholder="หมายเลขใบเบิก">
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('bring_date')) has-error @endif">
                                <label for="inputEmail3" class="col-lg-3 control-label">วันที่เบิก</label>
                                <div class="col-lg-9">
                                    <input type="text" id="bring_date" value="@if(false==empty(Input::old('bring_date'))){{Input::old('bring_date')}}@elseif(false==empty($Bring['bring_date'])){{$Bring['bring_date']}}@endif" name="bring_date" class="form-control" id="inputEmail3" placeholder="วันที่เบิก">
                                </div>
                            </div>
                            {{--<div class="form-group @if($errors->has('group_bring_id')) has-error @endif">--}}
                                {{--<label for="inputPassword3" class="col-lg-3 control-label">กลุ่มใบเบิก</label>--}}
                                {{--<div class="col-lg-5">--}}
                                    {{--<select class="form-control" name="group_bring_id">--}}
                                        {{--<option value="">เลือกกลุ่มใบเบิก</option>--}}
                                        {{--@foreach($dataGroupBring as $key=>$value)--}}
                                            {{--<option @if(Input::old('group_bring_id')==$value->group_bring_id||(false==empty($Bring['group_bring_id'])&&$Bring['group_bring_id']==$value->group_bring_id)) selected @endif value="{{$value->group_bring_id}}">{{$value->group_bring_name}}</option>--}}
                                        {{--@endforeach--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="form-group">
                                <label for="inputEmail3" class="col-lg-3 control-label">ประเภท</label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox1" @if(false==empty(Input::old('bring_type_manufacture'))) checked @elseif(false==empty($Bring['bring_type_manufacture'])&&$Bring['bring_type_manufacture']=='Y') checked @endif name="bring_type_manufacture" > สำหรับใช้ในงานผลิต
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox2" @if(false==empty(Input::old('bring_type_project'))) checked @elseif(false==empty($Bring['bring_type_project'])&&$Bring['bring_type_project']=='Y') checked @endif name="bring_type_project" > สำหรับใช้ในโครงการ
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox3" @if(false==empty(Input::old('bring_type_other'))) checked @elseif(false==empty($Bring['bring_type_other'])&&$Bring['bring_type_project']=='Y') checked @endif name="bring_type_other" > อื่น
                                </label>
                            </div>

                            {{--************************รายชื่อผู้เบิกของ*******************--}}
                            <div class="form-group @if($errors->has('id_member_bring')) has-error @endif">
                                <label for="inputPassword3" class="col-lg-3 control-label">ผู้ขอเบิก</label>
                                <div class="col-lg-5">
                                    <select class="form-control" name="id_member_bring">
                                        <option value="">เลือกผู้ขอเบิก</option>
                                        @foreach($dataStaff as $key=>$value)
                                            <option  @if (Input::old('id_member_bring')==$value->staff_id||(false==empty($Bring['id_member_bring'])&&$Bring['id_member_bring']==$value->staff_id)) selected @endif value="{{$value->staff_id}}">{{$value->staff_firstname}} {{$value->staff_lastname}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4 @if($errors->has('member_bring_date')) has-error @endif" style="">
                                    <input type="text"  value="@if(false==empty(Input::old('member_bring_date'))){{Input::old('member_bring_date')}}@elseif(false==empty($Bring['member_bring_date'])){{$Bring['member_bring_date']}}@endif" name="member_bring_date" class="form-control" id="member_bring_date" placeholder="วันที่ผู้ขอเบิก">
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('id_member_approve_bring')) has-error @endif">
                                <label for="inputPassword3" class="col-lg-3 control-label">ผู้อนุมัตเบิก</label>
                                <div class="col-lg-5">
                                    <select class="form-control" name="id_member_approve_bring">
                                        <option value="">เลือกผู้อนุมัตเบิก</option>
                                        @foreach($dataStaff as $key=>$value)
                                            <option  @if (Input::old('id_member_approve_bring')==$value->staff_id||(false==empty($Bring['id_member_approve_bring'])&&$Bring['id_member_approve_bring']==$value->staff_id)) selected @endif value="{{$value->staff_id}}">{{$value->staff_firstname}} {{$value->staff_lastname}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4 @if($errors->has('member_approve_bring_date')) has-error @endif" style="">
                                    <input type="text"  value="@if(false==empty(Input::old('member_approve_bring_date'))){{Input::old('member_approve_bring_date')}}@elseif(false==empty($Bring['member_approve_bring_date'])){{$Bring['member_approve_bring_date']}}@endif" name="member_approve_bring_date" class="form-control" id="member_approve_bring_date" placeholder="วันที่อนุมัติเบิก">
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('id_member_approve_pay')) has-error @endif">
                                <label for="inputPassword3" class="col-lg-3 control-label">ผู้อนุมัติจ่าย</label>
                                <div class="col-lg-5">
                                    <select class="form-control" name="id_member_approve_pay">
                                        <option value="">เลือกผู้อนุมัติจ่าย</option>
                                        @foreach($dataStaff as $key=>$value)
                                            <option  @if (Input::old('id_member_approve_pay')==$value->staff_id||(false==empty($Bring['id_member_approve_pay'])&&$Bring['id_member_approve_pay']==$value->staff_id)) selected @endif value="{{$value->staff_id}}">{{$value->staff_firstname}} {{$value->staff_lastname}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4 @if($errors->has('member_approve_pay_date')) has-error @endif" style="">
                                    <input type="text"  value="@if(false==empty(Input::old('member_approve_pay_date'))){{Input::old('member_approve_pay_date')}}@elseif(false==empty($Bring['member_approve_pay_date'])){{$Bring['member_approve_pay_date']}}@endif" name="member_approve_pay_date" class="form-control" id="member_approve_pay_date" placeholder="วันที่อนุมัติจ่าย">
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('id_member_record')) has-error @endif">
                                <label for="inputPassword3" class="col-lg-3 control-label">เจ้าหน้าที่คลัง</label>
                                <div class="col-lg-5">
                                    <select class="form-control" name="id_member_store">
                                        <option value="">เลือกเจ้าหน้าที่คลัง</option>
                                        @foreach($dataStaff as $key=>$value)
                                            <option  @if (Input::old('id_member_store')==$value->staff_id||(false==empty($Bring['id_member_store'])&&$Bring['id_member_store']==$value->staff_id)) selected @endif value="{{$value->staff_id}}">{{$value->staff_firstname}} {{$value->staff_lastname}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4  @if($errors->has('member_store_date')) has-error @endif" style="">
                                    <input type="text" value="@if(false==empty(Input::old('member_store_date'))){{Input::old('member_store_date')}}@elseif(false==empty($Bring['member_store_date'])){{$Bring['member_store_date']}}@endif" name="member_store_date" class="form-control" id="member_store_date" placeholder="วันที่จ่ายของ">
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('id_member_account')) has-error @endif">
                                <label for="inputPassword3" class="col-lg-3 control-label">บัญชี</label>
                                <div class="col-lg-5">
                                    <select class="form-control" name="id_member_account">
                                        <option value="">เลือกฝ่ายบัญชี</option>
                                        @foreach($dataStaff as $key=>$value)
                                            <option  @if (Input::old('id_member_account')==$value->staff_id||(false==empty($Bring['id_member_account'])&&$Bring['id_member_account']==$value->staff_id)) selected @endif value="{{$value->staff_id}}">{{$value->staff_firstname}} {{$value->staff_lastname}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4 @if($errors->has('member_account_date')) has-error @endif" style="">
                                    <input type="text" value="@if(false==empty(Input::old('member_account_date'))){{Input::old('member_account_date')}}@elseif(false==empty($Bring['member_account_date'])){{$Bring['member_account_date']}}@endif" name="member_account_date" class="form-control" id="member_account_date" placeholder="วันที่ตรวจสอบ">
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('id_member_driver')) has-error @endif">
                                <label for="inputPassword3" class="col-lg-2 control-label">ผู้ขับรถ</label>
                                <div class="col-lg-3" style="padding: 0px;">
                                    <select class="form-control" name="id_member_driver">
                                        <option value="">เลือกผู้ขับรถ</option>
                                        @foreach($dataStaff as $key=>$value)
                                            <option  @if (Input::old('id_member_driver')==$value->staff_id||(false==empty($Bring['id_member_driver'])&&$Bring['id_member_driver']==$value->staff_id)) selected @endif value="{{$value->staff_id}}">{{$value->staff_firstname}} {{$value->staff_lastname}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-3  @if($errors->has('bring_vehicles_id')) has-error @endif" style="">
                                    <select class="form-control" name="bring_vehicles_id">
                                        <option value="">รถที่ไปส่ง</option>
                                        @foreach($dataVehicles as $key=>$value)
                                            <option  @if (Input::old('bring_vehicles_id')==$value['id']||(false==empty($Bring['bring_vehicles_id'])&&$Bring['bring_vehicles_id']==$value['id'])) selected @endif value="{{$value->id}}">{{$value->brands}} {{$value->series}} {{$value->license_plate}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4  @if($errors->has('member_driver_date')) has-error @endif" style="">
                                    <input type="text" value="@if(false==empty(Input::old('member_driver_date'))){{Input::old('member_driver_date')}}@elseif(false==empty($Bring['member_driver_date'])){{$Bring['member_driver_date']}}@endif" name="member_driver_date" class="form-control" id="member_driver_date" placeholder="วันที่ออกรถ">
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('bring_detail')) has-error @endif">
                                <label class="col-lg-3 control-label" for="exampleInputEmail1">รายละเอียด</label>
                                <div class="col-lg-9" style="">
                                    <textarea rows="4" class="form-control" name="bring_detail" placeholder="รายละเอียด">@if(false==empty(Input::old('bring_detail'))){{Input::old('bring_detail')}}@elseif(false==empty($Bring['bring_detail'])){{$Bring['bring_detail']}}@endif</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        {{--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                        <button style="width: 100%;" type="submit" class="btn btn-primary">เพิ่ม</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop