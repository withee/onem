<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('page-title', 'PDF Document')</title>

</head>
<body>
<style>
    @font-face {
        font-family: "Angsana-New";
        src:url('/font/angsa.ttf') format("truetype");
    }
    h1 { font-family: "Kimberley", sans-serif }
    table{
        width:100%;
        border: solid 1px;
    }
    tr{
        width: 100%;
        border: solid 1px;
    }
    td.main{
        border: solid 1px #FFFFff;
        width: 10%;
    }
    td.noneBorder{
        border: solid 1px #ffffff;
        width: 10%;
        font-family: Angsana-New;
        font-size: 16px;
    }
    td.noneBorderTableHand{
        /*border: solid 1px #ffffff;*/
        width: 10%;
        font-family: Angsana-New;
        font-size: 13px;
        text-align: center;
        line-height: 0.9;
        /*height: 20px;*/
    }
    td.noneBorderTable{
        /*border: solid 1px #ffffff;*/
        width: 10%;
        font-family: Angsana-New;
        font-size: 11px;
    }

    /*tr.table>td:first-child{*/
        /*border-bottom: solid 1px;border-top: solid 1px;border-left: solid 1px;*/
    /*}*/
</style>
<table style="">
    <tr>
        <td class="main"></td>
        <td class="main"></td>
        <td class="main"></td>
        <td class="main"></td>
        <td class="main"></td>
        <td class="main"></td>
        <td class="main"></td>
        <td class="main"></td>
        <td class="main"></td>
        <td class="main"></td>
    </tr>
    <tr>
        <td class="noneBorder"><img style="width:50px" src="./image/onem.jpg"/></td>
        <td class="noneBorder" colspan="7" style="text-align: center;">
            <span style="font-size:18px;">ใบคืนสินค้า</span><br>
            <span style="font-size:14px;">วันที่........................... เดือน..................................... พ.ศ. ..............</span>
        </td>
        <td colspan="2" class="noneBorder" style="padding-bottom: 30px;">
            <span style="font-size:16px;">เลขที่  {{date('YmdHms')}}</span><br>
            {{--<span style="font-size:10px;color: #FFFFff;">ว</span>--}}
        </td>
    </tr>
    <tr>
        <td class="noneBorder" colspan="5">ชื่อผู้เบิก.............................................................................</td>
        <td class="noneBorder" colspan="5">ตำแหน่ง.................................................</td>
    </tr>
    <tr>
        <td class="noneBorder" colspan="2" style="line-height: 0.4;">วัตถุประสงค์การใช้งาน</td>
        <td class="noneBorder" colspan="6" style="line-height: 0.6;">
            [ ] สำหรับใช้ในการผลิต
        </td>
        <td class="noneBorder" colspan="2" style="line-height: 0.4;">

        </td>
    </tr>
    <tr>
        <td class="noneBorder" colspan="2"></td>
        <td class="noneBorder" colspan="6" style="line-height: 0.6;">
            [ ] สำหรับใช้ในโครงการ (ระบุโครงการ)......................
        </td>
        <td class="noneBorder" colspan="2"></td>
    </tr>
    <tr>
        <td class="noneBorder" colspan="2"></td>
        <td class="noneBorder" colspan="6" style="line-height: 0.6;">
            [ ] อื่น.................
        </td>
        <td class="noneBorder" colspan="2"></td>
    </tr>
    <tr>
        <td colspan="10"></td>
    </tr>
    <tr class="table">
        <td  class="noneBorderTableHand"><div style="height: 21px;border:solid 1px;border-bottom: solid 0px;">ลำดับ</div></td>
        <td  class="noneBorderTableHand" colspan="6"><div style="height: 21px;margin-left: -5px;border: solid 1px;border-bottom: solid 0px;">รายงาน</div></td>
        <td  class="noneBorderTableHand" colspan="2"><div style="height: 20px;margin-left: -5px;border:solid 1px;">จำนวนเบิก</div></td>
        <td  class="noneBorderTableHand"><div style="height: 21px;margin-left: -5px;border: solid 1px;border-bottom: solid 0px;">หมายเหตุ</div></td>
    </tr>
    <tr>
        <td class="noneBorderTableHand" ><div style="height: 20px;border:solid 1px;border-top: solid 0px;margin-top: -5px;"></div></td>
        <td class="noneBorderTableHand" colspan="6"><div style="height: 20px;margin-left: -5px;border:solid 1px;margin-top: -5px;border-top: solid 0px;"></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px">จำนวน</div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px">หน่วยนับ</div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px;border-top: solid 0px;"></div></td>
    </tr>
    <tr>
        <td class="noneBorderTableHand" ><div style="height: 20px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand" colspan="6"><div style="height: 20px;;margin-left: -5px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;;margin-left: -5px;border: solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
    </tr>
    <tr>
        <td class="noneBorderTableHand" ><div style="height: 20px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand" colspan="6"><div style="height: 20px;;margin-left: -5px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;;margin-left: -5px;border: solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
    </tr>
    <tr>
        <td class="noneBorderTableHand" ><div style="height: 20px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand" colspan="6"><div style="height: 20px;;margin-left: -5px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;;margin-left: -5px;border: solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
    </tr>
    <tr>
        <td class="noneBorderTableHand" ><div style="height: 20px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand" colspan="6"><div style="height: 20px;;margin-left: -5px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;;margin-left: -5px;border: solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
    </tr>
    <tr>
        <td class="noneBorderTableHand" ><div style="height: 20px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand" colspan="6"><div style="height: 20px;;margin-left: -5px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;;margin-left: -5px;border: solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
    </tr>
    <tr>
        <td class="noneBorderTableHand" ><div style="height: 20px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand" colspan="6"><div style="height: 20px;;margin-left: -5px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;;margin-left: -5px;border: solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
    </tr>
    <tr>
        <td class="noneBorderTableHand" ><div style="height: 20px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand" colspan="6"><div style="height: 20px;;margin-left: -5px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;;margin-left: -5px;border: solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
    </tr>
    <tr>
        <td class="noneBorderTableHand" ><div style="height: 20px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand" colspan="6"><div style="height: 20px;;margin-left: -5px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;;margin-left: -5px;border: solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
    </tr>
    <tr>
        <td class="noneBorderTableHand" ><div style="height: 20px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand" colspan="6"><div style="height: 20px;;margin-left: -5px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;;margin-left: -5px;border: solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
    </tr>
    <tr>
        <td class="noneBorderTableHand" ><div style="height: 20px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand" colspan="6"><div style="height: 20px;;margin-left: -5px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;;margin-left: -5px;border: solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
    </tr>
    {{--<tr>--}}
        {{--<td class="noneBorderTableHand" ><div style="height: 20px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>--}}
        {{--<td class="noneBorderTableHand" colspan="6"><div style="height: 20px;;margin-left: -5px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>--}}
        {{--<td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>--}}
        {{--<td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>--}}
        {{--<td class="noneBorderTableHand"><div style="height: 20px;;margin-left: -5px;border: solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>--}}
    {{--</tr>--}}
    <tr>
        <td class="noneBorderTableHand" colspan="2">
            <div style="border: solid 1px; margin-top: -5px">
                <div>ผู้ขอเบิก</div>
                <div style="margin-top: 10px;">(.........................................)</div>
                <div style="margin-top: 10px;">วันที่................................</div>
            </div>
        </td>
        <td class="noneBorderTableHand" colspan="2">
            <div style="border: solid 1px; margin-top: -5px">
                <div>ผู้อนุมัติเบิก</div>
                <div style="margin-top: 10px;">(.........................................)</div>
                <div style="margin-top: 10px;">วันที่................................</div>
            </div>
        </td>
        <td class="noneBorderTableHand" colspan="2">
            <div style="border: solid 1px; margin-top: -5px">
                <div>ผู้อนุมัติจ่าย</div>
                <div style="margin-top: 10px;">(.........................................)</div>
                <div style="margin-top: 10px;">วันที่................................</div>
            </div>
        </td>
        <td class="noneBorderTableHand" colspan="2">
            <div style="border: solid 1px; margin-top: -5px">
                <div>เจ้าหน้าที่คลัง</div>
                <div style="margin-top: 10px;">(.........................................)</div>
                <div style="margin-top: 10px;">วันที่................................</div>
            </div>
        </td>
        <td class="noneBorderTableHand" colspan="2">
            <div style="border: solid 1px; margin-top: -5px">
                <div>บัญชี</div>
                <div style="margin-top: 10px;">(.........................................)</div>
                <div style="margin-top: 10px;">วันที่................................</div>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="10"></td>
    </tr>
</table>
<table style="margin-top: 10px;">
    <tr>
        <td class="main"></td>
        <td class="main"></td>
        <td class="main"></td>
        <td class="main"></td>
        <td class="main"></td>
        <td class="main"></td>
        <td class="main"></td>
        <td class="main"></td>
        <td class="main"></td>
        <td class="main"></td>
    </tr>
    <tr>
        <td class="noneBorder"><img style="width:50px" src="./image/onem.jpg"/></td>
        <td class="noneBorder" colspan="7" style="text-align: center;">
            <span style="font-size:18px;">ใบคืนสินค้า</span><br>
            <span style="font-size:14px;">วันที่........................... เดือน..................................... พ.ศ. ..............</span>
        </td>
        <td colspan="2" class="noneBorder" style="padding-bottom: 30px;">
            <span style="font-size:16px;">เลขที่  {{date('YmdHms', strtotime('+1 Seconds'))}}</span><br>
            {{--<span style="font-size:10px;color: #FFFFff;">ว</span>--}}
        </td>
    </tr>
    <tr>
        <td class="noneBorder" colspan="5">ชื่อผู้เบิก.............................................................................</td>
        <td class="noneBorder" colspan="5">ตำแหน่ง.................................................</td>
    </tr>
    <tr>
        <td class="noneBorder" colspan="2" style="line-height: 0.4;">วัตถุประสงค์การใช้งาน</td>
        <td class="noneBorder" colspan="6" style="line-height: 0.6;">
            [ ] สำหรับใช้ในการผลิต
        </td>
        <td class="noneBorder" colspan="2" style="line-height: 0.4;">

        </td>
    </tr>
    <tr>
        <td class="noneBorder" colspan="2"></td>
        <td class="noneBorder" colspan="6" style="line-height: 0.6;">
            [ ] สำหรับใช้ในโครงการ (ระบุโครงการ)......................
        </td>
        <td class="noneBorder" colspan="2"></td>
    </tr>
    <tr>
        <td class="noneBorder" colspan="2"></td>
        <td class="noneBorder" colspan="6" style="line-height: 0.6;">
            [ ] อื่น.................
        </td>
        <td class="noneBorder" colspan="2"></td>
    </tr>
    <tr>
        <td colspan="10"></td>
    </tr>
    <tr class="table">
        <td  class="noneBorderTableHand"><div style="height: 21px;border:solid 1px;border-bottom: solid 0px;">ลำดับ</div></td>
        <td  class="noneBorderTableHand" colspan="6"><div style="height: 21px;margin-left: -5px;border: solid 1px;border-bottom: solid 0px;">รายงาน</div></td>
        <td  class="noneBorderTableHand" colspan="2"><div style="height: 20px;margin-left: -5px;border:solid 1px;">จำนวนเบิก</div></td>
        <td  class="noneBorderTableHand"><div style="height: 21px;margin-left: -5px;border: solid 1px;border-bottom: solid 0px;">หมายเหตุ</div></td>
    </tr>
    <tr>
        <td class="noneBorderTableHand" ><div style="height: 20px;border:solid 1px;border-top: solid 0px;margin-top: -5px;"></div></td>
        <td class="noneBorderTableHand" colspan="6"><div style="height: 20px;margin-left: -5px;border:solid 1px;margin-top: -5px;border-top: solid 0px;"></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px">จำนวน</div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px">หน่วยนับ</div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px;border-top: solid 0px;"></div></td>
    </tr>
    <tr>
        <td class="noneBorderTableHand" ><div style="height: 20px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand" colspan="6"><div style="height: 20px;;margin-left: -5px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;;margin-left: -5px;border: solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
    </tr>
    <tr>
        <td class="noneBorderTableHand" ><div style="height: 20px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand" colspan="6"><div style="height: 20px;;margin-left: -5px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;;margin-left: -5px;border: solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
    </tr>
    <tr>
        <td class="noneBorderTableHand" ><div style="height: 20px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand" colspan="6"><div style="height: 20px;;margin-left: -5px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;;margin-left: -5px;border: solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
    </tr>
    <tr>
        <td class="noneBorderTableHand" ><div style="height: 20px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand" colspan="6"><div style="height: 20px;;margin-left: -5px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;;margin-left: -5px;border: solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
    </tr>
    <tr>
        <td class="noneBorderTableHand" ><div style="height: 20px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand" colspan="6"><div style="height: 20px;;margin-left: -5px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;;margin-left: -5px;border: solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
    </tr>
    <tr>
        <td class="noneBorderTableHand" ><div style="height: 20px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand" colspan="6"><div style="height: 20px;;margin-left: -5px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;;margin-left: -5px;border: solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
    </tr>
    <tr>
        <td class="noneBorderTableHand" ><div style="height: 20px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand" colspan="6"><div style="height: 20px;;margin-left: -5px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;;margin-left: -5px;border: solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
    </tr>
    <tr>
        <td class="noneBorderTableHand" ><div style="height: 20px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand" colspan="6"><div style="height: 20px;;margin-left: -5px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;;margin-left: -5px;border: solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
    </tr>
    <tr>
        <td class="noneBorderTableHand" ><div style="height: 20px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand" colspan="6"><div style="height: 20px;;margin-left: -5px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;;margin-left: -5px;border: solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
    </tr>
    <tr>
        <td class="noneBorderTableHand" ><div style="height: 20px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand" colspan="6"><div style="height: 20px;;margin-left: -5px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;;margin-left: -5px;border: solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
    </tr>
    {{--<tr>--}}
        {{--<td class="noneBorderTableHand" ><div style="height: 20px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>--}}
        {{--<td class="noneBorderTableHand" colspan="6"><div style="height: 20px;;margin-left: -5px;border:solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>--}}
        {{--<td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>--}}
        {{--<td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color: #FFFFFF;">1</span></div></td>--}}
        {{--<td class="noneBorderTableHand"><div style="height: 20px;;margin-left: -5px;border: solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>--}}
    {{--</tr>--}}
    <tr>
        <td class="noneBorderTableHand" colspan="2">
            <div style="border: solid 1px; margin-top: -5px">
                <div>ผู้ขอเบิก</div>
                <div style="margin-top: 10px;">(.........................................)</div>
                <div style="margin-top: 10px;">วันที่................................</div>
            </div>
        </td>
        <td class="noneBorderTableHand" colspan="2">
            <div style="border: solid 1px; margin-top: -5px">
                <div>ผู้อนุมัติเบิก</div>
                <div style="margin-top: 10px;">(.........................................)</div>
                <div style="margin-top: 10px;">วันที่................................</div>
            </div>
        </td>
        <td class="noneBorderTableHand" colspan="2">
            <div style="border: solid 1px; margin-top: -5px">
                <div>ผู้อนุมัติจ่าย</div>
                <div style="margin-top: 10px;">(.........................................)</div>
                <div style="margin-top: 10px;">วันที่................................</div>
            </div>
        </td>
        <td class="noneBorderTableHand" colspan="2">
            <div style="border: solid 1px; margin-top: -5px">
                <div>เจ้าหน้าที่คลัง</div>
                <div style="margin-top: 10px;">(.........................................)</div>
                <div style="margin-top: 10px;">วันที่................................</div>
            </div>
        </td>
        <td class="noneBorderTableHand" colspan="2">
            <div style="border: solid 1px; margin-top: -5px">
                <div>บัญชี</div>
                <div style="margin-top: 10px;">(.........................................)</div>
                <div style="margin-top: 10px;">วันที่................................</div>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="10"></td>
    </tr>
</table>
</body>
</html>