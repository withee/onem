@extends('layouts.layouts')
@section('content')

    @if(false==empty($ControlBuildLog))
        <div class="row" style="margin-top: 10px;">
            <div class="col-lg-12">
                <ol class="breadcrumb">
                    <li><a href="/controlBuild/viewControlBuild">จัดการงานผลิตเเละโครงการ</a></li>
                    <li class="active">รายการสินค้า</li>
                </ol>
            </div>
        </div>
    @elseif(false==empty($dataSubProject))
        <div class="row" style="margin-top: 10px;">
            <div class="col-lg-12">
                <ol class="breadcrumb">
                    <li><a href="/project/viewSubProject?id={{$dataSubProject->project_sub_id}}">จัดการโครงการย่อย</a></li>
                    <li class="active">รายการสินค้า</li>
                </ol>
            </div>
        </div>
    @elseif(false==empty($Bring->group_bring_id))
        <div class="row" style="margin-top: 10px;">
            <div class="col-lg-12">
                <ol class="breadcrumb">
                    <li><a href="/groupBring/viewGroupBring">กลุ่มใบเบิกสินค้า</a></li>
                    <li class="active">รายการสินค้า</li>
                </ol>
            </div>
        </div>
    @else
        <div class="row" style="margin-top: 10px;">
            <div class="col-lg-12">
                <ol class="breadcrumb">
                    <li><a href="/bring/viewBring">จัดการใบเบิกสินค้า</a></li>
                    <li class="active">รายการสินค้า</li>
                </ol>
            </div>
        </div>
    @endif


    <div class="row">
        <div class="col-lg-6">
            <h3>ลงสินค้าเบิก</h3>
        </div>
        <div class="col-lg-6">
            <div style="font-size:16px;font-weight:bolder;padding: 5px;background-color: #CCCCCC;border-radius: 5px;border: solid 1px#aaaaaa;">
                <div>หมายเลข: {{$dataBring->bring_number}} บันทึกวันที่: {{$dataBring->created_at}} </div>
                <div>เบิกใช้: {{(($dataBring->bring_type_manufacture=='Y')?'เพื่อผลิต':'')}} {{(($dataBring->bring_type_project=='Y')?'ใช้ในโครงการ':'')}} {{(($dataBring->bring_type_other=='Y')?'อื่นๆ':'')}} </div>
                <div>ผู้บักทึก: {{ \App\Models\Staffs::getNameLastName($dataBring->id_member_record)}} ผู้เบิก: {{\App\Models\Staffs::getNameLastName($dataBring->id_member_bring)}}
                </div>
            </div>
    </div>

    <div class="row" style="padding: 10px;float: right;">
        <div class="col-lg-12">
            <form method="POST" action="/bring/addProduct" class="form-inline">
                <input type="hidden" name="bring_id" value="{{$id}}">
                @if (true==$errors->has('productId')||true==$errors->has('productCount')||true==$errors->has('storeId')||true==$errors->has('productLocation'))
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>
                        @endforeach
                    </div>
                @endif
                <div class="form-group @if ($errors->has('productId')) has-error @endif">
                    <label for="exampleInputName2">ชื่อสินค้า</label>
                    <select name="productId" class="form-control">
                        @foreach($dataProduct as $key=>$value)
                            <option @if (Input::old('productId')==$value['product_id']) selected @endif value="{{$value->product_id}}">{{$value->product_name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group @if ($errors->has('productCount')) has-error @endif">
                    <label for="exampleInputEmail2">จำนวน</label>
                    <input style="width: 100px;" name="productCount" type="number" class="form-control" placeholder="จำนวน" value="@if(false==empty(Input::old('productCount'))){{Input::old('productCount')}}@endif">
                </div>
                <div class="form-group @if ($errors->has('storeId')) has-error @endif">
                        <label for="exampleInputName2">คลัง</label>
                        <select name="storeId" class="form-control">
                            @foreach($dataStoreName as $key=>$value)
                                <option @if (Input::old('storeId')==$value['storename_id']) selected @endif value="{{$value->storename_id}}">{{$value->storename_name}}</option>
                            @endforeach
                        </select>
                </div>
                <div class="form-group @if ($errors->has('productPrice')) has-error @endif">
                    <label for="exampleInputEmail2">สถานที่ส่ง/หน้างาน</label>
                    <input style="width: 300px;" name="productLocation" type="text" class="form-control" placeholder="สถานที่" value="@if(false==empty(Input::old('productLocation'))){{Input::old('productLocation')}}@endif">
                </div>
                <button type="submit" class="btn btn-default">เพิ่มสินค้า</button>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th>รหัสสินค้า</th>
                    <th>ชื่อสินค้า</th>
                    <th>จำนวน</th>
                    <th>ลงคลัง</th>
                    <th>จัดส่ง/สาเหตุ</th>
                    <th>จัดการ</th>
                </tr>
                </thead>
                <tbody>
                @if(false==empty($listProduct))
                    @foreach($listProduct as $key=>$value)
                        <tr style="background-color: @if($value->bring_type=='bring') #FFFFFF @else #cccccc @endif">
                            <th scope="row">{{$key+1}}</th>
                            <td>{{$value->product_id}}</td>
                            <td>{{$value->product_name}}</td>
                            <td>{{$value->bring_detail_count}}</td>
                            <td>{{$value->storename_name}}</td>
                            <td>{{$value->bring_detail_location}}</td>
                            <td>
                                <button onclick="selectProduct('{{$value->product_id}}');" class="btn btn-danger" data-toggle="modal" data-target="#myModalDeteriorate">ชำรุด</button>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>

    <script>
        function selectProduct(data){
            $('select#manufactureProduct_id').val(data);
            $('select#deteriorateProduct_id').val(data);
        }
    </script>

    <div class="modal fade @if(true==$errors->has('deteriorateProduct_id')||true==$errors->has('deteriorateProduct_count')||true==$errors->has('deteriorateProduct_detail')) in @endif" style="@if(true==$errors->has('deteriorateProduct_id')||true==$errors->has('deteriorateProduct_count')||true==$errors->has('deteriorateProduct_detail')) display: block; @endif" id="myModalDeteriorate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <form method="POST" action="/bring/addProductDeteriorate" >
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">สินค้าชำรุด</h4>
                    </div>
                    <div class="modal-body">
                        @if ($errors->has())
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </div>
                        @endif
                        <div class="form-horizontal">
                            <input type="hidden" id="deteriorate_bring_id" value="{{$id}}" name="deteriorate_bring_id">
                            <div class="form-group @if ($errors->has('deteriorateProduct_id')||$errors->has('deteriorateProduct_count')) has-error @endif">
                                <label for="exampleInputName2" class="col-lg-3 control-label">สินค้าชำรุด</label>
                                <div class="col-lg-5" style="display:none;">
                                    <select readonly name="deteriorateProduct_id" id="deteriorateProduct_id" class="form-control">
                                        @foreach($dataProduct as $key=>$value)
                                            <option @if (Input::old('deteriorateProduct_id')==$value['product_id']) selected @endif value="{{$value->product_id}}">{{$value->product_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-3" style="">
                                    <input type="text" value="@if(false==empty(Input::old('deteriorateProduct_count'))){{Input::old('deteriorateProduct_count')}}@endif" name="deteriorateProduct_count" class="form-control" id="inputEmail3" placeholder="จำนวน">
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('deteriorateProduct_detail')) has-error @endif">
                                <label class="col-lg-3 control-label" for="exampleInputEmail1">สาเหตุ</label>
                                <div class="col-lg-8" style="">
                                    <textarea rows="4" class="form-control" value="@if(false==empty(Input::old('deteriorateProduct_detail'))){{Input::old('deteriorateProduct_detail')}}@endif" name="deteriorateProduct_detail" placeholder="สาเหตุ"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        {{--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                        <button style="width: 100%;" type="submit" class="btn btn-primary">เพิ่ม</button>
                    </div>
                </div>
            </div>
        </form>
    </div>


@stop