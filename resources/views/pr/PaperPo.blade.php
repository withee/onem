<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('page-title', 'PDF Document')</title>

</head>
<body style="font-family: angsana-new;">
<style >
    /*@font-face {*/
    /*font-family: "DejaVuSans";*/
    /*src:url('/font/DejaVuSans.ttf') format("truetype");*/
    /*}*/
    /*h1 { font-family: "Kimberley", sans-serif }*/
    table{
        width:100%;
        border: solid 1px;
    }
    tr{
        width: 100%;
        border: solid 1px;
    }
    td.main{
        border: solid 1px #fff;
        width: 10%;
    }
    td.noneBorder{
        border: solid 1px #ffffff;
        width: 10%;
    {{--font-family: DejaVuSans;--}}
        font-size: 16px;
    }
    td.noneBorderTableHand{
        /*border: solid 1px #ffffff;*/
        width: 10%;
    {{--font-family: DejaVuSans;--}}
        font-size: 13px;
        text-align: center;
        line-height: 0.9;
        /*height: 20px;*/
    }
    td.noneBorderTable{
        /*border: solid 1px #ffffff;*/
        width: 10%;
    {{--font-family: DejaVuSans;--}}
        font-size: 11px;
    }

    /*tr.table>td:first-child{*/
    /*border-bottom: solid 1px;border-top: solid 1px;border-left: solid 1px;*/
    /*}*/
</style>
<table style="">
    <tr>
        <td class="main"></td>
        <td class="main"></td>
        <td class="main"></td>
        <td class="main"></td>
        <td class="main"></td>
        <td class="main"></td>
        <td class="main"></td>
        <td class="main"></td>
        <td class="main"></td>
        <td class="main"></td>
    </tr>
    <tr>
        <td rowspan="2" style="text-align: center;"><img style="width:50px" src="./image/onem.jpg"/></td>
        <td colspan="9" style="font-size: 20px;line-height: 80%;">บริษัท วันเอ็ม จำกัด</td>
    </tr>
    <tr>
        {{--<td style="border: solid 1px"></td>--}}
        <td colspan="9" style="font-size: 16px;">290 ม.1 ต.กระโสบ อ.เมือง จ.อุบลราธานี 34000</td>
    </tr>
    <tr>
        <td colspan="10" style="font-size: 20px;text-align: right;">ใบจัดซื้อสินค้า
            {{--<td></td>--}}
    </tr>
    <tr>
        <td colspan="4">เลขประจำตัวผู้เสียภาษี {{(!empty($prOrder->pr_order_tax_number)?$prOrder->pr_order_tax_number:'-')}}</td>
        <td colspan="2" style="font-size: 20px;">สำนักงานใหญ่</td>
        <td colspan="4"></td>
    </tr>
    <tr>
        <td colspan="6" style="padding-left: 10px;">ถึง {{(!empty($customers->customers_name)?$customers->customers_name:'-')}} ที่อยู่ {{(!empty($customers->customers_address)?$customers->customers_address:'')}} โทร {{(!empty($customers->customers_telephone)?$customers->customers_telephone:"")}}</td>
        {{--<td colspan="5"></td>--}}
        <td colspan="2" style="text-align: right;">เลขใบจัดซื้อสินค้า</td>
        <td colspan="2" style="padding-left: 30px;">{{(!empty($prOrder->pr_order_number)?$prOrder->pr_order_number:'-')}}</td>
    </tr>
    <tr>
        <td style="padding-left: 20px;" colspan="10">{{(!empty($prOrder->PR_order_name)?$prOrder->PR_order_name:'-')}}</td>
    </tr>
    <tr>
        <td colspan="6"></td>
        <td colspan="2" style="text-align: right;">วันที่</td>
        <td colspan="2">{{(!empty($prOrder->pr_order_date)?\App\Models\BEConverter::ADtoBE($prOrder->pr_order_date):'-')}}</td>
    </tr>
    <tr>
        <td colspan="10"></td>
    </tr>
    <tr>
        <td colspan="10"></td>
    </tr>
    <tr>
        <td colspan="6" style="padding-left: 10px;">โทร. </td>
        <td colspan="2" style="text-align: right;">เครดิต </td>
        <td colspan="2" style="padding-left: 30px;">0 วัน</td>
    </tr>
    <tr>
        <td colspan="6" style="padding-left: 10px;">หมายเหตุ</td>
        <td colspan="2" style="text-align: right;">ขนส่งโดย</td>
        <td colspan="2" style="padding-left: 30px;"></td>
    </tr>
    <tr>
        <td colspan="10"><div style="height: 20px;margin-top: -5px;"></div></td>
    </tr>
    <tr class="table">
        <td  class="noneBorderTableHand"><div style="height: 21px;border: solid 1px;">No.</div></td>
        <td  class="noneBorderTableHand" colspan="5"><div style="text-align:center;height: 21px;margin-left: -5px;border: solid 1px;">รหัสสินค้า/รายละเอียด</div></td>
        <td  class="noneBorderTableHand" colspan="2"><div style="height: 21px;margin-left: -5px;border: solid 1px;">จำนวน</div></td>
        <td  class="noneBorderTableHand" ><div style="height: 21px;margin-left: -5px;border:solid 1px;">หน่วยล่ะ</div></td>
        <td  class="noneBorderTableHand"><div style="height: 21px;margin-left: -5px;border: solid 1px;">จำนวนเงิน</div></td>
    </tr>
    @foreach($prOrderDetail as $key=>$value)
        <div style="display: none;">{{$sum+=(\App\Models\Product::GetPriceByNameProduct($value->PR_order_detail_name)*$value->PR_order_detail_count)}}</div>
            <tr>
                <td class="noneBorderTableHand" >
                    <div style="height: 20px;margin-top: -5px;border-left: solid 1px;">
                        <span style="">{{$key+1}}</span>
                    </div>
                </td>
                <td class="noneBorderTableHand" colspan="5">
                    <div style="height: 20px;margin-left: -5px;margin-top: -5px;text-align: left;">
                        <span style="">{{$value->PR_order_detail_name}}</span>
                    </div>
                </td>
                <td class="noneBorderTableHand" colspan="2">
                    <div style="height: 20px;;margin-left: -5px;margin-top: -5px;">
                        <span style="">{{number_format($value->PR_order_detail_count,2)}} {{\App\Models\ProductUnit::getNameUnitByProductName($value->PR_order_detail_name)}}</span>
                    </div>
                </td>
                <td class="noneBorderTableHand">
                    <div style="height: 20px;margin-left: -5px;margin-top: -5px;text-align: right;">
                        <span style="padding-right: 5px;">{{number_format($value->pr_order_detail_price,2)}}</span>
                    </div>
                </td>
                <td class="noneBorderTableHand">
                    <div style="height: 20px;margin-left: -5px;margin-top: -5px;border-right: solid 1px;text-align: right;">
                        <span style="padding-right: 5px;">{{number_format(($value->pr_order_detail_price*$value->PR_order_detail_count),2)}}</span>
                    </div>
                </td>
            </tr>
    @endforeach
    @for($x = 0; $x <= (15-count($prOrderDetail)); $x++)
        @if($x==(15-count($prOrderDetail)))
            <tr>
                <td colspan="10">
                    <div style="height: 20px;margin-top: -5px;border-left: solid 1px;border-right: solid 1px;border-bottom: solid 1px;"></div>
                </td>
            </tr>
        @else
            <tr>
                <td colspan="10" >
                    <div style="height: 20px;margin-top: -5px;border-left: solid 1px;border-right: solid 1px;"></div>
                </td>
            </tr>
        @endif
    @endfor
    <tr class="noneBorderTableHand">
        <td colspan="6" style=""><div style="height: 20px;margin-top: -5px;border-left: solid 1px;"></div></td>
        <td colspan="3" style=""><div style="height: 20px;margin-top: -5px;">รวมเป็นเงิน</div></td>
        <td style="text-align: right;"><div style="height: 20px;margin-top: -5px;border-right: solid 1px;"><span style="padding-right: 5px;">{{number_format($sum,2)}}</span></div></td>
    </tr>
    <tr class="noneBorderTableHand">
        <td colspan="6" style=""><div style="height: 20px;margin-top: -5px;border-left: solid 1px;"></div></td>
        <td colspan="3" style=""><div style="height: 20px;margin-top: -5px;">หักส่วนลด</div></td>
        <td style="text-align: right;"><div style="height: 20px;margin-top: -5px;border-right: solid 1px;"><span style="padding-right: 5px;">{{number_format($prOrder->pr_order_discount,2)}}</span></div></td>
    </tr>
    <tr class="noneBorderTableHand">
        <td colspan="6" style=""><div style="height: 20px;margin-top: -5px;border-left: solid 1px;"></div></td>
        <td colspan="3" style=""><div style="height: 20px;margin-top: -5px;">จำนวนเงินหลังหักส่วนลด</div></td>
        <td style="text-align: right;"><div style="height: 20px;margin-top: -5px;border-right: solid 1px;"><span style="padding-right: 5px;">{{number_format($sum-$prOrder->pr_order_discount,2)}}</span></div></td>
    </tr>
    <tr class="noneBorderTableHand">
        <td colspan="6" style=""><div style="height: 20px;margin-top: -5px;border-left: solid 1px;"></div></td>
        <td colspan="3" style="">
            <div style="height: 20px;margin-top: -5px;">จำนวนภาษีมูลค่าเพิ่ม</div>
        </td>
        <td style="text-align: right;"><div style="height: 20px;margin-top: -5px;border-right: solid 1px;"><span style="padding-right: 5px;">{{number_format(((($sum*$prOrder->pr_order_tax)/100)),2)}}</span></div></td>
    </tr>
    <tr class="noneBorderTableHand">
        <td colspan="6" style="">
            <div style="height: 20px;margin-top: -5px;border-left: solid 1px;padding-left: 3px;">({{\App\Models\Customers::Convert((($sum+(($sum*$prOrder->pr_order_tax)/100))-$prOrder->pr_order_discount))}})</div></td>
        <td colspan="3" style="text-align: center;">
            <div style="height: 20px;margin-top: -5px;">จำนวนเงินรวมทั้งสิ้น</div>
        </td>
        <td style="text-align: right;">
            <div style="height: 20px;margin-top: -5px;border-right: solid 1px;">
                <span style="padding-right: 5px;">{{number_format((($sum+(($sum*$prOrder->pr_order_tax)/100))-$prOrder->pr_order_discount),2)}}</span>
            </div>
        </td>
    </tr>

    <tr class="noneBorderTableHand">
        <td colspan="10">
            <div style="height: 20px;margin-top: -5px;border-left:solid 1px;border-right:solid 1px;border-bottom: solid 1px;"><span style="color: #FFFFff;">.</span></div>
        </td>
    </tr>

    <tr class="noneBorderTableHand">
        <td colspan="7" style="">
            <div style="height: 20px;margin-top: -5px;border-left: solid 1px;padding-left: 30px;">หมายเหตุ: ชื่องาน {{(!empty($customersOrder->customers_order_name)?$customersOrder->customers_order_name:'-')}}
                ชื่อลูกค้า {{(!empty($customers->customers_name)?$customers->customers_name:'-')}}
                ที่อยู่ {{(!empty($customers->customers_address)?$customers->customers_address:'')}}
                โทร {{(!empty($customers->customers_telephone)?$customers->customers_telephone:"")}}
                เพื่อ{{(!empty($prOrder->PR_order_name)?$prOrder->PR_order_name:'-')}}
            </div>
        </td>
        <td colspan="3" style="">
            <div style="height: 20px;margin-top: -5px;border-right: solid 1px;text-align: center"></div>
        </td>
    </tr>

    <tr class="noneBorderTableHand">
        <td colspan="10">
            <div style="height: 60px;margin-top: -5px;border-left:solid 1px;border-right:solid 1px;"><span style="color: #FFFFff;">.</span></div>
        </td>
    </tr>

    <tr class="noneBorderTableHand">
        <td colspan="10">
            <div style="height: 60px;margin-top: -5px;border-left:solid 1px;border-right:solid 1px;border-bottom: solid 1px;"><span style="color: #FFFFff;">.</span></div>
        </td>
    </tr>

    <tr class="noneBorderTableHand">
        <td colspan="10">
            <div style="height: 60px;margin-top: -5px;border-left:solid 1px;border-right:solid 1px;"><span style="color: #FFFFff;">.</span></div>
        </td>
    </tr>

    <tr class="noneBorderTableHand">
        <td colspan="3">
            <div style="height: 20px;margin-top: -5px;border-left:solid 1px;text-align: center;">..................................................</div>
        </td>
        <td colspan="3" style="">
            <div style="height: 20px;margin-top: -5px;text-align: center;">..................................................</div>
        </td>
        <td colspan="4" style="">
            <div style="height: 20px;margin-top: -5px;border-right: solid 1px;"><span style="color: #FFFFff;">.</span></div>
        </td>
    </tr>

    <tr class="noneBorderTableHand">
        <td colspan="3">
            <div style="height: 20px;margin-top: -5px;border-left:solid 1px;text-align: center;">ผู้สั่งซื้อ</div>
        </td>
        <td colspan="3" style="">
            <div style="height: 20px;margin-top: -5px;text-align: center;">ผู้อนุมัติ</div>
        </td>
        <td colspan="4" style="">
            <div style="height: 20px;margin-top: -5px;border-right: solid 1px;"><span style="color: #FFFFff;">.</span></div>
        </td>
    </tr>
    <tr class="noneBorderTableHand">
        <td colspan="10"><div style="height: 20px;margin-top: -5px;border-left:solid 1px;border-bottom: solid 1px;border-right: solid 1px;"><span style="color: #FFFFff;">.</span></div></td>
    </tr>
</table>
</body>
</html>