@extends('layouts.layouts')
@section('content')
    <div class="row" style="padding-top: 10px;">
        <div class="col-lg-8">
            <ol class="breadcrumb" style="margin: 0px;">
                <li><a href="/Pr/listPr">เปิด PR สั่งของ</a></li>
                <li class="active">เพิ่มรายการสินค้า</li>
            </ol>
        </div>
        <div class="col-lg-3" style="padding: 0px;">
            <form method="GET" action="/Pr/listPr" class="form-inline">
                <div class="form-group">
                    <label for="exampleInputEmail2">ค้นหา</label>
                    <input style="width: 150px;" type="text" class="form-control" name="search" placeholder="ชื่อสินค้า">
                </div>
                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                <a href="/Pr/listPrOrder?orderId={{$PR_order_id}}" class="btn btn-default"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span></a>
            </form>
        </div>
        <div class="col-lg-1" style="padding-left: 0px;">
            @if($customersOrder->customers_order_status!="confirmPurchasing")
                <a class="btn btn-default" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> เพิ่มสินค้า</a>
            @endif
        </div>
    </div>
<form method="post" action="/Pr/addPrice">
    <input name="PR_order_id" type="hidden" value="{{$PR_order_id}}">
    <div class="row" style="padding: 10px;">
        <div class="col-lg-11" style="">
            <span style="font-size: 26px;font-weight: bolder">เพิ่มรายการสินค้า</span>
        </div>
        <div class="col-lg-1">
            <button class="btn btn-success" type="submit"><span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> บันทึก</button>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th>ชื่อสินค้า</th>
                    <th>จำนวน</th>
                    <th>ผู้เพิ่ม</th>
                    <th>จำนวน/ราคา</th>
                    <th>จัดการ</th>
                </tr>
                </thead>
                <tbody>
                @foreach($dataOrderDetail as $key=>$value)
                    <tr>
                        <th scope="row">{{$key+1}}</th>
                        <td>{{$value->PR_order_detail_name}}</td>
                        <td>{{$value->PR_order_detail_count}}</td>
                        <td>
                            {{$value->PR_order_detail_staff}}
                        </td>
                        <td>
                            <div class="form-inline">
                                <div class="form-group">
                                    <label for="exampleInputName2">จำนวน</label>
                                    <input name="order[{{$value->PR_order_detail_id}}][count]" style="width: 65px" type="text" class="form-control input-sm" id="" min="0" max="1000000" placeholder="จำนวน" value="{{$value->PR_order_detail_count}}">
                                    <label for="exampleInputName2">ราคาต่อหน่วย</label>
                                    <input name="order[{{$value->PR_order_detail_id}}][price]" type="text" class="form-control input-sm" id="" placeholder="ราคา" min="0" max="1000000" value="{{(!empty($value->pr_order_detail_price)?$value->pr_order_detail_price:0)}}">
                                </div>
                            </div>
                        </td>
                        <td>
                            @if($customersOrder->customers_order_status!="confirmPurchasing")
                                <a href="/Pr/deletePrOrderDetail?id={{$value->PR_order_detail_id}}&orderId={{$PR_order_id}}" onclick="return confirm('ยืนยันการ ยกเลิก สินค้า {{$value->PR_order_detail_name}} ')" class="btn btn-danger btn-xs">
                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> ลบ
                                </a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</form>
    <style>
        .twitter-typeahead{
            width: 100% !important;
        }
        .tt-query {
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        }

        .tt-hint {
            color: #999
        }

        .tt-menu {
            width: 422px;
            margin: 12px 0;
            padding: 8px 0;
            background-color: #fff;
            border: 1px solid #ccc;
            border: 1px solid rgba(0, 0, 0, 0.2);
            -webkit-border-radius: 8px;
            -moz-border-radius: 8px;
            border-radius: 8px;
            -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
            -moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
            box-shadow: 0 5px 10px rgba(0,0,0,.2);
        }

        .tt-suggestion {
            padding: 3px 20px;
            font-size: 18px;
            line-height: 24px;
        }

        .tt-suggestion:hover {
            cursor: pointer;
            color: #fff;
            background-color: #0097cf;
        }

        .tt-suggestion.tt-cursor {
            color: #fff;
            background-color: #0097cf;

        }

        .tt-suggestion p {
            margin: 0;
        }

        .gist {
            font-size: 14px;
        }
    </style>


    <script>
        $(document).ready(function () {
            var substringMatcher = function (strs) {
                return function findMatches(q, cb) {
                    var matches, substringRegex;

                    // an array that will be populated with substring matches
                    matches = [];

                    // regex used to determine if a string contains the substring `q`
                    substrRegex = new RegExp(q, 'i');

                    // iterate through the pool of strings and for any string that
                    // contains the substring `q`, add it to the `matches` array
                    $.each(strs, function (i, str) {
                        if (substrRegex.test(str)) {
                            matches.push(str);
                        }
                    });

                    cb(matches);
                };
            };

            $.ajax({
                url: "/factory/checkProductMaterial",
                type: "post",
                dataType: 'json'
            }).success(function (res) {
                $('input#the-basics').typeahead({
                            hint: true,
                            highlight: true,
                            minLength: 1
                        },
                        {
                            name: 'states',
                            source: substringMatcher(res)
                        });
            })
        });
    </script>

    <div class="modal fade @if (true==$errors->has('PR_order_detail_name')||true==$errors->has('PR_order_detail_count')) in @endif" style="@if (true==$errors->has('PR_order_detail_name')||true==$errors->has('PR_order_detail_count')) display: block @endif" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <form method="POST" action="/Pr/addPrMaterial" class="form-horizontal" novalidate>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">เพิ่มสินค้า</h4>
                </div>
                <div class="modal-body">
                    @if (true==$errors->has('PR_order_detail_name')||true==$errors->has('PR_order_detail_count'))
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif
                    <div class="form-horizontal" style="padding: 0px 20px;">
                        <input type="hidden" name="PR_order_detail_id" value="">
                        <input type="hidden" name="PR_order_id" value="{{$PR_order_id}}">
                        <div class="form-group @if ($errors->has('PR_order_detail_name')) has-error @endif">
                            <label for="exampleInputEmail1">ชื่อสินค้า <span style="color: #FF0000;">*</span></label>
                            <input id="the-basics" name="PR_order_detail_name" type="text" class="form-control" id="exampleInputEmail1" placeholder="ชื่อสินค้า">
                        </div>
                        <div class="form-group @if ($errors->has('PR_order_detail_count')) has-error @endif">
                            <label for="exampleInputEmail1">จำนวน <span style="color: #FF0000;">*</span></label>
                            <input name="PR_order_detail_count" type="text" class="form-control" id="exampleInputEmail1" placeholder="จำนวน">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </div>
            </form>
        </div>
    </div>

@stop