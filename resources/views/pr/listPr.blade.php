@extends('layouts.layouts')
@section('content')
<div class="row" style="padding: 10px 0px 10px 10px;">
    <div class="col-lg-7">
        <span style="font-weight: bolder;font-size: 24px;">เปิด PR สั่งของ</span>
    </div>
    <div class="col-lg-3" style="padding: 0px;">
        <form method="GET" action="/Pr/listPr" class="form-inline">
            <div class="form-group">
                <label for="exampleInputEmail2">ค้นหา</label>
                <input style="width: 150px;" type="text" class="form-control" name="search" placeholder="ชื่อใบ PR">
            </div>
            <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
            <a href="/Pr/listPr" class="btn btn-default"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span></a>
        </form>
    </div>
    <div class="col-lg-2">
        <a class="btn btn-default" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> เปิดใบ PR</a>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>#</th>
                <th>ชื่อใบ PR</th>
                <th>สถานะ</th>
                <th>ผู้เปิด/ผู้ยืนยัน</th>
                <th>จัดการ</th>
            </tr>
            </thead>
            <tbody>
            @foreach($dataPrOrder['data'] as $key=>$value)
                <tr>
                    <th scope="row">{{$key+1}}</th>
                    <td>
                        <a href="/Pr/listPrOrder?orderId={{$value->PR_order_id}}">
                            {{substr($value->PR_order_name, 0, 100)}}
                        </a></td>
                    <td>


                        @if($value->PR_order_status=='New')
                            <span class="label label-default">เปิดใหม่</span>
                        @elseif($value->PR_order_status=='confirmPurchasing')
                            <span class="label label-success">ฝ่ายสั่งซื้อยืนยัน</span>
                        @elseif($value->PR_order_status=='confirmStore')
                            <span class="label label-primary">ฝ่ายคลังยืนยัน</span>
                        @elseif($value->PR_order_status=='confirmManager')
                            <span class="label label-warning">ผู้จัดการยืนยัน</span>
                        @elseif($value->PR_order_status=='confirm')
                            <span class="label label-danger">ยืนยัน</span>
                        @endif

                        {{--{{\App\Models\PrOrder::checkStatus($value->PR_order_status)}}--}}

                    </td>
                    <td>
                        {{$value->PR_order_staff}}
                    </td>
                    <td>
                        @if($value->PR_order_status!="confirmPurchasing")
                            <a href="/Pr/listPr?id={{$value->PR_order_id}}" class="btn btn-primary">
                                <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> แก้ไข
                            </a>
                            @if(($value->PR_order_status=='confirmPurchasing' or $value->PR_order_status=='confirmManager')and($user->staff_usertype=='1' or $user->staff_usertype=='2' or $user->staff_usertype=='5'))
                                <a href="/Pr/deletePrOrder?id={{$value->PR_order_id}}" onclick="return confirm('ยืนยันการ ยกเลิก ใบ PR {{$value->PR_order_name}} ')" class="btn btn-danger">
                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> ลบ
                                </a>
                            @endif
                            <a href="/Pr/confirmPrOrder?id={{$value->PR_order_id}}" onclick="return confirm('ยืนยัน ใบ PR {{$value->PR_order_name}} ')" class="btn btn-success">
                                <span class="glyphicon glyphicon-check" aria-hidden="true"></span> ตรวจสอบเเล้ว
                            </a>
                        @endif
                        <a target="_blank" href="/Pr/PrintPr?orderId={{$value->PR_order_id}}" class="btn btn-default"><span class="glyphicon glyphicon-print" aria-hidden="true"></span> พิมพ์ใบ Pr</a>
                        <a target="_blank" href="/Pr/PrintPo?orderId={{$value->PR_order_id}}" class="btn btn-default"><span class="glyphicon glyphicon-print" aria-hidden="true"></span> พิมพ์ใบ Po</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="row">
    <div class="col-lg-12" style="text-align: center;">
        <nav>
            <ul class="pagination" style="margin: 0px;padding: 0px;padding-top: 3px;">
                @if($dataPrOrder['page']>1)
                    <li>
                        <a href="/factory/addOrder?page={{$dataPrOrder['page']-1}}@if(false==empty($dataPrOrder['search']))&search={{$dataPrOrder['search']}}@endif">Prev</a>
                    </li>
                @endif

                @for($x = 1; $x <= $dataPrOrder['max']; $x++)
                    @if($x==$dataPrOrder['page'])
                        <li class="active"><a
                                    href="/factory/addOrder?page={{$x}}@if(false==empty($dataPrOrder['search']))&search={{$dataPrOrder['search']}}@endif">{{$x}}</a>
                        </li>
                    @else
                        <li>
                            <a href="/factory/addOrder?page={{$x}}@if(false==empty($dataPrOrder['search']))&search={{$dataPrOrder['search']}}@endif">{{$x}}</a>
                        </li>
                    @endif
                @endfor

                @if($dataPrOrder['page']<$dataPrOrder['max'])
                    <li>
                        <a href="/factory/addOrder?page={{$dataPrOrder['page']+1}}@if(false==empty($dataPrOrder['search']))&search={{$dataPrOrder['search']}}@endif">Next</a>
                    </li>
                @endif
            </ul>
        </nav>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $( "#datepicker" ).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat:'yy-mm-dd'
        });
    });
</script>

<style>
    .ui-datepicker-month,.ui-datepicker-year{
        color: #333;
    }
</style>

<div class="modal fade @if (!empty($PrOrder->PR_order_id) || true==$errors->has('PR_order_name')) in @endif" style=" @if (!empty($PrOrder->PR_order_id) || true==$errors->has('PR_order_name')) display: block @endif" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <form method="POST" action="/Pr/createPr" class="form-horizontal" novalidate>
        <div class="modal-content">
            <div class="modal-header">
                <a href="/Pr/listPr" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">เปิดใบ PR</h4>
            </div>
            <div class="modal-body">
                @if (true==$errors->has('PR_order_name'))
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>
                        @endforeach
                    </div>
                @endif
                <div class="form-horizontal" style="padding: 0px 20px;">
                    <input type="hidden" name="PR_order_id" value="{{$PrOrder->PR_order_id}}">
                    <div class="form-group @if ($errors->has('pr_order_date')) has-error @endif">
                        <label for="exampleInputPassword1">วันที่</label>
                        <input type="text" value="{{$PrOrder->pr_order_date}}" class="form-control" name="pr_order_date" id="datepicker" placeholder="วันที่">
                    </div>
                    <div class="form-group @if ($errors->has('pr_order_tax_number')) has-error @endif">
                        <label for="exampleInputPassword1">เลขประจำตัวผู้เสียภาษี</label>
                        <input type="text" value="{{$PrOrder->pr_order_tax_number}}" class="form-control" name="pr_order_tax_number" id="exampleInputPassword1" placeholder="เลขประจำตัวผู้เสียภาษี">
                    </div>
                    <div class="form-group @if ($errors->has('pr_order_number')) has-error @endif">
                        <label for="exampleInputPassword1">หมายเลขใบสั่งซื้อ</label>
                        <input type="text" value="{{$PrOrder->pr_order_number}}" class="form-control" name="pr_order_number" id="exampleInputPassword1" placeholder="หมายเลขใบเสนอราคา">
                    </div>
                    <div class="form-group @if ($errors->has('PR_order_name')) has-error @endif">
                        <label for="exampleInputEmail1">ชื่อใบ PR <span style="color: #FF0000;">*</span></label>
                        <input value="{{$PrOrder->PR_order_name}}" name="PR_order_name" type="text" class="form-control" id="exampleInputEmail1" placeholder="ชื่อใบ PR">
                    </div>
                    <div class="form-group @if ($errors->has('pr_order_discount')||$errors->has('pr_order_tax')) has-error @endif">
                        {{--<label for="inputPassword3" class="col-sm-2 control-label">ส่วนลด</label>--}}
                        <div class="col-sm-6  @if ($errors->has('pr_order_discount')) has-error @endif" style="padding-left: 0px;">
                            <label for="inputPassword3">ส่วนลด</label>
                            <input type="number" value="{{$PrOrder->pr_order_discount}}" name="pr_order_discount" class="form-control" id="inputPassword3" placeholder="ส่วนลด" value="0">
                        </div>
                        {{--<label for="inputPassword3" class="col-sm-2 control-label">ภาษี</label>--}}
                        <div class="col-sm-6  @if ($errors->has('pr_order_tax')) has-error @endif" style="padding-left: 0px;">
                            <label for="inputPassword3">ภาษี</label>
                            <input type="number" value="{{$PrOrder->pr_order_tax}}" name="pr_order_tax" class="form-control" id="inputPassword3" placeholder="ภาษี" value="0">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="/Pr/listPr" type="button" class="btn btn-default" data-dismiss="modal">Close</a>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        </form>
    </div>
</div>

@stop