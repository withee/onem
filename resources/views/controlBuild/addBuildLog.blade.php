@extends('layouts.layouts')
@section('content')
    <div class="row" style="margin-top: 10px;">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li><a href="/controlBuild/viewControlBuild">จัดการงานผลิต</a></li>
                <li class="active">รายการเบิกสินค้า</li>
            </ol>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <h3>รายการเบิกสินค้า</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12" style="padding-bottom: 10px;">
            <button style="float: right;" type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                เพิ่มกิจกรรม
            </button>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th>รหัสใบเบิก</th>
                    <th>ชื่อ</th>
                    <th>ประเภท</th>
                    <th>รายละเอียด</th>
                    <th>วันที่เริ่ม</th>
                    <th>จัดการ</th>
                </tr>
                </thead>
                <tbody>
                @if(false==empty($dataControlBuild))
                    @foreach($dataControlBuild as $key=>$value)
                        <tr>
                            <th scope="row">{{$key+1}}</th>
                            <td>{{$value->control_build_id}}</td>
                            <td>{{$value->control_build_name}}</td>
                            <td>{{$value->control_build_type}}</td>
                            <td>{{$value->control_build_detail}}</td>
                            <td>{{$value->control_build_create_date}}</td>
                            <td>
                                <a href="/controlBuild/addBuildLog?id={{$value->control_build_id}}" class="btn btn-success">เบิกสินค้าเพื่อผลิต</a>
                                <a href="/controlBuild/addProductDefine?id={{$value->control_build_id}}" class="btn btn-warning">เพิ่มสินค้าที่ต้องการผลิต</a>
                                <a onclick="return confirm('ยืนยันการลบข้อมูล สินค้านี้ ?')" href="/controlBuild/remove?id={{$key}}" class="btn btn-danger" >ปิดโครง</a>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>


@stop