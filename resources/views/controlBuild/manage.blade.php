@extends('layouts.layouts')
@section('content')

    <div class="row" style="margin-top: 10px;">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li><a href="/controlBuild/viewControlBuild">จัดการงานผลิตเเละโครงการ</a></li>
                <li class="active">รายการสินค้า</li>
            </ol>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <h3>สินค้าที่ผลิต</h3>
        </div>
    </div>

    <div class="row" style="padding: 10px;float: right;">
        <div class="col-lg-12">
            <form method="POST" action="/controlBuild/addProductBuild" class="form-inline">
                <input type="hidden" name="control_build_id" value="{{$id}}">
                @if (true==$errors->has('productId')||true==$errors->has('productCount')||true==$errors->has('storeId')||true==$errors->has('productPrice'))
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>
                        @endforeach
                    </div>
                @endif
                <div class="form-group @if ($errors->has('productId')) has-error @endif">
                    <label for="exampleInputName2">ชื่อสินค้า</label>
                    <select name="productId" class="form-control">
                        @foreach($dataProduct as $key=>$value)
                            <option @if (Input::old('productId')==$value['product_id']) selected @endif value="{{$value->product_id}}">{{$value->product_name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group @if ($errors->has('productCount')) has-error @endif">
                    <label for="exampleInputEmail2">จำนวน</label>
                    <input name="productCount" type="number" class="form-control" placeholder="จำนวน" value="@if(false==empty(Input::old('productCount'))){{Input::old('productCount')}}@endif">
                </div>
                <button type="submit" class="btn btn-default">เพิ่มสินค้า</button>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    {{--<th>#</th>--}}
                    <th>รหัสสินค้า</th>
                    <th>ชื่อสินค้า</th>
                    <th>สินค้าที่ต้องผลิต</th>
                    <th>ผลิตเสร็จเเล้ว</th>
                    <th>เปอร์เซ็นต์</th>
                </tr>
                </thead>
                <tbody>
                @if(false==empty($listProduct))
                    @foreach($listProduct['data'] as $key=>$value)
                        <tr>
                            {{--<th scope="row">{{$key+1}}</th>--}}
                            <td>{{$key}}</td>
                            <td>{{$value['product_name']}}</td>
                            <td>{{$value['define']['count']}}
                            <td>{{$value['manufacture']['count']}}</td>
                            <td>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="{{$value['manufacture']['count']}}" aria-valuemin="0" aria-valuemax="{{$value['define']['count']}}" style="width:{{$value['manufacture']['percent']}}%">
                                        {{$value['manufacture']['percent']}}%
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>



@stop