@extends('layouts.layouts')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h3>จัดการงานผลิต</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12" style="padding-bottom: 10px;">
            <button style="float: right;" type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                เพิ่มงานผลิตเเละโครงการ
            </button>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th>รหัส</th>
                    <th>ชื่อ</th>
                    <th>ประเภท</th>
                    <th>รายละเอียด</th>
                    <th>วันที่เริ่ม</th>
                    <th>จัดการ</th>
                </tr>
                </thead>
                <tbody>
                @if(false==empty($dataControlBuild))
                    @foreach($dataControlBuild as $key=>$value)
                        <tr style="background-color: #ebebeb;border-top: solid 2px;">
                            <td scope="row">{{$key+1}}</td>
                            <td>{{$value->control_build_id}}</td>
                            <td>{{$value->control_build_name}}</td>
                            <td>{{$value->control_build_type}}</td>
                            <td>{{$value->control_build_detail}}</td>
                            <td>{{$value->control_build_create_date}}</td>
                            <td>
                                <div class="btn-group" aria-label="Basic example" role="group">
                                    <button onclick="myFunction({{$value->control_build_id}})" class="btn btn-success" data-toggle="modal" data-target="#myModalBring">เบิกสินค้าเพื่อผลิต</button>
                                    {{--<button data-toggle="modal" data-target="#myModalManufacture" class="btn btn-info">รับสินค้าผลิตเสร็จ</button>--}}
                                    <a href="/controlBuild/addProductDefine?id={{$value->control_build_id}}" class="btn btn-warning">สินค้าที่ต้องผลิต</a>
                                    <a onclick="return confirm('ยืนยันการปิด งานผลิตนี้ ?')" href="/controlBuild/remove?id={{$value->control_build_id}}" class="btn btn-danger @if(false==$value->deleted_at) disabled @endif " >ปิดโครง</a>
                                </div>
                            </td>
                        </tr>

                        <tr style="background-color: #cccccc;">
                            <td style="padding: 2px;text-align: center;"></td>
                            <td style="padding: 2px;text-align: center;">รหัสสินค้า</td>
                            <td style="padding: 2px;text-align: center;">ชื่อสินค้า</td>
                            <td style="padding: 2px;text-align: center;">สินค้าที่ต้องผลิต</td>
                            <td style="padding: 2px;text-align: center;">ผลิตเสร็จ/ชำรุด</td>
                            <td style="padding: 2px;text-align: center;">เปอร์เซ็นต์</td>
                            <td>จัดการ</td>
                        </tr>
                        @foreach($value->control_build_complete_date as $kProduct=>$vProduct)
                            <tr class="success">
                                <td></td>
                                <td style="padding: 2px;text-align: center;">{{$kProduct}}</td>
                                <td style="padding: 2px;text-align: center;">{{$vProduct['product_name']}}</td>
                                <td style="padding: 2px;text-align: center;">{{$vProduct['define']['count']}}
                                <td style="padding: 2px;text-align: center;">{{$vProduct['manufacture']['count']}}</td>
                                <td style="padding: 2px;text-align: center;">
                                    <div class="progress" style="margin: 0px;">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="{{$vProduct['manufacture']['count']}}" aria-valuemin="0" aria-valuemax="{{$vProduct['define']['count']}}" style="width:{{$vProduct['manufacture']['percent']}}%">
                                            {{$vProduct['manufacture']['percent']}}%
                                        </div>
                                    </div>
                                </td>
                                <td style="padding: 2px;text-align: center;">
                                    <button onclick="selectProduct('{{$kProduct}}');myFunction({{$value->control_build_id}})" data-toggle="modal" data-target="#myModalManufacture" class="btn btn-success btn-xs">รับสินค้าผลิตเสร็จ</button>
                                    <button onclick="selectProduct('{{$kProduct}}');myFunction({{$value->control_build_id}})" data-toggle="modal" data-target="#myModalDeteriorate" class="btn btn-danger btn-xs">สินค้าชำรุด</button>
                                </td>
                            </tr>
                            @if(false==empty($vProduct['deteriorate']['count']))
                            <tr class="danger">
                                <td></td>
                                <td style="padding: 2px;text-align: center;">{{$kProduct}}</td>
                                <td style="padding: 2px;text-align: center;">{{$vProduct['product_name']}}</td>
                                <td style="padding: 2px;text-align: center;">{{$vProduct['define']['count']}}
                                <td style="padding: 2px;text-align: center;">{{$vProduct['deteriorate']['count']}}</td>
                                <td style="padding: 2px;text-align: center;">
                                    <div class="progress" style="margin: 0px;">
                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="{{$vProduct['manufacture']['count']}}" aria-valuemin="0" aria-valuemax="{{$vProduct['define']['count']}}" style="width:{{$vProduct['deteriorate']['percent']}}%">
                                            {{$vProduct['deteriorate']['percent']}}%
                                        </div>
                                    </div>
                                </td>
                                <td style="padding: 2px;text-align: center;">
                                    {{--<button onclick="selectProduct('{{$kProduct}}');myFunction({{$value->control_build_id}})" data-toggle="modal" data-target="#myModalManufacture" class="btn btn-success btn-xs">รับสินค้าผลิตเสร็จ</button>--}}
                                    {{--<button onclick="selectProduct('{{$kProduct}}');myFunction({{$value->control_build_id}})" data-toggle="modal" data-target="#myModalDeteriorate" class="btn btn-danger btn-xs">สินค้าชำรุด</button>--}}
                                </td>
                            </tr>
                            @endif
                        @endforeach
                        <tr style="background-color: #cccccc;">
                            <td style="padding: 2px;text-align: center;"></td>
                            <td style="padding: 2px;text-align: center;">#</td>
                            <td style="padding: 2px;text-align: center;">วันที่</td>
                            <td style="padding: 2px;text-align: center;">ผู้เบิก/ผู้รับผลิต</td>
                            <td style="padding: 2px;text-align: center;">ประเภท</td>
                            <td style="padding: 2px;text-align: center;">รายละเอียด</td>
                            <td style="padding: 2px;text-align: center;">จัดการ</td>
                        </tr>
                        @foreach($value->lot_No as $klog=>$vlog)
                        <tr style="background-color: #ebebeb;">
                            <td style="padding: 0px;text-align: center;">
                                @if($vlog->control_build_log_type=='bring')
                                    <span class="glyphicon glyphicon-save" aria-hidden="true"></span>
                                @elseif($vlog->control_build_log_type=='manufacture')
                                    <span class="glyphicon glyphicon-open" aria-hidden="true"></span>
                                @elseif($vlog->control_build_log_type=='deteriorate')
                                    <span class="glyphicon glyphicon-fire" aria-hidden="true"></span>
                                @endif
                            </td>
                            <td style="padding: 0px;text-align: center;">{{$klog+1}}</td>
                            <td style="padding: 0px;text-align: center;">{{$vlog->created_at}}</td>
                            <td style="padding: 0px;text-align: center;">{{$vlog->staff_firstname}} {{$vlog->staff_lastname}}</td>
                            <td style="padding: 0px;text-align: center;">
                                @if($vlog->control_build_log_type=='bring')
                                    เบิกสินค้า
                                @elseif($vlog->control_build_log_type=='deteriorate')
                                    ความเสียหาย
                                @elseif($vlog->control_build_log_type=='manufacture')
                                    ผลิตเสร็จ
                                @endif
                            </td>
                            <td style="padding: 0px;text-align: center;">{{$vlog->control_build_log_detail}}</td>
                            <td style="padding: 2px;text-align: center;">
                                @if($vlog->control_build_log_type=='bring')
                                <a href="/bring/viewProduct?id={{$vlog->bring_id}}" class="btn btn-warning btn-xs">เบิกเพิ่มสินค้า</a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                        <tr><td colspan="7"></td></tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>

    <script>
        function myFunction(data) {
            $('input#control_build_id').val(data);
            $('input#manufacture_control_build_id').val(data);
            $('input#deteriorate_control_build_id').val(data);
        }
        function selectProduct(data){
            $('select#manufactureProduct_id').val(data);
            $('select#deteriorateProduct_id').val(data);
        }
    </script>

    <div class="modal fade @if(true==$errors->has('deteriorateProduct_id')||true==$errors->has('deteriorateProduct_count')||true==$errors->has('deteriorateProduct_detail')) in @endif" style="@if(true==$errors->has('deteriorateProduct_id')||true==$errors->has('deteriorateProduct_count')||true==$errors->has('deteriorateProduct_detail')) display: block; @endif" id="myModalDeteriorate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <form method="POST" action="/controlBuild/addProductDeteriorate" >
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">สินค้าชำรุด</h4>
                    </div>
                    <div class="modal-body">
                        @if ($errors->has())
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </div>
                        @endif
                        <div class="form-horizontal">
                            <input type="hidden" id="deteriorate_control_build_id" value="@if(false==empty(Input::old('deteriorate_control_build_id'))){{Input::old('deteriorate_control_build_id')}}@endif" name="deteriorate_control_build_id">
                            <div class="form-group @if ($errors->has('deteriorateProduct_id')||$errors->has('deteriorateProduct_count')) has-error @endif">
                                <label for="exampleInputName2" class="col-lg-3 control-label">สินค้าชำรุด</label>
                                <div class="col-lg-5" style="display: none;">
                                    <select readonly name="deteriorateProduct_id" id="deteriorateProduct_id" class="form-control">
                                        @foreach($dataProduct as $key=>$value)
                                            <option @if (Input::old('deteriorateProduct_id')==$value['product_id']) selected @endif value="{{$value->product_id}}">{{$value->product_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-3" style="">
                                    <input type="text" value="@if(false==empty(Input::old('deteriorateProduct_count'))){{Input::old('deteriorateProduct_count')}}@endif" name="deteriorateProduct_count" class="form-control" id="inputEmail3" placeholder="จำนวน">
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('deteriorateProduct_detail')) has-error @endif">
                                <label class="col-lg-3 control-label" for="exampleInputEmail1">สาเหตุ</label>
                                <div class="col-lg-8" style="">
                                    <textarea rows="4" class="form-control" value="@if(false==empty(Input::old('deteriorateProduct_detail'))){{Input::old('deteriorateProduct_detail')}}@endif" name="deteriorateProduct_detail" placeholder="สาเหตุ"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        {{--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                        <button style="width: 100%;" type="submit" class="btn btn-primary">เพิ่ม</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="modal fade @if(true==$errors->has('manufactureProduct_id')||true==$errors->has('manufactureProduct_count')) in @endif" style="@if(true==$errors->has('manufactureProduct_id')||true==$errors->has('manufactureProduct_count')) display: block; @endif" id="myModalManufacture" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <form method="POST" action="/controlBuild/addProductManufacture" >
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">สินค้าผลิตเสร็จ</h4>
                    </div>
                    <div class="modal-body">
                        @if ($errors->has())
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </div>
                        @endif
                        <div class="form-horizontal">
                            <input type="hidden" id="manufacture_control_build_id" value="@if(false==empty(Input::old('manufacture_control_build_id'))){{Input::old('manufacture_control_build_id')}}@endif" name="manufacture_control_build_id">
                            <div class="form-group @if ($errors->has('manufactureProduct_id')||$errors->has('manufactureProduct_count')) has-error @endif">
                                <label for="exampleInputName2" class="col-lg-3 control-label">สินค้าผลิตเสร็จ</label>
                                <div class="col-lg-5" style="display: none">
                                    <select readonly name="manufactureProduct_id" id="manufactureProduct_id" class="form-control">
                                        @foreach($dataProduct as $key=>$value)
                                            <option @if (Input::old('manufactureProduct_id')==$value['product_id']) selected @endif value="{{$value->product_id}}">{{$value->product_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-3" style="padding: 0px;">
                                    <input type="text" value="@if(false==empty(Input::old('manufactureProduct_count'))){{Input::old('manufactureProduct_count')}}@endif" name="manufactureProduct_count" class="form-control" id="inputEmail3" placeholder="จำนวน">
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        {{--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                        <button style="width: 100%;" type="submit" class="btn btn-primary">เพิ่ม</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="modal fade @if(true==$errors->has('control_build_create_date')||true==$errors->has('control_build_name')) in @endif" style="@if(true==$errors->has('control_build_create_date')||true==$errors->has('control_build_name')) display: block; @endif" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <form method="POST" action="/controlBuild/addControlBuild" >
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">กรอกรายละเอียดงานผลิต</h4>
                    </div>
                    <div class="modal-body">
                        @if ($errors->has())
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </div>
                        @endif
                        <div class="form-horizontal">
                            <div class="form-group @if($errors->has('control_build_create_date')) has-error @endif">
                                <label for="inputEmail3" class="col-lg-3 control-label">วันที่เริ่มผลิต</label>
                                <div class="col-lg-8">
                                    <input type="text" id="control_build_create_date" value="@if(false==empty(Input::old('control_build_create_date'))){{Input::old('control_build_create_date')}}@endif" name="control_build_create_date" class="form-control" id="inputEmail3" placeholder="วันที่เริ่มผลิต">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-lg-3 control-label">ประเภท</label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox1" name="control_build_type_Mf" /> ผลิต
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox2" name="control_build_type_IS" /> ติดตั้ง
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox3" name="control_build_type_OT" /> อื่น
                                </label>
                            </div>
                            <div class="form-group @if($errors->has('control_build_name')) has-error @endif">
                                <label for="inputEmail3" class="col-lg-3 control-label">ชื่องานผลิต</label>
                                <div class="col-lg-8">
                                    <input type="text" value="@if(false==empty(Input::old('control_build_name'))){{Input::old('control_build_name')}}@endif" name="control_build_name" class="form-control" id="inputEmail3" placeholder="ชื่องานผลิต">
                                </div>
                            </div>

                            <div class="form-group @if ($errors->has('product_id')||$errors->has('product_count')) has-error @endif">
                                <label for="exampleInputName2" class="col-lg-3 control-label">สินค้าที่ต้องผลิต</label>
                                <div class="col-lg-5" style="">
                                    <select name="product_id" class="form-control">
                                        @foreach($dataProduct as $key=>$value)
                                            <option @if (Input::old('product_id')==$value['product_id']) selected @endif value="{{$value->product_id}}">{{$value->product_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-2" style="padding: 0px;">
                                    <input type="text" value="@if(false==empty(Input::old('product_count'))){{Input::old('product_count')}}@endif" name="product_count" class="form-control" id="inputEmail3" placeholder="จำนวน">
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('control_build_detail')) has-error @endif">
                                <label class="col-lg-3 control-label" for="exampleInputEmail1">รายละเอียด</label>
                                <div class="col-lg-8" style="">
                                    <textarea rows="4" class="form-control" value="@if(false==empty(Input::old('control_build_detail'))){{Input::old('control_build_detail')}}@endif" name="control_build_detail" placeholder="รายละเอียด"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        {{--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                        <button style="width: 100%;" type="submit" class="btn btn-primary">เพิ่ม</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="modal fade @if(true==$errors->has('bring_number')||true==$errors->has('bring_date')||true==$errors->has('member_bring_date')||true==$errors->has('id_member_bring')) in @endif" style="@if(true==$errors->has('bring_number')||true==$errors->has('bring_date')||true==$errors->has('member_bring_date')||true==$errors->has('id_member_bring')) display: block; @endif" id="myModalBring" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <form method="POST" action="/bring/addBring" >
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">กรอกรายละเอียดใบเบิกสินค้า</h4>
                    </div>
                    <div class="modal-body">
                        @if ($errors->has())
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </div>
                        @endif
                        <div class="form-horizontal">
                            <input id="control_build_id" type="hidden" name="control_build_id" value="">
                            <div class="form-group @if($errors->has('bring_number')) has-error @endif">
                                <label for="inputEmail3" class="col-lg-3 control-label">หมายเลขใบเบิก</label>
                                <div class="col-lg-9">
                                    <input type="text" value="@if(false==empty(Input::old('bring_number'))){{Input::old('bring_number')}}@endif" name="bring_number" class="form-control" id="inputEmail3" placeholder="หมายเลขใบเบิก">
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('bring_date')) has-error @endif">
                                <label for="inputEmail3" class="col-lg-3 control-label">วันที่เบิก</label>
                                <div class="col-lg-9">
                                    <input type="text" id="bring_date" value="@if(false==empty(Input::old('bring_date'))){{Input::old('bring_date')}}@endif" name="bring_date" class="form-control" id="inputEmail3" placeholder="วันที่เบิก">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-lg-3 control-label">ประเภท</label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox1" name="bring_type_manufacture" > สำหรับใช้ในงานผลิต
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox2" name="bring_type_project" > สำหรับใช้ในโครงการ
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox3" name="bring_type_other" > อื่น
                                </label>
                            </div>

                            {{--************************รายชื่อผู้เบิกของ*******************--}}
                            <div class="form-group @if($errors->has('id_member_bring')) has-error @endif">
                                <label for="inputPassword3" class="col-lg-3 control-label">ผู้ขอเบิก</label>
                                <div class="col-lg-5">
                                    <select class="form-control" name="id_member_bring">
                                        <option value="">เลือกผู้ขอเบิก</option>
                                        @foreach($dataStaff as $key=>$value)
                                            <option  @if (Input::old('id_member_bring')==$value->staff_id) selected @endif value="{{$value->staff_id}}">{{$value->staff_firstname}} {{$value->staff_lastname}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4 @if($errors->has('member_bring_date')) has-error @endif" style="">
                                    <input type="text"  value="@if(false==empty(Input::old('member_bring_date'))){{Input::old('member_bring_date')}}@endif" name="member_bring_date" class="form-control" id="member_bring_date" placeholder="วันที่ผู้ขอเบิก">
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('id_member_approve_bring')) has-error @endif">
                                <label for="inputPassword3" class="col-lg-3 control-label">ผู้อนุมัตเบิก</label>
                                <div class="col-lg-5">
                                    <select class="form-control" name="id_member_approve_bring">
                                        <option value="">เลือกผู้อนุมัตเบิก</option>
                                        @foreach($dataStaff as $key=>$value)
                                            <option  @if (Input::old('id_member_approve_bring')==$value->staff_id) selected @endif value="{{$value->staff_id}}">{{$value->staff_firstname}} {{$value->staff_lastname}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4 @if($errors->has('member_approve_bring_date')) has-error @endif" style="">
                                    <input type="text"  value="@if(false==empty(Input::old('member_approve_bring_date'))){{Input::old('member_approve_bring_date')}}@endif" name="member_approve_bring_date" class="form-control" id="member_approve_bring_date" placeholder="วันที่อนุมัติเบิก">
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('id_member_approve_pay')) has-error @endif">
                                <label for="inputPassword3" class="col-lg-3 control-label">ผู้อนุมัติจ่าย</label>
                                <div class="col-lg-5">
                                    <select class="form-control" name="id_member_approve_pay">
                                        <option value="">เลือกผู้อนุมัติจ่าย</option>
                                        @foreach($dataStaff as $key=>$value)
                                            <option  @if (Input::old('id_member_approve_pay')==$value->staff_id) selected @endif value="{{$value->staff_id}}">{{$value->staff_firstname}} {{$value->staff_lastname}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4 @if($errors->has('member_approve_pay_date')) has-error @endif" style="">
                                    <input type="text"  value="@if(false==empty(Input::old('member_approve_pay_date'))){{Input::old('member_approve_pay_date')}}@endif" name="member_approve_pay_date" class="form-control" id="member_approve_pay_date" placeholder="วันที่อนุมัติจ่าย">
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('id_member_record')) has-error @endif">
                                <label for="inputPassword3" class="col-lg-3 control-label">เจ้าหน้าที่คลัง</label>
                                <div class="col-lg-5">
                                    <select class="form-control" name="id_member_store">
                                        <option value="">เลือกเจ้าหน้าที่คลัง</option>
                                        @foreach($dataStaff as $key=>$value)
                                            <option  @if (Input::old('id_member_store')==$value->staff_id) selected @endif value="{{$value->staff_id}}">{{$value->staff_firstname}} {{$value->staff_lastname}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4  @if($errors->has('member_store_date')) has-error @endif" style="">
                                    <input type="text" value="@if(false==empty(Input::old('member_store_date'))){{Input::old('member_store_date')}}@endif" name="member_store_date" class="form-control" id="member_store_date" placeholder="วันที่จ่ายของ">
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('id_member_account')) has-error @endif">
                                <label for="inputPassword3" class="col-lg-3 control-label">บัญชี</label>
                                <div class="col-lg-5">
                                    <select class="form-control" name="id_member_account">
                                        <option value="">เลือกฝ่ายบัญชี</option>
                                        @foreach($dataStaff as $key=>$value)
                                            <option  @if (Input::old('id_member_account')==$value->staff_id) selected @endif value="{{$value->staff_id}}">{{$value->staff_firstname}} {{$value->staff_lastname}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4 @if($errors->has('member_account_date')) has-error @endif" style="">
                                    <input type="text" value="@if(false==empty(Input::old('member_account_date'))){{Input::old('member_account_date')}}@endif" name="member_account_date" class="form-control" id="member_account_date" placeholder="วันที่ตรวจสอบ">
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('bring_detail')) has-error @endif">
                                <label class="col-lg-3 control-label" for="exampleInputEmail1">รายละเอียด</label>
                                <div class="col-lg-9" style="">
                                    <textarea rows="4" class="form-control" value="@if(false==empty(Input::old('bring_detail'))){{Input::old('bring_detail')}}@endif" name="bring_detail" placeholder="รายละเอียด"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        {{--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                        <button style="width: 100%;" type="submit" class="btn btn-primary">เพิ่ม</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop