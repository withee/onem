@extends('layouts.layouts')
@section('content')
    <div class="row" style="padding: 2%">
        <form class="form-horizontal" role="form" name="formaddcontractor" id="add-form" method="POST"
              action="/staffs/edit">
            <input type="hidden" value="{{ $user->staff_id }}" name="staff_id"/>

            <div class="form-group">
                <label class="col-sm-3 control-label">ชื่อเข้าระบบ <font color="red">*</font> </label>

                <div class="col-sm-9">
                    <input type="text" class="form-control" name="staff_username" value="{{ $user->staff_username }}"
                           placeholder="User name" disabled>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">รหัสผ่าน <font color="red">*</font> </label>

                <div class="col-sm-9">
                    <input type="password" class="form-control" value="{{ $user->staff_userpwd }}" name="staff_userpwd"
                           placeholder="Password">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">รหัสผ่านอีกครั้ง <font color="red">*</font></label>

                <div class="col-sm-9">
                    <input type="password" class="form-control" name="staff_userpwd_confirmation"
                           value="{{ $user->staff_userpwd }}"
                           placeholder="Password">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">ชื่อ <font color="red">*</font> </label>

                <div class="col-sm-9">
                    <input type="text" class="form-control" name="staff_firstname" value="{{ $user->staff_firstname }}"
                           placeholder="ชื่อ">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">นามสกุล <font color="red">*</font> </label>

                <div class="col-sm-9">
                    <input type="text" class="form-control" name="staff_lastname" value="{{ $user->staff_lastname }}"
                           placeholder="นามสกุล">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">ชื่อเล่น <font color="red">*</font> </label>

                <div class="col-sm-9">
                    <input type="text" class="form-control" name="staff_nickname" value="{{ $user->staff_nickname }}"
                           placeholder="ชื่อเล่น">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">อีเมล <font color="red">*</font> </label>

                <div class="col-sm-9">
                    <input type="text" class="form-control" name="staff_email" value="{{ $user->staff_email }}"
                           placeholder="อีเมล">
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword" class="col-sm-3 control-label">เบอร์โทร <font color="red">*</font> </label>

                <div class="col-sm-4">
                    <input type="text" class="form-control" name="staff_phonenumber"
                           value="{{ $user->staff_phonenumber }}" placeholder="เบอร์โทร">
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword" class="col-sm-3 control-label">ประเภท <font color="red">*</font> </label>

                <div class="col-sm-4">
                    <select class="form-control selectpicker" name="staff_usertype">
                        @foreach ($usertype as $item)
                            <option value="{{ $item['role_id'] }}"
                                    class="type-list">{{ $item['role_description'] }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group" id="wrong">
                <label class="col-sm-3 control-label">แจ้งเตือน </label>

                <div class="col-sm-9" id="error-report">
                    @if ($errors->has())
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label"></label>

                <div class="col-sm-4">
                    <button type="submit" class="btn btn-success">เพิ่ม</button>
                    <a href="/staffs/manage" class="btn btn-default" id="submit_cancel" parent="addstaff">ยกเลิก</a>
                </div>
            </div>
        </form>
    </div>
@stop