@extends('layouts.layouts')
@section('content')
    <div class="row" style="padding: 2%">
        <form class="form-horizontal" role="form" name="formaddcontractor" id="add-form" method="POST"
              action="/staffs/add">
            <div class="form-group">
                <label class="col-sm-3 control-label">ชื่อเข้าระบบ <font color="red">*</font> </label>

                <div class="col-sm-9">
                    <input type="text" class="form-control" name="staff_username" value="@if(!empty(Input::old('staff_username'))){{ Input::old('staff_username')}}@endif" placeholder="User name">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">รหัสผ่าน <font color="red">*</font> </label>

                <div class="col-sm-9">
                    <input type="password" class="form-control" name="staff_userpwd" placeholder="Password">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">รหัสผ่านอีกครั้ง <font color="red">*</font></label>

                <div class="col-sm-9">
                    <input type="password" class="form-control" name="staff_userpwd_confirmation"
                           placeholder="Password">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">ชื่อ <font color="red">*</font> </label>

                <div class="col-sm-9">
                    <input type="text" class="form-control" name="staff_firstname" value="@if(!empty(Input::old('staff_firstname'))){{ Input::old('staff_firstname')}}@endif" placeholder="ชื่อ">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">นามสกุล <font color="red">*</font> </label>

                <div class="col-sm-9">
                    <input type="text" class="form-control" name="staff_lastname" value="@if(!empty(Input::old('staff_lastname'))){{ Input::old('staff_lastname')}}@endif" placeholder="นามสกุล">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">ชื่อเล่น <font color="red">*</font> </label>

                <div class="col-sm-9">
                    <input type="text" class="form-control" name="staff_nickname" value="@if(!empty(Input::old('staff_nickname'))){{ Input::old('staff_nickname')}}@endif" placeholder="ชื่อเล่น">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">อีเมล <font color="red">*</font> </label>

                <div class="col-sm-9">
                    <input type="text" class="form-control" name="staff_email" value="@if(!empty(Input::old('staff_email'))){{ Input::old('staff_email')}}@endif" placeholder="อีเมล">
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword" class="col-sm-3 control-label">เบอร์โทร <font color="red">*</font> </label>

                <div class="col-sm-4">
                    <input type="text" class="form-control" name="staff_phonenumber" value="@if(!empty(Input::old('staff_phonenumber'))){{ Input::old('staff_phonenumber')}}@endif" placeholder="เบอร์โทร">
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword" class="col-sm-3 control-label">ประเภท <font color="red">*</font> </label>

                <div class="col-sm-4">
                    <select class="form-control selectpicker" name="staff_usertype">
                        @foreach ($usertype as $item)
                            <option value="{{ $item['role_id'] }}"
                                    class="type-list">{{ $item['role_description'] }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group" id="wrong">
                <label class="col-sm-3 control-label">แจ้งเตือน </label>

                <div class="col-sm-9" id="error-report">
                    @if ($errors->has())
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label"></label>

                <div class="col-sm-4">
                    <button type="submit" class="btn btn-success">เพิ่ม</button>
                    <button type="button" class="btn btn-default" id="submit_cancel" parent="addstaff">ยกเลิก</button>
                </div>
            </div>
        </form>
    </div>
@stop