@extends('layouts.layouts')
@section('content')
<div id="content">
    <div class="ng-scope" ng-controller="membercustomer">
        <div class="row">
            <h3>จัดการข้อมูลบุคลากร</h3>
            <div>
                <div class="form-horizontal" style="margin-left: 70%;">
                    <div class="form-group">
                        <div class="col-sm-7">
                            <input ng-model="search" class="form-control ng-pristine ng-valid" id="inputSearchName" placeholder="ชื่อ" type="text">
                        </div>
                        <div class="col-sm-3">
                           <a class="btn btn-success" id="addManage"><span class="glyphicon glyphicon-plus"></span> เพิ่ม</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="modal fade" id="addstaff" ng-class="(modalAddContractor)?'show':''">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" parent="addstaff" ng-click="modalAddContractor=false" >&times;</button>
                            <h4 class="modal-title" id="add_title">บุคลากร</h4>


                        </div>

                        <div class="modal-body">
                            <form class="form-horizontal" role="form" name="formaddcontractor" id="add-form" novalidate>
                                <input type="hidden" ng-model="contractor.contractor_id" id="staff_id" name="staff_id" value="" />
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">ชื่อเข้าระบบ <font color="red">*</font> </label>
                                    <div class="col-sm-9">
                                        <input required ng-model="contractor.contractor_name" type="text" class="form-control" id="add_username" name="staff_username" placeholder="User name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">รหัสผ่าน <font color="red">*</font> </label>
                                    <div class="col-sm-9">
                                        <input required ng-model="contractor.contractor_name" type="password" class="form-control" id="add_userpwd" name="staff_userpwd" placeholder="Password">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">รหัสผ่านอีกครั้ง <font color="red">*</font> </label>
                                    <div class="col-sm-9">
                                        <input required ng-model="contractor.contractor_name" type="password" class="form-control" id="add_userrepwd" name="staff_userpwd_confirmation" placeholder="Password">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">ชื่อ <font color="red">*</font> </label>
                                    <div class="col-sm-9">
                                        <input required ng-model="contractor.contractor_name" type="text" class="form-control" id="add_name" name="staff_firstname" placeholder="ชื่อ">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">นามสกุล <font color="red">*</font> </label>
                                    <div class="col-sm-9">
                                        <input required ng-model="contractor.contractor_name" type="text" class="form-control" id="add_surname" name="staff_lastname" placeholder="นามสกุล">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">ชื่อเล่น <font color="red">*</font> </label>
                                    <div class="col-sm-9">
                                        <input required ng-model="contractor.contractor_name" type="text" class="form-control" id="add_nickname" name="staff_nickname"  placeholder="ชื่อเล่น">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">อีเมล <font color="red">*</font> </label>
                                    <div class="col-sm-9">
                                        <input required ng-model="contractor.contractor_name" type="text" class="form-control" id="add_email" name="staff_email" placeholder="อีเมล">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword" class="col-sm-3 control-label">เบอร์โทร <font color="red">*</font> </label>
                                    <div class="col-sm-4">
                                        <input required ng-model="contractor.contractor_phone" type="text"  class="form-control" id="add_phone" name="staff_phonenumber" placeholder="เบอร์โทร">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword" class="col-sm-3 control-label">ประเภท <font color="red">*</font> </label>
                                    <div class="col-sm-4">
                                        <select class="form-control selectpicker" id="changed-type" name="staff_usertype">
                                            @foreach ($usertype as $item)
                                                <option value="{{ $item['role_id'] }}" class="type-list">{{ $item['role_description'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group hide" id="wrong">
                                    <label class="col-sm-3 control-label">แจ้งเตือน </label>
                                    <div class="col-sm-9" id="error-report" style="background-color: #dca7a7">

                                    </div>
                                </div>
                            </form>

                        </div>
                        <div class="modal-footer">
                            <button ng-if="contractor.contractor_id==''" ng-disabled="formaddcontractor.$invalid || isUnchanged(contractor)" ng-click="save(contractor)" type="" class="btn btn-success" id="submit_add">เพิ่ม</button>
                            <button ng-if="contractor.contractor_id!=''" ng-disabled="formaddcontractor.$invalid || isUnchanged(contractor)" ng-click="save(contractor)" type="button" class="btn btn-primary" id="submit_edit">แก้ไข</button>
                            <button ng-if="contractor.contractor_id!=''" ng-disabled="formaddcontractor.$invalid || isUnchanged(contractor)" ng-click="save(contractor)" type="button" class="btn btn-default" id="submit_cancel" parent="addstaff">ยกเลิก</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <table class="table table-bordered tableBlog">
                <thead>
                <tr>
                    <th style="width: 5%; text-align: center">#</th>
                    <th style="width: 10%; text-align: center">ชื่อเข้าใช้ระบบ</th>
                    <th style="width: 15%; text-align: center">ชื่อ-นามสกุล</th>
                    <th style="width:5%;text-align: center">ชื่อเล่น</th>
                    <th style="width: 10%; text-align: center">เบอร์โทรศัพท์</th>
                    <th style="width: 10%; text-align: center">อีเมล</th>
                    <th style="width: 13%; text-align: center">แผนก</th>
                    <th style="width: 17%; text-align: center">จัดการ</th>
                </tr>
                </thead>
                <tbody id="tbodyContent">
                <!-- ngRepeat: (memberK,memberV) in membercustomer | filter:{member_cu_sname:search} -->
                @foreach($paging as $user)
                <tr class="arrUser ng-scope" ng-repeat="(memberK,memberV) in membercustomer | filter:{member_cu_sname:search}">
                    <td class="ng-binding" style="text-align: center;">{{ $user['staff_id'] }}</td>
                    <td class="ng-binding">{{ $user['staff_username'] }}</td>
                    <td class="ng-binding">{{ $user['staff_firstname'] }} {{ $user['staff_lastname'] }}</td>
                    <td class="ng-binding">{{ $user['staff_nickname'] }}</td>
                    <td class="ng-binding">{{ $user['staff_phonenumber'] }}</td>
                    <td class="ng-binding">{{ $user['staff_email'] }}</td>
                    <td class="ng-binding">{{ $usertype[$user['staff_usertype']]['role_name'] }}</td>
                    <td style="text-align: center;">
                        <a class="btn btn-primary manage_Edit" ng-click="edit(memberV)" uid="{{ $user['staff_id'] }}">แก้ไข</a>
                        <a class="btn btn-danger manage_Delete remove_staff" ng-click="delete(memberV)" uid="{{ $user['staff_id'] }}">ลบ</a>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="row" style="">
            <div class="pagination"> {!! $paging->render() !!} </div>
        </div>

        {{--<div class="modal-backdrop ng-hide" ng-show="(loadingmain)" style="z-index: 1800;background-color:#fff;opacity: 0.9;">--}}
            {{--<div style="margin-top: 10%;margin-left: 45%;opacity: 0.8;">--}}
                {{--<img style="width:150px;height: 150px;" src="/images/loading.gif">--}}
            {{--</div>--}}
        {{--</div>--}}
    </div>
</div>

@stop