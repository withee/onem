@extends('layouts.layouts')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h3>จัดการใบคืนสินค้า</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-3" style="padding: 0px;margin: 0px;">
            <form style="float: left;" class="form-inline" method="GET" action="/return/viewReturn" novalidate>
                <div class="input-group input-group-sm">
                    <span class="input-group-addon" ><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></span>
                    <input type="text" class="form-control" name="search" value="{{$dataReturn['search']}}" placeholder="ค้นหา หมายเลขใบคืนสินค้า">
                </div>
                <button type="submit"  class="btn btn-sm"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
            </form>
        </div>
        <div class="col-lg-9" style="padding-bottom: 10px;">
            <button style="float: right;" type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                เพิ่มใบคืนสินค้า
            </button>
        </div>
    </div>

    <div class="row" style="float: right;">
        <div class="col-lg-12">
            <nav>
                <ul class="pagination" style="margin: 0px;padding: 0px;padding-top: 3px;">
                    @if($dataReturn['page']>1)
                        <li><a href="/return/viewReturn?page={{$dataReturn['page']-1}}@if(false==empty($dataReturn['search']))&search={{$dataReturn['search']}}@endif">Prev</a></li>
                    @endif

                    @for($x = 1; $x <= $dataReturn['maxProduct']; $x++)
                        @if($x==$dataReturn['page'])
                            <li class="active" ><a href="/return/viewReturn?page={{$x}}@if(false==empty($dataReturn['search']))&search={{$dataReturn['search']}}@endif">{{$x}}</a></li>
                        @else
                            <li ><a href="/return/viewReturn?page={{$x}}@if(false==empty($dataReturn['search']))&search={{$dataReturn['search']}}@endif">{{$x}}</a></li>
                        @endif
                    @endfor

                    @if($dataReturn['page']<$dataReturn['maxProduct'])
                        <li><a href="/return/viewReturn?page={{$dataReturn['page']+1}}@if(false==empty($dataReturn['search']))&search={{$dataReturn['search']}}@endif">Next</a></li>
                    @endif
                </ul>
            </nav>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#
                    <th>วันที่</th>
                    <th>รหัสใบรับสินค้า</th>
                    <th>ผู้คืนสินค้า</th>
                    <th>รายละเอียด</th>
                    <th>ประเภท</th>
                    <th>จัดการ</th>
                </tr>
                </thead>
                <tbody>
                @if(false==empty($dataReturn))
                    @foreach($dataReturn['dataProduct'] as $key=>$value)
                        <tr>
                            <th scope="row">{{$key+1}}</th>
                            <td>{{$value->return_date}}</td>
                            <td>{{$value->return_number}}</td>
                            <td>{{$value->staff_firstname}} {{$value->staff_lastname}}</td>
                            <td>{{$value->return_detail}}</td>
                            <td>
                                @if($value->return_type_manufacture=='Y')
                                    ใช้เพื่อผลิต
                                @endif
                                @if($value->return_type_project=='Y')
                                    ,ใช้เพื่อโครงการ
                                @endif
                                @if($value->return_type_other=='Y')
                                    ,อื่น
                                @endif
                            </td>
                            <td>
                                <a href="/return/viewProduct?id={{$value->return_id}}" class="btn btn-success" >เพิ่มสินค้า</a>
                                <a href="/return/viewReturn?id={{$value->return_id}}" class="btn btn-warning" >เพิ่มข้อมูลใบคืนสินค้า</a>
                                {{--<a onclick="return confirm('ยืนยันการลบข้อมูล สินค้านี้ ?')" href="/purchases/remove?id={{$key}}" class="btn btn-danger" >ลบ</a>--}}
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade @if(false==empty($Return['return_id'])||true==$errors->has('return_number')||true==$errors->has('return_date')||true==$errors->has('member_return_date')||true==$errors->has('id_member_return')) in @endif" style="@if(false==empty($Return['return_id'])||true==$errors->has('return_number')||true==$errors->has('return_date')||true==$errors->has('member_return_date')||true==$errors->has('id_member_return')) display: block; @endif" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <form method="POST" action="/return/addReturn" >
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <a href="/return/viewReturn" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                        <h4 class="modal-title" id="myModalLabel">กรอกรายละเอียดใบคืนสินค้า</h4>
                    </div>
                    <div class="modal-body">
                        @if ($errors->has())
                            <div class="alert alert-danger" style="padding: 0px;">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}
                                @endforeach
                            </div>
                        @endif
                        <div class="form-horizontal">
                            <input type="hidden" name="return_id" value="@if(false==empty(Input::old('return_id'))){{Input::old('return_id')}}@elseif($Return['return_id']){{$Return['return_id']}}@endif">
                            <div class="form-group @if($errors->has('return_number')) has-error @endif">
                                <label for="inputEmail3" class="col-lg-3 control-label">เลขใบคืนสินค้า</label>
                                <div class="col-lg-9">
                                    {{Input::old('return_number')}}
                                    <input type="text" value="@if(false==empty(Input::old('return_number'))){{Input::old('return_number')}}@elseif($Return['return_number']){{$Return['return_number']}}@endif" name="return_number" class="form-control" id="inputEmail3" placeholder="หมายเลขใบคืน">
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('bring_number')) has-error @endif">
                                <label for="inputEmail3" class="col-lg-3 control-label">หมายเลขใบเบิก</label>
                                <div class="col-lg-9">

                                    {{--<select class="form-control" name="bring_number">--}}
                                        {{--<option value="">เลือกรหัสใบเบิก</option>--}}
                                        {{--@foreach($dataBring as $key=>$value)--}}
                                            {{--<option  @if (Input::old('bring_number')==$value->bring_number||(false==empty($Return['bring_number'])&&$Return['bring_number']==$value->bring_number)) selected @endif value="{{$value->bring_number}}">{{$value->bring_number}}</option>--}}
                                        {{--@endforeach--}}
                                    {{--</select>--}}
                                    <input type="text" value="@if(false==empty(Input::old('bring_number'))){{Input::old('bring_number')}}@endif" name="bring_number" class="form-control" id="inputEmail3" placeholder="หมายเลขใบเบิก">
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('return_date')) has-error @endif">
                                <label for="inputEmail3" class="col-lg-3 control-label">วันที่คืน</label>
                                <div class="col-lg-9">
                                    <input type="text" id="return_date" value="@if(false==empty(Input::old('return_date'))){{Input::old('return_date')}}@elseif($Return['return_date']){{$Return['return_date']}}@endif" name="return_date" class="form-control" id="inputEmail3" placeholder="วันที่เบิก">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-lg-3 control-label">ประเภท</label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" @if(false==empty(Input::old('return_type_manufacture'))) checked @elseif(false==empty($Return['return_type_manufacture'])&&$Return['return_type_manufacture']=='Y') checked @endif id="inlineCheckbox1" name="return_type_manufacture" > สำหรับใช้ในงานผลิต
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" @if(false==empty(Input::old('return_type_project'))) checked @elseif(false==empty($Return['return_type_project'])&&$Return['return_type_project']=='Y') checked @endif id="inlineCheckbox2" name="return_type_project" > สำหรับใช้ในโครงการ
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" @if(false==empty(Input::old('return_type_other'))) checked @elseif(false==empty($Return['return_type_other'])&&$Return['return_type_other']=='Y') checked @endif id="inlineCheckbox3" name="return_type_other" > อื่น
                                </label>
                            </div>

                            {{--************************รายชื่อผู้เบิกของ*******************--}}
                            <div class="form-group @if($errors->has('id_member_return')) has-error @endif">
                                <label for="inputPassword3" class="col-lg-3 control-label">ผู้คืนสินค้า</label>
                                <div class="col-lg-5">
                                    <select class="form-control" name="id_member_return">
                                        <option value="">เลือกผู้คืนสินค้า</option>
                                        @foreach($dataStaff as $key=>$value)
                                            <option  @if (Input::old('id_member_return')==$value->staff_id||(false==empty($Return['id_member_return'])&&$Return['id_member_return']==$value->staff_id)) selected @endif value="{{$value->staff_id}}">{{$value->staff_firstname}} {{$value->staff_lastname}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4 @if($errors->has('member_return_date')) has-error @endif" style="">
                                    <input type="text"  value="@if(false==empty(Input::old('member_return_date'))){{Input::old('member_return_date')}}@elseif($Return['member_return_date']){{$Return['member_return_date']}}@endif" name="member_return_date" class="form-control" id="member_return_date" placeholder="วันที่ผู้ขอเบิก">
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('id_member_approve_return')) has-error @endif">
                                <label for="inputPassword3" class="col-lg-3 control-label">ผู้ตรวจนับ</label>
                                <div class="col-lg-5">
                                    <select class="form-control" name="id_member_approve_return">
                                        <option value="">เลือกผู้ตรวจนับ</option>
                                        @foreach($dataStaff as $key=>$value)
                                            <option  @if (Input::old('id_member_approve_return')==$value->staff_id||(false==empty($Return['id_member_approve_return'])&&$Return['id_member_approve_return']==$value->staff_id)) selected @endif value="{{$value->staff_id}}">{{$value->staff_firstname}} {{$value->staff_lastname}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4 @if($errors->has('member_approve_return_date')) has-error @endif" style="">
                                    <input type="text"  value="@if(false==empty(Input::old('member_approve_return_date'))){{Input::old('member_approve_return_date')}}@elseif($Return['member_approve_return_date']){{$Return['member_approve_return_date']}}@endif" name="member_approve_return_date" class="form-control" id="member_approve_bring_date" placeholder="วันที่อนุมัติเบิก">
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('id_member_approve_pay')) has-error @endif">
                                <label for="inputPassword3" class="col-lg-3 control-label">ผู้ตรวจสอบ</label>
                                <div class="col-lg-5">
                                    <select class="form-control" name="id_member_approve_pay">
                                        <option value="">เลือกผู้ตรวจสอบ</option>
                                        @foreach($dataStaff as $key=>$value)
                                            <option  @if (Input::old('id_member_approve_pay')==$value->staff_id||(false==empty($Return['id_member_approve_pay']&&$Return['id_member_approve_pay']==$value->staff_id))) selected @endif value="{{$value->staff_id}}">{{$value->staff_firstname}} {{$value->staff_lastname}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4 @if($errors->has('member_approve_pay_date')) has-error @endif" style="">
                                    <input type="text"  value="@if(false==empty(Input::old('member_approve_pay_date'))){{Input::old('member_approve_pay_date')}}@elseif($Return['member_approve_pay_date']){{$Return['member_approve_pay_date']}}@endif" name="member_approve_pay_date" class="form-control" id="member_approve_pay_date" placeholder="วันที่อนุมัติจ่าย">
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('id_member_record')) has-error @endif">
                                <label for="inputPassword3" class="col-lg-3 control-label">เจ้าหน้าที่คลัง</label>
                                <div class="col-lg-5">
                                    <select class="form-control" name="id_member_store">
                                        <option value="">เลือกเจ้าหน้าที่คลัง</option>
                                        @foreach($dataStaff as $key=>$value)
                                            <option  @if (Input::old('id_member_store')==$value->staff_id||(false==empty($Return['id_member_store']&&$Return['id_member_store']==$value->staff_id))) selected @endif value="{{$value->staff_id}}">{{$value->staff_firstname}} {{$value->staff_lastname}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4  @if($errors->has('member_store_date')) has-error @endif" style="">
                                    <input type="text" value="@if(false==empty(Input::old('member_store_date'))){{Input::old('member_store_date')}}@elseif($Return['member_store_date']){{$Return['member_store_date']}}@endif" name="member_store_date" class="form-control" id="member_store_date" placeholder="วันที่จ่ายของ">
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('id_member_account')) has-error @endif">
                                <label for="inputPassword3" class="col-lg-3 control-label">บัญชี</label>
                                <div class="col-lg-5">
                                    <select class="form-control" name="id_member_account">
                                        <option value="">เลือกฝ่ายบัญชี</option>
                                        @foreach($dataStaff as $key=>$value)
                                            <option  @if (Input::old('id_member_account')==$value->staff_id||(false==empty($Return['id_member_account']&&$Return['id_member_account']==$value->staff_id))) selected @endif value="{{$value->staff_id}}">{{$value->staff_firstname}} {{$value->staff_lastname}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4 @if($errors->has('member_account_date')) has-error @endif" style="">
                                    <input type="text" value="@if(false==empty(Input::old('member_account_date'))){{Input::old('member_account_date')}}@elseif($Return['member_account_date']){{$Return['member_account_date']}}@endif" name="member_account_date" class="form-control" id="member_account_date" placeholder="วันที่ตรวจสอบ">
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('return_detail')) has-error @endif">
                                <label class="col-lg-3 control-label" for="exampleInputEmail1">รายละเอียด</label>
                                <div class="col-lg-9" style="">
                                    <textarea rows="4" class="form-control" name="return_detail" placeholder="รายละเอียด">@if(false==empty(Input::old('return_detail'))){{Input::old('return_detail')}}@elseif($Return['return_detail']){{$Return['return_detail']}}@endif</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        {{--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                        <button style="width: 100%;" type="submit" class="btn btn-primary">เพิ่ม</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

@stop