<!DOCTYPE html>
<html>
<head>
    <script type="text/javascript">var centreGot = false;</script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript">
        //<![CDATA[

        var map; // Global declaration of the map
        var lat_longs_map = new Array();
        var markers_map = new Array();
        var iw_map;

        iw_map = new google.maps.InfoWindow();

        function initialize_map() {


            var myOptions = {
                zoom: 13,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

            // Try W3C Geolocation (Preferred)
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    map.setCenter(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
                }, function () {
                    alert("Unable to get your current position. Please try again. Geolocation service failed.");
                });
                // Browser doesn't support Geolocation
            } else {
                alert('Your browser does not support geolocation.');
            }
            google.maps.event.addListener(map, "bounds_changed", function (event) {
                if (!centreGot) {
                    var mapCentre = map.getCenter();
                    marker_0.setOptions({
                        position: new google.maps.LatLng(mapCentre.lat(), mapCentre.lng())
                    });
                }
                centreGot = true;
            });

            var markerOptions = {
                map: map
            };
            marker_0 = createMarker_map(markerOptions);


        }


        function createMarker_map(markerOptions) {
            var marker = new google.maps.Marker(markerOptions);
            markers_map.push(marker);
            lat_longs_map.push(marker.getPosition());
            return marker;
        }

        google.maps.event.addDomListener(window, "load", initialize_map);

        //]]>
    </script>
</head>
<body>
<div id="map_canvas" style="width:100%; height:450px;"></div>
</body>
</html>