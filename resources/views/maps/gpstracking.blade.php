<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="1M">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <title>Home</title>
    <link rel="stylesheet" href="{{ URL::asset('bootstrap-3.3.4/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/jquery-ui.min.css') }}">
    <script src="{{ URL::asset('bootstrap-3.3.4/js/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('bootstrap-3.3.4/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('js/jquery-ui.min.js') }}"></script>
    <script src="{{ URL::asset('js/Chart.js') }}"></script>
    <script src="{{ URL::asset('js/trackingmaps.js') }}"></script>
    {{--<script type="text/javascript"--}}
    {{--src="https://www.google.com/jsapi?autoload={--}}
    {{--'modules':[{--}}
    {{--'name':'visualization',--}}
    {{--'version':'1',--}}
    {{--'packages':['corechart']--}}
    {{--}]--}}
    {{--}"></script>--}}

    {{--<script type="text/javascript">--}}
    {{--google.setOnLoadCallback(drawChart);--}}

    {{--function drawChart() {--}}
    {{--var data = google.visualization.arrayToDataTable([--}}
    {{--['Year', 'Sales', 'Expenses'],--}}
    {{--['2004', 1000, 400],--}}
    {{--['2005', 1170, 460],--}}
    {{--['2006', 660, 1120],--}}
    {{--['2007', 1030, 540]--}}
    {{--]);--}}

    {{--var options = {--}}
    {{--title: 'Company Performance',--}}
    {{--curveType: 'function',--}}
    {{--legend: {position: 'bottom'}--}}
    {{--};--}}

    {{--var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));--}}

    {{--chart.draw(data, options);--}}
    {{--}--}}
    {{--</script>--}}


    {!! $maps['map']['js'] !!}

    {{--<script src="{{ URL::asset('js/staffsmanager.js') }}"></script>--}}


    {{--    @include('staffs.staffsheader')--}}
    {{--{{ HTML::script('assets/javascripts/staffsmanager.js') }}--}}
</head>
<body>
<div class="container">

    <header class="row">
        @include('layouts.header')
    </header>

    <div id="main" class="row">

        <div id="content">
            {{--flash Message--}}
            <div style="display: none;">{{ $success = Session::get('success') }}</div>
            <div style="display: none;">{{ $error = Session::get('error') }}</div>
            <div style="display: none;">{{ $warning = Session::get('warning') }}</div>

            @if($success)
                <div style="" class="alert alert-success alert-dismissible fade in " role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                aria-hidden="true">×</span></button>
                    {{ $success }}
                </div>
            @elseif($error)
                <div style="margin-top: 5px;text-align: center;" class="alert alert-danger alert-dismissible fade in "
                     role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                aria-hidden="true">×</span></button>
                    {{ $error }}
                </div>
            @elseif($warning)
                <div style="margin-top: 5px;text-align: center;" class="alert alert-warning alert-dismissible fade in "
                     role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                aria-hidden="true">×</span></button>
                    {{ $warning }}
                </div>
            @endif

            {{--@yield('content')--}}
            <div class="container-fluid">
                <div class="row">
                    {{--<form action="/maps" method="get" id="trackingform">--}}
                    {{--<div class="col-lg-2 col-lg-offset-0">--}}
                    {{--<input type="text" id="current-date" name="current-date" value="{{ $currentdate }}"/>--}}
                    {{--</div>--}}
                    {{--<div class="col-lg-2">--}}
                    {{--<select class="" name="vehicle">--}}
                    {{--<option value="1">รถหมายเลข 1</option>--}}
                    {{--<option value="2">รถหมายเลข 2</option>--}}
                    {{--</select>--}}
                    {{--</div>--}}
                    {{--<div class="col-lg-2">--}}
                    {{--<label class="checkbox-inline"><input type="checkbox" name="show-line" {{ $showline }}--}}
                    {{--value="true">แสดงเส้นทาง</label>--}}
                    {{--</div>--}}
                    {{--<div class="col-lg-2">--}}
                    {{--<label class="checkbox-inline"><input type="checkbox" name="show-freeze"  {{ $showfreeze }}--}}
                    {{--value="true">แสดงจุดจอดแช่</label>--}}
                    {{--</div>--}}
                    {{--<div class="col-lg-2">--}}
                    {{--<label class="checkbox-inline"><input type="checkbox" name="show-startstop" {{ $showstart }}--}}
                    {{--value="true">แสดงจุดเริ่มต้น/สิ้นสุด</label>--}}
                    {{--</div>--}}
                    {{--<div class="col-lg-2">--}}
                    {{--<input class="btn btn-success" type="submit" id="findroute" value="ดุเส้นทาง"/>--}}
                    {{--</div>--}}
                    {{--</form>--}}
                    <h1>แผนที่แสดงตำแหน่งของจีพีเอส</h1>
                </div>
                <div class="row">
                    <div class="col-lg-2">
                        <form action="/maps" method="get" id="trackingform">
                            <div class="row">
                                <input type="text" id="current-date" name="current-date" value="{{ $currentdate }}"
                                       readonly/>
                            </div>
                            <div class="row">
                                <select class="" id="vehicle-list" name="vehicle">
                                    {{--<option value="0">ทุกคัน</option>--}}
                                    @foreach($vehiclelist as $v)
                                        @if($v->id==$vehicle)
                                            <option value="{{ $v->id }}" selected>{{$v->series}} {{$v->color}} {{$v->license_plate}}</option>
                                        @else
                                            <option value="{{ $v->id }}">{{$v->license_plate}}</option>
                                        @endif
                                    @endforeach

                                </select>
                            </div>
                            <div class="row">
                                <label class="checkbox-inline"><input type="checkbox" name="show-line" {{ $showline }}
                                                                      value="true">แสดงเส้นทาง</label>
                            </div>
                            <div class="row">
                                <label class="checkbox-inline"><input type="checkbox"
                                                                      name="show-freeze"  {{ $showfreeze }}
                                                                      value="true">แสดงจุดจอดแช่</label>
                            </div>
                            <div class="row">
                                <label class="checkbox-inline"><input type="checkbox"
                                                                      name="show-startstop" {{ $showstart }}
                                                                      value="true">แสดงจุดเริ่มต้น/สิ้นสุด</label>
                            </div>
                            <div class="row">
                                <input class="btn btn-success" type="submit" id="findroute" value="ดุเส้นทาง"/>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-10">
                        {!! $maps['map']['html'] !!}
                        {{--<iframe src="/map">--}}

                        {{--</iframe>--}}
                    </div>
                </div>
                <div class="row">
                    <h1>ตารางแสดงความเร็วของรถโดยเฉลี่ย </h1>
                </div>
                <div class="row">
                    <div class="col-lg-2">
                        <label>ชั่วโมงที่</label>

                        <div id="timelist"></div>
                    </div>
                    <div class="col-lg-10">
                        <label>(Km/h)</label>

                        <div id="chart">
                            <canvas id="chart_div" style="width: 100%;height: 500px">
                            </canvas>
                        </div>
                        {{--@linechart('Stocks', 'temps_div');--}}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer class="row">
        @include('layouts.footer')
    </footer>

</div>
</body>
</html>