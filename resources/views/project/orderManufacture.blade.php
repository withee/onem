@extends('layouts.layouts')
@section('content')
    <div class="row" style="margin-top: 10px;">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li><a href="/project/viewProject">จัดการโครงการ</a></li>
                <li class="active">จัดการสินค้าผลิต</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <h3>จัดการสินค้าผลิต</h3>
        </div>
    </div>

    <style>
        .tt-query {
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        }

        .tt-hint {
            color: #999
        }

        .tt-menu {
            width: 422px;
            margin: 12px 0;
            padding: 8px 0;
            background-color: #fff;
            border: 1px solid #ccc;
            border: 1px solid rgba(0, 0, 0, 0.2);
            -webkit-border-radius: 8px;
            -moz-border-radius: 8px;
            border-radius: 8px;
            -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
            -moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
            box-shadow: 0 5px 10px rgba(0,0,0,.2);
        }

        .tt-suggestion {
            padding: 3px 20px;
            font-size: 18px;
            line-height: 24px;
        }

        .tt-suggestion:hover {
            cursor: pointer;
            color: #fff;
            background-color: #0097cf;
        }

        .tt-suggestion.tt-cursor {
            color: #fff;
            background-color: #0097cf;

        }

        .tt-suggestion p {
            margin: 0;
        }

        .gist {
            font-size: 14px;
        }
    </style>

    <script>
        $(document).ready(function () {
            var substringMatcher = function (strs) {
                return function findMatches(q, cb) {
                    var matches, substringRegex;

                    // an array that will be populated with substring matches
                    matches = [];

                    // regex used to determine if a string contains the substring `q`
                    substrRegex = new RegExp(q, 'i');

                    // iterate through the pool of strings and for any string that
                    // contains the substring `q`, add it to the `matches` array
                    $.each(strs, function (i, str) {
                        if (substrRegex.test(str)) {
                            matches.push(str);
                        }
                    });

                    cb(matches);
                };
            };

            var substringMatcherProduct = function (strs) {
                return function findMatches(q, cb) {
                    var matches, substringRegex;

                    // an array that will be populated with substring matches
                    matches = [];

                    // regex used to determine if a string contains the substring `q`
                    substrRegex = new RegExp(q, 'i');

                    // iterate through the pool of strings and for any string that
                    // contains the substring `q`, add it to the `matches` array
                    $.each(strs, function (i, str) {
                        if (substrRegex.test(str)) {
                            matches.push(str);
                        }
                    });

                    cb(matches);
                };
            };
            $.ajax({
                url: "/account/checkManufacture",
                type: "post",
                dataType: 'json'
            }).success(function (res) {
//                console.log(res);
                $('input#the-basics').typeahead({
                            hint: true,
                            highlight: true,
                            minLength: 1
                        },
                        {
                            name: 'states',
                            source: substringMatcher(res)
                        });
            })
            $.ajax({
                url: "/account/checkProduct",
                type: "post",
                dataType: 'json'
            }).success(function (res) {
//                console.log(res);
                $('input#the-basics-product').typeahead({
                            hint: true,
                            highlight: true,
                            minLength: 1
                        },
                        {
                            name: 'states',
                            source: substringMatcherProduct(res)
                        });
            })
        });
    </script>

    <div class="row" style="padding-bottom: 10px;">
        <div class="col-lg-12">
            <form method="POST" action="/project/addOrderManufacture" class="form-inline" novalidate>
                @if (true==$errors->has('name')||true==$errors->has('count')||true==$errors->has('type')||true==$errors->has('payee')||true==$errors->has('remark'))
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>
                        @endforeach
                    </div>
                @endif
                <input type="hidden" name="project_id" value="{{$id}}">
                <div class="form-group  @if ($errors->has('name')) has-error @endif">
                    <label for="exampleInputName2">รายการ</label>
                    <input id="the-basics" type="text" class="form-control" style="width: 320px;" value="@if(!empty($orderManufacture->income_list)){{$orderManufacture->income_list}}@elseif(!empty($orderManufacture->expenses_list)){{$orderManufacture->expenses_list}}@elseif(!empty(Input::old('name'))){{Input::old('name')}}@endif" name="name" placeholder="รายการ">
                </div>
                <div class="form-group  @if ($errors->has('count')) has-error @endif">
                        {{--<label for="exampleInputEmail2">จำนวน</label>--}}
                        <input type="text" class="form-control" style="width: 70px;" value="@if(!empty($orderManufacture->count)){{$orderManufacture->count}}@elseif(!empty($orderManufacture->count)){{$orderManufacture->count}}@elseif(!empty(Input::old('count'))){{Input::old('count')}}@endif" name="count" placeholder="จำนวน">
                </div>
                <div class="form-group  @if ($errors->has('payee')) has-error @endif">
                        <label for="exampleInputEmail2">รับผิดชอบ</label>
                        <select class="form-control" name="staff_id_asset" style="padding-left: 4px;padding-right: 4px;">
                            @foreach($staff as $key=>$value)
                                <option @if(Input::old('staff_id_asset')==$value->staff_id)selected @elseif(!empty($orderManufacture->staff_id_asset))selected @endif value="{{$value->staff_username}}">{{$value->staff_firstname}} {{$value->staff_lastname}}</option>
                            @endforeach
                        </select>
                        {{--<input type="text" class="form-control" style="width: 100px;" value="@if(!empty($orderManufacture->payee)){{$orderManufacture->payee}}@elseif(!empty(Input::old('payee'))){{Input::old('payee')}}@endif" name="payee"/>--}}
                </div>
                <button type="submit" class="btn btn-default">บันทึก</button>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-7">
            <div style="float: left;">
                <div style="float: left;width: 1px;height: 1px;" class="alert alert-danger"></div>
                <div style="float: left; padding: 5px;">สินค้าไม่พอ</div>
            </div>
            <div style="float: left;">
                <div style="float: left;width: 1px;height: 1px;" class="alert alert-info"></div>
                <div style="float: left; padding: 5px;">รอยืนยัน</div>
            </div>
            <div style="float: left;">
                <div style="float: left;width: 1px;height: 1px;" class="alert alert-warning"></div>
                <div style="float: left; padding: 5px;">รอสั่งซื้อ</div>
            </div>
            <div style="float: left;">
                <div style="float: left;width: 1px;height: 1px;" class="alert alert-success"></div>
                <div style="float: left; padding: 5px;">ยืนยันเเล้ว</div>
            </div>
        </div>
        <div class="col-lg-5">
            @if($user->staff_usertype==1 || $user->staff_usertype==2)
            <a href="/project/calculateBring?id={{$id}}" class="btn btn-default" ><span class="glyphicon glyphicon-tag" aria-hidden="true"></span> คำนวน</a>
            <a href="/project/reportProject?id={{$id}}" class="btn btn-primary"><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> รายงาน</a>
            @endif
            <a href="/project/bringOrderManufactureDetail?id={{$id}}" class="btn btn-warning" target="_blank"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> สั่งซื้อ</a>
            <a href="/project/printBring?id={{$id}}" class="btn btn-info" target="_blank"><span class="glyphicon glyphicon-print" aria-hidden="true"></span> พิมพ์ใบเบิก</a>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">

            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th>สถานะ</th>
                    <th>ชื่อสินค้า</th>
                    <th>จำนวน</th>
                    <th>ผลิตเเล้ว</th>
                    <th>คงเหลือ</th>
                    <th>ผู้รับผิดชอบ</th>
                    <th>วัตถุดิบ</th>
                    <th>จัดการ</th>
                </tr>
                </thead>
                <tbody>
                @foreach($dataOrderManufacture as $key=>$value)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{(($value->order_manufacture_type=='add')?'กำลังผลิต':'ผลิตเสร็จ')}}</td>
                    <td>{{ \App\Models\Product::GetNameProduct($value['product_id'])}}</td>
                    <td>{{$value['order_manufacture_count']}}</td>
                    <td>{{$value['order_manufacture_finished']}}</td>
                    <td>{{$value['order_manufacture_balance']}}</td>
                    <td>{{$value['staff_id_asset']}}</td>
                    <td>
                        <ul class="nav nav-pills nav-stacked">
                        @foreach(\App\Models\OrderManufactureDetail::where('order_manufacture_id', '=', $value['order_manufacture_id'])->get() as $keyLi=>$valueLi)
                            @if($valueLi->status=='approve')
                                <li data-toggle="modal" id="orderDetail" count="{{$valueLi->count}}" storename_id="{{$valueLi->storename_id}}" product_id="{{$valueLi->product_id}}" id_order="{{$valueLi->order_manufacture_detail_id}}" data-target="{{(($user->staff_usertype==1)?'#myModal':'')}}" class="alert alert-success" style="cursor: pointer;padding: 2px;margin: 2px;">
                                    {{\App\Models\Product::GetNameProduct($valueLi->product_id)}}
                                    <span style="font-weight: bolder;">คลัง</span> {{\App\Models\Storename::GetNameStore($valueLi->storename_id)}}
                                    <span style="font-weight: bolder;">จำนวน</span> {{$valueLi->count}}
                                </li>
                            @elseif($valueLi->status=='bring')
                                <li data-toggle="modal" id="orderDetail" count="{{$valueLi->count}}" storename_id="{{$valueLi->storename_id}}" product_id="{{$valueLi->product_id}}" id_order="{{$valueLi->order_manufacture_detail_id}}" data-target="{{(($user->staff_usertype==1)?'#myModal':'')}}" class="alert alert-warning" style="cursor: pointer;padding: 2px;margin: 2px;">
                                    {{\App\Models\Product::GetNameProduct($valueLi->product_id)}}
                                    <span style="font-weight: bolder;">คลัง</span> {{\App\Models\Storename::GetNameStore($valueLi->storename_id)}}
                                    <span style="font-weight: bolder;">จำนวน</span> {{$valueLi->count}}
                                </li>
                            @else
                                <li data-toggle="modal" id="orderDetail" count="{{$valueLi->count}}" storename_id="{{$valueLi->storename_id}}" product_id="{{$valueLi->product_id}}" id_order="{{$valueLi->order_manufacture_detail_id}}" data-target="{{(($user->staff_usertype==1)?'#myModal':'')}}" class="{{\App\Models\Stores::CheckStore($valueLi->product_id,$valueLi->storename_id,$valueLi->count)}}" style="cursor: pointer;padding: 2px;margin: 2px;">
                                    <a href="/project/removeOrderManufactureDetail?id={{$valueLi->order_manufacture_detail_id}}" onclick="return confirm('ยืนยันการลบ วัถุดิบที่ใช้ผลิตนี้')" style="padding:0px;float: left;" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                                    {{\App\Models\Product::GetNameProduct($valueLi->product_id)}}
                                    <span style="font-weight: bolder;">คลัง</span> {{\App\Models\Storename::GetNameStore($valueLi->storename_id)}}
                                    <span style="font-weight: bolder;">จำนวน</span> {{$valueLi->count}}
                                </li>
                            @endif
                        @endforeach
                        </ul>
                    </td>
                    <td style="width: 450px;">
                        @if($value->order_manufacture_type!='finish')
                        @if($user->staff_usertype==3 || $user->staff_usertype==4 || $user->staff_usertype==1 || $user->staff_usertype==2)
                        <form style="float: left;" method="POST" action="/project/addOrderManufactureDetail" class="form-inline" novalidate>
                            @if (true==$errors->has('name')||true==$errors->has('count')||true==$errors->has('type')||true==$errors->has('payee')||true==$errors->has('remark'))
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        {{ $error }}<br>
                                    @endforeach
                                </div>
                            @endif
                            <input type="hidden" name="order_manufacture_id" value="{{$value->order_manufacture_id}}">
                            <input type="hidden" name="project_id" value="{{$id}}">
                            <div class="form-group  @if ($errors->has('name')) has-error @endif">
                                <input id="the-basics-product" type="text" class="form-control" style="width: 220px;" value="" name="name" placeholder="วัตถุดิบ">
                            </div>
                                <div class="form-group  @if ($errors->has('storename')) has-error @endif">
                                    {{--<label for="exampleInputEmail2">คลัง</label>--}}
                                    <select class="form-control" name="storename_id" style="width: 80px;padding-left: 4px;padding-right: 4px;">
                                        @foreach($storeName as $key=>$valueSto)
                                            <option {{(($valueSto->storename_id==2)?'selected':'')}} value="{{$valueSto->storename_id}}">{{$valueSto->storename_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            <div class="form-group  @if ($errors->has('count')) has-error @endif">
                                <input id="" type="text" class="form-control" style="width: 70px;" value="" name="count" placeholder="จำนวน">
                            </div>
                            <button type="submit" class="btn btn-default">เพิ่ม</button>
                        </form>
                        @endif
                        <div style="clear: both"></div>
                        @if($user->staff_usertype==6 || $user->staff_usertype==1 || $user->staff_usertype==2)
                            <form style="float: left;" method="POST" action="/project/addManufacture" class="form-inline" novalidate>
                                <input type="hidden" name="project_id" value="{{$id}}">
                                <input type="hidden" name="order_manufacture_id" value="{{$value->order_manufacture_id}}">
                                <input type="hidden" name="payee" value="{{$user->staff_username}}">
                                <div class="form-group  @if ($errors->has('storename')) has-error @endif">
                                    {{--<label for="exampleInputEmail2">คลัง</label>--}}
                                    <select class="form-control" name="storename_id" style="width: 80px;padding-left: 4px;padding-right: 4px;">
                                        @foreach($storeName as $key=>$valueSto)
                                            <option value="{{$valueSto->storename_id}}">{{$valueSto->storename_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group  @if ($errors->has('count')) has-error @endif">
                                    <input id="" type="text" class="form-control" style="width: 70px;" value="0" name="count" placeholder="จำนวน">
                                </div>

                                <button type="submit" class="btn btn-default">รับสินค้า</button>
                            </form>
                        @endif
                        @endif
                        <div style="clear: both"></div>
                        <div style="margin-top: 10px;">
                            @if($value->order_manufacture_type!='finish')
                                @if($user->staff_usertype==6 || $user->staff_usertype==1 || $user->staff_usertype==2)
                                    <button id="BringProduct" manufacture_id="{{$value['order_manufacture_id']}}" style="float: left;margin-left: 5px;" class="btn btn-success" data-toggle="modal" data-target="#myModalBring">เบิกสินค้า</button>
                                @endif
                            @endif
                            @if($user->staff_usertype==1 || $user->staff_usertype==2)
                                <a style="float: left;margin-left: 5px;" onclick="return confirm('ยืนยันการยกเลิกการผลิต')" href="/project/RemoveOrderManufacture?id={{$value->order_manufacture_id}}" class="btn btn-danger">ลบ</a>
                            @endif
                        </div>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade" id="myModalBring" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <form style="float: left;width: 560px;" method="POST" action="/project/BringProduct"  >
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">เบิกสินคัา</h4>
                    </div>
                    <div class="modal-body" >
                        <input type="hidden" name="project_id" value="{{$id}}">
                        <div class="form-group  @if ($errors->has('payee')) has-error @endif">
                            <label for="exampleInputEmail2">ผู้เบิก</label>
                            <select class="form-control" name="payee" style="padding-left: 4px;padding-right: 4px;">
                                @foreach($staff as $key=>$value)
                                    <option value="{{$value->staff_username}}">{{$value->staff_firstname}} {{$value->staff_lastname}}</option>
                                @endforeach
                            </select>
                            {{--<input type="text" class="form-control" style="width: 100px;" value="@if(!empty($orderManufacture->payee)){{$orderManufacture->payee}}@elseif(!empty(Input::old('payee'))){{Input::old('payee')}}@endif" name="payee"/>--}}
                        </div>
                        <div class="form-group  @if ($errors->has('count')) has-error @endif">
                            <label for="exampleInputEmail2">หมายเหตุ</label>
                            <textarea class="form-control" name="remark" placeholder="หมายเหตุ"></textarea>
                        </div>
                        <div id="main_model"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">เบิกสินค้า</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $(document).on('click',"#BringProduct",function(){
                $.ajax({
                    url: "/project/GetOrderBring?id="+$(this).attr('manufacture_id'),
                    type: "get"
                }).success(function (res) {
                    $('div#main_model').html(res);
                })
            })

            $(document).on('click',"#orderDetail",function(){
                $('input#order_manufacture_detail_id').val($(this).attr('id_order'));
                $('select#product_id').val($(this).attr('product_id'));
                $('input#count').val($(this).attr('count'));

                $.ajax({
                    url: "/project/GetStorkByProduct?id="+$(this).attr('product_id'),
                    type: "get"
                }).success(function (res) {
                    $('select#select_storename_id').html(res);
                    $('select#select_storename_id').val($('li#orderDetail').attr('storename_id'));
                })

                $('div#myModal').show();
            })
        })
    </script>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <form style="float: left;width: 560px;" method="POST" action="/project/ConfirmOrderManufactureDetail" class="form-horizontal" novalidate>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">ยืนยันการเบิกสินค้า</h4>
                </div>
                <div class="modal-body">
                        @if (true==$errors->has('product_id')||true==$errors->has('count')||true==$errors->has('storename_id'))
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </div>
                        @endif
                        <input type="hidden" name="project_id" value="{{$id}}">
                        <input type="hidden" name="order_manufacture_detail_id" id="order_manufacture_detail_id" value="{{$value->order_manufacture_id}}">
                        <div class="form-group  @if ($errors->has('name')) has-error @endif">
                            <label for="inputEmail3" class="col-sm-2 control-label">วัตถุดิบ</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="product_id" id="product_id">
                                    @foreach($Product as $key=>$value)
                                        <option value="{{$value->product_id}}" >{{$value->product_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group  @if ($errors->has('count')) has-error @endif">
                            <label for="inputEmail3" class="col-sm-2 control-label">วัตถุดิบ</label>
                            <div class="col-sm-10">
                            <input id="count" type="text" class="form-control" style="" value="" name="count" placeholder="จำนวน">
                            </div>
                        </div>
                        <div class="form-group  @if ($errors->has('storename_id')) has-error @endif">
                            <label for="inputEmail3" class="col-sm-2 control-label">คลัง</label>
                            <div class="col-sm-10">
                            <select id="select_storename_id" class="form-control" name="storename_id" style="padding-left: 4px;padding-right: 4px;">

                            </select>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit"  class="btn btn-primary">ยืนยัน</button>
                </div>
            </div>
            </form>
        </div>
    </div>

@stop