@extends('layouts.layouts')
@section('content')
    <div class="row" style="margin-top: 10px;">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li><a href="/project/viewProject">จัดการโครงการ</a></li>
                <li><a href="/project/orderManufacture?id={{$id}}">จัดการสินค้าผลิต</a></li>
                <li class="active">รายงานโครงการ</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <h3>คำนวนวัสถุดิบก่อนการผลิต</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th>ชื่อสินค้า</th>
                    <th>จำนวน</th>
                    {{--<th>จัดการ</th>--}}
                </tr>
                </thead>
                <tbody>
                @foreach($dataCalculateBring as $key=>$value)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$value->product_name}}</td>
                    <td>{{$value->total}}</td>
                    {{--<td></td>--}}
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@stop