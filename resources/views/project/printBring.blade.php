<html>
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="1M">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <title>Home</title>
    <link rel="stylesheet" href="{{ URL::asset('bootstrap-3.3.4/css/bootstrap.min.css') }}">
</head>
<body>
<style>
    tr,td,th{
        border: solid 2px #000000 !important;
        /*background-color: #FFFFff;*/
        /*padding: 5px;*/
        font-size: 14px;
        padding: 5px !important;
        margin: 0px !important;
    }
    table{
        /*background-color: #666666;*/
    }
</style>
<div style="display: none;">{{$id=0}}{{$order=1}}</div>
<table style="width: 850px" class="table table-bordered">
    <thead>
    <tr>
        <th style="text-align: center;" colspan="6">ใบเบิกสินค้าเพื่อผลิต โครงการ {{$dataProject->project_name}}</th>
    </tr>
    <tr>
        <th>ลำดับ</th>
        <th>ชื่อสินค้า</th>
        <th style="text-align: center;">คลัง</th>
        {{--<th>จำนวน</th>--}}
        <th style="text-align: right;">จำนวน</th>
        <th>เบิกไปเเล้ว</th>
        <th style="text-align: right;">จำนวนเบิก</th>
        <th>หน่วย</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data as $key=>$value)
        {{--@if($id!=$value->order_manufacture_id)--}}
            {{--<span style="display: none;">{{$id=$value->order_manufacture_id}}{{$count=1}}</span>--}}
            {{--<tr>--}}
                {{--<td colspan="6"><span style="font-weight: bolder;">{{$order}}.</span> {{\App\Models\Product::GetNameProduct($value->markproduct_id)}}</td>--}}
            {{--</tr>--}}
            {{--<span style="display: none;">{{$order++}}</span>--}}
        {{--@endif--}}
    <tr>
        <td align="right" width="50" scope="row">{{$key+1}}</td>
        <td width="250">{{\App\Models\Product::GetNameProduct($value->product_id)}}</td>
        <td align="center" width="150">{{\App\Models\Storename::GetNameStore($value->storename_id)}}</td>
        {{--<td align="right" width="100">{{$value->count}}</td>--}}
        <td align="right" width="100">{{($value->count-$value->bring)}}</td>
        <td align="right" width="100">{{$value->bring}}</td>
        <td width="150"></td>
        <td width="100">{{\App\Models\ProductUnit::getNameUnitByProduct($value->product_id)}}</td>
    </tr>
        {{--<span style="display: none;">{{$count++}}</span>--}}
    @endforeach
    <tr>
        <td align="center" colspan="2">
            <br>ผู้รับผิดชอบ(.................................)
        </td>
        <td align="center" colspan="2">
            <br>คลัง(.......................................)
        </td>
        <td align="center" colspan="2">
            <br>ผู้เบิก(......................................)
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>