@extends('layouts.layouts')
@section('content')
    <input type="hidden" name="id" id="id" value="{{$id}}">
    <div class="row" style="margin-top: 10px;">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li><a href="/project/viewMainProject?id={{$dataProjectSub->project_id}}">จัดการ</a></li>
                <li class="active">รายการทำงาน</li>
            </ol>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $(document).on('change', "select#selectMonth", function(){
                $.get("/project/workChangeMonth", {
                    id:$('input#id').val(),
                    month:$(this).val()
                }, function (datalist) {
                    $('table#viewWork').html(datalist);
                })

                $.get("/project/workTableChangeMonth", {
                    id:$('input#id').val(),
                    month:$(this).val()
                }, function (datalist) {
                    $('table#tableView').html(datalist);
                })

            })

            $(document).on('click', "a#header_table", function(){
                $.get("/project/workTableChangeMonth", {
                    id:$('input#id').val(),
                    month:$('select#selectMonth').val(),
                    order:$(this).attr('order')
                }, function (datalist) {
                    $('table#tableView').html(datalist);
                })
            })

        });

        function myFunction(data) {
//            $('input#project_id').val(data);
            $('input#activity_project_id').val(data);
            $('input#project_sub_id').val(data);
        }
    </script>

    <div class="bs-example bs-example-tabs" data-example-id="togglable-tabs">
        <ul id="myTabs" class="nav nav-tabs" role="tablist" >
            <li role="presentation" class="active"><a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">การทำงาน</a></li>
            <li role="presentation"><a href="#profile" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile">ลงข้อมูลประจำวัน</a></li>
            <li style="float: right;display: none" >
                <form action="/upload" method="post" enctype="multipart/form-data">
                    <input style="float: left;" type=hidden name="id" value="{{$dataProjectSub->project_id}}" >
                    <input style="float: left;" type="hidden" name="filename" value="{{$dataProjectSub->project_sub_name}}" placeholder="filename">
                    <input style="float: left;" type="file" name="files">
                    <input style="float: left;" class="btn btn-default" type="submit" value="อัพโหลด">
                    <a style="margin-left: 5px;" class="btn btn-danger" data-toggle="modal" data-target="#myModalAddProjectsActivityLog"><span class="glyphicon glyphicon-floppy-save" aria-hidden="true"></span> บันทึก</a>
                </form>
            </li>
        </ul>
        <div id="myTabContent" class="tab-content" style="padding: 10px;border-left: solid 1px#CCCCCC;border-right: solid 1px#CCCCCC;border-bottom: solid 1px#CCCCCC;">
            <div role="tabpanel" class="tab-pane fade in active" id="home" aria-labelledby="home-tab">
                <div class="row">
                    <div class="col-lg-7">
                        <div style="font-size: 26px;font-weight: bolder">รายการทำงาน</div>
                    </div>
                    <div class="col-lg-5">
                        <form action="/upload" method="post" enctype="multipart/form-data">
                            <input style="float: left;" type=hidden name="id" value="{{$dataProjectSub->project_id}}" >
                            <input style="float: left;" type="hidden" name="filename" value="{{$dataProjectSub->project_sub_name}}" placeholder="filename">
                            <input style="float: left;" type="file" name="files">
                            <input style="float: left;" class="btn btn-default" type="submit" value="อัพโหลด">
                            {{--<a style="margin-left: 5px;" class="btn btn-danger" data-toggle="modal" data-target="#myModalAddProjectsActivityLog"><span class="glyphicon glyphicon-floppy-save" aria-hidden="true"></span> บันทึก</a>--}}
                        </form>
                        {{--<a style="margin-left: 5px;" href="/project/viewSubProject?id={{$dataProjectSub->project_sub_id}}" class="btn btn-default">จัดการ</a>--}}
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12" style="width: 100%;">
                        <img style="text-align: center;height: 100%;width: 100%;" src="{{$dataImage['data']}}">
                    </div>
                </div>

                <div class="row" style="margin-top: 20px;">
                    <div class="col-lg-12" style="width: 100%;">
                        @foreach($dataListProjectSub as $key=>$value)
                            <div style="border: solid 1px;padding: 0px;margin: 0px;" class="col-lg-6">
                                <div class="col-lg-12" style="border: solid 1px;font-weight: bolder;font-size: 18px;">{{$value->project_sub_name}}</div>
                                <div class="col-lg-12" style="padding: 0px;margin: 0px;">
                                    <div style="border: solid 1px;" class="col-lg-4">วันที่</div>
                                    <div style="border: solid 1px;" class="col-lg-4">กิจกรรม</div>
                                    <div style="border: solid 1px;" class="col-lg-1">เริ่ม</div>
                                    <div style="border: solid 1px;" class="col-lg-1">ถึง</div>
                                    <div style="border: solid 1px;" class="col-lg-2">จำนวน</div>
                                </div>
                                @foreach($value->created_at as $keyC=>$valueC)
                                    <div class="col-lg-12" style="padding: 0px;margin: 0px;">
                                        <div style="border: solid 1px;" class="col-lg-4">{{$valueC->created_at}}</div>
                                        <div style="border: solid 1px;" class="col-lg-4">{{$valueC->projects_activity_name}}</div>
                                        <div style="border: solid 1px;" class="col-lg-1">{{$valueC->start_at}}</div>
                                        <div style="border: solid 1px;" class="col-lg-1">{{$valueC->end_at}}</div>
                                        <div style="text-align:right; border: solid 1px;" class="col-lg-2">{{number_format($valueC->projects_activity_log_detail_value,2)}}</div>
                                    </div>
                                @endforeach
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="profile" aria-labelledby="profile-tab">
                <div class="row">
                    <div class="col-lg-12">
                        <h3>ลงข้อมูลประจำวัน</h3>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-9"></div>
                    <div class="col-lg-3">
                        <div class="form-inline">
                            <div class="form-group">
                               <label for="exampleInputEmail2">เลือกเดือน</label>
                               <select id="selectMonth" class="form-control">
                                   @foreach($monthNames as $key=>$value)
                                       <option @if($key==$countMonth)selected @endif value="{{$key}}">{{$value}}</option>
                                   @endforeach
                               </select>
                            </div>
                            {{--<button type="submit" class="btn btn-default">Send invitation</button>--}}
                        </div>
                    </div>
                </div>

                {{--<div class="row">--}}
                    {{--<div class="col-lg-9"></div>--}}
                    {{--<div class="col-lg-3">--}}
                        {{--<button onclick="myFunction({{$value->project_sub_id}})" class="btn btn-danger" data-toggle="modal" data-target="#myModalAddProjectsActivityLog"><span class="glyphicon glyphicon-floppy-save" aria-hidden="true"></span> บันทึก</button>--}}
                        {{--<button onclick="myFunction({{$value->project_sub_id}})" class="btn btn-info" data-toggle="modal" data-target="#myModalBring"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> เบิกสินค้า</button>--}}
                    {{--</div>--}}
                {{--</div>--}}

                <div class="row" style="margin-top: 10px;">
                    <div class="col-lg-12">
                        <table id="viewWork" class="table table-bordered">
                            <thead>
                            <tr>
                                <th rowspan="2">กิจกรรม</th>
                                <th style="text-align: center;" colspan="{{$countDay}}">เดือน {{$monthNames[$countMonth]}}</th>
                                <th rowspan="2">รวม</th>
                            </tr>
                            <tr>
                                @for($i = 1; $i <=$countDay; $i++)
                                    <th>{{$i}}</th>
                                @endfor
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($dataProjectsActivity as $key=>$value)
                                <span style="display: none;">{{$sum=0}}</span>
                                <tr>
                                    <td style="padding: 0px;">{{$value->projects_activity_name}}</td>
                                    @for($i = 1; $i <=$countDay; $i++)
                                        <td style="padding: 0px;text-align: center;">
                                            @if(false==empty($dataWorkMonth[$i][$value->projects_activity_id]))
                                                <span style="display: none;">{{$sum+=$dataWorkMonth[$i][$value->projects_activity_id]['value']}}</span>
                                                <span data-toggle="tooltip" data-placement="top" title="{{$dataWorkMonth[$i][$value->projects_activity_id]['text']}}" class="glyphicon glyphicon-road" aria-hidden="true"></span>
                                            @endif
                                        </td>
                                    @endfor
                                    <td style="padding: 0px;text-align: right;font-weight: bolder;">{{number_format($sum,2)}}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <td style="padding: 0px;font-weight: bolder;">สภาพอากาศ</td>
                                @for($i = 1; $i <=$countDay; $i++)
                                    <td style="padding: 0px;text-align: center;">
                                    </td>
                                @endfor
                                <td style="padding: 0px;text-align: center;font-weight: bolder;"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-9"></div>
                    <div class="col-lg-3" >
                        <button style="float: right;" onclick="myFunction({{$value->project_sub_id}})" class="btn btn-danger" data-toggle="modal" data-target="#myModalAddProjectsActivityLog"><span class="glyphicon glyphicon-floppy-save" aria-hidden="true"></span> บันทึก</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12" style="font-weight: bolder;font-size: 18px;">
                        {{$dataProjectSub->project_sub_name}}
                    </div>
                </div>
                <div class="row" style="margin-top: 10px;">
                    <div class="col-lg-12">
                        <table id="tableView" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th><a id="header_table" order="projects_activity_log.projects_activity_start_date" style="cursor: pointer;">วันที่</a></th>
                                <th><a id="header_table" order="projects_activity_log_detail.start_at" style="cursor: pointer;">ตำแหน่ง</a></th>
                                <th><a id="header_table" order="projects_activity.projects_activity_name" style="cursor: pointer;">กิจกรรม</a></th>
                                <th><a id="header_table" order="projects_activity_log_detail.projects_activity_log_detail_value" style="cursor: pointer;">จำนวน</a></th>
                                <th><a id="header_table" order="projects_activity_log.projects_activity_log_weather" style="cursor: pointer;">สภาพอากาศ</a></th>
                                <th><a id="header_table" order="projects_activity_log.projects_activity_log_problem" style="cursor: pointer;">ปัญหา</a></th>
                                <th><a id="header_table" order="staffs.staff_firstname" style="cursor: pointer;">ผู้ปฏิบัติงาน</a></th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($dataActivityLog as $key=>$value)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$value->projects_activity_start_date}}</td>
                                        <td>+{{$value->start_at}} - +{{$value->end_at}} ({{$value->project_position}})</td>
                                        <td>{{$value->projects_activity_name}}</td>
                                        <td style="text-align: right;">{{number_format($value->projects_activity_log_detail_value,2)}}</td>
                                        <td>{{$value->projects_activity_log_weather}}</td>
                                        <td>{{$value->projects_activity_log_problem}}</td>
                                        <td>{{$value->staff_firstname}} {{$value->staff_lastname}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="dropdown1" aria-labelledby="dropdown1-tab">
                <p>Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo retro fanny pack lo-fi farm-to-table readymade. Messenger bag gentrify pitchfork tattooed craft beer, iphone skateboard locavore carles etsy salvia banksy hoodie helvetica. DIY synth PBR banksy irony. Leggings gentrify squid 8-bit cred pitchfork. Williamsburg banh mi whatever gluten-free, carles pitchfork biodiesel fixie etsy retro mlkshk vice blog. Scenester cred you probably haven't heard of them, vinyl craft beer blog stumptown. Pitchfork sustainable tofu synth chambray yr.</p>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="dropdown2" aria-labelledby="dropdown2-tab">
                <p>Trust fund seitan letterpress, keytar raw denim keffiyeh etsy art party before they sold out master cleanse gluten-free squid scenester freegan cosby sweater. Fanny pack portland seitan DIY, art party locavore wolf cliche high life echo park Austin. Cred vinyl keffiyeh DIY salvia PBR, banh mi before they sold out farm-to-table VHS viral locavore cosby sweater. Lomo wolf viral, mustache readymade thundercats keffiyeh craft beer marfa ethical. Wolf salvia freegan, sartorial keffiyeh echo park vegan.</p>
            </div>
        </div>
    </div>

    <div class="modal fade @if(true==$errors->has('projects_activity_start_date')||true==$errors->has('projects_activity_end_date')||true==$errors->has('id_staff_assign')) in @endif" style="@if(true==$errors->has('projects_activity_end_date')||true==$errors->has('projects_activity_start_date')||true==$errors->has('id_staff_assign')) display: block; @endif" id="myModalAddProjectsActivityLog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <form method="POST" action="/project/addActivityLog" >
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <a type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                        <h4 class="modal-title" id="myModalLabel">สรุปการทำงานประจำวัน</h4>
                    </div>
                    <div class="modal-body">
                        @if ($errors->has())
                            <div class="alert alert-danger" style="padding: 0px;">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}
                                @endforeach
                            </div>
                        @endif
                        <div class="form-horizontal">
                            <input type="hidden" name="project_id" id="project_id" value="{{$dataProjectSub->project_id}}">
                            {{--<input type="hidden" name="project_sub_id" id="project_sub_id" value="{{Input::old('project_sub_id')}}">--}}

                            <div class="form-group @if($errors->has('project_sub_id')) has-error @endif">
                                <label for="inputPassword3" class="col-lg-3 control-label">ข้าง</label>
                                <div class="col-lg-9">
                                    <select class="form-control" name="project_sub_id">
                                        <option value="">เลือกข้าง</option>
                                        @foreach($dataListProjectSub as $key=>$value)
                                            <option  @if (Input::old('project_sub_id')==$value->project_sub_id) selected @endif value="{{$value->project_sub_id}}">{{$value->project_sub_name}}({{$value->project_position}})</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('id_staff_assign')) has-error @endif">
                                <label for="inputPassword3" class="col-lg-3 control-label">ชื่อทีม</label>
                                <div class="col-lg-9">
                                    <select class="form-control" name="id_staff_assign">
                                        <option value="">เลือกชื่อทีม</option>
                                        @foreach($dataStaff as $key=>$value)
                                            <option {{(Input::old('id_staff_assign')==$value->staff_id||(false==empty($purchases['id_staff_assign'])&&$purchases['id_staff_assign']==$value->staff_id))?'selected':''}} value="{{$value->staff_id}}">{{$value->staff_firstname}} {{$value->staff_lastname}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('id_staff_check')) has-error @endif">
                                <label for="inputPassword3" class="col-lg-3 control-label">ชื่อผู้ตรวจสอบ</label>
                                <div class="col-lg-9">
                                    <select class="form-control" name="id_staff_check">
                                        <option value="">เลือกชื่อผู้ตรวจสอบ</option>
                                        @foreach($dataStaff as $key=>$value)
                                            <option {{(Input::old('id_staff_check')==$value->staff_id||(false==empty($purchases['id_staff_check'])&&$purchases['id_staff_check']==$value->staff_id))?'selected':''}} value="{{$value->staff_id}}">{{$value->staff_firstname}} {{$value->staff_lastname}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group" style="display: none;" >
                                <div class="@if($errors->has('projects_activity_log_km')) has-error @endif">
                                    <label for="inputEmail3" class="col-lg-3 control-label" style="padding-right: 2px;padding-left: 0px;">กม.ที่</label>
                                    <div class="col-lg-2" style="padding: 0px;margin: 0px;">
                                        <input type="text" id="projects_activity_log_km" value="0" name="projects_activity_log_km" class="form-control"  placeholder="กม.ที่">
                                    </div>
                                </div>
                                <div class="@if($errors->has('projects_activity_log_start_telegraph_poles')) has-error @endif">
                                    <label for="inputEmail3" class="col-lg-1 control-label" style="padding-right: 2px;padding-left: 0px;">เลขเสา</label>
                                    <div class="col-lg-1" style="padding: 0px;margin: 0px;">
                                        <input style="padding: 6px;" type="number" id="projects_activity_log_start_telegraph_poles" value="{{Input::old('projects_activity_log_start_telegraph_poles')}}" name="projects_activity_log_start_telegraph_poles" class="form-control"  placeholder="เลข">
                                    </div>
                                </div>
                                <div class="@if($errors->has('projects_activity_log_start_telegraph_poles_total')) has-error @endif">
                                    <label for="inputEmail3" class="control-label" style="float: left; padding-right: 8px;padding-left: 8px;font-size: 18px;font-weight: bolder;">/</label>
                                    <div class="col-lg-1" style="padding: 0px;margin: 0px;">
                                        <input style="padding: 6px;" type="number"  id="projects_activity_log_start_telegraph_poles_total" value="16" name="projects_activity_log_start_telegraph_poles_total" class="form-control"  placeholder="สูงสุด">
                                    </div>
                                </div>

                                <div class="@if($errors->has('projects_activity_log_end_telegraph_poles')) has-error @endif">
                                    <label for="inputEmail3" class="col-lg-1 control-label" style="padding-right: 2px;padding-left: 0px;text-align: center;">ถึง</label>
                                    <div class="col-lg-1" style="padding: 0px;margin: 0px;">
                                        <input style="padding: 6px;" type="number" id="projects_activity_log_end_telegraph_poles" value="{{Input::old('projects_activity_log_end_telegraph_poles')}}" name="projects_activity_log_end_telegraph_poles" class="form-control"  placeholder="เลข">
                                    </div>
                                </div>
                                <div class="@if($errors->has('projects_activity_log_end_telegraph_poles_total')) has-error @endif">
                                    <label for="inputEmail3" class="control-label" style="float: left; padding-right: 8px;padding-left: 8px;font-size: 18px;font-weight: bolder;">/</label>
                                    <div class="col-lg-1" style="padding: 0px;margin: 0px;">
                                        <input style="padding: 6px;" type="number"  id="projects_activity_log_end_telegraph_poles_total" value="16" name="projects_activity_log_end_telegraph_poles_total" class="form-control"  placeholder="สูงสุด">
                                    </div>
                                </div>
                            </div>

                            {{--<div class="form-group">--}}
                            {{--<label for="inputEmail3" class="col-lg-2 control-label" style="padding-right: 2px;padding-left: 0px;">อำเภอ</label>--}}
                            {{--<div class="col-lg-3" style="padding: 0px;margin: 0px;">--}}
                            {{--<input type="text" id="projects_activity_log_amphoe" value="@if(false==empty($purchases['projects_activity_log_amphoe'])){{$purchases['projects_activity_log_amphoe']}}@elseif(false==empty(Input::old('projects_activity_log_amphoe'))){{Input::old('projects_activity_log_amphoe')}}@endif" name="projects_activity_log_amphoe" class="form-control"  placeholder="อำเภอ">--}}
                            {{--</div>--}}
                            {{--<label for="inputEmail3" class="col-lg-2 control-label" style="padding-right: 2px;padding-left: 0px;">จังหวัด</label>--}}
                            {{--<div class="col-lg-4" style="padding: 0px;margin: 0px;">--}}
                            {{--<input type="text" id="projects_activity_log_province" value="@if(false==empty($purchases['projects_activity_log_province'])){{$purchases['projects_activity_log_province']}}@elseif(false==empty(Input::old('projects_activity_log_province'))){{Input::old('projects_activity_log_province')}}@endif" name="projects_activity_log_province" class="form-control"  placeholder="จังหวัด">--}}
                            {{--</div>--}}
                            {{--</div>--}}

                            <div class="form-group @if($errors->has('projects_activity_log_weather')) has-error @endif">
                                <label class="col-lg-3 control-label" for="exampleInputEmail1" style="padding-right: 2px;padding-left: 2px;">สภาพอากาศ</label>
                                <div class="col-lg-9" style="padding: 0px;margin: 0px;">
                                    @foreach($dataWeather as $key=>$value)
                                        <label class="checkbox-inline" style="margin-left: 10px;float: left;">
                                            <input type="checkbox" id="inlineCheckbox1" value="{{$value->ref_project_activity_log_weather_name}}"  name="projects_activity_log_weather[{{$value->ref_project_activity_log_weather_id}}]" > {{$value->ref_project_activity_log_weather_name}}
                                        </label>
                                    @endforeach
                                    <div class="col-lg-5" id="activity_log_weather_other" style="display: none;">
                                        <input style="float: left;" type="text" class=" form-control" name="activity_log_weather_other" placeholder="สภาพอากาศ อื่นๆๆๆ">
                                    </div>
                                </div>
                            </div>

                            <script>
                                $(document).ready(function () {
                                    $("input[name=projects_activity_log_weather_1]").click(function () {
                                        if($("input[name=projects_activity_log_weather_1]").is(':checked')){
                                            $('div#activity_log_weather_other').show();
//                                            console.log('show');
                                        }else{
                                            $('div#activity_log_weather_other').hide();
//                                            console.log('hide');
                                        }
                                    })

                                    $("input[name=projects_activity_log_problem_1]").click(function () {
                                        if($("input[name=projects_activity_log_problem_1]").is(':checked')){
                                            $('div#activity_log_problem_other').show();
                                            console.log('show');
                                        }else{
                                            $('div#activity_log_problem_other').hide();
                                            console.log('hide');
                                        }
                                    })
                                })
                            </script>

                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="exampleInputEmail1" style="padding-right: 2px;padding-left: 0px;">ปัญหาอุปสรรค</label>
                                <div class="col-lg-9" style="padding: 0px;margin: 0px;">
                                    @foreach($dataProblem as $key=>$value)
                                        <label class="checkbox-inline" style="margin-left: 10px;float: left;">
                                            @if(1==$value->ref_projects_activity_log_problem_id)
                                                <input type="checkbox" id="inlineCheckbox1" value="{{$value->ref_projects_activity_log_problem_name}}" name="projects_activity_log_problem_1" > {{$value->ref_projects_activity_log_problem_name}}
                                            @else
                                                <input type="checkbox" id="inlineCheckbox1" value="{{$value->ref_projects_activity_log_problem_name}}" name="projects_activity_log_problem[{{$value->ref_projects_activity_log_problem_id}}]" > {{$value->ref_projects_activity_log_problem_name}}
                                            @endif
                                        </label>
                                    @endforeach
                                    <div class="col-lg-5" id="activity_log_problem_other" style="display: none;">
                                        <input style="float: left;" type="text" class=" form-control" name="activity_log_problem_other" placeholder="ปัญหาอุปสรรค อื่นๆๆๆ">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('projects_activity_log_dailyEvents')) has-error @endif">
                                <label class="col-lg-3 control-label" for="exampleInputEmail1">เหตุการประจำวัน</label>
                                <div class="col-lg-9" style="">
                                    <textarea rows="2" class="form-control" name="projects_activity_log_dailyEvents" placeholder="เหตุการประจำวัน">@if(false==empty(Input::old('projects_activity_log_dailyEvents'))){{Input::old('projects_activity_log_dailyEvents')}}@elseif(false==empty($Bring['projects_activity_log_dailyEvents'])){{$Bring['projects_activity_log_dailyEvents']}}@endif</textarea>
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('projects_activity_log_recommend')) has-error @endif">
                                <label class="col-lg-3 control-label" for="exampleInputEmail1">ข้อเสนอเเนะ</label>
                                <div class="col-lg-9" style="">
                                    <textarea rows="2" class="form-control" name="projects_activity_log_recommend" placeholder="ข้อเสนอเเนะ">@if(false==empty(Input::old('projects_activity_log_recommend'))){{Input::old('projects_activity_log_recommend')}}@elseif(false==empty($Bring['projects_activity_log_recommend'])){{$Bring['projects_activity_log_recommend']}}@endif</textarea>
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('projects_activity_start_date')) has-error @endif">
                                <label for="inputEmail3" class="col-lg-3 control-label" style="padding-right: 2px;padding-left: 0px;">วันที่</label>
                                <div class="col-lg-9">
                                    <input type="text" id="projects_activity_start_date" value="@if(false==empty($purchases['projects_activity_start_date'])){{$purchases['projects_activity_start_date']}}@elseif(false==empty(Input::old('projects_activity_start_date'))){{Input::old('projects_activity_start_date')}}@endif" name="projects_activity_start_date" class="form-control"  placeholder="วันที่">
                                </div>
                                {{--<label for="inputEmail3" class="col-lg-2 control-label" style="padding-right: 2px;padding-left: 0px;">วันที่เสร็จ</label>--}}
                                {{--<div class="col-lg-4">--}}
                                {{--<input type="text" id="projects_activity_end_date" value="@if(false==empty($purchases['projects_activity_end_date'])){{$purchases['projects_activity_end_date']}}@elseif(false==empty(Input::old('projects_activity_end_date'))){{Input::old('projects_activity_end_date')}}@endif" name="projects_activity_end_date" class="form-control" placeholder="วันที่เสร็จ">--}}
                                {{--</div>--}}
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button style="width: 100%;" type="submit" class="btn btn-primary">สร้าง</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

@stop