@extends('layouts.layouts')
@section('content')

    <div class="row" style="margin-top: 10px;">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li><a href="/project/viewImageProjectSub?id={{$dataProjectsActivityLog['project_sub_id']}}">โครงการย่อย</a></li>
                <li class="active">รายการกิจกรรมที่ทำเสร็จเเล้ว</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6">
            <h3>รายการกิจกรรมที่ทำเสร็จเเล้ว</h3>
        </div>
        <div class="col-lg-6">
            <div style="font-size:16px;font-weight:bolder;padding: 5px;background-color: #CCCCCC;border-radius: 5px;border: solid 1px#aaaaaa;">
                <div>หมายเลข: {{$dataSubProject->project_sub_name}} ฝั่ง: {{$dataSubProject->project_position}} </div>
                <div>POR: {{$dataSubProject->amount_POR}} เมตร สำรวจ: {{$dataSubProject->amount_survey}} เมตร</div>
                <div>ผู้บักทึก: {{ \App\Models\Staffs::getNameLastName($dataProjectsActivityLog->id_staff_record)}} ผู้ตรวจสอบ: {{\App\Models\Staffs::getNameLastName($dataProjectsActivityLog->id_staff_check)}}
                <div>สถาพอากาศ: {{$dataProjectsActivityLog->projects_activity_log_weather}} ปัญหา: {{$dataProjectsActivityLog->projects_activity_log_problem}}</div>
            </div>
        </div>
    </div>

    <div class="row" style="padding: 10px;float: right;">
        <div class="col-lg-12">
            <form method="POST" action="/project/addActivityLogDetail?id={{$id}}" class="form-inline">
                <input type="hidden" name="projects_activity_log_id" value="{{$id}}">
                @if (true==$errors->has('projects_activity_id')||true==$errors->has('projects_activity_log_value')||true==$errors->has('id_staff_works'))
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>
                        @endforeach
                    </div>
                @endif
                <div class="form-group @if ($errors->has('projects_activity_id')) has-error @endif">
                    <label for="exampleInputName2">ชื่อกิจกรรม</label>
                    <select name="projects_activity_id" class="form-control">
                        <option value="" >เลือกกิจกรรม</option>
                        @foreach($dataProjectsActivity as $key=>$value)
                            <option @if (Input::old('projects_activity_id')==$value->projects_activity_id) selected @endif value="{{$value->projects_activity_id}}">{{$value->projects_activity_name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group @if ($errors->has('projects_activity_log_value')) has-error @endif">
                    <label for="exampleInputEmail2">จำนวน</label>
                    <input style="width: 100px;" name="projects_activity_log_value" type="number" class="form-control" placeholder="จำนวน" value="@if(false==empty(Input::old('productCount'))){{Input::old('productCount')}}@endif">
                </div>
                <div class="form-group @if ($errors->has('projects_activity_log_value')) has-error @endif">
                    {{--<label for="exampleInputEmail2"></label>--}}
                    <input style="width: 100px;" name="start_at" type="number" class="form-control" placeholder="เริ่ม(เมตร)" value="@if(false==empty(Input::old('start_at'))){{Input::old('start_at')}}@endif">
                </div>
                <div class="form-group @if ($errors->has('projects_activity_log_value')) has-error @endif">
                    {{--<label for="exampleInputEmail2">จำนวน</label>--}}
                    <input style="width: 100px;" name="end_at" type="number" class="form-control" placeholder="ถึง(เมตร)" value="@if(false==empty(Input::old('end_at'))){{Input::old('end_at')}}@endif">
                </div>
                <div class="form-group @if ($errors->has('id_staff_works')) has-error @endif">
                    <label for="exampleInputName2">ผู้รับผิดชอบ</label>
                    <select name="id_staff_works" class="form-control">
                        <option value="">ผู้รับผิดชอบ</option>
                        @foreach($dataStaff as $key=>$value)
                            <option {{(Input::old('id_staff_works')==$value->staff_id || $dataProjectsActivityLog['id_staff_assign']==$value->staff_id)?'selected':''}} value="{{$value->staff_id}}">{{$value->staff_firstname}} {{$value->staff_lastname}}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> เพิ่มงานที่ทำเสร็จเเล้ว</button>
                {{--<a href="/project/printPaperWork?id={{$id}}" class="btn btn-default"><span class="glyphicon glyphicon-print" aria-hidden="true"></span> พิมพ์ใบสั่งงานนี้</a>--}}
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th>ชื่อกิจกรรม</th>
                    <th>จำนวน</th>
                    <th>หน่วย</th>
                    <th>ผู้รับผิดชอบ</th>
                    <th>ลบ</th>
                </tr>
                </thead>
                <tbody>
                @if(false==empty($dataProjectsActivityLogDetail))
                    @foreach($dataProjectsActivityLogDetail as $key=>$value)
                        <tr>
                            <th scope="row">{{$key+1}}</th>
                            <td>{{$value->projects_activity_name}}</td>
                            <td>{{$value->projects_activity_log_detail_value}}</td>
                            <td>{{$value->ref_projects_activity_units_name}}</td>
                            <td>{{$value->staff_firstname}} {{$value->staff_lastname}}</td>
                            <td>
                            <a onclick="return confirm('ยืนยันการลบข้อมูล นี้ ?')" href="/project/removeActivityLogDetail?id={{$value->projects_activity_log_detail_id}}" class="btn btn-danger" >ลบ</a>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop
