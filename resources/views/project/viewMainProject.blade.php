@extends('layouts.layouts')
@section('content')
    <div class="row" style="margin-top: 10px;">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li><a href="/project/viewProject">จัดการโครงการ</a></li>
                <li class="active">จัดการ</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <h3>จัดการ</h3>
        </div>
    </div>
    <div class="row" style="@if(true==empty($projectSub->project_sub_id) and (false==$errors->has('project_sub_name') || false==$errors->has('amount_POR') || false==$errors->has('amount_survey'))) display: none; @endif">
        <div class="col-lg-12" style="padding-bottom: 10px;">
            <form method="POST" action="/project/editSubProject" class="form-inline">
                @if ($errors->has())
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>
                        @endforeach
                    </div>
                @endif
                <input type="hidden" name="type" class="form-control" value="{{$type}}">
                <input type="hidden" name="project_sub_id" value="{{$projectSub->project_sub_id}}" class="form-control"
                       id="exampleInputName2" placeholder="กม.">

                <div class="form-group @if($errors->has('project_sub_name')) has-error @endif">
                    <label for="exampleInputName2">กม.</label>
                    <input type="text" name="project_sub_name" value="@if(false==empty(Input::old('project_sub_name'))){{Input::old('project_sub_name')}}@elseif(false==empty($projectSub->project_sub_name)){{$projectSub->project_sub_name}}@endif"
                           class="form-control" id="exampleInputName2" placeholder="กม.">
                </div>
                <div class="form-group @if($errors->has('amount_POR')) has-error @endif">
                    <label for="exampleInputEmail2">ปริมาณ POR</label>
                    <input type="text" name="amount_POR" value="{{$projectSub->amount_POR}}" class="form-control"
                           id="exampleInputEmail2" placeholder="ปริมาณ POR">
                </div>
                <div class="form-group @if($errors->has('amount_survey')) has-error @endif">
                    <label for="exampleInputEmail2">ปริมาณ สำรวจ</label>
                    <input type="text" name="amount_survey" value="{{$projectSub->amount_survey}}"
                           class="form-control" id="exampleInputEmail2" placeholder="ปริมาณ สำรวจ">
                </div>
                <button type="submit" class="btn btn-warning">แก้ไข</button>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th style="padding:0px;text-align: center" rowspan="2">กม.</th>
                    <th style="padding:0px;text-align: center" colspan="2">ปริมาณงาน(POR)</th>
                    <th style="padding:0px;text-align: center" rowspan="2">ระยะรวม</th>
                    <th style="padding:0px;text-align: center" colspan="2">ปริมาณ(สำรวจ)</th>
                    <th style="padding:0px;text-align: center" rowspan="2">ระยะรวม</th>
                    <th style="padding:0px;text-align: center" colspan="2">ผลงานรวมสะสม</th>
                    <th style="padding:0px;text-align: center" rowspan="2">ระยะรวม</th>
                    <th style="padding:0px;text-align: center" rowspan="2">จัดการ</th>
                </tr>
                <tr>
                    <th style="padding:0px;text-align: center">LT</th>
                    <th style="padding:0px;text-align: center">RT</th>
                    <th style="padding:0px;text-align: center">LT</th>
                    <th style="padding:0px;text-align: center">RT</th>
                    <th style="padding:0px;text-align: center">LT</th>
                    <th style="padding:0px;text-align: center">RT</th>
                </tr>
                </thead>
                <tbody>
                @foreach($dataProjectSub as $key=>$value)
                    <tr style="text-align: center;">
                        <td>
                            <a href="/project/viewImageProjectSub?id={{$value->project_sub_id}}">{{$value->project_sub_name}}</a>
                            @if(\Carbon\Carbon::now()->diffInDays(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$value->updated_at))<=1)
                                <span class="glyphicon glyphicon-warning-sign" aria-hidden="true"></span>
                            @endif
                        </td>
                        <td>{{$value->amount_PORL}}</td>
                        <td>{{$value->amount_PORR}}</td>
                        <td>{{$value->SumPor}}</td>
                        <td>{{$value->amount_surveyL}}</td>
                        <td>{{$value->amount_surveyR}}</td>
                        <td>{{$value->SumSurvey}}</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                        <td>
                            <a href="/project/viewMainProject?id={{$id}}&project_sub_name={{str_replace('+','_',$value->project_sub_name)}}&type=right"
                               class="btn btn-default btn-xs">จัดการ R</a>
                            <a href="/project/viewMainProject?id={{$id}}&project_sub_name={{str_replace('+','_',$value->project_sub_name)}}&type=left"
                               class="btn btn-default btn-xs">จัดการ L</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>



    <div class="modal fade @if(true==$errors->has('project_sub_name')) in @endif"
         style="@if(true==$errors->has('project_sub_name')) display: block; @endif" id="myModalaAddSub" tabindex="-1"
         role="dialog" aria-labelledby="myModalLabel">
        <form method="POST" action="/project/addSubProject">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <a href="/groupBring/viewGroupBring" type="button" class="close" data-dismiss="modal"
                           aria-label="Close"><span aria-hidden="true">&times;</span></a>
                        <h4 class="modal-title" id="myModalLabel">สร้างโครงการย่อย</h4>
                    </div>
                    <div class="modal-body">
                        @if ($errors->has())
                            <div class="alert alert-danger" style="padding: 0px;">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}
                                @endforeach
                            </div>
                        @endif
                        <div class="form-horizontal">
                            <input type="hidden" name="project_id" id="project_id" value="{{$id}}">

                            <div class="form-group @if($errors->has('project_sub_name')) has-error @endif">
                                <label for="inputEmail3" class="col-lg-3 control-label">ชื่อโครงการย่อย</label>

                                <div class="col-lg-9">
                                    <input type="text"
                                           value="@if(false==empty(Input::old('project_sub_name'))){{Input::old('project_sub_name')}}@elseif(false==empty($Bring['project_sub_name'])){{$Bring['project_sub_name']}}@endif"
                                           name="project_sub_name" class="form-control" id="inputEmail3"
                                           placeholder="ชื่อโครงการย่อย">
                                </div>
                            </div>

                            {{--<div class="form-group @if($errors->has('start_at')) has-error @endif">--}}
                            {{--<label for="inputEmail3" class="col-lg-3 control-label">เริ่ม กม.</label>--}}
                            {{--<div class="col-lg-9">--}}
                            {{--<input type="text" value="@if(false==empty(Input::old('start_at'))){{Input::old('start_at')}}@elseif(false==empty($Bring['start_at'])){{$Bring['start_at']}}@endif" name="start_at" class="form-control" placeholder="0.00+000">--}}
                            {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-group @if($errors->has('end_at')) has-error @endif">--}}
                            {{--<label for="inputEmail3" class="col-lg-3 control-label">สิ้นสุด กม.</label>--}}
                            {{--<div class="col-lg-9">--}}
                            {{--<input type="text" value="@if(false==empty(Input::old('end_at'))){{Input::old('end_at')}}@elseif(false==empty($Bring['end_at'])){{$Bring['end_at']}}@endif" name="end_at" class="form-control" placeholder="0.00+000">--}}
                            {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-group @if($errors->has('project_position')) has-error @endif">--}}
                            {{--<label for="inputEmail3" class="col-lg-3 control-label">ตำเหน่ง</label>--}}
                            {{--<div class="col-lg-9">--}}
                            {{--<select class="form-control" name="project_position">--}}
                            {{--<option value="right">ขวา</option>--}}
                            {{--<option value="left">ซ้าย</option>--}}
                            {{--</select>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button style="width: 100%;" type="submit" class="btn btn-primary">สร้าง</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

@stop