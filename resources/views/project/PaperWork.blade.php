<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('page-title', 'PDF Document')</title>

</head>
<body style="font-family: angsana-new;">
<style >
    /*@font-face {*/
    /*font-family: "DejaVuSans";*/
    /*src:url('/font/DejaVuSans.ttf') format("truetype");*/
    /*}*/
    /*h1 { font-family: "Kimberley", sans-serif }*/
    table{
        width:100%;
        border: solid 1px;
    }
    tr{
        width: 100%;
        border: solid 1px;
    }
    td.main{
        border: solid 1px #FFFFff;
        width: 10%;
    }
    td.noneBorder{
        border: solid 1px #ffffff;
        width: 10%;
    {{--font-family: DejaVuSans;--}}
        font-size: 16px;
    }
    td.noneBorderTableHand{
        /*border: solid 1px #ffffff;*/
        width: 10%;
    {{--font-family: DejaVuSans;--}}
        font-size: 18px;
        text-align: center;
        line-height: 0.9;
        /*height: 20px;*/
    }
    td.noneBorderTable{
        /*border: solid 1px #ffffff;*/
        width: 10%;
    {{--font-family: DejaVuSans;--}}
        font-size: 11px;
    }

    /*tr.table>td:first-child{*/
    /*border-bottom: solid 1px;border-top: solid 1px;border-left: solid 1px;*/
    /*}*/
</style>
<table style="">
    <tr>
        <td class="main"></td>
        <td class="main"></td>
        <td class="main"></td>
        <td class="main"></td>
        <td class="main"></td>
        <td class="main"></td>
        <td class="main"></td>
        <td class="main"></td>
        <td class="main"></td>
        <td class="main"></td>
    </tr>
    <tr>
        <td class="noneBorder"><img style="width:100px" src="./image/onem.jpg"/></td>
        <td class="noneBorder" colspan="10" style="text-align: center;">
            <span style="font-size:35px;">ใบสั่งงาน </span><br>
            <span style="font-size:20px;">ชื่อโครงการ {{$dataProjectsActivityLog[0]->project_name}} </span><br>
            <span style="font-size:18px;">วันที่เริ่ม {{$dataProjectsActivityLog[0]->projects_activity_start_date}}</span>
            <span style="font-size:18px;margin-left: 40px;">วันที่สินสุด {{$dataProjectsActivityLog[0]->projects_activity_end_date}}</span>
        </td>
        <td colspan="3" class="noneBorder" style="padding-bottom: 30px;">
            <span style="font-size:16px;">รหัสใบสั่งงาน # {{$dataProjectsActivityLog[0]->projects_activity_log_id}}</span><br>
            <span style="font-size:20px;">ผู้รับผิดชอบ {{$dataProjectsActivityLog[0]->staff_firstname}} {{$dataProjectsActivityLog[0]->staff_lastname}}</span><br>
            {{--<span style="font-size:10px;color: #FFFFff;">ว</span>--}}
        </td>
    </tr>

    <tr>
        <td colspan="14"></td>
    </tr>
    <tr class="table">
        <td  class="noneBorderTableHand"><div style="height: 21px;border:solid 1px;border-bottom: solid 0px;">ลำดับ</div></td>
        <td  class="noneBorderTableHand" colspan="6"><div style="height: 21px;margin-left: -5px;border: solid 1px;border-bottom: solid 0px;">กิจกรรม</div></td>
        <td  class="noneBorderTableHand" colspan="2"><div style="height: 20px;margin-left: -5px;border:solid 1px;">จำนวนปฏิบัติงาน</div></td>
        <td  class="noneBorderTableHand"><div style="height: 21px;margin-left: -5px;border: solid 1px;border-bottom: solid 0px;">ผู้รับผิดชอบ</div></td>
        <td  class="noneBorderTableHand" colspan="2"><div style="height: 20px;margin-left: -5px;border:solid 1px;">ตรวจสอบ(ผ่าน)</div></td>
        <td  class="noneBorderTableHand"><div style="height: 21px;margin-left: -5px;border: solid 1px;border-bottom: solid 0px;">ผู้ตรวจสอบ</div></td>
        {{--<td  class="noneBorderTableHand"><div style="height: 21px;margin-left: -5px;border: solid 1px;border-bottom: solid 0px;">หมายเหตุ</div></td>--}}
    </tr>
    <tr>
        <td class="noneBorderTableHand" ><div style="height: 30px;border:solid 1px;border-top: solid 0px;margin-top: -5px;"></div></td>
        <td class="noneBorderTableHand" colspan="6"><div style="height: 30px;margin-left: -5px;border:solid 1px;margin-top: -5px;border-top: solid 0px;"></div></td>
        <td class="noneBorderTableHand"><div style="height: 30px;margin-left: -5px;border: solid 1px;margin-top: -5px">จำนวน</div></td>
        <td class="noneBorderTableHand"><div style="height: 30px;margin-left: -5px;border: solid 1px;margin-top: -5px">หน่วยนับ</div></td>
        <td class="noneBorderTableHand"><div style="height: 30px;margin-left: -5px;border: solid 1px;margin-top: -5px;border-top: solid 0px;"></div></td>
        <td class="noneBorderTableHand"><div style="height: 30px;margin-left: -5px;border: solid 1px;margin-top: -5px">จำนวน</div></td>
        <td class="noneBorderTableHand"><div style="height: 30px;margin-left: -5px;border: solid 1px;margin-top: -5px">หน่วยนับ</div></td>
        <td class="noneBorderTableHand"><div style="height: 30px;margin-left: -5px;border: solid 1px;margin-top: -5px;border-top: solid 0px;"></div></td>
        {{--<td class="noneBorderTableHand"><div style="height: 30px;margin-left: -5px;border: solid 1px;margin-top: -5px;border-top: solid 0px;"></div></td>--}}
        {{--<td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px;border-top: solid 0px;"></div></td>--}}
    </tr>
    @foreach($dataProjectsActivityLogDetail as $key=>$value)
    <tr>
        <td class="noneBorderTableHand" ><div style="height: 30px;border:solid 1px;margin-top: -5px;"><span style="">{{$key+1}}</span></div></td>
        <td class="noneBorderTableHand" style="text-align: left;" colspan="6"><div style="height: 30px;margin-left: -5px;border:solid 1px;margin-top: -5px;"><span style="margin-left: 5px;">{{$value->projects_activity_name}}</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 30px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="">{{$value->projects_activity_log_detail_value}}</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 30px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="">{{$value->ref_projects_activity_units_name}}</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 30px;;margin-left: -5px;border: solid 1px;margin-top: -5px;"><span style="">{{$value->staff_firstname}} {{$value->staff_lastname}}</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 30px;;margin-left: -5px;border: solid 1px;margin-top: -5px;"><span style=""></span></div></td>
        <td class="noneBorderTableHand"><div style="height: 30px;;margin-left: -5px;border: solid 1px;margin-top: -5px;"><span style="">{{$value->ref_projects_activity_units_name}}</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 30px;;margin-left: -5px;border: solid 1px;margin-top: -5px;"><span style=""></span></div></td>
        {{--<td class="noneBorderTableHand"><div style="height: 30px;;margin-left: -5px;border: solid 1px;margin-top: -5px;"><span style=""></span></div></td>--}}
    </tr>
    @endforeach
    <tr>
        <td colspan="" style="text-align: right;font-size: 20px;">หมายเหตุ</td>

        <td colspan="12"><div style="border: solid 1px;height: 70px;"></div></td>

    </tr>
    <tr>
        <td colspan="13"></td>
    </tr>

    <tr>
        <td colspan="13"></td>
    </tr>
    <tr>
        <td class="noneBorderTableHand" colspan="4">
            <div style="border: solid 1px; margin-top: -5px">
                <div>ผู้รับผิดชอบ</div>
                <div style="margin-top: 10px;">(.........................................)</div>
                <div style="margin-top: 10px;">วันที่................................</div>
            </div>
        </td>
        <td class="noneBorderTableHand" colspan="4">
            <div style="border: solid 1px; margin-top: -5px">
                <div>ผู้ตรวจสอบ</div>
                <div style="margin-top: 10px;">(.........................................)</div>
                <div style="margin-top: 10px;">วันที่................................</div>
            </div>
        </td>
        <td class="noneBorderTableHand" colspan="5">
            <div style="border: solid 1px; margin-top: -5px">
                <div>ผู้บันทึกข้อมูล</div>
                <div style="margin-top: 10px;">(.........................................)</div>
                <div style="margin-top: 10px;">วันที่................................</div>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="13"></td>
    </tr>
</table>

</body>
</html>