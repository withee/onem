<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('page-title', 'PDF Document')</title>

</head>
<body style="font-family: angsana-new;">
<style >
    /*@font-face {*/
    /*font-family: "DejaVuSans";*/
    /*src:url('/font/DejaVuSans.ttf') format("truetype");*/
    /*}*/
    /*h1 { font-family: "Kimberley", sans-serif }*/
    table{
        width:660px;
        /*border: solid 1px;*/
    }
    tr{
        width: 100%;
        border: solid 1px;
    }
    td.main{
        border: solid 1px;
        width: 10%;
    }
    td.noneBorder{
        border: solid 1px #ffffff;
        width: 10%;
    {{--font-family: DejaVuSans;--}}
        font-size: 16px;
    }
    td.noneBorderTableHand{
        /*border: solid 1px #ffffff;*/
        width: 10%;
    {{--font-family: DejaVuSans;--}}
        font-size: 18px;
        text-align: center;
        line-height: 0.9;
        /*height: 20px;*/
    }
    td.noneBorderTable{
        /*border: solid 1px #ffffff;*/
        width: 10%;
    {{--font-family: DejaVuSans;--}}
        font-size: 11px;
    }

    /*tr.table>td:first-child{*/
    /*border-bottom: solid 1px;border-top: solid 1px;border-left: solid 1px;*/
    /*}*/
</style>
<table style="">
    <tr>
        <td ></td>
        <td ></td>
        <td ></td>
        <td ></td>
        <td ></td>
        <td ></td>
        <td ></td>
        <td ></td>
        <td ></td>
        <td ></td>
    </tr>
    <tr>
        <td class="noneBorder" colspan="2"><img style="width:50px" src="./image/onem.jpg"/></td>
        <td class="noneBorder" colspan="6" style="text-align: center;">
            <span style="font-size:35px;">ใบรายงานประจำวัน </span><br>
            <span style="font-size:20px;">ชื่อโครงการ ................................................</span><br>
            <span style="font-size:18px;">วันที่ ...............................................</span>
            {{--<span style="font-size:18px;margin-left: 10px;">วันที่สินสุด ...............................................</span>--}}
        </td>
        <td colspan="2" class="noneBorder" style="padding-bottom: 30px;">
            <span style="font-size:16px;">รหัสใบสั่งงาน # {{date('YmdHms')}}</span><br>
            <span style="font-size:16px;margin-left: 40px;">ทีมที่รับผิดชอบ</span><br>
            <span style="font-size:16px;"> ........................................................</span><br>
            <span style="font-size:16px;">จำนวนคน........................</span><br>
            {{--<span style="font-size:10px;color: #FFFFff;">ว</span>--}}
        </td>
    </tr>
    <tr>
        <td colspan="1"></td>
        <td colspan="9"><span>พื้นที่ปฏิบัตงาน กม. .......................................................</span></td>
    </tr>
    <tr>
        <td colspan="10"></td>
    </tr>
    <tr class="table">
        <td  class="noneBorderTableHand"><div style="height: 21px;border:solid 1px;border-bottom: solid 0px;">ลำดับ</div></td>
        <td  class="noneBorderTableHand" colspan="5"><div style="height: 21px;margin-left: -5px;border: solid 1px;border-bottom: solid 0px;">กิจกรรม</div></td>
        {{--<td  class="noneBorderTableHand" colspan="2"><div style="height: 20px;margin-left: -5px;border:solid 1px;">จำนวนปฏิบัติงาน</div></td>--}}
        <td  class="noneBorderTableHand" colspan="2"><div style="height: 20px;margin-left: -5px;border:solid 1px;">ตรวจสอบ(ผ่าน)</div></td>
        <td  class="noneBorderTableHand" colspan="2"><div style="height: 21px;margin-left: -5px;border: solid 1px;border-bottom: solid 0px;">เมตรที่</div></td>
        <td  class="noneBorderTableHand"><div style="height: 21px;margin-left: -5px;border: solid 1px;border-bottom: solid 0px;">ผู้ตรวจสอบ</div></td>
        {{--<td  class="noneBorderTableHand"><div style="height: 21px;margin-left: -5px;border: solid 1px;border-bottom: solid 0px;">หมายเหตุ</div></td>--}}
    </tr>
    <tr>
        <td class="noneBorderTableHand"><div style="height: 30px;border:solid 1px;border-top: solid 0px;margin-top: -5px;"></div></td>
        {{--<td class="noneBorderTableHand" colspan="5"><div style="height: 30px;margin-left: -5px;border:solid 1px;margin-top: -5px;border-top: solid 0px;"></div></td>--}}
        <td class="noneBorderTableHand" colspan="5"><div style="height: 30px;margin-left: -5px;border: solid 1px;margin-top: -5px;border-top: solid 0px;"></div></td>
        <td class="noneBorderTableHand"><div style="height: 30px;margin-left: -5px;border: solid 1px;margin-top: -5px">จำนวน</div></td>
        <td class="noneBorderTableHand"><div style="height: 30px;margin-left: -5px;border: solid 1px;margin-top: -5px">หน่วยนับ</div></td>
        <td class="noneBorderTableHand"><div style="height: 30px;margin-left: -5px;border: solid 1px;margin-top: -5px">เริ่ม</div></td>
        <td class="noneBorderTableHand"><div style="height: 30px;margin-left: -5px;border: solid 1px;margin-top: -5px">ถึง</div></td>
        <td class="noneBorderTableHand"><div style="height: 30px;margin-left: -5px;border: solid 1px;margin-top: -5px;border-top: solid 0px;"></div></td>
        {{--<td class="noneBorderTableHand"><div style="height: 30px;margin-left: -5px;border: solid 1px;margin-top: -5px;border-top: solid 0px;"></div></td>--}}
        {{--<td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px;border-top: solid 0px;"></div></td>--}}
    </tr>
    @foreach($dataProjectsActivity as $key=>$value)
    <tr>
        <td class="noneBorderTableHand" ><div style="height: 30px;border:solid 1px;margin-top: -5px;"><span style="">{{$key+1}}</span></div></td>
        <td class="noneBorderTableHand" style="text-align: left;" colspan="5"><div style="height: 30px;margin-left: -5px;border:solid 1px;margin-top: -5px;"><span style="margin-left: 5px;">{{$value->projects_activity_name}}</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 30px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color:white;">0</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 30px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color:white;">0</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 30px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color:white;">เมตรที่</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 30px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color:white;">0</span></div></td>
        <td class="noneBorderTableHand"><div style="height: 30px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="color:white;">0</span></div></td>
    </tr>
    @endforeach
    <tr>
        <td  colspan="2" style="text-align: right;font-size: 20px;">เหตุการณ์ประจำวัน</td>
        <td  colspan="8">...................................................................................................................................................................................................................</td>
    </tr>
    <tr>
        <td  colspan="10"><div style="margin-top: -10px;">............................................................................................................................................................................................................................................................</div></td>
    </tr>
    <tr>
        <td  colspan="10"><div style="margin-top: -10px;">............................................................................................................................................................................................................................................................</div></td>
    </tr>
    <tr>
        <td  colspan="2" style="text-align: right;font-size: 20px;">สภาพอากาศ</td>
        <td  colspan="8">...................................................................................................................................................................................................................</td>
    </tr>

    <tr>
        <td  colspan="2" style="text-align: right;font-size: 20px;">ปัญหาอุปสรรค</td>
        <td  colspan="8">...................................................................................................................................................................................................................</td>
    </tr>
    <tr>
        <td  colspan="2" style="text-align: right;font-size: 20px;">ข้อเสนอเเนะ</td>
        <td  colspan="8">...................................................................................................................................................................................................................</td>
    </tr>
    <tr>
        <td  colspan="10"><div style="margin-top: -10px;">............................................................................................................................................................................................................................................................</div></td>
    </tr>
    <tr>
        <td  colspan="10"><div style="margin-top: -10px;">............................................................................................................................................................................................................................................................</div></td>
    </tr>
    <tr>
        <td colspan="10" height="20"></td>
    </tr>
    <tr>
        <td class="noneBorderTableHand" colspan="4">
            <div style="border: solid 1px; margin-top: -5px">
                <div>ผู้รับผิดชอบ</div>
                <div style="margin-top: 10px;">(.........................................)</div>
                <div style="margin-top: 10px;">วันที่................................</div>
            </div>
        </td>
        <td class="noneBorderTableHand" colspan="3">
            <div style="border: solid 1px; margin-top: -5px">
                <div>ผู้ตรวจสอบ</div>
                <div style="margin-top: 10px;">(.........................................)</div>
                <div style="margin-top: 10px;">วันที่................................</div>
            </div>
        </td>
        <td class="noneBorderTableHand" colspan="3">
            <div style="border: solid 1px; margin-top: -5px">
                <div>ผู้บันทึกข้อมูล</div>
                <div style="margin-top: 10px;">(.........................................)</div>
                <div style="margin-top: 10px;">วันที่................................</div>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="10"></td>
    </tr>
</table>

</body>
</html>