@extends('layouts.layouts')
@section('content')
    <div class="row" style="margin-top: 10px;">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li><a href="/project/viewProject">จัดการโครงการ</a></li>
                <li><a href="/project/orderManufacture?id={{$id}}">จัดการสินค้าผลิต</a></li>
                <li class="active">รายงานโครงการ</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <h2>รายงานโครงการ</h2>
        </div>
    </div>
    <div style="display: none;">{{$sum=0}}</div>
    <div class="row">
        <div class="col-lg-12">
            <div>
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">วัถุดิบ</a></li>
                    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">ค่าแรง</a></li>
                    {{--<li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Messages</a></li>--}}
                    {{--<li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Settings</a></li>--}}
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="home" style="padding: 10px;">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th rowspan="2">#</th>
                                <th rowspan="2" style="text-align: center">วันที่</th>
                                <th colspan="2" style="text-align: center">รับสินค้า</th>
                                <th colspan="3" style="text-align: center">เบิกสินค้า</th>
                                <th rowspan="2" style="text-align: right">ราคารวม</th>
                                <th rowspan="2" style="text-align: center">ผู้เบิก</th>
                                <th rowspan="2" style="text-align: center">หมายเหตุ</th>
                                {{--<th rowspan="2" style="text-align: center">จัดการ</th>--}}
                            </tr>
                            <tr>
                                <th>รายการ</th>
                                <th style="text-align: right">จำนวน</th>
                                <th>รายการ</th>
                                <th style="text-align: right">จำนวน</th>
                                <th style="text-align: right">ทุน</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($dataAccounts as $key=>$value)
                                @if($key==0)
                                    <div style="display: none">{{$day=$accounts->formatDate($value->created_at,'j')}}</div>
                                @endif
                                @if(!empty($value->expenses))
                                    <span style="display: none;">{{$costs=\App\Models\ProductsAccount::checkCostsProductId($value->product_id)}}{{$sum+=($costs*$value->expenses)}}</span>
                                @endif
                                <tr style="@if(($day!=$accounts->formatDate($value->created_at,'j')))border-top: solid 2px;@endif">
                                    <td>{{$key+1}}</td>
                                    <td>{{$value->created_at}}</td>
                                    <td>{{(!empty($value->income_product_id)?$value->income_product_id:'-')}}</td>
                                    <td style="text-align: right">{{(!empty($value->income)?number_format($value->income, 2):'-')}}</td>
                                    {{--<td style="text-align: right">0</td>--}}
                                    <td>{{(!empty($value->expenses_product_id)?$value->expenses_product_id:'-')}}</td>
                                    <td style="text-align: right">{{(!empty($value->expenses)?number_format($value->expenses, 2):'-')}}</td>
                                    <td style="text-align: right">
                                        @if(!empty($value->expenses))
                                            @if(!empty($costs))
                                                <a id="ViewCosts" product_id="{{$value->product_id}}" style="cursor: pointer;" data-toggle="modal" data-target="#myModal">{{$costs}}</a>
                                            @else
                                                <a id="ViewCosts" product_id="{{$value->product_id}}" style="color:#FF0000;cursor: pointer;" data-toggle="modal" data-target="#myModal">0</a>
                                            @endif
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td style="text-align: right;font-weight: bolder;">{{(!empty($value->expenses)?($costs*$value->expenses):'-')}}</td>
                                    <td style="text-align: center;">{{$value->payee}}</td>
                                    <td>{{$value->remark}}</td>
                                    {{--<td></td>--}}
                                </tr>
                                @if ($day!=$accounts->formatDate($value->created_at,'j'))
                                    <div style="display: none">{{$day=$accounts->formatDate($value->created_at,'j')}}</div>
                                @endif
                            @endforeach
                            <tr style="font-weight: bolder;font-size: 20px;border-top: thick double #000000;border-bottom: solid 2px #000000;">
                                <td colspan="7">รวม</td>
                                <td style="text-align: right;">{{number_format($sum,2)}}</td>
                                <td colspan="2">บาท</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="profile">
                        <div style="display: none">{{$day=0}}</div>
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-lg-12">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th rowspan="2">#</th>
                                        <th rowspan="2" style="text-align: center">วันที่</th>
                                        <th colspan="2" style="text-align: center">รายรับ</th>
                                        <th colspan="2" style="text-align: center">รายจ่าย</th>
                                        <th rowspan="2" style="text-align: right">รวม</th>
                                        <th rowspan="2" style="text-align: center">ผู้เบิก</th>
                                        <th rowspan="2" style="text-align: center">หมายเหตุ</th>
                                        {{--<th rowspan="2" style="text-align: center">จัดการ</th>--}}
                                    </tr>
                                    <tr>
                                        <th>รายการ</th>
                                        <th style="text-align: right">จำนวน</th>
                                        <th>รายการ</th>
                                        <th style="text-align: right">จำนวน</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($dataInventoryAccounts as $key=>$value)
                                        @if($key==0)
                                            <div style="display: none">{{$day=$inventoryAccounts->formatDate($value->created_at,'j')}}</div>
                                        @endif
                                        <tr style="@if(($day!=$inventoryAccounts->formatDate($value->created_at,'j')))border-top: solid 2px;@endif">
                                            <td>{{$key+1}}</td>
                                            <td>{{$value->created_at}}</td>
                                            <td>{{(!empty($value->income_list)?$value->income_list:'-')}}</td>
                                            <td style="text-align: right">{{(!empty($value->income)?number_format($value->income, 2):'-')}}</td>
                                            <td>{{(!empty($value->expenses_list)?$value->expenses_list:'-')}}</td>
                                            <td style="text-align: right">{{(!empty($value->expenses)?number_format($value->expenses, 2):'-')}}</td>
                                            <td style="text-align: right;font-weight: bolder;">-</td>
                                            <td>{{$value->payee}}</td>
                                            <td>{{$value->remark}}</td>
                                            {{--<td>--}}
                                                {{--<a href="/account/manage?id={{$value->id}}" class="btn btn-primary btn-xs">แก้ไข</a>--}}
                                                {{--<a href="/account/remove?id={{$value->id}}" onclick="return confirm('ยืนยันการลบรายการ')"  class="btn btn-danger btn-xs">ลบ</a>--}}
                                            {{--</td>--}}
                                        </tr>
                                        @if ($day!=$inventoryAccounts->formatDate($value->created_at,'j'))
                                            <div style="display: none">{{$day=$inventoryAccounts->formatDate($value->created_at,'j')}}</div>
                                        @endif
                                    @endforeach
                                    <tr style="font-weight: bold;border-top: double 3px#000000;">
                                        <td colspan="2">รวม</td>
                                        <td colspan="2" style="text-align: right;">{{$sumIncome}}</td>
                                        <td colspan="2" style="text-align: right;">{{$sumExpenses}}</td>
                                        <td style="text-align: right;">{{$sumIncome-$sumExpenses}}</td>
                                        <td colspan="2"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $(document).on('click',"a#ViewCosts",function(){
                $.ajax({
                    url: "/account/listProductsAccounts?id="+$(this).attr('product_id'),
                    type: "get"
                }).success(function (res) {
                    $('div#list_products_accounts').html(res);
                    $('div#list_products_accounts').modal('show');
                })
            })
            $(document).on('click',"button#CloseModal",function(){
                $('div#list_products_accounts').modal('hide');
            })
        })
    </script>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" id="CloseModal"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">รายการซื้อสินค้าย้อนหลัง</h4>
                </div>
                <div class="modal-body">
                    <div style="padding: 5px;" id="list_products_accounts">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="CloseModal">Close</button>
                    {{--<button type="button" class="btn btn-primary">Save changes</button>--}}
                </div>
            </div>
        </div>
    </div>
@stop