<?php
/**
 * Created by PhpStorm.
 * User: sootipong
 * Date: 10/5/2015
 * Time: 13:33
 */
$this->layout = null
?>
<h3>รายการสั่งซื้อของเพิ่ม โครงการ {{$dataProject->project_name}}</h3>
<table border="1" class="table table-bordered">
    <thead>
    <tr>
        <th>ลำดับ</th>
        <th>ชื่อสินค้า</th>
        <th>จำนวน</th>
        <th>คลัง</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data as $key=>$value)
    <tr>
        <td width="50" scope="row">{{$key+1}}</td>
        <td width="250">{{$value['name']}}</td>
        <td width="100">{{$value['count']}}</td>
        <td width="150">{{$value['storename']}}</td>
    </tr>
    @endforeach
    </tbody>
</table>