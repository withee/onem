@extends('layouts.layouts')
@section('content')
    <div class="row" style="margin-top: 10px;">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li><a href="/project/viewSubProject?id={{$dataProjectSub->project_sub_id}}">โครงการย่อย</a></li>
                <li class="active">เพิ่มกิจกรรมการทำงาน</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <h3>เพิ่มกิจกรรมการทำงาน</h3>
        </div>
    </div>

    <script>
//        $(document).ready(function () {
//            $('[data-toggle="tooltip"]').tooltip()
//        })
    </script>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th rowspan="2">กิจกรรม</th>
                    <th style="text-align: center;" colspan="{{$countDay}}">เดือน มิถุนายน</th>
                    <th rowspan="2">รวม</th>
                </tr>
                <tr>
                    @for($i = 1; $i <=$countDay; $i++)
                        <th>{{$i}}</th>
                    @endfor
                </tr>
                </thead>
                <tbody>
                @foreach($dataProjectsActivity as $key=>$value)
                    <?php $sum=0; ?>
                    <tr>
                        <td style="padding: 0px;">{{$value->projects_activity_name}}</td>
                        @for($i = 1; $i <=$countDay; $i++)
                            <td style="padding: 0px;text-align: center;">
                                @if(false==empty($dataWorkMonth[$i][$value->projects_activity_id]))
                                    <span style="display: none;">{{$sum=+$dataWorkMonth[$i][$value->projects_activity_id]['value']}}</span>
                                    <span data-toggle="tooltip" data-placement="left" title="{{$dataWorkMonth[$i][$value->projects_activity_id]['text']}}" class="glyphicon glyphicon-road" aria-hidden="true"></span>
                                @endif
                            </td>

                        @endfor
                        <td style="padding: 0px;text-align: center;font-weight: bolder;">0</td>
                    </tr>
                @endforeach
                <tr>
                    <td style="padding: 0px;font-weight: bolder;">สภาพอากาศ</td>
                    @for($i = 1; $i <=$countDay; $i++)
                        <td style="padding: 0px;text-align: center;">
                            @if($i==10)
                                <span data-toggle="tooltip" data-placement="left" title="" class="glyphicon glyphicon-cloud" aria-hidden="true"></span>
                            @elseif($i==15)
                                <span data-toggle="tooltip" data-placement="left" title="" class="glyphicon glyphicon-flash" aria-hidden="true"></span>
                            @elseif($i==16)
                                <span data-toggle="tooltip" data-placement="left" title="" class="glyphicon glyphicon-cloud-download" aria-hidden="true"></span>
                            @elseif($i==17)
                                <span data-toggle="tooltip" data-placement="left" title="" class="glyphicon glyphicon-certificate" aria-hidden="true"></span>
                            @endif
                        </td>

                    @endfor
                    <td style="padding: 0px;text-align: center;font-weight: bolder;">0</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

@stop