@extends('layouts.layouts')
@section('content')
    <div class="row" style="margin-top: 10px;">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li><a href="/project/viewProject">จัดการโครงการ</a></li>
                <li class="active">รายงานทีมทำงาน</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <h3>รายงานทีมทำงาน</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th rowspan="2">กิจกรรม</th>
                    <th style="text-align: center;" colspan="{{$countDay}}">เดือน {{$monthNames[$countMonth]}}</th>
                    <th rowspan="2">รวม</th>
                </tr>
                <tr>
                    @for($i = 1; $i <=$countDay; $i++)
                        <th>{{$i}}</th>
                    @endfor
                </tr>
                </thead>
                <tbody>
                @foreach($dataStaff as $key=>$value)
                    <div style="display: none;">{{$sum=0}}</div>
                    <tr>
                        <td style="padding: 0px;font-weight: bolder;">{{$value["staff_firstname"]}} {{$value["staff_lastname"]}}</td>
                        @for($i = 1; $i <=$countDay; $i++)
                            <td style="padding: 0px;text-align: center;">
                                @if(false==empty($dataWorkMonth[$i][$value->staff_id]))
                                    <span style="display: none;">{{$sum+=$dataWorkMonth[$i][$value->staff_id]['value']}}</span>
                                    <span data-toggle="tooltip" data-placement="left" title="{{$dataWorkMonth[$i][$value->staff_id]['text']}}" class="glyphicon glyphicon-user" aria-hidden="true"></span>
                                @endif
                            </td>
                        @endfor
                        <td style="padding: 0px;text-align: center;font-weight: bolder;">{{$sum}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@stop