@extends('layouts.layouts')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h3>จัดการโครงการ</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-3" style="padding: 0px;margin: 0px;">
            <form style="float: left;" class="form-inline" method="GET" action="/purchases/viewPurchases" novalidate>
                <div class="input-group input-group-sm">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-edit"
                                                          aria-hidden="true"></span></span>
                    <input type="text" class="form-control" name="search" value="{{$dataProject['search']}}"
                           placeholder="ค้นหา ชื่อโครงการ">
                </div>
                <button type="submit" class="btn btn-sm"><span class="glyphicon glyphicon-search"
                                                               aria-hidden="true"></span></button>
            </form>
        </div>
        <div class="col-lg-9" style="padding-bottom: 10px;">
            <button style="float: right;" type="button" class="btn btn-primary" data-toggle="modal"
                    data-target="#myModal">
                เพิ่มโครงการ
            </button>
        </div>
    </div>

    <div class="row" style="float: right;">
        <div class="col-lg-12">
            <nav>
                <ul class="pagination" style="margin: 0px;padding: 0px;padding-top: 3px;">
                    @if($dataProject['page']>1)
                        <li>
                            <a href="/project/viewProject?page={{$dataProject['page']-1}}@if(false==empty($dataProject['search']))&search={{$dataProject['search']}}@endif">Prev</a>
                        </li>
                    @endif

                    @for($x = 1; $x <= $dataProject['maxProduct']; $x++)
                        @if($x==$dataProject['page'])
                            <li class="active"><a
                                        href="/project/viewProject?page={{$x}}@if(false==empty($dataProject['search']))&search={{$dataProject['search']}}@endif">{{$x}}</a>
                            </li>
                        @else
                            <li>
                                <a href="/project/viewProject?page={{$x}}@if(false==empty($dataProject['search']))&search={{$dataProject['search']}}@endif">{{$x}}</a>
                            </li>
                        @endif
                    @endfor

                    @if($dataProject['page']<$dataProject['maxProduct'])
                        <li>
                            <a href="/project/viewProject?page={{$dataProject['page']+1}}@if(false==empty($dataProject['search']))&search={{$dataProject['search']}}@endif">Next</a>
                        </li>
                    @endif
                </ul>
            </nav>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th colspan="4">ชื่อโครงการ</th>
                    <th>จัดการ</th>
                </tr>
                </thead>
                <tbody>
                @if(false==empty($dataProject))
                    @foreach($dataProject['dataProduct'] as $key=>$value)
                        <tr style="border-top: solid 2px;">
                            <th scope="row">{{$key+1}}</th>
                            <td colspan="4" style="font-weight: bolder;font-size: 20px;">{{$value->project_name}}</td>
                            <td>
                                <a href="/project/orderManufacture?id={{$value->project_id}}" class="btn btn-default"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span> สั่งผลิต</a>
                                <a href="/project/viewMainProject?id={{$value->project_id}}" class="btn btn-success"><span class="glyphicon glyphicon-equalizer" aria-hidden="true"></span> จัดการ</a>
                                <button onclick="myFunction({{$value->project_id}})" type="button"
                                        class="btn btn-warning" data-toggle="modal" data-target="#myModalActivity"><span
                                            class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                                    เพิ่มกิจกรรม
                                </button>
                                <a href="/project/viewStaffWork?id={{$value->project_id}}" class="btn btn-info"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> ผู้ปฏิบัติงาน</a>
                                <a href="/project/viewSendWork?id={{$value->project_id}}" class="btn btn-primary"><span class="glyphicon glyphicon-paste" aria-hidden="true"></span> รายงานงวด</a>
                                <a href="/project/viewPlanWork?id={{$value->project_id}}" class="btn btn-danger"><span class="glyphicon glyphicon-stats" aria-hidden="true"></span> แผน-ผล</a>
                                <a href="/project/printPaperWorkProject?id={{$value->project_id}}" class="btn btn-default"><span class="glyphicon glyphicon-print" aria-hidden="true"></span> ใบตรวจงาน</a>
                                {{--<button onclick="myFunction({{$value->project_id}})" type="button" class="btn btn-default" data-toggle="modal" data-target="#myModalAddProjectsActivityLog"><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> ออกใบสั่งงาน</button>--}}
                            </td>
                        </tr>
                        {{--@if(false==empty($value->deleted_at))--}}
                            {{--<tr style="background-color: #cccccc;">--}}
                                {{--<td style="padding: 2px"></td>--}}
                                {{--<td style="padding: 2px;text-align: center;">ลำดับ</td>--}}
                                {{--<td style="padding: 2px;text-align: center;">ผู้สร้าง</td>--}}
                                {{--<td style="padding: 2px;text-align: center;">กิจกรรม</td>--}}
                                {{--<td style="padding: 2px;text-align: center;">หน่วย</td>--}}
                                {{--<td style="padding: 2px;text-align: center;">จัดการ</td>--}}
                            {{--</tr>--}}
                            {{--@foreach($value->deleted_at as $keyA=>$valueA)--}}
                                {{--<tr>--}}
                                    {{--<td style="padding: 4px"></td>--}}
                                    {{--<td style="padding: 4px">{{$keyA+1}}</td>--}}
                                    {{--<td style="padding: 4px">{{$valueA->staff_firstname}} {{$valueA->staff_lastname}}</td>--}}
                                    {{--<td style="padding: 4px;width: 30%;">{{$valueA->projects_activity_name}}</td>--}}
                                    {{--<td style="padding: 4px">{{$valueA->ref_projects_activity_units_name}}</td>--}}
                                    {{--<td style="padding: 4px"></td>--}}
                                {{--</tr>--}}
                            {{--@endforeach--}}
                        {{--@endif--}}
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>

    <script>
        function myFunction(data) {
            $('input#project_id').val(data);
            $('input#activity_project_id').val(data);
        }
    </script>

    <div class="modal fade @if(true==$errors->has('projects_activity_name')||true==$errors->has('projects_activity_value')||true==$errors->has('projects_activity_unit')) in @endif"
         style="@if(true==$errors->has('projects_activity_name')||true==$errors->has('projects_activity_value')||true==$errors->has('projects_activity_unit')) display: block; @endif"
         id="myModalActivity" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <form method="POST" action="/project/addActivity">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <a href="/project/viewProject" type="button" class="close" data-dismiss="modal"
                           aria-label="Close"><span aria-hidden="true">&times;</span></a>
                        <h4 class="modal-title" id="myModalLabel">เพิ่มกิจกรรมโครงการ</h4>
                    </div>
                    <div class="modal-body">
                        @if ($errors->has())
                            <div class="alert alert-danger" style="padding: 0px;">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}
                                @endforeach
                            </div>
                        @endif
                        <input type="hidden" name="project_id" id="project_id" value="@if(false==empty(Input::old('project_id'))){{Input::old('project_id')}}@elseif(false==empty($Bring['project_id'])){{$Bring['project_id']}}@endif">
                        <input type="hidden" name="projects_activity_value" id="projects_activity_value" value="0">

                        <div class="form-horizontal">
                            <div class="form-group @if($errors->has('projects_activity_name')) has-error @endif">
                                <label for="inputEmail3" class="col-lg-3 control-label">ชื่อกิจกรรม</label>
                                <div class="col-lg-4">
                                    <input type="text"
                                           value="@if(false==empty(Input::old('projects_activity_name'))){{Input::old('projects_activity_name')}}@elseif(false==empty($Bring['projects_activity_name'])){{$Bring['projects_activity_name']}}@endif"
                                           name="projects_activity_name" class="form-control" id="inputEmail3"
                                           placeholder="ชื่อกิจกรรม">
                                </div>
                                <label for="inputEmail3" class="col-lg-1 control-label">หน่วย</label>
                                <div class="col-lg-3">
                                    <select name="projects_activity_unit" class="form-control">
                                        <option value="">เลือกหน่วย</option>
                                        @foreach($dataProjectsActivityUnits as $key=>$value)
                                            <option @if (Input::old('projects_activity_unit')==$value->ref_projects_activity_units_id)
                                                selected
                                                @endif value="{{$value->ref_projects_activity_units_id}}">{{$value->ref_projects_activity_units_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-horizontal">
                            <div class="form-group @if($errors->has('projects_activity_convert_mm')) has-error @endif">
                                <label for="inputEmail3" class="col-lg-3 control-label">แปลงเมตร</label>
                                <div class="col-lg-3">
                                    <input type="text"
                                           value="{{Input::old('projects_activity_convert_mm')}}"
                                           name="projects_activity_convert_mm" class="form-control" id="inputEmail3"
                                           placeholder="หน่วยแปลงเมตร">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button style="width: 100%;" type="submit" class="btn btn-primary">สร้าง</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="modal fade @if(true==$errors->has('project_name')) in @endif"
         style="@if(true==$errors->has('project_name')) display: block; @endif" id="myModal" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel">
        <form method="POST" action="/project/addProject">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <a href="/groupBring/viewGroupBring" type="button" class="close" data-dismiss="modal"
                           aria-label="Close"><span aria-hidden="true">&times;</span></a>
                        <h4 class="modal-title" id="myModalLabel">สร้างโครงการ</h4>
                    </div>
                    <div class="modal-body">
                        @if ($errors->has())
                            <div class="alert alert-danger" style="padding: 0px;">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}
                                @endforeach
                            </div>
                        @endif
                        <div class="form-horizontal">
                            <div class="form-group @if($errors->has('group_bring_name')) has-error @endif">
                                <label for="inputEmail3" class="col-lg-3 control-label">ชื่อโครงการ</label>
                                <div class="col-lg-9">
                                    <input type="text"
                                           value="@if(false==empty(Input::old('project_name'))){{Input::old('project_name')}}@elseif(false==empty($Bring['project_name'])){{$Bring['project_name']}}@endif"
                                           name="project_name" class="form-control" id="inputEmail3"
                                           placeholder="ชื่อโครงการ">
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('group_bring_name')) has-error @endif">
                                <label for="inputEmail3" class="col-lg-3 control-label">เริ่ม กม.</label>
                                <div class="col-lg-9">
                                    <input type="text"
                                           value="@if(false==empty(Input::old('project_name'))){{Input::old('project_name')}}@elseif(false==empty($Bring['project_name'])){{$Bring['project_name']}}@endif"
                                           name="start_km" class="form-control" id="inputEmail3"
                                           placeholder="0+000">
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('group_bring_name')) has-error @endif">
                                <label for="inputEmail3" class="col-lg-3 control-label">ถึง กม.</label>
                                <div class="col-lg-9">
                                    <input type="text"
                                           value="@if(false==empty(Input::old('project_name'))){{Input::old('project_name')}}@elseif(false==empty($Bring['project_name'])){{$Bring['project_name']}}@endif"
                                           name="end_km" class="form-control" id="inputEmail3"
                                           placeholder="0+000">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button style="width: 100%;" type="submit" class="btn btn-primary">สร้าง</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

@stop