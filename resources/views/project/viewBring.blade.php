@extends('layouts.layouts')
@section('content')
    <div class="row" style="margin-top: 10px;">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li><a href="/project/viewSubProject?id={{$dataSubProject->project_sub_id}}">จัดการโครงการย่อย</a></li>
                <li class="active">รายการเบิกสินค้าย้อนหลัง</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <h3>รายการเบิกสินค้าย้อนหลัง {{$dataSubProject->project_sub_name}}</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div subProjectId="{{$dataSubProject->project_sub_id}}" id="chart"></div>
        </div>
    </div>

    <script>
//        $(document).ready(function() {
//            var id=$('#chart').attr('subProjectId');
//            var req = $.ajax({
//                type: 'GET',
//                url: '/project/getChartBringProduct?id='+id,
//                dataType: 'JSON'
//            });
//            req.done(function(res) {
//                if (res.status == 'success') {
//                    obj=[];
//                    var data = new google.visualization.DataTable();
//                    data.addColumn('string', 'ชื่อสินค้า');
//                    for (var index in res.product) {
//                        data.addColumn('number', res.product[index]['product_name']);
//                    }
//                    var id=0;
//                    for (var indexP in res.dataProduct) {
//                        var logDate = [];
//                        logDate.push(indexP);
//                        for (var index in res.product) {
//                            if(typeof (res.dataProduct[indexP][res.product[index]['product_id']])!=='undefined'){
//                                logDate.push(res.dataProduct[indexP][res.product[index]['product_id']]['sum']);
//                            }else{
//                                logDate.push(0);
//                            }
//                        }
//                        obj.push(logDate);
//                    }
//                    data.addRows(obj);
//                    var options = {
//                        width: '100%',
//                        height: 500,
//                        legend: {position: 'bottom'},
//                        is3D: true,
//                        title: 'รายการเบิกสินค้า'
//                    };
//
//                    var chart = new google.visualization.ColumnChart(document.getElementById("chart"));
//                    chart.draw(data, options);
//
//                }
//            })
//        });
    </script>

    <div class="row">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>#</th>
                <th>วันที่</th>
                <th>หมายเลขใบเบิก</th>
                <th colspan="3">ผู้เบิกสินค้า</th>
            </tr>
            </thead>
            <tbody>
            @if(false==empty($dataProductBring))
                @foreach($dataProductBring as $key=>$value)
                <tr style="border-top: solid 2px;font-weight: bolder;font-size: 16px;">
                    <th scope="row">{{$key+1}}</th>
                    <td style="width: 180px;">{{$value->member_bring_date}}</td>
                    <td>{{$value->bring_number}}</td>
                    <td colspan="3">{{$value->staff_firstname}} {{$value->staff_lastname}}</td>
                </tr>
                    @if(false==empty($value->deleted_at))
                        <tr style="padding: 2px;margin: 0px;background-color: #cccccc;font-weight: bolder;">
                            <td style="margin: 0px;padding: 2px;"></td>
                            <td style="margin: 0px;padding: 2px;">สถานะ</td>
                            <td style="margin: 0px;padding: 2px;">ชื่อสินค้า</td>
                            <td style="margin: 0px;padding: 2px;">จำนวน</td>
                            <td style="margin: 0px;padding: 2px;">คลัง</td>
                            <td style="margin: 0px;padding: 2px;">สถานที่ส่ง</td>
                        </tr>
                        @foreach($value->deleted_at as $key=>$value)
                            <tr>
                                <td style="margin: 0px;padding: 2px;"></td>
                                <td style="margin: 0px;padding: 2px;">{{$value->bring_type}}</td>
                                <td style="margin: 0px;padding: 2px;">{{$value->product_name}}</td>
                                <td style="margin: 0px;padding: 2px;">{{$value->bring_detail_count}}</td>
                                <td style="margin: 0px;padding: 2px;">{{$value->storename_name}}</td>
                                <td style="margin: 0px;padding: 2px;">{{$value->bring_detail_location}}</td>
                            </tr>
                        @endforeach
                    @endif
                @endforeach
            @endif
            </tbody>
        </table>
    </div>

@stop