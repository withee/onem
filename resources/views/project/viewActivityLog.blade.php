@extends('layouts.layouts')
@section('content')
    <div class="row" style="margin-top: 10px;">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li><a href="/project/viewSubProject?id={{$dataSubProject->project_sub_id}}">จัดการโครงการย่อย</a></li>
                <li class="active">รายการกิจกรรมย้อนหลัง</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <h3>รายการกิจกรรมย้อนหลัง {{$dataSubProject->project_sub_name}}</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div subProjectId="{{$dataSubProject->project_sub_id}}" id="chart"></div>
        </div>
    </div>

    <script>
//        $(document).ready(function() {
//            var id=$('#chart').attr('subProjectId');
//            var req = $.ajax({
//                type: 'GET',
//                url: '/project/getChartActivitySubProject?id='+id,
//                dataType: 'JSON'
//            });
//            req.done(function(res) {
//                if (res.status == 'success') {
//                    obj=[];
//                    var data = new google.visualization.DataTable();
//                    data.addColumn('string', 'กิจกรรม');
//                    data.addColumn('number', 'จำนวน');
//                    var id=0;
//                    for (var indexP in res.data) {
//                        var logDate = [];
//                        logDate.push(res.data[indexP]['projects_activity_name']);
//                        logDate.push(parseInt( res.data[indexP]['sum']));
//                        obj.push(logDate);
//                    }
//                    console.log(obj);
//                    data.addRows(obj);
//                    var options = {
//                        width: '100%',
//                        height: 500,
//                        legend: {position: 'bottom'},
////                        is3D: true,
//                        title: 'รายการงานที่ทำเสร็จเเล้ว'
//                    };
//
//                    var chart = new google.visualization.ColumnChart(document.getElementById("chart"));
//                    chart.draw(data, options);
//                }
//            })
//        });
    </script>

    <div class="row">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>#</th>
                <th colspan="3">สถานที่ปฏิบัติงาน</th>
                <th colspan="2">เหตุการประจำวัน</th>
                {{--<th>สภาพอากาศ</th>--}}
                {{--<th>ปัญหา</th>--}}
                {{--<th>เสนอเเนะ</th>--}}
                <th colspan="3">ผู้รับผิดชอบ</th>
                {{--<th>จัดการ</th>--}}
            </tr>
            </thead>
            <tbody>
            @foreach($dataActivityLog as $key=>$value)
                <tr style="border-top: solid 2px;font-weight: bolder;font-size: 16px;">
                    <th scope="row">{{$value->created_at}}</th>
                    <td colspan="3">
                        {{($value->projects_activity_log_km)?'กม.ที่ '.$value->projects_activity_log_km:''}}
                        {{($value->projects_activity_log_village)?' บ้าน '.$value->projects_activity_log_village:''}}
                        {{($value->projects_activity_log_tambon)?' ตำบล '.$value->projects_activity_log_tambon:''}}
                        {{($value->projects_activity_log_amphoe)?' อำเภอ '.$value->projects_activity_log_amphoe:''}}
                        {{($value->projects_activity_log_province)?' จังหวัด '.$value->projects_activity_log_province:''}}
                    </td>
                    <td colspan="2" >{{$value->projects_activity_log_dailyEvents}}
                        {{($value->projects_activity_log_weather)?' สภาพอากาศ '.$value->projects_activity_log_weather:''}}
                        {{($value->projects_activity_log_problem)?' ปัญหา '.$value->projects_activity_log_problem:''}}
                        {{($value->projects_activity_log_recommend)?' แนะนำ '.$value->projects_activity_log_recommend:''}}
                    </td>
                    <td colspan="3">{{$value->staff_firstname}} {{$value->staff_lastname}}</td>
                    {{--<td></td>--}}
                </tr>
                @if($value->deleted_at)
                    <tr style="padding: 2px;margin: 0px;background-color: #cccccc;font-weight: bolder;">
                        <td style="margin: 0px;padding: 2px;"></td>
                        <td style="margin: 0px;padding: 2px;width: 180px;">วันที่</td>
                        <td colspan="2" style="margin: 0px;padding: 2px;">กิจกรรม</td>
                        <td style="margin: 0px;padding: 2px;text-align: center;">จำนวน</td>
                        <td style="margin: 0px;padding: 2px;text-align: center;">หน่วย</td>
                        <td style="margin: 0px;padding: 2px;">ผู้ปฏิบัติงาน</td>
                    </tr>
                    @foreach($value->deleted_at as $keyD=>$valueD)
                        <tr>
                            <td style="margin: 0px;padding: 2px;">+{{$valueD->start_at}} ถึง +{{$valueD->end_at}}</td>
                            <td style="margin: 0px;padding: 2px;width: 180px;">{{$valueD->created_at}}</td>
                            <td colspan="2" style="margin: 0px;padding: 2px;">{{$valueD->projects_activity_name}}</td>
                            <td style="margin: 0px;padding: 2px;text-align: right;font-weight: bolder;">{{$valueD->projects_activity_log_detail_value}}</td>
                            <td style="margin: 0px;padding: 2px;">{{$valueD->ref_projects_activity_units_name}}</td>
                            <td style="margin: 0px;padding: 2px;">{{$valueD->staff_firstname}} {{$valueD->staff_lastname}}</td>
                        </tr>
                    @endforeach
                @endif
            @endforeach
            </tbody>
        </table>
    </div>

@stop