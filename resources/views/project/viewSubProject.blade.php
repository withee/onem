@extends('layouts.layouts')
@section('content')

    <div class="row" style="margin-top: 10px;">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li><a href="/project/viewImageProjectSub?id={{$dataProjectSub->project_id}}">รายงานทำงาน</a></li>
                <li class="active">เพิ่มกิจกรรมการทำงาน</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <h3>จัดการโครงการย่อย</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th rowspan="2">กิจกรรม</th>
                    <th style="text-align: center;" colspan="{{$countDay}}">เดือน {{$monthNames[$countMonth]}}</th>
                    <th rowspan="2">รวม</th>
                </tr>
                <tr>
                    @for($i = 1; $i <=$countDay; $i++)
                        <th>{{$i}}</th>
                    @endfor
                </tr>
                </thead>
                <tbody>
                @foreach($dataProjectsActivity as $key=>$value)
                    <span style="display: none;">{{$sum=0}}</span>
                    <tr>
                        <td style="padding: 0px;">{{$value->projects_activity_name}}</td>
                        @for($i = 1; $i <=$countDay; $i++)
                            <td style="padding: 0px;text-align: center;">
                                @if(false==empty($dataWorkMonth[$i][$value->projects_activity_id]))
                                    <span style="display: none;">{{$sum+=$dataWorkMonth[$i][$value->projects_activity_id]['value']}}</span>
                                    <span data-toggle="tooltip" data-placement="left" title="{{$dataWorkMonth[$i][$value->projects_activity_id]['text']}}" class="glyphicon glyphicon-road" aria-hidden="true"></span>
                                @endif
                            </td>
                        @endfor
                        <td style="padding: 0px;text-align: center;font-weight: bolder;">{{$sum}}</td>
                    </tr>
                @endforeach
                <tr>
                    <td style="padding: 0px;font-weight: bolder;">สภาพอากาศ</td>
                    @for($i = 1; $i <=$countDay; $i++)
                        <td style="padding: 0px;text-align: center;">
                            {{--@if($i==10)--}}
                                {{--<span data-toggle="tooltip" data-placement="left" title="" class="glyphicon glyphicon-cloud" aria-hidden="true"></span>--}}
                            {{--@elseif($i==15)--}}
                                {{--<span data-toggle="tooltip" data-placement="left" title="" class="glyphicon glyphicon-flash" aria-hidden="true"></span>--}}
                            {{--@elseif($i==16)--}}
                                {{--<span data-toggle="tooltip" data-placement="left" title="" class="glyphicon glyphicon-cloud-download" aria-hidden="true"></span>--}}
                            {{--@elseif($i==17)--}}
                                {{--<span data-toggle="tooltip" data-placement="left" title="" class="glyphicon glyphicon-certificate" aria-hidden="true"></span>--}}
                            {{--@endif--}}
                        </td>

                    @endfor
                    <td style="padding: 0px;text-align: center;font-weight: bolder;"></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th colspan="4">ชื่อโครงการย่อย</th>
                    <th style="width: 300px;">จัดการ</th>
                </tr>
                </thead>
                <tbody>
                @if(false==empty($dataSubProject))
                    @foreach($dataSubProject as $key=>$value)
                        <tr style="border-top: solid 2px;">
                            <th scope="row">{{$key+1}}</th>
                            <td colspan="4" style="font-weight: bolder;font-size: 20px;">{{$value->project_sub_name}} ({{$value->project_position}})</td>
                            <td>
                                <button onclick="myFunction({{$value->project_sub_id}})" class="btn btn-danger" data-toggle="modal" data-target="#myModalAddProjectsActivityLog"><span class="glyphicon glyphicon-floppy-save" aria-hidden="true"></span> บันทึก</button>
                                <button onclick="myFunction({{$value->project_sub_id}})" class="btn btn-info" data-toggle="modal" data-target="#myModalBring"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> เบิกสินค้า</button>
                                <a href="/project/viewActivityWork?id={{$value->project_sub_id}}" class="btn btn-default"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> รายงาน</a>
                                {{--<button onclick="myFunction({{$value->project_sub_id}})" type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModalActivity"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> เพิ่มกิจกรรม</button>--}}
                                {{--<button onclick="myFunction({{$value->project_sub_id}})" type="button" class="btn btn-default" data-toggle="modal" data-target="#myModalAddProjectsActivityLog"><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> ออกใบสั่งงาน</button>--}}
                            </td>
                        </tr>
                        <tr style="background-color: #888888;">
                            <td colspan="5" style="font-size: 16px;font-weight: bolder;padding-left: 30px;"><span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> รายการกิจกรรม</td>
                            <td><a href="/project/viewActivityLog?id={{$value->project_sub_id}}" class="btn btn-default"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> ดูกิจกรรมย้อนหลัง</a></td>
                        </tr>
                        <tr style="background-color: #cccccc;">
                            <td style="padding: 2px"></td>
                            <td style="padding: 2px;text-align: center;">ลำดับ</td>
                            <td style="padding: 2px;text-align: center;">กิจกรรม</td>
                            <td style="padding: 2px;text-align: center;">จำนวน(เสร็จ)</td>
                            <td style="padding: 2px;text-align: center;">หน่วย</td>
                            {{--<td style="padding: 2px;text-align: center;">เปอร์เซ็น</td>--}}
                            <td style="padding: 2px;text-align: center;">จัดการ</td>
                        </tr>
                        @foreach($value->updated_at as $keyA=>$valueA)
                            <tr>
                                <td style="padding: 4px"></td>
                                <td style="padding: 4px">{{$keyA+1}}</td>
                                <td style="padding: 4px;width: 30%;">{{$valueA->projects_activity_name}}</td>
                                <td style="font-weight: bolder;padding: 4px;text-align: right;">{{$valueA->sum}}</td>
                                <td style="padding: 4px">{{$valueA->ref_projects_activity_units_name}}</td>
                                {{--<td style="padding: 4px">--}}
                                    {{--<div class="progress" style="margin: 0px;" >--}}
                                        {{--<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">--}}
                                            {{--60%--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</td>--}}
                                <td style="padding: 4px"></td>
                            </tr>
                        @endforeach
                        <tr style="background-color: #888888;">
                            <td colspan="5" style="font-size: 16px;font-weight: bolder;padding-left: 30px;"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> รายการสินค้าเบิกไป</td>
                            <td><a href="/project/viewBring?id={{$value->project_sub_id}}" class="btn btn-default"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> ดูเบิกย้อนหลัง</a></td>
                        </tr>
                            <tr style="background-color: #cccccc;">
                                <td style="padding: 2px"></td>
                                <td style="padding: 2px;text-align: center;">ลำดับ</td>
                                <td style="padding: 2px;text-align: center;">ชื่อสินค้าที่เบิกไปผลิต</td>
                                <td style="padding: 2px;text-align: center;">เบิกไปเเล้ว</td>
                                <td style="padding: 2px;text-align: center;">หน่วย</td>
                                <td style="padding: 2px;text-align: center;">จัดการ</td>
                            </tr>
                                @foreach($value->deleted_at as $keyD=>$valueD)
                                <tr>

                                    <td style="padding: 4px"></td>
                                    <td style="padding: 4px">{{$keyD+1}}</td>
                                    <td style="padding: 4px">{{$valueD->product_name}}</td>
                                    <td style="font-weight: bolder;padding: 4px;text-align: right;">{{$valueD->count}}</td>
                                    <td style="padding: 4px">{{$valueD->unit_name}}</td>
                                    <td style="padding: 4px"></td>
                                </tr>
                                @endforeach
                        <tr style=""><td colspan="5"></td></tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>

    <script>
        function myFunction(data) {
//            $('input#project_id').val(data);
            $('input#activity_project_id').val(data);
            $('input#project_sub_id').val(data);
        }
    </script>


    <div class="modal fade @if(true==$errors->has('bring_number')||true==$errors->has('bring_date')||true==$errors->has('member_bring_date')||true==$errors->has('id_member_bring')) in @endif" style="@if(true==$errors->has('bring_number')||true==$errors->has('bring_date')||true==$errors->has('member_bring_date')||true==$errors->has('id_member_bring')) display: block; @endif" id="myModalBring" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <form method="POST" action="/bring/addBring" >
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">กรอกรายละเอียดใบเบิกสินค้า</h4>
                    </div>
                    <div class="modal-body">
                        @if ($errors->has())
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </div>
                        @endif
                        <div class="form-horizontal">
                            <input id="project_sub_id" type="hidden" name="project_sub_id" value="{{Input::old('project_sub_id')}}">
                            <div class="form-group @if($errors->has('bring_number')) has-error @endif">
                                <label for="inputEmail3" class="col-lg-3 control-label">หมายเลขใบเบิก</label>
                                <div class="col-lg-9">
                                    <input type="text" value="@if(false==empty(Input::old('bring_number'))){{Input::old('bring_number')}}@endif" name="bring_number" class="form-control" id="inputEmail3" placeholder="หมายเลขใบเบิก">
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('bring_date')) has-error @endif">
                                <label for="inputEmail3" class="col-lg-3 control-label">วันที่เบิก</label>
                                <div class="col-lg-9">
                                    <input type="text" id="bring_date" value="@if(false==empty(Input::old('bring_date'))){{Input::old('bring_date')}}@endif" name="bring_date" class="form-control" id="inputEmail3" placeholder="วันที่เบิก">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-lg-3 control-label">ประเภท</label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox1" name="bring_type_manufacture" > สำหรับใช้ในงานผลิต
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox2" name="bring_type_project" > สำหรับใช้ในโครงการ
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox3" name="bring_type_other" > อื่น
                                </label>
                            </div>

                            {{--************************รายชื่อผู้เบิกของ*******************--}}
                            <div class="form-group @if($errors->has('id_member_bring')) has-error @endif">
                                <label for="inputPassword3" class="col-lg-3 control-label">ผู้ขอเบิก</label>
                                <div class="col-lg-5">
                                    <select class="form-control" name="id_member_bring">
                                        <option value="">เลือกผู้ขอเบิก</option>
                                        @foreach($dataStaff as $key=>$value)
                                            <option  @if (Input::old('id_member_bring')==$value->staff_id) selected @endif value="{{$value->staff_id}}">{{$value->staff_firstname}} {{$value->staff_lastname}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4 @if($errors->has('member_bring_date')) has-error @endif" style="">
                                    <input type="text"  value="@if(false==empty(Input::old('member_bring_date'))){{Input::old('member_bring_date')}}@endif" name="member_bring_date" class="form-control" id="member_bring_date" placeholder="วันที่ผู้ขอเบิก">
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('id_member_approve_bring')) has-error @endif">
                                <label for="inputPassword3" class="col-lg-3 control-label">ผู้อนุมัตเบิก</label>
                                <div class="col-lg-5">
                                    <select class="form-control" name="id_member_approve_bring">
                                        <option value="">เลือกผู้อนุมัตเบิก</option>
                                        @foreach($dataStaff as $key=>$value)
                                            <option  @if (Input::old('id_member_approve_bring')==$value->staff_id) selected @endif value="{{$value->staff_id}}">{{$value->staff_firstname}} {{$value->staff_lastname}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4 @if($errors->has('member_approve_bring_date')) has-error @endif" style="">
                                    <input type="text"  value="@if(false==empty(Input::old('member_approve_bring_date'))){{Input::old('member_approve_bring_date')}}@endif" name="member_approve_bring_date" class="form-control" id="member_approve_bring_date" placeholder="วันที่อนุมัติเบิก">
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('id_member_approve_pay')) has-error @endif">
                                <label for="inputPassword3" class="col-lg-3 control-label">ผู้อนุมัติจ่าย</label>
                                <div class="col-lg-5">
                                    <select class="form-control" name="id_member_approve_pay">
                                        <option value="">เลือกผู้อนุมัติจ่าย</option>
                                        @foreach($dataStaff as $key=>$value)
                                            <option  @if (Input::old('id_member_approve_pay')==$value->staff_id) selected @endif value="{{$value->staff_id}}">{{$value->staff_firstname}} {{$value->staff_lastname}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4 @if($errors->has('member_approve_pay_date')) has-error @endif" style="">
                                    <input type="text"  value="@if(false==empty(Input::old('member_approve_pay_date'))){{Input::old('member_approve_pay_date')}}@endif" name="member_approve_pay_date" class="form-control" id="member_approve_pay_date" placeholder="วันที่อนุมัติจ่าย">
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('id_member_record')) has-error @endif">
                                <label for="inputPassword3" class="col-lg-3 control-label">เจ้าหน้าที่คลัง</label>
                                <div class="col-lg-5">
                                    <select class="form-control" name="id_member_store">
                                        <option value="">เลือกเจ้าหน้าที่คลัง</option>
                                        @foreach($dataStaff as $key=>$value)
                                            <option  @if (Input::old('id_member_store')==$value->staff_id) selected @endif value="{{$value->staff_id}}">{{$value->staff_firstname}} {{$value->staff_lastname}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4  @if($errors->has('member_store_date')) has-error @endif" style="">
                                    <input type="text" value="@if(false==empty(Input::old('member_store_date'))){{Input::old('member_store_date')}}@endif" name="member_store_date" class="form-control" id="member_store_date" placeholder="วันที่จ่ายของ">
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('id_member_account')) has-error @endif">
                                <label for="inputPassword3" class="col-lg-3 control-label">บัญชี</label>
                                <div class="col-lg-5">
                                    <select class="form-control" name="id_member_account">
                                        <option value="">เลือกฝ่ายบัญชี</option>
                                        @foreach($dataStaff as $key=>$value)
                                            <option  @if (Input::old('id_member_account')==$value->staff_id) selected @endif value="{{$value->staff_id}}">{{$value->staff_firstname}} {{$value->staff_lastname}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4 @if($errors->has('member_account_date')) has-error @endif" style="">
                                    <input type="text" value="@if(false==empty(Input::old('member_account_date'))){{Input::old('member_account_date')}}@endif" name="member_account_date" class="form-control" id="member_account_date" placeholder="วันที่ตรวจสอบ">
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('bring_detail')) has-error @endif">
                                <label class="col-lg-3 control-label" for="exampleInputEmail1">รายละเอียด</label>
                                <div class="col-lg-9" style="">
                                    <textarea rows="4" class="form-control" value="@if(false==empty(Input::old('bring_detail'))){{Input::old('bring_detail')}}@endif" name="bring_detail" placeholder="รายละเอียด"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        {{--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                        <button style="width: 100%;" type="submit" class="btn btn-primary">เพิ่ม</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="modal fade @if(true==$errors->has('projects_activity_start_date')||true==$errors->has('projects_activity_end_date')||true==$errors->has('id_staff_assign')) in @endif" style="@if(true==$errors->has('projects_activity_end_date')||true==$errors->has('projects_activity_start_date')||true==$errors->has('id_staff_assign')) display: block; @endif" id="myModalAddProjectsActivityLog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <form method="POST" action="/project/addActivityLog" >
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <a href="/project/viewProject" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                        <h4 class="modal-title" id="myModalLabel">สรุปการทำงานประจำวัน</h4>
                    </div>
                    <div class="modal-body">
                        @if ($errors->has())
                            <div class="alert alert-danger" style="padding: 0px;">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}
                                @endforeach
                            </div>
                        @endif
                        <div class="form-horizontal">
                            <input type="hidden" name="project_id" id="project_id" value="{{$dataProjectSub->project_id}}">
                            <input type="hidden" name="project_sub_id" id="project_sub_id" value="{{Input::old('project_sub_id')}}">

                            <div class="form-group @if($errors->has('id_staff_assign')) has-error @endif">
                                <label for="inputPassword3" class="col-lg-3 control-label">ชื่อทีม</label>
                                <div class="col-lg-9">
                                    <select class="form-control" name="id_staff_assign">
                                        <option value="">เลือกชื่อทีม</option>
                                        @foreach($dataStaff as $key=>$value)
                                            <option {{(Input::old('id_staff_assign')==$value->staff_id||(false==empty($purchases['id_staff_assign'])&&$purchases['id_staff_assign']==$value->staff_id))?'selected':''}} value="{{$value->staff_id}}">{{$value->staff_firstname}} {{$value->staff_lastname}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('id_staff_check')) has-error @endif">
                                <label for="inputPassword3" class="col-lg-3 control-label">ชื่อผู้ตรวจสอบ</label>
                                <div class="col-lg-9">
                                    <select class="form-control" name="id_staff_check">
                                        <option value="">เลือกชื่อผู้ตรวจสอบ</option>
                                        @foreach($dataStaff as $key=>$value)
                                            <option {{(Input::old('id_staff_check')==$value->staff_id||(false==empty($purchases['id_staff_check'])&&$purchases['id_staff_check']==$value->staff_id))?'selected':''}} value="{{$value->staff_id}}">{{$value->staff_firstname}} {{$value->staff_lastname}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group" style="display: none;" >
                                <div class="@if($errors->has('projects_activity_log_km')) has-error @endif">
                                    <label for="inputEmail3" class="col-lg-3 control-label" style="padding-right: 2px;padding-left: 0px;">กม.ที่</label>
                                    <div class="col-lg-2" style="padding: 0px;margin: 0px;">
                                        <input type="text" id="projects_activity_log_km" value="0" name="projects_activity_log_km" class="form-control"  placeholder="กม.ที่">
                                    </div>
                                </div>
                                <div class="@if($errors->has('projects_activity_log_start_telegraph_poles')) has-error @endif">
                                    <label for="inputEmail3" class="col-lg-1 control-label" style="padding-right: 2px;padding-left: 0px;">เลขเสา</label>
                                    <div class="col-lg-1" style="padding: 0px;margin: 0px;">
                                        <input style="padding: 6px;" type="number" id="projects_activity_log_start_telegraph_poles" value="{{Input::old('projects_activity_log_start_telegraph_poles')}}" name="projects_activity_log_start_telegraph_poles" class="form-control"  placeholder="เลข">
                                    </div>
                                </div>
                                <div class="@if($errors->has('projects_activity_log_start_telegraph_poles_total')) has-error @endif">
                                    <label for="inputEmail3" class="control-label" style="float: left; padding-right: 8px;padding-left: 8px;font-size: 18px;font-weight: bolder;">/</label>
                                    <div class="col-lg-1" style="padding: 0px;margin: 0px;">
                                        <input style="padding: 6px;" type="number"  id="projects_activity_log_start_telegraph_poles_total" value="16" name="projects_activity_log_start_telegraph_poles_total" class="form-control"  placeholder="สูงสุด">
                                    </div>
                                </div>

                                <div class="@if($errors->has('projects_activity_log_end_telegraph_poles')) has-error @endif">
                                    <label for="inputEmail3" class="col-lg-1 control-label" style="padding-right: 2px;padding-left: 0px;text-align: center;">ถึง</label>
                                    <div class="col-lg-1" style="padding: 0px;margin: 0px;">
                                        <input style="padding: 6px;" type="number" id="projects_activity_log_end_telegraph_poles" value="{{Input::old('projects_activity_log_end_telegraph_poles')}}" name="projects_activity_log_end_telegraph_poles" class="form-control"  placeholder="เลข">
                                    </div>
                                </div>
                                <div class="@if($errors->has('projects_activity_log_end_telegraph_poles_total')) has-error @endif">
                                    <label for="inputEmail3" class="control-label" style="float: left; padding-right: 8px;padding-left: 8px;font-size: 18px;font-weight: bolder;">/</label>
                                    <div class="col-lg-1" style="padding: 0px;margin: 0px;">
                                        <input style="padding: 6px;" type="number"  id="projects_activity_log_end_telegraph_poles_total" value="16" name="projects_activity_log_end_telegraph_poles_total" class="form-control"  placeholder="สูงสุด">
                                    </div>
                                </div>
                            </div>

                            {{--<div class="form-group">--}}
                                {{--<label for="inputEmail3" class="col-lg-2 control-label" style="padding-right: 2px;padding-left: 0px;">อำเภอ</label>--}}
                                {{--<div class="col-lg-3" style="padding: 0px;margin: 0px;">--}}
                                    {{--<input type="text" id="projects_activity_log_amphoe" value="@if(false==empty($purchases['projects_activity_log_amphoe'])){{$purchases['projects_activity_log_amphoe']}}@elseif(false==empty(Input::old('projects_activity_log_amphoe'))){{Input::old('projects_activity_log_amphoe')}}@endif" name="projects_activity_log_amphoe" class="form-control"  placeholder="อำเภอ">--}}
                                {{--</div>--}}
                                {{--<label for="inputEmail3" class="col-lg-2 control-label" style="padding-right: 2px;padding-left: 0px;">จังหวัด</label>--}}
                                {{--<div class="col-lg-4" style="padding: 0px;margin: 0px;">--}}
                                    {{--<input type="text" id="projects_activity_log_province" value="@if(false==empty($purchases['projects_activity_log_province'])){{$purchases['projects_activity_log_province']}}@elseif(false==empty(Input::old('projects_activity_log_province'))){{Input::old('projects_activity_log_province')}}@endif" name="projects_activity_log_province" class="form-control"  placeholder="จังหวัด">--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            <div class="form-group @if($errors->has('projects_activity_log_weather')) has-error @endif">
                                <label class="col-lg-3 control-label" for="exampleInputEmail1" style="padding-right: 2px;padding-left: 2px;">สภาพอากาศ</label>
                                <div class="col-lg-9" style="padding: 0px;margin: 0px;">
                                    @foreach($dataWeather as $key=>$value)
                                    <label class="checkbox-inline" style="margin-left: 10px;float: left;">
                                        <input type="checkbox" id="inlineCheckbox1" value="{{$value->ref_project_activity_log_weather_name}}"  name="projects_activity_log_weather[{{$value->ref_project_activity_log_weather_id}}]" > {{$value->ref_project_activity_log_weather_name}}
                                    </label>
                                    @endforeach
                                        <div class="col-lg-5" id="activity_log_weather_other" style="display: none;">
                                            <input style="float: left;" type="text" class=" form-control" name="activity_log_weather_other" placeholder="สภาพอากาศ อื่นๆๆๆ">
                                        </div>
                                </div>
                            </div>

                            <script>
                                $(document).ready(function () {
                                    $("input[name=projects_activity_log_weather_1]").click(function () {
                                        if($("input[name=projects_activity_log_weather_1]").is(':checked')){
                                            $('div#activity_log_weather_other').show();
//                                            console.log('show');
                                        }else{
                                            $('div#activity_log_weather_other').hide();
//                                            console.log('hide');
                                        }
                                    })

                                    $("input[name=projects_activity_log_problem_1]").click(function () {
                                        if($("input[name=projects_activity_log_problem_1]").is(':checked')){
                                            $('div#activity_log_problem_other').show();
                                            console.log('show');
                                        }else{
                                            $('div#activity_log_problem_other').hide();
                                            console.log('hide');
                                        }
                                    })
                                })
                            </script>

                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="exampleInputEmail1" style="padding-right: 2px;padding-left: 0px;">ปัญหาอุปสรรค</label>
                                <div class="col-lg-9" style="padding: 0px;margin: 0px;">
                                    @foreach($dataProblem as $key=>$value)
                                    <label class="checkbox-inline" style="margin-left: 10px;float: left;">
                                        @if(1==$value->ref_projects_activity_log_problem_id)
                                            <input type="checkbox" id="inlineCheckbox1" value="{{$value->ref_projects_activity_log_problem_name}}" name="projects_activity_log_problem_1" > {{$value->ref_projects_activity_log_problem_name}}
                                        @else
                                            <input type="checkbox" id="inlineCheckbox1" value="{{$value->ref_projects_activity_log_problem_name}}" name="projects_activity_log_problem[{{$value->ref_projects_activity_log_problem_id}}]" > {{$value->ref_projects_activity_log_problem_name}}
                                        @endif
                                    </label>
                                    @endforeach
                                        <div class="col-lg-5" id="activity_log_problem_other" style="display: none;">
                                            <input style="float: left;" type="text" class=" form-control" name="activity_log_problem_other" placeholder="ปัญหาอุปสรรค อื่นๆๆๆ">
                                        </div>
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('projects_activity_log_dailyEvents')) has-error @endif">
                                <label class="col-lg-3 control-label" for="exampleInputEmail1">เหตุการประจำวัน</label>
                                <div class="col-lg-9" style="">
                                    <textarea rows="2" class="form-control" name="projects_activity_log_dailyEvents" placeholder="เหตุการประจำวัน">@if(false==empty(Input::old('projects_activity_log_dailyEvents'))){{Input::old('projects_activity_log_dailyEvents')}}@elseif(false==empty($Bring['projects_activity_log_dailyEvents'])){{$Bring['projects_activity_log_dailyEvents']}}@endif</textarea>
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('projects_activity_log_recommend')) has-error @endif">
                                <label class="col-lg-3 control-label" for="exampleInputEmail1">ข้อเสนอเเนะ</label>
                                <div class="col-lg-9" style="">
                                    <textarea rows="2" class="form-control" name="projects_activity_log_recommend" placeholder="ข้อเสนอเเนะ">@if(false==empty(Input::old('projects_activity_log_recommend'))){{Input::old('projects_activity_log_recommend')}}@elseif(false==empty($Bring['projects_activity_log_recommend'])){{$Bring['projects_activity_log_recommend']}}@endif</textarea>
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('projects_activity_start_date')) has-error @endif">
                                <label for="inputEmail3" class="col-lg-3 control-label" style="padding-right: 2px;padding-left: 0px;">วันที่</label>
                                <div class="col-lg-9">
                                    <input type="text" id="projects_activity_start_date" value="@if(false==empty($purchases['projects_activity_start_date'])){{$purchases['projects_activity_start_date']}}@elseif(false==empty(Input::old('projects_activity_start_date'))){{Input::old('projects_activity_start_date')}}@endif" name="projects_activity_start_date" class="form-control"  placeholder="วันที่">
                                </div>
                                {{--<label for="inputEmail3" class="col-lg-2 control-label" style="padding-right: 2px;padding-left: 0px;">วันที่เสร็จ</label>--}}
                                {{--<div class="col-lg-4">--}}
                                    {{--<input type="text" id="projects_activity_end_date" value="@if(false==empty($purchases['projects_activity_end_date'])){{$purchases['projects_activity_end_date']}}@elseif(false==empty(Input::old('projects_activity_end_date'))){{Input::old('projects_activity_end_date')}}@endif" name="projects_activity_end_date" class="form-control" placeholder="วันที่เสร็จ">--}}
                                {{--</div>--}}
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button style="width: 100%;" type="submit" class="btn btn-primary">สร้าง</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="modal fade @if(true==$errors->has('project_sub_name')) in @endif" style="@if(true==$errors->has('project_sub_name')) display: block; @endif" id="myModalaAddSub" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <form method="POST" action="/project/addSubProject" >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <a href="/groupBring/viewGroupBring" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title" id="myModalLabel">สร้างโครงการย่อย</h4>
                </div>
                    <div class="modal-body">
                        @if ($errors->has())
                        <div class="alert alert-danger" style="padding: 0px;">
                            @foreach ($errors->all() as $error)
                                {{ $error }}
                            @endforeach
                        </div>
                        @endif
                        <div class="form-horizontal">
                            <input type="hidden" name="project_id" id="project_id" value="{{$dataProjectSub->project_id}}">
                            <div class="form-group @if($errors->has('project_sub_name')) has-error @endif">
                                <label for="inputEmail3" class="col-lg-3 control-label">ชื่อโครงการย่อย</label>
                                <div class="col-lg-9">
                                    <input type="text" value="@if(false==empty(Input::old('project_sub_name'))){{Input::old('project_sub_name')}}@elseif(false==empty($Bring['project_sub_name'])){{$Bring['project_sub_name']}}@endif" name="project_sub_name" class="form-control" id="inputEmail3" placeholder="ชื่อโครงการย่อย">
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('start_at')) has-error @endif">
                                <label for="inputEmail3" class="col-lg-3 control-label">เริ่ม กม.</label>
                                <div class="col-lg-9">
                                    <input type="text" value="@if(false==empty(Input::old('start_at'))){{Input::old('start_at')}}@elseif(false==empty($Bring['start_at'])){{$Bring['start_at']}}@endif" name="start_at" class="form-control" placeholder="0.00+000">
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('end_at')) has-error @endif">
                                <label for="inputEmail3" class="col-lg-3 control-label">สิ้นสุด กม.</label>
                                <div class="col-lg-9">
                                    <input type="text" value="@if(false==empty(Input::old('end_at'))){{Input::old('end_at')}}@elseif(false==empty($Bring['end_at'])){{$Bring['end_at']}}@endif" name="end_at" class="form-control" placeholder="0.00+000">
                                </div>
                            </div>

                            <div class="form-group @if($errors->has('project_position')) has-error @endif">
                                <label for="inputEmail3" class="col-lg-3 control-label">ตำเหน่ง</label>
                                <div class="col-lg-9">
                                    <select class="form-control" name="project_position">
                                        <option value="right">ขวา</option>
                                        <option value="left">ซ้าย</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="modal-footer">
                    <button style="width: 100%;" type="submit" class="btn btn-primary">สร้าง</button>
                </div>
            </div>
        </div>
    </form>
    </div>


@stop