@extends('layouts.layouts')
@section('content')
    <div class="row" style="margin-top: 10px;">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li><a href="/project/viewProject">จัดการโครงการ</a></li>
                <li class="active">จัดการ</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <h3>รายการ แผน-ผล</h3>
        </div>
    </div>
    <style>
        tr.SumTR>td{
            padding: 0px !important;
            text-align: center;
        }
    </style>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th rowspan="2">ลำดับ</th>
                    <th rowspan="2">รายการ</th>
                    <th>ปริมาณ</th>
                    <th>มูลค่า</th>
                    <th rowspan="2">เดือน</th>
                    <th colspan="10">ระยะเวลา 270 วัน</th>
                    <th>งาน</th>
                </tr>
                <tr>
                    <th>(เมตร)</th>
                    <th>(บาท)</th>
                    <th>ก.ค.58</th>
                    <th>ส.ค.58</th>
                    <th>ก.ย.58</th>
                    <th>ต.ค.58</th>
                    <th>พ.ย.58</th>
                    <th>ธ.ค.58</th>
                    <th>ม.ค.58</th>
                    <th>ก.พ.58</th>
                    <th>มี.ค.58</th>
                    <th>เม.ย.58</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th rowspan="2">1</th>
                    <td rowspan="2">งานติดตั้งรั้วสองข้างทางรถไฟ ติดตั้งรั้วจาก กม.338+500-365+500</td>
                    <td rowspan="2">52,411.00</td>
                    <td rowspan="2">70,267,289.72</td>
                    <td>แผน</td>
                    <td>1.98%</td>
                    <td>5.38%</td>
                    <td>11.68%</td>
                    <td>11.07%</td>
                    <td>11.58%</td>
                    <td>11.16%</td>
                    <td>14.53%</td>
                    <td>15.48%</td>
                    <td>11.42%</td>
                    <td>5.72%</td>
                    <td></td>
                </tr>
                <tr>
                    <th>ผล</th>
                    <td>0.00%</td>
                    <td>6.49%</td>
                    <td>10.47%</td>
                    <td>8.14%</td>
                    <td>16.75%</td>
                    <td>8.69%</td>
                    <td>8.45%</td>
                    <td>16.81%</td>
                    <td>12.70%</td>
                    <td>6.80%</td>
                    <td></td>
                </tr>
                <tr class="SumTR">
                    <td></td>
                    <td>รวม</td>
                    <td>52,411.00</td>
                    <td>70,267,289.72</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr class="SumTR">
                    <td></td>
                    <td colspan="2" style="text-align: left;">แผนงาน</td>
                    <td colspan="2">%แผนงานเเต่ละเดือน</td>
                    <td>1.98%</td>
                    <td>5.38%</td>
                    <td>11.68%</td>
                    <td>11.07%</td>
                    <td>11.58%</td>
                    <td>11.16%</td>
                    <td>14.53%</td>
                    <td>15.48%</td>
                    <td>11.42%</td>
                    <td>5.72%</td>
                    <td></td>
                </tr>
                <tr class="SumTR">
                    <td></td>
                    <td colspan="2" style="text-align: left;">กราฟแผนงาน</td>
                    <td colspan="2">%แผนงานสะสม</td>
                    <td>1.98%</td>
                    <td>5.38%</td>
                    <td>11.68%</td>
                    <td>11.07%</td>
                    <td>11.58%</td>
                    <td>11.16%</td>
                    <td>14.53%</td>
                    <td>15.48%</td>
                    <td>11.42%</td>
                    <td>5.72%</td>
                    <td></td>
                </tr>
                <tr class="SumTR">
                    <td></td>
                    <td colspan="2" style="text-align: left;">ผลงาน</td>
                    <td colspan="2">%แผนงานเเต่ละเดือน</td>
                    <td>0.00%</td>
                    <td>6.49%</td>
                    <td>10.47%</td>
                    <td>8.14%</td>
                    <td>16.75%</td>
                    <td>8.69%</td>
                    <td>8.45%</td>
                    <td>16.81%</td>
                    <td>12.70%</td>
                    <td>6.80%</td>
                    <td></td>
                </tr>
                <tr class="SumTR">
                    <td></td>
                    <td colspan="2" style="text-align: left;">กราฟผลงาน</td>
                    <td colspan="2">%ผลการสะสม</td>
                    <td>0.00%</td>
                    <td>6.49%</td>
                    <td>10.47%</td>
                    <td>8.14%</td>
                    <td>16.75%</td>
                    <td>8.69%</td>
                    <td>8.45%</td>
                    <td>16.81%</td>
                    <td>12.70%</td>
                    <td>6.80%</td>
                    <td></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@stop