@extends('layouts.layouts')
@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h3>รายการสินค้าทั้งหมด</h3>
        </div>
    </div>

    <div class="row" style="margin-top: 10px;padding-bottom: 10px;">
        <div class="col-lg-3" style="padding: 0px;margin: 0px;">
            <form style="float: left;" class="form-inline" method="GET" action="" novalidate>
                <input type="hidden">
                <div class="input-group input-group-sm">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></span>
                    <input type="text" class="form-control" name="search" value="{{$product['search']}}" placeholder="ค้นหา ชื่อสินค้า">
                </div>
                <button type="submit" class="btn btn-sm"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
            </form>
        </div>
        <div class="col-lg-9">
            <button style="float: right;" type="submit" class="btn btn-success" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> เพิ่ม</button>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="<?php echo (($type=='all')?'active':'')?>" ><a href="#home" aria-controls="home" role="tab" data-toggle="tab">ทั้งหมด</a></li>
                    <li role="presentation" class="<?php echo (($type=='material')?'active':'')?>"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">วัถุดิบ</a></li>
                    <li role="presentation" class="<?php echo (($type=='manufacture')?'active':'')?>"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">สินค้าสำเร็จ</a></li>
                    <li role="presentation" class="<?php echo (($type=='equipment')?'active':'')?>"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">อุปกรณ์</a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane <?php echo (($type=='all')?'active':'')?>" id="home">
                        <div class="row" style="float: right;">
                            <div class="col-lg-12">
                                <nav>
                                    <ul class="pagination" style="margin: 0px;padding: 0px;padding-top: 3px;">
                                        @if($product['page']>1)
                                            <li>
                                                <a href="/products/search?type=all&page={{$product['page']-1}}@if(false==empty($product['search']))&search={{$product['search']}}@endif">Prev</a>
                                            </li>
                                        @endif

                                        @for($x = 1; $x <= $product['maxDataProduct']; $x++)
                                            @if($x==$product['page'])
                                                <li class="active"><a
                                                            href="/products/search?type=all&page={{$x}}@if(false==empty($product['search']))&search={{$product['search']}}@endif">{{$x}}</a>
                                                </li>
                                            @else
                                                <li>
                                                    <a href="/products/search?type=all&page={{$x}}@if(false==empty($product['search']))&search={{$product['search']}}@endif">{{$x}}</a>
                                                </li>
                                            @endif
                                        @endfor

                                        @if($product['page']<$product['maxDataProduct'])
                                            <li>
                                                <a href="/products/search?type=all&page={{$product['page']+1}}@if(false==empty($product['search']))&search={{$product['search']}}@endif">Next</a>
                                            </li>
                                        @endif
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-lg-12">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr class="info">
                                        <th>#</th>
                                        <th>รหัสสินค้า</th>
                                        <th>ชื่อสินค้า</th>
                                        <th>ราคา</th>
                                        @foreach($storeName as $key=>$value)
                                            <th style="border: solid 2px;">{{$value->storename_name}}</th>
                                        @endforeach
                                        <th style="border: solid 2px;font-weight: bolder;text-align: center">รวม</th>
                                        <th style="border: solid 2px;">จอง</th>
                                        <th>หน่วย</th>
                                        <th>ประเภท</th>
                                        <th>จัดการ</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($product['dataProduct'] as $productK=>$productV)
                                        <tr>
                                            <th scope="row">{{(($page-1)*30)+($productK+1)}} <span style="display: none">{{$sum=0}}</span></th>
                                            <td>{{$productV->product_id}}</td>
                                            <td>{{$productV->product_name}} </td>
                                            <td style="text-align: right;">{{number_format($productV->product_price,2)}}</td>
                                            @foreach($storeName as $keyS=>$valueS)
                                                @if(false==empty($productV->deleted_at))
                                                    @if(false==empty($productV->deleted_at[$valueS->storename_id]))
                                                        <span style="display: none">{{$sum+=$productV->deleted_at[$valueS->storename_id]->balance_product}}</span>
                                                        @if(false==empty($valueS->minimum))
                                                            @if(($valueS->minimum)>($productV->deleted_at[$valueS->storename_id]->balance_product))
                                                                <td style="background-color:rgba(255,0,0,0.2);text-align: right;border: solid 2px;">{{$productV->deleted_at[$valueS->storename_id]->balance_product}}</td>
                                                            @else
                                                                <td style="text-align: right;border: solid 2px;">{{$productV->deleted_at[$valueS->storename_id]->balance_product}}</td>
                                                            @endif
                                                        @else
                                                            <td style="text-align: right;border: solid 2px;background-color:rgba(255,255,0,0.2);">{{$productV->deleted_at[$valueS->storename_id]->balance_product}}</td>
                                                        @endif
                                                    @else
                                                        <td style="text-align: right;border: solid 2px;background-color:rgba(255,125,0,0.2);">0</td>
                                                    @endif
                                                @else
                                                    <td style="text-align: right;border: solid 2px; background-color:rgba(255,125,0,0.2);">0</td>
                                                @endif
                                            @endforeach
                                            <td style="text-align: right;border: solid 2px;">{{$sum}}</td>
                                            <td style="border: solid 2px;text-align: right;">{{\App\Models\CustomersOrderProduct::getCountProductPlan($productV->product_name)}}</td>
                                            <td>{{$productV->unit_name}}</td>
                                            <td>
                                                @if($productV->product_type=='material')
                                                    วัถุดิบ
                                                @elseif($productV->product_type=='manufacture')
                                                    สินค้าผลิต
                                                @elseif($productV->product_type=='equipment')
                                                    อุปกรณ์
                                                @endif
                                            </td>
                                            <td>
                                                <a href="/products/displaybyid?id={{$productV->product_id}}&type={{$type}}&page={{$product['page']}}@if(false==empty($product['search']))&search={{$product['search']}}@endif"
                                                   class="btn btn-primary">แก้ไข</a>
                                                <a href="/products/delete?id={{$productV->product_id}}" class="btn btn-danger"
                                                   onclick="return confirm('ยืนยันการลบข้อมูลนี้ ?')">ลบ</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane <?php echo (($type=='material')?'active':'')?>" id="profile">
                        <div class="row" style="float: right;">
                            <div class="col-lg-12">
                                <nav>
                                    <ul class="pagination" style="margin: 0px;padding: 0px;padding-top: 3px;">
                                        @if($product['page']>1)
                                            <li>
                                                <a href="/products/search?type=material&page={{$product['page']-1}}@if(false==empty($product['search']))&search={{$product['search']}}@endif">Prev</a>
                                            </li>
                                        @endif

                                        @for($x = 1; $x <= $product['maxDataProductMaterial']; $x++)
                                            @if($x==$product['page'])
                                                <li class="active"><a
                                                            href="/products/search?type=material&page={{$x}}@if(false==empty($product['search']))&search={{$product['search']}}@endif">{{$x}}</a>
                                                </li>
                                            @else
                                                <li>
                                                    <a href="/products/search?type=material&page={{$x}}@if(false==empty($product['search']))&search={{$product['search']}}@endif">{{$x}}</a>
                                                </li>
                                            @endif
                                        @endfor

                                        @if($product['page']<$product['maxDataProductMaterial'])
                                            <li>
                                                <a href="/products/search?type=material&page={{$product['page']+1}}@if(false==empty($product['search']))&search={{$product['search']}}@endif">Next</a>
                                            </li>
                                        @endif
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-lg-12">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr class="info">
                                        <th>#</th>
                                        <th>รหัสสินค้า</th>
                                        <th>ชื่อสินค้า</th>
                                        <th>ราคา</th>
                                        @foreach($storeName as $key=>$value)
                                            <th style="border: solid 2px;">{{$value->storename_name}}</th>
                                        @endforeach
                                        <th style="border: solid 2px;font-weight: bolder;text-align: center">รวม</th>
                                        <th style="border: solid 2px;">จอง</th>
                                        <th>หน่วย</th>
                                        <th>ประเภท</th>
                                        <th>จัดการ</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($product['dataProductMaterial'] as $productK=>$productV)
                                        <tr>
                                            <th scope="row">{{(($page-1)*30)+($productK+1)}} <span style="display: none">{{$sum=0}}</span></th>
                                            <td>{{$productV->product_id}}</td>
                                            <td>{{$productV->product_name}} </td>
                                            <td style="text-align: right;">{{number_format($productV->product_price,2)}}</td>
                                            @foreach($storeName as $keyS=>$valueS)
                                                @if(false==empty($productV->deleted_at))
                                                    @if(false==empty($productV->deleted_at[$valueS->storename_id]))
                                                        <span style="display: none">{{$sum+=$productV->deleted_at[$valueS->storename_id]->balance_product}}</span>
                                                        @if(false==empty($valueS->minimum))
                                                            @if(($valueS->minimum)>($productV->deleted_at[$valueS->storename_id]->balance_product))
                                                                <td style="background-color:rgba(255,0,0,0.2);text-align: right;border: solid 2px;">{{$productV->deleted_at[$valueS->storename_id]->balance_product}}</td>
                                                            @else
                                                                <td style="text-align: right;border: solid 2px;">{{$productV->deleted_at[$valueS->storename_id]->balance_product}}</td>
                                                            @endif
                                                        @else
                                                            <td style="text-align: right;border: solid 2px;background-color:rgba(255,255,0,0.2);">{{$productV->deleted_at[$valueS->storename_id]->balance_product}}</td>
                                                        @endif
                                                    @else
                                                        <td style="text-align: right;border: solid 2px;background-color:rgba(255,125,0,0.2);">0</td>
                                                    @endif
                                                @else
                                                    <td style="text-align: right;border: solid 2px; background-color:rgba(255,125,0,0.2);">0</td>
                                                @endif
                                            @endforeach
                                            <td style="text-align: right;border: solid 2px;">{{$sum}}</td>
                                            <td style="border: solid 2px;text-align: right;">{{\App\Models\CustomersOrderProduct::getCountProductPlan($productV->product_name)}}</td>
                                            <td>{{$productV->unit_name}}</td>
                                            <td>
                                                @if($productV->product_type=='material')
                                                    วัถุดิบ
                                                @elseif($productV->product_type=='manufacture')
                                                    สินค้าผลิต
                                                @elseif($productV->product_type=='equipment')
                                                    อุปกรณ์
                                                @endif
                                            </td>
                                            <td>
                                                <a href="/products/displaybyid?id={{$productV->product_id}}&page={{$product['page']}}@if(false==empty($product['search']))&search={{$product['search']}}@endif"
                                                   class="btn btn-primary">แก้ไข</a>
                                                <a href="/products/delete?id={{$productV->product_id}}" class="btn btn-danger"
                                                   onclick="return confirm('ยืนยันการลบข้อมูลนี้ ?')">ลบ</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane <?php echo (($type=='manufacture')?'active':'')?>" id="messages">
                        <div class="row" style="float: right;">
                            <div class="col-lg-12">
                                <nav>
                                    <ul class="pagination" style="margin: 0px;padding: 0px;padding-top: 3px;">
                                        @if($product['page']>1)
                                            <li>
                                                <a href="/products/search?type=manufacture&page={{$product['page']-1}}@if(false==empty($product['search']))&search={{$product['search']}}@endif">Prev</a>
                                            </li>
                                        @endif

                                        @for($x = 1; $x <= $product['maxDataProductManufacture']; $x++)
                                            @if($x==$product['page'])
                                                <li class="active"><a
                                                            href="/products/search?type=manufacture&page={{$x}}@if(false==empty($product['search']))&search={{$product['search']}}@endif">{{$x}}</a>
                                                </li>
                                            @else
                                                <li>
                                                    <a href="/products/search?type=manufacture&page={{$x}}@if(false==empty($product['search']))&search={{$product['search']}}@endif">{{$x}}</a>
                                                </li>
                                            @endif
                                        @endfor

                                        @if($product['page']<$product['maxDataProductManufacture'])
                                            <li>
                                                <a href="/products/search?type=manufacture&page={{$product['page']+1}}@if(false==empty($product['search']))&search={{$product['search']}}@endif">Next</a>
                                            </li>
                                        @endif
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-lg-12">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr class="info">
                                        <th>#</th>
                                        <th>รหัสสินค้า</th>
                                        <th>ชื่อสินค้า</th>
                                        <th>ราคา</th>
                                        @foreach($storeName as $key=>$value)
                                            <th style="border: solid 2px;">{{$value->storename_name}}</th>
                                        @endforeach
                                        <th style="border: solid 2px;font-weight: bolder;text-align: center">รวม</th>
                                        <th style="border: solid 2px;">จอง</th>
                                        <th>หน่วย</th>
                                        <th>ประเภท</th>
                                        <th>จัดการ</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($product['dataProductManufacture'] as $productK=>$productV)
                                        <tr>
                                            <th scope="row">{{(($page-1)*30)+($productK+1)}} <span style="display: none">{{$sum=0}}</span></th>
                                            <td>{{$productV->product_id}}</td>
                                            <td>{{$productV->product_name}} </td>
                                            <td style="text-align: right;">{{number_format($productV->product_price,2)}}</td>
                                            @foreach($storeName as $keyS=>$valueS)
                                                @if(false==empty($productV->deleted_at))
                                                    @if(false==empty($productV->deleted_at[$valueS->storename_id]))
                                                        <span style="display: none">{{$sum+=$productV->deleted_at[$valueS->storename_id]->balance_product}}</span>
                                                        @if(false==empty($valueS->minimum))
                                                            @if(($valueS->minimum)>($productV->deleted_at[$valueS->storename_id]->balance_product))
                                                                <td style="background-color:rgba(255,0,0,0.2);text-align: right;border: solid 2px;">{{$productV->deleted_at[$valueS->storename_id]->balance_product}}</td>
                                                            @else
                                                                <td style="text-align: right;border: solid 2px;">{{$productV->deleted_at[$valueS->storename_id]->balance_product}}</td>
                                                            @endif
                                                        @else
                                                            <td style="text-align: right;border: solid 2px;background-color:rgba(255,255,0,0.2);">{{$productV->deleted_at[$valueS->storename_id]->balance_product}}</td>
                                                        @endif
                                                    @else
                                                        <td style="text-align: right;border: solid 2px;background-color:rgba(255,125,0,0.2);">0</td>
                                                    @endif
                                                @else
                                                    <td style="text-align: right;border: solid 2px; background-color:rgba(255,125,0,0.2);">0</td>
                                                @endif
                                            @endforeach
                                            <td style="text-align: right;border: solid 2px;">{{$sum}}</td>
                                            <td style="border: solid 2px; text-align: right;">{{\App\Models\CustomersOrderProduct::getCountProductPlan($productV->product_name)}}</td>
                                            <td>{{$productV->unit_name}}</td>
                                            <td>
                                                @if($productV->product_type=='material')
                                                    วัถุดิบ
                                                @elseif($productV->product_type=='manufacture')
                                                    สินค้าผลิต
                                                @elseif($productV->product_type=='equipment')
                                                    อุปกรณ์
                                                @endif
                                            </td>
                                            <td>
                                                <a href="/products/displaybyid?id={{$productV->product_id}}&page={{$product['page']}}@if(false==empty($product['search']))&search={{$product['search']}}@endif"
                                                   class="btn btn-primary">แก้ไข</a>
                                                <a href="/products/delete?id={{$productV->product_id}}" class="btn btn-danger"
                                                   onclick="return confirm('ยืนยันการลบข้อมูลนี้ ?')">ลบ</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane <?php echo (($type=='equipment')?'active':'')?>" id="settings">
                        <div class="row" style="float: right;">
                            <div class="col-lg-12">
                                <nav>
                                    <ul class="pagination" style="margin: 0px;padding: 0px;padding-top: 3px;">
                                        @if($product['page']>1)
                                            <li>
                                                <a href="/products/search?type=equipment&page={{$product['page']-1}}@if(false==empty($product['search']))&search={{$product['search']}}@endif">Prev</a>
                                            </li>
                                        @endif

                                        @for($x = 1; $x <= $product['maxDataProductEquipment']; $x++)
                                            @if($x==$product['page'])
                                                <li class="active"><a
                                                            href="/products/search?type=equipment&page={{$x}}@if(false==empty($product['search']))&search={{$product['search']}}@endif">{{$x}}</a>
                                                </li>
                                            @else
                                                <li>
                                                    <a href="/products/search?type=equipment&page={{$x}}@if(false==empty($product['search']))&search={{$product['search']}}@endif">{{$x}}</a>
                                                </li>
                                            @endif
                                        @endfor

                                        @if($product['page']<$product['maxDataProductEquipment'])
                                            <li>
                                                <a href="/products/search?type=equipment&page={{$product['page']+1}}@if(false==empty($product['search']))&search={{$product['search']}}@endif">Next</a>
                                            </li>
                                        @endif
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-lg-12">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr class="info">
                                        <th>#</th>
                                        <th>รหัสสินค้า</th>
                                        <th>ชื่อสินค้า</th>
                                        <th>ราคา</th>

                                        @foreach($storeName as $key=>$value)
                                            <th style="border: solid 2px;">{{$value->storename_name}}</th>
                                        @endforeach
                                        <th style="border: solid 2px;font-weight: bolder;text-align: center">รวม</th>
                                        <th style="border: solid 2px;">จอง</th>
                                        <th>หน่วย</th>
                                        <th>ประเภท</th>
                                        <th>จัดการ</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($product['dataProductEquipment'] as $productK=>$productV)
                                        <tr>
                                            <th scope="row">{{(($page-1)*30)+($productK+1)}} <span style="display: none">{{$sum=0}}</span></th>
                                            <td>{{$productV->product_id}}</td>
                                            <td>{{$productV->product_name}} </td>
                                            <td style="text-align: right;">{{number_format($productV->product_price,2)}}</td>
                                            @foreach($storeName as $keyS=>$valueS)
                                                @if(false==empty($productV->deleted_at))
                                                    @if(false==empty($productV->deleted_at[$valueS->storename_id]))
                                                        <span style="display: none">{{$sum+=$productV->deleted_at[$valueS->storename_id]->balance_product}}</span>
                                                        @if(false==empty($valueS->minimum))
                                                            @if(($valueS->minimum)>($productV->deleted_at[$valueS->storename_id]->balance_product))
                                                                <td style="background-color:rgba(255,0,0,0.2);text-align: right;border: solid 2px;">{{$productV->deleted_at[$valueS->storename_id]->balance_product}}</td>
                                                            @else
                                                                <td style="text-align: right;border: solid 2px;">{{$productV->deleted_at[$valueS->storename_id]->balance_product}}</td>
                                                            @endif
                                                        @else
                                                            <td style="text-align: right;border: solid 2px;background-color:rgba(255,255,0,0.2);">{{$productV->deleted_at[$valueS->storename_id]->balance_product}}</td>
                                                        @endif
                                                    @else
                                                        <td style="text-align: right;border: solid 2px;background-color:rgba(255,125,0,0.2);">0</td>
                                                    @endif
                                                @else
                                                    <td style="text-align: right;border: solid 2px; background-color:rgba(255,125,0,0.2);">0</td>
                                                @endif
                                            @endforeach
                                            <td style="text-align: right;border: solid 2px;">{{$sum}}</td>
                                            <td style="border: solid 2px; text-align: right;">{{\App\Models\CustomersOrderProduct::getCountProductPlan($productV->product_name)}}</td>
                                            <td>{{$productV->unit_name}}</td>
                                            <td>
                                                @if($productV->product_type=='material')
                                                    วัถุดิบ
                                                @elseif($productV->product_type=='manufacture')
                                                    สินค้าผลิต
                                                @elseif($productV->product_type=='equipment')
                                                    อุปกรณ์
                                                @endif
                                            </td>
                                            <td>
                                                <a href="/products/displaybyid?id={{$productV->product_id}}&page={{$product['page']}}@if(false==empty($product['search']))&search={{$product['search']}}@endif"
                                                   class="btn btn-primary">แก้ไข</a>
                                                <a href="/products/delete?id={{$productV->product_id}}" class="btn btn-danger"
                                                   onclick="return confirm('ยืนยันการลบข้อมูลนี้ ?')">ลบ</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="modal fade @if(true==$errors->has()||false==empty($dataProduct['product_id'])) in @endif"
                 style="@if(true==$errors->has()||false==empty($dataProduct['product_id'])) display: block; @endif"
                 id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                @if (!empty($dataProduct['product_id']))
                    <form style="" class="" method="POST"
                          action="/products/validationEditManage?id=@if (!empty($dataProduct['product_id'])){{$dataProduct['product_id']}}@endif&page={{$product['page']}}@if(false==empty($product['search']))&search={{$product['search']}}@endif"
                          novalidate>
                        @else
                            <form style="" class="" method="POST" action="/products/validationManage" novalidate>
                                @endif
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <a href="/products/manage" type="button" class="close" data-dismiss="modal"
                                               aria-label="Close"><span aria-hidden="true">&times;</span></a>
                                            <h4 class="modal-title">จัดการ</h4>
                                        </div>
                                        <div class="modal-body">
                                            @if ($errors->has())
                                                <div class="alert alert-danger">
                                                    @foreach ($errors->all() as $error)
                                                        {{ $error }}<br>
                                                    @endforeach
                                                </div>
                                            @endif
                                            <div class="form-group @if ($errors->has('product_id')) has-error @endif">
                                                <label for="exampleInputName2">รหัสสินค้า</label>
                                                <input type="text" class="form-control" name="product_id"
                                                       value="@if (!empty($dataProduct['product_id'])){{$dataProduct['product_id']}}@elseif(!empty(Input::old('product_id'))){{Input::old('product_id')}}@endif"
                                                       placeholder="รหัสสินค้า">
                                            </div>
                                            <div class="form-group @if ($errors->has('product_name')) has-error @endif">
                                                <label for="exampleInputName2">ชื่อสินค้า</label>
                                                <input type="text" class="form-control" name="product_name"
                                                       value="@if (!empty($dataProduct['product_name'])){{$dataProduct['product_name']}}@elseif(!empty(Input::old('product_name'))){{ Input::old('product_name')}}@endif"
                                                       placeholder="ชื่อสินค้า">
                                            </div>
                                            <div class="form-group @if ($errors->has('product_price')) has-error @endif">
                                                <label for="exampleInputName2">ราคา</label>
                                                <input type="text" class="form-control" name="product_price"
                                                       value="@if (!empty($dataProduct['product_price'])){{$dataProduct['product_price']}}@elseif(!empty(Input::old('product_price'))){{ Input::old('product_price')}}@endif"
                                                       placeholder="ราคา">
                                            </div>
                                            <div class="form-group @if ($errors->has('product_unit')) has-error @endif">
                                                <label for="exampleInputName2">หน่วย</label>
                                                <select class="form-control" name="product_unit">
                                                    <option value="">หน่วย</option>
                                                    @foreach($dataProductUnit as $key=>$value)
                                                        <option @if (Input::old('product_unit')==$value['unit_id'] || $dataProduct['product_unit']==$value['unit_id'])
                                                            selected
                                                            @endif value="{{$value['unit_id']}}">{{$value['unit_name']}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group @if ($errors->has('product_type')) has-error @endif">
                                                <label for="exampleInputName2">ประเภท</label>
                                                <select class="form-control" name="product_type">
                                                    <option @if (Input::old('product_type')=='material'  || $dataProduct['product_type']=='material')
                                                        selected @endif value="material">วัตถุดิบ
                                                    </option>
                                                    <option @if (Input::old('product_type')=='manufacture'  || $dataProduct['product_type']=='manufacture')
                                                        selected @endif value="manufacture">สินค้าผลิต
                                                    </option>
                                                    <option @if (Input::old('product_type')=='equipment'  || $dataProduct['product_type']=='equipment')
                                                        selected @endif value="equipment">อุปกรณ์
                                                    </option>
                                                </select>
                                            </div>

                                            <div class="form-group @if ($errors->has('minimum')) has-error @endif">
                                                <label for="exampleInputName2">จำนวนหน่อยสุด</label>
                                                <input type="text" class="form-control" name="minimum"
                                                           value="@if (!empty($dataProduct['minimum'])){{$dataProduct['minimum']}}@elseif(!empty(Input::old('minimum'))){{ Input::old('minimum')}}@endif"
                                                           placeholder="ราคา">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <a href="/products/manage" type="button" class="btn btn-default"
                                               data-dismiss="modal">Close</a>
                                            @if (!empty($dataProduct['product_id']))
                                                <button type="submit" class="btn btn-primary"><span
                                                            class="glyphicon glyphicon-pencil"
                                                            aria-hidden="true"></span> แก้ไข
                                                </button>
                                            @else
                                                <button type="submit" class="btn btn-success"><span
                                                            class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                                    เพิ่ม
                                                </button>
                                            @endif
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </form>
            </div>
            <!-- /.modal -->
        </div>
    </div>

    <script>
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            e.target // newly activated tab
            e.relatedTarget // previous active tab
            console.log($(this).attr('aria-controls'))
        })
    </script>

@stop