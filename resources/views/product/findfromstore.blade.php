@extends('layouts.layouts')
@section('content')

    {{--<div class="row" style="margin-top: 10px;">--}}
        {{--<div class="col-lg-12">--}}
            {{--<ol class="breadcrumb">--}}
                {{--<li><a href="/purchases/viewPurchases">จัดการใบลงรับสินค้า</a></li>--}}
                {{--<li class="active">รายการสินค้า</li>--}}
            {{--</ol>--}}
        {{--</div>--}}
    {{--</div>--}}


    <div class="row">
        {{--<div class="col-lg-12">--}}
            {{--<h3>ค้นหาสินค้าจากคลัง</h3>--}}
        {{--</div>--}}
    </div>

    <div class="row" style="padding: 10px;float: right;">
        <div class="col-lg-12">
            <form method="POST" action="/purchases/addProduct" class="form-inline">
                <div class="form-group @if ($errors->has('productId')) has-error @endif">
                    <label for="exampleInputName2">ชื่อสินค้า</label>
                    <select name="productId" id="productId" class="form-control">
                        <option value="0">เลือกสินค้า</option>
                        @foreach($data as $key=>$value)
                            <option value="{{$value->product_id}}">{{$value->product_name}}</option>
                        @endforeach
                    </select>
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th>คลัง</th>
                    <th>จำนวนทั้งหมด</th>
                    <th>จำนวนที่ใช้ไป</th>
                    <th>จำนวนคงคลัง</th>
                    {{--<th>ลบ</th>--}}
                </tr>
                </thead>
                <tbody id="store_list">
                </tbody>
            </table>
        </div>
    </div>



@stop