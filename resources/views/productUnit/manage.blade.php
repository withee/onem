@extends('layouts.layouts')
@section('content')
    <style>
        .has-error>input{
            background-color: rgba(169,68,66,0.1);
        }
    </style>
    <div class="row">
        <div class="col-lg-12">
            <h3>รายชื่อหน่วยสินค้าทั้งหมด</h3>
        </div>
    </div>
    <div class="row" style="margin-top: 20px;">
        <div class="col-lg-3" style="padding: 0px;margin: 0px;">
            <form style="float: left;" class="form-inline" method="GET" action="/productUnit/search" novalidate>
                <div class="input-group input-group-sm">
                    <span class="input-group-addon" ><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></span>
                    <input type="text" class="form-control" name="search" placeholder="ค้นหา ชื่อหน่วยสินค้า">
                </div>
                <button type="submit"  class="btn btn-sm"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
            </form>

        </div>
        <div class="col-lg-9">
            <button style="float: right;" type="submit" class="btn btn-success" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> เพิ่ม</button>
        </div>
    </div>

    <div class="row" style="float: right;">
        <div class="col-lg-12">
            <nav>
                <ul class="pagination" style="margin: 0px;padding: 0px;padding-top: 3px;">
                    @if($productUnit['page']>1)
                        <li><a href="/productUnit/search?page={{$productUnit['page']-1}}@if(false==empty($productUnit['search']))&search={{$productUnit['search']}}@endif">Prev</a></li>
                    @endif

                    @for($x = 1; $x <= $productUnit['maxProduct']; $x++)
                        @if($x==$productUnit['page'])
                            <li class="active" ><a href="/productUnit/search?page={{$x}}@if(false==empty($productUnit['search']))&search={{$productUnit['search']}}@endif">{{$x}}</a></li>
                        @else
                            <li ><a href="/productUnit/search?page={{$x}}@if(false==empty($productUnit['search']))&search={{$productUnit['search']}}@endif">{{$x}}</a></li>
                        @endif
                    @endfor

                    @if($productUnit['page']<$productUnit['maxProduct'])
                        <li><a href="/productUnit/search?page={{$productUnit['page']+1}}@if(false==empty($productUnit['search']))&search={{$productUnit['search']}}@endif">Next</a></li>
                    @endif
                </ul>
            </nav>
        </div>
    </div>

    <div class="row" style="margin-top: 10px;">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr class="info">
                    <th>#</th>
                    <th>ชื่อหน่วย</th>
                    <th>จัดการ</th>
                </tr>
                </thead>
                <tbody>
                @foreach($productUnit['dataProductUnit'] as $key=>$value)
                    <tr>
                        <th scope="row">{{$key+1}}</th>
                        <td>{{$value->unit_name}}</td>
                        <td>
                            <a href="/productUnit/displaybyid?id={{$value->unit_id}}&@if(false==empty($page))page={{$page}}@endif&@if(false==empty($search))search={{$search}}@endif" class="btn btn-primary">แก้ไข</a>
                            <a href="/productUnit/delete?id={{$value->unit_id}}" class="btn btn-danger" onclick="return confirm('ยืนยันการลบข้อมูลนี้ ?')" >ลบ</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
        <div class="modal fade @if(true==$errors->has()||false==empty($dataProductUnit['unit_id'])) in @endif" style="@if(true==$errors->has()||false==empty($dataProductUnit['unit_id'])) display: block; @endif" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            @if (false==empty($dataProductUnit['unit_id']))
                <form style="" class="form-inline" method="POST" action="/productUnit/validationEditManage?id=
                @if(false==empty($dataProductUnit['unit_id'])){{$dataProductUnit['unit_id']}}@endif&
                @if(false==empty($page))page={{$page}}@endif&
                @if(false==empty($page))search={{$search}}@endif" novalidate>
                    @else
                        <form style="" class="form-inline" method="POST" action="/productUnit/validationManage" novalidate>
                            @endif
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <a href="/productUnit/manage" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                        <h4 class="modal-title">จัดการหน่วนสินค้า</h4>
                    </div>
                    <div class="modal-body">
                        @if (true==$errors->has())
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </div>
                        @endif
                        <div class="form-group @if ($errors->has('product_id')) has-error @endif">
                            <label style="padding: 5px 25px 5px 5px;" for="exampleInputName2">ชื่อประเภทสินค้า</label>
                            <input type="text" class="form-control" name="unit_name" value="@if (false==empty($dataProductUnit['unit_name'])){{$dataProductUnit['unit_name']}}@elseif(false==empty(Input::old('unit_name'))){{Input::old('unit_name')}}@endif" placeholder="ชื่อประเภทสินค้า">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="/productUnit/manage" type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-off" aria-hidden="true"></span> ปิด</a>
                        @if (false==empty($dataProductUnit['unit_id']))
                            <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> แก้ไข</button>
                        @else
                            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> เพิ่ม</button>
                        @endif
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
            </form>
        </div><!-- /.modal -->
        </div>
    </div>

@stop