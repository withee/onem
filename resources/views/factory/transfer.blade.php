@extends('layouts.layouts')
@section('content')
    <div class="row" style="padding-top: 10px;">
        <div class="col-lg-12">
            <ol class="breadcrumb" style="margin: 0px;">
                <li><a href="/factory/manageCustomers">จัดการข้อมูลลูกค้า</a></li>
                <li class="active">แผนจัดส่ง</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12" style="padding: 10px;">
            <span style="font-size: 26px;font-weight: bolder">แผนจัดส่ง</span>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-10"></div>
        <div class="col-lg-2" style="padding: 5px;">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-paste" aria-hidden="true"></span> ออกใบส่งสินค้า</button>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th style="width: 10px;"></th>
                    <th>ชื่อสินค้า</th>
                    <th>จำนวน</th>
                    <th>ส่งเเล้ว</th>
                    <th>เหลือ</th>
                    <th>จำนวนส่ง</th>
                </tr>
                </thead>
                <tbody>
                @foreach($dataOrderManufacture as $key=>$value)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td><input type="checkbox"></td>
                        <td>{{ \App\Models\Product::GetNameProduct($value['product_id'])}}</td>
                        <td>{{$value['order_manufacture_count']}}</td>
                        <td>0</td>
                        <th>{{$value['order_manufacture_count']}}</th>
                        <td>
                            <select class="form-control">
                                @for($i = 1; $i <= $value['order_manufacture_count']; $i++)
                                    <option value="{{$i}}">{{$i}}</option>
                                @endfor
                            </select>
                            {{--<a style="float: left;margin-left: 5px;" onclick="return confirm('ยืนยันการส่งผลิต')" href="/project/RemoveOrderManufacture?id={{$value->order_manufacture_id}}" class="btn btn-info"><span class="glyphicon glyphicon-paste" aria-hidden="true"></span> ใบเบิก</a>--}}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">ข้อมูลการส่งสินค้า</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">วันที่</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" id="inputEmail3" placeholder="วันที่">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">ทะเบียนรถ</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" id="inputPassword3" placeholder="ทะเบียนรถ">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@stop