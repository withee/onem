@extends('layouts.layouts')
@section('content')
    <div class="row" style="padding: 10px 0px 10px 10px;">
        <div class="col-lg-8">
            <span style="font-weight: bolder;font-size: 24px;">ถอดเเบบ</span>
        </div>
        <div class="col-lg-3" style="padding: 0px;">
            <form method="POST" action="/factory/searchCustomersOrder" class="form-inline">
                <div class="form-group">
                    <label for="exampleInputEmail2">ค้นหา</label>
                    <input style="width: 150px;" type="text" class="form-control" name="search" placeholder="ชื่อ Order">
                </div>
                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                <a href="/factory/addOrder" class="btn btn-default"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span></a>
            </form>
        </div>

        <div class="col-lg-1">
            {{--<a class="btn btn-default" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> เพิ่ม</a>--}}
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th>ชื่อ Order</th>
                    <th>ชื่อ-นามสกุลหรือชื่อบริษัท</th>
                    <th>สถานะ</th>
                    <th>จัดการ</th>
                </tr>
                </thead>
                <tbody>
                @foreach($dataCustomersOrder['data'] as $key=>$value)
                    <tr>
                        <th scope="row">{{$key+1}}</th>
                        <td><a href="/factory/checkMaterial?orderId={{$value->customers_order_id}}">{{$value->customers_order_name}}</a></td>
                        <td>{{$value->customers_order_customers}}</td>
                        <td>
                            {{\App\Models\CustomersOrder::checkStatus($value->customers_order_status)}}
                        </td>
                        <td>
                            @if($value->customers_order_status=='markOrder')
                            <a href="/factory/confirmDealCustomersOrder?id={{$value->customers_order_id}}" class="btn btn-info">
                                <span class="glyphicon glyphicon-flag" aria-hidden="true"></span> ส่งผลิต
                            </a>
                            @endif
                            <a href="/factory/deleteCustomersOrder?id={{$value->customers_order_id}}" onclick="return confirm('ยืนยันการ ยกเลิก Order {{$value->customers_order_name}} ')" class="btn btn-danger">
                                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> ยกเลิก
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12" style="text-align: center;">
            <nav>
                <ul class="pagination" style="margin: 0px;padding: 0px;padding-top: 3px;">
                    @if($dataCustomersOrder['page']>1)
                        <li>
                            <a href="/factory/addOrder?page={{$dataCustomersOrder['page']-1}}@if(false==empty($dataCustomersOrder['search']))&search={{$dataCustomersOrder['search']}}@endif">Prev</a>
                        </li>
                    @endif

                    @for($x = 1; $x <= $dataCustomersOrder['max']; $x++)
                        @if($x==$dataCustomersOrder['page'])
                            <li class="active"><a
                                        href="/factory/addOrder?page={{$x}}@if(false==empty($dataCustomersOrder['search']))&search={{$dataCustomersOrder['search']}}@endif">{{$x}}</a>
                            </li>
                        @else
                            <li>
                                <a href="/factory/addOrder?page={{$x}}@if(false==empty($dataCustomersOrder['search']))&search={{$dataCustomersOrder['search']}}@endif">{{$x}}</a>
                            </li>
                        @endif
                    @endfor

                    @if($dataCustomersOrder['page']<$dataCustomersOrder['max'])
                        <li>
                            <a href="/factory/addOrder?page={{$dataCustomersOrder['page']+1}}@if(false==empty($dataCustomersOrder['search']))&search={{$dataCustomersOrder['search']}}@endif">Next</a>
                        </li>
                    @endif
                </ul>
            </nav>
        </div>
    </div>

    <style>
        .twitter-typeahead{
            width: 100% !important;
        }
        .tt-query {
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        }

        .tt-hint {
            color: #999
        }

        .tt-menu {
            width: 422px;
            margin: 12px 0;
            padding: 8px 0;
            background-color: #fff;
            border: 1px solid #ccc;
            border: 1px solid rgba(0, 0, 0, 0.2);
            -webkit-border-radius: 8px;
            -moz-border-radius: 8px;
            border-radius: 8px;
            -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
            -moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
            box-shadow: 0 5px 10px rgba(0,0,0,.2);
        }

        .tt-suggestion {
            padding: 3px 20px;
            font-size: 18px;
            line-height: 24px;
        }

        .tt-suggestion:hover {
            cursor: pointer;
            color: #fff;
            background-color: #0097cf;
        }

        .tt-suggestion.tt-cursor {
            color: #fff;
            background-color: #0097cf;

        }

        .tt-suggestion p {
            margin: 0;
        }

        .gist {
            font-size: 14px;
        }
    </style>


    <script>
        $(document).ready(function () {
            var substringMatcher = function (strs) {
                return function findMatches(q, cb) {
                    var matches, substringRegex;

                    // an array that will be populated with substring matches
                    matches = [];

                    // regex used to determine if a string contains the substring `q`
                    substrRegex = new RegExp(q, 'i');

                    // iterate through the pool of strings and for any string that
                    // contains the substring `q`, add it to the `matches` array
                    $.each(strs, function (i, str) {
                        if (substrRegex.test(str)) {
                            matches.push(str);
                        }
                    });

                    cb(matches);
                };
            };

            $.ajax({
                url: "/factory/checkCustomers",
                type: "post",
                dataType: 'json'
            }).success(function (res) {
                $('input#the-basics').typeahead({
                            hint: true,
                            highlight: true,
                            minLength: 1
                        },
                        {
                            name: 'states',
                            source: substringMatcher(res)
                        });
            })
        });
    </script>

    <script type="text/javascript">
        $(function() {
            $( "#datepicker" ).datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat:'yy-mm-dd'
            });
        });
    </script>

    <style>
        .ui-datepicker-month,.ui-datepicker-year{
            color: #333;
        }
    </style>

    <!-- Modal -->
    <div class="modal fade @if(!empty($customersOrder->customers_order_id) || true==$errors->has('customers_order_customers')||true==$errors->has('customers_order_name')) in @endif" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="@if (!empty($customersOrder->customers_order_id) || true==$errors->has('customers_order_customers')||true==$errors->has('customers_order_name')) display: block; @endif">
        <div class="modal-dialog" role="document">
            <form method="POST" action="/factory/createOrder" class="form-horizontal" novalidate >
                <div class="modal-content">
                    <div class="modal-header">
                        <a href="/factory/addOrder" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                        <h4 class="modal-title" id="myModalLabel">เพิ่ม-แก้ไข เปิด Order ลูกค้า</h4>
                    </div>
                    <div class="modal-body">
                        @if (true==$errors->has('customers_order_customers')||true==$errors->has('customers_order_name')||true==$errors->has('customers_order_tax')||true==$errors->has('customers_order_discount'))
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </div>
                        @endif
                        <div class="form-horizontal" style="padding: 0px 20px;">
                            <input type="hidden" name="customers_order_id" value="{{$customersOrder->customers_order_id}}">
                            <div class="form-group @if ($errors->has('customers_order_date')) has-error @endif">
                                <label for="exampleInputPassword1">วันที่</label>
                                <input type="text" class="form-control" name="customers_order_date" id="datepicker" placeholder="วันที่">
                            </div>
                            <div class="form-group @if ($errors->has('customers_order_tax_number')) has-error @endif">
                                <label for="exampleInputPassword1">เลขประจำตัวผู้เสียภาษี</label>
                                <input type="text" class="form-control" name="customers_order_tax_number" id="exampleInputPassword1" placeholder="เลขประจำตัวผู้เสียภาษี">
                            </div>
                            <div class="form-group @if ($errors->has('customers_order_number')) has-error @endif">
                                <label for="exampleInputPassword1">หมายเลขใบเสนอราคา</label>
                                <input type="text" class="form-control" name="customers_order_number" id="exampleInputPassword1" placeholder="หมายเลขใบเสนอราคา">
                            </div>
                            <div class="form-group @if ($errors->has('customers_order_customers')) has-error @endif" style="margin-bottom: 0px;">
                                <label for="exampleInputEmail1">ชื่อลูกค้า</label>
                            </div>
                            <div class="form-group @if ($errors->has('customers_order_customers')) has-error @endif">
                                <input id="the-basics" type="text" name="customers_order_customers" class="form-control" id="exampleInputPassword1" placeholder="ชื่อลูกค้า">
                            </div>
                            <div class="form-group @if ($errors->has('customers_order_name')) has-error @endif">
                                <label for="exampleInputPassword1">ชื่อ Order</label>
                                <input type="text" class="form-control" name="customers_order_name" id="exampleInputPassword1" placeholder="ชื่อ Order">
                            </div>
                            <div class="form-group @if ($errors->has('customers_order_discount')||$errors->has('customers_order_tax')) has-error @endif">
                                {{--<label for="inputPassword3" class="col-sm-2 control-label">ส่วนลด</label>--}}
                                <div class="col-sm-6  @if ($errors->has('customers_order_discount')) has-error @endif" style="padding-left: 0px;">
                                    <label for="inputPassword3">ส่วนลด</label>
                                    <input type="number" name="customers_order_discount" class="form-control" id="inputPassword3" placeholder="ส่วนลด" value="0">
                                </div>
                                {{--<label for="inputPassword3" class="col-sm-2 control-label">ภาษี</label>--}}
                                <div class="col-sm-6  @if ($errors->has('customers_order_tax')) has-error @endif" style="padding-left: 0px;">
                                    <label for="inputPassword3">ภาษี</label>
                                    <input type="number" name="customers_order_tax" class="form-control" id="inputPassword3" placeholder="ภาษี" value="0">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="/factory/addOrder" type="button" class="btn btn-default" data-dismiss="modal">Close</a>
                        <input type="submit" class="btn btn-primary" value="Save changes" />
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop