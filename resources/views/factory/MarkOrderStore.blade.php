@extends('layouts.layouts')
@section('content')
    {{--<div class="row" style="padding-top: 10px;">--}}
    {{--<div class="col-lg-12">--}}
    {{--<ol class="breadcrumb" style="margin: 0px;">--}}
    {{--<li><a href="/factory/checkMaterial">ถอดแบบการผลิต</a></li>--}}
    {{--<li class="active">PR</li>--}}
    {{--</ol>--}}
    {{--</div>--}}
    {{--</div>--}}
    <div class="row">
        <div class="col-lg-8" style="padding: 10px;">
            <span style="font-size: 26px;font-weight: bolder">ผลิตสินค้า(ฝ่ายคลัง)</span>
        </div>
        <div class="col-lg-4" style="padding: 10px;">
            <form method="POST" action="/factory/MarkOrderStore" class="form-inline">
                <div class="form-group">
                    <label for="exampleInputEmail2">ค้นหา</label>
                    <input style="width: 209px;" type="text" class="form-control" name="search" placeholder="ชื่อ Order">
                </div>
                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                <a href="/factory/MarkOrderStore" class="btn btn-default"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span></a>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th>ชื่อ Order</th>
                    {{--<th>% การผลิต</th>--}}
                    <th>สถานะ</th>
                </tr>
                </thead>
                <tbody>
                @foreach($dataCustomersOrder['data'] as $key=>$value)
                    <tr>
                        <th scope="row">{{$value->customers_order_id}}</th>
                        <td><a href="/factory/BringManufactureStore?orderId={{$value->customers_order_id}}">{{$value->customers_order_name}}</a></td>
                        {{--<td>--}}
                            {{--<div class="progress">--}}
                                {{--<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: {{$customersOrder->GetSumPercentOrder($value->customers_order_id)}}%;">--}}
                                    {{--{{$customersOrder->GetSumPercentOrder($value->customers_order_id)}}%--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</td>--}}
                        <td>
                            {{\App\Models\CustomersOrder::checkStatus($value->customers_order_status)}}
                        </td>
                        {{--<td>--}}
                        {{--<a href="/factory/confirmDealCustomersOrder?id={{$value->customers_order_id}}" onclick="return confirm('ยืนยันการ ส่งผลิต Order {{$value->customers_order_name}} ')" class="btn btn-info">--}}
                        {{--<span class="glyphicon glyphicon-sort" aria-hidden="true"></span> ส่งผลิต--}}
                        {{--</a>--}}
                        {{--<a href="/factory/deleteCustomersOrder?id={{$value->customers_order_id}}" onclick="return confirm('ยืนยันการ ยกเลิก Order {{$value->customers_order_name}} ')" class="btn btn-danger">--}}
                        {{--<span class="glyphicon glyphicon-trash" aria-hidden="true"></span> ยกเลิก--}}
                        {{--</a>--}}
                        {{--</td>--}}
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12" style="text-align: center;">
            <nav>
                <ul class="pagination" style="margin: 0px;padding: 0px;padding-top: 3px;">
                    @if($dataCustomersOrder['page']>1)
                        <li>
                            <a href="/factory/MarkOrderStore?page={{$dataCustomersOrder['page']-1}}@if(false==empty($dataCustomersOrder['search']))&search={{$dataCustomersOrder['search']}}@endif">Prev</a>
                        </li>
                    @endif

                    @for($x = 1; $x <= $dataCustomersOrder['max']; $x++)
                        @if($x==$dataCustomersOrder['page'])
                            <li class="active">
                                <a href="/MarkOrder/MarkOrderStore?page={{$x}}@if(false==empty($dataCustomersOrder['search']))&search={{$dataCustomersOrder['search']}}@endif">{{$x}}</a>
                            </li>
                        @else
                            <li>
                                <a href="/factory/MarkOrderStore?page={{$x}}@if(false==empty($dataCustomersOrder['search']))&search={{$dataCustomersOrder['search']}}@endif">{{$x}}</a>
                            </li>
                        @endif
                    @endfor

                    @if($dataCustomersOrder['page']<$dataCustomersOrder['max'])
                        <li>
                            <a href="/factory/MarkOrderStore?page={{$dataCustomersOrder['page']+1}}@if(false==empty($dataCustomersOrder['search']))&search={{$dataCustomersOrder['search']}}@endif">Next</a>
                        </li>
                    @endif
                </ul>
            </nav>
        </div>
    </div>
@stop