@extends('layouts.layouts')
@section('content')
    <div class="row" style="padding-top: 10px;">
        <div class="col-lg-12">
            <ol class="breadcrumb" style="margin: 0px;">
                <li><a href="/factory/MarkOrder">ผลิตสินค้า(ฝ่ายสำนักงาน)</a></li>
                <li class="active">ใบเสนอราคา</li>
            </ol>
        </div>
    </div>
    <form method="POST" action="/factory/editOrder" class="form-horizontal" novalidate >
        <input type="hidden" name="customers_order_id" value="{{$dataCustomersOrder->customers_order_id}}">
    <div class="row">
        <div class="col-lg-12" style="padding: 10px;">
            <span style="font-size: 26px;font-weight: bolder">ใบเสนอราคา</span>
        </div>
    </div>
    <div class="row" style="padding: 10px;">
        <div class="col-lg-8"></div>
        <div class="col-lg-4">
            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> บันทึก</button>
            <a target="_blank" href="/factory/PrintQuotation?orderId={{$orderId}}" class="btn btn-default"><span class="glyphicon glyphicon-print" aria-hidden="true"></span> พิมพ์ใบเสนอราคา</a>
            <a target="_blank" href="/factory/PrintDeposits?orderId={{$orderId}}" class="btn btn-default"><span class="glyphicon glyphicon-print" aria-hidden="true"></span> พิมพ์ใบมัดจำ</a>
        </div>
    </div>

    <div class="row" style="padding: 10px;">
        <div class="col-lg-12">
            <div style="padding: 5px;border: solid 1px #444;border-radius: 5px;background-color: #aaa;font-size: 16px;">
                <div style="text-align: center;font-weight: bolder;">สำนักงานใหญ่</div>
                <div style="float: left;width: 100%;padding: 5px;">
                    <div style="float: left;">เลขประจำตัวผู้เสียภาษี: </div>
                    <div style="float: left;padding-left: 10px;">
                        <input type="text" class="form-control input-sm" name="customers_order_tax_number" id="exampleInputPassword1" placeholder="เลขประจำตัวผู้เสียภาษี" value="{{$dataCustomersOrder->customers_order_tax_number}}">
                    </div>
                </div>
                <div style="float: left;width: 100%;padding: 5px;">
                    <div style="float: left;width: 70%;">
                        <div style="float: left;">ชื่อลูกค้า: </div>
                        <div style="float: left;padding-left: 10px;">{{$dataCustomersOrder->customers_order_customers}}</div>
                    </div>
                    <div style="float: left;width: 30%;">
                        <div style="float: left;width: 100%;padding: 5px;">
                            <div style="float: left;">หมายเลขใบเสนอราคา:</div>
                            <div style="float: left;padding-left: 10px;">
                                <input type="text" class="form-control input-sm" name="customers_order_number" id="exampleInputPassword1" placeholder="หมายเลขใบเสนอราคา" value="{{$dataCustomersOrder->customers_order_number}}">
                            </div>
                        </div>
                        <div style="float: left;width: 100%;padding: 5px;">
                            <div style="float: left;">วันที่ : </div>
                            <div style="float: left;padding-left: 10px;">
                                <input type="text" class="form-control input-sm" name="customers_order_date" id="datepicker" placeholder="วันที่" value="{{Carbon\Carbon::parse($dataCustomersOrder->customers_order_date)->format('Y-m-d')}}">
                            </div>
                        </div>
                    </div>
                </div>
                <div style="float: left;width: 100%;padding: 5px;">
                    <div style="float: left;">ชื่องาน: </div>
                    <div style="float: left;padding-left: 10px;">{{$dataCustomersOrder->customers_order_name}}</div>
                </div>
                <div style="clear: both;"></div>
            </div>
        </div>
    </div>

    <div class="row" style="padding: 0px;margin: 0px;">
         <div class="col-lg-12" style="padding: 0px;margin: 0px;">
             <a style="float: right;" class="btn btn-primary" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> เพิ่มสินค้า</a>
                {{--<a style="" class="btn btn-danger" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> ยกเลิกการผลิต</a>--}}
         </div>
    </div>

    <div class="row" style="padding: 10px;">
        <div class="col-lg-12">
            <table class="table" style="border: solid 1px #000000;">
                <thead>
                <tr>
                    <th>#</th>
                    <th>รหัสสินค้า/รายละเอียด</th>
                    <th style="text-align: center;width: 100px;">จำนวน</th>
                    <th style="text-align: right;width: 100px;">หน่วยล่ะ</th>
                    <th style="text-align: right;width: 100px;">จำนวนเงิน</th>
                    <th style="text-align: center;width: 40px;">#</th>
                </tr>
                </thead>
                <tbody>
                @foreach($dataCustomersOrderProduct as $key=>$value)
                    <div style="display: none;">{{$sum+=($value->customers_order_product_price*$value->customers_order_product_count)}}</div>
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$value->customers_order_product_name}}</td>
                        <td style="text-align: center;">{{$value->customers_order_product_count}}</td>
                        <td style="text-align: right;">{{number_format($value->customers_order_product_price,2)}}</td>
                        <td style="text-align: right;">{{number_format(($value->customers_order_product_price*$value->customers_order_product_count),2)}}</td>
                        <td style="text-align: center;"><a href="/factory/checkMaterialMarkOrder?orderId={{$orderId}}&productId={{$value->customers_order_product_id}}" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
                    </tr>
                @endforeach
                <tr><td colspan="6"></td></tr>
                <tr>
                    <td style="border-top: solid 1px #000000;" colspan="2"></td>
                    <td style="border-top: solid 1px #000000;" colspan="2">รวมเป็น</td>
                    <td style="border-top: solid 1px #000000;text-align: right;">{{number_format($sum,2)}}</td>
                    <td style="border-top: solid 1px #000000;text-align: left;padding-left: 0px;">บาท</td>
                </tr>
                <tr>
                    <td style="border-top: solid 1px #FFFFff;" colspan="2"></td>
                    <td style="border-top: solid 1px #FFFFff;" colspan="2">
                        <span style="float: left;padding-right: 5px;">หักส่วนลด</span>
                        <select style="float: left;width: 50px;padding: 0px;" class="form-control input-sm" name="customers_order_type_discount">
                            <option {{(($dataCustomersOrder->customers_order_type_discount=='Normal')?'selected="selected"':'')}} value="Normal">บาท</option>
                            <option {{(($dataCustomersOrder->customers_order_type_discount=='Percent')?'selected="selected"':'')}} value="Percent">%</option>
                        </select>
                        <input style="float: left;width: 60px;padding: 0px;" type="number" min="0" max="1000000" name="customers_order_discount" class="form-control input-sm" id="inputPassword3" placeholder="ส่วนลด" value="{{$dataCustomersOrder->customers_order_discount}}"></td>
                    <td style="border-top: solid 1px #FFFFff;text-align: right;">
                        @if($dataCustomersOrder->customers_order_type_discount=='Percent')
                            {{number_format((($sum*$dataCustomersOrder->customers_order_discount)/100),2)}}
                        @elseif($dataCustomersOrder->customers_order_type_discount=='Normal')
                            {{number_format($dataCustomersOrder->customers_order_discount,2)}}
                        @endif
                    </td>
                    <td style="border-top: solid 1px #FFFFff;text-align: left;padding-left: 0px;" >บาท</td>
                </tr>
                <tr>
                    <td style="border-top: solid 1px #FFFFff;" colspan="2"></td>
                    <td style="border-top: solid 1px #FFFFff;" colspan="2">จำนวนเงินหลังหักส่วนลด</td>
                    <td style="border-top: solid 1px #FFFFff;text-align: right;">
                        @if($dataCustomersOrder->customers_order_type_discount=='Percent')
                            {{number_format((($sum+(($sum*$dataCustomersOrder->customers_order_tax)/100))-(($sum*$dataCustomersOrder->customers_order_discount)/100)))}}
                        @elseif($dataCustomersOrder->customers_order_type_discount=='Normal')
                            {{number_format($sum-$dataCustomersOrder->customers_order_discount,2)}}
                        @endif
                        {{--{{number_format($sum-$dataCustomersOrder->customers_order_discount,2)}}--}}
                    </td>
                    <td style="border-top: solid 1px #FFFFff;text-align: left;padding-left: 0px;" >บาท</td>
                </tr>
                <tr>
                    <td style="border-top: solid 1px #FFFFff;" colspan="2"></td>
                    <td style="border-top: solid 1px #FFFFff;" colspan="2">
                        <span style="float: left;padding-right: 5px;">จำนวนภาษีมูลค่าเพิ่ม</span>
                        <input style="float:left;width: 35px;padding: 0px;" type="number" min="0" max="100" name="customers_order_tax" class="form-control input-sm" id="inputPassword3" placeholder="ภาษี" value="{{$dataCustomersOrder->customers_order_tax}}">
                        <span style="float: left;padding-left: 5px;">%</span>
                    </td>
                    <td style="border-top: solid 1px #FFFFff;text-align: right;" >
                        {{number_format(((($sum*$dataCustomersOrder->customers_order_tax)/100)),2)}}
                    </td>
                    <td style="border-top: solid 1px #FFFFff;text-align: left;padding-left: 0px;" >บาท</td>
                </tr>
                <tr>
                    <td style="border-top: solid 1px #FFFFff;" colspan="2"></td>
                    <td style="border-top: solid 1px #FFFFff;font-size:18px;font-weight: bolder;" colspan="2">จำนวนเงินรวมทั้งสิ้น</td>
                    <td style="border-top: solid 1px #FFFFff;font-size:18px;text-align: right;font-weight: bolder;" >
                        @if($dataCustomersOrder->customers_order_type_discount=='Percent')
                            {{number_format((($sum+(($sum*$dataCustomersOrder->customers_order_tax)/100))-(($sum*$dataCustomersOrder->customers_order_discount)/100)),2)}}
                        @elseif($dataCustomersOrder->customers_order_type_discount=='Normal')
                            {{number_format((($sum+(($sum*$dataCustomersOrder->customers_order_tax)/100))-$dataCustomersOrder->customers_order_discount),2)}}
                        @endif
                        {{--{{number_format((($sum+(($sum*$dataCustomersOrder->customers_order_tax)/100))-$dataCustomersOrder->customers_order_discount),2)}}--}}
                    </td>
                    <td style="border-top: solid 1px #FFFFff;font-size:18px;text-align: left;padding-left: 0px;font-weight: bolder;" >บาท</td>
                </tr>
                <tr>
                    <td style="border-top: solid 1px #FFFFff;">
                        <div style="font-size: 15px;font-weight: bolder;padding: 5px;">มัดจำ</div>
                    </td>
                    <td style="border-top: solid 1px #FFFFff;">
                        <div style="float: right;">
                            <div style="float: left;">
                                <div style="font-size: 15px;font-weight: bolder;padding: 5px;float: left;" for="exampleInputName2">วันที่มัดจำ</div>
                                <div style="float: left;">
                                    <input name="customers_order_deposit_date_set" value="{{$dataCustomersOrder->customers_order_deposit_date_set}}" style="float: left;width: 100px;" type="text" class="form-control" id="datepickerSet" placeholder="วันที่มัดจำ">
                                </div>
                            </div>
                            <div style="float: left;">
                                <div style="font-size: 15px;font-weight: bolder;padding: 5px;float: left;" for="exampleInputEmail2">วันที่ครบกำหนด</div>
                                <div style="float: left;">
                                    <input name="customers_order_deposit_date_limit" value="{{$dataCustomersOrder->customers_order_deposit_date_limit}}" style="float: left;width: 100px;" type="text" class="form-control" id="datepickerLimit" placeholder="วันที่ครบกำหนด">
                                </div>
                            </div>
                            <div style="float: left;">
                                <div style="font-size: 15px;font-weight: bolder;padding: 5px;float: left;" for="exampleInputEmail2">จำนวนเงินมัดจำ</div>
                                <div style="float: left;">
                                    <input name="customers_order_deposit" value="{{$dataCustomersOrder->customers_order_deposit}}" style="float: left;width: 100px;" type="number" class="form-control" placeholder="จำนวนเงินมัดจำ">
                                </div>
                            </div>
                        </div>
                    </td>
                    <td style="border-top: solid 1px #fff;" colspan="2">มัดจำ</td>
                    <td style="border-top: solid 1px #fff;text-align: right;">{{number_format($dataCustomersOrder->customers_order_deposit,2)}}</td>
                    <td style="border-top: solid 1px #fff;text-align: left;padding-left: 0px;">บาท</td>
                </tr>
                <tr>
                    <td style="border-top: solid 1px #FFFFff;" colspan="2"></td>
                    <td style="border-top: solid 1px #000;border-bottom:solid  2px;font-size:18px;font-weight: bolder;" colspan="2">หลังหักมัดจำ</td>
                    <td style="border-top: solid 1px #000;border-bottom:solid 2px;font-size:18px;text-align: right;font-weight: bolder;" >
                        @if($dataCustomersOrder->customers_order_type_discount=='Percent')
                            {{number_format(((($sum+(($sum*$dataCustomersOrder->customers_order_tax)/100))-(($sum*$dataCustomersOrder->customers_order_discount)/100))-$dataCustomersOrder->customers_order_deposit),2)}}
                        @elseif($dataCustomersOrder->customers_order_type_discount=='Normal')
                            {{number_format(((($sum+(($sum*$dataCustomersOrder->customers_order_tax)/100))-$dataCustomersOrder->customers_order_discount)-$dataCustomersOrder->customers_order_deposit),2)}}
                        @endif
                    </td>
                    <td style="border-top: solid 1px #000;border-bottom:solid 2px;" ></td>
                </tr>
                <tr><td></td></tr>
                </tbody>
            </table>
        </div>
    </div>
    </form>

    <script>
        function SetInput(id) {
            $('#InputCustomersOrderProduct').val(id);
        }
    </script>

    <script type="text/javascript">
        $(function() {
            $( "#datepicker,#datepickerSet,#datepickerLimit" ).datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat:'yy-mm-dd'
            });
        });
    </script>

    <style>
        .ui-datepicker-month,.ui-datepicker-year{
            color: #333;
        }
    </style>

    {{--<div class="row" style="display: none;">--}}
        {{--<div class="col-lg-12" style="text-align: center;">--}}
            {{--<nav>--}}
                {{--<ul class="pagination" style="margin: 0px;padding: 0px;padding-top: 3px;">--}}
                    {{--@if($dataCustomersOrderProduct['page']>1)--}}
                        {{--<li>--}}
                            {{--<a href="/factory/checkMaterialMarkOrder?orderId={{$orderId}}&page={{$dataCustomersOrderProduct['page']-1}}@if(false==empty($dataCustomersOrderProduct['search']))&search={{$dataCustomersOrderProduct['search']}}@endif">Prev</a>--}}
                        {{--</li>--}}
                    {{--@endif--}}

                    {{--@for($x = 1; $x <= $dataCustomersOrderProduct['max']; $x++)--}}
                        {{--@if($x==$dataCustomersOrderProduct['page'])--}}
                            {{--<li class="active"><a--}}
                                        {{--href="/factory/checkMaterialMarkOrder?orderId={{$orderId}}&page={{$x}}@if(false==empty($dataCustomersOrderProduct['search']))&search={{$dataCustomersOrderProduct['search']}}@endif">{{$x}}</a>--}}
                            {{--</li>--}}
                        {{--@else--}}
                            {{--<li>--}}
                                {{--<a href="/factory/checkMaterialMarkOrder?orderId={{$orderId}}&page={{$x}}@if(false==empty($dataCustomersOrderProduct['search']))&search={{$dataCustomersOrderProduct['search']}}@endif">{{$x}}</a>--}}
                            {{--</li>--}}
                        {{--@endif--}}
                    {{--@endfor--}}

                    {{--@if($dataCustomersOrderProduct['page']<$dataCustomersOrderProduct['max'])--}}
                        {{--<li>--}}
                            {{--<a href="/factory/checkMaterialMarkOrder?orderId={{$orderId}}&page={{$dataCustomersOrderProduct['page']+1}}@if(false==empty($dataCustomersOrderProduct['search']))&search={{$dataCustomersOrderProduct['search']}}@endif">Next</a>--}}
                        {{--</li>--}}
                    {{--@endif--}}
                {{--</ul>--}}
            {{--</nav>--}}
        {{--</div>--}}
    {{--</div>--}}

    <style>
        .twitter-typeahead{
            width: 100% !important;
        }
        .tt-query {
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        }

        .tt-hint {
            color: #999
        }

        .tt-menu {
            width: 422px;
            margin: 12px 0;
            padding: 8px 0;
            background-color: #fff;
            border: 1px solid #ccc;
            border: 1px solid rgba(0, 0, 0, 0.2);
            -webkit-border-radius: 8px;
            -moz-border-radius: 8px;
            border-radius: 8px;
            -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
            -moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
            box-shadow: 0 5px 10px rgba(0,0,0,.2);
        }

        .tt-suggestion {
            padding: 3px 20px;
            font-size: 18px;
            line-height: 24px;
        }

        .tt-suggestion:hover {
            cursor: pointer;
            color: #fff;
            background-color: #0097cf;
        }

        .tt-suggestion.tt-cursor {
            color: #fff;
            background-color: #0097cf;

        }

        .tt-suggestion p {
            margin: 0;
        }

        .gist {
            font-size: 14px;
        }
    </style>


    <script>
        $(document).ready(function () {
            $('select#selectStorename').val(2);
            var substringMatcher = function (strs) {
                return function findMatches(q, cb) {
                    var matches, substringRegex;

                    // an array that will be populated with substring matches
                    matches = [];

                    // regex used to determine if a string contains the substring `q`
                    substrRegex = new RegExp(q, 'i');

                    // iterate through the pool of strings and for any string that
                    // contains the substring `q`, add it to the `matches` array
                    $.each(strs, function (i, str) {
                        if (substrRegex.test(str)) {
                            matches.push(str);
                        }
                    });

                    cb(matches);
                };
            };

            $.ajax({
                url: "/factory/checkProductManufacture",
                type: "post",
                dataType: 'json'
            }).success(function (res) {
                $('input#the-basics').typeahead({
                            hint: true,
                            highlight: true,
                            minLength: 1
                        },
                        {
                            name: 'states',
                            source: substringMatcher(res)
                        });
            })
        });

        function addPR(product_name,job_name,com_name,quantity){
            $.ajax({
                url: "/factory/addprimmediately",
                type: "post",
                data:{pname:product_name,jname:job_name,cname:com_name,quantity:quantity},
                dataType: 'json'
            }).success(function (res) {
                console.log(res);
                if(res.success){
                    alert("New PR added");
                }
            });
        }
    </script>

    <div class="modal fade @if (!empty($CustomersOrderProduct->customers_order_id) || true==$errors->has('customers_order_product_name') || true==$errors->has('customers_order_product_count') || true==$errors->has('customers_order_product_price')) in @endif" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="@if (!empty($CustomersOrderProduct->customers_order_id) || true==$errors->has('customers_order_product_name')||true==$errors->has('customers_order_product_count')) display: block; @endif">
        <div class="modal-dialog" role="document">
            <form method="POST" action="/factory/addProductOrderCheckMaterial" class="form-horizontal" novalidate >
                <div class="modal-content">
                    <div class="modal-header">
                        <a href="/factory/checkMaterialMarkOrder?orderId={{$orderId}}" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                        <h4 class="modal-title" id="myModalLabel">เพิ่มสินค้าที่จะผลิต</h4>
                    </div>
                    <div class="modal-body">
                        @if (true==$errors->has('customers_order_product_name')||true==$errors->has('customers_order_product_count'))
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </div>
                        @endif
                        <div class="form-horizontal" style="padding: 0px 20px;">
                            <input type="hidden" name="customers_order_product_id" value="{{$CustomersOrderProduct->customers_order_product_id}}"/>
                            <input type="hidden" name="customers_order_id" value="{{$orderId}}"/>
                                <div class="form-group @if ($errors->has('customers_order_product_type')) has-error @endif">
                                    <label for="exampleInputEmail1">รูปแบการผลิต</label>
                                    <label class="radio-inline" style="margin-left: 15px;padding-top: 0px;">
                                        <input type="radio" checked name="customers_order_product_type" id="inlineRadio1" value="Normal"> ผลิตตามใบสั่งซื้อ
                                    </label>
                                    <label class="radio-inline" style="margin-left: 15px;padding-top: 0px;">
                                        <input type="radio" name="customers_order_product_type" id="inlineRadio2" value="Supplement"> ผลิตเสริม
                                    </label>
                                </div>
                            <div class="form-group @if ($errors->has('customers_order_product_name')) has-error @endif">
                                <label for="exampleInputEmail1">ชื่อสำเร็จรูป</label>
                                <input id="the-basics" value="{{(!empty($CustomersOrderProduct->customers_order_product_name)?$CustomersOrderProduct->customers_order_product_name:Input::old('customers_order_product_name'))}}" name="customers_order_product_name" type="text" class="form-control" id="exampleInputEmail1" placeholder="ชื่อสำเร็จรูป">
                            </div>
                            <div class="form-group @if ($errors->has('customers_order_product_count')) has-error @endif">
                                <label for="exampleInputPassword1">จำนวน</label>
                                <input type="text" value="{{(!empty($CustomersOrderProduct->customers_order_product_count)?$CustomersOrderProduct->customers_order_product_count:Input::old('customers_order_product_count'))}}" name="customers_order_product_count" class="form-control" id="exampleInputPassword1" placeholder="จำนวน">
                            </div>
                            <div class="form-group @if ($errors->has('customers_order_product_price')) has-error @endif">
                                <label for="exampleInputPassword1">ราคา</label>
                                <input type="text" value="{{(!empty($CustomersOrderProduct->customers_order_product_price)?$CustomersOrderProduct->customers_order_product_price:Input::old('customers_order_product_price'))}}" name="customers_order_product_price" class="form-control" id="exampleInputPassword1" placeholder="ราคา">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="/factory/checkMaterialMarkOrder?orderId={{$orderId}}" type="button" class="btn btn-default" data-dismiss="modal">Close</a>
                        <input type="submit" class="btn btn-primary" value="Save changes" />
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop