@extends('layouts.layouts')
@section('content')
    <div class="row" style="padding-top: 10px;">
        <div class="col-lg-12">
            <ol class="breadcrumb" style="margin: 0px;">
                <li><a href="/factory/MarkOrderStore">ผลิตสินค้า(ฝ่ายคลัง)</a></li>
                <li class="active">ยืนยันการเบิกสินค้าเพื่อผลิต</li>
            </ol>
        </div>
    </div>
    <div class="row" style="padding: 10px 0px;">
        <div class="col-lg-4" style="padding: 10px;">
            <span style="font-size: 26px;font-weight: bolder">ยืนยันการเบิกสินค้าเพื่อผลิต</span>
        </div>
    </div>
    <div class="row" style="padding-bottom: 10px;">
        <div class="col-lg-10">
            <form method="GET" action="/factory/BringManufactureStore" class="form-inline">
                {{--<input type="hidden" id="order_product_id" name="customers_order_product_id" value="@if($newCustomersOrderPlanMaterial->order_product_id){{$newCustomersOrderPlanMaterial->order_product_id}}@endif">--}}
                <input type="hidden" id="order_id" name="orderId" value="{{$orderId}}">
                {{--<input type="hidden" id="order_plan_id" name="customers_order_plan_id" value="@if($newCustomersOrderPlanMaterial->customers_order_plan_id){{$newCustomersOrderPlanMaterial->customers_order_plan_id}}@endif">--}}
                <select name="dateSelect" style="width: 200px;" class="form-control">
                    <option value="">เลือกวันที่จะผลิต</option>
                    @foreach($dataDateCustomersOrderPlan as $key=>$value)
                        <option value="{{$value->customers_order_plan_date}}">{{$value->customers_order_plan_date}}</option>
                    @endforeach
                </select>
                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                <a href="/factory/BringManufactureStore?orderId={{$orderId}}" class="btn btn-default"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span></a>
            </form>
        </div>
        <div class="col-lg-2">
            {{--<a style="" class="btn btn-default" data-toggle="modal" data-target="#myModaladd"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> สร้างแผนการผลิต</a>--}}
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th style="text-align: center;">วันที่</th>
                    <th style="text-align: center;">สภานะ</th>
                    <th style="text-align: center;">รายการสินค้าที่จะผลิต</th>
                    <th style="text-align: center;">งานที่จะผลิต</th>
                    <th style="text-align: center;">ส่งผลิต</th>
                </tr>
                </thead>
                <tbody>
                @foreach($dataCustomersOrderPlan as $key=>$value)
                    <tr>
                        <td>{{$value->customers_order_plan_date}}</td>
                        <td style="text-align: center;">
                            @if($value->customers_order_plan_status=='create')
                                <span class="label label-default">รอส่งอนุมัติ</span>
                            @elseif($value->customers_order_plan_status=='confirmation')
                                <span class="label label-info">รออนุมัติ</span>
                            @elseif($value->customers_order_plan_status='approve')
                                <span class="label label-success">อนุมัติ</span>
                            @endif
                        </td>
                        <td>{{\App\Models\CustomersOrderProduct::getNameProduct($value->customers_order_product_id)}} {{$value->customers_order_plan_count}} {{\App\Models\ProductUnit::getNameUnitByProductName(\App\Models\CustomersOrderProduct::getNameProduct($value->customers_order_product_id))}}</td>
                        <td>
                            @if(!empty(\App\Models\CustomersOrderPlanMaterial::getByOrderPlanId($value->customers_order_plan_id)))
                                <table class="table table-bordered">
                                    <tr>
                                        <th>สถานะ</th>
                                        <th>วัตถุดิบ</th>
                                        <th>คลัง</th>
                                        <th>ยอดคงคลัง</th>
                                        <th>จำนวนเบิก</th>
                                    </tr>
                                    @foreach(\App\Models\CustomersOrderPlanMaterial::getByOrderPlanId($value->customers_order_plan_id) as $keyMaterial=>$valueMaterial)

                                        <tr>
                                            <td>
                                                @if($valueMaterial->customers_order_plan_status=='Bring')
                                                    <span class="label label-success">เบิกเเล้ว</span>
                                                @elseif($valueMaterial->customers_order_plan_status=='create')
                                                    <span style="display: none;">{{$status=true}}</span>
                                                    <span class="label label-warning">ยังไม่ได้เบิก</span>
                                                @endif
                                            </td>
                                            <td>{{\App\Models\CustomersOrderMaterial::getNameMaterialName($valueMaterial->customers_order_material_id)}}</td>
                                            <td>{{\App\Models\Storename::getStoreName(\App\Models\CustomersOrderMaterial::getNameStoreId($valueMaterial->customers_order_material_id))}}</td>
                                            <td>{{\App\Models\Stores::CheckStoreCount(\App\Models\Product::GetIdProduct(\App\Models\CustomersOrderMaterial::getNameMaterialName($valueMaterial->customers_order_material_id)),\App\Models\CustomersOrderMaterial::getNameStoreId($valueMaterial->customers_order_material_id))}} {{\App\Models\ProductUnit::getNameUnitByProductName(\App\Models\CustomersOrderMaterial::getNameMaterialName($valueMaterial->customers_order_material_id))}}</td>
                                            <td>{{$valueMaterial->customers_order_material_count}} {{\App\Models\ProductUnit::getNameUnitByProductName(\App\Models\CustomersOrderMaterial::getNameMaterialName($valueMaterial->customers_order_material_id))}}</td>
                                        </tr>
                                    @endforeach
                                </table>
                            @endif
                        </td>
                        <td>
                            <a target="_blank" href="/factory/PrintBringManufactureStore?orderId={{$value->customers_order_plan_id}}" class="btn btn-primary"><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> พิมพ์ใบเบิก</a>
                            @if($status)
                                <a onclick="return confirm('ยืนยันการเบิกสินค้าทั้งหมด')" href="/factory/ConfirmBringMaterial?orderId={{$value->customers_order_plan_id}}" class="btn btn-success"><span class="glyphicon glyphicon-check" aria-hidden="true"></span> ยืนยันรับสินค้า</a>
                            @endif
                            {{--<a href="/factory/checkQualityManufacture?orderId={{$value->customers_order_plan_id}}" onclick="return confirm('ยืนยันการส่ง แผนนี้เพื่อ ตรวจคุณภาพ')" class="btn btn-info"><span class="glyphicon glyphicon-check" aria-hidden="true"></span> ส่งตรวจคุณภาพ</a>--}}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@stop