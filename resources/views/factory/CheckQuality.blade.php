@extends('layouts.layouts')
@section('content')
    <div class="row" style="padding-top: 10px;">
        <div class="col-lg-12">
            <ol class="breadcrumb" style="margin: 0px;">
                <li><a href="/factory/MarkOrderCheckQuality">ผลิตสินค้า(ฝ่ายตรวจสอบ)</a></li>
                <li class="active">ตรวจสอบสินค้าที่ผลิตเสร็จ</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12" style="padding: 10px;">
            <span style="font-size: 26px;font-weight: bolder">ตรวจสอบสินค้าที่ผลิตเสร็จ</span>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th>รายการสินค้าที่จะผลิต</th>
                    <th>จำนวน</th>
                    <th>ตรวจสอบ</th>
                </tr>
                </thead>
                <tbody>
                @foreach($dataCustomersOrderPlan as $key=>$value)
                    <tr>
                        <td>{{$value->customers_order_plan_date}}</td>
                        <td>{{\App\Models\CustomersOrderProduct::getNameProduct($value->customers_order_product_id)}}</td>
                        <td>{{$value->customers_order_plan_count}} {{\App\Models\ProductUnit::getNameUnitByProductName(\App\Models\CustomersOrderProduct::getNameProduct($value->customers_order_product_id))}}</td>
                        <td>
                            <a onclick="SetInput({{$value->customers_order_plan_id}},{{$value->customers_order_plan_count}})" class="btn btn-primary" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-check" aria-hidden="true"></span> ตรวจสอบ</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <script>
        function SetInput(order_id,count) {
            console.log(order_id);
            $('input#customers_order_plan_id').val(order_id);
            $('input#customers_order_plan_complete').val(count);
        }
    </script>

    <div class="modal fade @if (true==$errors->has('customers_order_plan_complete')||true==$errors->has('customers_order_plan_waste')) in @endif" style="@if (true==$errors->has('customers_order_plan_complete')||true==$errors->has('customers_order_plan_waste')) display: none @endif;" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <form method="POST" action="/factory/confirmCheckQuality" novalidate >
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">ผลตรวจสินค้า</h4>
                    </div>
                    <div class="modal-body">
                        @if (true==$errors->has('customers_order_customers')||true==$errors->has('customers_order_name'))
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </div>
                        @endif
                        <input type="hidden" name="customers_order_id" value="{{$orderId}}">
                        <input type="hidden" id="customers_order_plan_id" name="customers_order_plan_id" value="">
                        <div class="form-horizontal" style="padding: 0px 20px;">
                            <div class="form-group  @if ($errors->has('customers_order_plan_complete')) has-error @endif">
                                <label for="exampleInputPassword1">ผ่านการตรวจสอบ</label>
                                <input name="customers_order_plan_complete" type="text" class="form-control" id="customers_order_plan_complete" placeholder="จำนวน">
                            </div>
                            <div class="form-group  @if ($errors->has('customers_order_plan_waste')) has-error @endif">
                                <label for="exampleInputPassword1">ไม่ผ่านการตรวจสอบ</label>
                                <input name="customers_order_plan_waste" type="text" class="form-control" id="exampleInputPassword1" placeholder="จำนวน" value="0">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">ยืนยัน</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop