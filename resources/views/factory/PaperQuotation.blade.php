<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('page-title', 'PDF Document')</title>

</head>
<body style="font-family: angsana-new;">
<style >
    /*@font-face {*/
    /*font-family: "DejaVuSans";*/
    /*src:url('/font/DejaVuSans.ttf') format("truetype");*/
    /*}*/
    /*h1 { font-family: "Kimberley", sans-serif }*/
    table{
        width:100%;
        border: solid 1px;
    }
    tr{
        width: 100%;
        border: solid 1px;
    }
    td.main{
        border: solid 1px #000;
        width: 10%;
    }
    td.noneBorder{
        border: solid 1px #ffffff;
        width: 10%;
    {{--font-family: DejaVuSans;--}}
        font-size: 16px;
    }
    td.noneBorderTableHand{
        /*border: solid 1px #ffffff;*/
        width: 10%;
    {{--font-family: DejaVuSans;--}}
        font-size: 13px;
        text-align: center;
        line-height: 0.9;
        /*height: 20px;*/
    }
    td.noneBorderTable{
        /*border: solid 1px #ffffff;*/
        width: 10%;
    {{--font-family: DejaVuSans;--}}
        font-size: 11px;
    }

    /*tr.table>td:first-child{*/
    /*border-bottom: solid 1px;border-top: solid 1px;border-left: solid 1px;*/
    /*}*/
</style>
<table style="">
    {{--<tr>--}}
        {{--<td class="main"></td>--}}
        {{--<td class="main"></td>--}}
        {{--<td class="main"></td>--}}
        {{--<td class="main"></td>--}}
        {{--<td class="main"></td>--}}
        {{--<td class="main"></td>--}}
        {{--<td class="main"></td>--}}
        {{--<td class="main"></td>--}}
        {{--<td class="main"></td>--}}
        {{--<td class="main"></td>--}}
    {{--</tr>--}}
    <tr>
        <td colspan="10" style="font-size: 20px;line-height: 80%;">บริษัท วันเอ็ม จำกัด</td>
    </tr>
    <tr>
        <td colspan="10" style="font-size: 16px;">290 ม.1 ต.กระโสบ อ.เมือง จ.อุบลราธานี 34000</td>
    </tr>
    <tr>
        <td colspan="9" style="font-size: 20px;text-align: right;">ใบเสนอราคา
        <td></td>
    </tr>
    <tr>
        <td colspan="4">เลขประจำตัวผู้เสียภาษี {{$customersOrder->customers_order_tax_number}}</td>
        <td colspan="2" style="font-size: 20px;">สำนักงานใหญ่</td>
        <td colspan="4"></td>
    </tr>
    <tr>
        <td style="padding-left: 10px;">ถึง</td>
        <td colspan="5"></td>
        <td colspan="2" style="text-align: right;">เลขใบเสนอราคา</td>
        <td colspan="2" style="padding-left: 30px;">{{$customersOrder->customers_order_number}}</td>
    </tr>
    <tr>
        <td style="padding-left: 20px;" colspan="10">{{$customers->customers_name}}</td>
    </tr>
    <tr>
        <td colspan="6"></td>
        <td colspan="2" style="text-align: right;">วันที่</td>
        <td colspan="2">{{\App\Models\BEConverter::ADtoBE($customersOrder->customers_order_date)}}</td>
    </tr>
    <tr>
        <td colspan="10"></td>
    </tr>
    <tr>
        <td colspan="10"></td>
    </tr>
    <tr>
        <td colspan="6" style="padding-left: 10px;">โทร. {{$customers->customers_telephone}}</td>
        <td colspan="2" style="text-align: right;">ยืนราคา 0 วัน</td>
        <td colspan="2" style="padding-left: 30px;">ถึงวันที่</td>
    </tr>
    <tr>
        <td colspan="6" style="padding-left: 10px;">อ้างอิง</td>
        <td colspan="2" style="text-align: right;">เงื่อนไขชำระเงิน</td>
        <td colspan="2" style="padding-left: 30px;">เงินสด</td>
    </tr>
    <tr>
        <td colspan="10">บริษัทฯ มีความยินดีที่จะเสนอราคาสินค้า ดังต่อไปนี้ :</td>
    </tr>
    {{--<tr>--}}
        {{--<td class="noneBorder"><img style="width:50px" src="./image/onem.jpg"/></td>--}}
        {{--<td class="noneBorder" colspan="7" style="text-align: center;">--}}
            {{--<span style="font-size:18px;">ใบเบิกสินค้า {{(($dataCustomersOrderPlan->customers_order_plan_type=='Normal')?'เพื่อผลิตตามแผน':'เพื่อผลิตทดแทน')}}</span><br>--}}
            {{--<span style="font-size:14px;">วันที่เบิก........................... เดือน..................................... พ.ศ. ..............</span>--}}
            {{--<div style="font-size:14px;">ตามแผนผลิต วันที่.........{{Carbon\Carbon::parse($dataCustomersOrderPlan->customers_order_plan_date)->format('d')}}........... เดือน.....{{Carbon\Carbon::parse($dataCustomersOrderPlan->customers_order_plan_date)->format('F')}}..... พ.ศ. ..{{Carbon\Carbon::parse($dataCustomersOrderPlan->customers_order_plan_date)->format('Y')}}.. ผู้รับผิดชอบ ....{{$dataCustomersOrderPlan->customers_order_plan_staff}}....</div>--}}
        {{--</td>--}}
        {{--<td colspan="2" class="noneBorder" style="padding-bottom: 30px;">--}}
            {{--<span style="font-size:16px;">เลขที่แผนผลิต  {{$dataCustomersOrderPlan->customers_order_plan_id}}</span><br>--}}
            {{--<span style="font-size:10px;color: #FFFFff;">ว</span>--}}
        {{--</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
        {{--<td class="noneBorder" colspan="5">ชื่อผู้เบิก.............................................................................</td>--}}
        {{--<td class="noneBorder" colspan="5">ตำแหน่ง.................................................</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
        {{--<td class="noneBorder" colspan="1" style="line-height: 0.4;">วัตถุประสงค์การใช้งาน</td>--}}
        {{--<td class="noneBorder" colspan="3" style="line-height: 0.6;">{{(($dataCustomersOrderProduct->customers_order_product_type=='Normal')?'เพื่อผลิตตามใบสั่งซื้อ':'เพื่อผลิตเสริม')}}</td>--}}
        {{--<td class="noneBorder" colspan="1" style="line-height: 0.4;">เพื่อผลิต</td>--}}
        {{--<td class="noneBorder" colspan="5" style="line-height: 0.4;">--}}
            {{--{{$dataCustomersOrderProduct->customers_order_product_name}}--}}
            {{--จำนวน {{$dataCustomersOrderProduct->customers_order_product_count}}--}}
            {{--{{\App\Models\ProductUnit::getNameUnitByProductName(\App\Models\CustomersOrderProduct::getNameProduct($dataCustomersOrderProduct->customers_order_product_id))}}--}}
        {{--</td>--}}
    {{--</tr>--}}
    <tr>
        <td colspan="10"></td>
    </tr>
    <tr class="table">
        <td  class="noneBorderTableHand"><div style="height: 21px;border: solid 1px;">No.</div></td>
        <td  class="noneBorderTableHand" colspan="5"><div style="text-align:center;height: 21px;margin-left: -5px;border: solid 1px;">รหัสสินค้า/รายละเอียด</div></td>
        <td  class="noneBorderTableHand" colspan="2"><div style="height: 21px;margin-left: -5px;border: solid 1px;">จำนวน</div></td>
        <td  class="noneBorderTableHand" ><div style="height: 21px;margin-left: -5px;border:solid 1px;">หน่วยล่ะ</div></td>
        <td  class="noneBorderTableHand"><div style="height: 21px;margin-left: -5px;border: solid 1px;">จำนวนเงิน</div></td>
    </tr>
    {{--<tr>--}}
        {{--<td class="noneBorderTableHand" ><div style="height: 20px;border:solid 1px;border-top: solid 0px;margin-top: -5px;"></div></td>--}}
        {{--<td class="noneBorderTableHand" colspan="5"><div style="height: 20px;margin-left: -5px;border:solid 1px;margin-top: -5px;border-top: solid 0px;"></div></td>--}}
        {{--<td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px;border-top: solid 0px;"></div></td>--}}
        {{--<td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px">จำนวน</div></td>--}}
        {{--<td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px">หน่วยนับ</div></td>--}}
        {{--<td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px;border-top: solid 0px;"></div></td>--}}
    {{--</tr>--}}
    @foreach($customersOrderProduct as $key=>$value)
        <div style="display: none;">{{$sum+=($value->customers_order_product_price*$value->customers_order_product_count)}}</div>
            <tr>
                <td class="noneBorderTableHand" >
                    <div style="height: 20px;margin-top: -5px;border-left: solid 1px;">
                        <span style="">{{$key+1}}</span>
                    </div>
                </td>
                <td class="noneBorderTableHand" colspan="5">
                    <div style="height: 20px;margin-left: -5px;margin-top: -5px;text-align: left;">
                        <span style="">{{$value->customers_order_product_name}}</span>
                    </div>
                </td>
                <td class="noneBorderTableHand" colspan="2">
                    <div style="height: 20px;;margin-left: -5px;margin-top: -5px;">
                        <span style="">{{number_format($value->customers_order_product_count,2)}} {{\App\Models\ProductUnit::getNameUnitByProductName($value->customers_order_product_name)}}</span>
                    </div>
                </td>
                <td class="noneBorderTableHand">
                    <div style="height: 20px;margin-left: -5px;margin-top: -5px;text-align: right;">
                        <span style="padding-right: 5px;">{{number_format($value->customers_order_product_price,2)}}</span>
                    </div>
                </td>
                <td class="noneBorderTableHand">
                    <div style="height: 20px;margin-left: -5px;margin-top: -5px;border-right: solid 1px;text-align: right;">
                        <span style="padding-right: 5px;">{{number_format(($value->customers_order_product_price*$value->customers_order_product_count),2)}}</span>
                    </div>
                </td>
            </tr>
    @endforeach

    @for($x = 0; $x <= (15-count($customersOrderProduct)); $x++)
        @if($x==(15-count($customersOrderProduct)))
            <tr>
                <td colspan="10">
                    <div style="height: 20px;margin-top: -5px;border-left: solid 1px;border-right: solid 1px;border-bottom: solid 1px;"></div>
                </td>
            </tr>
        @else
            <tr>
                <td colspan="10" >
                    <div style="height: 20px;margin-top: -5px;border-left: solid 1px;border-right: solid 1px;"></div>
                </td>
            </tr>
        @endif
    @endfor


    {{--<tr>--}}
        {{--<td colspan="10"></td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
        {{--<td class="noneBorderTableHand" colspan="4">--}}
            {{--<div style="border: solid 1px; margin-top: -5px">--}}
                {{--<div>ผู้ขอเบิก</div>--}}
                {{--<div style="margin-top: 10px;">(.........................................)</div>--}}
                {{--<div style="margin-top: 10px;">วันที่................................</div>--}}
            {{--</div>--}}
        {{--</td>--}}
        {{--<td class="noneBorderTableHand" colspan="3">--}}
            {{--<div style="border: solid 1px; margin-top: -5px">--}}
                {{--<div>ผู้อนุมัติเบิก</div>--}}
                {{--<div style="margin-top: 10px;">(.........................................)</div>--}}
                {{--<div style="margin-top: 10px;">วันที่................................</div>--}}
            {{--</div>--}}
        {{--</td>--}}
        {{--<td class="noneBorderTableHand" colspan="2">--}}
            {{--<div style="border: solid 1px; margin-top: -5px">--}}
                {{--<div>ผู้อนุมัติจ่าย</div>--}}
                {{--<div style="margin-top: 10px;">(.........................................)</div>--}}
                {{--<div style="margin-top: 10px;">วันที่................................</div>--}}
            {{--</div>--}}
        {{--</td>--}}
        {{--<td class="noneBorderTableHand" colspan="3">--}}
            {{--<div style="border: solid 1px; margin-top: -5px">--}}
                {{--<div>เจ้าหน้าที่คลัง</div>--}}
                {{--<div style="margin-top: 10px;">(.........................................)</div>--}}
                {{--<div style="margin-top: 10px;">วันที่................................</div>--}}
            {{--</div>--}}
        {{--</td>--}}
        {{--<td class="noneBorderTableHand" colspan="2">--}}
            {{--<div style="border: solid 1px; margin-top: -5px">--}}
                {{--<div>บัญชี</div>--}}
                {{--<div style="margin-top: 10px;">(.........................................)</div>--}}
                {{--<div style="margin-top: 10px;">วันที่................................</div>--}}
            {{--</div>--}}
        {{--</td>--}}
    {{--</tr>--}}
    <tr class="noneBorderTableHand">
        <td colspan="6" style=""><div style="height: 20px;margin-top: -5px;border-left: solid 1px;"></div></td>
        <td colspan="3" style=""><div style="height: 20px;margin-top: -5px;">รวมเป็นเงิน</div></td>
        <td style="text-align: right;"><div style="height: 20px;margin-top: -5px;border-right: solid 1px;"><span style="padding-right: 5px;">{{number_format($sum,2)}}</span></div></td>
    </tr>
    <tr class="noneBorderTableHand">
        <td colspan="6" style=""><div style="height: 20px;margin-top: -5px;border-left: solid 1px;"></div></td>
        <td colspan="3" style=""><div style="height: 20px;margin-top: -5px;">หักส่วนลด {{$customersOrder->customers_order_discount}} {{(($customersOrder->customers_order_type_discount=='Normal')?'บาท':'%')}}</div></td>
        <td style="text-align: right;"><div style="height: 20px;margin-top: -5px;border-right: solid 1px;"><span style="padding-right: 5px;">
                    @if($customersOrder->customers_order_type_discount=='Percent')
                        {{number_format((($sum*$customersOrder->customers_order_discount)/100),2)}}
                    @elseif($customersOrder->customers_order_type_discount=='Normal')
                        {{number_format($customersOrder->customers_order_discount,2)}}
                    @endif
                    {{--{{number_format($customersOrder->customers_order_discount,2)}}--}}
                </span></div></td>
    </tr>
    <tr class="noneBorderTableHand">
        <td colspan="6" style=""><div style="height: 20px;margin-top: -5px;border-left: solid 1px;"></div></td>
        <td colspan="3" style=""><div style="height: 20px;margin-top: -5px;">จำนวนเงินหลังหักส่วนลด</div></td>
        <td style="text-align: right;"><div style="height: 20px;margin-top: -5px;border-right: solid 1px;"><span style="padding-right: 5px;">
                    @if($customersOrder->customers_order_type_discount=='Percent')
                        {{number_format((($sum+(($sum*$customersOrder->customers_order_tax)/100))-(($sum*$customersOrder->customers_order_discount)/100)))}}
                    @elseif($customersOrder->customers_order_type_discount=='Normal')
                        {{number_format($sum-$customersOrder->customers_order_discount,2)}}
                    @endif
                </span></div></td>
    </tr>
    <tr class="noneBorderTableHand">
        <td colspan="6" style=""><div style="height: 20px;margin-top: -5px;border-left: solid 1px;"></div></td>
        <td colspan="3" style="">
            <div style="height: 20px;margin-top: -5px;">จำนวนภาษีมูลค่าเพิ่ม {{$customersOrder->customers_order_tax}}%</div>
        </td>
        <td style="text-align: right;"><div style="height: 20px;margin-top: -5px;border-right: solid 1px;"><span style="padding-right: 5px;">{{number_format(((($sum*$customersOrder->customers_order_tax)/100)),2)}}</span></div></td>
    </tr>
    <tr class="noneBorderTableHand">
        <td colspan="6" style="">
            <div style="height: 20px;margin-top: -5px;border-left: solid 1px;padding-left: 3px;">(
                @if($customersOrder->customers_order_type_discount=='Percent')
                    {{\App\Models\Customers::Convert((($sum+(($sum*$customersOrder->customers_order_tax)/100))-(($sum*$customersOrder->customers_order_discount)/100)))}}
                @elseif($customersOrder->customers_order_type_discount=='Normal')
                    {{\App\Models\Customers::Convert((($sum+(($sum*$customersOrder->customers_order_tax)/100))-$customersOrder->customers_order_discount))}}
                @endif
                )</div></td>
        <td colspan="3" style="text-align: center;">
            <div style="height: 20px;margin-top: -5px;">จำนวนเงินรวมทั้งสิ้น</div>
        </td>
        <td style="text-align: right;">
            <div style="height: 20px;margin-top: -5px;border-right: solid 1px;">
                <span style="padding-right: 5px;">
                    @if($customersOrder->customers_order_type_discount=='Percent')
                        {{number_format((($sum+(($sum*$customersOrder->customers_order_tax)/100))-(($sum*$customersOrder->customers_order_discount)/100)),2)}}
                    @elseif($customersOrder->customers_order_type_discount=='Normal')
                        {{number_format((($sum+(($sum*$customersOrder->customers_order_tax)/100))-$customersOrder->customers_order_discount),2)}}
                    @endif
                </span>
            </div>
        </td>
    </tr>

    <tr class="noneBorderTableHand">
        <td colspan="10">
            <div style="height: 20px;margin-top: -5px;border-left:solid 1px;border-right:solid 1px;border-bottom: solid 1px;"><span style="color: #FFFFff;">.</span></div>
        </td>
    </tr>

    <tr class="noneBorderTableHand">
        <td colspan="7" style="">
            <div style="height: 20px;margin-top: -5px;border-left: solid 1px;padding-left: 30px;">หมายเหตุ:</div>
        </td>
        <td colspan="3" style="">
            <div style="height: 20px;margin-top: -5px;border-right: solid 1px;text-align: center">ขอแสดงความนับถือ</div>
        </td>
    </tr>
    <tr class="noneBorderTableHand">
        <td colspan="10">
            <div style="height: 20px;margin-top: -5px;border-left:solid 1px;border-right:solid 1px;"><span style="color: #FFFFff;">.</span></div>
        </td>
    </tr>
    <tr class="noneBorderTableHand">
        <td colspan="7">
            <div style="height: 20px;margin-top: -5px;border-left:solid 1px;"><span style="color: #FFFFff;">.</span></div>
        </td>
        <td colspan="3" style="">
            <div style="height: 20px;margin-top: -5px;border-right:solid 1px;text-align: center;">..................................................</div>
        </td>
    </tr>
    <tr class="noneBorderTableHand">
        <td colspan="7"><div style="height: 20px;margin-top: -5px;border-left:solid 1px;"><span style="color: #FFFFff;">.</span></div></td>
        <td colspan="3" style="text-align: center;">
            <div style="height: 20px;margin-top: -5px;border-right:solid 1px;text-align: center;">(.....................................................................)</div>
        </td>
    </tr>
    <tr class="noneBorderTableHand">
        <td colspan="10"><div style="height: 20px;margin-top: -5px;border-left:solid 1px;border-bottom: solid 1px;border-right: solid 1px;"><span style="color: #FFFFff;">.</span></div></td>
    </tr>
</table>
</body>
</html>