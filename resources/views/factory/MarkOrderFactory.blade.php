@extends('layouts.layouts')
@section('content')
    <div class="row" style="padding: 10px 0px 10px 10px;">
        <div class="col-lg-8">
            <span style="font-weight: bolder;font-size: 24px;">ผลิตสินค้า(ฝ่ายโรงงาน)</span>
        </div>
        <div class="col-lg-4" style="padding: 10px;">
            <form method="GET" action="/factory/MarkOrderFactory" class="form-inline">
                <div class="form-group">
                    <label for="exampleInputEmail2">ค้นหา</label>
                    <input style="width: 209px;" type="text" class="form-control" name="search" placeholder="ชื่อ Order">
                </div>
                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                <a href="/factory/MarkOrderFactory" class="btn btn-default"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span></a>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th>รายการ</th>
                    <th>สถานะ</th>
                    <th>จัดการ</th>
                </tr>
                </thead>
                <tbody>
                @foreach($dataCustomersOrder['data'] as $key=>$value)
                <tr>
                    <th scope="row">{{$key+1}}</th>
                    <td><a href="/factory/BringManufacture?orderId={{$value->customers_order_id}}">{{$value->customers_order_name}}</a></td>
                    <td>
                        {{\App\Models\CustomersOrder::checkStatus($value->customers_order_status)}}
                        {{--@if($value->customers_order_status=='plan')--}}
                            {{--วางแผนผลิต--}}
                        {{--@elseif($value->customers_order_status=='deal')--}}
                            {{--ลูกค้ายืนยันเเล้ว--}}
                        {{--@elseif($value->customers_order_status=='plan')--}}
                            {{--วางแผน--}}
                        {{--@endif--}}
                    </td>
                    <td>
                        <a onclick="return confirm('ยืนยันการส่งตรวจคุณภาพ ทุกแผน ของ ใบส่งซื้อนี้ ')" href="/factory/checkQualityManufactureAll?orderId={{$value->customers_order_id}}" class="btn btn-info">
                            <span class="glyphicon glyphicon-flag" aria-hidden="true"></span> ส่งตรวจคุณภาพ
                        </a>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12" style="text-align: center;">
            <nav>
                <ul class="pagination" style="margin: 0px;padding: 0px;padding-top: 3px;">
                    @if($dataCustomersOrder['page']>1)
                        <li>
                            <a href="/factory/MarkOrderFactory?page={{$dataCustomersOrder['page']-1}}@if(false==empty($dataCustomersOrder['search']))&search={{$dataCustomersOrder['search']}}@endif">Prev</a>
                        </li>
                    @endif

                    @for($x = 1; $x <= $dataCustomersOrder['max']; $x++)
                        @if($x==$dataCustomersOrder['page'])
                            <li class="active">
                                <a href="/MarkOrder/MarkOrderFactory?page={{$x}}@if(false==empty($dataCustomersOrder['search']))&search={{$dataCustomersOrder['search']}}@endif">{{$x}}</a>
                            </li>
                        @else
                            <li>
                                <a href="/factory/MarkOrderFactory?page={{$x}}@if(false==empty($dataCustomersOrder['search']))&search={{$dataCustomersOrder['search']}}@endif">{{$x}}</a>
                            </li>
                        @endif
                    @endfor

                    @if($dataCustomersOrder['page']<$dataCustomersOrder['max'])
                        <li>
                            <a href="/factory/MarkOrderFactory?page={{$dataCustomersOrder['page']+1}}@if(false==empty($dataCustomersOrder['search']))&search={{$dataCustomersOrder['search']}}@endif">Next</a>
                        </li>
                    @endif
                </ul>
            </nav>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <form class="form-horizontal">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">เพิ่ม-แก้ไข เปิด Order ลูกค้า</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal" style="padding: 0px 20px;">
                            <div class="form-group">
                                <label for="exampleInputEmail1">ชื่อลูกค้า</label>
                                <input type="text" class="form-control" id="exampleInputPassword1" placeholder="ชื่อลูกค้า">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">ชื่อ Order</label>
                                <input type="text" class="form-control" id="exampleInputPassword1" placeholder="ชื่อสินค้าสำเร็จรูป">
                            </div>
                            {{--<div class="form-group">--}}
                                {{--<label for="exampleInputPassword1">จำนวน</label>--}}
                                {{--<textarea class="form-control" id="exampleInputPassword1" placeholder="ราคา"></textarea>--}}
                            {{--</div>--}}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop