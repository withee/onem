@extends('layouts.layouts')
@section('content')
    <div class="row" style="padding-top: 10px;">
        <div class="col-lg-12">
            <ol class="breadcrumb" style="margin: 0px;">
                <li><a href="/factory/MarkOrderManager">ผลิตสินค้า(ฝ่ายผู้จัดการ)</a></li>
                <li class="active">อนุมัติแผนการผลิต</li>
            </ol>
        </div>
    </div>
    <div class="row" style="padding: 10px 0px;">
        <div class="col-lg-3" style="padding: 10px;">
            <span style="font-size: 26px;font-weight: bolder">แผนการผลิต</span>
        </div>
        <div class="col-lg-9">
            <div style="padding: 5px;border: solid 1px #444;border-radius: 5px;background-color: #aaa;">
                <div style="padding: 5px 10px;"><span style="font-weight: bolder;">ชื่องาน: </span><span>{{$dataCustomersOrder->customers_order_name}}</span> <span style="padding-left: 20px;font-weight: bolder;">ชื่อลูกค้า: </span><span>{{$dataCustomersOrder->customers_order_customers}}</span></div>
                <div style="padding: 5px 10px;">
                    <table class="table table-hover">
                        <tr>
                            <th>#</th>
                            <th>ชื่อสินค้าสำเร็จรูป</th>
                            <th style="text-align: center;">ผลิตทั้งหมด</th>
                            <th style="text-align: center;">ผลิตเสร็จ</th>
                            <th style="text-align: center;">ยังไม่ผลิต</th>
                            <th style="text-align: center;">เสียหาย</th>
                            <th style="text-align: center;">%</th>
                        </tr>
                        @foreach($dataOrderProduct as $key=>$value)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$value->customers_order_product_name}}</td>
                                <td style="text-align: center;">{{$value->customers_order_product_count}}</td>
                                <td style="text-align: center;">{{\App\Models\CustomersOrderPlan::getSumComplete($value->customers_order_id,$value->customers_order_product_id)}}</td>
                                <td style="text-align: center;">{{$value->customers_order_product_count-(\App\Models\CustomersOrderPlan::getSumComplete($value->customers_order_id,$value->customers_order_product_id))}}</td>
                                <td style="text-align: center;">{{\App\Models\CustomersOrderPlan::getSumWaste($value->customers_order_id,$value->customers_order_product_id)}}</td>
                                <td>
                                    <div class="progress" style="margin-bottom: 0px;">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="" aria-valuemin="0" aria-valuemax="100" style="width: {{(((\App\Models\CustomersOrderPlan::getSumComplete($value->customers_order_id,$value->customers_order_product_id))/$value->customers_order_product_count)*100)}}%;">
                                            {{(((\App\Models\CustomersOrderPlan::getSumComplete($value->customers_order_id,$value->customers_order_product_id))/$value->customers_order_product_count)*100)}}%
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>

        </div>
    </div>
    <div class="row" style="padding-bottom: 10px;">
        <div class="col-lg-10">
            <form method="GET" action="/factory/ApproveManufacture" class="form-inline">
                {{--<input type="hidden" id="order_product_id" name="customers_order_product_id" value="@if($newCustomersOrderPlanMaterial->order_product_id){{$newCustomersOrderPlanMaterial->order_product_id}}@endif">--}}
                <input type="hidden" id="order_id" name="orderId" value="{{$orderId}}">
                {{--<input type="hidden" id="order_plan_id" name="customers_order_plan_id" value="@if($newCustomersOrderPlanMaterial->customers_order_plan_id){{$newCustomersOrderPlanMaterial->customers_order_plan_id}}@endif">--}}
                <select name="dateSelect" style="width: 200px;" class="form-control">
                    <option value="">เลือกวันที่จะผลิต</option>
                    @foreach($dataDateCustomersOrderPlan as $key=>$value)
                        <option value="{{$value->customers_order_plan_date}}">{{$value->customers_order_plan_date}}</option>
                    @endforeach
                </select>
                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                <a href="/factory/ApproveManufacture?orderId={{$orderId}}" class="btn btn-default"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span></a>
            </form>
        </div>
        <div class="col-lg-2">
            {{--<a style="" class="btn btn-default" data-toggle="modal" data-target="#myModaladd"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> สร้างแผนการผลิต</a>--}}
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th style="text-align: center;">วันที่</th>
                    <th style="text-align: center;">สภานะ</th>
                    <th style="text-align: center;">รายการสินค้าที่จะผลิต</th>
                    <th style="text-align: center;">งานที่จะผลิต</th>
                    <th style="text-align: center;">ส่งผลิต</th>
                </tr>
                </thead>
                <tbody>
                @foreach($dataCustomersOrderPlan as $key=>$value)
                    <tr>
                        <td>{{$value->customers_order_plan_date}}</td>
                        <td style="text-align: center;">
                            @if($value->customers_order_plan_status=='create')
                                <span class="label label-default">รอส่งอนุมัติ</span>
                            @elseif($value->customers_order_plan_status=='confirmation')
                                <span class="label label-warning">รออนุมัติ</span>
                            @elseif($value->customers_order_plan_status=='approve')
                                <span class="label label-success">อนุมัติ</span>
                            @elseif($value->customers_order_plan_status=='complete')
                                <span class="label label-info">ตรวจสอบเเล้ว</span>
                            @elseif($value->customers_order_plan_status=='checkQuality')
                                <span class="label label-danger">รอตรวจสอบเเล้ว</span>
                            @endif
                        </td>
                        <td>{{\App\Models\CustomersOrderProduct::getNameProduct($value->customers_order_product_id)}} {{$value->customers_order_plan_count}} {{\App\Models\ProductUnit::getNameUnitByProductName(\App\Models\CustomersOrderProduct::getNameProduct($value->customers_order_product_id))}}</td>
                        <td>
                            @if(!empty(\App\Models\CustomersOrderPlanMaterial::getByOrderPlanId($value->customers_order_plan_id)))
                                <table class="table table-bordered">
                                    <tr>
                                        <th>วัตถุดิบ</th>
                                        <th>คลัง</th>
                                        <th>จำนวนเบิก</th>
                                        <th>เบิกแล้ว</th>
                                        <th>แก้ไข/ลบ</th>
                                    </tr>
                                    @foreach(\App\Models\CustomersOrderPlanMaterial::getByOrderPlanId($value->customers_order_plan_id) as $keyMaterial=>$valueMaterial)
                                        <tr>
                                            <td>{{\App\Models\CustomersOrderMaterial::getNameMaterialName($valueMaterial->customers_order_material_id)}}</td>
                                            <td>{{\App\Models\Storename::getStoreName(\App\Models\CustomersOrderMaterial::getNameStoreId($valueMaterial->customers_order_material_id))}}</td>
                                            <td>{{$valueMaterial->customers_order_material_count}} {{\App\Models\ProductUnit::getNameUnitByProductName(\App\Models\CustomersOrderMaterial::getNameMaterialName($valueMaterial->customers_order_material_id))}}</td>
                                            <td>0 คิว</td>
                                            <td><a onclick="EditInput({{$value->customers_order_id}},{{$value->customers_order_plan_id}},{{$value->customers_order_product_id}},'{{\App\Models\CustomersOrderMaterial::getNameMaterialName($valueMaterial->customers_order_material_id)}}',{{$valueMaterial->customers_order_material_count}})" data-toggle="modal" data-target="#myModal" class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a><a onclick="return confirm('ยืนยันการลบวัตถุดิบนี้ ')" href="/factory/deletePlanMaterial?orderId={{$orderId}}&materialId={{$valueMaterial->customers_order_plan_material_id}}" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>
                                        </tr>
                                    @endforeach
                                </table>
                            @endif
                        </td>
                        <td>
                            <a onclick="SetInput({{$value->customers_order_id}},{{$value->customers_order_plan_id}},{{$value->customers_order_product_id}})" class="btn btn-primary" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> เพิ่มวัถุดิบ</a>
                            <a href="/factory/sendCheckApprove?orderId={{$orderId}}&planId={{$value->customers_order_plan_id}}" onclick="return confirm('ยืนยันการอนุมัติ')" class="btn btn-success" ><span class="glyphicon glyphicon-check" aria-hidden="true"></span> อนุมัติ</a>
                            <a href="/factory/deletePlanManage?orderId={{$orderId}}&planId={{$value->customers_order_plan_id}}" onclick="return confirm('ยกเลิกแผนการผลิต')" class="btn btn-danger" ><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> ยกเลิก</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {
            $( "#datepicker" ).datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat:'yy-mm-dd'
            });
        });
    </script>

    <style>
        .ui-datepicker-month,.ui-datepicker-year{
            color: #333;
        }
    </style>

    <script>
        function SetInput(order_id,order_plan_id,order_product_id) {
            console.log(order_id);
            $('input#order_id').val(order_id);
            $('#order_plan_id').val(order_plan_id);
            $('#order_product_id').val(order_product_id);
            $.get( "/factory/getOrderMaterial?orderId="+order_product_id, function( data ) {
                $( "#order_material_id" ).html( data );
            });
        }

        function EditInput(order_id,order_plan_id,order_product_id,material_name,quantity) {
            console.log(order_id);
            $('input#order_id').val(order_id);
            $('#order_plan_id').val(order_plan_id);
            $('#order_product_id').val(order_product_id);
            $('#customers_order_material_count').val(quantity);
            var url = "/factory/getOrderMaterial?orderId="+order_product_id+"&material="+material_name;
            $.get( url, function( data ) {
                $( "#order_material_id" ).html( data );
            });
        }
    </script>

    <div class="modal fade @if (true==$errors->has('customers_order_material_id')||true==$errors->has('customers_order_material_count')) in @endif" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="@if (true==$errors->has('customers_order_material_id')||true==$errors->has('customers_order_material_count')) display: block; @endif">
        <div class="modal-dialog" role="document">
            <form method="POST" action="/factory/addCustomersOrderPlanMaterialManage" novalidate>
                <div class="modal-content">
                    <div class="modal-header">
                        <a href="/factory/PlanManufacture?orderId={{$orderId}}" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                        <h4 class="modal-title" id="myModalLabel">เพิ่มวัตถุดิบที่จะเบิก</h4>
                    </div>
                    <div class="modal-body">
                        @if (true==$errors->has('customers_order_material_id')||true==$errors->has('customers_order_material_count'))
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </div>
                        @endif
                        <input type="hidden" id="order_product_id" name="customers_order_product_id" value="@if($newCustomersOrderPlanMaterial->order_product_id){{$newCustomersOrderPlanMaterial->order_product_id}}@endif">
                        <input type="hidden" id="order_id" name="customers_order_id" value="@if($newCustomersOrderPlanMaterial->customers_order_id){{$newCustomersOrderPlanMaterial->customers_order_id}}@endif">
                        <input type="hidden" id="order_plan_id" name="customers_order_plan_id" value="@if($newCustomersOrderPlanMaterial->customers_order_plan_id){{$newCustomersOrderPlanMaterial->customers_order_plan_id}}@endif">
                        <div class="form-group  @if ($errors->has('customers_order_material_id')) has-error @endif">
                            <label for="exampleInputEmail1">ชื่อวัถุดิบ</label>
                            <select name="customers_order_material_id" id="order_material_id" class="form-control">
                                @if(!empty($dataCustomersOrderMaterial))
                                    @foreach($dataCustomersOrderMaterial as $key=>$value)
                                        <option value="{{$value->customers_order_material_id}}">{{$value->customers_order_material_name}}:{{$value->storename_name}}</option>
                                    @endforeach
                                @else
                                    <option>เลือก</option>
                                @endif
                            </select>
                        </div>
                        <div class="form-group @if ($errors->has('customers_order_material_count')) has-error @endif">
                            <label for="exampleInputPassword1">จำนวน</label>
                            <input name="customers_order_material_count" id="customers_order_material_count" type="text" class="form-control" id="exampleInputPassword1" placeholder="จำนวน">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a hidden="/factory/PlanManufacture?orderId={{$orderId}}" type="button" class="btn btn-default" data-dismiss="modal">Close</a>
                        <button type="submit" class="btn btn-primary"> เพิ่ม</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop