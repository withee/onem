@extends('layouts.layouts')
@section('content')
    <div class="row" style="padding: 10px 0px 10px 10px;">
        <div class="col-lg-11">
            <span style="font-weight: bolder;font-size: 24px;">จัดการข้อมูลลูกค้า</span>
        </div>
        <div class="col-lg-1"></div>
    </div>
    <div class="row">
        <div class="col-lg-8"></div>
        <div class="col-lg-3" style="padding: 0px;">
            <form method="POST" action="/factory/searchCustomers" class="form-inline">
                <div class="form-group">
                    <label for="exampleInputEmail2">ค้นหา</label>
                    <input style="width: 150px;" type="text" class="form-control" name="search" placeholder="ชื่อ">
                </div>
                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                <a href="/factory/manageCustomers" class="btn btn-default"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span></a>
            </form>
        </div>
        <div class="col-lg-1" style="padding: 0px;">
            <a style="" class="btn btn-default" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> เพิ่ม</a>
        </div>
    </div>
    <div class="row" style="padding: 10px 0px 10px 10px;">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th>ชื่อ-นามสกุล</th>
                    <th>เบอร์โทร</th>
                    <th>ที่อยู่</th>
                    <th>จัดการ</th>
                </tr>
                </thead>
                <tbody>
                @if(!empty($dataCustomers['data']))
                    @foreach($dataCustomers['data'] as $key=>$value)
                    <tr>
                        <th scope="row">{{$key+1}}</th>
                        <td>{{$value->customers_name}}</td>
                        <td>{{$value->customers_telephone}}</td>
                        <td>{{$value->customers_address}}</td>
                        <td>
                            <a href="/factory/manageCustomers?id={{$value->customers_id}}" class="btn btn-primary"><span class="glyphicon glyphicon-flag" aria-hidden="true"></span> แก้ไข</a>
                            <a href="/factory/deleteCustomers?id={{$value->customers_id}}" onclick="return confirm('ยืนยันการลบลูกค้า {{$value->customers_name}}  ')" class="btn btn-danger"><span class="glyphicon glyphicon-flag" aria-hidden="true"></span> ลบ</a>
                        </td>
                    </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5" style="text-align: center;">ไม่มีข้อมูล ลูกค้า</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12" style="text-align: center;">
            <nav>
                <ul class="pagination" style="margin: 0px;padding: 0px;padding-top: 3px;">
                    @if($dataCustomers['page']>1)
                        <li>
                            <a href="/factory/manageCustomers?page={{$dataCustomers['page']-1}}@if(false==empty($dataCustomers['search']))&search={{$dataCustomers['search']}}@endif">Prev</a>
                        </li>
                    @endif

                    @for($x = 1; $x <= $dataCustomers['max']; $x++)
                        @if($x==$dataCustomers['page'])
                            <li class="active"><a
                                        href="/factory/manageCustomers?page={{$x}}@if(false==empty($dataCustomers['search']))&search={{$dataCustomers['search']}}@endif">{{$x}}</a>
                            </li>
                        @else
                            <li>
                                <a href="/factory/manageCustomers?page={{$x}}@if(false==empty($dataCustomers['search']))&search={{$dataCustomers['search']}}@endif">{{$x}}</a>
                            </li>
                        @endif
                    @endfor

                    @if($dataCustomers['page']<$dataCustomers['max'])
                        <li>
                            <a href="/factory/manageCustomers?page={{$dataCustomers['page']+1}}@if(false==empty($dataCustomers['search']))&search={{$dataCustomers['search']}}@endif">Next</a>
                        </li>
                    @endif
                </ul>
            </nav>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade @if (!empty($customers->customers_id) || true==$errors->has('customers_name')||true==$errors->has('customers_telephone')||true==$errors->has('customers_address')) in @endif" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="@if (!empty($customers->customers_id)||true==$errors->has('customers_name')||true==$errors->has('customers_telephone')||true==$errors->has('customers_address')) display: block; @endif">
        <div class="modal-dialog" role="document">
            <form method="POST" action="/factory/addCustomers" class="form-horizontal" novalidate>
            <div class="modal-content">
                <div class="modal-header">
                    <a href="/factory/manageCustomers" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title" id="myModalLabel">เพิ่ม-แก้ไข ข้อมูลลูกค้า</h4>
                </div>
                <div class="modal-body">

                    @if (true==$errors->has('customers_name')||true==$errors->has('customers_telephone')||true==$errors->has('customers_address'))
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif

                    <div class="form-horizontal" style="padding: 0px 20px;">
                        <input type="hidden" name="customers_id" value="{{$customers->customers_id}}">
                        <div class="form-group @if ($errors->has('customers_name')) has-error @endif">
                            <label for="exampleInputEmail1">ชื่อ-นามสกุลหรือชื่อบริษัท <span style="color: #FF0000;">*</span></label>
                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="ชื่อ-นามสกุลหรือชื่อบริษัท" name="customers_name"
                                   value="@if(!empty($customers->customers_name)){{$customers->customers_name}}@elseif(!empty(Input::old('customers_name'))){{Input::old('customers_name')}}@endif" >
                        </div>
                        <div class="form-group @if ($errors->has('customers_telephone')) has-error @endif">
                            <label for="exampleInputPassword1">เบอร์โทร</label>
                            <input type="text" class="form-control" id="exampleInputPassword1" placeholder="เบอร์โทร" name="customers_telephone"
                                   value="@if(!empty($customers->customers_telephone)){{$customers->customers_telephone}}@elseif(!empty(Input::old('customers_telephone'))){{Input::old('customers_telephone')}}@endif" >
                        </div>
                        <div class="form-group @if ($errors->has('customers_address')) has-error @endif">
                            <label for="exampleInputPassword1">ที่อยู่ <span style="color: #FF0000;">*</span></label>
                            <input class="form-control" id="exampleInputPassword1" placeholder="ที่อยู่" name="customers_address" value="@if(!empty($customers->customers_address)){{$customers->customers_address}}@elseif(!empty(Input::old('customers_address'))){{Input::old('customers_address')}}@endif">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="/factory/manageCustomers" type="button" class="btn btn-default" data-dismiss="modal">Close</a>
                    @if (!empty($customers->customers_id))
                        <input type="submit" class="btn btn-primary" value="Save changes"/>
                    @else
                        <input type="submit" class="btn btn-primary" value="Save"/>
                    @endif
                </div>
</div>
</form>
</div>
</div>
@stop