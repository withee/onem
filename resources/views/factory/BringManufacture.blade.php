@extends('layouts.layouts')
@section('content')
    <div class="row" style="padding-top: 10px;">
        <div class="col-lg-12">
            <ol class="breadcrumb" style="margin: 0px;">
                <li><a href="/factory/MarkOrderFactory">ผลิตสินค้า(โรงงาน)</a></li>
                <li class="active">เบิกสินค้าเพื่อผลิต</li>
            </ol>
        </div>
    </div>
    <div class="row" style="padding: 10px 0px;">
        <div class="col-lg-3" style="padding: 10px;">
            <span style="font-size: 26px;font-weight: bolder">เบิกสินค้าเพื่อผลิต</span>
        </div>
        <div class="col-lg-9" style="display: none;">
            <div style="padding: 5px;border: solid 1px #444;border-radius: 5px;background-color: #aaa;">
                <div style="padding: 5px 10px;"><span style="font-weight: bolder;">ชื่องาน: </span><span>{{$dataCustomersOrder->customers_order_name}}</span> <span style="padding-left: 20px;font-weight: bolder;">ชื่อลูกค้า: </span><span>{{$dataCustomersOrder->customers_order_customers}}</span></div>
                <div style="padding: 5px 10px;">
                    <table class="table table-hover">
                        <tr>
                            <th>#</th>
                            <th>ชื่อสินค้าสำเร็จรูป</th>
                            <th style="text-align: center;">ผลิตทั้งหมด</th>
                            <th style="text-align: center;">ผลิตเสร็จ</th>
                            <th style="text-align: center;">ยังไม่ผลิต</th>
                            <th style="text-align: center;">เสียหาย</th>
                            <th>กราฟ</th>
                        </tr>
                        @foreach($dataOrderProduct as $key=>$value)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$value->customers_order_product_name}}</td>
                                <td style="text-align: center;">{{$value->customers_order_product_count}}</td>
                                <td style="text-align: center;">{{\App\Models\CustomersOrderPlan::getSumComplete($value->customers_order_id,$value->customers_order_product_id)}}</td>
                                <td style="text-align: center;">{{$value->customers_order_product_count-(\App\Models\CustomersOrderPlan::getSumComplete($value->customers_order_id,$value->customers_order_product_id))}}</td>
                                <td style="text-align: center;">{{\App\Models\CustomersOrderPlan::getSumWaste($value->customers_order_id,$value->customers_order_product_id)}}</td>
                                <td>
                                    <div class="progress" style="margin-bottom: 0px;">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="" aria-valuemin="0" aria-valuemax="100" style="width: {{(((\App\Models\CustomersOrderPlan::getSumComplete($value->customers_order_id,$value->customers_order_product_id))/$value->customers_order_product_count)*100)}}%;">
                                            {{(((\App\Models\CustomersOrderPlan::getSumComplete($value->customers_order_id,$value->customers_order_product_id))/$value->customers_order_product_count)*100)}}%
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="padding-bottom: 10px;">
        <div class="col-lg-10">
            <form method="GET" action="/factory/BringManufacture" class="form-inline">
                {{--<input type="hidden" id="order_product_id" name="customers_order_product_id" value="@if($newCustomersOrderPlanMaterial->order_product_id){{$newCustomersOrderPlanMaterial->order_product_id}}@endif">--}}
                <input type="hidden" id="order_id" name="orderId" value="{{$orderId}}">
                {{--<input type="hidden" id="order_plan_id" name="customers_order_plan_id" value="@if($newCustomersOrderPlanMaterial->customers_order_plan_id){{$newCustomersOrderPlanMaterial->customers_order_plan_id}}@endif">--}}
                <select name="dateSelect" style="width: 200px;" class="form-control">
                    <option value="">เลือกวันที่จะผลิต</option>
                    @foreach($dataDateCustomersOrderPlan as $key=>$value)
                        <option value="{{$value->customers_order_plan_date}}">{{$value->customers_order_plan_date}}</option>
                    @endforeach
                </select>
                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                <a href="/factory/BringManufacture?orderId={{$orderId}}" class="btn btn-default"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span></a>
            </form>
        </div>
        <div class="col-lg-2">
            {{--<a style="" class="btn btn-default" data-toggle="modal" data-target="#myModaladd"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> สร้างแผนการผลิต</a>--}}
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th style="text-align: center;">วันที่</th>
                    <th style="text-align: center;">สภานะ</th>
                    <th style="text-align: center;">รายการสินค้าที่จะผลิต</th>
                    <th style="text-align: center;">งานที่จะผลิต</th>
                    <th style="text-align: center;">ส่งผลิต</th>
                </tr>
                </thead>
                <tbody>
                @foreach($dataCustomersOrderPlan as $key=>$value)
                    <tr>
                        <td>{{$value->customers_order_plan_date}}</td>
                        <td style="text-align: center;">
                            @if($value->customers_order_plan_status=='create')
                                <span class="label label-default">รอส่งอนุมัติ</span>
                            @elseif($value->customers_order_plan_status=='confirmation')
                                <span class="label label-info">รออนุมัติ</span>
                            @elseif($value->customers_order_plan_status='approve')
                                <span class="label label-success">อนุมัติ</span>
                            @endif
                        </td>
                        <td>{{\App\Models\CustomersOrderProduct::getNameProduct($value->customers_order_product_id)}} {{$value->customers_order_plan_count}} {{\App\Models\ProductUnit::getNameUnitByProductName(\App\Models\CustomersOrderProduct::getNameProduct($value->customers_order_product_id))}}</td>
                        <td>
                            @if(!empty(\App\Models\CustomersOrderPlanMaterial::getByOrderPlanId($value->customers_order_plan_id)))
                                <table class="table table-bordered">
                                    <tr>
                                        <th>วัตถุดิบ</th>
                                        <th>คลัง</th>
                                        <th>ยอดคงคลัง</th>
                                        <th>จำนวนเบิก</th>
                                    </tr>
                                    @foreach(\App\Models\CustomersOrderPlanMaterial::getByOrderPlanId($value->customers_order_plan_id) as $keyMaterial=>$valueMaterial)
                                        <tr>
                                            <td>{{\App\Models\CustomersOrderMaterial::getNameMaterialName($valueMaterial->customers_order_material_id)}}</td>
                                            <td>{{\App\Models\Storename::getStoreName(\App\Models\CustomersOrderMaterial::getNameStoreId($valueMaterial->customers_order_material_id))}}</td>
                                            <td>{{\App\Models\Stores::CheckStoreCount(\App\Models\Product::GetIdProduct(\App\Models\CustomersOrderMaterial::getNameMaterialName($valueMaterial->customers_order_material_id)),\App\Models\CustomersOrderMaterial::getNameStoreId($valueMaterial->customers_order_material_id))}} {{\App\Models\ProductUnit::getNameUnitByProductName(\App\Models\CustomersOrderMaterial::getNameMaterialName($valueMaterial->customers_order_material_id))}}</td>
                                            <td>{{$valueMaterial->customers_order_material_count}} {{\App\Models\ProductUnit::getNameUnitByProductName(\App\Models\CustomersOrderMaterial::getNameMaterialName($valueMaterial->customers_order_material_id))}}</td>
                                        </tr>
                                    @endforeach
                                </table>
                            @endif
                        </td>
                        <td>
                            <a target="_blank" href="/factory/PrintBringManufacture?orderId={{$value->customers_order_plan_id}}" class="btn btn-primary"><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> เบิกวัตถุดิบ</a>
                            <a href="/factory/checkQualityManufacture?orderId={{$value->customers_order_plan_id}}" onclick="return confirm('ยืนยันการส่ง แผนนี้เพื่อ ตรวจคุณภาพ')" class="btn btn-info"><span class="glyphicon glyphicon-check" aria-hidden="true"></span> ส่งตรวจคุณภาพ</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@stop