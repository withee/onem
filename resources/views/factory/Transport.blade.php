@extends('layouts.layouts')
@section('content')
    <div class="row" style="padding: 10px 0px 10px 10px;">
        <div class="col-lg-8">
            <span style="font-weight: bolder;font-size: 24px;">ส่งสินค้า</span>
        </div>
        <div class="col-lg-4" style="padding: 10px;">
            <form method="GET" action="/factory/Transport" class="form-inline">
                <div class="form-group">
                    <label for="exampleInputEmail2">ค้นหา</label>
                    <input style="width: 209px;" type="text" class="form-control" name="search" placeholder="ชื่อ Order">
                </div>
                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                <a href="/factory/Transport" class="btn btn-default"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span></a>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th>รายการ</th>
                    <th>สถานะ</th>
                    <th>จัดการ</th>
                </tr>
                </thead>
                <tbody>
                @foreach($dataCustomersOrder['data'] as $key=>$value)
                    <tr>
                        <th scope="row">{{$key+1}}</th>
                        <td><a href="/factory/ReportTransport?orderId={{$value->customers_order_id}}">{{$value->customers_order_name}}</a></td>
                        <td>
                            {{\App\Models\CustomersOrder::checkStatus($value->customers_order_status)}}
                            {{--@if($value->customers_order_status=='plan')--}}
                            {{--วางแผนผลิต--}}
                            {{--@elseif($value->customers_order_status=='deal')--}}
                            {{--ลูกค้ายืนยันเเล้ว--}}
                            {{--@elseif($value->customers_order_status=='plan')--}}
                            {{--วางแผน--}}
                            {{--@endif--}}
                        </td>
                        <td>
                            <a href="/factory/ReportTransport?orderId={{$value->customers_order_id}}" class="btn btn-info">
                                <span class="glyphicon glyphicon-road" aria-hidden="true"></span> รายงานขนส่งสินค้า
                            </a>
                            {{--<a href="" onclick="return confirm('ยืนยันการปิดงาน')" class="btn btn-success"><span class="glyphicon glyphicon-check" aria-hidden="true"></span> อนุมัติปิดงานนี้</a>--}}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12" style="text-align: center;">
            <nav>
                <ul class="pagination" style="margin: 0px;padding: 0px;padding-top: 3px;">
                    @if($dataCustomersOrder['page']>1)
                        <li>
                            <a href="/factory/Transport?page={{$dataCustomersOrder['page']-1}}@if(false==empty($dataCustomersOrder['search']))&search={{$dataCustomersOrder['search']}}@endif">Prev</a>
                        </li>
                    @endif

                    @for($x = 1; $x <= $dataCustomersOrder['max']; $x++)
                        @if($x==$dataCustomersOrder['page'])
                            <li class="active">
                                <a href="/MarkOrder/Transport?page={{$x}}@if(false==empty($dataCustomersOrder['search']))&search={{$dataCustomersOrder['search']}}@endif">{{$x}}</a>
                            </li>
                        @else
                            <li>
                                <a href="/factory/Transport?page={{$x}}@if(false==empty($dataCustomersOrder['search']))&search={{$dataCustomersOrder['search']}}@endif">{{$x}}</a>
                            </li>
                        @endif
                    @endfor

                    @if($dataCustomersOrder['page']<$dataCustomersOrder['max'])
                        <li>
                            <a href="/factory/Transport?page={{$dataCustomersOrder['page']+1}}@if(false==empty($dataCustomersOrder['search']))&search={{$dataCustomersOrder['search']}}@endif">Next</a>
                        </li>
                    @endif
                </ul>
            </nav>
        </div>
    </div>
@stop