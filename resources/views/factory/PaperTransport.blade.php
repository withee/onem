<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('page-title', 'PDF Document')</title>

</head>
<body style="font-family: angsana-new;">
<style >
    /*@font-face {*/
    /*font-family: "DejaVuSans";*/
    /*src:url('/font/DejaVuSans.ttf') format("truetype");*/
    /*}*/
    /*h1 { font-family: "Kimberley", sans-serif }*/
    table{
        width:100%;
        border: solid 1px;
    }
    tr{
        width: 100%;
        border: solid 1px;
    }
    td.main{
        border: solid 1px #FFFFff;
        width: 10%;
    }
    td.noneBorder{
        border: solid 1px #ffffff;
        width: 10%;
    {{--font-family: DejaVuSans;--}}
        font-size: 16px;
    }
    td.noneBorderTableHand{
        /*border: solid 1px #ffffff;*/
        width: 10%;
    {{--font-family: DejaVuSans;--}}
        font-size: 13px;
        text-align: center;
        line-height: 0.9;
        /*height: 20px;*/
    }
    td.noneBorderTable{
        /*border: solid 1px #ffffff;*/
        width: 10%;
    {{--font-family: DejaVuSans;--}}
        font-size: 11px;
    }

    /*tr.table>td:first-child{*/
    /*border-bottom: solid 1px;border-top: solid 1px;border-left: solid 1px;*/
    /*}*/
</style>
<table style="">
    <tr>
        <td class="main"></td>
        <td class="main"></td>
        <td class="main"></td>
        <td class="main"></td>
        <td class="main"></td>
        <td class="main"></td>
        <td class="main"></td>
        <td class="main"></td>
        <td class="main"></td>
        <td class="main"></td>
    </tr>
    <tr>
        <td class="noneBorder"><img style="width:50px" src="./image/onem.jpg"/></td>
        <td class="noneBorder" colspan="7" style="text-align: center;">
            <span style="font-size:18px;">ใบส่งสินค้า </span><br>
            <div style="font-size:14px;">ออกใบส่งสินค้า วันที่.........{{Carbon\Carbon::parse($customersOrderTransport->created_at)->format('d')}}........... เดือน.....{{$thai_month_arr[Carbon\Carbon::parse($customersOrderTransport->created_at)->format('n')]}}..... พ.ศ. ..{{Carbon\Carbon::parse($customersOrderTransport->created_at)->format('Y')+543}}..</div>
            <span style="font-size:14px;">ส่งเสร็จ วันที่........................... เดือน..................................... พ.ศ. ..............</span>

        </td>
        <td colspan="2" class="noneBorder" style="padding-bottom: 30px;">
            <span style="font-size:16px;">เลขที่  {{$customersOrderTransport->customers_order_transport_code}}</span><br>
            {{--<span style="font-size:10px;color: #FFFFff;">ว</span>--}}
        </td>
    </tr>
    <tr>
        <td class="noneBorder" colspan="5">ชื่อผู้เบิก.............................................................................</td>
        <td class="noneBorder" colspan="5">ตำแหน่ง.................................................</td>
    </tr>
    <tr>
        <td colspan="10"></td>
    </tr>
    <tr class="table">
        <td  class="noneBorderTableHand"><div style="height: 21px;border:solid 1px;border-bottom: solid 0px;">ลำดับ</div></td>
        <td  class="noneBorderTableHand" colspan="6"><div style="height: 21px;margin-left: -5px;border: solid 1px;border-bottom: solid 0px;">รายงาน</div></td>
        <td  class="noneBorderTableHand" ><div style="height: 21px;margin-left: -5px;border: solid 1px;border-bottom: solid 0px;">ทะเบียนรถ</div></td>
        <td  class="noneBorderTableHand" colspan="2"><div style="height: 20px;margin-left: -5px;border:solid 1px;">จำนวนเบิก</div></td>
        <td  class="noneBorderTableHand"><div style="height: 21px;margin-left: -5px;border: solid 1px;border-bottom: solid 0px;">หมายเหตุ</div></td>
    </tr>
    <tr>
        <td class="noneBorderTableHand" ><div style="height: 20px;border:solid 1px;border-top: solid 0px;margin-top: -5px;"></div></td>
        <td class="noneBorderTableHand" colspan="6"><div style="height: 20px;margin-left: -5px;border:solid 1px;margin-top: -5px;border-top: solid 0px;"></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px;border-top: solid 0px;"></div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px">จำนวน</div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px">หน่วยนับ</div></td>
        <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px;border-top: solid 0px;"></div></td>
    </tr>
    @foreach($customersOrderTransportProduct as $key=>$value)
        <tr>
            <td class="noneBorderTableHand" ><div style="height: 20px;border:solid 1px;margin-top: -5px;"><span style="">{{$key+1}}</span></div></td>
            <td class="noneBorderTableHand" colspan="6"><div style="height: 20px;margin-left: -5px;border:solid 1px;margin-top: -5px;text-align: left;"><span style="">{{\App\Models\CustomersOrderProduct::getNameProduct($value->customers_order_product_id)}}</span></div></td>
            <td class="noneBorderTableHand"><div style="height: 20px;;margin-left: -5px;border:solid 1px;margin-top: -5px;"><span style="">{{$value->vehicles_name}}</span></div></td>
            <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="">{{$value->customers_order_product_count}}</span></div></td>
            <td class="noneBorderTableHand"><div style="height: 20px;margin-left: -5px;border: solid 1px;margin-top: -5px"><span style="">{{\App\Models\ProductUnit::getNameUnitByProductName(\App\Models\CustomersOrderProduct::getNameProduct($value->customers_order_product_id))}}</span></div></td>
            <td class="noneBorderTableHand"><div style="height: 20px;;margin-left: -5px;border: solid 1px;margin-top: -5px;"><span style="color: #FFFFFF;">1</span></div></td>
        </tr>
    @endforeach
    <tr>
        <td colspan="10"></td>
    </tr>
    <tr>
        <td class="noneBorderTableHand" colspan="2">
            <div style="border: solid 1px; margin-top: -5px">
                <div>ผู้ขอเบิก</div>
                <div style="margin-top: 10px;">(.........................................)</div>
                <div style="margin-top: 10px;">วันที่................................</div>
            </div>
        </td>
        <td class="noneBorderTableHand" colspan="2">
            <div style="border: solid 1px; margin-top: -5px">
                <div>ผู้รับสินค้า</div>
                <div style="margin-top: 10px;">(.........................................)</div>
                <div style="margin-top: 10px;">วันที่................................</div>
            </div>
        </td>
        <td class="noneBorderTableHand" colspan="2">
            <div style="border: solid 1px; margin-top: -5px">
                <div>คนขับรถ</div>
                <div style="margin-top: 10px;">(.........................................)</div>
                <div style="margin-top: 10px;">วันที่................................</div>
            </div>
        </td>
        <td class="noneBorderTableHand" colspan="2">
            <div style="border: solid 1px; margin-top: -5px">
                <div>เจ้าหน้าที่คลัง</div>
                <div style="margin-top: 10px;">(.........................................)</div>
                <div style="margin-top: 10px;">วันที่................................</div>
            </div>
        </td>
        <td class="noneBorderTableHand" colspan="3">
            <div style="border: solid 1px; margin-top: -5px">
                <div>ผู้จัดการ</div>
                <div style="margin-top: 10px;">(.........................................)</div>
                <div style="margin-top: 10px;">วันที่................................</div>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="10"></td>
    </tr>
</table>
</body>
</html>