@extends('layouts.layouts')
@section('content')

    <div class="row" style="padding-top: 10px;">
        <div class="col-lg-12">
            <ol class="breadcrumb" style="margin: 0px;">
                <li><a href="/factory/Transport">ส่งสินค้า</a></li>
                <li class="active">รายงานขนส่งสินค้า</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12" style="padding: 10px;">
            <span style="font-size: 26px;font-weight: bolder">รายงานขนส่งสินค้า</span>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-12">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">ทั้งหมด</a></li>
                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">ส่งสำเร็จ</a></li>
                <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">เสียหาย</a></li>
                <li role="presentation"><a href="#return" aria-controls="return" role="tab" data-toggle="tab">ส่งคืน</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="home">
                    <form method="POST" action="/factory/createTransport" target="_blank">
                        <input type="hidden" name="customers_order_id" value="{{$orderId}}">
                    <div class="row">
                        <div class="col-lg-10" style="padding: 10px;"></div>
                        <div class="col-lg-2" style="padding: 10px;">
                            <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> ออกใบส่งของ</button>
                        </div>
                    </div>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>รายการสินค้าที่จะผลิต</th>
                            <th style="text-align: right;">จำนวนทั้งหมด</th>
                            <th style="text-align: right;">ผลิตเสร็จ</th>
                            {{--<th style="text-align: right;">ผลิตเสียหาย</th>--}}
                            <th style="text-align: right;">ส่งสำเร็จ</th>
                            <th style="text-align: right;">ส่งเสียหาย</th>
                            <th style="text-align: right;">เหลือ</th>
                            <th>จำนวนส่ง</th>
                            <th>เลือก</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($customersOrderProduct as $key=>$value)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$value->customers_order_product_name}}</td>
                                <td style="text-align: right;">{{$value->customers_order_product_count}} {{\App\Models\ProductUnit::getNameUnitByProductName($value->customers_order_product_name)}}</td>
                                <td style="text-align: right;">{{\App\Models\CustomersOrderPlan::getSumComplete($orderId,$value->customers_order_product_id)}} {{\App\Models\ProductUnit::getNameUnitByProductName($value->customers_order_product_name)}}</td>
                                <td style="text-align: right;">{{\App\Models\CustomersOrderTransportProduct::getSumComplete($orderId,$value->customers_order_product_id)}} {{\App\Models\ProductUnit::getNameUnitByProductName($value->customers_order_product_name)}}</td>
                                <td style="text-align: right;">{{\App\Models\CustomersOrderTransportProduct::getSumWaste($orderId,$value->customers_order_product_id)}} {{\App\Models\ProductUnit::getNameUnitByProductName($value->customers_order_product_name)}}</td>
                                <td style="text-align: right;">{{$value->customers_order_product_count-(\App\Models\CustomersOrderTransportProduct::getSumComplete($orderId,$value->customers_order_product_id))}} {{\App\Models\ProductUnit::getNameUnitByProductName($value->customers_order_product_name)}}</td>
                                <td>
                                    <div class="form-inline">
                                        <select name="transport[{{$value->customers_order_product_id}}][vehicles_id]" class="form-control">
                                            {{--<option value="">เลือก</option>--}}
                                            @foreach($vehicles as $keyV=>$valueV)
                                                <option value="{{$valueV->id}}">{{$valueV->brands}} : {{$valueV->license_plate}}</option>
                                            @endforeach
                                        </select>
                                        <input style="width: 70px;" class="form-control input-sm" name="transport[{{$value->customers_order_product_id}}][count]" type="number" value="{{$value->customers_order_product_count-(\App\Models\CustomersOrderTransportProduct::getSumComplete($orderId,$value->customers_order_product_id))}}" placeholder="จำนวนส่ง">
                                    </div>
                                </td>
                                <td>
                                    <input type="checkbox" name="transport[{{$value->customers_order_product_id}}][status]">
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </form>
                </div>
                <div role="tabpanel" class="tab-pane" id="profile">
                    <form method="POST" action="/factory/addCompleteTransport">
                        <input type="hidden" name="customers_order_id" value="{{$orderId}}">
                    <div class="row">
                        <div class="col-lg-10" style="padding: 10px;"></div>
                        <div class="col-lg-2" style="padding: 10px;">
                            <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-floppy-saved" aria-hidden="true"></span> บันทึกส่งสำเร็จ</button>
                        </div>
                    </div>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>รายการสินค้าที่จะผลิต</th>
                            <th style="text-align: right;">จำนวนทั้งหมด</th>
                            <th style="text-align: right;">ผลิตเสร็จ</th>
                            <th style="text-align: right;">ส่งเเล้ว</th>
                            <th style="text-align: right;">เสียหาย</th>
                            <th style="text-align: right;">เหลือ</th>
                            <th>จำนวนส่งสำเร็จ</th>
                            <th>เลือก</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($customersOrderProduct as $key=>$value)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$value->customers_order_product_name}}</td>
                                <td style="text-align: right;">{{$value->customers_order_product_count}} {{\App\Models\ProductUnit::getNameUnitByProductName($value->customers_order_product_name)}}</td>
                                <td style="text-align: right;">{{\App\Models\CustomersOrderPlan::getSumComplete($orderId,$value->customers_order_product_id)}} {{\App\Models\ProductUnit::getNameUnitByProductName($value->customers_order_product_name)}}</td>
                                <td style="text-align: right;">{{\App\Models\CustomersOrderTransportProduct::getSumComplete($orderId,$value->customers_order_product_id)}} {{\App\Models\ProductUnit::getNameUnitByProductName($value->customers_order_product_name)}}</td>
                                <td style="text-align: right;">{{\App\Models\CustomersOrderTransportProduct::getSumWaste($orderId,$value->customers_order_product_id)}} {{\App\Models\ProductUnit::getNameUnitByProductName($value->customers_order_product_name)}}</td>
                                <td style="text-align: right;">{{$value->customers_order_product_count-(\App\Models\CustomersOrderTransportProduct::getSumComplete($orderId,$value->customers_order_product_id))}} {{\App\Models\ProductUnit::getNameUnitByProductName($value->customers_order_product_name)}}</td>
                                <td>
                                    <div class="form-inline">
                                        <select name="complete[{{$key}}][customers_order_transport_product_id]" class="form-control">
                                            <option value="">เลือก</option>
                                            @if(!empty(\App\Models\CustomersOrderTransportProduct::getTransportProductCode($orderId,$value->customers_order_product_id)))
                                                @foreach(\App\Models\CustomersOrderTransportProduct::getTransportProductCode($orderId,$value->customers_order_product_id) as $keySelect=>$valueSelect)
                                                    <option value="{{$valueSelect->customers_order_transport_product_id}}">{{$valueSelect->customers_order_transport_code}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <input name="complete[{{$key}}][count]" style="" class="form-control input-sm" type="number" value="{{$value->customers_order_product_count-(\App\Models\CustomersOrderTransportProduct::getSumComplete($orderId,$value->customers_order_product_id))}}" placeholder="จำนวนส่งสำเร็จ">
                                    </div>
                                </td>
                                <td><input type="checkbox" name="complete[{{$key}}][status]"></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </form>
                </div>
                <div role="tabpanel" class="tab-pane" id="messages">
                    <form method="POST" action="/factory/addWasteTransport">
                        <input type="hidden" name="customers_order_id" value="{{$orderId}}">
                    <div class="row">
                        <div class="col-lg-10" style="padding: 10px;"></div>
                        <div class="col-lg-2" style="padding: 10px;">
                            <button class="btn btn-default"><span class="glyphicon glyphicon-floppy-saved" aria-hidden="true"></span> บันทึกส่งเสียหาย</button>
                        </div>
                    </div>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>รายการสินค้าที่จะผลิต</th>
                            <th style="text-align: right;">จำนวนทั้งหมด</th>
                            <th style="text-align: right;">ผลิตเสร็จ</th>
                            <th style="text-align: right;">ส่งเเล้ว</th>
                            <th style="text-align: right;">เสียหาย</th>
                            <th style="text-align: right;">เหลือ</th>
                            <th>จำนวนส่งเสียหาย</th>
                            <th>เลือก</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($customersOrderProduct as $key=>$value)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$value->customers_order_product_name}}</td>
                                <td style="text-align: right;">{{$value->customers_order_product_count}} {{\App\Models\ProductUnit::getNameUnitByProductName($value->customers_order_product_name)}}</td>
                                <td style="text-align: right;">{{\App\Models\CustomersOrderPlan::getSumComplete($orderId,$value->customers_order_product_id)}} {{\App\Models\ProductUnit::getNameUnitByProductName($value->customers_order_product_name)}}</td>
                                <td style="text-align: right;">{{\App\Models\CustomersOrderTransportProduct::getSumComplete($orderId,$value->customers_order_product_id)}} {{\App\Models\ProductUnit::getNameUnitByProductName($value->customers_order_product_name)}}</td>
                                <td style="text-align: right;">{{\App\Models\CustomersOrderTransportProduct::getSumWaste($orderId,$value->customers_order_product_id)}} {{\App\Models\ProductUnit::getNameUnitByProductName($value->customers_order_product_name)}}</td>
                                <td style="text-align: right;">{{$value->customers_order_product_count-(\App\Models\CustomersOrderTransportProduct::getSumComplete($orderId,$value->customers_order_product_id))}} {{\App\Models\ProductUnit::getNameUnitByProductName($value->customers_order_product_name)}}</td>
                                <td>
                                    <div class="form-inline">
                                        <select name="complete[{{$key}}][customers_order_transport_product_id]" class="form-control">
                                            <option value="">เลือก</option>
                                            @if(!empty(\App\Models\CustomersOrderTransportProduct::getTransportProductCode($orderId,$value->customers_order_product_id)))
                                                @foreach(\App\Models\CustomersOrderTransportProduct::getTransportProductCode($orderId,$value->customers_order_product_id) as $keySelect=>$valueSelect)
                                                    <option value="{{$valueSelect->customers_order_transport_product_id}}">{{$valueSelect->customers_order_transport_code}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    <input name="complete[{{$key}}][count]" class="form-control input-sm" type="text" value="0" placeholder="จำนวนส่งเสียหาย">
                                    </div>
                                </td>
                                <td><input type="checkbox" name="complete[{{$key}}][status]"></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </form>
                </div>
                <div role="tabpanel" class="tab-pane" id="return">
                    <form method="POST" action="/factory/addReturnTransport">
                        <input type="hidden" name="customers_order_id" value="{{$orderId}}">
                        <div class="row">
                            <div class="col-lg-10" style="padding: 10px;"></div>
                            <div class="col-lg-2" style="padding: 10px;">
                                <button class="btn btn-default"><span class="glyphicon glyphicon-floppy-saved" aria-hidden="true"></span> บันทึกส่งคืน</button>
                            </div>
                        </div>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>รายการสินค้าที่จะผลิต</th>
                                <th style="text-align: right;">จำนวนทั้งหมด</th>
                                <th style="text-align: right;">ผลิตเสร็จ</th>
                                <th style="text-align: right;">ส่งเเล้ว</th>
                                <th style="text-align: right;">เสียหาย</th>
                                <th style="text-align: right;">เหลือ</th>
                                <th>จำนวนส่งคืน</th>
                                <th>เลือก</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($customersOrderProduct as $key=>$value)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$value->customers_order_product_name}}</td>
                                    <td style="text-align: right;">{{$value->customers_order_product_count}} {{\App\Models\ProductUnit::getNameUnitByProductName($value->customers_order_product_name)}}</td>
                                    <td style="text-align: right;">{{\App\Models\CustomersOrderPlan::getSumComplete($orderId,$value->customers_order_product_id)}} {{\App\Models\ProductUnit::getNameUnitByProductName($value->customers_order_product_name)}}</td>
                                    <td style="text-align: right;">{{\App\Models\CustomersOrderTransportProduct::getSumComplete($orderId,$value->customers_order_product_id)}} {{\App\Models\ProductUnit::getNameUnitByProductName($value->customers_order_product_name)}}</td>
                                    <td style="text-align: right;">{{\App\Models\CustomersOrderTransportProduct::getSumWaste($orderId,$value->customers_order_product_id)}} {{\App\Models\ProductUnit::getNameUnitByProductName($value->customers_order_product_name)}}</td>
                                    <td style="text-align: right;">{{$value->customers_order_product_count-(\App\Models\CustomersOrderTransportProduct::getSumComplete($orderId,$value->customers_order_product_id))}} {{\App\Models\ProductUnit::getNameUnitByProductName($value->customers_order_product_name)}}</td>
                                    <td>
                                        <div class="form-inline">
                                            <select name="complete[{{$key}}][customers_order_transport_product_id]" class="form-control">
                                                <option value="">เลือก</option>
                                                @if(!empty(\App\Models\CustomersOrderTransportProduct::getTransportProductCode($orderId,$value->customers_order_product_id)))
                                                    @foreach(\App\Models\CustomersOrderTransportProduct::getTransportProductCode($orderId,$value->customers_order_product_id) as $keySelect=>$valueSelect)
                                                        <option value="{{$valueSelect->customers_order_transport_product_id}}">{{$valueSelect->customers_order_transport_code}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                            <input name="complete[{{$key}}][count]" class="form-control input-sm" type="text" value="0" placeholder="จำนวนส่งเสียหาย">
                                        </div>
                                    </td>
                                    <td><input type="checkbox" name="complete[{{$key}}][status]"></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>

        </div>
    </div>
@stop