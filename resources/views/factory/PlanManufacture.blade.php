@extends('layouts.layouts')
@section('content')
    <div class="row" style="padding-top: 10px;">
        <div class="col-lg-12">
            <ol class="breadcrumb" style="margin: 0px;">
                <li><a href="/factory/MarkOrderEngineer">ผลิตสินค้า(ฝ่ายวิศวกร)</a></li>
                <li class="active">แผนการผลิต</li>
            </ol>
        </div>
    </div>
    <div class="row" style="padding: 10px 0px;">
        <div class="col-lg-3" style="padding: 10px;">
            <span style="font-size: 26px;font-weight: bolder">แผนการผลิต</span>
        </div>
        <div class="col-lg-9">
            <div style="padding: 5px;border: solid 1px #444;border-radius: 5px;background-color: #aaa;">
                <div style="padding: 5px 10px;">
                    <span style="font-weight: bolder;">ชื่องาน: </span>
                    <span>{{$dataCustomersOrder->customers_order_name}}</span>
                    <span style="padding-left: 20px;font-weight: bolder;">ชื่อลูกค้า: </span><span>{{$dataCustomersOrder->customers_order_customers}}</span>
                    <a style="margin-left:5px;float: right;" href="/factory/checkMaterial?orderId={{$orderId}}" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span> ถอดแบบ</a>
                    <a data-toggle="modal" data-target="#myModalProduct" style="float: right;" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> เพิ่มสินค้าสำเร็จรูป</a>
                </div>
                <div style="padding: 5px 10px;">
                    <table class="table table-hover">
                        <tr>
                            <th>#</th>
                            <th>ชื่อสินค้าสำเร็จรูป</th>
                            <th>ประเภท</th>
                            <th style="text-align: center;">ผลิตทั้งหมด</th>
                            <th style="text-align: center;">ผลิตเสร็จ</th>
                            <th style="text-align: center;">ยังไม่ผลิต</th>
                            <th style="text-align: center;">เสียหาย</th>
                            <th style="text-align: center;">%</th>
                        </tr>
                        @foreach($dataOrderProduct as $key=>$value)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$value->customers_order_product_name}}</td>
                            <td>
                                @if($value->customers_order_product_type=="Normal")
                                    <span class="label label-success">ใบสั่งซื้อ</span>
                                @elseif($value->customers_order_product_type=="Supplement")
                                    <span class="label label-warning">ผลิตเสริม</span>
                                @endif
                            </td>
                            <td style="text-align: center;">{{$value->customers_order_product_count}}</td>
                            <td style="text-align: center;">{{\App\Models\CustomersOrderPlan::getSumComplete($value->customers_order_id,$value->customers_order_product_id)}}</td>
                            <td style="text-align: center;">{{((($value->customers_order_product_count-(\App\Models\CustomersOrderPlan::getSumComplete($value->customers_order_id,$value->customers_order_product_id))))>0?($value->customers_order_product_count-(\App\Models\CustomersOrderPlan::getSumComplete($value->customers_order_id,$value->customers_order_product_id))):0)}}</td>
                            <td style="text-align: center;">{{\App\Models\CustomersOrderPlan::getSumWaste($value->customers_order_id,$value->customers_order_product_id)}}</td>
                            <td>
                                <div class="progress" style="margin-bottom: 0px;">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="" aria-valuemin="0" aria-valuemax="100" style="width: {{(((\App\Models\CustomersOrderPlan::getSumComplete($value->customers_order_id,$value->customers_order_product_id))/$value->customers_order_product_count)*100)}}%;">
                                        {{number_format((((\App\Models\CustomersOrderPlan::getSumComplete($value->customers_order_id,$value->customers_order_product_id))/$value->customers_order_product_count)*100),2)}}%
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="padding-bottom: 10px;">
        <div class="col-lg-10">
            <form method="GET" action="/factory/PlanManufacture" class="form-inline">
            {{--<input type="hidden" id="order_product_id" name="customers_order_product_id" value="@if($newCustomersOrderPlanMaterial->order_product_id){{$newCustomersOrderPlanMaterial->order_product_id}}@endif">--}}
            <input type="hidden" id="order_id" name="orderId" value="{{$orderId}}">
            {{--<input type="hidden" id="order_plan_id" name="customers_order_plan_id" value="@if($newCustomersOrderPlanMaterial->customers_order_plan_id){{$newCustomersOrderPlanMaterial->customers_order_plan_id}}@endif">--}}
            <select name="dateSelect" style="width: 200px;" class="form-control">
                <option value="">เลือกวันที่จะผลิต</option>
                @foreach($dataDateCustomersOrderPlan as $key=>$value)
                    <option value="{{$value->customers_order_plan_date}}">{{$value->customers_order_plan_date}}</option>
                @endforeach
            </select>
            <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
            <a href="/factory/PlanManufacture?orderId={{$orderId}}" class="btn btn-default"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span></a>
            </form>
        </div>
        <div class="col-lg-2">
            <a style="" class="btn btn-default" data-toggle="modal" data-target="#myModaladd"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> สร้างแผนการผลิต</a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th style="text-align: center;">วันที่</th>
                    <th style="text-align: center;">สภานะ</th>
                    <th style="text-align: center;">ประเภท</th>
                    <th style="text-align: center;">รายการสินค้าที่จะผลิต</th>
                    <th style="text-align: center;">งานที่จะผลิต</th>
                    <th style="text-align: center;">ส่งผลิต</th>
                </tr>
                </thead>
                <tbody>
                @foreach($dataCustomersOrderPlan as $key=>$value)
                <tr>
                    <td>{{$value->customers_order_plan_date}}</td>
                    <td style="text-align: center;">
                        @if($value->customers_order_plan_status=='create')
                            <span class="label label-default">รอส่งอนุมัติ</span>
                        @elseif($value->customers_order_plan_status=='confirmation')
                            <span class="label label-warning">รออนุมัติ</span>
                        @elseif($value->customers_order_plan_status=='approve')
                            <span class="label label-success">อนุมัติ</span>
                        @elseif($value->customers_order_plan_status=='complete')
                            <span class="label label-info">ตรวจสอบเเล้ว</span>
                        @elseif($value->customers_order_plan_status=='checkQuality')
                            <span class="label label-danger">รอตรวจสอบเเล้ว</span>
                        @endif
                    </td>
                    <td>
                        @if($value->customers_order_plan_type=='Normal')
                            <span class="label label-primary">แผนตามใบสั่งซื้อ</span>
                        @elseif($value->customers_order_plan_type=='Compensate')
                            <span class="label label-danger">แผนผลิตทดแทน</span>
                        @endif
                    </td>
                    <td>{{\App\Models\CustomersOrderProduct::getNameProduct($value->customers_order_product_id)}} {{$value->customers_order_plan_count}} {{\App\Models\ProductUnit::getNameUnitByProductName(\App\Models\CustomersOrderProduct::getNameProduct($value->customers_order_product_id))}}</td>
                    <td>
                        @if(!empty(\App\Models\CustomersOrderPlanMaterial::getByOrderPlanId($value->customers_order_plan_id)))
                            <table class="table table-bordered">
                                <tr>
                                    <th>วัตถุดิบ</th>
                                    <th>คลัง</th>
                                    <th>ยอดคงคลัง</th>
                                    <th>จำนวนเบิก</th>
                                </tr>
                                @foreach(\App\Models\CustomersOrderPlanMaterial::getByOrderPlanId($value->customers_order_plan_id) as $keyMaterial=>$valueMaterial)
                                <tr>
                                    <td>{{\App\Models\CustomersOrderMaterial::getNameMaterialName($valueMaterial->customers_order_material_id)}}</td>
                                    <td>{{\App\Models\Storename::getStoreName(\App\Models\CustomersOrderMaterial::getNameStoreId($valueMaterial->customers_order_material_id))}}</td>
                                    <td>{{\App\Models\Stores::CheckStoreCount(\App\Models\Product::GetIdProduct(\App\Models\CustomersOrderMaterial::getNameMaterialName($valueMaterial->customers_order_material_id)),\App\Models\CustomersOrderMaterial::getNameStoreId($valueMaterial->customers_order_material_id))}} {{\App\Models\ProductUnit::getNameUnitByProductName(\App\Models\CustomersOrderMaterial::getNameMaterialName($valueMaterial->customers_order_material_id))}}</td>
                                    <td>{{$valueMaterial->customers_order_material_count}} {{\App\Models\ProductUnit::getNameUnitByProductName(\App\Models\CustomersOrderMaterial::getNameMaterialName($valueMaterial->customers_order_material_id))}}</td>
                                </tr>
                                @endforeach
                            </table>
                        @endif
                    </td>
                    <td>
                        @if($value->customers_order_plan_status=='create')
                            <a onclick="SetInput({{$value->customers_order_id}},{{$value->customers_order_plan_id}},{{$value->customers_order_product_id}})" class="btn btn-success" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> เพิ่มวัถุดิบ</a>
                            <a href="/factory/sendCheckPlan?orderId={{$orderId}}&planId={{$value->customers_order_plan_id}}" onclick="return confirm('ยืนยันการส่งอนุมัติเบิกสินค้า')" class="btn btn-primary" ><span class="glyphicon glyphicon-check" aria-hidden="true"></span> ส่งอนุมัติ</a>
                            <a href="/factory/deletePlanEngineer?orderId={{$orderId}}&planId={{$value->customers_order_plan_id}}" onclick="return confirm('ยกเลิกแผนการผลิต')" class="btn btn-danger" ><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> ยกเลิก</a>
                        @elseif($value->customers_order_plan_status=='confirmation')
                            {{--<a onclick="SetInput({{$value->customers_order_id}},{{$value->customers_order_plan_id}},{{$value->customers_order_product_id}})" class="btn btn-success" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> เพิ่มวัถุดิบ</a>--}}
                        @endif
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {
            $( "#datepicker" ).datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat:'yy-mm-dd'
            });
        });
    </script>

    <style>
        .ui-datepicker-month,.ui-datepicker-year{
            color: #333;
        }
    </style>

    <script>
        function SetInput(order_id,order_plan_id,order_product_id) {
            $('input#order_id').val(order_id);
            $('#order_plan_id').val(order_plan_id);
            $('#order_product_id').val(order_product_id);
            $.get( "/factory/getOrderMaterial?orderId="+order_product_id, function( data ) {
                $( "#order_material_id" ).html( data );
            });
        }
    </script>

    <div class="modal fade @if (true==$errors->has('customers_order_material_id')||true==$errors->has('customers_order_material_count')) in @endif" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="@if (true==$errors->has('customers_order_material_id')||true==$errors->has('customers_order_material_count')) display: block; @endif">
        <div class="modal-dialog" role="document">
            <form method="POST" action="/factory/addCustomersOrderPlanMaterial" novalidate>
            <div class="modal-content">
                <div class="modal-header">
                    <a href="/factory/PlanManufacture?orderId={{$orderId}}" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title" id="myModalLabel">เพิ่มวัตถุดิบที่จะเบิก</h4>
                </div>
                <div class="modal-body">
                    @if (true==$errors->has('customers_order_material_id')||true==$errors->has('customers_order_material_count'))
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif
                    <input type="hidden" id="order_product_id" name="customers_order_product_id" value="@if($newCustomersOrderPlanMaterial->order_product_id){{$newCustomersOrderPlanMaterial->order_product_id}}@endif">
                    <input type="hidden" id="order_id" name="customers_order_id" value="@if($newCustomersOrderPlanMaterial->customers_order_id){{$newCustomersOrderPlanMaterial->customers_order_id}}@endif">
                    <input type="hidden" id="order_plan_id" name="customers_order_plan_id" value="@if($newCustomersOrderPlanMaterial->customers_order_plan_id){{$newCustomersOrderPlanMaterial->customers_order_plan_id}}@endif">
                        <div class="form-group  @if ($errors->has('customers_order_material_id')) has-error @endif">
                            <label for="exampleInputEmail1">ชื่อวัถุดิบ</label>
                            <select name="customers_order_material_id" id="order_material_id" class="form-control">
                                @if(!empty($dataCustomersOrderMaterial))
                                    @foreach($dataCustomersOrderMaterial as $key=>$value)
                                        <option value="{{$value->customers_order_material_id}}">{{$value->customers_order_material_name}}:{{$value->storename_name}}</option>
                                    @endforeach
                                @else
                                    <option>เลือก</option>
                                @endif
                            </select>
                        </div>
                        <div class="form-group @if ($errors->has('customers_order_material_count')) has-error @endif">
                            <label for="exampleInputPassword1">จำนวน</label>
                            <input name="customers_order_material_count" type="text" class="form-control" id="exampleInputPassword1" placeholder="จำนวน">
                        </div>
                </div>
                <div class="modal-footer">
                    <a hidden="/factory/PlanManufacture?orderId={{$orderId}}" type="button" class="btn btn-default" data-dismiss="modal">Close</a>
                    <button type="submit" class="btn btn-primary"> เพิ่ม</button>
                </div>
            </div>
            </form>
        </div>
    </div>

    <div class="modal fade  @if (true==$errors->has('customers_order_plan_date')||true==$errors->has('customers_order_plan_count')||true==$errors->has('customers_order_product_id')) in @endif" id="myModaladd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="@if (true==$errors->has('customers_order_plan_date')||true==$errors->has('customers_order_plan_count')||true==$errors->has('customers_order_product_id')) display: block; @endif">
        <div class="modal-dialog" role="document">
            <form method="POST" action="/factory/addCustomersOrderPlan" class="form-horizontal" novalidate>
                <div class="modal-content">
                    <div class="modal-header">
                        <a href="/factory/PlanManufacture?orderId={{$orderId}}" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                        <h4 class="modal-title" id="myModalLabel">เพิ่ม-แก้ไข แผนการผลิต</h4>
                    </div>
                    <div class="modal-body">
                        @if (true==$errors->has('customers_order_plan_date')||true==$errors->has('customers_order_plan_count')||true==$errors->has('customers_order_product_id'))
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </div>
                        @endif
                        <div class="form-horizontal" style="padding: 0px 20px;">
                            <input type="hidden" name="customers_order_id" value="{{$orderId}}">

                            <div class="form-group @if ($errors->has('customers_order_plan_type')) has-error @endif">
                                <label for="exampleInputEmail1">รูปแบบแผน</label>
                                <label class="radio-inline" style="margin-left: 15px;padding-top: 0px;">
                                    <input type="radio" checked name="customers_order_plan_type" id="inlineRadio1" value="Normal"> แผนตามใบสั่งซื้อ
                                </label>
                                <label class="radio-inline" style="margin-left: 15px;padding-top: 0px;">
                                    <input type="radio"  name="customers_order_plan_type" id="inlineRadio2" value="Compensate"> แผนผลิตทดแทน
                                </label>
                            </div>

                            <div class="form-group @if ($errors->has('customers_order_plan_date')) has-error @endif">
                                <label for="exampleInputPassword1">วันที่ <span style="color: #FF0000;">*</span></label>
                                <input name="customers_order_plan_date" type="text" class="form-control" id="datepicker" placeholder="วันที่">
                            </div>
                            <div class="form-group  @if ($errors->has('customers_order_product_id')) has-error @endif">
                                <label for="exampleInputEmail1">ชื่อสินค้าสำเร็จ</label>
                                <select name="customers_order_product_id" class="form-control">
                                    @foreach($dataOrderProduct as $key=>$value)
                                        <option value="{{$value->customers_order_product_id}}">{{$value->customers_order_product_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group  @if ($errors->has('customers_order_plan_count')) has-error @endif">
                                <label for="exampleInputPassword1">จำนวน <span style="color: #FF0000;">*</span></label>
                                <input name="customers_order_plan_count" type="text" class="form-control" id="exampleInputPassword1" placeholder="จำนวน">
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="/factory/PlanManufacture?orderId={{$orderId}}" type="button" class="btn btn-default" data-dismiss="modal">Close</a>
                        <input type="submit" class="btn btn-primary" value="เพิ่มแผน"/>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <style>
        .twitter-typeahead{
            width: 100% !important;
        }
        .tt-query {
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        }

        .tt-hint {
            color: #999
        }

        .tt-menu {
            width: 422px;
            margin: 12px 0;
            padding: 8px 0;
            background-color: #fff;
            border: 1px solid #ccc;
            border: 1px solid rgba(0, 0, 0, 0.2);
            -webkit-border-radius: 8px;
            -moz-border-radius: 8px;
            border-radius: 8px;
            -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
            -moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
            box-shadow: 0 5px 10px rgba(0,0,0,.2);
        }

        .tt-suggestion {
            padding: 3px 20px;
            font-size: 18px;
            line-height: 24px;
        }

        .tt-suggestion:hover {
            cursor: pointer;
            color: #fff;
            background-color: #0097cf;
        }

        .tt-suggestion.tt-cursor {
            color: #fff;
            background-color: #0097cf;

        }

        .tt-suggestion p {
            margin: 0;
        }

        .gist {
            font-size: 14px;
        }
    </style>


    <script>
        $(document).ready(function () {
            $('select#selectStorename').val(2);
            var substringMatcher = function (strs) {
                return function findMatches(q, cb) {
                    var matches, substringRegex;

                    // an array that will be populated with substring matches
                    matches = [];

                    // regex used to determine if a string contains the substring `q`
                    substrRegex = new RegExp(q, 'i');

                    // iterate through the pool of strings and for any string that
                    // contains the substring `q`, add it to the `matches` array
                    $.each(strs, function (i, str) {
                        if (substrRegex.test(str)) {
                            matches.push(str);
                        }
                    });

                    cb(matches);
                };
            };

            $.ajax({
                url: "/factory/checkProductManufacture",
                type: "post",
                dataType: 'json'
            }).success(function (res) {
                $('input#the-basics').typeahead({
                            hint: true,
                            highlight: true,
                            minLength: 1
                        },
                        {
                            name: 'states',
                            source: substringMatcher(res)
                        });
            })
        });
    </script>

    <div class="modal fade @if (!empty($CustomersOrderProduct->customers_order_id) || true==$errors->has('customers_order_product_name')||true==$errors->has('customers_order_product_count')||true==$errors->has('customers_order_product_price')) in @endif" id="myModalProduct" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="@if (!empty($CustomersOrderProduct->customers_order_id) || true==$errors->has('customers_order_product_name')||true==$errors->has('customers_order_product_count')) display: block; @endif">
        <div class="modal-dialog" role="document">
            <form method="POST" action="/factory/addProductOrderEngineer" class="form-horizontal" novalidate >
                <div class="modal-content">
                    <div class="modal-header">
                        <a href="/factory/PlanManufacture?orderId={{$orderId}}" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                        <h4 class="modal-title" id="myModalLabel">เพิ่มสินค้าที่จะผลิต</h4>
                    </div>
                    <div class="modal-body">
                        @if (true==$errors->has('customers_order_product_name')||true==$errors->has('customers_order_product_count'))
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </div>
                        @endif
                        <div class="form-horizontal" style="padding: 0px 20px;">
                            <input type="hidden" name="customers_order_product_id" value="{{$CustomersOrderProduct->customers_order_product_id}}"/>
                            <input type="hidden" name="customers_order_id" value="{{$orderId}}"/>
                            <div class="form-group @if ($errors->has('customers_order_product_type')) has-error @endif">
                                <label for="exampleInputEmail1">รูปแบการผลิต</label>
                                <label class="radio-inline" style="margin-left: 15px;padding-top: 0px;">
                                    <input type="radio"  name="customers_order_product_type" id="inlineRadio1" value="Normal"> ผลิตตามใบสั่งซื้อ
                                </label>
                                <label class="radio-inline" style="margin-left: 15px;padding-top: 0px;">
                                    <input type="radio" checked name="customers_order_product_type" id="inlineRadio2" value="Supplement"> ผลิตเสริม
                                </label>
                            </div>
                            <div class="form-group @if ($errors->has('customers_order_product_name')) has-error @endif">
                                <label for="exampleInputEmail1">ชื่อสำเร็จรูป</label>
                                <input id="the-basics" value="{{Input::old('customers_order_product_name')}}" name="customers_order_product_name" type="text" class="form-control" id="exampleInputEmail1" placeholder="ชื่อสำเร็จรูป">
                            </div>
                            <div class="form-group @if ($errors->has('customers_order_product_count')) has-error @endif">
                                <label for="exampleInputPassword1">จำนวน</label>
                                <input type="text" value="{{Input::old('customers_order_product_count')}}" name="customers_order_product_count" class="form-control" id="exampleInputPassword1" placeholder="จำนวน">
                            </div>
                            <div class="form-group @if ($errors->has('customers_order_product_price')) has-error @endif">
                                <label for="exampleInputPassword1">ราคา</label>
                                <input type="text" value="{{(!empty(Input::old('customers_order_product_price'))?Input::old('customers_order_product_price'):0)}}" name="customers_order_product_price" class="form-control" id="exampleInputPassword1" placeholder="ราคา">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="/factory/PlanManufacture?orderId={{$orderId}}" type="button" class="btn btn-default" data-dismiss="modal">Close</a>
                        <input type="submit" class="btn btn-primary" value="Save changes" />
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop