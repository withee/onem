@extends('layouts.layouts')
@section('content')
    {{--<div class="row" style="padding-top: 10px;">--}}
        {{--<div class="col-lg-12">--}}
            {{--<ol class="breadcrumb" style="margin: 0px;">--}}
                {{--<li><a href="/factory/checkMaterial">ถอดแบบการผลิต</a></li>--}}
                {{--<li class="active">PR</li>--}}
            {{--</ol>--}}
        {{--</div>--}}
    {{--</div>--}}
    <div class="row">
        <div class="col-lg-8" style="padding: 10px;">
            <span style="font-size: 26px;font-weight: bolder">ผลิตสินค้า(ฝ่ายผู้จัดการ)</span>
        </div>
        <div class="col-lg-4" style="padding: 10px;">
            <form method="GET" action="/factory/MarkOrderManager" class="form-inline">
                <div class="form-group">
                    <label for="exampleInputEmail2">ค้นหา</label>
                    <input style="width: 209px;" type="text" class="form-control" name="search" placeholder="ชื่อ Order">
                </div>
                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                <a href="/factory/MarkOrderManager" class="btn btn-default"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span></a>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th>ชื่อ Order</th>
                    {{--<th>% การผลิต</th>--}}
                    <th>สถานะ</th>
                    <th>จัดการ</th>
                </tr>
                </thead>
                <tbody>
                @foreach($dataCustomersOrder['data'] as $key=>$value)
                    <tr>
                        <th scope="row">{{$key+1}}</th>
                        <td><a href="/factory/ApproveManufacture?orderId={{$value->customers_order_id}}">{{$value->customers_order_name}}</a></td>
                        {{--<td>--}}
                            {{--<div class="progress">--}}
                                {{--<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">--}}
                                    {{--60%--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</td>--}}
                        <td>
                            {{\App\Models\CustomersOrder::checkStatus($value->customers_order_status)}}
                        </td>
                        <td>
                            <a href="/factory/ReportTransport?orderId={{$value->customers_order_id}}" class="btn btn-info">
                                <span class="glyphicon glyphicon-road" aria-hidden="true"></span> รายงานขนส่งสินค้า
                            </a>
                            @if($value->customers_order_status!='complete')
                                <a href="/factory/closeCustomersOrder?orderId={{$value->customers_order_id}}" onclick="return confirm('ยืนยันการปิดงาน')" class="btn btn-success"><span class="glyphicon glyphicon-check" aria-hidden="true"></span> อนุมัติปิดงานนี้</a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12" style="text-align: center;">
            <nav>
                <ul class="pagination" style="margin: 0px;padding: 0px;padding-top: 3px;">
                    @if($dataCustomersOrder['page']>1)
                        <li>
                            <a href="/factory/MarkOrderManager?page={{$dataCustomersOrder['page']-1}}@if(false==empty($dataCustomersOrder['search']))&search={{$dataCustomersOrder['search']}}@endif">Prev</a>
                        </li>
                    @endif

                    @for($x = 1; $x <= $dataCustomersOrder['max']; $x++)
                        @if($x==$dataCustomersOrder['page'])
                            <li class="active">
                                <a href="/MarkOrder/MarkOrderManager?page={{$x}}@if(false==empty($dataCustomersOrder['search']))&search={{$dataCustomersOrder['search']}}@endif">{{$x}}</a>
                            </li>
                        @else
                            <li>
                                <a href="/factory/MarkOrderManager?page={{$x}}@if(false==empty($dataCustomersOrder['search']))&search={{$dataCustomersOrder['search']}}@endif">{{$x}}</a>
                            </li>
                        @endif
                    @endfor

                    @if($dataCustomersOrder['page']<$dataCustomersOrder['max'])
                        <li>
                            <a href="/factory/MarkOrderManager?page={{$dataCustomersOrder['page']+1}}@if(false==empty($dataCustomersOrder['search']))&search={{$dataCustomersOrder['search']}}@endif">Next</a>
                        </li>
                    @endif
                </ul>
            </nav>
        </div>
    </div>
@stop