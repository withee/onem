@extends('layouts.layouts')
@section('content')
    <div class="row" style="padding-top: 10px;">
        <div class="col-lg-12">
            <ol class="breadcrumb" style="margin: 0px;">
                <li><a href="/factory/MarkOrderCompensate">ผลิตทดแทน</a></li>
                <li class="active">ถอดแบการผลิตทดแทน</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12" style="padding: 10px;">
            <span style="font-size: 26px;font-weight: bolder">ถอดแบบการผลิตทดแทน</span>
        </div>
    </div>

    <div class="row" style="padding: 10px;">
        <div class="col-lg-9">
            <div style="padding: 5px;border: solid 1px #444;border-radius: 5px;background-color: #aaa;">
                <div style="padding: 5px 10px;"><span>ชื่องาน: </span><span>{{$order['customers_order_name']}}</span></div>
                <div style="padding: 5px 10px;"><span>ชื่อลูกค้า: </span><span>{{$order['customers_order_customers']}}</span></div>
            </div>
        </div>
        <div class="col-lg-3">
            <a style="" class="btn btn-default" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> เพิ่มสินค้าที่จะผลิต</a>
            {{--<a style="" class="btn btn-danger" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> ยกเลิกการผลิต</a>--}}
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th>รายการสินค้าที่จะผลิต</th>
                    <th>รายการวัตถุดิบ</th>
                    <th>จัดการ</th>
                </tr>
                </thead>
                <tbody>
                @foreach($dataCustomersOrderProduct['data'] as $key=>$value)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$value->customers_order_product_name}} {{$value->customers_order_product_count}} {{\App\Models\ProductUnit::getNameUnitByProductName($value->customers_order_product_name)}}</td>
                        <td>
                            <table class="table table-bordered">
                                <tr>
                                    <th>วัตถุดิบ</th>
                                    <th>คลัง</th>
                                    <th>จำนวน</th>
                                    <th>ยอดคงคลัง</th>
                                    <th>PR</th>
                                    <th style="width: 40px;">ลบ</th>
                                </tr>
                                @foreach(\App\Models\CustomersOrderMaterial::getListMaterial($value->customers_order_product_id) as $key=>$value)
                                    <tr class="{{\App\Models\Stores::CheckStoreStatus(\App\Models\Product::GetIdProduct($value->customers_order_material_name),$value->storename_id,$value->customers_order_material_count)}}">
                                        <td>{{$value->customers_order_material_name}}</td>
                                        <td>{{\App\Models\Storename::GetNameStore($value->storename_id)}}</td>
                                        <td>{{$value->customers_order_material_count}} {{\App\Models\ProductUnit::getNameUnitByProductName($value->customers_order_material_name)}}</td>
                                        <td>{{\App\Models\Stores::CheckStoreCount(\App\Models\Product::GetIdProduct($value->customers_order_material_name),$value->storename_id)}} {{\App\Models\ProductUnit::getNameUnitByProductName($value->customers_order_material_name)}}</td>
                                        <td><a href="/Pr/listPr">(PR)</a></td>
                                        <td><a href="/factory/deleteCustomersOrderMaterialCompensate?id={{$value->customers_order_material_id}}&orderId={{$orderId}}" onclick="return confirm('ยืนยันการยกเลิกวัตถุดิบนี้ ')" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>
                                    </tr>
                                @endforeach
                            </table>
                        </td>
                        <td>
                            <a onclick="SetInput({{$value->customers_order_product_id}})" class="btn btn-primary" data-toggle="modal" data-target="#myModalAdd"><span class="glyphicon glyphicon-flag" aria-hidden="true"></span> เพิ่มวัถุดิบ</a>
                            <a href="/factory/deleteCustomersOrderProduct?orderId={{$orderId}}&id={{$value->customers_order_product_id}}" onclick="return confirm('ยืนยันการยกเลิกการผลิตนี้')" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> ยกเลิกการผลิต</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <script>
        function SetInput(id) {
            $('#InputCustomersOrderProduct').val(id);
        }
    </script>

    <div class="row">
        <div class="col-lg-12" style="text-align: center;">
            <nav>
                <ul class="pagination" style="margin: 0px;padding: 0px;padding-top: 3px;">
                    @if($dataCustomersOrderProduct['page']>1)
                        <li>
                            <a href="/factory/Compensate?orderId={{$orderId}}&page={{$dataCustomersOrderProduct['page']-1}}@if(false==empty($dataCustomersOrderProduct['search']))&search={{$dataCustomersOrderProduct['search']}}@endif">Prev</a>
                        </li>
                    @endif

                    @for($x = 1; $x <= $dataCustomersOrderProduct['max']; $x++)
                        @if($x==$dataCustomersOrderProduct['page'])
                            <li class="active"><a
                                        href="/factory/Compensate?orderId={{$orderId}}&page={{$x}}@if(false==empty($dataCustomersOrderProduct['search']))&search={{$dataCustomersOrderProduct['search']}}@endif">{{$x}}</a>
                            </li>
                        @else
                            <li>
                                <a href="/factory/Compensate?orderId={{$orderId}}&page={{$x}}@if(false==empty($dataCustomersOrderProduct['search']))&search={{$dataCustomersOrderProduct['search']}}@endif">{{$x}}</a>
                            </li>
                        @endif
                    @endfor

                    @if($dataCustomersOrderProduct['page']<$dataCustomersOrderProduct['max'])
                        <li>
                            <a href="/factory/Compensate?orderId={{$orderId}}&page={{$dataCustomersOrderProduct['page']+1}}@if(false==empty($dataCustomersOrderProduct['search']))&search={{$dataCustomersOrderProduct['search']}}@endif">Next</a>
                        </li>
                    @endif
                </ul>
            </nav>
        </div>
    </div>

    <style>
        .twitter-typeahead{
            width: 100% !important;
        }
        .tt-query {
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        }

        .tt-hint {
            color: #999
        }

        .tt-menu {
            width: 422px;
            margin: 12px 0;
            padding: 8px 0;
            background-color: #fff;
            border: 1px solid #ccc;
            border: 1px solid rgba(0, 0, 0, 0.2);
            -webkit-border-radius: 8px;
            -moz-border-radius: 8px;
            border-radius: 8px;
            -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
            -moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
            box-shadow: 0 5px 10px rgba(0,0,0,.2);
        }

        .tt-suggestion {
            padding: 3px 20px;
            font-size: 18px;
            line-height: 24px;
        }

        .tt-suggestion:hover {
            cursor: pointer;
            color: #fff;
            background-color: #0097cf;
        }

        .tt-suggestion.tt-cursor {
            color: #fff;
            background-color: #0097cf;

        }

        .tt-suggestion p {
            margin: 0;
        }

        .gist {
            font-size: 14px;
        }
    </style>


    <script>
        $(document).ready(function () {

            $('select#selectStorename').val(2);

            var substringMatcher = function (strs) {
                return function findMatches(q, cb) {
                    var matches, substringRegex;

                    // an array that will be populated with substring matches
                    matches = [];

                    // regex used to determine if a string contains the substring `q`
                    substrRegex = new RegExp(q, 'i');

                    // iterate through the pool of strings and for any string that
                    // contains the substring `q`, add it to the `matches` array
                    $.each(strs, function (i, str) {
                        if (substrRegex.test(str)) {
                            matches.push(str);
                        }
                    });

                    cb(matches);
                };
            };

            $.ajax({
                url: "/factory/checkProductManufacture",
                type: "post",
                dataType: 'json'
            }).success(function (res) {
                $('input#the-basics').typeahead({
                            hint: true,
                            highlight: true,
                            minLength: 1
                        },
                        {
                            name: 'states',
                            source: substringMatcher(res)
                        });
            })

            $.ajax({
                url: "/factory/checkProductMaterial",
                type: "post",
                dataType: 'json'
            }).success(function (res) {
                $('input#the-basics2').typeahead({
                            hint: true,
                            highlight: true,
                            minLength: 1
                        },
                        {
                            name: 'states',
                            source: substringMatcher(res)
                        });
            })
        });

        function addPR(product_name,job_name,com_name,quantity){
            $.ajax({
                url: "/factory/addprimmediately",
                type: "post",
                data:{pname:product_name,jname:job_name,cname:com_name,quantity:quantity},
                dataType: 'json'
            }).success(function (res) {
                console.log(res);
                if(res.success){
                    alert("New PR added");
                }
            });
        }
    </script>

    {{--<!-- Modal -->--}}
    {{--<div class="modal fade @if (!empty($customersOrder->customers_order_id) || true==$errors->has('customers_order_customers')||true==$errors->has('customers_order_name')) in @endif" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="@if (!empty($customersOrder->customers_order_id) || true==$errors->has('customers_order_customers')||true==$errors->has('customers_order_name')) display: block; @endif">--}}
    {{--<div class="modal-dialog" role="document">--}}
    {{--<form method="POST" action="/factory/createOrder" class="form-horizontal" novalidate >--}}
    {{--<div class="modal-content">--}}
    {{--<div class="modal-header">--}}
    {{--<a href="/factory/addOrder" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>--}}
    {{--<h4 class="modal-title" id="myModalLabel">เพิ่ม-แก้ไข เปิด Order ลูกค้า</h4>--}}
    {{--</div>--}}
    {{--<div class="modal-body">--}}
    {{--@if (true==$errors->has('customers_order_customers')||true==$errors->has('customers_order_name'))--}}
    {{--<div class="alert alert-danger">--}}
    {{--@foreach ($errors->all() as $error)--}}
    {{--{{ $error }}<br>--}}
    {{--@endforeach--}}
    {{--</div>--}}
    {{--@endif--}}
    {{--<div class="form-horizontal" style="padding: 0px 20px;">--}}
    {{--<input type="hidden" name="customers_order_id" value="{{$customersOrder->customers_order_id}}">--}}
    {{--<div class="form-group @if ($errors->has('customers_order_customers')) has-error @endif" style="margin-bottom: 0px;">--}}
    {{--<label for="exampleInputEmail1">ชื่อลูกค้า</label>--}}
    {{--</div>--}}
    {{--<div class="form-group @if ($errors->has('customers_order_customers')) has-error @endif">--}}
    {{--<input id="the-basics" type="text" name="customers_order_customers" class="form-control" id="exampleInputPassword1" placeholder="ชื่อลูกค้า">--}}
    {{--</div>--}}
    {{--<div class="form-group @if ($errors->has('customers_order_name')) has-error @endif">--}}
    {{--<label for="exampleInputPassword1">ชื่อ Order</label>--}}
    {{--<input type="text" class="form-control" name="customers_order_name" id="exampleInputPassword1" placeholder="ชื่อ Order">--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="modal-footer">--}}
    {{--<a href="/factory/addOrder" type="button" class="btn btn-default" data-dismiss="modal">Close</a>--}}
    {{--<input type="submit" class="btn btn-primary" value="Save changes" />--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</form>--}}
    {{--</div>--}}
    {{--</div>--}}

    <!-- Modal -->
    <div class="modal fade @if (!empty($CustomersOrderProduct->customers_order_id) || true==$errors->has('customers_order_product_name')||true==$errors->has('customers_order_product_count')) in @endif" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="@if (!empty($CustomersOrderProduct->customers_order_id) || true==$errors->has('customers_order_product_name')||true==$errors->has('customers_order_product_count')) display: block; @endif">
        <div class="modal-dialog" role="document">
            <form method="POST" action="/factory/addProductOrderCompensate" class="form-horizontal" novalidate >
                <div class="modal-content">
                    <div class="modal-header">
                        <a href="/factory/Compensate?orderId={{$orderId}}" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                        <h4 class="modal-title" id="myModalLabel">เพิ่มสินค้าที่จะผลิต</h4>
                    </div>
                    <div class="modal-body">
                        @if (true==$errors->has('customers_order_product_name')||true==$errors->has('customers_order_product_count'))
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </div>
                        @endif
                        <div class="form-horizontal" style="padding: 0px 20px;">
                            <input type="hidden" name="customers_order_product_id" value="{{$CustomersOrderProduct->customers_order_product_id}}"/>
                            <input type="hidden" name="customers_order_id" value="{{$orderId}}"/>
                            <div class="form-group @if ($errors->has('customers_order_product_name')) has-error @endif">
                                <label for="exampleInputEmail1">ชื่อสำเร็จรูป</label>
                                <input id="the-basics" value="{{Input::old('customers_order_product_name')}}" name="customers_order_product_name" type="text" class="form-control" id="exampleInputEmail1" placeholder="ชื่อสำเร็จรูป">
                            </div>
                            <div class="form-group @if ($errors->has('customers_order_product_count')) has-error @endif">
                                <label for="exampleInputPassword1">จำนวน</label>
                                <input type="text" value="{{Input::old('customers_order_product_count')}}" name="customers_order_product_count" class="form-control" id="exampleInputPassword1" placeholder="จำนวน">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="/factory/Compensate?orderId={{$orderId}}" type="button" class="btn btn-default" data-dismiss="modal">Close</a>
                        <input type="submit" class="btn btn-primary" value="Save changes" />
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade @if (true==$errors->has('customers_order_material_name')||true==$errors->has('customers_order_material_count')||true==$errors->has('storename_id')) in @endif" id="myModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="@if (true==$errors->has('customers_order_material_name')||true==$errors->has('customers_order_material_count')||true==$errors->has('storename_id')) display: block; @endif">
        <div class="modal-dialog" role="document">
            <form method="POST" action="/factory/addMaterialOrderCompensate" class="form-horizontal" novalidate>
                <div class="modal-content">
                    <div class="modal-header">
                        <a href="/factory/Compensate?orderId={{$orderId}}" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                        <h4 class="modal-title" id="myModalLabel">เพิ่ม-แก้ไข รายการสินค้าที่จะผลิต</h4>
                    </div>
                    <div class="modal-body">
                        @if (true==$errors->has('customers_order_material_name')||true==$errors->has('customers_order_material_count')||true==$errors->has('storename_id'))
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br>
                                @endforeach
                            </div>
                        @endif
                        <div class="form-horizontal" style="padding: 0px 20px;">
                            <input type="hidden" id="InputCustomersOrderProduct" name="customers_order_product_id" value=""/>
                            <input type="hidden" name="customers_order_id" value="{{$orderId}}"/>
                            <div class="form-group @if ($errors->has('customers_order_material_name')) has-error @endif">
                                <label for="exampleInputEmail1">ชื่อวัถุดิบ</label>
                                <input id="the-basics2" name="customers_order_material_name" type="text" class="form-control" id="exampleInputEmail1" placeholder="ชื่อวัถุดิบ">
                            </div>
                            <div class="form-group @if ($errors->has('customers_order_material_count')) has-error @endif">
                                <label for="exampleInputPassword1">จำนวน</label>
                                <input type="text" name="customers_order_material_count" class="form-control" id="exampleInputPassword1" placeholder="จำนวน">
                            </div>
                            <div class="form-group @if ($errors->has('storename_id')) has-error @endif">
                                <label for="exampleInputPassword1">คลัง</label>
                                <select class="form-control" id="selectStorename" name="storename_id">
                                    @foreach(\App\Models\Storename::all() as $key=>$value)
                                        <option value="{{$value->storename_id}}">{{$value->storename_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="/factory/Compensate?orderId={{$orderId}}" type="button" class="btn btn-default" data-dismiss="modal">Close</a>
                        <input type="submit" class="btn btn-primary" value="Save changes" />
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop