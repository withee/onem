@extends('layouts.layouts')
@section('content')
    <div class="row" style="padding-top: 10px;">
        <div class="col-lg-12">
            <ol class="breadcrumb" style="margin: 0px;">
                <li><a href="/factory/checkMaterial">ถอดแบบการผลิต</a></li>
                <li class="active">PR</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12" style="padding: 10px;">
            <span style="font-size: 26px;font-weight: bolder">ออกใบPR</span>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-9"></div>
        <div class="col-lg-3">
            <a style="" class="btn btn-default" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> เพิ่มสินค้า</a>
            {{--<a style="" class="btn btn-danger" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> ยกเลิกการผลิต</a>--}}
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th>รายการ</th>
                    <th>จำนวน</th>
                    <th>ผู้สั่ง</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th scope="row">1</th>
                    <td>เหล็ก</td>
                    <td>1000 เมตร</td>
                    <td>ผู้บริหาร1</td>
                </tr>
                <tr>
                    <th scope="row">1</th>
                    <td>น็อต</td>
                    <td>100 ตัว</td>
                    <td>ผู้บริหาร1</td>
                </tr>
                <tr>
                    <th scope="row">1</th>
                    <td>เหล็ก</td>
                    <td>1000 เมตร</td>
                    <td>ผู้บริหาร1</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <form class="form-horizontal">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">เพิ่ม-แก้ไข สินค้าPR</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal" style="padding: 0px 20px;">
                            <div class="form-group">
                                <label for="exampleInputEmail1">ชื่อวัถุดิบ</label>
                                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="ชื่อวัถุดิบ">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">จำนวน</label>
                                <input type="text" class="form-control" id="exampleInputPassword1" placeholder="จำนวน">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop