@extends('layouts.layouts')
@section('content')
    <div class="row" style="padding: 10px 0px 10px 10px;">
        <div class="col-lg-9">
            <span style="font-weight: bolder;font-size: 24px;">ข้อมูลพนักงานรายวัน</span>
        </div>
        <div class="col-lg-3">
            <form method="GET" action="/account/employeesDaily" class="form-inline">
                <div class="form-group">
                    {{--<label for="exampleInputName2">ค้นหา</label>--}}
                    <select name="month" class="form-control">
                        @foreach($monthNames as $key=>$value)
                            @if($key==$selectMonth && $value!=='')
                                <option selected value="{{$key}}">{{$value}}</option>
                            @elseif($value!=='')
                                <option value="{{$key}}">{{$value}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    {{--<label for="exampleInputEmail2">ปี</label>--}}
                    <select name="year" class="form-control">
                        <option @if ($selectYear=='2014')selected @endif value="2014">2014</option>
                        <option @if ($selectYear=='2015')selected @endif value="2015">2015</option>
                        <option @if ($selectYear=='2016')selected @endif value="2016">2016</option>
                        <option @if ($selectYear=='2017')selected @endif value="2017">2017</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
            </form>
        </div>
    </div>
    <style>
        table,tr,td,th{
            border-color: #000000 !important;
        }
    </style>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th style="text-align: center" colspan="7">ข้อมูลพนักงานรายวัน เดือน {{$monthNames[$selectMonth]}} ปี {{$selectYear}}</th>
                </tr>
                <tr>
                    <th style="width:14%;text-align: center">อาทิตย์.</th>
                    <th style="width:14%;text-align: center">จันทร์.</th>
                    <th style="width:14%;text-align: center">อังคาร.</th>
                    <th style="width:14%;text-align: center">พูธ.</th>
                    <th style="width:14%;text-align: center">พฤหัสบดี.</th>
                    <th style="width:14%;text-align: center">ศุกร์.</th>
                    <th style="width:14%;text-align: center">เสาร์.</th>
                </tr>
                </thead>
                <tbody>
                @for ($i = 1; $i < $countDay; $i++)
                    @if($datetime->format('w')==0)
                        <tr>
                    @endif
                    @if(intval($datetime->format('m'))!=$selectMonth)
                        <td class="active" style="width:14%;height: 100px;">
                    @elseif(($selectDay-1)>intval($datetime->format('d')))
                        <td day="{{$datetime->format('d')}}" class="" style="width:14%;height: 100px;">
                    @else
                        <td class="{{((($selectDay==intval($datetime->format('d')))&&($monthNow==$datetime->format('m')))?'success':'')}}" style="cursor: pointer;width:14%;height: 100px;">
                    @endif
                        @if(($selectDay-1>intval($datetime->format('d')))||($monthNow!=$datetime->format('m')))
                            <div style="border-bottom:solid 1px #FF0000;font-weight:bolder;width: 100%;text-align: right;height: 20px;color: #FF0000;">{{$datetime->format('d')}}</div>
                        @else
                            <div id="addDaily" data-toggle="modal" data-target="#myModal" day="{{$datetime->format('d')}}" style="border-bottom:solid 1px #cccccc;font-weight:bolder;width: 100%;text-align: right;height: 20px;">{{$datetime->format('d')}}</div>
                        @endif
                        @if(!empty($dataEmployeesDaily[$datetime->format('d')]))
                            @foreach($dataEmployeesDaily[$datetime->format('d')] as $key=>$value)
                                <div style="float: left;width: 100%;text-align: left;height: 20px;">
                                    @if($selectDay-1>intval($datetime->format('d')))
                                        <span style="color: #FF0000;float: left;width: 75%;">{{$value->employees_daily_work}}</span>
                                    @else
                                        <span style="float: left;width: 65%;">{{$value->employees_daily_work}}</span>
                                        <a href="/account/removeDaily?id={{$value->employees_daily_id}}" onclick="return confirm('ยืนยันการลบงานนี้')" style="float: left;padding: 1px 0px 0px 0px;color:#333;">
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </a>
                                    @endif
                                    <span data-toggle="tooltip" data-placement="top" title="{{$value->employees_daily_detail}}" style="float: left;padding: 2px" class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
                                    <span data-toggle="tooltip" data-placement="top" title="ชาย {{$value->employees_daily_man}} คน<br>หญิง {{$value->employees_daily_lady}} คน" style="float: left;padding: 2px" class="glyphicon glyphicon-user" aria-hidden="true"></span>
                                </div>
                            @endforeach
                        @endif
                    </td>
                    @if($datetime->format('w')==6)
                        </tr>
                    @endif
                    <span style="display: none;">{{$datetime->modify('+1 day')}}</span>
                @endfor
                </tbody>
            </table>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $(document).on('click',"#addDaily",function(){
                $('input#daily_day').val($(this).attr('day'));
            })
        })
    </script>

    <div class="modal fade @if (true==$errors->has('hour')||true==$errors->has('minute')||true==$errors->has('detail')) in @endif" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="@if (true==$errors->has('hour')||true==$errors->has('minute')||true==$errors->has('detail')) display: block; @endif">
        <div class="modal-dialog" role="document">
            <form action="/account/addDaily" method="post" class="form-horizontal" novalidate>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">เพิ่มกิจกรรม พนักงานรายวัน</h4>
                </div>
                <div class="modal-body">
                    @if (true==$errors->has('hour')||true==$errors->has('minute')||true==$errors->has('detail'))
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif
                    {{--<input type="hidden" class="form-control" value="" name="id">--}}
                    <input type="hidden" class="form-control" id="daily_day" name="day">
                        <div class="form-group" >
                            <label class="col-sm-2 control-label" for="exampleInputName2">โครงการ</label>
                            <div class="col-sm-10" style="padding-left: 0px;">
                            <select name="project_id" class="form-control">
                                <option value="">ไม่เลือกโครงการ</option>
                                @foreach($project as $key=>$value)
                                    @if(!empty($project_id) && $value->project_id==$project_id && $project_id!=='')
                                        <option selected value="{{$value->project_id}}">{{$value->project_name}}</option>
                                    @else
                                        <option value="{{$value->project_id}}">{{$value->project_name}}</option>
                                    @endif
                                @endforeach
                            </select>
                            </div>
                        </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="exampleInputName2">เวลา</label>
                        <div class="col-sm-1 @if ($errors->has('hour')) has-error @endif" style="padding: 0px;">
                            <select class="form-control" name="hour" style="padding: 0px;">
                                @foreach($selectHour as $key=>$value)
                                    <option value="{{$value}}">{{$value}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-1 @if ($errors->has('minute')) has-error @endif" style="padding: 0px;">
                            <select class="form-control" name="minute" style="padding: 0px;">
                                @foreach($selectMinute as $key=>$value)
                                    <option value="{{$value}}">{{$value}}</option>
                                @endforeach
                            </select>
                        </div>
                        <label style="padding-left: 0px;" class="col-sm-2 control-label" for="exampleInputName2">คนงานชาย</label>
                        <div class="col-sm-1" style="padding: 0px;">
                            <select class="form-control" name="man" style="padding: 0px;">
                                @for ($i = 0; $i <= 20; $i++)
                                    <option value="{{$i}}">{{$i}}</option>
                                @endfor
                            </select>
                        </div>
                        <label style="padding-left: 0px;" class="col-sm-2 control-label" for="exampleInputName2">คนงานหญิง</label>
                        <div class="col-sm-1" style="padding: 0px;">
                            <select class="form-control" name="lady" style="padding: 0px;">
                                @for ($i = 0; $i <= 20; $i++)
                                    <option value="{{$i}}">{{$i}}</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="exampleInputName2">งาน</label>
                        <div class="col-sm-10  @if ($errors->has('detail')) has-error @endif" style="padding-left: 0px;" >
                            <textarea class="form-control" name="detail" ></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
                    <input type="submit" class="btn btn-primary" value="เพิ่มงาน" />
                </div>
            </div>
            </form>
        </div>
    </div>
@stop