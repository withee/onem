@extends('layouts.layouts')
@section('content')
    <div class="row" style="padding: 10px 0px 10px 10px;">
        <div class="col-lg-9">
            <span style="font-weight: bolder;font-size: 24px;">ลงข้อมูลซ่อมบำรุง</span>
        </div>
        <div class="col-lg-3">
            <form method="POST" action="/account/search" class="form-inline">
                <div class="form-group">
                    {{--<label for="exampleInputName2">ค้นหา</label>--}}
                    <select name="month" class="form-control">
                        @foreach($monthNames as $key=>$value)
                            @if($key==$selectMonth && $value!=='')
                                <option selected value="{{$key}}">{{$value}}</option>
                            @elseif($value!=='')
                                <option value="{{$key}}">{{$value}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    {{--<label for="exampleInputEmail2">ปี</label>--}}
                    <select name="year" class="form-control">
                        <option @if ($selectYear=='2014')selected @endif value="2014">2014</option>
                        <option @if ($selectYear=='2015')selected @endif value="2015">2015</option>
                        <option @if ($selectYear=='2016')selected @endif value="2016">2016</option>
                        <option @if ($selectYear=='2017')selected @endif value="2017">2017</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
            </form>
        </div>
    </div>

    <style>
        .tt-query {
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        }

        .tt-hint {
            color: #999
        }

        .tt-menu {
            width: 422px;
            margin: 12px 0;
            padding: 8px 0;
            background-color: #fff;
            border: 1px solid #ccc;
            border: 1px solid rgba(0, 0, 0, 0.2);
            -webkit-border-radius: 8px;
            -moz-border-radius: 8px;
            border-radius: 8px;
            -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
            -moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
            box-shadow: 0 5px 10px rgba(0,0,0,.2);
        }

        .tt-suggestion {
            padding: 3px 20px;
            font-size: 18px;
            line-height: 24px;
        }

        .tt-suggestion:hover {
            cursor: pointer;
            color: #fff;
            background-color: #0097cf;
        }

        .tt-suggestion.tt-cursor {
            color: #fff;
            background-color: #0097cf;

        }

        .tt-suggestion p {
            margin: 0;
        }

        .gist {
            font-size: 14px;
        }
    </style>

    <script>
        $(document).ready(function () {
            var substringMatcher = function (strs) {
                return function findMatches(q, cb) {
                    var matches, substringRegex;

                    // an array that will be populated with substring matches
                    matches = [];

                    // regex used to determine if a string contains the substring `q`
                    substrRegex = new RegExp(q, 'i');

                    // iterate through the pool of strings and for any string that
                    // contains the substring `q`, add it to the `matches` array
                    $.each(strs, function (i, str) {
                        if (substrRegex.test(str)) {
                            matches.push(str);
                        }
                    });

                    cb(matches);
                };
            };
            $.ajax({
                url: "/account/checkRepair",
                type: "post",
                dataType: 'json'
            }).success(function (res) {
//                console.log(res);
                $('input#the-basics').typeahead({
                            hint: true,
                            highlight: true,
                            minLength: 1
                        },
                        {
                            name: 'states',
                            source: substringMatcher(res)
                        });
            })
        });
    </script>

    <div class="row">
        <div class="col-lg-12">
            <form method="POST" action="/account/addRepair" class="form-inline" novalidate>
                @if (true==$errors->has('name')||true==$errors->has('count')||true==$errors->has('type')||true==$errors->has('payee')||true==$errors->has('remark'))
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>
                        @endforeach
                    </div>
                @endif
                <input type="hidden" name="id" value="{{$accounts->id}}">
                <div class="form-group  @if ($errors->has('name')) has-error @endif">
                    <label for="exampleInputName2">อุปกรณ์</label>
                    <input id="the-basics" type="text" class="form-control" style="width: 320px;font-size: 18px;font-weight: bolder;" value="@if(!empty($accounts->income_list)){{$accounts->income_list}}@elseif(!empty($accounts->expenses_list)){{$accounts->expenses_list}}@elseif(!empty(Input::old('name'))){{Input::old('name')}}@endif" name="name" placeholder="อุปกรณ์">
                </div>
                {{--<div class="form-group  @if ($errors->has('storename')) has-error @endif">--}}
                    {{--<label for="exampleInputEmail2">คลัง</label>--}}
                    {{--<select class="form-control" name="storeName" style="width: 100px;padding-left: 4px;padding-right: 4px;">--}}
                        {{--@foreach($storeName as $key=>$value)--}}
                            {{--<option @if(Input::old('storename')==$value->storename_id)selected @elseif(!empty($accounts->storename))selected @endif value="{{$value->storename_id}}">{{$value->storename_name}}</option>--}}
                        {{--@endforeach--}}
                    {{--</select>--}}
                {{--</div>--}}
                <div class="form-group  @if ($errors->has('count')) has-error @endif">
                    <input type="text" class="form-control" style="width: 70px;" value="@if(!empty($accounts->count)){{$accounts->count}}@elseif(!empty($accounts->count)){{$accounts->count}}@elseif(!empty(Input::old('count'))){{Input::old('count')}}@endif" name="count" placeholder="ราคาซ่อม">
                </div>
                <div class="form-group  @if ($errors->has('symptom')) has-error @endif">
                    {{--<label for="exampleInputName2">อาการ</label>--}}
                    <input id="the-basics" type="text" class="form-control" style="width: 170px;" value="@if(!empty($accounts->symptom)){{$accounts->symptom}}@elseif(!empty($accounts->symptom)){{$accounts->symptom}}@elseif(!empty(Input::old('symptom'))){{Input::old('symptom')}}@endif" name="symptom" placeholder="อาการ">
                </div>

                {{--<div class="form-group  @if ($errors->has('type')) has-error @endif">--}}
                {{--<label for="exampleInputEmail2">ประเภท</label>--}}
                {{--<select class="form-control" name="type" style="width: 100px;padding-left: 4px;padding-right: 4px;">--}}
                {{--<option @if(Input::old('type')=='Bring')selected @elseif(!empty($accounts->expenses_list))selected @endif value="Bring">เบิกสินค้า</option>--}}
                {{--<option @if(Input::old('type')=='Income')selected @elseif(!empty($accounts->income_list))selected @endif value="Income">รับสินค้า</option>--}}
                {{--</select>--}}
                {{--</div>--}}
                {{--<div class="form-group  @if ($errors->has('project_id')) has-error @endif">--}}
                    {{--<label for="exampleInputEmail2">โครงการ</label>--}}
                    {{--<select class="form-control" name="project_id" style="width: 130px;padding-left: 4px;padding-right: 4px;">--}}
                        {{--<option value="">ไม่เลือกโครงการ</option>--}}
                        {{--@foreach($project as $key=>$value)--}}
                            {{--<option @if(Input::old('project_id')==$value->project_id)selected @elseif(!empty($accounts->project_id))selected @endif value="{{$value->project_id}}">{{$value->project_name}}</option>--}}
                        {{--@endforeach--}}
                    {{--</select>--}}
                {{--</div>--}}
                <div class="form-group  @if ($errors->has('payee')) has-error @endif">
                    <label for="exampleInputEmail2">ผู้ส่งซ่อม</label>
                    <select class="form-control" name="payee" style="width:150px;padding-left: 4px;padding-right: 4px;">
                        @foreach($staff as $key=>$value)
                            <option @if(Input::old('payee')==$value->staff_id)selected @elseif(!empty($accounts->payee))selected @endif value="{{$value->staff_username}}">{{$value->staff_firstname}} {{$value->staff_lastname}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group  @if ($errors->has('remark')) has-error @endif">
                    <input type="text" class="form-control" style="width: 100px;" value="@if(!empty($accounts->remark)){{$accounts->remark}}@elseif(!empty(Input::old('remark'))){{Input::old('remark')}}@endif" name="remark" placeholder="หมายเหตุ">
                </div>
                <button type="submit" class="btn btn-default">บันทึก</button>
            </form>
        </div>
    </div>
    <div style="display: none">{{$day=0}}</div>
    <div class="row" style="margin-top: 10px;">
        <div class="col-lg-12">
            <form method="post" action="/account/allRepair">
                <div style="width: 100%;float: left;padding: 5px;">
                    <input style="float: right;" class="btn btn-info" type="submit" value="ซ่อมเสร็จทั้งหมด">
                </div>
                <div style="clear: both;"></div>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th style="text-align: center;width: 160px;" >#</th>
                        <th style="text-align: center">วันที่</th>
                        <th style="text-align: center">ชื่อสินค้า</th>
                        <th style="text-align: center">อาการ</th>
                        <th style="text-align: center">ราคา</th>
                        <th style="text-align: right">สถานะ</th>
                        <th style="text-align: right">คงเหลือ</th>
                        <th style="text-align: center">ผู้ซ่อม</th>
                        <th style="text-align: center">หมายเหตุ</th>
                        <th style="text-align: center">จัดการ</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($dataAccounts as $key=>$value)
                        @if($key==0)
                            <div style="display: none">{{$day=$accounts->formatDate($value->created_at,'j')}}</div>
                        @endif
                        <tr style=" @if($value->repair_accounts_type=='repair')background-color:#fcd0d0;@endif @if(($day!=$accounts->formatDate($value->created_at,'j')))border-top: solid 2px;@endif">

                            @if($value->repair_accounts_type=='repair')
                                <td><input name="rentals[{{$value->repair_accounts_id}}]" value="{{$value->repair_accounts_id}}" type="checkbox" /></td>
                            @else
                                <td style="text-align: center;">{{$value->repair_accounts_id}}</td>
                            @endif
                            <td>{{$value->created_at}}</td>
                            <td>{{(!empty($value->repair_accounts_name)?$value->repair_accounts_name:'-')}}</td>
                            <td>{{$value->repair_accounts_symptom}}</td>
                            <td style="text-align: right">{{(!empty($value->repair_accounts_price)?number_format($value->repair_accounts_price, 2):'-')}}</td>
                            <td style="text-align: center;">
                                @if($value->repair_accounts_type=='repair')
                                    ส่งซ่อม
                                @elseif($value->repair_accounts_type=='return')
                                    ซ่อมเสร็จ
                                @elseif($value->repair_accounts_type=='waste')
                                    เสียหาย
                                @endif
                            </td>
                            <td style="text-align: right">{{(!empty($value->repair_accounts_balance)?number_format($value->repair_accounts_balance, 2):'0')}}</td>
                            <td style="text-align: center;">{{$value->payee}}</td>
                            <td>{{$value->remark}}</td>
                            <td>
                                @if($value->repair_accounts_type=='repair')
                                    <a href="/account/repairReturn?id={{$value->repair_accounts_id}}" onclick="return confirm('ยืนยันการซ่อมเสร็จ')" class="btn btn-info btn-xs">ซ่อมเสร็จ</a>
                                    <a href="/account/repairWaste?id={{$value->repair_accounts_id}}" onclick="return confirm('ยืนยันการสูญเสีย')" class="btn btn-warning btn-xs">สูญเสีย</a>
                                @endif
                                {{--<a href="/account/repair?id={{$value->repair_accounts_id}}" class="btn btn-primary btn-xs">แก้ไข</a>--}}
                                <a href="/account/repairRemove?id={{$value->repair_accounts_id}}" onclick="return confirm('ยืนยันการลบรายการ')"  class="btn btn-danger btn-xs">ลบ</a>

                            </td>
                        </tr>
                        @if ($day!=$accounts->formatDate($value->created_at,'j'))
                            <div style="display: none">{{$day=$accounts->formatDate($value->created_at,'j')}}</div>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </form>
        </div>
    </div>
@stop