@extends('layouts.layouts')
@section('content')
    <div class="row" style="padding: 10px 0px 10px 10px;">
        <div class="col-lg-9">
            <span style="font-weight: bolder;font-size: 24px;">ลงข้อมูลใบรายรับ-รายจ่าย</span>
        </div>
        <div class="col-lg-3">
            <form method="POST" action="/account/search" class="form-inline">
                <div class="form-group">
                    {{--<label for="exampleInputName2">ค้นหา</label>--}}
                    <select name="month" class="form-control">
                        @foreach($monthNames as $key=>$value)
                            @if($key==$selectMonth && $value!=='')
                                <option selected value="{{$key}}">{{$value}}</option>
                            @elseif($value!=='')
                                <option value="{{$key}}">{{$value}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    {{--<label for="exampleInputEmail2">ปี</label>--}}
                    <select name="year" class="form-control">
                        <option @if ($selectYear=='2014')selected @endif value="2014">2014</option>
                        <option @if ($selectYear=='2015')selected @endif value="2015">2015</option>
                        <option @if ($selectYear=='2016')selected @endif value="2016">2016</option>
                        <option @if ($selectYear=='2017')selected @endif value="2017">2017</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            @if($user->staff_usertype==5 || $user->staff_usertype==1 || $user->staff_usertype==2)
            <form method="POST" action="/account/addAccount" class="form-inline" novalidate>
                @if (true==$errors->has('name')||true==$errors->has('count')||true==$errors->has('type')||true==$errors->has('payee')||true==$errors->has('remark'))
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>
                        @endforeach
                    </div>
                @endif
                <input type="hidden" name="id" value="{{$accounts->id}}">
                <div class="form-group  @if ($errors->has('name')) has-error @endif">
                    <label for="exampleInputName2">รายการ</label>
                    <input type="text" class="form-control" style="width: 320px;" value="@if(!empty($accounts->income_list)){{$accounts->income_list}}@elseif(!empty($accounts->expenses_list)){{$accounts->expenses_list}}@elseif(!empty(Input::old('name'))){{Input::old('name')}}@endif" name="name" placeholder="รายการ">
                </div>
                <div class="form-group  @if ($errors->has('count')) has-error @endif">
                    <label for="exampleInputEmail2">ราคา</label>
                    <input type="text" class="form-control" style="width: 60px;" value="@if(!empty($accounts->income)){{$accounts->income}}@elseif(!empty($accounts->expenses)){{$accounts->expenses}}@elseif(!empty(Input::old('count'))){{Input::old('count')}}@endif" name="count" placeholder="ราคา">
                </div>
                <div class="form-group  @if ($errors->has('type')) has-error @endif">
                    <label for="exampleInputEmail2">ประเภท</label>
                    <select class="form-control" name="type">
                        <option @if(Input::old('type')=='Expenses')selected @elseif(!empty($accounts->expenses_list))selected @endif value="Expenses">รายจ่าย</option>
                        <option @if(Input::old('type')=='Income')selected @elseif(!empty($accounts->income_list))selected @endif value="Income">รายรับ</option>
                    </select>
                </div>
                    <div class="form-group  @if ($errors->has('project_id')) has-error @endif">
                        <label for="exampleInputEmail2">โครงการ</label>
                        <select class="form-control" name="project_id" style="width: 80px;">
                            @foreach($project as $key=>$value)
                                <option @if(Input::old('project_id')==$value->project_id)selected @elseif(!empty($accounts->project_id))selected @endif value="{{$value->project_id}}">{{$value->project_name}}</option>
                            @endforeach
                        </select>
                    </div>
                <div class="form-group  @if ($errors->has('payee')) has-error @endif">
                    <label for="exampleInputEmail2">ผู้ขอเบิก</label>
                    <select class="form-control" name="payee" style="width: 80px;">
                        @foreach($staff as $key=>$value)
                            <option @if(Input::old('payee')==$value->staff_username)selected @elseif(!empty($accounts->staff_username) and $accounts->staff_username==$value->staff_username)selected @endif value="{{$value->staff_username}}">{{$value->staff_firstname}} {{$value->staff_lastname}}</option>
                        @endforeach
                    </select>
                    {{--<input type="text" class="form-control" style="width: 100px;" value="@if(!empty($accounts->payee)){{$accounts->payee}}@elseif(!empty(Input::old('payee'))){{Input::old('payee')}}@endif" name="payee"/>--}}
                </div>
                <div class="form-group  @if ($errors->has('remark')) has-error @endif">
                    <label for="exampleInputEmail2">หมายเหตุ</label>
                    <input type="text" class="form-control" style="width: 100px;" value="@if(!empty($accounts->remark)){{$accounts->remark}}@elseif(!empty(Input::old('remark'))){{Input::old('remark')}}@endif" name="remark" placeholder="หมายเหตุ">
                </div>
                <button type="submit" class="btn btn-default">บันทึก</button>
            </form>
            @endif
        </div>
    </div>
<div style="display: none">{{$day=0}}</div>
    <div class="row" style="margin-top: 10px;">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th rowspan="2">#</th>
                    <th rowspan="2" style="text-align: center;width: 160px;">วันที่</th>
                    <th colspan="2" style="text-align: center">รายรับ</th>
                    <th colspan="2" style="text-align: center">รายจ่าย</th>
                    <th rowspan="2" style="text-align: right">คงเหลือ</th>
                    <th rowspan="2" style="text-align: center">ผู้เบิก</th>
                    <th rowspan="2" style="text-align: center">หมายเหตุ</th>
                    <th rowspan="2" style="text-align: center">จัดการ</th>
                </tr>
                <tr>
                    <th>รายการ</th>
                    <th style="text-align: right">จำนวน</th>
                    <th>รายการ</th>
                    <th style="text-align: right">จำนวน</th>
                </tr>
                </thead>
                <tbody>
                @foreach($dataAccounts as $key=>$value)
                    @if($key==0)
                        <div style="display: none">{{$day=$accounts->formatDate($value->created_at,'j')}}</div>
                    @endif
                    <tr style="@if(($day!=$accounts->formatDate($value->created_at,'j')))border-top: solid 2px;@endif @if(!empty($value->deleted_at))color:#FF0000;@endif">
                        <td>{{$value->id}}</td>
                        <td>{{$value->created_at}}</td>
                        <td>{{(!empty($value->income_list)?$value->income_list:'-')}}</td>
                        <td style="text-align: right">
                            @if($user->staff_usertype==5 || $user->staff_usertype==1 || $user->staff_usertype==2)
                                {{((!empty($value->income))?number_format($value->income, 2):'-')}}
                            @else
                                -
                            @endif
                        </td>
                        <td>{{(!empty($value->expenses_list)?$value->expenses_list:'-')}}</td>
                        <td style="text-align: right">
                            @if($user->staff_usertype==5 || $user->staff_usertype==1 || $user->staff_usertype==2)
                                {{(!empty($value->expenses)?number_format($value->expenses, 2):'-')}}
                            @else
                                -
                            @endif
                        </td>
                        <td style="text-align: right;font-weight: bolder;">
                            @if($user->staff_usertype==5 || $user->staff_usertype==1 || $user->staff_usertype==2)
                                {{number_format($value->balance, 2)}}
                            @else
                                -
                            @endif
                        </td>
                        <td>{{$value->payee}}</td>
                        <td>{{$value->remark}}</td>
                        <td>
                            {{--<a href="/account/manage?id={{$value->id}}" class="btn btn-primary btn-xs">แก้ไข</a>--}}
                            @if(empty($value->deleted_at))
                                <a href="/account/remove?id={{$value->id}}" onclick="return confirm('ยืนยันการลบรายการ')"  class="btn btn-danger btn-xs">ลบ</a>
                            @endif
                        </td>
                    </tr>
                    @if ($day!=$accounts->formatDate($value->created_at,'j'))
                        <div style="display: none">{{$day=$accounts->formatDate($value->created_at,'j')}}</div>
                    @endif
                @endforeach
                @if($user->staff_usertype==5 || $user->staff_usertype==1 || $user->staff_usertype==2)
                    <tr style="font-weight: bold;border-top: double 3px#000000;">
                        <td colspan="2">รวม</td>
                        <td colspan="2" style="text-align: right;">{{$sumIncome}}</td>
                        <td colspan="2" style="text-align: right;">{{$sumExpenses}}</td>
                        <td style="text-align: right;">{{$sumIncome-$sumExpenses}}</td>
                        <td colspan="3"></td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>

@stop