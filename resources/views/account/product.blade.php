@extends('layouts.layouts')
@section('content')
    <div class="row" style="padding: 10px 0px 10px 10px;">
        <div class="col-lg-3">
            <span style="font-weight: bolder;font-size: 24px;">ลงข้อมูลเบิกจ่ายสินค้า</span>
        </div>
        <div class="col-lg-9">
            <form method="POST" action="/account/searchProduct" class="form-inline">
                <div class="form-group">
                    <select name="project_id"  class="form-control">
                        <option value="">ไม่เลือกโครงการ</option>
                        @foreach($project as $key=>$value)
                            @if(!empty($project_id) && $value->project_id==$project_id && $project_id!=='')
                                <option selected value="{{$value->project_id}}">{{$value->project_name}}</option>
                            @else
                                <option value="{{$value->project_id}}">{{$value->project_name}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <select name="product_id" class="form-control">
                        <option value="">ไม่เลือกวัสถุดิบ</option>
                        @foreach($product as $key=>$value)
                            @if(!empty($product_id) && $value->product_id==$product_id && $product_id!=='')
                                <option selected value="{{$value->product_id}}">{{$value->product_name}}</option>
                            @else
                                <option value="{{$value->product_id}}">{{$value->product_name}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <select name="month" class="form-control">
                        <option value="">ไม่เลือกเดือน</option>
                        @foreach($monthNames as $key=>$value)
                            @if($key==$selectMonth && $value!=='')
                                <option selected value="{{$key}}">{{$value}}</option>
                            @elseif($value!=='')
                                <option value="{{$key}}">{{$value}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    {{--<label for="exampleInputEmail2">ปี</label>--}}
                    <select name="year" class="form-control">
                        <option @if ($selectYear=='2014')selected @endif value="2014">2014</option>
                        <option @if ($selectYear=='2015')selected @endif value="2015">2015</option>
                        <option @if ($selectYear=='2016')selected @endif value="2016">2016</option>
                        <option @if ($selectYear=='2017')selected @endif value="2017">2017</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
            </form>
        </div>
    </div>
    <style>
        .tt-query {
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        }

        .tt-hint {
            color: #999
        }

        .tt-menu {
            width: 422px;
            margin: 12px 0;
            padding: 8px 0;
            background-color: #fff;
            border: 1px solid #ccc;
            border: 1px solid rgba(0, 0, 0, 0.2);
            -webkit-border-radius: 8px;
            -moz-border-radius: 8px;
            border-radius: 8px;
            -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
            -moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
            box-shadow: 0 5px 10px rgba(0,0,0,.2);
        }

        .tt-suggestion {
            padding: 3px 20px;
            font-size: 18px;
            line-height: 24px;
        }

        .tt-suggestion:hover {
            cursor: pointer;
            color: #fff;
            background-color: #0097cf;
        }

        .tt-suggestion.tt-cursor {
            color: #fff;
            background-color: #0097cf;

        }

        .tt-suggestion p {
            margin: 0;
        }

        .gist {
            font-size: 14px;
        }
    </style>

    <script>
        $(document).ready(function () {
            var substringMatcher = function (strs) {
                return function findMatches(q, cb) {
                    var matches, substringRegex;

                    // an array that will be populated with substring matches
                    matches = [];

                    // regex used to determine if a string contains the substring `q`
                    substrRegex = new RegExp(q, 'i');

                    // iterate through the pool of strings and for any string that
                    // contains the substring `q`, add it to the `matches` array
                    $.each(strs, function (i, str) {
                        if (substrRegex.test(str)) {
                            matches.push(str);
                        }
                    });

                    cb(matches);
                };
            };

            $.ajax({
                url: "/account/checkProduct",
                type: "post",
                dataType: 'json'
            }).success(function (res) {
                $('input#the-basics').typeahead({
                            hint: true,
                            highlight: true,
                            minLength: 1
                        },
                        {
                            name: 'states',
                            source: substringMatcher(res)
                        });
            })
        });
    </script>

    <div class="row">
        <div class="col-lg-12">
            @if($user->staff_usertype==6 ||$user->staff_usertype==5 || $user->staff_usertype==1 || $user->staff_usertype==2)
            <form method="POST" action="/account/addProduct" class="form-inline" novalidate>
                @if (true==$errors->has('name')||true==$errors->has('count')||true==$errors->has('type')||true==$errors->has('payee')||true==$errors->has('remark'))
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>
                        @endforeach
                    </div>
                @endif
                <input type="hidden" name="id" value="{{$accounts->id}}">
                <div class="form-group  @if ($errors->has('name')) has-error @endif">
                    <label for="exampleInputName2">รายการ</label>
                    <input id="the-basics" type="text" class="form-control" style="width: 320px;font-size: 18px;font-weight: bolder;" value="@if(!empty($accounts->income_list)){{$accounts->income_list}}@elseif(!empty($accounts->expenses_list)){{$accounts->expenses_list}}@elseif(!empty(Input::old('name'))){{Input::old('name')}}@endif" name="name" placeholder="รายการ">
                </div>
                    <div class="form-group  @if ($errors->has('storename')) has-error @endif">
                        <label for="exampleInputEmail2">คลัง</label>
                        <select class="form-control" name="storeName" style="width: 80px;padding-left: 4px;padding-right: 4px;">
                            @foreach($storeName as $key=>$value)
                                <option @if(Input::old('storename')==$value->storename_id)selected @elseif(!empty($accounts->storename))selected @endif value="{{$value->storename_id}}">{{$value->storename_name}}</option>
                            @endforeach
                        </select>
                    </div>
                <div class="form-group  @if ($errors->has('count')) has-error @endif">
                    {{--<label for="exampleInputEmail2">จำนวน</label>--}}
                    <input type="text" class="form-control" style="width: 70px;" value="@if(!empty($accounts->income)){{$accounts->income}}@elseif(!empty($accounts->expenses)){{$accounts->expenses}}@elseif(!empty(Input::old('count'))){{Input::old('count')}}@endif" name="count" placeholder="จำนวน">
                </div>
                <div class="form-group  @if ($errors->has('type')) has-error @endif">
                    <label for="exampleInputEmail2">ประเภท</label>
                    <select class="form-control" name="type" style="width: 100px;padding-left: 4px;padding-right: 4px;">
                        <option @if(Input::old('type')=='Bring')selected @elseif(!empty($accounts->expenses_list))selected @endif value="Bring">เบิกสินค้า</option>
                        <option @if(Input::old('type')=='Income')selected @elseif(!empty($accounts->income_list))selected @endif value="Income">รับสินค้า</option>
                    </select>
                </div>
                    <div class="form-group  @if ($errors->has('project_id')) has-error @endif">
                        <label for="exampleInputEmail2">โครงการ</label>
                        <select class="form-control" name="project_id" style="width: 80px;padding-left: 4px;padding-right: 4px;">
                            @foreach($project as $key=>$value)
                                <option @if(Input::old('project_id')==$value->project_id)selected @elseif(!empty($accounts->project_id))selected @endif value="{{$value->project_id}}">{{$value->project_name}}</option>
                            @endforeach
                        </select>
                    </div>
                <div class="form-group  @if ($errors->has('payee')) has-error @endif">
                    <label for="exampleInputEmail2">ผู้เบิก</label>
                    <select class="form-control" name="payee" style="width: 80px;padding-left: 4px;padding-right: 4px;">
                        @foreach($staff as $key=>$value)
                            <option @if(Input::old('payee')==$value->staff_id)selected @elseif(!empty($accounts->payee))selected @endif value="{{$value->staff_username}}">{{$value->staff_firstname}} {{$value->staff_lastname}}</option>
                        @endforeach
                    </select>
                    {{--<input type="text" class="form-control" style="width: 100px;" value="@if(!empty($accounts->payee)){{$accounts->payee}}@elseif(!empty(Input::old('payee'))){{Input::old('payee')}}@endif" name="payee"/>--}}
                </div>
                <div class="form-group  @if ($errors->has('remark')) has-error @endif">
                    {{--<label for="exampleInputEmail2">หมายเหตุ</label>--}}
                    <input type="text" class="form-control" style="width: 100px;" value="@if(!empty($accounts->remark)){{$accounts->remark}}@elseif(!empty(Input::old('remark'))){{Input::old('remark')}}@endif" name="remark" placeholder="หมายเหตุ">
                </div>
                <button type="submit" class="btn btn-default">บันทึก</button>
            </form>
            @endif
        </div>
    </div>
<div style="display: none">{{$day=0}}</div>
    <div class="row" style="margin-top: 10px;">
        <form method="POST" action="/account/addProductPrice">
        <div class="col-lg-12">
            @if($user->staff_usertype==6 ||$user->staff_usertype==5 || $user->staff_usertype==1 || $user->staff_usertype==2)
            <input style="float: right;margin: 5px;" type="submit" class="btn btn-warning" value="บันทึกราคาทุน">
            @endif
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th rowspan="2">#</th>
                    <th rowspan="2" style="text-align: center;width: 160px;">วันที่</th>
                    <th colspan="2" style="text-align: center">รับสินค้า</th>
                    <th colspan="2" style="text-align: center">เบิกสินค้า</th>
                    <th rowspan="2" style="text-align: right">คงเหลือ</th>
                    <th rowspan="2" style="text-align: center">ผู้เบิก</th>
                    <th rowspan="2" style="text-align: center">หมายเหตุ</th>
                    <th rowspan="2" style="text-align: center;width:100px;">จัดการ</th>
                </tr>
                <tr>
                    <th>รายการ</th>
                    <th style="text-align: right">จำนวน</th>
                    <th>รายการ</th>
                    <th style="text-align: right">จำนวน</th>
                </tr>
                </thead>
                <tbody>
                @foreach($dataAccounts as $key=>$value)
                    @if($key==0)
                        <div style="display: none">{{$day=$accounts->formatDate($value->created_at,'j')}}</div>
                    @endif
                    <tr style="@if(($day!=$accounts->formatDate($value->created_at,'j')))border-top: solid 2px;@endif @if(!empty($value->deleted_at))color:#FF0000;@endif">
                        <td>{{$value->id}}</td>
                        <td>{{$value->created_at}}</td>
                        <td>{{(!empty($value->income_product_id)?$value->income_product_id:'-')}}</td>
                        <td style="text-align: right">{{(!empty($value->income)?number_format($value->income, 2):'-')}}</td>
                        <td>{{(!empty($value->expenses_product_id)?$value->expenses_product_id:'-')}}</td>
                        <td style="text-align: right">{{(!empty($value->expenses)?number_format($value->expenses, 2):'-')}}</td>
                        <td style="text-align: right;font-weight: bolder;">{{number_format($value->balance, 2)}}</td>
                        <td style="text-align: center;">{{$value->payee}}</td>
                        <td>{{$value->remark}}</td>
                        <td>
                            @if($user->staff_usertype==6 ||$user->staff_usertype==5 || $user->staff_usertype==1 || $user->staff_usertype==2)
                            <a style="float: left;" href="/account/productRemove?id={{$value->id}}" onclick="return confirm('ยืนยันการลบรายการ')"  class="btn btn-danger btn-xs">ลบ</a>
                                @if($user->staff_usertype==5 || $user->staff_usertype==1 || $user->staff_usertype==2)
                                    @if(empty($value->price) && !empty($value->income_product_id))
                                        <input style="padding:2px 5px;margin-left: 5px;float:left;height:22px;width: 40px;"  type="text" class="form-control input-sm" name="price[{{$value->id}}]" value="0" >
                                    @endif
                                @endif
                            @endif
                        </td>
                    </tr>
                    @if ($day!=$accounts->formatDate($value->created_at,'j'))
                        <div style="display: none">{{$day=$accounts->formatDate($value->created_at,'j')}}</div>
                    @endif
                @endforeach
                {{--<tr style="font-weight: bold;border-top: double 3px#000000;">--}}
                    {{--<td colspan="2">รวม</td>--}}
                    {{--<td colspan="2" style="text-align: right;">{{$sumIncome}}</td>--}}
                    {{--<td colspan="2" style="text-align: right;">{{$sumExpenses}}</td>--}}
                    {{--<td style="text-align: right;">{{$sumIncome-$sumExpenses}}</td>--}}
                    {{--<td colspan="3"></td>--}}
                {{--</tr>--}}
                </tbody>
            </table>
        </div>
        </form>
    </div>
@stop