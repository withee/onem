@extends('layouts.layouts')
@section('content')
    <div class="row" style="padding: 10px 0px 10px 10px;">
        <div class="col-lg-9">
            <span style="font-weight: bolder;font-size: 24px;">ลงข้อมูลเช่ายืม-อุปกรณ์</span>
        </div>
        <div class="col-lg-3">
            <form method="GET" action="/account/rentals" class="form-inline">
                {{--<div class="form-group">--}}
                    {{--<label for="exampleInputName2">ค้นหา</label>--}}
                    {{--<select name="type" class="form-control">--}}
                        {{--<option value="">ไม่เลือก</option>--}}
                        {{--<option {{(($type!='' and $type=='borrow')?'selected':'')}} value="borrow">ยืม</option>--}}
                        {{--<option {{(($type!='' and $type=='return')?'selected':'')}} value="return">คืน</option>--}}
                    {{--</select>--}}
                {{--</div>--}}
                <div class="form-group">
                    {{--<label for="exampleInputName2">ค้นหา</label>--}}
                    <select name="month" class="form-control">
                        @foreach($monthNames as $key=>$value)
                            @if($key==$selectMonth && $value!=='')
                                <option selected value="{{$key}}">{{$value}}</option>
                            @elseif($value!=='')
                                <option value="{{$key}}">{{$value}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    {{--<label for="exampleInputEmail2">ปี</label>--}}
                    <select name="year" class="form-control">
                        <option @if ($selectYear=='2014')selected @endif value="2014">2014</option>
                        <option @if ($selectYear=='2015')selected @endif value="2015">2015</option>
                        <option @if ($selectYear=='2016')selected @endif value="2016">2016</option>
                        <option @if ($selectYear=='2017')selected @endif value="2017">2017</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
            </form>
        </div>
    </div>

    <style>
        .tt-query {
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        }

        .tt-hint {
            color: #999
        }

        .tt-menu {
            width: 422px;
            margin: 12px 0;
            padding: 8px 0;
            background-color: #fff;
            border: 1px solid #ccc;
            border: 1px solid rgba(0, 0, 0, 0.2);
            -webkit-border-radius: 8px;
            -moz-border-radius: 8px;
            border-radius: 8px;
            -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
            -moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
            box-shadow: 0 5px 10px rgba(0,0,0,.2);
        }

        .tt-suggestion {
            padding: 3px 20px;
            font-size: 18px;
            line-height: 24px;
        }

        .tt-suggestion:hover {
            cursor: pointer;
            color: #fff;
            background-color: #0097cf;
        }

        .tt-suggestion.tt-cursor {
            color: #fff;
            background-color: #0097cf;

        }

        .tt-suggestion p {
            margin: 0;
        }

        .gist {
            font-size: 14px;
        }
    </style>

    <script>
        $(document).ready(function () {

            $(document).on('change',"#selectType",function(){
//                console.log($(this).val());
                if($(this).val()=='borrow' || $(this).val()=='return' || $(this).val()=='dilapidated'){
                    if($(this).val()=='borrow') {
                        $('tr.type_borrow').show();
                        $('tr.type_return').hide();
                        $('tr.type_dilapidated').hide();
                    }else if($(this).val()=='return'){
                        $('tr.type_return').show();
                        $('tr.type_borrow').hide();
                        $('tr.type_dilapidated').hide();
                    }else if($(this).val()=='dilapidated'){
                        $('tr.type_dilapidated').show();
                        $('tr.type_return').hide();
                        $('tr.type_borrow').hide();
                    }
                }else{
                    $('tr.type_return').show();
                    $('tr.type_borrow').show();
                    $('tr.type_dilapidated').show();
                }
            })

            var substringMatcher = function (strs) {
                return function findMatches(q, cb) {
                    var matches, substringRegex;

                    // an array that will be populated with substring matches
                    matches = [];

                    // regex used to determine if a string contains the substring `q`
                    substrRegex = new RegExp(q, 'i');

                    // iterate through the pool of strings and for any string that
                    // contains the substring `q`, add it to the `matches` array
                    $.each(strs, function (i, str) {
                        if (substrRegex.test(str)) {
                            matches.push(str);
                        }
                    });

                    cb(matches);
                };
            };
            $.ajax({
                url: "/account/checkEquipment",
                type: "post",
                dataType: 'json'
            }).success(function (res) {
//                console.log(res);
                $('input#the-basics').typeahead({
                            hint: true,
                            highlight: true,
                            minLength: 1
                        },
                        {
                            name: 'states',
                            source: substringMatcher(res)
                        });
            })
        });
    </script>

    <div class="row">
        <div class="col-lg-12">
            @if($user->staff_usertype==6 || $user->staff_usertype==1 || $user->staff_usertype==2)
            <form method="POST" action="/account/addRentals" class="form-inline" novalidate>
                @if (true==$errors->has('name')||true==$errors->has('count')||true==$errors->has('type')||true==$errors->has('payee')||true==$errors->has('remark'))
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>
                        @endforeach
                    </div>
                @endif
                <input type="hidden" name="id" value="{{$accounts->id}}">
                <div class="form-group  @if ($errors->has('name')) has-error @endif">
                    <label for="exampleInputName2">รายการ</label>
                    <input id="the-basics" type="text" class="form-control" style="width: 320px;font-size: 18px;font-weight: bolder;" value="@if(!empty($accounts->income_list)){{$accounts->income_list}}@elseif(!empty($accounts->expenses_list)){{$accounts->expenses_list}}@elseif(!empty(Input::old('name'))){{Input::old('name')}}@endif" name="name" placeholder="รายการ">
                </div>
                    <div class="form-group  @if ($errors->has('storename')) has-error @endif">
                        <label for="exampleInputEmail2">คลัง</label>
                        <select class="form-control" name="storeName" style="width: 100px;padding-left: 4px;padding-right: 4px;">
                            @foreach($storeName as $key=>$value)
                                <option @if(Input::old('storename')==$value->storename_id)selected @elseif(!empty($accounts->storename))selected @endif value="{{$value->storename_id}}">{{$value->storename_name}}</option>
                            @endforeach
                        </select>
                    </div>
                <div class="form-group  @if ($errors->has('count')) has-error @endif">
                    <input type="text" class="form-control" style="width: 70px;" value="@if(!empty($accounts->income)){{$accounts->income}}@elseif(!empty($accounts->expenses)){{$accounts->expenses}}@elseif(!empty(Input::old('count'))){{Input::old('count')}}@endif" name="count" placeholder="จำนวน">
                </div>
                {{--<div class="form-group  @if ($errors->has('type')) has-error @endif">--}}
                    {{--<label for="exampleInputEmail2">ประเภท</label>--}}
                    {{--<select class="form-control" name="type" style="width: 100px;padding-left: 4px;padding-right: 4px;">--}}
                        {{--<option @if(Input::old('type')=='Bring')selected @elseif(!empty($accounts->expenses_list))selected @endif value="Bring">เบิกสินค้า</option>--}}
                        {{--<option @if(Input::old('type')=='Income')selected @elseif(!empty($accounts->income_list))selected @endif value="Income">รับสินค้า</option>--}}
                    {{--</select>--}}
                {{--</div>--}}
                    <div class="form-group  @if ($errors->has('project_id')) has-error @endif">
                        <label for="exampleInputEmail2">โครงการ</label>
                        <select class="form-control" name="project_id" style="width: 130px;padding-left: 4px;padding-right: 4px;">
                            <option value="">ไม่เลือกโครงการ</option>
                            @foreach($project as $key=>$value)
                                <option @if(Input::old('project_id')==$value->project_id)selected @elseif(!empty($accounts->project_id))selected @endif value="{{$value->project_id}}">{{$value->project_name}}</option>
                            @endforeach
                        </select>
                    </div>
                <div class="form-group  @if ($errors->has('payee')) has-error @endif">
                    <label for="exampleInputEmail2">ผู้เบิก</label>
                    <select class="form-control" name="payee" style="width:150px;padding-left: 4px;padding-right: 4px;">
                        @foreach($staff as $key=>$value)
                            <option @if(Input::old('payee')==$value->staff_id)selected @elseif(!empty($accounts->payee))selected @endif value="{{$value->staff_username}}">{{$value->staff_firstname}} {{$value->staff_lastname}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group  @if ($errors->has('remark')) has-error @endif">
                    <input type="text" class="form-control" style="width: 100px;" value="@if(!empty($accounts->remark)){{$accounts->remark}}@elseif(!empty(Input::old('remark'))){{Input::old('remark')}}@endif" name="remark" placeholder="หมายเหตุ">
                </div>
                <button type="submit" class="btn btn-default">บันทึก</button>
            </form>
                @endif
        </div>
    </div>
<div style="display: none">{{$day=0}}</div>
    <div class="row" style="margin-top: 10px;">
        <div class="col-lg-12">
            <form method="post" action="/account/allReturn">
            <div style="width: 100%;float: left;padding: 5px;">
                <select style="margin-left: 5px;float: right;width: 100px;" id="selectType" class="form-control">
                    <option value="all">ทั้งหมด</option>
                    <option value="borrow">ยืม</option>
                    <option value="return">คืน</option>
                    <option value="dilapidated">ชำรุด</option>
                </select>
                @if($user->staff_usertype==6 || $user->staff_usertype==1 || $user->staff_usertype==2)
                <input style="float: right;" class="btn btn-warning" type="submit" value="คืนสินค้าทั้งหมด">
                @endif
            </div>
            <div style="clear: both;"></div>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th style="text-align: center;" >#</th>
                    <th style="text-align: center;width: 160px;">วันที่</th>
                    <th style="text-align: center">ชื่อสินค้า</th>
                    <th style="text-align: center">จำนวน</th>
                    <th style="text-align: right">สถานะ</th>
                    <th style="text-align: right">คงเหลือ</th>
                    <th style="text-align: center">ผู้เบิก</th>
                    <th style="text-align: center">หมายเหตุ</th>
                    <th style="text-align: center">จัดการ</th>
                </tr>
                </thead>
                <tbody>
                @foreach($dataAccounts as $key=>$value)
                    @if($key==0)
                        <div style="display: none">{{$day=$accounts->formatDate($value->created_at,'j')}}</div>
                    @endif
                    <tr class="type_{{$value->product_rentals_type}}" style=" @if($value->product_rentals_type=='borrow')background-color:#fcd0d0;@endif @if(($day!=$accounts->formatDate($value->created_at,'j')))border-top: solid 2px;@endif">

                    @if($value->product_rentals_type=='borrow' and ($user->staff_usertype==6 || $user->staff_usertype==1 || $user->staff_usertype==2))
                        <td><input name="rentals[{{$value->product_rentals_id}}]" value="{{$value->product_rentals_id}}" type="checkbox" /></td>
                    @else
                        <td style="text-align: center;">{{$value->product_rentals_id}}</td>
                    @endif
                        <td>{{$value->created_at}}</td>
                        <td>{{(!empty($value->product_name)?$value->product_name:'-')}}</td>
                        <td style="text-align: right">{{(!empty($value->product_rentals_count)?number_format($value->product_rentals_count, 2):'-')}}</td>
                        <td style="text-align: center;">
                            @if($value->product_rentals_type=='borrow')
                                ยืม
                            @elseif($value->product_rentals_type=='return')
                                คืนเเล้ว
                            @elseif($value->product_rentals_type=='dilapidated')
                                ชำรุด
                            @endif
                        </td>
                        <td style="text-align: right">{{(!empty($value->product_rentals_balance)?number_format($value->product_rentals_balance, 2):'0')}}</td>
                        <td style="text-align: center;">{{$value->payee}}</td>
                        <td>{{$value->remark}}</td>
                        <td>
                            @if($user->staff_usertype==6 || $user->staff_usertype==1 || $user->staff_usertype==2)
                                @if($value->product_rentals_type=='borrow')
                                    <a href="/account/rentalsReturn?id={{$value->product_rentals_id}}" class="btn btn-info btn-xs">คืน</a>
                                    <a href="/account/rentalsDilapidated?id={{$value->product_rentals_id}}"onclick="return confirm('ยืนยันการส่งคลังชำรุด')" class="btn btn-warning btn-xs">ส่งคลังชำรุด</a>
                                @endif
                                {{--<a href="/account/rentals?id={{$value->product_rentals_id}}" class="btn btn-primary btn-xs">แก้ไข</a>--}}
                                <a href="/account/rentalsRemove?id={{$value->product_rentals_id}}" onclick="return confirm('ยืนยันการลบรายการ')"  class="btn btn-danger btn-xs">ลบ</a>
                            @endif
                        </td>
                    </tr>
                    @if ($day!=$accounts->formatDate($value->created_at,'j'))
                        <div style="display: none">{{$day=$accounts->formatDate($value->created_at,'j')}}</div>
                    @endif
                @endforeach
                </tbody>
            </table>
            </form>
        </div>
    </div>
@stop