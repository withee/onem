@extends('layouts.layouts')
@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h3>ปรับปรุงสินค้าในคลัง</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12" >
            <button style="float: right;margin-bottom: 10px;" type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                จัดการ
            </button>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th>วันที่</th>
                    <th>ชื่อสินค้า</th>
                    <th>ชื่อคลัง</th>
                    <th>ผู้แเก้ไข</th>
                    <th>จำนวน</th>
                    <th>ประเภท</th>
                </tr>
                </thead>
                <tbody>
                @if(false==empty($dataStore))
                    @foreach($dataStore as $key=>$value)
                        <tr>
                            <th scope="row">{{$key+1}}</th>
                            <td>{{$value->created_at}}</td>
                            <td>{{$value->product_name}}</td>
                            <td>{{$value->storename_name}}</td>
                            <td>{{$value->staff_firstname}} {{$value->staff_lastname}}</td>
                            <td>{{$value->log_stores_count}}</td>
                            <td>
                                @if($value->log_stores_type=='minus')
                                    'ลด'
                                @endif
                                @if($value->log_stores_type=='plus')
                                    'เพิ่ม'
                                @endif
                                @if($value->log_stores_type=='new')
                                    'ใหม่'
                                @endif
                                {{--{{$value->log_stores_type}}--}}
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>

    {{--<div class="row">--}}
        <div class="modal fade @if(true==$errors->has('product_id')||true==$errors->has('storename_id')||true==$errors->has('id_member')||true==$errors->has('log_stores_count')||true==$errors->has('log_stores_type')||true==$errors->has('log_return_detail')) in @endif" style="@if(true==$errors->has('product_id')||true==$errors->has('storename_id')||true==$errors->has('id_member')||true==$errors->has('log_stores_count')||true==$errors->has('log_stores_type')||true==$errors->has('log_return_detail')) display: block; @endif" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <form method="POST" action="/store/addLogStore">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">ปรับปรุงสินค้า</h4>
                        </div>
                        <div class="modal-body">
                            @if ($errors->has())
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        {{ $error }}<br>
                                    @endforeach
                                </div>
                            @endif
                            <div class="form-horizontal">
                                {{--<div class="form-group @if($errors->has('id_member')) has-error @endif">--}}
                                    {{--<label for="inputPassword3" class="col-lg-4 control-label">ผู้ปรับปรุงสินค้า</label>--}}
                                    {{--<div class="col-lg-8">--}}
                                        {{--<select class="form-control" name="id_member">--}}
                                            {{--<option value="">เลือกผู้ปรับปรุงสินค้า</option>--}}
                                            {{--@foreach($dataStaff as $key=>$value)--}}
                                                {{--<option  @if (Input::old('id_member')==$value->staff_id) selected @endif value="{{$value->staff_id}}">{{$value->staff_firstname}} {{$value->staff_lastname}}</option>--}}
                                            {{--@endforeach--}}
                                        {{--</select>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                <div class="form-group @if ($errors->has('product_id')) has-error @endif">
                                    <label for="exampleInputName2" class="col-lg-4 control-label">ชื่อสินค้า</label>
                                    <div class="col-lg-8">
                                        <select name="product_id" class="form-control">
                                            @foreach($dataProduct as $key=>$value)
                                                <option @if (Input::old('product_id')==$value->product_id) selected @endif value="{{$value->product_id}}">{{$value->product_name}} ({{$value->unit_name}})</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group @if ($errors->has('storename_id')) has-error @endif">
                                    <label for="exampleInputName2" class="col-lg-4 control-label">ลงคลัง</label>
                                    <div class="col-lg-8">
                                        <select name="storename_id" class="form-control">
                                            @foreach($dataStoreName as $key=>$value)
                                                <option @if (Input::old('storename_id')==$value['storename_id']) selected @endif value="{{$value->storename_id}}">{{$value->storename_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group @if($errors->has('log_stores_count')) has-error @endif">
                                    <label for="inputEmail3" class="col-lg-4 control-label">จำนวน</label>
                                    <div class="col-lg-8">
                                        <input type="text" value="@if(false==empty(Input::old('log_stores_count'))){{Input::old('log_stores_count')}}@endif" name="log_stores_count" class="form-control" id="inputEmail3" placeholder="จำนวน">
                                    </div>
                                </div>
                                <div class="form-group @if($errors->has('log_stores_type')) has-error @endif">
                                    <label for="inputPassword3" class="col-lg-4 control-label">ประเภท</label>
                                    <div class="col-lg-8">
                                        <select class="form-control" name="log_stores_type">
                                            <option value="">เลือกประเภท</option>
                                            <option @if (Input::old('log_stores_type')=='plus') selected @endif value="plus">เพิ่ม</option>
                                            <option @if (Input::old('log_stores_type')=='minus') selected @endif value="minus">ลบ</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group @if($errors->has('log_return_detail')) has-error @endif">
                                    <label class="col-lg-3 control-label" for="exampleInputEmail1">รายละเอียด</label>
                                    <div class="col-lg-9" style="">
                                        <textarea rows="4" class="form-control" value="@if(false==empty(Input::old('log_return_detail'))){{Input::old('log_return_detail')}}@endif" name="log_return_detail" placeholder="รายละเอียด"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">บันทึก</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    {{--</div>--}}

@stop