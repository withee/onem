@extends('layouts.layouts')
@section('content')

    <div class="row" style="margin-top: 10px;">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li><a href="/storeName/manage">จัดการคลัง</a></li>
                <li class="active">คลัง</li>
            </ol>
        </div>
    </div>

    <div class="row" style="">
        <div class="col-lg-3" style="">
            <form style="float: left;" class="form-inline" method="GET" action="/store/search?id=@if(false==empty($id)){{$id}}@endif" novalidate>
                <div class="input-group input-group-sm">
                    <span class="input-group-addon" ><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></span>
                    <input type="hidden" name="id" value="{{$product['id']}}">
                    <input type="text" class="form-control" name="search" value="{{$product['search']}}" placeholder="ค้นหา ชื่อสินค้า">
                </div>
                <button type="submit"  class="btn btn-sm"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
            </form>

        </div>
        <div class="col-lg-9"></div>
    </div>

    <div class="row" style="float: right;">
        <div class="col-lg-12">

            <nav>
                <ul class="pagination" style="margin: 0px;padding: 0px;">
                    @if($product['page']>1)
                    <li><a href="/store/view?id={{$product['id']}}&page={{$product['page']-1}}@if(false==empty($product['search']))&search={{$product['search']}}@endif">Prev</a></li>
                    @endif

                    @for($x = 1; $x <= $product['maxProduct']; $x++)
                        @if($x==$product['page'])
                            <li class="active" ><a href="/store/view?id={{$product['id']}}&page={{$x}}@if(false==empty($product['search']))&search={{$product['search']}}@endif">{{$x}}</a></li>
                        @else
                            <li ><a href="/store/view?id={{$product['id']}}&page={{$x}}@if(false==empty($product['search']))&search={{$product['search']}}@endif">{{$x}}</a></li>
                        @endif
                    @endfor

                    @if($product['page']<$product['maxProduct'])
                    <li><a href="/store/view?id={{$product['id']}}&page={{$product['page']+1}}@if(false==empty($product['search']))&search={{$product['search']}}@endif">Next</a></li>
                    @endif
                </ul>
            </nav>

            {{--<div style="margin: 0px;" class="pagination"> {{ $pagination->links() }}</div>--}}
        </div>
    </div>

    <div class="row" style="margin-top: 10px;">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr class="info">
                    <th>#</th>
                    <th>รหัสสินค้า</th>
                    <th>ชื่อสินค้า</th>
                    <th>ราคา</th>
                    <th style="text-align: right;border: solid 2px;">จำนวน</th>
                    <th>หน่วย</th>
                    <th>จัดการ</th>
                </tr>
                </thead>
                <tbody>
                @if(false==empty($product['dataProduct']))
                    @foreach($product['dataProduct'] as $productK=>$productV)
                        <tr>
                            <th scope="row">{{$productK+1}}</th>
                            <td>{{ $productV->product_id }}</td>
                            <td>{{$productV->product_name}} </td>
                            <td style="text-align: right;">{{$productV->product_price}}</td>
                            <td style="border: solid 2px;text-align: right">{{$productV->balance_product}}</td>
                            <td>{{$productV->unit_name}}</td>
                            <td>
                                <a href="/products/displaybyid?id={{$productV->product_id}}" class="btn btn-primary" disabled >ตัดสินค้า</a>
                                {{--<a href="/products/delete?id={{$productV['product_id']}}" class="btn btn-danger" onclick="return confirm('ยืนยันการลบข้อมูลนี้ ?')" >ลบ</a>--}}
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop