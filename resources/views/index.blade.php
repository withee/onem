<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="1M">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <title>Home</title>
    <link rel="stylesheet" href="{{ URL::asset('bootstrap-3.3.4/css/bootstrap.css') }}">
    <script src="{{ URL::asset('bootstrap-3.3.4/js/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('bootstrap-3.3.4/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('js/login.js') }}"></script>
    {{--    @include('staffs.staffsheader')--}}
    {{--{{ HTML::script('assets/javascripts/staffsmanager.js') }}--}}
</head>
<body>
<div class="container-fluid" style="">
    <div id="main" class="row">
        <!-- main content -->
        <div id="content">
            <div style="text-align: center;padding-top: 15%;background-color: #000000;color: #ffffff;" class="row">
                <div class="col-lg-12">
                    <h1>OneM</h1>
                </div>
                <div style="text-align: center;padding-bottom: 10px;" class="col-lg-12">
                    <h1>ระบบจัดการโครงการ ONEM</h1>
                </div>
            </div>
            <div style="text-align: center;background-color: #ffffff;padding:2%" class="row">
                <div class="col-md-12">
                    {{--start login form--}}
                    <form class="form-inline">
                        <input type="text" class="form-control" name="staff_username" placeholder="User Name">
                        <input type="password" class="form-control" name="staff_userpwd" placeholder="Password">
                        <a class="btn btn-default btn-primary" id="check-login">Login</a>
                    </form>
                    {{--end login form--}}
                </div>
                <div style="text-align: center;background-color: #ffffff;padding:2%" class="col-lg-12">
                    <div class="col-md-12 hide" style="background-color: rosybrown" id="error-report">
                    </div>
                </div>
            </div>
        </div>
        <footer class="row" >
            @include('layouts.footer')
        </footer>
    </div>
</div>
</body>
</html>