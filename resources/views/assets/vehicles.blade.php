@extends('layouts.layouts')
@section('content')
    <div id="content">
        <div class="ng-scope" ng-controller="membercustomer">
            <div class="row">
                <h3>จัดการข้อมูลพาหนะ</h3>

                <div>
                    <div class="form-horizontal" style="margin-left: 70%;">
                        <div class="form-group">
                            <div class="col-sm-7">
                                {{--<input ng-model="search" class="form-control ng-pristine ng-valid" id="inputSearchName" placeholder="ชื่อ" type="text">--}}
                            </div>
                            <div class="col-sm-3">
                                <a class="btn btn-success" id="addVehicle"><span
                                            class="glyphicon glyphicon-plus"></span> เพิ่ม</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="modal fade" id="vehicle-modal" ng-class="(modalAddContractor)?'show':''">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" parent="addstaff"
                                        ng-click="modalAddContractor=false">&times;</button>
                                <h4 class="modal-title" id="add_title">พาหนะ</h4>
                            </div>

                            <div class="modal-body">
                                <form class="form-horizontal" role="form" name="formaddcontractor" id="add-form"
                                      novalidate>
                                    <input type="hidden" id="vehicle_id" name="vehicle_id" value=""/>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">ยี่ห้อ <font color="red">*</font> </label>

                                        <div class="col-sm-9">
                                            <input required ng-model="contractor.contractor_name" type="text"
                                                   class="form-control" id="brands" name="brands" placeholder="ยี่ห้อ">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">รุ่น <font color="red">*</font> </label>

                                        <div class="col-sm-9">
                                            <input required ng-model="contractor.contractor_name" type="text"
                                                   class="form-control" id="series" name="series" placeholder="รุ่น">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">สีของรถ <font color="red">*</font>
                                        </label>

                                        <div class="col-sm-9">
                                            <input required ng-model="contractor.contractor_name" type="text"
                                                   class="form-control" id="color" name="color"
                                                   placeholder="สีของรถ">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">ป้ายทะเบียน <font color="red">*</font>
                                        </label>

                                        <div class="col-sm-9">
                                            <input required ng-model="contractor.contractor_name" type="text"
                                                   class="form-control" id="license_plate" name="license_plate"
                                                   placeholder="ป้ายทะเบียนรถ">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">หมายเลขเครื่อง <font color="red">*</font>
                                        </label>

                                        <div class="col-sm-9">
                                            <input required ng-model="contractor.contractor_name" type="text"
                                                   class="form-control" id="engine_number" name="engine_number"
                                                   placeholder="หมายเลขเครื่อง">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">สีบนแผนที่ <font color="red">*</font>
                                        </label>

                                        <div class="col-sm-9">
                                            <input required ng-model="contractor.contractor_name" type="text"
                                                   class="form-control" id="represent_color" name="represent_color"
                                                   value="ff0000">
                                        </div>
                                    </div>
                                    <div class="form-group hide" id="wrong">
                                        <label class="col-sm-3 control-label">แจ้งเตือน </label>

                                        <div class="col-sm-9" id="error-report" style="background-color: #dca7a7">

                                        </div>
                                    </div>
                                </form>

                            </div>
                            <div class="modal-footer">
                                <button ng-if="contractor.contractor_id==''"
                                        ng-disabled="formaddcontractor.$invalid || isUnchanged(contractor)"
                                        ng-click="save(contractor)" type="" class="btn btn-success"
                                        id="submit_vehicle_add">เพิ่ม
                                </button>
                                <button ng-if="contractor.contractor_id!=''"
                                        ng-disabled="formaddcontractor.$invalid || isUnchanged(contractor)"
                                        ng-click="save(contractor)" type="button" class="btn btn-primary"
                                        id="submit_vehicle_edit">แก้ไข
                                </button>
                                <button ng-if="contractor.contractor_id!=''"
                                        ng-disabled="formaddcontractor.$invalid || isUnchanged(contractor)"
                                        ng-click="save(contractor)" type="button" class="btn btn-default"
                                        id="submit_vehicle_cancel" parent="vehicle-modal">ยกเลิก
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <table class="table table-bordered tableBlog">
                    <thead>
                    <tr>
                        <th style="width: 5%; text-align: center">#</th>
                        <th style="width: 10%; text-align: center">ยี่ห้อ</th>
                        <th style="width: 10%; text-align: center">รุ่น</th>
                        <th style="width: 10%; text-align: center">สี</th>
                        <th style="width: 10%; text-align: center">ป้ายทะเบียน</th>
                        <th style="width: 10%; text-align: center">หมายเลขเครื่อง</th>
                        <th style="width: 10%; text-align: center">สีบนแผนที่</th>
                        <th style="width: 17%; text-align: center">จัดการ</th>
                    </tr>
                    </thead>
                    <tbody id="tbodyContent">
                    <!-- ngRepeat: (memberK,memberV) in membercustomer | filter:{member_cu_sname:search} -->
                    @foreach($paging as $vehicle)
                        <tr class="arrUser ng-scope"
                            ng-repeat="(memberK,memberV) in membercustomer | filter:{member_cu_sname:search}">
                            <td class="ng-binding" style="text-align: center;">{{ $vehicle['id'] }}</td>
                            <td class="ng-binding">{{ $vehicle['brands'] }}</td>
                            <td class="ng-binding">{{ $vehicle['series'] }}</td>
                            <td class="ng-binding">{{ $vehicle['color'] }}</td>
                            <td class="ng-binding">{{ $vehicle['license_plate'] }}</td>
                            <td class="ng-binding">{{ $vehicle['serial'] }}</td>
                            <td class="ng-binding"
                                style="background-color: #{{ $vehicle['represent_color'] }}">{{ $vehicle['represent_color'] }}</td>
                            <td style="text-align: center;">
                                <a href="/vehicle/vehicleMaintenance?vehicles_id={{$vehicle->id}}" class="btn btn-warning">ส่งซ่อม</a>
                                <a class="btn btn-primary edit_vehicle" uid="{{ $vehicle['id'] }}">แก้ไข</a>
                                <a class="btn btn-danger remove_vehicle" uid="{{ $vehicle['id'] }}">ลบ</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="row" style="">
                <div class="pagination"> {!! $paging->render() !!}</div>
            </div>

            {{--<div class="modal-backdrop ng-hide" ng-show="(loadingmain)" style="z-index: 1800;background-color:#fff;opacity: 0.9;">--}}
            {{--<div style="margin-top: 10%;margin-left: 45%;opacity: 0.8;">--}}
            {{--<img style="width:150px;height: 150px;" src="/images/loading.gif">--}}
            {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>

@stop