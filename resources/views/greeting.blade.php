@extends('layouts.layouts')
@section('content')
    <div style="text-align: center;" class="row">
        <div class="col-lg-12">
            <h1>OneN</h1>
            @if($success)
                <h1>ยินดีต้อนรับ {{ $data->username }}</h1>
            @else
                <h1>เกิดข้อผิดพลาด {{ $desc }}</h1>
                <a href="/login">กลับหน้าล็อกอิน</a>
            @endif
        </div>
    </div>
@stop