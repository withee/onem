@extends('layouts.layouts')
@section('content')

    <div class="row" style="margin-top: 10px;">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li><a href="/purchases/viewPurchases">จัดการใบลงรับสินค้า</a></li>
                <li class="active">รายการสินค้า</li>
            </ol>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-6">
            <h3>ลงรับสินค้า</h3>
        </div>
        <div class="col-lg-6">
            <div style="font-size:16px;font-weight:bolder;padding: 5px;background-color: #CCCCCC;border-radius: 5px;border: solid 1px#aaaaaa;">
                <div>หมายเลข: {{$dataPurchases->purchases_number}} บันทึกวันที่: {{$dataPurchases->created_at}} </div>
                <div>ภาษี: {{number_format($dataPurchases->purchases_vat,2)}} บาท ส่วนลด: {{number_format($dataPurchases->purchases_discount,2)}} บาท</div>
                <div>ยอดรวม: {{number_format($dataPurchases->purchases_sum,2)}} บาท หลังส่วนลดเเละภาษี: {{number_format($dataPurchases->purchases_total,2)}} บาท</div>
                <div>ผู้บักทึก: {{ \App\Models\Staffs::getNameLastName($dataPurchases->id_member_record)}} ผู้ตรวจสอบ: {{\App\Models\Staffs::getNameLastName($dataPurchases->id_member_toget)}}
                    <div>ตัวเเทนจำหน่าย: {{\App\Models\Company::getNameCompany($dataPurchases->company_id)}}</div>
                </div>
        </div>
    </div>

    <div class="row" style="padding: 10px;float: right;">
        <div class="col-lg-12">
            <form method="POST" action="/purchases/addProduct" class="form-inline">
                <input type="hidden" name="purchases_id" value="{{$id}}">
                @if (true==$errors->has('productId')||true==$errors->has('productCount')||true==$errors->has('storeId')||true==$errors->has('productPrice'))
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>
                        @endforeach
                    </div>
                @endif
                <div class="form-group @if ($errors->has('productId')) has-error @endif">
                    <label for="exampleInputName2">ชื่อสินค้า</label>
                    <select name="productId" class="form-control">
                        @foreach($dataProduct as $key=>$value)
                            <option @if (Input::old('productId')==$value['product_id']) selected @endif value="{{$value->product_id}}">{{$value->product_name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group @if ($errors->has('productCount')) has-error @endif">
                    <label for="exampleInputEmail2">จำนวน</label>
                    <input name="productCount" type="number" class="form-control" placeholder="จำนวน" value="@if(false==empty(Input::old('productCount'))){{Input::old('productCount')}}@endif">
                </div>
                <div class="form-group @if ($errors->has('productPrice')) has-error @endif">
                    <label for="exampleInputEmail2">ราคาต่อหน่วย</label>
                    <input name="productPrice" type="number" class="form-control" placeholder="ราคาต่อหน่วย" value="@if(false==empty(Input::old('productPrice'))){{Input::old('productPrice')}}@endif">
                </div>
                <div class="form-group @if ($errors->has('storeId')) has-error @endif">
                    <label for="exampleInputName2">ลงคลัง</label>
                    <select name="storeId" class="form-control">
                        @foreach($dataStoreName as $key=>$value)
                            <option @if (Input::old('storeId')==$value['storename_id']) selected @endif value="{{$value->storename_id}}">{{$value->storename_name}}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-default">เพิ่มสินค้า</button>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th>รหัสสินค้า</th>
                    <th>ชื่อสินค้า</th>
                    <th>จำนวน</th>
                    <th>ราคาต่อชิ้น</th>
                    <th>ลงคลัง</th>
                    <th>ลบ</th>
                </tr>
                </thead>
                <tbody>
                @if(false==empty($listProduct))
                    @foreach($listProduct as $key=>$value)
                        <tr>
                            <th scope="row">{{$key+1}}</th>
                            <td>{{$value->purchasesdetail_id}}</td>
                            <td>{{$value->product_name}}</td>
                            <td>{{number_format($value->purchasesdetail_productcount, 2, '.', '')}}</td>
                            <td>{{number_format($value->purchasesdetail_productprice, 2, '.', '')}}</td>
                            <td>{{$value->storename_name}}</td>
                            <td>
                                <a onclick="return confirm('ยืนยันการลบ รายการลงรับสินค้า นี้ ?')" href="/purchases/removePurchasesDetail?id={{$value->purchasesdetail_id}}" class="btn btn-danger" >ลบ</a>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop