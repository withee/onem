@extends('layouts.layouts')
@section('content')
    <div class="row" style="margin-top: 20px;">
        <h1>รายการคำสั่งซื้อทั้งหมด</h1>

        <div class="col-lg-3" style="padding: 0px;margin: 0px;">
            <form style="float: left;" class="form-inline" method="GET" action="/products/search" novalidate>
                <div class="input-group input-group-sm">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-edit"
                                                          aria-hidden="true"></span></span>
                    {{--<input type="text" class="form-control" name="search" value="{{$product['search']}}"--}}
                    {{--placeholder="ค้นหา ชื่อสินค้า">--}}
                </div>
                <button type="submit" class="btn btn-sm"><span class="glyphicon glyphicon-search"
                                                               aria-hidden="true"></span></button>
            </form>

        </div>
        <div class="col-lg-9">
            <button style="float: right;" class="btn btn-success" id="add_pr_btn"><span class="glyphicon glyphicon-plus"
                                                                                        aria-hidden="true"></span> เพิ่ม
            </button>
        </div>
    </div>

    {{--<div class="row" style="float: right;">--}}
    {{--<div class="col-lg-12">--}}
    {{--<nav>--}}
    {{--<ul class="pagination" style="margin: 0px;padding: 0px;padding-top: 3px;">--}}
    {{--@if($product['page']>1)--}}
    {{--<li><a href="/products/search?page={{$product['page']-1}}@if(false==empty($product['search']))&search={{$product['search']}}@endif">Prev</a></li>--}}
    {{--@endif--}}

    {{--@for($x = 1; $x <= $product['maxProduct']; $x++)--}}
    {{--@if($x==$product['page'])--}}
    {{--<li class="active" ><a href="/products/search?page={{$x}}@if(false==empty($product['search']))&search={{$product['search']}}@endif">{{$x}}</a></li>--}}
    {{--@else--}}
    {{--<li ><a href="/products/search?page={{$x}}@if(false==empty($product['search']))&search={{$product['search']}}@endif">{{$x}}</a></li>--}}
    {{--@endif--}}
    {{--@endfor--}}

    {{--@if($product['page']<$product['maxProduct'])--}}
    {{--<li><a href="/products/search?page={{$product['page']+1}}@if(false==empty($product['search']))&search={{$product['search']}}@endif">Next</a></li>--}}
    {{--@endif--}}
    {{--</ul>--}}
    {{--</nav>--}}
    {{--</div>--}}
    {{--</div>--}}

    <div class="row" style="margin-top: 10px;">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr class="info">
                    <th>#</th>
                    <th>วัตถุประสงค์</th>
                    <th>ผู้เขียนคำร้อง</th>
                    <th>วันที่</th>
                    <th>สถานะ</th>
                    <th>จัดการ</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data as $pr)
                    <tr>
                        <th scope="row">{{$pr->id}}</th>
                        <td>{{$pr->purpose}}</td>
                        <td> {{$pr->staff_firstname}} {{$pr->staff_lastname}}</td>
                        <td style="text-align: right;">{{$pr->created_at}}</td>
                        <td>
                            @if('N'==$pr->accept)
                                ยังไม่รับคำร้อง
                            @else
                                {{$pr->purchase_firstname}} {{$pr->purchase_lastname}} รับคำร้องแล้ว
                            @endif
                        </td>
                        <td>
                            @if($pr->accept=='N')
                                <a href="#" class="btn btn-success pr_add_product" pr_id="{{$pr->id}}">
                                    เพิ่มสินค้า
                                    {{--<span class="glyphicon glyphicon-plus pr_add_product" pr_id="{{$pr->id}}"></span>--}}
                                </a>
                                <a href="#" class="btn btn-warning pr_edit" pr_id="{{$pr->id}}">
                                    แก้ไขรายการ
                                    {{--<span class="glyphicon glyphicon-eye-open pr_view" pr_id="{{$pr->id}}"></span>--}}
                                </a>
                                <a href="#" class="btn btn-danger pr_rm" pr_id="{{$pr->id}}">
                                    ลบคำสั่งซื้อ
                                    {{--<span class="glyphicon glyphicon-plus pr_add_product" pr_id="{{$pr->id}}"></span>--}}
                                </a>
                                <a href="#" class="btn btn-primary pr_approve" pr_id="{{$pr->id}}">
                                    รับทราบ
                                    {{--<span class="glyphicon glyphicon-check pr_approve" pr_id="{{$pr->id}}"></span>--}}
                                </a>
                            @else
                                <a href="#" class="btn btn-success pr_view" pr_id="{{$pr->id}}">
                                    ดูรายการสินค้า
                                    {{--<span class="glyphicon glyphicon-eye-open pr_view" pr_id="{{$pr->id}}"></span>--}}
                                </a>
                            @endif

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <!-- Modal -->
    <div id="prview_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">รายการสินค้าที่สั่ง</h4>
                </div>
                <div class="modal-body">
                    <table style="width: 100%;" class="table table-bordered">
                        <thead>
                        <tr>
                            <th style="padding: 5px">#</th>
                            <th style="padding: 5px">ชื่อสินค้า</th>
                            <th>จำนวน</th>
                            <th>ลบ</th>
                        </tr>
                        </thead>
                        <tbody id="pr_view_table">

                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-warning" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <!-- Modal -->
    <div id="pr_add_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">เพิ่มสินค้าลงใบสั่งซื้อ</h4>
                </div>
                <div class="modal-body">
                    <form role="form">
                        <div class="form-group">
                            <label for="email">สินค้า:</label>
                            <select class="form-control" id="pr_product_list">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="pwd">จำนวน:</label>
                            <input type="text" class="form-control" id="add_quantity" placeholder="0">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-primary" id="add_new_pr">เพิ่ม</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>


    <!-- Modal -->
    <div id="add_pr_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">เพิ่มใบสั่งซื้อ</h4>
                </div>
                <div class="modal-body">
                    <form role="form">
                        <div class="form-group">
                            <label for="pwd">วัตถุประสงค์:</label>
                            <input type="text" class="form-control" id="pr_purpose" placeholder="เติมสินค้าในคลัง">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-primary" id="add_pr">เพิ่ม</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
@stop