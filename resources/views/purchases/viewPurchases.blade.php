@extends('layouts.layouts')
@section('content')
    <div class="row">
        <div class="col-lg-6">
            <h3>จัดการใบรับสินค้า</h3>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-3" style="padding: 0px;margin: 0px;">
            <form style="float: left;" class="form-inline" method="GET" action="/purchases/viewPurchases" novalidate>
                <div class="input-group input-group-sm">
                    <span class="input-group-addon" ><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></span>
                    <input type="text" class="form-control" name="search" value="{{$dataPurchases['search']}}" placeholder="ค้นหา หมายเลขใบสั่งซื้อ">
                </div>
                <button type="submit"  class="btn btn-sm"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
            </form>
        </div>
        <div class="col-lg-9" style="padding-bottom: 10px;">
            <button style="float: right;" type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                เพิ่มใบลงรับสินค้า
            </button>
        </div>
    </div>

    <div class="row" style="float: right;">
        <div class="col-lg-12">
            <nav>
                <ul class="pagination" style="margin: 0px;padding: 0px;padding-top: 3px;">
                    @if($dataPurchases['page']>1)
                        <li><a href="/purchases/viewPurchases?page={{$dataPurchases['page']-1}}@if(false==empty($dataPurchases['search']))&search={{$dataPurchases['search']}}@endif">Prev</a></li>
                    @endif

                    @for($x = 1; $x <= $dataPurchases['maxProduct']; $x++)
                        @if($x==$dataPurchases['page'])
                            <li class="active" ><a href="/purchases/viewPurchases?page={{$x}}@if(false==empty($dataPurchases['search']))&search={{$dataPurchases['search']}}@endif">{{$x}}</a></li>
                        @else
                            <li ><a href="/purchases/viewPurchases?page={{$x}}@if(false==empty($dataPurchases['search']))&search={{$dataPurchases['search']}}@endif">{{$x}}</a></li>
                        @endif
                    @endfor

                    @if($dataPurchases['page']<$dataPurchases['maxProduct'])
                        <li><a href="/purchases/viewPurchases?page={{$dataPurchases['page']+1}}@if(false==empty($dataPurchases['search']))&search={{$dataPurchases['search']}}@endif">Next</a></li>
                    @endif
                </ul>
            </nav>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#
                    <th>วันที่</th>
                    <th>รหัสใบรับสินค้า</th>
                    <th>ยอดเงิน</th>
                    <th>ผู้ตรวจ</th>
                    <th>ผู้บันทึก</th>
                    <th>ลบ</th>
                </tr>
                </thead>
                <tbody>
                @if(false==empty($dataPurchases))
                    @foreach($dataPurchases['dataProduct'] as $key=>$value)
                        <tr>
                            <th scope="row">{{$key+1}}</th>
                            <td>{{$value->purchases_date}}</td>
                            <td>{{$value->purchases_number}}</td>
                            <td>{{$value->purchases_total}}</td>
                            <td>{{$value->id_member_record}}</td>
                            <td>{{$value->id_member_toget}}</td>
                            <td>
                                <a href="/purchases/viewProduct?id={{$value->purchases_id}}" class="btn btn-success" >เพิ่มสินค้า</a>
                                <a href="/purchases/editPurchases?id={{$value->purchases_id}}" class="btn btn-warning" >เพิ่มข้อมูลใบรับ</a>
                                {{--<a onclick="return confirm('ยืนยันการลบข้อมูล สินค้านี้ ?')" href="/purchases/remove?id={{$key}}" class="btn btn-danger" >ลบ</a>--}}
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade @if(false==empty($purchases['purchases_id']) || true==$errors->has('purchases_vat')||true==$errors->has('purchases_discount')||true==$errors->has('purchases_number')||true==$errors->has('company_id')||true==$errors->has('id_member_record')||true==$errors->has('id_member_toget')||true==$errors->has('member_toget_date')||true==$errors->has('purchases_date')) in @endif" style="@if(false==empty($purchases['purchases_id'])||true==$errors->has('purchases_vat')||true==$errors->has('purchases_discount')||true==$errors->has('purchases_number')||true==$errors->has('company_id')||true==$errors->has('id_member_record')||true==$errors->has('id_member_toget')||true==$errors->has('member_toget_date')||true==$errors->has('purchases_date')) display: block; @endif" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <form method="POST" action="/purchases/addPurchases" novalidate >
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <a href="/purchases/viewPurchases" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                        <h4 class="modal-title" id="myModalLabel">กรอกรายละเอียดใบลงรับสินค้า</h4>
                    </div>
                    <div class="modal-body">
                        @if ($errors->has())
                            <div class="alert alert-danger" style="padding: 0px;">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}
                                @endforeach
                            </div>
                        @endif
                        <div class="form-horizontal">
                            <input type="hidden" name="purchases_id" value="@if(false==empty($purchases['purchases_id'])){{$purchases['purchases_id']}}@elseif(false==empty(Input::old('purchases_id'))){{Input::old('purchases_id')}}@endif">
                            <div class="form-group @if($errors->has('purchases_number')) has-error @endif">
                                <label for="inputEmail3" class="col-lg-4 control-label">เลขใบเสร็จ</label>
                                <div class="col-lg-8">
                                    <input type="text" value="@if(false==empty($purchases['purchases_number'])){{$purchases['purchases_number']}}@elseif(false==empty(Input::old('purchases_number'))){{Input::old('purchases_number')}}@endif" name="purchases_number" class="form-control" placeholder="เลขใบเสร็จ">
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('purchases_date')) has-error @endif">
                                <label for="inputEmail3" class="col-lg-4 control-label">วันที่ใบเสร็จ {{Input::old('purchases_date')}}</label>
                                <div class="col-lg-8">
                                    <input type="text" id="purchases_date" value="@if(false==empty($purchases['purchases_date'])){{$purchases['purchases_date']}}@elseif(false==empty(Input::old('purchases_date'))){{Input::old('purchases_date')}}@endif" name="purchases_date" class="form-control" id="inputEmail3" placeholder="วันที่ใบเสร็จ">
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('company_id')) has-error @endif">
                                <label for="inputPassword3" class="col-lg-4 control-label">ตัวแทนจำหน่าย {{Input::old('company_id')}}</label>
                                <div class="col-lg-8">
                                    <select class="form-control" name="company_id">
                                        <option value="">เลือกตัวแทน</option>
                                        @foreach($dataCompany as $key=>$value)
                                            <option {{(Input::old('company_id')==$value['company_id']||(false==empty($purchases['company_id'])&&$purchases['company_id']==$value['company_id']))?'selected':''}} value="{{$value->company_id}}">{{$value->company_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            {{--<div class="form-group @if($errors->has('id_member_record')) has-error @endif">--}}
                                {{--<label for="inputPassword3" class="col-lg-4 control-label">ผู้บันทึก</label>--}}
                                {{--<div class="col-lg-8">--}}

                                    <input type="hidden" id="purchases_date" value="{!! \Illuminate\Support\Facades\Session::get('user')->staff_id !!}" name="id_member_record" class="form-control" id="inputEmail3" placeholder="วันที่ใบเสร็จ">
                                    {{--<select class="form-control" name="id_member_record">--}}
                                        {{--<option value="">เลือกผู้บันทึก</option>--}}
                                        {{--@foreach($dataStaff as $key=>$value)--}}
                                            {{--<option {{(Input::old('id_member_record')==$value->staff_id||(false==empty($purchases['id_member_record'])&&$purchases['id_member_record']==$value->staff_id))?'selected':''}} value="{{$value->staff_id}}">{{$value->staff_firstname}} {{$value->staff_lastname}}</option>--}}
                                        {{--@endforeach--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="form-group @if($errors->has('id_member_toget')) has-error @endif">
                                <label for="inputPassword3" class="col-lg-4 control-label">ผู้ตรวจ</label>
                                <div class="col-lg-8">
                                    <select class="form-control" name="id_member_toget">
                                        <option value="">เลือกผู้ตรวจ</option>
                                        @foreach($dataStaff as $key=>$value)
                                            <option {{(Input::old('id_member_toget')==$value->staff_id||(false==empty($purchases['id_member_toget'])&&$purchases['id_member_toget']==$value->staff_id))?'selected':''}} value="{{$value->staff_id}}">{{$value->staff_firstname}} {{$value->staff_lastname}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('member_toget_date')) has-error @endif">
                                <label for="inputEmail3" class="col-lg-4 control-label">วันที่ตรวจสอบ</label>
                                <div class="col-lg-8">
                                    <input type="text" id="member_toget_date" value="@if(false==empty($purchases['member_toget_date'])){{$purchases['member_toget_date']}}@elseif(false==empty(Input::old('member_toget_date'))){{Input::old('member_toget_date')}}@endif" name="member_toget_date" class="form-control" id="inputEmail3" placeholder="วันที่ตรวจสอบ">
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('purchases_vat')) has-error @endif">
                                <label for="inputEmail4" class="col-lg-4 control-label">หักภาษี</label>
                                <div class="col-lg-8" style="">
                                    <input type="number" value="@if(false==empty($purchases['purchases_vat'])){{$purchases['purchases_vat']}}@elseif(false==empty(Input::old('purchases_vat'))){{Input::old('purchases_vat')}}@else{{0}}@endif" name="purchases_vat" class="form-control" id="inputEmail3" placeholder="หักภาษี">
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('purchases_discount')) has-error @endif">
                                <label for="inputPassword" class="col-lg-4 control-label">ส่วนลด</label>
                                <div class="col-lg-8" style="">
                                    <input type="number" value="@if(false==empty($purchases['purchases_discount'])){{$purchases['purchases_discount']}}@elseif(false==empty(Input::old('purchases_discount'))){{Input::old('purchases_discount')}}@else{{0}}@endif" name="purchases_discount" class="form-control" id="inputEmail3" placeholder="ส่วนลด">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        {{--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                        <button style="width: 100%;" type="submit" class="btn btn-primary">เพิ่ม</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop