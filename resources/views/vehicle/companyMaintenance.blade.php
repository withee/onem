@extends('layouts.layouts')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h2>จัดการ บริษัทซ่อมบำรุง</h2>
        </div>
    </div>

    <div class="row" style="padding: 10px;">
        <div class="col-lg-12">
            <form method="POST" action="/vehicle/addCompanyMaintenance" class="form-inline" novalidate>
                @if (true==$errors->has('company_maintenance_name'))
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>
                        @endforeach
                    </div>
                @endif
                <input type="hidden" name="company_maintenance_id" value="{{$company->company_maintenance_id}}">
                <div class="form-group  @if ($errors->has('company_maintenance_name')) has-error @endif">
                    <input type="text" class="form-control" style="width: 320px;" value="@if(!empty($company->company_maintenance_name)){{$company->company_maintenance_name}}@elseif(!empty(Input::old('company_maintenance_name'))){{Input::old('company_maintenance_name')}}@endif" name="company_maintenance_name" placeholder="ชื่อบริษัท">
                </div>
                    <div class="form-group  @if ($errors->has('company_maintenance_tel')) has-error @endif">
                        <input type="text" class="form-control" style="width: 120px;" value="@if(!empty($company->company_maintenance_tel)){{$company->company_maintenance_tel}}@elseif(!empty(Input::old('company_maintenance_tel'))){{Input::old('company_maintenance_tel')}}@endif" name="company_maintenance_tel" placeholder="เบอร์โทร">
                    </div>
                    <div class="form-group  @if ($errors->has('company_maintenance_address')) has-error @endif">
                        <input type="text" class="form-control" style="width: 320px;" value="@if(!empty($company->company_maintenance_address)){{$company->company_maintenance_address}}@elseif(!empty(Input::old('company_maintenance_address'))){{Input::old('company_maintenance_address')}}@endif" name="company_maintenance_address" placeholder="ที่อยู่">
                    </div>
                @if(!empty($company->company_maintenance_id))
                    <button type="submit" class="btn btn-warning">แก้ไข</button>
                @else
                    <button type="submit" class="btn btn-primary">เพิ่ม</button>
                @endif
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th>ชื่อ</th>
                    <th>ที่อยู่</th>
                    <th>เบอร์โทร</th>
                    <th>จัดการ</th>
                </tr>
                </thead>
                <tbody>
                @foreach($dataCompany as $key=>$value)
                    <tr>
                        <th scope="row">{{$key+1}}</th>
                        <td>{{$value->company_maintenance_name}}</td>
                        <td>{{$value->company_maintenance_tel}}</td>
                        <td>{{$value->company_maintenance_address}}</td>
                        <td>
                            <a href="/vehicle/companyMaintenance?id={{$value->company_maintenance_id}}" class="btn btn-warning">แก้ไข</a>
                            <a href="/vehicle/removeCompanyMaintenance?id={{$value->company_maintenance_id}}" class="btn btn-danger" onclick="return confirm('ยืนยันการลบบริษัทซ่อมนี้')">ลบ</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop