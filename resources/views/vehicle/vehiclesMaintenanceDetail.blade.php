@extends('layouts.layouts')
@section('content')
    <div class="row" style="margin-top: 10px;">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li><a href="/assets/vehicles">จัดการยานพาหนะ</a></li>
                <li><a href="/vehicle/vehicleMaintenance?vehicles_id={{$vehiclesMaintenance->vehicles_id}}">รายการส่งซ่อม</a></li>
                <li class="active">รายจ่ายค่าซ่อม</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-7">
            <h2>จัดการ รายจ่ายค่าซ่อม</h2>
        </div>
        <div class="col-lg-5">
            <div style="margin: 5px;padding: 5px;border: solid 1px #3c3c3c;background-color: #cccccc;border-radius: 5px;">
                <div style="float: left;width: 50%;">
                    <div style="float: left;font-size: 18px;font-weight: bolder;">ยี่ห้อ: </div>
                    <div style="float: left;font-size: 18px;padding-left: 5px;">{{$vehicles->brands}}</div>
                </div>
                <div style="float: left;width: 50%;">
                    <div style="float: left;font-size: 18px;font-weight: bolder;">รุ่น: </div>
                    <div style="float: left;font-size: 18px;padding-left: 5px;">{{$vehicles->series}}</div>
                </div>
                <div style="float: left;width: 50%;">
                    <div style="float: left;font-size: 18px;font-weight: bolder;">สี่: </div>
                    <div style="float: left;font-size: 18px;padding-left: 5px;">{{$vehicles->color}}</div>
                </div>
                <div style="float: left;width: 50%;">
                    <div style="float: left;font-size: 18px;font-weight: bolder;">ทะเบียน: </div>
                    <div style="float: left;font-size: 18px;padding-left: 5px;">{{$vehicles->license_plate}}</div>
                </div>
                <div style="float: left;width: 50%;">
                    <div style="float: left;font-size: 18px;font-weight: bolder;">ชื่อที่ซ่อม: </div>
                    <div style="float: left;font-size: 18px;padding-left: 5px;">{{$companyMaintenance->company_maintenance_name}}</div>
                </div>
                <div style="float: left;width: 50%;">
                    <div style="float: left;font-size: 18px;font-weight: bolder;">ผู้ส่งซ่อม: </div>
                    <div style="float: left;font-size: 18px;padding-left: 5px;">{{$vehiclesMaintenance->staff_id}}</div>
                </div>
                <div style="float: left;width: 100%;">
                    <div style="float: left;font-size: 18px;font-weight: bolder;">อาการ: </div>
                    <div style="float: left;font-size: 18px;padding-left: 5px;">{{$vehiclesMaintenance->vehicles_maintenance_symptom}}</div>
                </div>
                <div style="clear: both;"></div>
            </div>
        </div>
    </div>
    <div class="row" style="padding: 10px;">
        <div class="col-lg-12">
            <form method="POST" action="/vehicle/addVehiclesMaintenanceDetail" class="form-inline" novalidate>
                @if (true==$errors->has('vehicles_maintenance_detail_count') || true==$errors->has('vehicles_maintenance_detail_name') || true==$errors->has('vehicles_maintenance_detail_price'))
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>
                        @endforeach
                    </div>
                @endif
                <input type="hidden" name="vehicles_maintenance_detail_id" value="{{$vehiclesMaintenanceDetail->vehicles_maintenance_detail_id}}"/>
                <input type="hidden" name="vehicles_maintenance_id" value="{{$vehiclesMaintenanceDetail->vehicles_maintenance_id}}"/>
                <div class="form-group  @if ($errors->has('vehicles_maintenance_detail_name')) has-error @endif">
                    <input type="text" class="form-control" style="width: 420px;" value="@if(!empty($vehiclesMaintenanceDetail->vehicles_maintenance_detail_name)){{$vehiclesMaintenanceDetail->vehicles_maintenance_detail_name}}@elseif(!empty(Input::old('vehicles_maintenance_detail_name'))){{Input::old('vehicles_maintenance_detail_name')}}@endif" name="vehicles_maintenance_detail_name" placeholder="ค่าใช่จ่าย">
                </div>
                    <div class="form-group  @if ($errors->has('vehicles_maintenance_detail_status')) has-error @endif">
                        <select class="form-control" name="vehicles_maintenance_detail_status">
                            <option {{(($vehiclesMaintenanceDetail->$vehiclesMaintenanceDetail=='add')?'selected':'')}} value="add">เพิ่มค่าใช่จ่าย</option>
                            <option {{(($vehiclesMaintenanceDetail->$vehiclesMaintenanceDetail=='pay')?'selected':'')}} value="pay">จ่ายเเล้ว</option>
                        </select>
                    </div>
                <div class="form-group  @if ($errors->has('vehicles_maintenance_detail_count')) has-error @endif">
                    <input type="text" class="form-control" style="width: 120px;" value="@if(!empty($vehiclesMaintenanceDetail->vehicles_maintenance_detail_count)){{$vehiclesMaintenanceDetail->vehicles_maintenance_detail_count}}@elseif(!empty(Input::old('vehicles_maintenance_detail_count'))){{Input::old('vehicles_maintenance_detail_count')}}@endif" name="vehicles_maintenance_detail_count" placeholder="จำนวน">
                </div>
                <div class="form-group  @if ($errors->has('vehicles_maintenance_detail_price')) has-error @endif">
                    <input type="text" class="form-control" style="width: 120px;" value="@if(!empty($vehiclesMaintenanceDetail->vehicles_maintenance_detail_price)){{$vehiclesMaintenanceDetail->vehicles_maintenance_detail_price}}@elseif(!empty(Input::old('vehicles_maintenance_detail_price'))){{Input::old('vehicles_maintenance_detail_price')}}@endif" name="vehicles_maintenance_detail_price" placeholder="ราคาต่อหน่วย">
                </div>
                @if(!empty($vehiclesMaintenanceDetail->vehicles_maintenance_detail_id))
                    <button type="submit" class="btn btn-warning">แก้ไข</button>
                @else
                    <button type="submit" class="btn btn-primary">เพิ่ม</button>
                @endif
            </form>
        </div>
    </div>
    <div class="row" style="padding: 5px;">
        <div class="col-lg-10"></div>
        <div class="col-lg-2">
            @if(count($dataVehiclesMaintenanceDetail)>0)
                <a onclick="return confirm('ยืนยันการ ปิดค่าใช้จ่ายทั้งหมด')" href="/vehicle/clearMaintenanceDetail?id={{$vehiclesMaintenanceDetail->vehicles_maintenance_id}}" class="btn btn-block btn-info">ปิดค่าใช้ทั้งหมด</a>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th>ชื่อค่าใช้จ่าย</th>
                    <th>ประเภท</th>
                    <th>จำนวน</th>
                    <th>ราคา/หน่วย</th>
                    <th>ราคารวม</th>
                    <th>จัดการ</th>
                </tr>
                </thead>
                <tbody>
                @foreach($dataVehiclesMaintenanceDetail as $key=>$value)
                <tr>
                    <td>{{$value->updated_at}}</td>
                    <td>{{$value->vehicles_maintenance_detail_name}}</td>
                    <td>{{(($value->vehicles_maintenance_detail_status=='add')?'ค่าใช้จ่าย':'ชำระเเล้ว')}}</td>
                    <td>{{$value->vehicles_maintenance_detail_count}}</td>
                    <td>{{$value->vehicles_maintenance_detail_price}}</td>
                    <td>{{$value->vehicles_maintenance_detail_total}}</td>
                    <td>
                        @if($value->vehicles_maintenance_detail_status=='add')
                            <a href="/vehicle/payVehiclesMaintenanceDetail?id={{$value->vehicles_maintenance_detail_id}}" class="btn btn-info">ชำระเงิน</a>
                        @endif
                        <a href="/vehicle/vehiclesMaintenanceDetail?vehicles_maintenance_id={{$vehiclesMaintenanceDetail->vehicles_maintenance_id}}&id={{$value->vehicles_maintenance_detail_id}}" class="btn btn-warning">แก้ไข</a>
                        <a onclick="return confirm('ยืนยันการลบค่าใช่จ่ายนี้ ')" href="/vehicle/removeVehiclesMaintenanceDetail?id={{$value->vehicles_maintenance_detail_id}}" class="btn btn-danger">ลบ</a>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop