@extends('layouts.layouts')
@section('content')
    <div class="row" style="margin-top: 10px;">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li><a href="/assets/vehicles">จัดการยานพาหนะ</a></li>
                <li class="active">รายการส่งซ่อม</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-7">
            <h2>รายการส่งซ่อม</h2>
        </div>
        <div class="col-lg-5">
            <div style="margin: 5px;padding: 5px;border: solid 1px #3c3c3c;background-color: #cccccc;border-radius: 5px;">
                <div style="float: left;width: 50%;">
                    <div style="float: left;font-size: 18px;font-weight: bolder;">ยี่ห้อ: </div>
                    <div style="float: left;font-size: 18px;padding-left: 5px;">{{$vehicles->brands}}</div>
                </div>
                <div style="float: left;width: 50%;">
                    <div style="float: left;font-size: 18px;font-weight: bolder;">รุ่น: </div>
                    <div style="float: left;font-size: 18px;padding-left: 5px;">{{$vehicles->series}}</div>
                </div>
                <div style="float: left;width: 50%;">
                    <div style="float: left;font-size: 18px;font-weight: bolder;">สี่: </div>
                    <div style="float: left;font-size: 18px;padding-left: 5px;">{{$vehicles->color}}</div>
                </div>
                <div style="float: left;width: 50%;">
                    <div style="float: left;font-size: 18px;font-weight: bolder;">ทะเบียน: </div>
                    <div style="float: left;font-size: 18px;padding-left: 5px;">{{$vehicles->license_plate}}</div>
                </div>
                <div style="float: left;width: 50%;">
                    <div style="float: left;font-size: 18px;font-weight: bolder;">เพิ่ม: </div>
                    <div style="float: left;font-size: 18px;padding-left: 5px;">{{$vehicles->created_at}}</div>
                </div>
                <div style="float: left;width: 50%;">
                    <div style="float: left;font-size: 18px;font-weight: bolder;">แก้ไข: </div>
                    <div style="float: left;font-size: 18px;padding-left: 5px;">{{$vehicles->updated_at}}</div>
                </div>
                <div style="clear: both;"></div>
            </div>
        </div>
    </div>
    <div class="row" style="padding: 10px;">
        <div class="col-lg-12">
            <form method="POST" action="/vehicle/addVehiclesMaintenance" class="form-inline" novalidate>
                @if (true==$errors->has('vehicles_maintenance_symptom') || true==$errors->has('company_maintenance_id') || true==$errors->has('vehicles_maintenance_bill_id') || true==$errors->has('vehicles_id') || true==$errors->has('staff_id'))
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>
                        @endforeach
                    </div>
                @endif
                <input type="hidden" name="vehicles_maintenance_id" value="{{$vehiclesMaintenance->vehicles_maintenance_id}}"/>
                <input type="hidden" name="vehicles_id" value="{{$vehiclesMaintenance->vehicles_id}}"/>
                    <div class="form-group  @if ($errors->has('company_maintenance_id')) has-error @endif">
                        <select class="form-control" name="company_maintenance_id">
                            @foreach($companyMaintenance as $keyCM=>$valueCM)
                                @if($valueCM->company_maintenance_id==$vehiclesMaintenance->company_maintenance_id)
                                    <option selected value="{{$valueCM->company_maintenance_id}}">{{$valueCM->company_maintenance_name}}</option>
                                @else
                                    <option value="{{$valueCM->company_maintenance_id}}">{{$valueCM->company_maintenance_name}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                {{--<div class="form-group  @if ($errors->has('vehicles_maintenance_bill_id')) has-error @endif">--}}
                    {{--<input type="text" class="form-control" style="width: 120px;" value="@if(!empty($vehiclesMaintenance->vehicles_maintenance_bill_id)){{$vehiclesMaintenance->vehicles_maintenance_bill_id}}@elseif(!empty(Input::old('vehicles_maintenance_bill_id'))){{Input::old('vehicles_maintenance_bill_id')}}@endif" name="vehicles_maintenance_bill_id" placeholder="รหัสบิล">--}}
                {{--</div>--}}
                    <div class="form-group  @if ($errors->has('staff_id')) has-error @endif">
                        {{--<label for="exampleInputEmail2">ผู้ขอเบิก</label>--}}
                        <select class="form-control" name="staff_id">
                            @foreach($staff as $keyst=>$valuest)
                                <option @if(Input::old('staff_id')==$valuest->staff_id)selected @elseif(!empty($vehiclesMaintenance->staff_id))selected @endif value="{{$valuest->staff_username}}">{{$valuest->staff_firstname}} {{$valuest->staff_lastname}}</option>
                            @endforeach
                        </select>
                        {{--<input type="text" class="form-control" style="width: 100px;" value="@if(!empty($vehiclesMaintenance->payee)){{$vehiclesMaintenance->payee}}@elseif(!empty(Input::old('payee'))){{Input::old('payee')}}@endif" name="payee"/>--}}
                    </div>
                <div class="form-group  @if ($errors->has('vehicles_maintenance_symptom')) has-error @endif">
                    <input type="text" class="form-control" style="width: 420px;" value="@if(!empty($vehiclesMaintenance->vehicles_maintenance_symptom)){{$vehiclesMaintenance->vehicles_maintenance_symptom}}@elseif(!empty(Input::old('vehicles_maintenance_symptom'))){{Input::old('vehicles_maintenance_symptom')}}@endif" name="vehicles_maintenance_symptom" placeholder="อาการ">
                </div>
                <div class="form-group  @if ($errors->has('vehicles_maintenance_remark')) has-error @endif">
                    <input type="text" class="form-control" style="width: 220px;" value="@if(!empty($vehiclesMaintenance->vehicles_maintenance_remark)){{$vehiclesMaintenance->vehicles_maintenance_remark}}@elseif(!empty(Input::old('vehicles_maintenance_remark'))){{Input::old('vehicles_maintenance_remark')}}@endif" name="vehicles_maintenance_remark" placeholder="หมายเหตุ">
                </div>
                @if(!empty($vehiclesMaintenance->vehicles_maintenance_id))
                    <button type="submit" class="btn btn-warning">แก้ไข</button>
                @else
                    <button type="submit" class="btn btn-primary">เพิ่ม</button>
                @endif
            </form>
        </div>
    </div>
    <div class="row" style="">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th>สถานะ</th>
                    <th>ที่ซ่อม</th>
                    <th>ผู้ส่งซ้อม</th>
                    <th>อาการ</th>
                    <th>หมายเหตุ</th>
                    <th>จัดการ</th>
                </tr>
                </thead>
                <tbody>
                @foreach($dataVehiclesMaintenance as $key=>$value)
                    <tr>
                        <td>{{$value->created_at}}</td>
                        <td>{{(($value->vehicles_maintenance_status=='repair')?'ส่งซ่อม':'ซ่อมเสร็จ')}}</td>
                        <td>{{\App\Models\CompanyMaintenance::getCompanyMaintenanceName($value->company_maintenance_id)}}</td>
                        <td>{{$value->staff_id}}</td>
                        <td>{{$value->vehicles_maintenance_symptom}}</td>
                        <td>{{$value->vehicles_maintenance_remark}}</td>
                        <td>
                            <a href="/vehicle/vehiclesMaintenanceDetail?vehicles_maintenance_id={{$value->vehicles_maintenance_id}}" class="btn btn-success">ลงบิล</a>
                            <a href="/vehicle/vehicleMaintenance?vehicles_id={{$vehiclesMaintenance->vehicles_id}}&id={{$value->vehicles_maintenance_id}}" class="btn btn-warning">แก้ไข</a>
                            <a onclick="return confirm('ยืนยันการลบ งานส่งซ่อม')" href="/vehicle/removeVehicleMaintenance?id={{$value->vehicles_maintenance_id}}" class="btn btn-danger">ลบ</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop