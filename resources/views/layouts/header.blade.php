{{--<div class="navbar">--}}
    {{--<div class="navbar-inner">--}}
        {{--<a id="logo" href="/">Single Malt</a>--}}
        {{--<ul class="nav">--}}
            {{--<li><a href="/">Home</a></li>--}}
            {{--<li><a href="/about">About</a></li>--}}
            {{--<li><a href="/projects">Projects</a></li>--}}
            {{--<li><a href="/contact">Contact</a></li>--}}
        {{--</ul>--}}
    {{--</div>--}}
{{--</div>--}}

<div class="row">
    <div class="navbar-header navbar-inverse">
        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-example-js-navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/products/manage">OneM</a>
    </div>
    <div class="collapse navbar-collapse navbar-inverse">
        <ul class="nav navbar-nav">
            <li class="dropdown">
                <a aria-expanded="false" aria-haspopup="true" href="#" id="drop1" role="button" class="dropdown-toggle" data-toggle="dropdown">คลัง <b class="caret"></b></a>
                <ul class="dropdown-menu" role="menu" aria-labelledby="drop1">
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="/products/manage">รายการสินค้า</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="/purchases/viewPurchases">ลงรับสินค้า</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="/bring/viewBring">เบิกสินค้า</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="/groupBring/viewGroupBring">กลุ่มใบเบิกสินค้า</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="/store/change">ปรับปรุงสินค้า</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="/return/viewReturn">คืนสินค้า</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="/storeName/manage">จัดการคลัง</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="/companys/manage">จัดการตัวแทนจำหน่าย</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="/productUnit/manage">จัดการหน่วยสินค้า</a></li>
                    {{--<li role="presentation"><a role="menuitem" tabindex="-1" href="/products/productbystore">รายงานสินค้าตามคลัง</a></li>--}}
                </ul>
            </li>
            <li class="dropdown">
                <a aria-expanded="false" aria-haspopup="true" href="#" id="drop1" role="button" class="dropdown-toggle" data-toggle="dropdown">โรงงาน <b class="caret"></b></a>
                <ul class="dropdown-menu" role="menu" aria-labelledby="drop1">
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="/controlBuild/viewControlBuild">จัดการควบคุมการผลิต</a>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="/controlBuild/viewControlBuildClose">โครงการผลิตเก่า</a>
                </ul>
            </li>
            <li class="dropdown">
                <a aria-expanded="false" aria-haspopup="true" href="#" id="drop1" role="button" class="dropdown-toggle" data-toggle="dropdown">โครงการ <b class="caret"></b></a>
                <ul class="dropdown-menu" role="menu" aria-labelledby="drop1">
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="/project/viewProject">จัดการโครงการ</a>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">รายงาน</a>
                    {{--</li>--}}
                    <!--                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="">จัดสร้าง</a></li>-->
                </ul>
            </li>
            <li class="dropdown">
                <a aria-expanded="false" aria-haspopup="true" href="#" id="drop1" role="button" class="dropdown-toggle" data-toggle="dropdown">โลจิสติก <b class="caret"></b></a>
                <ul class="dropdown-menu" role="menu" aria-labelledby="drop1">
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="/assets/vehicles">พาหนะ</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="/maps">ระบบติดตามรถ</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="/vehicle/companyMaintenance">บริษัทซ่อม</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a aria-expanded="false" aria-haspopup="true" href="#" id="drop1" role="button" class="dropdown-toggle" data-toggle="dropdown">บุคลากร <b class="caret"></b></a>
                <ul class="dropdown-menu" role="menu" aria-labelledby="drop1">
                    {{--<li role="presentation"><a role="menuitem" tabindex="-1" href="/staffs/addform">เพิ่ม</a></li>--}}
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="/staffs/manage">จัดการ</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="/account/employeesDaily">พนักงานรายวัน</a></li>
                    <!--                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="">จัดสร้าง</a></li>-->
                </ul>
            </li>

            <li class="dropdown">
                <a aria-expanded="false" aria-haspopup="true" href="#" id="drop1" role="button" class="dropdown-toggle" data-toggle="dropdown">พิมพ์เอกสาร<b class="caret"></b></a>
                <ul class="dropdown-menu" role="menu" aria-labelledby="drop1">
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="/bring/createPaperBring">ใบเบิก</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="/bring/createPaperReturn">ใบคืนสินค้า</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a aria-expanded="false" aria-haspopup="true" href="#" id="drop1" role="button" class="dropdown-toggle" data-toggle="dropdown">คำร้อง <b class="caret"></b></a>
                <ul class="dropdown-menu" role="menu" aria-labelledby="drop1">
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="/purchases/request">คำร้องสั่งซื้อ</a></li>

                </ul>
            </li>
            <li class="dropdown">
                <a aria-expanded="false" aria-haspopup="true" href="#" id="drop1" role="button" class="dropdown-toggle" data-toggle="dropdown">ลงใบรับจ่าย <b class="caret"></b></a>
                <ul class="dropdown-menu" role="menu" aria-labelledby="drop1">
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="/account/manage">ลงใบรับจ่าย</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="/account/product">ลงเบิกจ่ายสินค้า</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="/account/rentals">ลงข้อมูลเช่ายืม</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="/account/repair">ลงข้อมูลส่งซ่อม</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a aria-expanded="false" aria-haspopup="true" href="#" id="drop1" role="button" class="dropdown-toggle" data-toggle="dropdown">ระบบโรงงาน<b class="caret"></b></a>
                <ul class="dropdown-menu" role="menu" aria-labelledby="drop1">
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="/Pr/listPr">ใบ PR</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="/factory/manageCustomers">ข้อมูลลูกค้า</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="/factory/MarkOrder">ผลิตสินค้า(ฝ่ายสำนักงาน)</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="/factory/addOrder">ถอดเเบบ</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="/factory/MarkOrderEngineer">ผลิตสินค้า(ฝ่ายวิศวกร)</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="/factory/MarkOrderManager">ผลิตสินค้า(ฝ่ายผู้จัดการ)</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="/factory/MarkOrderFactory">ผลิตสินค้า(ฝ่ายโรงงาน)</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="/factory/MarkOrderCheckQuality">ผลิตสินค้า(ฝ่ายตรวจสอบ)</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="/factory/MarkOrderStore">ผลิตสินค้า(ฝ่ายคลัง)</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="/factory/Transport">ส่งสินค้า</a></li>
                </ul>
            </li>
        </ul>

        @if(false==empty(\Illuminate\Support\Facades\Session::get('user')))
            {{--*/ $user = \Illuminate\Support\Facades\Session::get('user') /*--}}
            <ul class="nav navbar-nav navbar-right" id="current-user">
                <li id="fat-menu" class="dropdown">
                    <a aria-expanded="false" aria-haspopup="true" href="#" id="drop3" role="button"
                       class="dropdown-toggle" data-toggle="dropdown">{{ $user->staff_firstname }} {{ $user->staff_lastname }}<b class="caret"></b></a>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="drop3">
                        {{--<li role="presentation"><a role="menuitem" tabindex="-1" href="">แก้ไขข้อมูลส่วนตัว</a></li>--}}
                        <li role="presentation" class="divider"></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="/staffs/logout">ออกระบบ</a></li>
                    </ul>
                </li>
            </ul>
        @endif
    </div>
</div>
