<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="1M">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <title>Home</title>
    <link rel="stylesheet" href="{{ URL::asset('bootstrap-3.3.4/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/colpick.css') }}">
    <script src="{{ URL::asset('bootstrap-3.3.4/js/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('bootstrap-3.3.4/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('js/colpick.js') }}"></script>
    <script src="{{ URL::asset('js/typeahead.js') }}"></script>
{{--    <script src="{{ URL::asset('bootstrap-3.3.4/jquery-ui-1.11.4.custom/jquery-ui.js') }}"></script>--}}
    <script src="{{ URL::asset('js/staffsmanager.js') }}"></script>
    <script src="{{ URL::asset('js/vehicle.js') }}"></script>
    <script src="{{ URL::asset('js/pr.js') }}"></script>
    <script src="{{ URL::asset('js/productreport.js') }}"></script>


    <script src="{{ URL::asset('jquery-ui-1.9.1.custom/jquery-ui-1.9.1.custom/js/jquery-ui-1.9.1.custom.min.js') }}"></script>
    <link rel="stylesheet" href="{{ URL::asset('jquery-ui-1.9.1.custom/jquery-ui-1.9.1.custom/css/ui-lightness/jquery-ui-1.9.1.custom.min.css') }}">
    <script src="{{ URL::asset('jQuery-Timepicker-Addon/jquery-ui-sliderAccess.js') }}"></script>
    <script src="{{ URL::asset('jQuery-Timepicker-Addon/jquery-ui-timepicker-addon.js') }}"></script>
    <link rel="stylesheet" href="{{ URL::asset('jQuery-Timepicker-Addon/jquery-ui-timepicker-addon.css') }}">



    {{--    @include('staffs.staffsheader')--}}
    {{--{{ HTML::script('assets/javascripts/staffsmanager.js') }}--}}
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>

    <script type="text/javascript">
        google.load('visualization', '1', {'packages': ['columnchart']});
    </script>
</head>
<body>
<div class="container">

    <header class="row">
        @include('layouts.header')
    </header>

    <div id="main" class="row">
        <script>
            $(document).ready(function () {
                $('input#projects_activity_start_date,input#projects_activity_end_date,input#control_build_create_date,input#return_date,input#member_return_date,input#member_approve_bring_date,input#member_approve_pay_date,input#member_store_date,input#member_account_date,input#purchases_date,input#member_toget_date,input#member_store_date,input#bring_date,input#member_bring_date,input#member_approve_bring_date,input#member_approve_pay_date,input#member_store_date,input#member_account_date,input#member_driver_date').datetimepicker({
                    dateFormat: "yy-mm-dd",
                    timeFormat: "hh:mm:00"
                })
                $('[data-toggle="tooltip"]').tooltip({html: true});
            })
        </script>

        {{--<!-- sidebar content -->--}}
        {{--<div id="sidebar" class="col-md-4">--}}
        {{--@include('layouts.leftmenu')--}}
        {{--</div>--}}

        <!-- main content -->
        <div id="content">
            {{--flash Message--}}
            <div style="display: none;">{{ $success = Session::get('success') }}</div>
            <div style="display: none;">{{ $error = Session::get('error') }}</div>
            <div style="display: none;">{{ $warning = Session::get('warning') }}</div>

            @if($success)
                <div style="" class="alert alert-success alert-dismissible fade in " role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    {{ $success }}
                </div>
            @elseif($error)
                <div style="margin-top: 5px;text-align: center;" class="alert alert-danger alert-dismissible fade in " role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    {{ $error }}
                </div>
            @elseif($warning)
                <div style="margin-top: 5px;text-align: center;" class="alert alert-warning alert-dismissible fade in " role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    {{ $warning }}
                </div>
            @endif

            @yield('content')
        </div>

    </div>

    <footer class="row">
        @include('layouts.footer')
    </footer>

</div>
</body>
</html>