@extends('layouts.layouts')
@section('content')
    <style>
        .has-error > input {
            background-color: rgba(169, 68, 66, 0.1);
        }
    </style>
    <div class="row">
        <div class="col-lg-12">
            <h3>รายชื่อตัวแทนจำหน่ายทั้งหมด</h3>
        </div>
    </div>

    <div class="row" style="margin-top: 20px;">
        <div class="col-lg-4" style="padding: 0px;margin: 0px;">
            <form style="float: left;" class="form-inline" method="GET" action="/companys/search" novalidate>
                <div class="input-group input-group-sm">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-edit"
                                                          aria-hidden="true"></span></span>
                    <input type="text" class="form-control" name="search" placeholder="ค้นหา ชื่อตัวแทน">
                </div>
                <button type="submit" class="btn btn-sm"><span class="glyphicon glyphicon-search"
                                                               aria-hidden="true"></span></button>
            </form>

        </div>
        <div class="col-lg-8">
            <button style="float: right;" type="submit" class="btn btn-success" data-toggle="modal"
                    data-target="#myModal"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> เพิ่ม
            </button>
        </div>
    </div>

    <div class="row" style="float: right;">
        <div class="col-lg-12">
            <nav>
                <ul class="pagination" style="margin: 0px;padding: 0px;padding-top: 3px;">
                    @if($company['page']>1)
                        <li>
                            <a href="/companys/search?page={{$company['page']-1}}@if(false==empty($company['search']))&search={{$company['search']}}@endif">Prev</a>
                        </li>
                    @endif

                    @for($x = 1; $x <= $company['maxProduct']; $x++)
                        @if($x==$company['page'])
                            <li class="active"><a
                                        href="/companys/search?page={{$x}}@if(false==empty($product['search']))&search={{$company['search']}}@endif">{{$x}}</a>
                            </li>
                        @else
                            <li>
                                <a href="/companys/search?page={{$x}}@if(false==empty($company['search']))&search={{$company['search']}}@endif">{{$x}}</a>
                            </li>
                        @endif
                    @endfor

                    @if($company['page']<$company['maxProduct'])
                        <li>
                            <a href="/companys/search?page={{$company['page']+1}}@if(false==empty($company['search']))&search={{$company['search']}}@endif">Next</a>
                        </li>
                    @endif
                </ul>
            </nav>
        </div>
    </div>

    <div class="row" style="margin-top: 10px;">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                <tr class="info">
                    <th>#</th>
                    <th>ชื่อตัวแทนจำหน่าย</th>
                    <th>ที่ตั้ง</th>
                    <th>เบอร์ติดต่อ</th>
                    <th>ผู้ประสานงาน</th>
                    <th>จัดการ</th>
                </tr>
                </thead>
                <tbody>
                @foreach($company['dataCompanys'] as $key=>$value)
                    <tr>
                        <th scope="row">{{$key+1}}</th>
                        <td>{{$value->company_name}}</td>
                        <td>{{$value->location}}</td>
                        <td>{{$value->phone}}</td>
                        <td>{{$value->contractor}}</td>
                        <td>
                            <a href="/companys/displaybyid?id={{$value->company_id}}&@if(false==empty($page))page={{$page}}@endif&@if(false==empty($search))search={{$search}}@endif"
                               class="btn btn-primary">แก้ไข</a>
                            <a href="/companys/delete?id={{$value->company_id}}" class="btn btn-danger"
                               onclick="return confirm('ยืนยันการลบข้อมูลนี้ ?')">ลบ</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="modal fade @if(true==$errors->has()||false==empty($dataCompany['company_id'])) in @endif"
                 style="@if(true==$errors->has()||false==empty($dataCompany['company_id'])) display: block; @endif"
                 id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                @if (false==empty($dataCompany['company_id']))
                    <form style="" class="form-horizontal" method="POST" action="/companys/validationEditManage?id=
                @if(false==empty($dataCompany['company_id'])){{$dataCompany['company_id']}}@endif&
                @if(false==empty($page))page={{$page}}@endif&
                @if(false==empty($page))search={{$search}}@endif" novalidate>
                        @else
                            <form style="" class="form-horizontal" method="POST" action="/companys/validationManage"
                                  novalidate>
                                @endif
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <a href="/companys/manage" type="button" class="close" data-dismiss="modal"
                                               aria-label="Close"><span aria-hidden="true">&times;</span></a>
                                            <h4 class="modal-title">จัดการชื่อตัวแทนจำหน่าย</h4>
                                        </div>
                                        <div class="modal-body">
                                            @if (true==$errors->has())
                                                <div class="alert alert-danger">
                                                    @foreach ($errors->all() as $error)
                                                        {{ $error }}<br>
                                                    @endforeach
                                                </div>
                                            @endif
                                            <div class="form-group @if ($errors->has('company_name')) has-error @endif">
                                                <label class="col-sm-4 control-label" style="padding: 5px 25px 5px 5px;" for="exampleInputName2">ชื่อตัวแทนจำหน่าย</label>
                                                <input class="col-sm-6" type="text" class="form-control" name="company_name"
                                                       value="@if (false==empty($dataCompany['company_name'])){{$dataCompany['company_name']}}@elseif(false==empty(Input::old('company_name'))){{Input::old('company_name')}}@endif"
                                                       placeholder="ชื่อตัวแทนจำหน่าย">
                                            </div>
                                            <div class="form-group @if ($errors->has('company_name')) has-error @endif">
                                                <label class="col-sm-4 control-label" style="padding: 5px 25px 5px 5px;" for="exampleInputName2">ที่ตั้ง</label>
                                                <input class="col-sm-6" type="text" class="form-control" name="location"
                                                       value="@if (false==empty($dataCompany['location'])){{$dataCompany['location']}}@elseif(false==empty(Input::old('location'))){{Input::old('location')}}@endif"
                                                       placeholder="ที่ตั้ง">
                                            </div>
                                            <div class="form-group @if ($errors->has('company_name')) has-error @endif">
                                                <label class="col-sm-4 control-label" style="padding: 5px 25px 5px 5px;" for="exampleInputName2">เบอร์ติดต่อ</label>
                                                <input class="col-sm-6" type="text" class="form-control" name="phone"
                                                       value="@if (false==empty($dataCompany['phone'])){{$dataCompany['phone']}}@elseif(false==empty(Input::old('phone'))){{Input::old('phone')}}@endif"
                                                       placeholder="เบอร์ติดต่อ">
                                            </div>
                                            <div class="form-group @if ($errors->has('company_name')) has-error @endif">
                                                <label class="col-sm-4 control-label" style="padding: 5px 25px 5px 5px;" for="exampleInputName2">คนประสานงาน</label>
                                                <input class="col-sm-6" type="text" class="form-control" name="contractor"
                                                       value="@if (false==empty($dataCompany['contractor'])){{$dataCompany['contractor']}}@elseif(false==empty(Input::old('contractor'))){{Input::old('contractor')}}@endif"
                                                       placeholder="ผู้ประสานงาน">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <a href="/companys/manage" type="button" class="btn btn-default"
                                               data-dismiss="modal"><span class="glyphicon glyphicon-off"
                                                                          aria-hidden="true"></span> ปิด</a>
                                            @if (false==empty($dataCompany['company_id']))
                                                <button type="submit" class="btn btn-primary"><span
                                                            class="glyphicon glyphicon-pencil"
                                                            aria-hidden="true"></span> แก้ไข
                                                </button>
                                            @else
                                                <button type="submit" class="btn btn-success"><span
                                                            class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                                    เพิ่ม
                                                </button>
                                            @endif
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </form>
            </div>
            <!-- /.modal -->
        </div>
    </div>

@stop